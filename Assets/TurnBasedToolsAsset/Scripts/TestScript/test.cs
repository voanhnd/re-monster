using UnityEngine;

public class test : MonoBehaviour
{
    public Transform target;

    Vector3 offset = new Vector3(0f, -90f, 0f);

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        transform.rotation *= Quaternion.Euler(offset);
    }
}
