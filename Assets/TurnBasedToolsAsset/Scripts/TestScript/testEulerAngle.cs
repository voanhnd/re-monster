using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Monster;
using UnityEngine;

public class testEulerAngle : MonoBehaviour
{
    public Transform origin, target;

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = new Vector3(target.position.x, transform.position.y, target.position.z);

        Vector3 targetDir = targetPos - origin.position;

        ////180 to -180 angle
        float angle = Vector3.SignedAngle(targetDir.normalized, new Vector3(0f, 0f, 1f), Vector3.down);
        //                                //Look direction     /Look vector             /Rotate vector

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, angle, 0f), 1f);

        Debug.Log("euler angle = " + Quaternion.Euler(0f, angle, 0f).eulerAngles);

        Debug.Log("hex face = " + ParameterInGame.GetHexFaceTo(Quaternion.Euler(0f, angle, 0f).eulerAngles.y));
    }
}
