using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testWorldPosToScreenSpaceUI : MonoBehaviour
{
    public Camera worldCam;
    public Camera uiCam;

    public Canvas canvas;

    public Transform target;

    private void Update()
    {
        UpdatePos();
    }

    public void UpdatePos()
    {
        var screen = worldCam.WorldToScreenPoint(target.transform.position);
        screen.z = (canvas.transform.position - uiCam.transform.position).magnitude;
        var position = uiCam.ScreenToWorldPoint(screen);
        transform.position = position;
    }
}
