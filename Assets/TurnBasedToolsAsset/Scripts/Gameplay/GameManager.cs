﻿using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public enum CellState
{
    eNormal,
    eHover,
    ePositive,
    eNegative,
    eMovement,
    eBlocked
}

[System.Serializable]
public class GameTeamEvent : UnityEvent<GameTeam>
{ }

[System.Serializable]
public class GridUnitEvent : UnityEvent<GridUnit>
{ }

[System.Serializable]
public struct TeamInfo
{
    public int TeamId;

    public TeamInfo(int InTeamId)
    {
        TeamId = InTeamId;
    }

    public static TeamInfo InvalidTeam()
    {
        return new TeamInfo(-1);
    }

    public bool IsValid()
    {
        return TeamId != -1;
    }

    public override bool Equals(object obj)
    {
        TeamInfo otherTeamInfo = (TeamInfo)obj;
        return otherTeamInfo.TeamId == TeamId;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

public class GameManager : MonoBehaviour
{
    private static GameManager sInstance = null;

    [Space(10)]

    [SerializeField] private ILevelGrid m_LevelGrid;

    [SerializeField] private GameRules m_GameRules;

    [SerializeField] private FogOfWar m_FogOfWar;

    [SerializeField][HideInInspector] private InputHandler m_InputHandler;

    [SerializeField] private CameraController m_CameraController;

    [Space(10)]

    [SerializeField] private GameObject m_SelectedHoverObject;

    [Space(10)]
    [Header("Team Data")]

    [SerializeField] private HumanTeamData m_FriendlyTeamData;

    [SerializeField] private TeamData m_HostileTeamData;

    [Space(10)]

    [SerializeField] private WinCondition[] m_WinConditions;

    [SerializeField] private GameObject[] m_SpawnOnStart;

    [SerializeField] private GameObject[] m_AddToSpawnedUnits;

    [SerializeField] private AbilityParticle[] m_DeathParticles;

    [Space(10)]

    public GameTeamEvent OnTeamWon;

    [HideInInspector] public GridUnitEvent OnUnitSelected;

    [HideInInspector] public GridUnitEvent OnUnitHover = new();
    private List<GridObject> SpawnedCellObjects = new();
    private Dictionary<GameTeam, List<GridUnit>> m_Teams = new();
    private Dictionary<GameTeam, int> m_NumberOfKilledTargets = new();
    private Dictionary<GameTeam, int> m_NumberOfKilledEntities = new();
    private List<ILevelCell> CurrentHoverCells = new();
    private UnityEvent OnFinishedPerformedActions = new();
    private ILevelCell m_CurrentHoverCell;
    private int m_NumActionsBeingPerformed;
    private bool m_bIsPlaying = false;

    private void Awake()
    {
        if (sInstance == null)
        {
            sInstance = this;
        }
    }

    private void Start()
    {
        DirectionalCellSpawner[] CellSpawners = (DirectionalCellSpawner[])GameObject.FindObjectsOfType(typeof(DirectionalCellSpawner));
        foreach (DirectionalCellSpawner cellSpawner in CellSpawners)
        {
            Destroy(cellSpawner.gameObject);
        }

        if (!m_LevelGrid)
        {
            Debug.Log("([TurnBasedTools]::GameManager::Start) Missing Grid");
        }

        if (!m_GameRules)
        {
            Debug.Log("([TurnBasedTools]::GameManager::Start) Missing GameRules");
        }

        if (m_WinConditions.Length == 0)
        {
            Debug.Log("([TurnBasedTools]::GameManager::Start) Missing WinConditions");
        }

        if (!m_FriendlyTeamData)
        {
            Debug.Log("([TurnBasedTools]::GameManager::Start) Missing Friendly Team Data");
        }

        if (!m_HostileTeamData)
        {
            Debug.Log("([TurnBasedTools]::GameManager::Start) Missing Hostile Team Data");
        }

        if (m_FriendlyTeamData)
        {
            m_FriendlyTeamData.SetTeam(GameTeam.Blue);
        }

        if (m_HostileTeamData)
        {
            m_HostileTeamData.SetTeam(GameTeam.Red);
        }

        if (m_GameRules)
        {
            m_InputHandler.OnNumPressed.AddListener(m_GameRules.HandleNumPressed);
            OnTeamWon.AddListener(m_GameRules.HandleTeamWon);
        }

        Initalize();

        foreach (GameObject SpawnObj in m_SpawnOnStart)
        {
            if (SpawnObj)
            {
                Instantiate(SpawnObj);
            }
        }
    }

    private void Update()
    {
        m_InputHandler.Update();

        if (m_GameRules)
            m_GameRules.Update();
    }

    private void Initalize()
    {
        SetupGrid();
        if (m_GameRules)
            m_GameRules.InitalizeRules();

        if (m_FogOfWar)
            m_FogOfWar.SpawnFogObjects();
    }

    private void SetupGrid()
    {
        if (m_LevelGrid)
        {
            m_LevelGrid.SetupAllCellAdjacencies();
            m_LevelGrid.OnCellInteraction.AddListener(HandleInteraction);
        }
        SetupMaterials();
    }

    private void SetupMaterials()
    {
        if (!m_LevelGrid)
            return;

        ILevelCell TestCell = null;

        List<ILevelCell> LevelCells = m_LevelGrid.GetAllCells();
        if (LevelCells.Count > 0)
            TestCell = LevelCells[0];
        if (TestCell == null) return;
        for (int i = 0; i < LevelCells.Count; i++)
        {
            ILevelCell currCell = LevelCells[i];
            CellState cellState = currCell.GetNormalState();
            currCell.SetCellState(cellState);
            currCell.SetMaterial(cellState);
        }
    }

    #region Public Statics

    public static GameManager Get() => sInstance;

    public static ILevelGrid GetGrid() => sInstance.m_LevelGrid;

    public static GameRules GetRules() => sInstance.m_GameRules;

    public static FogOfWar GetFogOfWar() => sInstance.m_FogOfWar;

    public static CameraController GetCameraController() => sInstance.m_CameraController;

    public static GameObject GetSelectedHoverPrefab() => sInstance.m_SelectedHoverObject;

    public static List<WinCondition> GetWinConditions() => new(sInstance.m_WinConditions);

    public static GameTeam GetTeamAffinity(GameTeam InTeam1, GameTeam InTeam2)
    {
        if (InTeam1 == InTeam2)
        {
            return GameTeam.Blue;
        }

        return GameTeam.Red;
    }

    public static Dictionary<GameTeam, List<GridUnit>> GetTeamsMap() => sInstance.m_Teams;

    public static List<GameTeam> GetTeamList()
    {
        List<GameTeam> outTeams = new();

        foreach (GameTeam item in GetTeamsMap().Keys)
        {
            outTeams.Add(item);
        }

        return outTeams;
    }

    //Checks if a player is moving, attacking or basically anything that should prevent input.
    public static bool IsActionBeingPerformed() => (sInstance.m_NumActionsBeingPerformed > 0);

    public static void AddActionBeingPerformed() => ++sInstance.m_NumActionsBeingPerformed;

    public static void RemoveActionBeingPerformed()
    {
        --sInstance.m_NumActionsBeingPerformed;

        if (sInstance.m_NumActionsBeingPerformed == 0)
        {
            sInstance.OnFinishedPerformedActions.Invoke();
        }
    }

    public static void BindToOnFinishedPerformedActions(UnityAction InAction) => sInstance.OnFinishedPerformedActions.AddListener(InAction);

    public static void UnBindFromOnFinishedPerformedActions(UnityAction InAction) => sInstance.OnFinishedPerformedActions.RemoveListener(InAction);

    public static int GetNumTargetsKilled(GameTeam InTeam)
    {
        if (sInstance.m_NumberOfKilledTargets.ContainsKey(InTeam))
        {
            return sInstance.m_NumberOfKilledTargets[InTeam];
        }

        return 0;
    }

    public static int NumUnitsKilled(GameTeam InTeam)
    {
        if (sInstance.m_NumberOfKilledEntities.ContainsKey(InTeam))
        {
            return sInstance.m_NumberOfKilledEntities[InTeam];
        }

        return 0;
    }

    public static bool AreAllUnitsOnTeamDead(GameTeam InTeam)
    {
        Debug.Log("Are All Units On Team Dead team = " + InTeam.ToString());
        Debug.Log("Number of team unit killed = " + NumUnitsKilled(InTeam));
        Debug.Log("sInstance.m_Teams[InTeam].Count = " + sInstance.m_Teams[InTeam].Count);
        Debug.Log("Units on team count = " + GetUnitsOnTeam(InTeam).Count);

        return NumUnitsKilled(InTeam) == sInstance.m_Teams[InTeam].Count;
    }

    public static bool CanFinishTurn()
    {
        bool bActionBeingPerformed = IsActionBeingPerformed();
        bool bIsTeamHuman = true;

        GameRules gameRules = GetRules();
        if (gameRules)
        {
            bIsTeamHuman = IsTeamHuman(gameRules.GetCurrentTeam());
        }

        return (!bActionBeingPerformed && bIsTeamHuman);
    }

    public static int GetNumOfTargets(GameTeam InTeam) => GetTeamTargets(InTeam).Count;

    public static List<GridUnit> GetTeamTargets(GameTeam InTeam)
    {
        List<GridUnit> units = new();

        if (!sInstance.m_Teams.ContainsKey(InTeam)) return units;

        foreach (GridUnit unit in sInstance.m_Teams[InTeam])
        {
            if (unit.IsTarget())
            {
                units.Add(unit);
            }
        }

        return units;
    }

    public static bool KilledAllTargets(GameTeam InTeam) => GetNumTargetsKilled(InTeam) == GetNumOfTargets(InTeam);

    public static HumanTeamData GetFriendlyTeamData() => sInstance.m_FriendlyTeamData;

    public static TeamData GetHostileTeamData() => sInstance.m_HostileTeamData;

    public static TeamData GetDataForTeam(GameTeam InTeam)
    {
        switch (InTeam)
        {
            case GameTeam.Blue:
                return GetFriendlyTeamData();
            case GameTeam.Red:
                return GetHostileTeamData();
        }

        Debug.Log("([TurnBasedTools]::GameManager::GetDataForTeam) Trying to get TeamData for invalid team: " + InTeam.ToString());
        return new();
    }

    public static T GetDataForTeam<T>(GameTeam InTeam) where T : TeamData
    {
        return InTeam switch
        {
            GameTeam.Blue => GetFriendlyTeamData() as T,
            GameTeam.Red => GetHostileTeamData() as T,
            _ => null,
        };
    }

    public static GridObject SpawnObjectOnCell(GameObject InObject, ILevelCell InCell, Vector3 InOffset = default)
    {
        if (InCell && InObject)
        {
            GridObject SpawnedGridObject = Instantiate(InObject).AddComponent<GridObject>();

            float ObjHeight = SpawnedGridObject.GetBounds().y;
            float CellHeight = InCell.GetRenderer().bounds.size.y;

            Vector3 HeightOffset = new(0.0f, (CellHeight * 0.5f) + (ObjHeight * 0.5f), 0.0f);

            SpawnedGridObject.gameObject.transform.position = InCell.gameObject.transform.position + HeightOffset + InOffset;

            SpawnedGridObject.Initalize();
            SpawnedGridObject.SetGrid(GetGrid());
            SpawnedGridObject.SetCurrentCell(InCell);
            SpawnedGridObject.PostInitalize();

            return SpawnedGridObject;
        }

        return null;
    }

    public static GridUnit SpawnUnit(UnitData InUnitData, GameTeam InTeam, Vector2 InIndex, CompassDir InStartDirection = CompassDir.S)
    {
        ILevelCell cell = sInstance.m_LevelGrid[InIndex];

        if (InTeam == GameTeam.Blue)
        {
            cell.SetVisible(true);
        }

        GridUnit SpawnedGridUnit;

        if (InUnitData.m_UnitClass == "")
        {
            SpawnedGridUnit = Instantiate(InUnitData.m_Model).AddComponent<GridUnit>();
        }
        else
        {
            System.Type classType = GameUtils.FindType(InUnitData.m_UnitClass);
            SpawnedGridUnit = Instantiate(InUnitData.m_Model).AddComponent(classType) as GridUnit;
        }

        SpawnedGridUnit.Initalize();
        SpawnedGridUnit.SetUnitData(InUnitData);
        SpawnedGridUnit.SetTeam(InTeam);
        SpawnedGridUnit.SetGrid(sInstance.m_LevelGrid);
        SpawnedGridUnit.SetCurrentCell(cell);
        SpawnedGridUnit.AlignToGrid();
        SpawnedGridUnit.PostInitalize();

        ILevelCell DirCell = SpawnedGridUnit.GetCell().GetAdjacentCell(InStartDirection);
        if (DirCell)
        {
            SpawnedGridUnit.LookAtCell(DirCell);
        }

        foreach (GameObject obj in sInstance.m_AddToSpawnedUnits)
        {
            if (obj)
            {
                Instantiate(obj, SpawnedGridUnit.gameObject.transform);
            }
        }

        sInstance.SpawnedCellObjects.Add(SpawnedGridUnit);

        if (!sInstance.m_Teams.ContainsKey(InTeam))
        {
            sInstance.m_Teams.Add(InTeam, new List<GridUnit>());
        }

        sInstance.m_Teams[InTeam].Add(SpawnedGridUnit);

        if (InTeam != GameTeam.Blue)
        {
            return SpawnedGridUnit;
        }

        if (sInstance.m_FogOfWar)
        {
            sInstance.m_FogOfWar.CheckPoint(SpawnedGridUnit.GetCell());
        }

        return SpawnedGridUnit;
    }

    public static GridUnit SpawnUnit(UnitData InUnitData, GameObject unitModel, GameTeam InTeam, ILevelCell spawnCell, CompassDir InStartDirection = CompassDir.S)
    {
        GridUnit SpawnedGridUnit;

        if (InUnitData.m_UnitClass == "")
        {
            SpawnedGridUnit = unitModel.AddComponent<GridUnit>();
        }
        else
        {
            System.Type classType = GameUtils.FindType(InUnitData.m_UnitClass);
            SpawnedGridUnit = unitModel.AddComponent(classType) as GridUnit;
        }

        SpawnedGridUnit.Initalize();
        SpawnedGridUnit.SetUnitData(InUnitData);
        SpawnedGridUnit.SetTeam(InTeam);
        SpawnedGridUnit.SetGrid(sInstance.m_LevelGrid);
        SpawnedGridUnit.SetCurrentCell(spawnCell);
        SpawnedGridUnit.AlignToGrid();
        SpawnedGridUnit.PostInitalize();

        ILevelCell DirCell = SpawnedGridUnit.GetCell().GetAdjacentCell(InStartDirection);
        if (DirCell)
        {
            SpawnedGridUnit.LookAtCell(DirCell);
        }

        for (int i = 0; i < sInstance.m_AddToSpawnedUnits.Length; i++)
        {
            GameObject obj = sInstance.m_AddToSpawnedUnits[i];
            if (obj)
            {
                Instantiate(obj, SpawnedGridUnit.gameObject.transform);
            }
        }

        sInstance.SpawnedCellObjects.Add(SpawnedGridUnit);

        if (!sInstance.m_Teams.ContainsKey(InTeam))
        {
            sInstance.m_Teams.Add(InTeam, new List<GridUnit>());
        }

        sInstance.m_Teams[InTeam].Add(SpawnedGridUnit);

        return SpawnedGridUnit;
    }

    public static GridUnit SnapUnitCell(GridUnit SpawnedGridUnit, ILevelCell changeCell, CompassDir InStartDirection = CompassDir.S)
    {
        Debug.Log("SnapUnitCell unit id = " + SpawnedGridUnit.GetComponent<MonsterInGame>().MonsterInfo.monsterID);
        Debug.Log("Current cell = " + SpawnedGridUnit.GetCell().name);
        Debug.Log("change Cell = " + changeCell.name);

        SpawnedGridUnit.SetCurrentCell(changeCell);
        Vector3 pos = changeCell.GetAllignPos(SpawnedGridUnit);
        SpawnedGridUnit.transform.position = pos;

        ILevelCell DirCell = SpawnedGridUnit.GetCell().GetAdjacentCell(InStartDirection);
        if (DirCell)
        {
            SpawnedGridUnit.LookAtCell(changeCell);
        }

        return SpawnedGridUnit;
    }

    private void SpawnDeathParticlesForUnit(GridUnit InUnit)
    {
        for (int i = 0; i < m_DeathParticles.Length; i++)
        {
            AbilityParticle particle = m_DeathParticles[i];
            if (!particle)
            {
                continue;
            }
            AbilityParticle spawnedParticle = Instantiate(particle.gameObject, InUnit.gameObject.transform.position, particle.gameObject.transform.rotation).GetComponent<AbilityParticle>();
            if (spawnedParticle)
            {
                spawnedParticle.Setup(null, null, null);
            }
        }
    }

    public static List<Renderer> GetAllRenderersOfObject(GameObject InObject)
    {
        List<Renderer> Renderers = new()
        {
            InObject.GetComponent<Renderer>()
        };
        Renderers.AddRange(InObject.GetComponentsInChildren<Renderer>());

        return Renderers;
    }

    public static Vector3 GetBoundsOfObject(GameObject InObject)
    {
        if (!InObject) return new Vector3();
        Vector3 bounds = new();

        List<Renderer> Renderers = GetAllRenderersOfObject(InObject);
        for (int i = 0; i < Renderers.Count; i++)
        {
            Renderer currRenderer = Renderers[i];
            if (!currRenderer)
            {
                continue;
            }
            Vector3 rendererBound = currRenderer.bounds.size;
            if (rendererBound.x > bounds.x)
            {
                bounds.x = rendererBound.x;
            }
            if (rendererBound.y > bounds.y)
            {
                bounds.y = rendererBound.y;
            }
            if (rendererBound.z > bounds.z)
            {
                bounds.z = rendererBound.z;
            }
        }

        return bounds;
    }

    public static List<GridUnit> GetUnitsOnTeam(GameTeam InTeam)
    {
        if (InTeam == GameTeam.None)
        {
            Debug.Log("([TurnBasedTools]::GameManager::GetUnitsOnTeam) Trying to get units for invalid team: " + InTeam.ToString());
        }

        List<GridUnit> gridUnits = new();

        if (sInstance.m_Teams.ContainsKey(InTeam))
        {
            gridUnits.AddRange(sInstance.m_Teams[InTeam]);

            return gridUnits;
        }

        return gridUnits;
    }

    public static bool IsTeamHuman(GameTeam InTeam) => GetDataForTeam<HumanTeamData>(InTeam) != null;

    public static bool IsTeamAI(GameTeam InTeam) => GetDataForTeam<AITeamData>(InTeam) != null;

    public static bool IsUnitOnTeam(GridUnit InUnit, GameTeam InTeam)
    {
        if (sInstance.m_Teams.ContainsKey(InTeam))
        {
            return sInstance.m_Teams[InTeam].Contains(InUnit);
        }
        return false;
    }

    public static bool IsPlaying() => sInstance.m_bIsPlaying;

    public static void HandleGameStarted()
    {
        sInstance.m_bIsPlaying = true;
    }

    public static GameTeam GetUnitTeam(GridUnit InUnit)
    {
        if (IsUnitOnTeam(InUnit, GameTeam.Blue))
        {
            return GameTeam.Blue;
        }
        if (IsUnitOnTeam(InUnit, GameTeam.Red))
        {
            return GameTeam.Red;
        }

        return GameTeam.None;
    }

    public static void ResetCellState(ILevelCell InCell)
    {
        SetCellState(InCell, InCell.GetNormalState());
    }

    public static void SetCellState(ILevelCell InCell, CellState InCellState)
    {
        if (!sInstance.m_LevelGrid)
        {
            return;
        }
        InCell.SetMaterial(InCellState);
        InCell.SetCellState(InCellState);
        if (InCell.IsMouseOver())
        {
            sInstance.BeginHover(InCell);
        }
    }

    public static bool CanCasterEffectTarget(ILevelCell InCaster, ILevelCell InTarget, GameTeam InEffectedTeam, bool bAllowBlocked)
    {
        if (!InCaster || !InTarget)
        {
            return false;
        }

        if ((InCaster.IsBlocked() || InTarget.IsBlocked()) && !bAllowBlocked)
        {
            return false;
        }

        if (InEffectedTeam == GameTeam.None)
        {
            return false;
        }

        if (InCaster.GetCellTeam() == GameTeam.None)
        {
            return true;
        }
        GameTeam ObjAffinity = GameManager.GetTeamAffinity(InCaster.GetCellTeam(), InTarget.GetCellTeam());
        if (ObjAffinity == GameTeam.Blue && InEffectedTeam == GameTeam.Red)
        {
            return false;
        }

        return true;
    }

    public static void FinishTurn()
    {
        if (!CanFinishTurn())
        {
            return;
        }
        if (sInstance.m_GameRules)
        {
            sInstance.m_GameRules.EndTurn();
        }

        CheckWinConditions();
    }

    public static void CheckWinConditions()
    {
        if (!sInstance.m_bIsPlaying)
        {
            return;
        }

        Dictionary<GameTeam, int> TeamToWinCount = new()
        {
            { GameTeam.Blue, 0},
            { GameTeam.Red, 0}
        };

        int NumWinConditions = sInstance.m_WinConditions.Length;

        for (int i = 0; i < sInstance.m_WinConditions.Length; i++)
        {
            WinCondition currWinCondition = sInstance.m_WinConditions[i];
            if (!currWinCondition)
            {
                continue;
            }
            if (currWinCondition.m_bCheckWinFirst)
            {
                if (DidTeamPassCondition(currWinCondition, GameTeam.Blue) && ++TeamToWinCount[GameTeam.Blue] >= NumWinConditions)
                {
                    TeamWon(GameTeam.Blue);
                }

                if (DidTeamPassCondition(currWinCondition, GameTeam.Red) && ++TeamToWinCount[GameTeam.Red] >= NumWinConditions)
                {
                    TeamWon(GameTeam.Red);
                }

                //CheckLost(currWinCondition, GameTeam.Friendly);
                //CheckLost(currWinCondition, GameTeam.Hostile);
                continue;
            }
            //CheckLost(currWinCondition, GameTeam.Friendly);
            //CheckLost(currWinCondition, GameTeam.Hostile);

            if (DidTeamPassCondition(currWinCondition, GameTeam.Blue) && ++TeamToWinCount[GameTeam.Blue] >= NumWinConditions)
            {
                TeamWon(GameTeam.Blue);
            }

            if (DidTeamPassCondition(currWinCondition, GameTeam.Red) && ++TeamToWinCount[GameTeam.Red] >= NumWinConditions)
            {
                TeamWon(GameTeam.Red);
            }
        }
    }

    public static void HandleUnitDeath(GridUnit InUnit)
    {
        if (!sInstance.m_NumberOfKilledEntities.ContainsKey(InUnit.GetTeam()))
        {
            sInstance.m_NumberOfKilledEntities.Add(InUnit.GetTeam(), 0);
        }

        sInstance.m_NumberOfKilledEntities[InUnit.GetTeam()]++;

        if (InUnit.IsTarget())
        {
            if (!sInstance.m_NumberOfKilledTargets.ContainsKey(InUnit.GetTeam()))
            {
                sInstance.m_NumberOfKilledTargets.Add(InUnit.GetTeam(), 0);
            }

            sInstance.m_NumberOfKilledTargets[InUnit.GetTeam()]++;
        }

        sInstance.SpawnDeathParticlesForUnit(InUnit);

        CheckWinConditions();
    }

    public static void HandleUnitActivated(GridUnit InUnit)
    {

    }

    #endregion

    #region Event Handles
    private void BeginHover(ILevelCell InCell)
    {
        m_CurrentHoverCell = InCell;
        UpdateHoverCells();

        m_GameRules.BeginHover(m_CurrentHoverCell);
    }

    public ILevelCell GetHoverCell() => m_CurrentHoverCell;

    public void UpdateHoverCells()
    {
        CleanupHoverCells();

        if (!m_CurrentHoverCell) return;

        GridUnit hoverGrid = m_CurrentHoverCell.GetUnitOnCell();

        if (hoverGrid)
            OnUnitHover.Invoke(hoverGrid);
        // prevent dupplicate
        if (!CurrentHoverCells.Contains(m_CurrentHoverCell))
            CurrentHoverCells.Add(m_CurrentHoverCell);

        GameRules gameRules = GetRules();
        SetGameRuleAction(gameRules);

        for (int i = 0; i < CurrentHoverCells.Count; i++)
        {
            ILevelCell currCell = CurrentHoverCells[i];
            currCell.SetMaterial(CellState.eHover);
        }

        m_CurrentHoverCell.HandleMouseOver();
    }

    private void SetGameRuleAction(GameRules gameRules)
    {
        if (!gameRules) return;

        GridUnit selectedUnit = gameRules.GetSelectedUnit();

        if (!selectedUnit) return;

        UnitState unitState = selectedUnit.GetCurrentState();
        switch (unitState)
        {
            case UnitState.UsingAbility:
                CurrentHoverCells.AddRange(selectedUnit.GetAbilityHoverCells(m_CurrentHoverCell));
                break;
            case UnitState.Moving:
                //ILevelCell targetMovementCell = CalculateMoveToCell(selectedUnit, m_CurrentHoverCell);

                List<ILevelCell> allowedMovementCells = selectedUnit.GetAllowedMovementCells();

                if (!allowedMovementCells.Contains(m_CurrentHoverCell)) break;

                List<ILevelCell> PathToCursor = selectedUnit.GetPathTo(m_CurrentHoverCell, allowedMovementCells);

                for (int i = 0; i < PathToCursor.Count; i++)
                {
                    ILevelCell pathCell = PathToCursor[i];
                    if (pathCell && allowedMovementCells.Contains(pathCell))
                        CurrentHoverCells.Add(pathCell);
                }
                break;
        }
    }

    public static ILevelCell CalculateMoveToCell(GridUnit InUnit, ILevelCell targetCell)
    {
        List<ILevelCell> AllowedMovementCells = InUnit.GetAllowedMovementCells();

        //bool bAbleToAttack = AllowedMovementCells.Count > 0;
        //if (bAbleToAttack)
        //{
        //    //Cells exist that allow movement, and attack.
        //    int currDistance = -1;
        //    ILevelCell selectedCell = null;
        //    foreach (ILevelCell levelCell in AllowedMovementCells)
        //    {
        //        AIPathInfo pathInfo = new AIPathInfo
        //        {
        //            StartCell = levelCell,
        //            TargetCell = targetCell,
        //            bIgnoreUnits = false,
        //            bTakeWeightIntoAccount = true
        //        };

        //        List<ILevelCell> levelPath = AIManager.GetPath(pathInfo);
        //        int cellDistance = levelPath.Count - 1;
        //        if (cellDistance > currDistance)
        //        {
        //            selectedCell = levelCell;
        //            currDistance = cellDistance;
        //        }
        //    }

        //    Debug.Log("InUnit move");

        //    return selectedCell;
        //}
        //else// Move towards target
        int currDistance = int.MaxValue;
        ILevelCell selectedCell = null;
        foreach (ILevelCell levelCell in AllowedMovementCells)
        {
            AIPathInfo pathInfo = new()
            {
                StartCell = levelCell,
                TargetCell = targetCell,
                bIgnoreUnits = false,
                bTakeWeightIntoAccount = true
            };

            int cellDistance = AIManager.GetPath(pathInfo).Count - 1;
            if (cellDistance < currDistance)
            {
                selectedCell = levelCell;
                currDistance = cellDistance;
            }
        }

        Debug.Log("InUnit move towards");

        return selectedCell;
    }

    private void EndHover(ILevelCell InCell)
    {
        CleanupHoverCells();

        if (InCell)
        {
            InCell.HandleMouseExit();
        }

        m_CurrentHoverCell = null;

        OnUnitHover.Invoke(null);

        m_GameRules.EndHover(InCell);
    }

    public void CustomEndHover()
    {
        CleanupHoverCells();

        if (m_CurrentHoverCell)
        {
            m_CurrentHoverCell.HandleMouseExit();

            m_CurrentHoverCell.SetMaterial(m_CurrentHoverCell.GetCellState());
        }

        m_CurrentHoverCell = null;
    }

    public List<ILevelCell> GetTargetsHover()
    {
        List<ILevelCell> levelCells = new();

        levelCells.AddRange(CurrentHoverCells);

        return levelCells;
    }

    private void CleanupHoverCells()
    {
        for (int i = 0; i < CurrentHoverCells.Count; i++)
        {
            ILevelCell currCell = CurrentHoverCells[i];
            currCell.HandleMouseExit();
            currCell.SetMaterial(currCell.GetCellState());
        }

        CurrentHoverCells.Clear();
    }

    public void HandleCellClicked(ILevelCell InCell)
    {
        if (!InCell)
        {
            return;
        }

        if (!m_GameRules)
        {
            return;
        }

        GridUnit gridUnit = InCell.GetUnitOnCell();
        if (gridUnit)
        {
            GameTeam CurrentTurnTeam = m_GameRules.GetCurrentTeam();
            GameTeam UnitsTeam = gridUnit.GetTeam();

            if (UnitsTeam == CurrentTurnTeam)
            {
                m_GameRules.HandlePlayerSelected(gridUnit);

                Debug.Log("Player clicked Current Turn Team = " + CurrentTurnTeam);
            }
            else if (UnitsTeam != CurrentTurnTeam)
            {
                m_GameRules.HandleEnemySelected(gridUnit);

                Debug.Log("Enemy clicked Current Turn Team = " + CurrentTurnTeam);
            }
        }
        m_GameRules.HandleCellSelected(InCell);
    }

    private static bool DidTeamPassCondition(WinCondition InCondition, GameTeam InTeam) => InCondition && InCondition.CheckTeamWin(InTeam);

    private static void CheckLost(WinCondition InCondition, GameTeam InTeam)
    {
        if (InCondition && InCondition.CheckTeamLost(InTeam))
        {
            TeamLost(InTeam);
        }
    }

    private static void TeamWon(GameTeam InTeam)
    {
        sInstance.OnTeamWon.Invoke(InTeam);
        sInstance.HandleGameComplete();
    }

    private static void TeamLost(GameTeam InTeam)
    {
        Debug.Log("TeamLost TeamWon");
        GameTeam WinningTeam = InTeam == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue;
        TeamWon(WinningTeam);
    }

    private void HandleGameComplete()
    {
        m_CurrentHoverCell = null;
        CleanupHoverCells();
        m_bIsPlaying = false;
    }

    private void HandleInteraction(ILevelCell InCell, CellInteractionState InInteractionState)
    {
        GridObject ObjOnCell = InCell.GetObjectOnCell();
        if (ObjOnCell)
        {
            ObjOnCell.HandleInteraction(InInteractionState);
        }

        if (InCell.IsBlocked())
        {
            return;
        }

        switch (InInteractionState)
        {
            case CellInteractionState.eBeginHover:
                BeginHover(InCell);
                break;
            case CellInteractionState.eEndHover:
                EndHover(InCell);
                break;
            case CellInteractionState.eLeftClick:
                HandleCellClicked(InCell);
                break;
        }

    }

    #endregion
}