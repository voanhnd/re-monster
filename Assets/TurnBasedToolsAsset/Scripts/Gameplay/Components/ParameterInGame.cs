﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.BattleUI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ParameterInGame : MonoBehaviour
{
    private BasicParameters monsterInfoParameters;

    private BasicParameters basicParameters;

    private BattleParameters battleParameters;

    public int seftTurnCount = 0;

    public bool isDefense = false;

    [SerializeField]
    private InnateTrait.InnateTraitInfo innateTrait = new InnateTrait.InnateTraitInfo();

    [SerializeField]
    private List<AcquiredTrait.AcquiredTraitInfo> acquiredTraitInfos = new List<AcquiredTrait.AcquiredTraitInfo>();

    [SerializeField]
    private AcquiredTrait.AcquiredTraitInfo complexAcquiredTrait = new AcquiredTrait.AcquiredTraitInfo();

    [SerializeField]
    private FarmTrait.FarmTraitInfo farmTrait = new FarmTrait.FarmTraitInfo();

    [SerializeField] private BasicParameters cur_monsterInfoParameters;

    [SerializeField] private BasicParameters cur_basicParameters;

    [SerializeField] private BattleParameters cur_battleParameters;

    [SerializeField] private float skillDelay = 0f;

    [HideInInspector] public UnityEvent OnHealthDepleted = new();

    [HideInInspector] public UnityEvent OnMagicalArmorDepleted = new();

    [HideInInspector] public UnityEvent OnHit = new();

    [HideInInspector] public UnityEvent OnHeal = new();

    public List<InGameBuffDetail> inGameBuffDetails = new();

    public List<InGameDeBuffDetail> inGameDeBuffDetails = new();

    private InGameBuffDetail complexBuff;

    private InGameDeBuffDetail complexDeBuff;

    bool avoidTurn = false;

    [SerializeField]
    List<DelayAbility.DelayAbilityInfo> delayAbilityInfos = new();

    private MonsterInGame monsterInGame;

    BuffSpriteGO buffFX = null;

    private float penaltyDelay = 0;
    public MonsterInGame MonsterInGame => monsterInGame;

    private void Awake()
    {
        monsterInGame = GetComponent<MonsterInGame>();
    }

    public static CompassDir GetHexFaceTo(float yEulerAngle)
    {
        if (yEulerAngle >= 0f && yEulerAngle <= 60f)
        {
            return CompassDir.NE;
        }
        if (yEulerAngle >= 61f && yEulerAngle <= 120f)
        {
            return CompassDir.E;
        }
        if (yEulerAngle >= 121f && yEulerAngle <= 180f)
        {
            return CompassDir.SE;
        }
        if (yEulerAngle >= 181f && yEulerAngle <= 240f)
        {
            return CompassDir.SW;
        }
        if (yEulerAngle >= 241f && yEulerAngle <= 300f)
        {
            return CompassDir.W;
        }
        return CompassDir.NW;
    }

    public static CompassDir GetHexFlipFaceTo(CompassDir compassDir)
    {
        switch (compassDir)
        {
            case CompassDir.NW:
                {
                    return CompassDir.SE;
                }

            case CompassDir.SE:
                {
                    return CompassDir.NW;
                }

            case CompassDir.NE:
                {
                    return CompassDir.SW;
                }

            case CompassDir.SW:
                {
                    return CompassDir.NE;
                }

            case CompassDir.W:
                {
                    return CompassDir.E;
                }

            case CompassDir.E:
                {
                    return CompassDir.W;
                }
        }

        return compassDir;
    }

    public int CheckBuffCount(MonsterSpecialBufferType buffType)
    {
        int count = 0;

        for (int i = 0; i < inGameBuffDetails.Count; i++)
        {
            InGameBuffDetail b = inGameBuffDetails[i];
            if (buffType == b.monsterSpecialBufferType)
                count++;
        }

        return count;
    }

    public int CheckDeBuffCount(MonsterSpecialDeBufferType buffType)
    {
        int count = 0;

        foreach (InGameDeBuffDetail b in inGameDeBuffDetails)
        {
            if (buffType == b.monsterSpecialDeBufferType)
            {
                count++;
            }
        }

        return count;
    }

    public void AddStamina(float stamina)
    {
        float currentST = monsterInGame.Unit.GetCurrentST();

        currentST += stamina;

        if (currentST < 0)
        {
            currentST = 0;
        }

        monsterInGame.Unit.SetAbilityPoints(Mathf.FloorToInt(currentST));
        if (stamina != 0)
        {
            //UIDamgePopupController.OnStaminaChanged?.Invoke(monsterInGame.uiStatsPos.position, stamina);
            UIBattleMonsterStatsContainer.OnStaminaChanged?.Invoke(monsterInGame.GetBattleID());
            UIBattleMonsterStatsContainer.OnApplyDelay?.Invoke(monsterInGame.GetBattleID());
        }
    }

    public float GetCurrentStamina() => monsterInGame.Unit.GetCurrentST();

    public float GetHealth()
    {
        return GetCurrentComplexParameter().health;
    }

    public float GetHealthPercentage()
    {
        return (GetCurrentComplexParameter().health / GetDefaultComplexParameter().health);
    }

    public BasicParameters GetDefaultComplexParameter() => Monster.Get_Complex_Parameter(monsterInfoParameters, basicParameters);

    public BasicParameters GetCurrentComplexParameter() => Monster.Get_Complex_Parameter(cur_monsterInfoParameters, cur_basicParameters);

    public InnateTrait.InnateTraitInfo GetInnateTrait()
    {
        return innateTrait;
    }

    public List<AcquiredTrait.AcquiredTraitInfo> GetAcquiredTraits()
    {
        return acquiredTraitInfos;
    }

    public AcquiredTrait.AcquiredTraitInfo GetAcquiredTraitComplex()
    {
        return complexAcquiredTrait;
    }

    public FarmTrait.FarmTraitInfo GetFarmTrait()
    {
        return farmTrait;
    }

    public void AddCurrentParameters(BasicParameters parameters)
    {
        cur_monsterInfoParameters.health += parameters.health;

        cur_monsterInfoParameters.strenght += parameters.strenght;

        cur_monsterInfoParameters.intelligent += parameters.intelligent;

        cur_monsterInfoParameters.dexterity += parameters.dexterity;

        cur_monsterInfoParameters.agility += parameters.agility;

        cur_monsterInfoParameters.vitality += parameters.vitality;
    }

    public BattleParameters GetCurrentBattleParameters() => cur_battleParameters;

    public InGameBuffDetail GetInGameComplexBuff() => InGameBuffDetail.GetBuffComplex(inGameBuffDetails);

    public InGameDeBuffDetail GetInGameComplexDeBuff() => InGameDeBuffDetail.GetDeBuffComplex(inGameDeBuffDetails);

    public float GetCurrentBattleParametersSpeed() => cur_battleParameters.Speed;

    public void AddSkillDelay(float delay, bool isSelfDelay = true)
    {
        skillDelay += delay;

        if (!isSelfDelay) penaltyDelay = delay;

        if (skillDelay < 0)
        {
            skillDelay = 0f;
            penaltyDelay = 0;
        }

        UIBattleMonsterStatsContainer.OnApplyDelay?.Invoke(monsterInGame.GetBattleID());
        /*if (isSelfDelay)
        {
            UIDamgePopupController.OnSkillDelay?.Invoke(monsterInGame.uiStatsPos.position, -skillDelay);
        }*/
    }

    public float GetSkillDelay() => skillDelay;

    public void SetParameters(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        seftTurnCount = 0;

        //Debug.Log("SetParameters");
        //Debug.Log("Monster " + gameObject.name);
        //Debug.Log("Monster id " + playerMonsterInfo.monsterID);

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        if(playerFarmInfo != null)
        {
            farmTrait = FarmTrait.GetFarmTraitInfo(playerFarmInfo.farmTraitType);
        }
        else
        {
            farmTrait = FarmTrait.GetFarmTraitInfo(FarmTrait.FarmTraitType.none);
        }

        innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);

        acquiredTraitInfos = AcquiredTrait.GetAcquiredTraitList(playerMonsterInfo.acquiredTraits);

        complexAcquiredTrait = AcquiredTrait.GetComplexAcquiredTrait(acquiredTraitInfos);

        MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        BasicParameters basicParametersByTrait = new BasicParameters
            (
             farmTrait.health + complexAcquiredTrait.health,
             farmTrait.strenght + complexAcquiredTrait.strenght,
             farmTrait.intelligent + complexAcquiredTrait.intelligent,
             farmTrait.dexterity + complexAcquiredTrait.dexterity,
             farmTrait.agility + complexAcquiredTrait.agility,
             farmTrait.vitality + complexAcquiredTrait.vitality
            );

        basicParameters = monsterDataScriptObj.Monster.basicParameters.Clone();
        cur_basicParameters = basicParameters.Clone();

        battleParameters = monsterDataScriptObj.Monster.battleParameters.Clone();
        cur_battleParameters = battleParameters.Clone();

        BasicParameters tempParameter = Monster.Get_Complex_Parameter(Monster.Get_Battle_PlayerInfo_Basic_Parameter(playerMonsterInfo, monsterDataScriptObj), basicParametersByTrait);
        monsterInfoParameters = tempParameter.Clone();
        cur_monsterInfoParameters = monsterInfoParameters.Clone();

        monsterInGame.Unit.SetMovementPoints(battleParameters.Move);

        //Stamina
        int startStaminaByTrait = Mathf.RoundToInt(GetAcquiredTraitComplex().battle_Start_Stamina + farmTrait.battle_Start_Stamina);

        monsterInGame.Unit.SetAbilityPoints(Mathf.FloorToInt(MonsterBattle.GetStartMonsterStamina()) + startStaminaByTrait);

        //End stamina

        monsterInGame.Unit.SetSpeed(battleParameters.Speed * 0.1f);

        isDefense = false;
    }

    public void SetTurn()
    {

    }

    void BuffTurn()
    {
        avoidTurn = false;

        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        if (customGameRules && customGameRules.GetCurrentTurn() > 0)
        {
            //InGameBuffDetail complexBuff = InGameBuffDetail.GetBuffComplex(inGameBuffDetails);

            //InGameDeBuffDetail complexDeBuff = InGameDeBuffDetail.GetDeBuffComplex(inGameDeBuffDetails);

            //Caculate buff turn

            List<InGameBuffDetail> removeBuffList = new();

            foreach (InGameBuffDetail buff in inGameBuffDetails)
            {
                buff.buffTurn--;

                if (buff.buffTurn < 0)
                {
                    removeBuffList.Add(buff);
                }
            }

            foreach (InGameBuffDetail buff in removeBuffList)
            {
                inGameBuffDetails.Remove(buff);
            }

            //End Caculate buff turn

            //Caculate debuff turn

            List<InGameDeBuffDetail> removeDeBuffList = new();

            foreach (InGameDeBuffDetail deBuff in inGameDeBuffDetails)
            {
                if(deBuff.avoidTurn > 0)
                {
                    deBuff.avoidTurn--;

                    avoidTurn = true;
                }

                deBuff.deBuffTurn--;

                if (deBuff.deBuffTurn < 0)
                {
                    removeDeBuffList.Add(deBuff);
                }
            }

            foreach (InGameDeBuffDetail deBuff in removeDeBuffList)
            {
                Debug.Log("Remove debuff = " + deBuff.monsterSpecialDeBufferType.ToString());

                if (deBuff.monsterSpecialDeBufferType == MonsterSpecialDeBufferType.睡眠_Sleep)
                {
                    monsterInGame.Unit.PlayAnimation(monsterInGame.Unit.GetUnitData().m_IdleAnimation);
                }

                inGameDeBuffDetails.Remove(deBuff);
            }

            //End Caculate debuff turn

            complexBuff = GetInGameComplexBuff();

            complexDeBuff = GetInGameComplexDeBuff();

            if (complexBuff.disableDeBuff > 0)
            {
                List<InGameDeBuffDetail> temp = new();
                temp.AddRange(inGameDeBuffDetails);

                for (int i = 0; i < complexBuff.disableDeBuff; i++)
                {
                    Debug.Log("remove debuff index = " + (temp.Count - 1));

                    temp.RemoveAt(temp.Count - 1);
                }

                complexDeBuff = InGameDeBuffDetail.GetDeBuffComplex(temp);
            }
        }

        ActiveBuffFX();
    }

    public void SeftTurn()
    {
        BuffTurn();

        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        if (customGameRules && customGameRules.GetCurrentTurn() > 0)
        {
            //reset spd delay from skill delay effect
            AddSkillDelay(-GetSkillDelay());

            UIBattleMonsterStatsContainer.OnTurnSwitched?.Invoke(monsterInGame.GetBattleID());

            BasicParameters default_ComplexParameters = GetDefaultComplexParameter();

            BasicParameters cur_ComplexParameters = GetCurrentComplexParameter();

            seftTurnCount++;

            CalculateStaminaRecover();

            CalculateHealthRecover(default_ComplexParameters);

            CalculateDOT(cur_ComplexParameters);

            CalculateSpeed();

            CalculateMovement();

            ResetDefenseStat(default_ComplexParameters);

            int trueMove = 0;

            for (int i = 0; i < battleParameters.Move; i++)
            {
                float staminaCost = MonsterBattle.Get_Move_Stamina_Cost(i + 1);

                if (staminaCost <= GetCurrentStamina())
                    trueMove = i + 1;
                else
                    break;
            }

            monsterInGame.Unit.SetMovementPoints(trueMove);
        }

        InGameBuffDetail inGameBuffDetail = GetInGameComplexBuff();
        InGameDeBuffDetail inGameDeBuffDetail = GetInGameComplexDeBuff();

        //Check avoid turn by debuff
        if (avoidTurn == true)
        {
            Debug.Log("debuff next turn");

            customGameRules.CustomHandlePlayerSelected(monsterInGame.Unit);

            CustomGameManager.Instance.SetNextTurn();
        }
        else if (monsterInGame.Unit.IsAI())
        {
            Debug.Log("caculate AI unit turn");

            AIManager.RunOneAI(monsterInGame.Unit, CustomGameManager.Instance.SetNextTurn);
        }

        UIBattleMonsterStatsContainer.OnStaminaChanged?.Invoke(monsterInGame.GetBattleID());
        UIBattleMonsterStatsContainer.OnApplyDelay?.Invoke(monsterInGame.GetBattleID());

        ExecuteDelayAbility();
    }

    void ExecuteDelayAbility()
    {
        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        int currentTurn = customGameRules.GetCurrentTurn();

        List< DelayAbility.DelayAbilityInfo > removeInfos = new List<DelayAbility.DelayAbilityInfo>();

        for (int k = 0; k < delayAbilityInfos.Count; k++)
        {
            DelayAbility.DelayAbilityInfo nowInfo = delayAbilityInfos[k];

            int tempTurn = currentTurn - nowInfo.fromTurn + nowInfo.delayAbility.delayTurn;

            if(tempTurn >= 0)
            {
                //Debug.Log("delay ability executed");

                //if (nowInfo.unitAbility == null)
                //{
                //    Debug.Log("delay ability executed nowInfo.unitAbility == null");
                //}

                //if (monsterInGame.Unit == null)
                //{
                //    Debug.Log("delay ability executed monsterInGame.Unit == null");
                //}

                //if (nowInfo.targetUnit == null)
                //{
                //    Debug.Log("delay ability executed nowInfo.targetUnit == null");
                //}

                //if (nowInfo.selectedCell == null)
                //{
                //    Debug.Log("delay ability executed nowInfo.selectedCell == null");
                //}

                StartCoroutine(nowInfo.delayAbility.Execute(nowInfo.unitAbility, monsterInGame.Unit, nowInfo.targetUnit, nowInfo.selectedCell));

                delayAbilityInfos.Remove(nowInfo);
                k--;
            }
        }
    }

    public void AddDelayAbility(UnitAbility unitAbility, DelayAbility delayAbility, GridUnit TargetUnit, ILevelCell SelectedCell)
    {
        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        DelayAbility.DelayAbilityInfo info = new DelayAbility.DelayAbilityInfo();

        int currentTurn = customGameRules.GetCurrentTurn();

        info.fromTurn = currentTurn;

        info.targetUnit = TargetUnit;

        info.selectedCell = SelectedCell;

        info.unitAbility = unitAbility;

        info.delayAbility = delayAbility;

        delayAbilityInfos.Add(info);

        //Sort
        for (int j = 0; j <= delayAbilityInfos.Count - 2; j++)
        {
            for (int k = 0; k <= delayAbilityInfos.Count - 2; k++)
            {
                DelayAbility.DelayAbilityInfo nowInfo = delayAbilityInfos[k];
                DelayAbility.DelayAbilityInfo nextInfo = delayAbilityInfos[k + 1];

                int curInfoTurnLeft = currentTurn - nowInfo.fromTurn + nowInfo.delayAbility.delayTurn;
                int nextInfoTurnLeft = currentTurn - nextInfo.fromTurn + nextInfo.delayAbility.delayTurn;

                if (curInfoTurnLeft >= nextInfoTurnLeft) continue;

                DelayAbility.DelayAbilityInfo temp = nextInfo;
                nextInfo = nowInfo;
                nowInfo = temp;
            }
        }
    }

    public void RemoveDelayAbility(DelayAbility delayAbility)
    {
        foreach(DelayAbility.DelayAbilityInfo info in delayAbilityInfos)
        {
            if(info.delayAbility == delayAbility)
            {
                delayAbilityInfos.Remove(info);

                break;
            }
        }
    }

    private void ResetDefenseStat(BasicParameters default_ComplexParameters)
    {
        if (isDefense)
        {
            isDefense = false;

            float defAGI = (monsterInfoParameters.agility + default_ComplexParameters.agility) * 0.2f;

            float defVIT = (monsterInfoParameters.vitality + default_ComplexParameters.vitality) * 0.2f;

            BasicParameters temp = new()
            {
                agility = -defAGI,

                vitality = -defVIT
            };

            AddCurrentParameters(temp);
        }
    }

    private void CalculateMovement()
    {
        cur_battleParameters.Move = battleParameters.Move + complexDeBuff.move_Decrease;

        if (cur_battleParameters.Move < 1)
        {
            cur_battleParameters.Move = 1;
        }
    }

    private void CalculateSpeed()
    {
        float speedStateCorrection = complexBuff.speed_Up_Correction + complexDeBuff.speed_Decrease_Correction;

        float speedCorrectionByTrait = innateTrait.SPD_Correction;

        float speedByTrait = GetAcquiredTraitComplex().speed + farmTrait.speed;

        cur_battleParameters.Speed = battleParameters.Speed + (battleParameters.Speed * (speedStateCorrection + speedCorrectionByTrait)) + speedByTrait;
    }

    /// <summary>
    /// For calculating damage overtime, usually when new turn start
    /// </summary>
    /// <param name="cur_ComplexParameters"></param>
    private void CalculateDOT(BasicParameters cur_ComplexParameters)
    {
        float hp_Dam = cur_ComplexParameters.health * complexDeBuff.Health_Turn_Dam_Correction;

        Debug.Log("New turn Damage = " + hp_Dam.ToString());

        if (hp_Dam > 0)
        {
            Damage(hp_Dam);
        }
    }

    private void CalculateHealthRecover(BasicParameters default_ComplexParameters)
    {
        float hp_Recovery_StateCorrection = complexBuff.health_Heal_Correction + complexDeBuff.Health_Recovery_Correction;

        float hp_Recovery_TraitCorrection = innateTrait.HP_Recovery_Correction;

        float hp_Recovery = default_ComplexParameters.health * (hp_Recovery_StateCorrection + hp_Recovery_TraitCorrection);

        Debug.Log("New turn health Recovery = " + hp_Recovery.ToString());

        Heal_Health(hp_Recovery);
    }

    private void CalculateStaminaRecover()
    {
        float st_CaculateCorrection = complexBuff.stamina_Recovery_Correction + complexDeBuff.Stamina_Recovery_Correction;

        Debug.Log("Stamina recovery st_CaculateCorrection = " + st_CaculateCorrection.ToString());

        float recoveryStamina = 0f;

        recoveryStamina += recoveryStamina * complexDeBuff.Stamina_Turn_Dam_Correction;

        Debug.Log("Stamina recoveryStamina = " + recoveryStamina.ToString());

        if (seftTurnCount <= 1)
        {
            Debug.Log("Frist turn recoveryStamina is disabled = ");
        }
        else
        {
            InGameBuffDetail inGameBuffDetail = GetInGameComplexBuff();
            InGameDeBuffDetail inGameDeBuffDetail = GetInGameComplexDeBuff();

            float traitCorrection = innateTrait.ST_Recovery_Correction;

            float buffTraitCorrection = 0f;
            float debuffTraitCorrection = 0f;

            recoveryStamina = MonsterBattle.Get_Stamina_Recovery_Turn
                (
                cur_battleParameters.StaminaRecovery, traitCorrection,
                inGameBuffDetail.stamina_Recovery_Correction, inGameDeBuffDetail.Stamina_Recovery_Correction,
                buffTraitCorrection, debuffTraitCorrection
                );

            UIDamgePopupController.OnStaminaChanged?.Invoke(monsterInGame.uiStatsPos.position, recoveryStamina);
        }

        int stamina = monsterInGame.Unit.GetCurrentST() + Mathf.RoundToInt(recoveryStamina);

        Vector2 minMax = MonsterBattle.Get_Stamina_Min_Max();
        stamina = Mathf.Clamp(stamina, (int)minMax.x, (int)minMax.y);

        Debug.Log("Stamina recovery caculated = " + stamina.ToString());
        monsterInGame.Unit.SetAbilityPoints(stamina);
    }

    void ActiveBuffFX()
    {
        if(buffFX == null)
        {
           buffFX = monsterInGame.SpawnBuffFX(this);
        }

        buffFX.ActiveBuffSprite(this);
    }

    public void AddNewBuffs(List<InGameBuffDetail> buffs)
    {
        List<InGameBuffDetail> tempList = new();
        tempList.AddRange(inGameBuffDetails);
        tempList.AddRange(buffs);

        List<InGameBuffDetail> physicBuffs = new();
        List<InGameBuffDetail> psychicBuffs = new();

        int maxBuff = 4;
        int buffLeft = maxBuff;

        int maxPhysicCount = 3;
        int maxPsychicCount = 1;

        for (int i = tempList.Count - 1; i >= 0; i--)
        {
            if (buffLeft > 0 && psychicBuffs.Count < maxPsychicCount && InGameBuffDetail.IsPsychic(tempList[i].monsterSpecialBufferType))
            {
                int sameBuffCount = 0;

                foreach (InGameBuffDetail b in psychicBuffs)
                {
                    if (tempList[i].monsterSpecialBufferType == b.monsterSpecialBufferType)
                    {
                        sameBuffCount++;
                        break;
                    }
                }

                if (sameBuffCount == 0)
                {
                    psychicBuffs.Insert(0, tempList[i]);
                    buffLeft--;
                }
            }
            else if (buffLeft > 0 && physicBuffs.Count < maxPhysicCount && !InGameBuffDetail.IsPsychic(tempList[i].monsterSpecialBufferType))
            {
                int sameBuffCount = 0;

                foreach (InGameBuffDetail b in physicBuffs)
                {
                    if (tempList[i].monsterSpecialBufferType == b.monsterSpecialBufferType)
                    {
                        sameBuffCount++;
                        break;
                    }
                }

                if (sameBuffCount == 0)
                {
                    physicBuffs.Insert(0, tempList[i]);
                    buffLeft--;
                }
            }
        }

        if(psychicBuffs.Count > 0)
        {
            foreach(InGameDeBuffDetail debuff in inGameDeBuffDetails)
            {
                if (InGameDeBuffDetail.IsPsychic(debuff.monsterSpecialDeBufferType))
                {
                    inGameDeBuffDetails.Remove(debuff);
                }
            }
        }

        List<InGameBuffDetail> newList = new();
        newList.AddRange(physicBuffs);
        newList.AddRange(psychicBuffs);

        inGameBuffDetails = newList;

        ActiveBuffFX();
    }

    public void AddNewDeBuffs(List<InGameDeBuffDetail> deBuffs)
    {
        List<InGameDeBuffDetail> tempList = new();
        tempList.AddRange(inGameDeBuffDetails);
        tempList.AddRange(deBuffs);

        List<InGameDeBuffDetail> physicDeBuffs = new();
        List<InGameDeBuffDetail> psychicDeBuffs = new();

        int maxBuff = 4;
        int buffLeft = maxBuff;

        int maxPhysicCount = 3;
        int maxPsychicCount = 1;

        for (int i = tempList.Count - 1; i >= 0; i--)
        {
            if (buffLeft > 0 && psychicDeBuffs.Count < maxPsychicCount && InGameDeBuffDetail.IsPsychic(tempList[i].monsterSpecialDeBufferType))
            {
                int sameBuffCount = 0;

                foreach (InGameDeBuffDetail b in psychicDeBuffs)
                {
                    if (tempList[i].monsterSpecialDeBufferType == b.monsterSpecialDeBufferType)
                    {
                        sameBuffCount++;
                        break;
                    }
                }

                if (sameBuffCount == 0)
                {
                    psychicDeBuffs.Insert(0, tempList[i]);
                    buffLeft--;
                }
                continue;
            }
            if (buffLeft > 0 && physicDeBuffs.Count < maxPhysicCount && !InGameDeBuffDetail.IsPsychic(tempList[i].monsterSpecialDeBufferType))
            {
                int sameBuffCount = 0;

                foreach (InGameDeBuffDetail physicDeBuff in physicDeBuffs)
                {
                    if (tempList[i].monsterSpecialDeBufferType == physicDeBuff.monsterSpecialDeBufferType)
                    {
                        sameBuffCount++;
                        break;
                    }
                }

                if (sameBuffCount == 0)
                {
                    physicDeBuffs.Insert(0, tempList[i]);

                    buffLeft--;
                }
            }
        }

        if (psychicDeBuffs.Count > 0)
        {
            foreach (InGameBuffDetail buff in inGameBuffDetails)
            {
                if (InGameBuffDetail.IsPsychic(buff.monsterSpecialBufferType))
                {
                    inGameBuffDetails.Remove(buff);
                }
            }
        }

        List<InGameDeBuffDetail> newList = new();
        newList.AddRange(physicDeBuffs);
        newList.AddRange(psychicDeBuffs);

        inGameDeBuffDetails = newList;

        ActiveBuffFX();
    }

    public void DamgeOnBlockedCell()
    {
        float maxHealth = monsterInfoParameters.health + basicParameters.health;

        float dam = maxHealth * 0.3f;

        Debug.Log("On blocked cell get dam = " + dam);

        Damage(dam);
    }

    public void MissHit()
    {
        Debug.Log("Miss hit");

        //unit.PlayAnimation(unit.GetUnitData().m_DamagedAnimation, true);
        UIDamgePopupController.OnMissed?.Invoke(monsterInGame.uiStatsPos.position, monsterInGame.Unit.GetTeam() == CustomGameManager.Instance.GetPlayerTeam());
    }

    //Health
    public void Damage(float InDamage, bool isCrit = false)
    {
        InDamage = InDamage + (InDamage * innateTrait.Taken_Damage_Correction);

        if (InDamage < 0)
        {
            InDamage = 0;

            Debug.Log("Damage lower than 0");
        }

        BattleStatsCollector.Instance.AddDamageReceived(monsterInGame.Unit, InDamage, monsterInGame.Unit.GetTeam());

        Debug.Log("InDamage = " + InDamage);

        float HealthBefore = GetCurrentComplexParameter().health;

        if (cur_monsterInfoParameters.health > 0f)
        {
            cur_monsterInfoParameters.health -= InDamage;

            if (cur_monsterInfoParameters.health < 0f)
            {
                cur_basicParameters.health += cur_monsterInfoParameters.health;

                cur_monsterInfoParameters.health = 0f;
            }
        }
        else
        {
            cur_basicParameters.health -= InDamage;

            if (cur_basicParameters.health < 0f)
            {
                cur_basicParameters.health = 0f;
            }
        }

        float m_Health = GetCurrentComplexParameter().health;

        if (m_Health <= 0)
        {
            CustomGameManager.Instance.SetDeadMonster(this);

            OnHealthDepleted.Invoke();
        }
        else
        {
            //GridUnit gridUnit = GetComponent<GridUnit>();

            //gridUnit.PlayAnimation(gridUnit.GetUnitData().m_DamagedAnimation, true);

            //Remove sleep debuff

            foreach(InGameDeBuffDetail buff in inGameDeBuffDetails)
            {
                if(buff.monsterSpecialDeBufferType == MonsterSpecialDeBufferType.睡眠_Sleep)
                {
                    inGameDeBuffDetails.Remove(buff);

                    break;
                }
            }

            OnHit.Invoke();

            monsterInGame.Unit.PlayAnimation(monsterInGame.Unit.GetUnitData().m_DamagedAnimation, true);
        }
        UIBattleMonsterStatsContainer.OnHealthChanged?.Invoke(monsterInGame.GetBattleID());
        UIDamgePopupController.OnReciveDamage?.Invoke(monsterInGame.uiStatsPos.position, InDamage, penaltyDelay, monsterInGame.Unit.GetTeam() == CustomGameManager.Instance.GetPlayerTeam(), isCrit);
    }

    public void Heal_Health(float InHeal)
    {
        if (InHeal <= 0f)
        {
            Debug.Log("In Heal lower than 0 = " + InHeal);

            return;
        }

        Debug.Log("In Heal = " + InHeal);

        cur_monsterInfoParameters.health += InHeal;

        float m_Health = GetCurrentComplexParameter().health;

        float max_Health = GetDefaultComplexParameter().health;

        if (m_Health > max_Health)
        {
            float temp = m_Health - max_Health;

            cur_monsterInfoParameters.health -= temp;
        }

        Debug.Log("True Heal = " + (cur_monsterInfoParameters.health + cur_basicParameters.health));

        OnHeal.Invoke();
        UIDamgePopupController.OnHeal?.Invoke(monsterInGame.uiStatsPos.position, InHeal);
    }

    //End health
}
