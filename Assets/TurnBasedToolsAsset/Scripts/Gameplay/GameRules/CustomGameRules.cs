﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CustomGameRules", menuName = "TurnBasedTools/GameRules/Create Custom GameRules", order = 1)]
public class CustomGameRules : GameRules
{
    [SerializeField]
    GameObject m_CurrentHoverObject;

    [SerializeField]
    GridUnit SelectedUnit = null;

    [SerializeField]
    int turnCount = 0;

    protected override void Init()
    {
        base.Init();
    }

    public void CustomInit()
    {
        turnCount = 0;

        CustomGridManager.Instance.UpdateDeadBound(turnCount);
    }

    public override void CustomEndTurn(GameTeam nextTeam)
    {
        Debug.Log("Custom end turn");

        CustomGridManager.Instance.UpdateDeadBound(turnCount);
    }

    public void CustomCountTurn()
    {
        Debug.Log("Custom count turn");

        turnCount++;
    }

    public int GetCurrentTurn()
    {
        return turnCount;
    }

    public void PlaceUnits(GameTeam gameTeam, List<CustomGameManager.SpawnListInfo> spawnListInfos, UnitAI unitAI)
    {
        Debug.Log("Place Units team = " + gameTeam.ToString());

        for (int i = 0; i < spawnListInfos.Count; i++)
        {
            CustomGameManager.SpawnListInfo spawnInfo = spawnListInfos[i];

            MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(spawnInfo.playerMonsterInfo.monsterID);

            UnitData unitData = monsterDataScriptObj.unitData;

            if (unitData)
            {
                CompassDir compassDir = CompassDir.S;

                if (gameTeam == GameTeam.Blue)
                {
                    compassDir = CompassDir.E;
                }
                else if (gameTeam == GameTeam.Red)
                {
                    compassDir = CompassDir.W;
                }

                GridUnit SpawnedUnit = GameManager.SpawnUnit(unitData, spawnInfo.spawnedModel, gameTeam, spawnInfo.levelCell, compassDir);
                SpawnedUnit.SetAsTarget(false);
                SpawnedUnit.AddAI(unitAI);
                SpawnedUnit.SetAIActive(false);

                SpawnedUnit.GetComponent<MonsterInGame>().SetMonsterInfo(spawnInfo.playerMonsterInfo);
                SpawnedUnit.GetComponent<ParameterInGame>().SetParameters(spawnInfo.playerMonsterInfo);
            }
        }
    }

    void CleanUpSelectedUnit()
    {
        if (SelectedUnit)
        {
            Debug.Log("CleanUpSelectedUnit");

            SelectedUnit.UnBindFromOnMovementComplete(UpdateSelectedHoverObject);
            UnselectUnit();
        }
    }

    void SetupTeam(GameTeam InTeam)
    {
        List<GridUnit> Units = GameManager.GetUnitsOnTeam(InTeam);

        foreach (GridUnit unit in Units)
        {
            if (unit != null)
            {
                unit.HandleTurnStarted();

                ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

                if (parameterInGame)
                {
                    parameterInGame.SetTurn();
                }
            }
        }
    }

    private bool IsHoverObjectSpawned() => m_CurrentHoverObject != null;

    private void UpdateSelectedHoverObject()
    {
        if (m_CurrentHoverObject)
        {
            Debug.Log("Destroy m_CurrentHoverObject");

            Destroy(m_CurrentHoverObject);
        }

        if (!SelectedUnit || SelectedUnit.IsDead()) return;

        GameObject hoverObj = GameManager.GetSelectedHoverPrefab();
        if (hoverObj)
            m_CurrentHoverObject = Instantiate(hoverObj, SelectedUnit.GetCell().GetAllignPos(SelectedUnit), hoverObj.transform.rotation);
    }

    public override void Update()
    {
        base.Update();

        if (!SelectedUnit && IsHoverObjectSpawned())
        {
            UpdateSelectedHoverObject();
        }

        if (GameManager.IsActionBeingPerformed())
        {
            return;
        }

        //if (Input.GetMouseButtonDown(1) && GameManager.IsPlaying())
        //{
        //    if (SelectedUnit)
        //    {
        //        UnitState currentState = SelectedUnit.GetCurrentState();
        //        if (currentState == UnitState.UsingAbility)
        //        {
        //            Debug.Log("Update UsingAbility SetupMovement");

        //            SelectedUnit.SetupMovement();
        //        }
        //        else if (currentState == UnitState.Moving)
        //        {
        //            UnselectUnit();
        //        }
        //    }
        //}
    }

    public void UnselectUnit()
    {
        if (SelectedUnit)
        {
            SelectedUnit.CleanUp();
            SelectedUnit = null;

            GameManager.Get().OnUnitSelected.Invoke(SelectedUnit);

            UpdateSelectedHoverObject();
        }

        GameManager.Get().UpdateHoverCells();
    }

    public override GridUnit GetSelectedUnit()
    {
        return SelectedUnit;
    }

    public override void HandleNumPressed(int InNumPressed)
    {
        if (SelectedUnit)
        {
            SelectedUnit.SetupAbility(InNumPressed - 1);
        }
    }

    public void CustomUnitTurn(GridUnit gridUnit)
    {
        GameTeam gameTeam = gridUnit.GetTeam();

        Debug.Log("CustomUnitTurn team turn = " + gameTeam.ToString());

        CleanUpSelectedUnit();
        SetupTeam(gameTeam);

        AilmentHandler.HandleTurnStart(gameTeam);

        ParameterInGame parameterInGame = gridUnit.GetComponent<ParameterInGame>();
        parameterInGame.SeftTurn();
    }

    public override void EndTeamTurn(GameTeam InTeam)
    {
        Debug.Log("End Team Turn");

        AilmentHandler.HandleTurnEnd(InTeam);
    }

    public override void BeginHover(ILevelCell InCell)
    {
        if (SelectedUnit != null)
        {
            UnitAbility unitAbility = SelectedUnit.GetCurrentAbility();

            if (unitAbility != null)
            {
                List<ILevelCell> abilityCellsInRange = unitAbility.GetAbilityCells(SelectedUnit);

                if (abilityCellsInRange.Contains(InCell))
                {
                    UIAccuracyRate.Instance.SetTargetsAccury(SelectedUnit.GetCurrentAbility(), SelectedUnit, GameManager.Get().GetTargetsHover(), InCell);

                    return;
                }
            }
        }

        UIAccuracyRate.Instance.SetTargetsAccury(null, null, null, null);
    }

    public override void EndHover(ILevelCell InCell)
    {

    }

    public override void HandleEnemySelected(GridUnit InEnemyUnit)
    {
        GameTeam currTeam = GetCurrentTeam();
        if (currTeam != InEnemyUnit.GetTeam() && SelectedUnit && SelectedUnit.GetCurrentState() == UnitState.Moving)
        {
            ILevelCell targetMovementCell = GameManager.CalculateMoveToCell(SelectedUnit, InEnemyUnit.GetCell());

            SelectedUnit.ExecuteMovement(targetMovementCell);
        }
    }

    public void CustomHandlePlayerSelected(GridUnit InPlayerUnit)
    {
        SelectedUnit = InPlayerUnit;

        if (SelectedUnit)
        {
            SelectedUnit.SelectUnit();

            SelectedUnit.BindToOnMovementComplete(UpdateSelectedHoverObject);

            GameManager.Get().OnUnitSelected.Invoke(SelectedUnit);
        }
        else
        {
            CustomGameManager.Instance.SetNextTurn();
        }

        UpdateSelectedHoverObject();
    }

    public override void HandleCellSelected(ILevelCell selectedCell)
    {
        if (GameManager.IsActionBeingPerformed() || selectedCell == null) { return; }

        if (CustomGameManager.Instance.GetIsPlayingBool() == false) 
        {
            CustomGameManager.Instance.monsterSpawnListController.EditSpawnedMonster(selectedCell);
        }

        if (!SelectedUnit) { return; }

        if(selectedCell.GetUnitOnCell() == SelectedUnit)
        {
            return;
        }

        UnitState currentState = SelectedUnit.GetCurrentState();

        if (currentState == UnitState.Moving && SelectedUnit.GetCell() != selectedCell)
        {
            ILevelCell targetMovementCell = GameManager.CalculateMoveToCell(SelectedUnit, selectedCell);

            SelectedUnit.ExecuteMovement(targetMovementCell);
            return;
        }

        if (currentState == UnitState.UsingAbility)
        {
            UnitAbility unitAbility = SelectedUnit.GetCurrentAbility();

            List<ILevelCell> abilityCellsInRange = unitAbility.GetSetupCells(SelectedUnit);

            List<ILevelCell> enemyProvokeCells = new();

            List<ILevelCell> hoverCells = GameManager.Get().GetTargetsHover();

            GetEnemyProvokeCells(ref enemyProvokeCells, abilityCellsInRange);

            Debug.Log("Provke cell in ability range count = " + enemyProvokeCells.Count);

            if (!abilityCellsInRange.Contains(selectedCell))
            {
                Debug.Log("HandleCellSelected ExecuteAbility Target not in range");
                return;
            }
            int targetCount = 0;

            for (int i = 0; i < hoverCells.Count; i++)
            {
                ILevelCell cell = hoverCells[i];
                GridUnit unit = cell.GetUnitOnCell();

                if (unit != null && AbilityParam.IsEffectTeam(SelectedUnit, unit, unitAbility))
                {
                    if (enemyProvokeCells.Count > 0 && unit.GameMonster.InGameParameter.CheckBuffCount(MonsterSpecialBufferType.挑発_Provocation) > 0)
                        targetCount++;
                    else if (enemyProvokeCells.Count == 0)
                        targetCount++;
                }
            }

            Debug.Log("targetCount = " + targetCount);

            if (targetCount > 0)
                SelectedUnit.ExecuteAbility(selectedCell);
        }

        GameManager.Get().CustomEndHover();
    }

    private void GetEnemyProvokeCells(ref List<ILevelCell> enemyProvokeCells,List<ILevelCell> hoverCellList)
    {
        for (int i = 0; i < hoverCellList.Count; i++)
        {
            ILevelCell cell = hoverCellList[i];
            GridUnit unit = cell.GetUnitOnCell();

            if (unit != null && unit.GetTeam() != SelectedUnit.GetTeam() && unit.GameMonster.InGameParameter.CheckBuffCount(MonsterSpecialBufferType.挑発_Provocation) > 0)
                enemyProvokeCells.Add(cell);
        }
    }

    public override void OnMovePosition(GridUnit gridUnit, int cellDistance)
    {
        Debug.Log("Move cell distance = " + cellDistance);

        ParameterInGame parameterInGame = gridUnit.GetComponent<ParameterInGame>();

        float staminaCost = MonsterBattle.Get_Move_Stamina_Cost(cellDistance);

        Debug.Log("move stamina cost = " + staminaCost);

        parameterInGame.AddStamina(-staminaCost);
    }

    public override void HandleTeamWon(GameTeam InTeam)
    {
        UnselectUnit();
    }

    public override void HandleSTChanged(int staminaPoint)
    {
        if (SelectedUnit != null)
            CustomGameUIManager.instance.abilityListUIElement.SetupAbilityList();
    }
}
