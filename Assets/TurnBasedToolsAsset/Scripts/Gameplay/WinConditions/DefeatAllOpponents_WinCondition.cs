﻿using UnityEngine;

[CreateAssetMenu(fileName = "DefeatAllOpponents", menuName = "TurnBasedTools/WinCondition/Create DefeatAllOpponents", order = 1)]
public class DefeatAllOpponents_WinCondition : WinCondition
{
    protected override bool DidTeamWin(GameTeam InTeam)
    {
        if (InTeam == GameTeam.Blue)
        {
            return GameManager.AreAllUnitsOnTeamDead(GameTeam.Red);
        }
        else if (InTeam == GameTeam.Red)
        {
            return GameManager.AreAllUnitsOnTeamDead(GameTeam.Blue);
        }

        return false;
    }

    public override string GetConditionStateString()
    {
        GameTeam TargetTeam = GameTeam.Blue;

        GameTeam CurrentTeam = GameManager.GetRules().GetCurrentTeam();

        if (GameManager.IsTeamHuman(CurrentTeam))
        {
            TargetTeam = (CurrentTeam == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue);
        }

        int numTargets = GameManager.GetUnitsOnTeam(TargetTeam).Count;
        int numTargetsKilled = GameManager.NumUnitsKilled(TargetTeam);

        return "(" + numTargetsKilled + "/" + numTargets + ")";
    }
}
