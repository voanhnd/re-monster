﻿using UnityEngine;

[CreateAssetMenu(fileName = "DefeatNumberOfOpponents", menuName = "TurnBasedTools/WinCondition/Create DefeatNumberOfOpponents", order = 1)]
public class DefeatNumberOfOpponents_WinCondition : WinCondition
{
    public int m_NumberRequired;

    protected override bool DidTeamWin(GameTeam InTeam)
    {
        if (InTeam == GameTeam.Blue)
        {
            return GameManager.NumUnitsKilled(GameTeam.Red) == m_NumberRequired;
        }
        else if (InTeam == GameTeam.Red)
        {
            return GameManager.NumUnitsKilled(GameTeam.Blue) == m_NumberRequired;
        }

        return false;
    }

    public override string GetConditionStateString()
    {
        GameTeam TargetTeam = GameTeam.Blue;

        GameTeam CurrentTeam = GameManager.GetRules().GetCurrentTeam();

        if (GameManager.IsTeamHuman(CurrentTeam))
        {
            TargetTeam = (CurrentTeam == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue);
        }

        int numTargets = GameManager.GetUnitsOnTeam(TargetTeam).Count;
        int numTargetsKilled = GameManager.NumUnitsKilled(TargetTeam);

        return "(" + numTargetsKilled + "/" + m_NumberRequired + ")";
    }
}
