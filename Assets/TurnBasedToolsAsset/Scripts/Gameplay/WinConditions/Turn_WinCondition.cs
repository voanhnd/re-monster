using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Turn_WinCondition", menuName = "TurnBasedTools/WinCondition/Create Turn_WinCondition", order = 1)]
public class Turn_WinCondition : WinCondition
{
    protected override bool DidTeamWin(GameTeam InTeam)
    {
        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        if (customGameRules && customGameRules.GetCurrentTurn() >= 20)
        {
            if (InTeam == GameTeam.Blue)
            {
                List<GridUnit> friendlyUnits = GameManager.GetUnitsOnTeam(GameTeam.Blue);

                float friendlyHealth = 0f;
                foreach (GridUnit unit in friendlyUnits)
                {
                    ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

                    friendlyHealth += parameterInGame.GetCurrentComplexParameter().health;
                }

                List<GridUnit> hostilelyUnits = GameManager.GetUnitsOnTeam(GameTeam.Red);

                float hostileHealth = 0f;
                foreach (GridUnit unit in hostilelyUnits)
                {
                    ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

                    hostileHealth += parameterInGame.GetCurrentComplexParameter().health;
                }

                if (friendlyHealth > hostileHealth)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (InTeam == GameTeam.Red)
            {
                List<GridUnit> friendlyUnits = GameManager.GetUnitsOnTeam(GameTeam.Blue);

                float friendlyHealth = 0f;
                foreach (GridUnit unit in friendlyUnits)
                {
                    ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

                    friendlyHealth += parameterInGame.GetCurrentComplexParameter().health;
                }

                List<GridUnit> hostilelyUnits = GameManager.GetUnitsOnTeam(GameTeam.Red);

                float hostileHealth = 0f;
                foreach (GridUnit unit in hostilelyUnits)
                {
                    ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

                    hostileHealth += parameterInGame.GetCurrentComplexParameter().health;
                }

                if (friendlyHealth < hostileHealth)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        if (InTeam == GameTeam.Blue)
        {
            return GameManager.AreAllUnitsOnTeamDead(GameTeam.Red);
        }
        else if (InTeam == GameTeam.Red)
        {
            return GameManager.AreAllUnitsOnTeamDead(GameTeam.Blue);
        }

        return false;
    }

    public override string GetConditionStateString()
    {
        GameTeam TargetTeam = GameTeam.Blue;

        GameTeam CurrentTeam = GameManager.GetRules().GetCurrentTeam();

        if (GameManager.IsTeamHuman(CurrentTeam))
        {
            TargetTeam = (CurrentTeam == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue);
        }

        int numTargets = GameManager.GetUnitsOnTeam(TargetTeam).Count;
        int numTargetsKilled = GameManager.NumUnitsKilled(TargetTeam);

        return "(" + numTargetsKilled + "/" + numTargets + ")";
    }
}
