﻿using UnityEngine;

[CreateAssetMenu(fileName = "DefeatAllTargets", menuName = "TurnBasedTools/WinCondition/Create DefeatAllTargets", order = 1)]
public class DefeatAllTargets_WinCondition : WinCondition
{
    protected override bool DidTeamWin(GameTeam InTeam)
    {
        if (InTeam == GameTeam.Blue)
        {
            return GameManager.KilledAllTargets(GameTeam.Red);
        }
        else if (InTeam == GameTeam.Red)
        {
            return GameManager.KilledAllTargets(GameTeam.Blue);
        }

        return false;
    }

    public override string GetConditionStateString()
    {
        GameTeam TargetTeam = GameTeam.Blue;

        GameTeam CurrentTeam = GameManager.GetRules().GetCurrentTeam();

        if (GameManager.IsTeamHuman(CurrentTeam))
        {
            TargetTeam = (CurrentTeam == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue);
        }

        int numTargets = GameManager.GetNumOfTargets(TargetTeam);
        int numTargetsKilled = GameManager.GetNumTargetsKilled(TargetTeam);

        return "(" + numTargetsKilled + "/" + numTargets + ")";
    }
}
