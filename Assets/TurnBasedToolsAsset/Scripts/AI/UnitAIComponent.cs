﻿using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using UnityEngine;

public class UnitAIComponent : MonoBehaviour
{
    [SerializeField] private UnitAI m_AIData;

    [SerializeField] private List<int> skillIndexList = new();

    [SerializeField] private List<UnitAbilityPlayerData> abilityList = new();

    private MonsterInGame inGameMonster;

    private void Start()
    {
        inGameMonster = GetComponent<MonsterInGame>();

        SetSkillsIndex(inGameMonster.MonsterInfo.SelectedBattleSkillsId);
        if (!inGameMonster.Unit.IsAI())
        {
            SetSkillToMonsterPlayer();
        }
        else
        {
            SetSkillToMonsterBot();
        }
    }

    public UnitAI GetAIData() => m_AIData;

    public void SetAIData(UnitAI InAIData) => m_AIData = InAIData;

    public void SetSkillToMonsterBot()
    {
        List<int> skillIndexList = new();
        List<UnitAbilityPlayerData> abilityList = inGameMonster.Unit.GetAbilities();

        for (int i = 0; i < abilityList.Count; i++)
        {
            skillIndexList.Add(i);
        }

        ////Test 
        //skillIndexList.Clear();
        //for (int i = 0; i < abilityList.Count; i++)
        //{
        //    if(abilityList[i].unitAbility.GetSkillID() == "SignWitch_Aries_Skill_4")
        //    {
        //        skillIndexList.Add(i);
        //    }
        //}
        ////End test

        int skillNumToGet = 3;

        for (int i = 0; i < skillNumToGet; i++)
        {
            if(skillIndexList.Count > 0)
            {
                int ranIndex = Random.Range(0, skillIndexList.Count);

                this.skillIndexList.Add(skillIndexList[ranIndex]);

                this.abilityList.Add(abilityList[skillIndexList[ranIndex]]);

                skillIndexList.Remove(skillIndexList[ranIndex]);
            }
            else
            {
                break;
            }
        }
    }

    private void SetSkillToMonsterPlayer()
    {
        List<UnitAbilityPlayerData> abilityList = inGameMonster.Unit.GetAbilities();

        for (int i = 0; i < skillIndexList.Count; i++)
        {
            int skillIndex = skillIndexList[i];
            this.abilityList.Add(abilityList[skillIndex]);
        }
    }

    public void SetSkillsIndex(List<string> selectedSkillList)
    {
        skillIndexList.Clear();

        List<UnitAbilityPlayerData> abilityList = inGameMonster.Unit.GetAbilities();

        for (int i = 0; i < abilityList.Count; i++)
        {
            int skillIndex = i;
            if (selectedSkillList.Contains(abilityList[skillIndex].unitAbility.GetSkillID()))
            {
                skillIndexList.Add(skillIndex);
            }
        }
    }

    public List<UnitAbilityPlayerData> GetAbilities() => abilityList;
}
