﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewUnitAI", menuName = "TurnBasedTools/AI/Create UnitAIData", order = 1)]
public class UnitAI : ScriptableObject
{
    public bool m_bActivateOnStart;
    public int m_ActivationRange = 5;

    public virtual IEnumerator RunOnUnit(GridUnit InAIUnit)
    {
        yield return new WaitForSeconds(0.0f);

        GameManager.AddActionBeingPerformed();

        if (InAIUnit)
        {
            CheckActivation(InAIUnit);

            if (InAIUnit.IsActivated())
            {
                //Calculate ability
                UnitAbility selectedAbility = CalculateAbility(InAIUnit);

                //Calculate target
                GridUnit target = CalculateTargetUnit(InAIUnit, selectedAbility);

                if (target)
                {
                    // Do movement.
                    ILevelCell targetMovementCell = CalculateMoveToCell(InAIUnit, target, selectedAbility);

                    if (targetMovementCell)
                    {
                        if (targetMovementCell != InAIUnit.GetCell())
                        {
                            UnityEvent OnTraverseToComplete = new();

                            List<ILevelCell> AllowedCells = InAIUnit.GetAllowedMovementCells();

                            Debug.Log("RunOnUnit EnumeratorTraverseTo");

                            yield return GameManager.Get().StartCoroutine(InAIUnit.EnumeratorTraverseTo(targetMovementCell, OnTraverseToComplete, AllowedCells));
                        }
                    }

                    if (!InAIUnit.IsDead() && selectedAbility != null && selectedAbility.CustomExcuteCondition(InAIUnit, target)) //Unit can die while walking around.
                    {
                        // Do ability.

                        InAIUnit.SetCurrentAbility(selectedAbility);

                        if (selectedAbility)
                        {
                            if (selectedAbility.GetExcuteSeftInSelect() == false)
                            {
                                List<ILevelCell> abilityCells = selectedAbility.GetAbilityCells(InAIUnit);
                                if (abilityCells.Contains(target.GetCell()))
                                {
                                    Debug.Log("RunOnUnit ExecuteAbility");

                                    yield return GameManager.Get().StartCoroutine(AIManager.ExecuteAbility(InAIUnit, target.GetCell(), selectedAbility));
                                }
                            }
                            else
                            {
                                yield return GameManager.Get().StartCoroutine(AIManager.ExecuteAbility(InAIUnit, target.GetCell(), selectedAbility));
                            }

                        }
                    }

                }
            }
        }

        GameManager.RemoveActionBeingPerformed();

        Debug.Log("RunOnUnit done");

        yield break;
    }

    protected void CheckActivation(GridUnit InUnit)
    {
        if (!InUnit.IsActivated())
        {
            if (m_bActivateOnStart)
            {
                InUnit.SetActivated(true);
            }
            else
            {
                AIRadiusInfo radiusInfo = new(InUnit.GetCell(), m_ActivationRange)
                {
                    Caster = InUnit,
                    bAllowBlocked = true,
                    bStopAtBlockedCell = true,
                    EffectedTeam = GameTeam.Red
                };

                List<ILevelCell> ActivationCells = AIManager.GetRadius(radiusInfo);
                foreach (var cell in ActivationCells)
                {
                    GridObject objOnCell = cell.GetObjectOnCell();
                    if (objOnCell)
                    {
                        if (GameManager.GetTeamAffinity(InUnit.GetTeam(), objOnCell.GetTeam()) == GameTeam.Red)
                        {
                            InUnit.SetActivated(true);
                        }
                    }
                }
            }
        }
    }

    protected GridUnit CalculateTargetUnit(GridUnit InAIUnit, UnitAbility unitAbility)
    {
        GameTeam SelectedTeam = GameTeam.None;

        if(unitAbility != null)
        {
            if (unitAbility.GetEffectedTeam() == GameTeam.Blue) //If attack target is friendly
            {
                if (InAIUnit.GetTeam() == GameTeam.Red)
                {
                    SelectedTeam = GameTeam.Red;
                }
                else
                {
                    SelectedTeam = GameTeam.Blue;
                }
            }
            else if (unitAbility.GetEffectedTeam() == GameTeam.Red) //If attack target is enemy
            {
                if (InAIUnit.GetTeam() == GameTeam.Red)
                {
                    SelectedTeam = GameTeam.Blue;
                }
                else
                {
                    SelectedTeam = GameTeam.Red;
                }
            }
            else if (unitAbility.GetEffectedTeam() == GameTeam.All)
            {
                SelectedTeam = GameTeam.All;
            }

            if (unitAbility.GetExcuteSeftInSelect())
            {
                return InAIUnit;
            }
        }

        List<GridUnit> AIUnits = new();

        if(SelectedTeam == GameTeam.All)
        {
            AIUnits.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Red));
            AIUnits.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Blue));
        }
        else
        {
            AIUnits = GameManager.GetUnitsOnTeam(SelectedTeam);
        }

        AIUnits.Remove(InAIUnit);

        int closestIndex = int.MaxValue;
        GridUnit selectedTarget = null;
        foreach (GridUnit currUnit in AIUnits)
        {
            if (currUnit && !currUnit.IsDead())
            {
                AIPathInfo pathInfo = new()
                {
                    StartCell = InAIUnit.GetCell(),
                    TargetCell = currUnit.GetCell(),
                    bIgnoreUnits = false,
                    bTakeWeightIntoAccount = true
                };

                List<ILevelCell> unitPath = AIManager.GetPath(pathInfo);
                if (unitPath.Count < closestIndex)
                {
                    closestIndex = unitPath.Count;
                    selectedTarget = currUnit;
                }
            }
        }

        if (unitAbility != null && unitAbility.GetEffectedTeam() == GameTeam.Red)
        {
            //Get MonsterSpecialBufferType.挑発_Provoke buff cells

            List<GridUnit> ProvokeUnits = new();

            //Debug.Log("Unit AI Caculate Provoke Cells");

            //Debug.Log("AIUnits = " + AIUnits.Count);

            foreach (GridUnit unit in AIUnits)
            {
                ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();
                if (unit.IsDead() == false && parameterInGame.CheckBuffCount(MonsterSpecialBufferType.挑発_Provocation) > 0)
                {
                    ProvokeUnits.Add(unit);

                    //Debug.Log("挑発_Provoke GridUnit = " + unit.name);
                }
            }

            if (ProvokeUnits.Count > 0)
            {
                int ranIndex = Random.Range(0, ProvokeUnits.Count);

                Debug.Log("Selected 挑発_Provoke GridUnit = " + ProvokeUnits[ranIndex].name);

                return ProvokeUnits[ranIndex];
            }
        }

        return selectedTarget;
    }

    protected UnitAbility CalculateAbility(GridUnit InAIUnit)
    {
        UnitAbility slectedAbility = null;

        if (InAIUnit)
        {
            int allowedAP = InAIUnit.GetCurrentST();
            //List<UnitAbilityPlayerData> abilities = InAIUnit.GetAbilities();

            List<UnitAbilityPlayerData> abilities = InAIUnit.GameMonster.UnitAi.GetAbilities();

            List<UnitAbilityPlayerData> canUseList = new();

            if (abilities.Count > 0)
            {
                for(int i=0; i< abilities.Count; i++)
                {
                    if (abilities[i].unitAbility.GetActionPointCost() <= allowedAP)
                    {
                        canUseList.Add(abilities[i]);
                    }
                }
            }

            if (canUseList.Count > 0)
            {
                int ranIndex = Random.Range(0, canUseList.Count);

                slectedAbility = canUseList[ranIndex].unitAbility;
            }
        }

        return slectedAbility;
    }

    protected ILevelCell CalculateMoveToCell(GridUnit InAIUnit, GridUnit InTarget, UnitAbility unitAbility)
    {
        if (InTarget == null || InAIUnit == InTarget)
        {
            return InAIUnit.GetCell();
        }

        List<UnitAbilityPlayerData> AIUnitAbilities = InAIUnit.GetAbilities();
        if (AIUnitAbilities.Count < 0)
        {
            return InAIUnit.GetCell();
        }

        List<ILevelCell> AllowedMovementCells = InAIUnit.GetAllowedMovementCells();
        List<ILevelCell> OverlapCells = new();

        if (unitAbility != null)
        {
            //Debug.Log("CalculateMoveToCell SelectedAbility.GetShape().GetCellList");
            List<ILevelCell> CellsAroundUnitToAttack = unitAbility.GetShape().GetCellList(InTarget, InTarget.GetCell(), unitAbility.GetRadius(), unitAbility.DoesAllowBlocked());

            //Debug.Log("CalculateMoveToCell GetAbilityCells SelectedAbility.GetShape().GetCellList");
            //If you can attack from where you are, do so.
            List<ILevelCell> AbilityCells = unitAbility.GetShape().GetCellList(InAIUnit, InAIUnit.GetCell(), unitAbility.GetRadius(), unitAbility.DoesAllowBlocked());
            if (AbilityCells.Contains(InTarget.GetCell()))
            {
                return InAIUnit.GetCell();
            }

            //Find cells that you can move to and attack.
            foreach (ILevelCell levelCell in CellsAroundUnitToAttack)
            {
                if (AllowedMovementCells.Contains(levelCell))
                {
                    OverlapCells.Add(levelCell);
                }
            }
        }

        bool bAbleToAttack = OverlapCells.Count > 0;
        if (bAbleToAttack)
        {
            //Cells exist that allow movement, and attack.
            int currDistance = -1;
            ILevelCell selectedCell = null;
            foreach (ILevelCell levelCell in OverlapCells)
            {
                AIPathInfo pathInfo = new()
                {
                    StartCell = levelCell,
                    TargetCell = InTarget.GetCell(),
                    bIgnoreUnits = false,
                    bTakeWeightIntoAccount = true
                };

                List<ILevelCell> levelPath = AIManager.GetPath(pathInfo);
                int cellDistance = levelPath.Count - 1;
                if (cellDistance > currDistance)
                {
                    selectedCell = levelCell;
                    currDistance = cellDistance;
                }
            }

            Debug.Log("ai move");

            return selectedCell;
        }
        else// Move towards target
        {
            int currDistance = int.MaxValue;
            ILevelCell selectedCell = null;
            foreach (ILevelCell levelCell in AllowedMovementCells)
            {
                AIPathInfo pathInfo = new()
                {
                    StartCell = levelCell,
                    TargetCell = InTarget.GetCell(),
                    bIgnoreUnits = false,
                    bTakeWeightIntoAccount = true
                };

                int cellDistance = AIManager.GetPath(pathInfo).Count - 1;
                if (cellDistance < currDistance)
                {
                    selectedCell = levelCell;
                    currDistance = cellDistance;
                }
            }

            Debug.Log("ai move towards");

            return selectedCell;
        }
    }
}
