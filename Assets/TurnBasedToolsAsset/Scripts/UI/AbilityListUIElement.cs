﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityListUIElement : MonoBehaviour
{
    GridUnit m_SelectedUnit;

    public Texture2D BlankTexture;
    public GameObject AbilityList;

    public HorizontalLayoutGroup horizontalLayoutGroup;

    public UIAbilityMoveButton uIAbilityMoveButton;

    public UIAbilityDefenseButton uIAbilityDefenseButton;

    public UIAbilityBlowAwayButton uIAbilityBlowAwayButton;

    [SerializeField]
    UnitAbility selectedUnitAbility = null;

    public List<AbilityDataUIElement> m_UIAbilities;

    void Start()
    {
        ClearAbilityList();
        GameManager.Get().OnUnitSelected.AddListener(HandleUnitSelected);
        AbilityList.SetActive(false);

        UIAccuracyRate.Instance.SetActive(false);
    }

    public void HandleUnitSelected(GridUnit InUnit)
    {
        if (InUnit)
        {
            AbilityList.SetActive(true);
            m_SelectedUnit = InUnit;
            SetupAbilityList();

            UIAccuracyRate.Instance.SetActive(true);
        }
        else
        {
            ClearAbilityList();
            m_SelectedUnit = null;
            AbilityList.SetActive(false);

            UIAccuracyRate.Instance.SetActive(false);
        }
    }

    public void SetupAbilityList()
    {
        selectedUnitAbility = null;

        if (m_SelectedUnit != null)
        {
            MonsterInGame monsterInGame = m_SelectedUnit.GameMonster;

            MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInGame.MonsterInfo.monsterID);

            List<UnitAbilityPlayerData> unitAbilities = m_SelectedUnit.GetAbilities();

            List<UnitAbilityPlayerData> specAbilities = m_SelectedUnit.GetSpecAbilities();

            List<AbilityDataUIElement> spec_UIAbilities = new List<AbilityDataUIElement>();

            for (int i = 0; i < m_UIAbilities.Count; i++)
            {
                if (i < monsterInGame.MonsterInfo.SelectedBattleSkillsId.Count)
                {
                    m_UIAbilities[i].SetOwner(this);

                    for (int k = 0; k < unitAbilities.Count; k++)
                    {
                        if (unitAbilities[k].unitAbility.GetSkillID() == monsterInGame.MonsterInfo.SelectedBattleSkillsId[i])
                        {
                            m_UIAbilities[i].SetAbility(unitAbilities[k].unitAbility);

                            break;
                        }
                    }

                    m_UIAbilities[i].gameObject.SetActive(true);
                }
                else
                {
                    m_UIAbilities[i].gameObject.SetActive(false);

                    spec_UIAbilities.Add(m_UIAbilities[i]);
                }
            }

            for (int i = 0; i < spec_UIAbilities.Count; i++)
            {
                if (i < specAbilities.Count)
                {
                    spec_UIAbilities[i].SetOwner(this);

                    spec_UIAbilities[i].SetAbility(specAbilities[i].unitAbility);

                    spec_UIAbilities[i].gameObject.SetActive(true);
                }
                else
                {
                    spec_UIAbilities[i].gameObject.SetActive(false);
                }
            }
        }

        uIAbilityDefenseButton.CheckCost(m_SelectedUnit);
        uIAbilityMoveButton.CheckCost(m_SelectedUnit);
        uIAbilityBlowAwayButton.CheckCost(m_SelectedUnit);

        StartCoroutine(ForceUpdateLayout(horizontalLayoutGroup));
    }

    private IEnumerator ForceUpdateLayout(Component panel)
    {
        var layoutgroup = panel.GetComponentInChildren<LayoutGroup>();
        if (layoutgroup != null)
        {
            yield return null;
            layoutgroup.enabled = false;
            layoutgroup.enabled = true;
        }

        yield break;
    }

    void ClearAbilityList()
    {
        for (int i = 0; i < m_UIAbilities.Count; i++)
        {
            AbilityDataUIElement abilityUI = m_UIAbilities[i];
            if (abilityUI)
            {
                abilityUI.SetOwner(this);
                abilityUI.ClearAbility();
            }
        }
    }

    public void HandleAbilitySelected(UnitAbility unitAbility)
    {
        selectedUnitAbility = null;

        if (m_SelectedUnit)
        {
            selectedUnitAbility = unitAbility;

            m_SelectedUnit.SetupAbility(unitAbility);
        }
    }

    public void HandleAbilityHover(UnitAbility unitAbility, bool onHover)
    {
        if (m_SelectedUnit && unitAbility != null)
        {
            if (onHover)
            {
                m_SelectedUnit.SetupAbilityHover(unitAbility);
            }
            else if (selectedUnitAbility != null)
            {
                HandleAbilitySelected(selectedUnitAbility);
            }
            else
            {
                uIAbilityMoveButton.Move();
            }
        }
    }
}
