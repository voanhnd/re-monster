﻿using Assets.Scripts;
using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.Scriptables;
using Assets.Scripts.UI;
using Assets.Scripts.UI.BattleUI;
using System;
using System.Collections.Generic;
using System.Net.Configuration;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class AbilityConnections
{
    [SerializeField] private Image abilityImage;

    [Header("Range")]
    [SerializeField] private TextMeshProUGUI rangeNumText;

    [Header("Cost")]
    [SerializeField] private TextMeshProUGUI costNumText;
    [Space(5)]
    [SerializeField] private TextMeshProUGUI delayNumText;
    [SerializeField] private TextMeshProUGUI delayTimeNumText;
    [SerializeField] private TextMeshProUGUI abilityLevelText;

    [Header("Slider stat")]
    [SerializeField] private Slider hitSlider;
    [SerializeField] private Slider crtSlider;
    [SerializeField] private Slider hpSlider;
    [SerializeField] private Slider stSlider;
    [SerializeField] private Slider delayDmgSlider;

    [Header("Stat rank")]
    [SerializeField] private Image hitRankImage;
    [SerializeField] private Image crtRankImage;
    [SerializeField] private Image hpRankImage;
    [SerializeField] private Image stRankImage;
    [SerializeField] private Image delayRankImage;

    [Header("Ability Pop-up")]
    [SerializeField] private GameObject abilityPopup;
    [SerializeField] private Image abilityTypeGraphic;
    [SerializeField] private Image abilityAttributeTypeGraphic;
    [SerializeField] private TextMeshProUGUI abilityNameText;
    [SerializeField] private TextMeshProUGUI abilityExecutionText;
    [SerializeField] private TextMeshProUGUI abilityTypeText;
    [SerializeField] private TextMeshProUGUI abilityAttributeTypeText;
    [SerializeField] private TextMeshProUGUI abilityScopeOfEffectText;

    [SerializeField] private bool isLocked;
    [SerializeField] private Button lockBtn;
    [SerializeField] private Image lockBackground;

    [Header("Ability effect")]
    [SerializeField] private TextMeshProUGUI abilityEffectText;
    [SerializeField] private Transform abilityEffectContainer;
    [SerializeField] private GameObject abilityEffectFragmentPrefab;

    public Image AbilityImage => abilityImage;
    public Image LockBackground => lockBackground;
    public Button LockBtn => lockBtn;
    public bool IsLocked { get => isLocked; set => isLocked = value; }
    public GameObject AbilityPopup => abilityPopup;
    public TextMeshProUGUI AbilityNameText => abilityNameText;
    public TextMeshProUGUI AbilityExecutionText => abilityExecutionText;
    public TextMeshProUGUI CostNumText => costNumText;
    public TextMeshProUGUI RangeNumText => rangeNumText;
    public TextMeshProUGUI DelayNumText => delayNumText;
    public TextMeshProUGUI DelayTimeNumText => delayTimeNumText;
    public TextMeshProUGUI AbilityLevelText => abilityLevelText;
    public TextMeshProUGUI AbilityTypeText => abilityTypeText;
    public TextMeshProUGUI AbilityAttributeTypeText => abilityAttributeTypeText;
    public TextMeshProUGUI AbilityEffectText => abilityEffectText;
    public TextMeshProUGUI AbilityScopeOfEffectText => abilityScopeOfEffectText;

    public Slider HitSlider => hitSlider;
    public Slider CrtSlider => crtSlider;
    public Slider HpSlider => hpSlider;
    public Slider StSlider => stSlider;
    public Slider DelayDmgSlider => delayDmgSlider;

    public Image HitRankImage => hitRankImage;
    public Image CrtRankImage => crtRankImage;
    public Image HpRankImage => hpRankImage;
    public Image StRankImage => stRankImage;
    public Image AbilityTypeGraphic => abilityTypeGraphic;
    public Image AbilityAttributeTypeGraphic => abilityAttributeTypeGraphic;
    public Image DelayRankImage => delayRankImage;

    public Transform AbilityEffectContainer => abilityEffectContainer;
    public GameObject AbilityEffectFragmentPrefab => abilityEffectFragmentPrefab;

}

public class AbilityDataUIElement : MonoBehaviour
{
    [SerializeField]
    private UnitAbility ability;
    private AbilityListUIElement owner;
    [SerializeField] private List<SkillTypeColorSetting> skillAttributeColor;
    [SerializeField] private UIElementsScriptable[] elementalDatas;
    [SerializeField] private AbilityConnections connections;
    [SerializeField] private GameObject overflowSTNotify;
    [SerializeField] private TextMeshProUGUI overflowValueText;

    public UnitAbility Ability { get => ability; set => ability = value; }
    public AbilityConnections Connections => connections;

    private void Start()
    {
        SetPopupVisibility(false);
        SetupUIElement();
        connections.LockBackground.color = new Color(0, 0, 0, 0);
        GameManager.Get().OnTeamWon.AddListener(HandleGameDone);
    }

    public void SetOwner(AbilityListUIElement InListUIElem) => owner = InListUIElem;

    public void SetAbility(UnitAbility InAbility)
    {
        Ability = InAbility;
        SetupUIElement();
    }

    #region UI control
    public void ClearAbility()
    {
        Ability = null;

        connections.AbilityImage.sprite = null;

        connections.RangeNumText.text = "";
        connections.CostNumText.text = "";

        connections.AbilityNameText.text = "";
        //Connections.AbilityInfoText.text = "";
    }

    private void SetupUIElement()
    {
        if (Ability == null) return;

        SkillDataScriptObj skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(Ability.GetSkillID());

        GridUnit selectedUnit = GameManager.GetRules().GetSelectedUnit();

        MonsterInGame monsterIngame = selectedUnit.GameMonster;

        int skillLvl = Skill.GetSkillLevel(monsterIngame.MonsterInfo, skillDataScriptObj);

        if (skillLvl <= 0)
        {
            skillLvl = 1;
        }

        Skill.SkillDetail skillDetail = skillDataScriptObj.Skill.skillDetails[skillLvl - 1];

        float overflowValue = CheckOverflowST(skillDetail, selectedUnit);

        connections.AbilityImage.sprite = Ability.GetIconSprite();

        connections.RangeNumText.text = Ability.GetRadius() + " Hex ";

        connections.AbilityScopeOfEffectText.text = $"{AreaScopeOfEffectConvert.Instance.ConvertScopeEng(skillDetail.areaOfEffectType)}: {skillDetail.areaOfEffectRange}";

        connections.AbilityExecutionText.text = $"{AreaScopeOfEffectConvert.Instance.ConvertScopeEng(skillDetail.occurrenceType)}";

        connections.CostNumText.text = skillDetail.consumedST + " ST";

        connections.AbilityNameText.text = skillDataScriptObj.Skill.skillName;

        connections.DelayNumText.text = $"{skillDetail.delay} SPD";

        connections.AbilityLevelText.text = "Lv " + skillLvl;

        connections.AbilityTypeText.text = SkillAttribute.GetSkillGenTypeString(skillDataScriptObj.Skill.skillGenType);

        connections.AbilityTypeGraphic.color = SkillTypeColorSetting.GetSkillGenColor(skillAttributeColor, skillDataScriptObj.Skill.skillGenType);

        UIElementsScriptable elementData = UIElementsScriptable.GetElementData(elementalDatas,skillDetail.attributeType);
        if(elementData != null)
        {
            connections.AbilityAttributeTypeText.text = elementData.ElementNameJp;
            connections.AbilityAttributeTypeGraphic.sprite = elementData.ElementIcon;
            connections.AbilityAttributeTypeGraphic.color = elementData.ElementColor;
        }

        connections.HitSlider.value = skillDetail.hit + skillDetail.hit * overflowValue;

        connections.CrtSlider.value = skillDetail.crt + skillDetail.crt * overflowValue;

        connections.HpSlider.value = skillDetail.hp_dmg + skillDetail.hp_dmg * overflowValue;

        connections.StSlider.value = skillDetail.st_dmg + skillDetail.st_dmg * overflowValue;

        connections.DelayDmgSlider.value = skillDetail.delayDamage + skillDetail.delayDamage * overflowValue;

        if (skillDetail.crt > 0)
            connections.CrtRankImage.sprite = RankText.Instance.GetRank(Skill.Get_Skill_CRT_Rank(skillDetail.crt + skillDetail.crt * overflowValue));
        else
            connections.CrtRankImage.sprite = RankText.Instance.NullRank;

        if (skillDetail.st_dmg > 0)
            connections.StRankImage.sprite = RankText.Instance.GetRank(Skill.Get_Skill_ST_DMG_Rank(skillDetail.st_dmg + skillDetail.st_dmg * overflowValue));
        else
            connections.StRankImage.sprite = RankText.Instance.NullRank;

        if (skillDetail.hit > 0)
            connections.HitRankImage.sprite = RankText.Instance.GetRank(Skill.Get_Skill_HIT_Rank(skillDetail.hit + skillDetail.hit * overflowValue));
        else
            connections.HitRankImage.sprite = RankText.Instance.NullRank;

        if (skillDetail.hp_dmg > 0)
            connections.HpRankImage.sprite = RankText.Instance.GetRank(Skill.Get_Skill_HP_DMG_Rank(skillDetail.hp_dmg + skillDetail.hp_dmg * overflowValue));
        else
            connections.HpRankImage.sprite = RankText.Instance.NullRank;

        if (skillDetail.delayDamage > 0)
            connections.DelayRankImage.sprite = RankText.Instance.GetRank(Skill.Get_Skill_DELAY_DMG_Rank(skillDetail.delayDamage + skillDetail.delayDamage * overflowValue));
        else
            connections.DelayRankImage.sprite = RankText.Instance.NullRank;

        Helpers.DestroyChildren(connections.AbilityEffectContainer);

        int effectCount = 0;
        for (int i = 0; i < skillDetail.skillBuffDetails.Count; i++)
        {
            GameObject abilityEffectGameObject = Instantiate(connections.AbilityEffectFragmentPrefab, connections.AbilityEffectContainer);
            UIAbilityEffectFragment abilityEffectUI = abilityEffectGameObject.GetComponent<UIAbilityEffectFragment>();

            SkillBuffImageManagerScriptObj.BuffEffectsData buffEffectData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffEffectData(skillDetail.skillBuffDetails[i].monsterSpecialBufferType);
            abilityEffectUI.SetEffectFragment(buffEffectData.BuffSprite, true);
            abilityEffectUI.SetEffectSuccessRate(string.Empty);
            effectCount++;
        }

        for (int i = 0; i < skillDetail.skillDeBuffDetails.Count; i++)
        {
            GameObject abilityEffectGameObject = Instantiate(connections.AbilityEffectFragmentPrefab, connections.AbilityEffectContainer);
            UIAbilityEffectFragment abilityEffectUI = abilityEffectGameObject.GetComponent<UIAbilityEffectFragment>();
            switch (skillDetail.skillDeBuffDetails[i].monsterSpecialDeBufferType)
            {
                case MonsterSpecialDeBufferType.毒_Poison:
                case MonsterSpecialDeBufferType.猛毒_Deadly_Poison:
                case MonsterSpecialDeBufferType.衰弱_Weakness:
                case MonsterSpecialDeBufferType.盲目_Blindness:
                case MonsterSpecialDeBufferType.加速_Slowness:
                case MonsterSpecialDeBufferType.麻痺_Paralysis:
                case MonsterSpecialDeBufferType.睡眠_Sleep:
                case MonsterSpecialDeBufferType.激怒_Rage:
                case MonsterSpecialDeBufferType.恐怖_Fear:
                case MonsterSpecialDeBufferType.混乱_Confusion:
                case MonsterSpecialDeBufferType.魅了_Charm:
                    abilityEffectUI.SetEffectSuccessRate($"{skillDetail.skillDeBuffDetails[i].value:0.##}%");
                    break;
            }
            SkillBuffImageManagerScriptObj.DebuffEffectsData debuffEffectData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDebuffEffectData(skillDetail.skillDeBuffDetails[i].monsterSpecialDeBufferType);

            abilityEffectUI.SetEffectFragment(debuffEffectData.DebuffSprite, false);
            effectCount++;
        }

        if (effectCount > 0)
            connections.AbilityEffectText.text = "";
        else
            connections.AbilityEffectText.text = "-";

        //Connections.AbilityInfoText.text = GenerateAbilityInfo();

        CheckLocked(skillLvl, skillDetail, selectedUnit);
    }

    private float CheckOverflowST(Skill.SkillDetail skillDetail, GridUnit selectedUnit)
    {
        if (skillDetail.consumedST >= selectedUnit.GetCurrentST())
        {
            overflowSTNotify.SetActive(false);
            return 0;
        }
        float overflowValue = MonsterBattle.Get_Stamina_Performance_Improvement(selectedUnit.GetCurrentST(), skillDetail.consumedST);
        overflowSTNotify.SetActive(true);
        this.overflowValueText.text = $"{overflowValue * 100f:0.##}%";
        return overflowValue;
    }

    private void CheckLocked(int skillLvl, Skill.SkillDetail skillDetail, GridUnit selectedUnit)
    {
        if (skillLvl == 0 || skillDetail.consumedST > selectedUnit.GetCurrentST())
        {
            Debug.Log("Skill can not use");

            Debug.Log("Skill level = " + skillLvl);

            Debug.Log("Skill cost = " + skillDetail.consumedST.ToString());

            Debug.Log("CurrentAbilityPoints = " + selectedUnit.GetCurrentST().ToString());

            connections.IsLocked = true;
        }
        else
        {
            connections.IsLocked = false;
        }

        connections.LockBtn.interactable = !connections.IsLocked;
        if (connections.IsLocked)
            connections.LockBackground.color = new Color(0, 0, 0, 0.7f);
        else
            connections.LockBackground.color = new Color(0, 0, 0, 0);
        connections.LockBackground.raycastTarget = connections.IsLocked;
    }

    private void SetPopupVisibility(bool bInVisible)
    {
        if (bInVisible && !Ability) return;

        connections.AbilityPopup.SetActive(bInVisible);
        if (bInVisible) SetupUIElement();
    }
    #endregion

    #region Event handles
    public void OnClicked()
    {
        Debug.Log("Ability clicked = " + ability.GetSkillID());

        if (GameManager.IsActionBeingPerformed())
            return;

        GridUnit selectedUnit = GameManager.GetRules().GetSelectedUnit();

        Debug.Log("selectedUnit = " + selectedUnit.name);

        Debug.Log("m_Ability = " + Ability.GetSkillID());

        Debug.Log("selectedUnit.IsAttacking() = " + selectedUnit.IsAttacking());

        Debug.Log("m_Connections.isLocked = " + connections.IsLocked);

        if (Ability && selectedUnit && selectedUnit.IsAttacking() == false && connections.IsLocked == false && owner)
        {
            owner.HandleAbilitySelected(Ability);
        }

        SetPopupVisibility(true);
    }

    public void OnHover(bool bStart)
    {
        if (GameManager.IsActionBeingPerformed())
            return;

        GridUnit selectedUnit = GameManager.GetRules().GetSelectedUnit();

        if (Ability && selectedUnit && selectedUnit.IsAttacking() == false && connections.IsLocked == false && owner)
        {
            owner.HandleAbilityHover(Ability, bStart);
        }

        SetPopupVisibility(bStart);
    }

    private void HandleGameDone(GameTeam InWinningTeam) => gameObject.SetActive(false);
    #endregion

    [Serializable]
    private class SkillTypeColorSetting
    {
        [SerializeField] private Color targetColor;
        [SerializeField] private Skill.SkillGenType skillGen;

        public Color TargetColor => targetColor;
        public Skill.SkillGenType SkillGen => skillGen;

        public static Color GetSkillGenColor(List<SkillTypeColorSetting> colorList, Skill.SkillGenType skillGen)
        {
            for (int i = 0; i < colorList.Count; i++)
            {
                if (skillGen == colorList[i].SkillGen)
                {
                    return colorList[i].TargetColor;
                }
            }
            return Color.white;
        }
    }
}
