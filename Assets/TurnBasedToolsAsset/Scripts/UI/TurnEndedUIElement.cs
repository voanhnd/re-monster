﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TurnEndedUIElement : MonoBehaviour
{
    public GameObject m_Screen;
    public Image m_Image;
    public Sprite m_FriendlyTurn;
    public Sprite m_HostileTurn;

    public Button playagainBttn;

    bool m_bShowing = false;
    Queue<GameTeam> TeamQueue = new Queue<GameTeam>();

    void Start()
    {
        playagainBttn.onClick.AddListener(ReloadScene);

        GameManager.Get().OnTeamWon.AddListener(HandleGameDone);

        GameRules gameRules = GameManager.GetRules();
        if (gameRules)
        {
            gameRules.OnTeamTurnBegin.AddListener(HandleTeamChanged);
        }

        m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, 0.0f);
    }

    void HandleTeamChanged(GameTeam InTeam)
    {
        if (InTeam == GameTeam.Blue || InTeam == GameTeam.Red)
        {
            if (m_bShowing)
            {
                TeamQueue.Enqueue(InTeam);
            }
            else
            {
                m_Image.sprite = InTeam == GameTeam.Blue ? m_FriendlyTurn : m_HostileTurn;
                m_bShowing = true;
                StartCoroutine(HandleShowGraphic());
            }
        }
    }

    IEnumerator HandleShowGraphic()
    {
        m_bShowing = true;

        m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, 1.0f);

        yield return new WaitForSeconds(0.5f);

        m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, 0.0f);

        m_bShowing = false;

        if (TeamQueue.Count > 0)
        {
            HandleTeamChanged(TeamQueue.Dequeue());
        }

        yield break;
    }


    void HandleGameDone(GameTeam InWinningTeam)
    {
        m_Screen.SetActive(false);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
