﻿using UnityEngine;
using UnityEngine.UI;

public class GameEndUIElement : MonoBehaviour
{
    public GameObject m_EndScreen;

    public Image m_EndTeamImage;

    public Sprite m_FriendlyWinSprite;
    public Sprite m_HostileWinSprite;

    void Start()
    {
        m_EndScreen.SetActive(false);
        GameManager.Get().OnTeamWon.AddListener(HandleTeamWin);
    }

    void HandleTeamWin(GameTeam InTeam)
    {
        ShowEndScreen(InTeam == GameTeam.Blue);
    }

    void ShowEndScreen(bool bIsFriendlyWin)
    {
        m_EndScreen.SetActive(true);
        m_EndTeamImage.sprite = bIsFriendlyWin ? m_FriendlyWinSprite : m_HostileWinSprite;
    }
}
