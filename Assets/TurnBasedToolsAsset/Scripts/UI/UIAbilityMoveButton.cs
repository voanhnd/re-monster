using Assets.Scripts.Monster;
using UnityEngine;
using UnityEngine.UI;

public class UIAbilityMoveButton : MonoBehaviour
{
    GridUnit m_SelectedUnit;

    ParameterInGame parameterInGame;

    public Button m_Button;

    public Transform lockBG;

    private void Start()
    {
        m_Button.onClick.AddListener(() => { Move(); });
    }

    void Update()
    {
        m_Button.interactable = !GameManager.IsActionBeingPerformed();
    }

    public void CheckCost(GridUnit unit)
    {
        m_SelectedUnit = unit;

        parameterInGame = m_SelectedUnit.GetComponent<ParameterInGame>();

        if (m_SelectedUnit != null && parameterInGame != null)
        {
            if (parameterInGame.GetCurrentStamina() >= MonsterBattle.Get_Move_Stamina_Cost(1))
            {
                lockBG.gameObject.SetActive(false);
            }
            else
            {
                lockBG.gameObject.SetActive(true);
            }
        }
    }

    public void Move()
    {
        Debug.Log("Move button");

        if (m_SelectedUnit)
        {
            m_SelectedUnit.HandleAbilityFinished();

            UIAccuracyRate.Instance.SetActive(false);
        }
    }
}
