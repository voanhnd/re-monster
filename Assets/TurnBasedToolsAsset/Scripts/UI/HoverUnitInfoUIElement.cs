﻿using Assets.Scripts.Monster;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HoverUnitInfoUIElement : MonoBehaviour
{
    public GameObject m_ScreenObject;

    [Space(5)]

    public TextMeshProUGUI m_UnitName;

    [Space(5)]

    public TextMeshProUGUI m_HealthText;
    public TextMeshProUGUI m_APText;

    [Space(5)]

    public Slider m_HealthSlider;
    public Slider m_APSlider;

    GridUnit m_CurrUnit = null;

    bool bEnabled = true;

    private void Awake()
    {
        SetupScreen();
    }

    void Start()
    {
        GameManager.Get().OnUnitHover.AddListener(HandleUnitHover);
        GameManager.Get().OnTeamWon.AddListener(HandleGameDone);
    }

    void Update()
    {
        SetupScreen();
    }

    void HandleUnitHover(GridUnit InUnit)
    {
        if (m_CurrUnit)
        {
            ParameterInGame parameterInGame = m_CurrUnit.GameMonster.InGameParameter;
            if (parameterInGame)
            {
                parameterInGame.OnHealthDepleted.RemoveListener(HandleUnitDeath);
            }
        }

        m_CurrUnit = InUnit;

        if (m_CurrUnit)
        {
            ParameterInGame parameterInGame = m_CurrUnit.GameMonster.InGameParameter;
            if (parameterInGame)
            {
                parameterInGame.OnHealthDepleted.AddListener(HandleUnitDeath);
            }
        }

        SetupScreen();
    }

    void HandleUnitDeath()
    {
        HandleUnitHover(null);
    }

    void SetupScreen()
    {
        if (m_CurrUnit && bEnabled)
        {
            m_ScreenObject.SetActive(true);

            UnitData unitData = m_CurrUnit.GetUnitData();

            float CurrAbilityPoints = m_CurrUnit.GetCurrentST();
            float TotalAbilityPoints = MonsterBattle.Get_Stamina_Min_Max().y;

            m_UnitName.text = unitData.monsterDataScriptObj.Monster.monsterName;
            m_APText.text = CurrAbilityPoints.ToString();

            float APPercentage = ((float)CurrAbilityPoints) / ((float)TotalAbilityPoints);
            m_APSlider.value = APPercentage;

            ParameterInGame parameterInGame = m_CurrUnit.GetComponent<ParameterInGame>();
            if (parameterInGame)
            {
                m_HealthText.text = parameterInGame.GetHealth().ToString();

                m_HealthSlider.value = parameterInGame.GetHealthPercentage();
            }
        }
        else
        {
            m_ScreenObject.SetActive(false);
        }
    }


    void HandleGameDone(GameTeam InWinningTeam)
    {
        m_ScreenObject.SetActive(false);
        bEnabled = false;
    }
}
