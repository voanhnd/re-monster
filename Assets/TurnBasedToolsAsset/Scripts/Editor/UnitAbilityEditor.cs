﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UnitAbility))]
[CanEditMultipleObjects]
public class UnitAbilityEditor : Editor
{
    void DrawAbilityHeader()
    {
        SerializedProperty m_Script = serializedObject.FindProperty("m_Script");
        SerializedProperty m_AbilityName = serializedObject.FindProperty("m_AbilityName");
        SerializedProperty m_SpriteIcon = serializedObject.FindProperty("m_SpriteIcon");
        SerializedProperty m_Radius = serializedObject.FindProperty("m_Radius");
        SerializedProperty m_ActionPointCost = serializedObject.FindProperty("m_ActionPointCost");
        SerializedProperty m_moveToTarget = serializedObject.FindProperty("m_moveToTarget");
        SerializedProperty m_bAllowBlocked = serializedObject.FindProperty("m_bAllowBlocked");
        SerializedProperty m_EffectedType = serializedObject.FindProperty("m_EffectedType");
        SerializedProperty m_EffectedTeam = serializedObject.FindProperty("m_EffectedTeam");
        SerializedProperty m_ExcuteSeftInSelect = serializedObject.FindProperty("m_ExcuteSeftInSelect");
        //SerializedProperty m_AvoidSeft = serializedObject.FindProperty("m_AvoidSeft");
        SerializedProperty m_BuffSeft = serializedObject.FindProperty("m_BuffSeft");
        SerializedProperty m_DeBuffSeft = serializedObject.FindProperty("m_DeBuffSeft");
        SerializedProperty m_AbilityShape = serializedObject.FindProperty("m_AbilityShape");

        SerializedProperty m_EffectShape = serializedObject.FindProperty("m_EffectShape");
        SerializedProperty m_EffectRadius = serializedObject.FindProperty("m_EffectRadius");

        Texture2D IconTexture = AssetPreview.GetAssetPreview(m_SpriteIcon.objectReferenceValue);

        GUIStyle BorderStyle = new GUIStyle(EditorStyles.helpBox)
        {
            margin = new RectOffset(5, 5, 5, 5)
        };

        EditorGUILayout.PropertyField(m_Script, new GUIContent(m_Script.displayName));

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.BeginVertical(BorderStyle, GUILayout.Width(100), GUILayout.Height(110));
        GUI.backgroundColor = Color.white;

        GUILayout.Label(IconTexture, GUILayout.Width(100), GUILayout.Height(100));
        EditorGUILayout.PropertyField(m_SpriteIcon, new GUIContent(""));

        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();

        EditorGUILayout.PropertyField(m_AbilityName, new GUIContent(m_AbilityName.displayName));
        EditorGUILayout.PropertyField(m_Radius, new GUIContent(m_Radius.displayName));
        EditorGUILayout.PropertyField(m_ActionPointCost, new GUIContent(m_ActionPointCost.displayName));
        EditorGUILayout.PropertyField(m_moveToTarget, new GUIContent(m_moveToTarget.displayName));
        EditorGUILayout.PropertyField(m_bAllowBlocked, new GUIContent(m_bAllowBlocked.displayName));
        EditorGUILayout.PropertyField(m_EffectedType, new GUIContent(m_EffectedType.displayName));
        EditorGUILayout.PropertyField(m_EffectedTeam, new GUIContent(m_EffectedTeam.displayName));
        EditorGUILayout.PropertyField(m_ExcuteSeftInSelect, new GUIContent(m_ExcuteSeftInSelect.displayName));
        //EditorGUILayout.PropertyField(m_AvoidSeft, new GUIContent(m_AvoidSeft.displayName));
        EditorGUILayout.PropertyField(m_BuffSeft, new GUIContent(m_BuffSeft.displayName));
        EditorGUILayout.PropertyField(m_DeBuffSeft, new GUIContent(m_DeBuffSeft.displayName));
        EditorGUILayout.PropertyField(m_AbilityShape, new GUIContent(m_AbilityShape.displayName));

        EditorGUILayout.PropertyField(m_EffectShape, new GUIContent(m_EffectShape.displayName));
        EditorGUILayout.PropertyField(m_EffectRadius, new GUIContent(m_EffectRadius.displayName));

        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();
    }

    public override void OnInspectorGUI()
    {
        DrawAbilityHeader();

        List<PropertyReplaceInfo> ReplaceInfoList = new List<PropertyReplaceInfo>()
        {
            new PropertyReplaceInfo("m_CustomAbilities", EditorUtils.MakeCustomArrayWidget),
            new PropertyReplaceInfo("m_Params", EditorUtils.MakeCustomArrayWidget),
            new PropertyReplaceInfo("m_AbilityName", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_SpriteIcon", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_Radius", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_ActionPointCost", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_moveToTarget", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_bAllowBlocked", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_EffectedType", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_EffectedTeam", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_ExcuteSeftInSelect", EditorUtils.MakeBlankWidget),
            //new PropertyReplaceInfo("m_AvoidSeft", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_BuffSeft", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_DeBuffSeft", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_AbilityTimeLength", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_AbilityShape", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_SpawnOnCaster", EditorUtils.MakeCustomArrayWidget),
            new PropertyReplaceInfo("m_SpawnOnTarget", EditorUtils.MakeCustomArrayWidget),
            new PropertyReplaceInfo("m_Ailments", EditorUtils.MakeCustomArrayWidget),
            new PropertyReplaceInfo("m_EffectShape", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_EffectRadius", EditorUtils.MakeBlankWidget),
            new PropertyReplaceInfo("m_Script", EditorUtils.MakeBlankWidget),
        };

        EditorUtils.DrawAllProperties(serializedObject, ReplaceInfoList);

        serializedObject.ApplyModifiedProperties(); 
    }
}
