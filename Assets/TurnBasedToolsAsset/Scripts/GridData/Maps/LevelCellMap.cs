﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LevelCellPair
{
    [SerializeField]
    public CompassDir _Key;

    [SerializeField]
    public ILevelCell _Value;

    public LevelCellPair(CompassDir InKey, ILevelCell InValue) : this()
    {
        this._Key = InKey;
        this._Value = InValue;
    }
}

[System.Serializable]
public class LevelCellMap : System.Object
{
    [SerializeField]
    public List<LevelCellPair> Pairs;

    public LevelCellMap() => Pairs = new();

    public LevelCellPair Add(CompassDir InKey, ILevelCell InValue)
    {
        for (int i = 0; i < Pairs.Count; i++)
        {
            LevelCellPair item = Pairs[i];
            if (item._Key == InKey)
            {
                return item;
            }
        }

        Pairs.Add(new LevelCellPair(InKey, InValue));
        return Pairs[^1];
    }

    public bool ContainsKey(CompassDir InKey)
    {
        for (int i = 0; i < Pairs.Count; i++)
        {
            LevelCellPair item = Pairs[i];
            if (item._Key == InKey)
            {
                return true;
            }
        }

        return false;
    }

    public ILevelCell this[CompassDir InKey]
    {
        get
        {
            for (int i = 0; i < Pairs.Count; i++)
            {
                LevelCellPair item = Pairs[i];
                if (item._Key == InKey)
                {
                    return item._Value;
                }
            }

            return null;
        }
    }
}
