﻿using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum EffectedUnitType
{
    All,
    Ground,
    Flying,
}

[CreateAssetMenu(fileName = "NewAbility", menuName = "TurnBasedTools/Ability/Create New Ability", order = 1)]
public class UnitAbility : ScriptableObject
{
    [SerializeField]
    string m_AbilityName;

    [SerializeField]
    Sprite m_SpriteIcon;

    [SerializeField]
    int m_Radius;

    [SerializeField]
    int m_ActionPointCost;

    [SerializeField]
    bool m_moveToTarget;

    [SerializeField]
    bool m_bAllowBlocked;

    [SerializeField]
    GameTeam m_EffectedTeam;

    [SerializeField]
    EffectedUnitType m_EffectedType;

    [SerializeField]
    bool m_ExcuteSeftInSelect;

    //[SerializeField]
    //bool m_AvoidSeft;

    public bool m_BuffSeft;

    public bool m_DeBuffSeft;

    [SerializeField]
    AbilityShape m_AbilityShape;

    [SerializeField]
    AbilityShape m_EffectShape;

    [SerializeField]
    int m_EffectRadius;

    [SerializeField]
    AbilityParticle[] m_SpawnOnCaster;

    [SerializeField]
    AbilityParticle[] m_SpawnOnTarget;

    [SerializeField]
    AbilityParam[] m_Params;

    [SerializeField]
    Ailment[] m_Ailments;

    [SerializeField]
    DelayAbility[] m_DelayAbilities;

    List<ILevelCell> m_currentAbilitiesCells = new();

    [SerializeField]
    string skillID;

    void Reset()
    {
        m_bAllowBlocked = false;
    }

    #region Getters

    public string GetSkillID()
    {
        return skillID;
    }

    public string GetAbilityName()
    {
        return m_AbilityName;
    }

    public Sprite GetIconSprite()
    {
        return m_SpriteIcon;
    }

    public int GetActionPointCost()
    {
        return m_ActionPointCost;
    }

    public bool DoesAllowBlocked()
    {
        return m_bAllowBlocked;
    }

    public GameTeam GetEffectedTeam()
    {
        return m_EffectedTeam;
    }

    public bool IsEffectTeam(GridUnit InCaster, GridUnit InTarget)
    {
        if (GetEffectedTeam() == GameTeam.Red && InCaster.GetTeam() != InTarget.GetTeam())
        {
            return true;
        }
        else
        if (GetEffectedTeam() == GameTeam.Blue && InCaster.GetTeam() == InTarget.GetTeam())
        {
            return true;
        }
        else
        if (GetEffectedTeam() == GameTeam.All)
        {
            return true;
        }

        //Debug.Log("IsEffectTeam false");

        return false;
    }

    public EffectedUnitType GetEffectedUnitType()
    {
        return m_EffectedType;
    }

    public bool GetExcuteSeftInSelect()
    {
        return m_ExcuteSeftInSelect;
    }

    //public bool GetAvoidSeft()
    //{
    //    return m_AvoidSeft;
    //}

    public AbilityShape GetShape()
    {
        return m_AbilityShape;
    }

    public int GetRadius()
    {
        return m_Radius;
    }

    public AbilityShape GetEffectShape()
    {
        return m_EffectShape;
    }

    public int GetEffectRadius()
    {
        return m_EffectRadius;
    }

    public List<AbilityParticle> GetCasterParticles()
    {
        return new List<AbilityParticle>(m_SpawnOnCaster);
    }

    public List<AbilityParticle> GetTargetParticles()
    {
        return new List<AbilityParticle>(m_SpawnOnTarget);
    }

    public List<AbilityParam> GetParameters()
    {
        return new List<AbilityParam>(m_Params);
    }

    public List<Ailment> GetAilments()
    {
        return new List<Ailment>(m_Ailments);
    }

    public List<DelayAbility> GetDelayAbilities()
    {
        return new List<DelayAbility>(m_DelayAbilities);
    }

    #endregion

    public virtual List<ILevelCell> Setup(GridUnit InCasterUnit)
    {
        if (!GetShape() || InCasterUnit == null)
        {
            return new List<ILevelCell>();
        }

        List<ILevelCell> abilityCells = GetAbilityCells(InCasterUnit);

        CellState AbilityState = (GetEffectedTeam() != CustomGameManager.Instance.GetPlayerTeam()) ? (CellState.eNegative) : (CellState.ePositive);

        //Get MonsterSpecialBufferType.挑発_Provoke buff cells

        List<ILevelCell> ProvokeCells = new();
        List<ILevelCell> NonProvokeCells = new();

        //Debug.Log("Caculate Provoke Cells");

        //Debug.Log("abilityCells = " + abilityCells.Count);

        for (int i = 0; i < abilityCells.Count; i++)
        {
            ILevelCell cell = abilityCells[i];

            GridUnit gridUnit = cell.GetUnitOnCell();

            if (gridUnit != null && gridUnit != InCasterUnit && gridUnit.IsDead() == false)
            {
                if (gridUnit.GetTeam() != InCasterUnit.GetTeam() && GetEffectedTeam() != CustomGameManager.Instance.GetPlayerTeam())
                {
                    ParameterInGame parameterInGame = gridUnit.GameMonster.InGameParameter;
                    if (parameterInGame.CheckBuffCount(MonsterSpecialBufferType.挑発_Provocation) > 0)
                    {
                        ProvokeCells.Add(cell);

                        Debug.Log("挑発_Provoke Cell = " + cell.name);

                        GameManager.SetCellState(cell, AbilityState);
                    }
                    continue;
                }
                if (gridUnit.GetTeam() == InCasterUnit.GetTeam() && GetEffectedTeam() == CustomGameManager.Instance.GetPlayerTeam())
                {
                    NonProvokeCells.Add(cell);
                    continue;
                }

                abilityCells.Remove(cell);
            }
            else
            {
                NonProvokeCells.Add(cell);
            }
        }

        if (ProvokeCells.Count > 0)
        {
            for (int i = 0; i < ProvokeCells.Count; i++)
            {
                ILevelCell cell = ProvokeCells[i];
                if (cell)
                {
                    GameManager.SetCellState(cell, AbilityState);
                }
            }

            for (int i = 0; i < NonProvokeCells.Count; i++)
            {
                ILevelCell cell = NonProvokeCells[i];
                if (cell)
                {
                    GameManager.SetCellState(cell, CellState.eBlocked);
                }
            }
        }
        else
        {
            for (int i = 0; i < abilityCells.Count; i++)
            {
                ILevelCell cell = abilityCells[i];
                if (cell)
                {
                    GameManager.SetCellState(cell, AbilityState);
                }
            }
        }

        GameManager.Get().CustomEndHover();

        return abilityCells;
    }

    public virtual List<ILevelCell> GetSetupCells(GridUnit InCasterUnit)
    {
        if (!GetShape() || InCasterUnit == null)
        {
            return new List<ILevelCell>();
        }

        List<ILevelCell> abilityCells = GetAbilityCells(InCasterUnit);

        List<ILevelCell> ProvokeCells = new();
        List<ILevelCell> NonProvokeCells = new();

        for (int i = 0; i < abilityCells.Count; i++)
        {
            ILevelCell cell = abilityCells[i];

            GridUnit gridUnit = cell.GetUnitOnCell();

            if (gridUnit != null && gridUnit != InCasterUnit && !gridUnit.IsDead())
            {
                if (gridUnit.GetTeam() != InCasterUnit.GetTeam() && GetEffectedTeam() != CustomGameManager.Instance.GetPlayerTeam())
                {
                    ParameterInGame parameterInGame = gridUnit.GetComponent<ParameterInGame>();
                    if (parameterInGame.CheckBuffCount(MonsterSpecialBufferType.挑発_Provocation) > 0)
                    {
                        ProvokeCells.Add(cell);
                    }
                    continue;
                }
                if (gridUnit.GetTeam() == InCasterUnit.GetTeam() && GetEffectedTeam() == CustomGameManager.Instance.GetPlayerTeam())
                {
                    NonProvokeCells.Add(cell);
                    continue;
                }

                abilityCells.Remove(cell);
            }
            else
            {
                NonProvokeCells.Add(cell);
            }
        }

        return abilityCells;
    }

    public virtual bool CustomExcuteCondition(GridUnit InCasterUnit, GridUnit targetUnit)
    {
        return true;
    }

    public List<ILevelCell> GetEffectedCells(GridUnit InCasterUnit, ILevelCell InTarget)
    {
        List<ILevelCell> EffectCellList = new();

        if (GetEffectShape() != null)
        {
            //Debug.Log("GetEffectedCells GetEffectShape().GetCellList");
            List<ILevelCell> EffectCells = GetEffectShape().GetCellList(InCasterUnit, InTarget, GetEffectRadius(), DoesAllowBlocked(), GameTeam.All);
            EffectCellList.AddRange(EffectCells);
        }
        else
        {
            bool bEffectsTarget = GameManager.CanCasterEffectTarget(InCasterUnit.GetCell(), InTarget, GetEffectedTeam(), DoesAllowBlocked());
            if (bEffectsTarget)
            {
                EffectCellList.Add(InTarget);
            }
        }

        return EffectCellList;
    }

    public List<ILevelCell> GetAbilityCells(GridUnit InUnit)
    {
        if (!InUnit)
        {
            return new List<ILevelCell>();
        }

        //Debug.Log("GetAbilityCells SelectedAbility.GetShape().GetCellList");
        //List<ILevelCell> AbilityCells = GetShape().GetCellList(InUnit, InUnit.GetCell(), GetRadius(), DoesAllowBlocked(), GetEffectedTeam());
        List<ILevelCell> AbilityCells = GetShape().GetCellList(InUnit, InUnit.GetCell(), GetRadius(), DoesAllowBlocked(), GameTeam.All);

        if (GetEffectedUnitType() == EffectedUnitType.All)
        {
            return AbilityCells;
        }
        List<ILevelCell> RemoveList = new();
        for (int i = 0; i < AbilityCells.Count; i++)
        {
            ILevelCell cell = AbilityCells[i];
            if (!cell)
            {
                continue;
            }
            GridUnit unitOnCell = cell.GetUnitOnCell();
            if (unitOnCell)
            {
                bool bIsFlying = unitOnCell.IsFlying();

                if (GetEffectedUnitType() == EffectedUnitType.Flying)
                {
                    if (!bIsFlying)
                    {
                        RemoveList.Add(cell);
                    }
                    continue;
                }
                if (GetEffectedUnitType() == EffectedUnitType.Ground)
                {
                    if (bIsFlying)
                    {
                        RemoveList.Add(cell);
                    }
                    continue;
                }
            }
            else
            {
                RemoveList.Add(cell);
            }
        }

        foreach (ILevelCell cell in RemoveList)
        {
            AbilityCells.Remove(cell);
        }

        return AbilityCells;
    }

    public virtual bool CanSelectTarget()
    {
        return true;
    }

    public virtual IEnumerator Execute(GridUnit InCasterUnit, ILevelCell InTarget, UnityEvent OnComplete = null)
    {
        //GridUnit TargetUnit = InTarget.GetUnitOnCell();

        //if(TargetUnit && InCasterUnit != TargetUnit && InCasterUnit.GetTeam() != TargetUnit.GetTeam())
        //{
        //    TargetUnit.PlayAnimation(InTarget.GetUnitOnCell().GetUnitData().m_DamagedAnimation);
        //}

        if (GetShape())
        {
            //Caculate Skill Stamina

            ParameterInGame atkParameterInGame = InCasterUnit.GameMonster.InGameParameter;
            MonsterInGame monsterAtk = InCasterUnit.GameMonster;
            //MonsterDataScriptObj monsterAtkScriptObject = PlayerStats.instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.playerMonsterInfo.monsterRace);
            //BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();
            //InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();
            SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(GetSkillID());

            int skillLvl = Skill.GetSkillLevel(monsterAtk.MonsterInfo, atk_skillDataScriptObj);
            Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[skillLvl - 1];

            //End caculate skill stamina

            List<ILevelCell> abilityCells = GetAbilityCells(InCasterUnit);

            List<ILevelCell> PathCells = InCasterUnit.GetPathTo(InTarget, abilityCells);
            int distanceAtkToDef = PathCells.Count - 1;

            if (abilityCells.Contains(InTarget) || GetExcuteSeftInSelect())
            {
                InternalHandlePreExcuteEffectedCell(InCasterUnit, InTarget);

                if (InCasterUnit.GetCell() != InTarget)
                {
                    InCasterUnit.LookAtCell(InTarget);
                }

                List<ILevelCell> EffectCellList = new();
                EffectCellList.AddRange(GetEffectedCells(InCasterUnit, InTarget));

                if (!EffectCellList.Contains(InTarget))
                {
                    EffectCellList.Add(InTarget);
                }

                GameManager.AddActionBeingPerformed();
                //Debug.Log("Execute AddActionBeingPerformed");

                InCasterUnit.RemoveAbilityPoints(m_ActionPointCost);

                UnitAbilityPlayerData SelectedPlayerData = InCasterUnit.GetUnitAbilityPlayerData(this);

                AnimationClip AbilityAnimation = SelectedPlayerData.AssociatedAnimation;
                if (AbilityAnimation)
                {
                    //Get MonsterSpecialDeBufferType.睡眠_Sleep buff cells

                    if (atk_skillDetail.CheckDeBuffCount(MonsterSpecialDeBufferType.睡眠_Sleep) > 0)
                    {
                        //Debug.Log("Play animation get Debuff 睡眠_Sleep = " + InCasterUnit.name);

                        InCasterUnit.PlayAnimation(AbilityAnimation, SelectedPlayerData.ExecuteAfterTime, false);
                    }
                    else
                    {
                        InCasterUnit.PlayAnimation(AbilityAnimation, SelectedPlayerData.ExecuteAfterTime, true);
                    }
                }

                AudioClip startAudioClip = SelectedPlayerData.AudioOnStart;
                if (startAudioClip)
                {
                    AudioPlayData audioData = new(startAudioClip);
                    AudioHandler.PlayAudio(audioData, InCasterUnit.transform.position);
                }

                yield return new WaitForSeconds(SelectedPlayerData.ExecuteAfterTime);

                AudioClip executeAudioClip = SelectedPlayerData.AudioOnExecute;
                if (executeAudioClip)
                {
                    AudioPlayData audioData = new AudioPlayData(executeAudioClip);
                    AudioHandler.PlayAudio(audioData, InCasterUnit.transform.position);
                }

                foreach (ILevelCell EffectCell in EffectCellList)
                {
                    InternalHandleEffectedCell(InCasterUnit, EffectCell, InTarget);
                }

                if (AbilityAnimation)
                {
                    float timeRemaining = (AbilityAnimation.length - SelectedPlayerData.ExecuteAfterTime);
                    timeRemaining = Mathf.Max(0, timeRemaining);

                    yield return new WaitForSeconds(timeRemaining);
                }

                //Debug.Log("Skill Excute ability staminaCost = " + atk_skillDetail.cost);

                //Caculate Skill Delay

                atkParameterInGame.AddSkillDelay(atk_skillDetail.delay);

                //End Caculate Skill Delay

                atkParameterInGame.AddStamina(-atk_skillDetail.consumedST);

                float staminaAfterExcute = atk_skillDetail.consumedST * atk_inGameDeBuffDetail.Stamina_Skill_Correction;
                //Debug.Log("Skill after Excute staminaCost buff = " + staminaAfterExcute);
                atkParameterInGame.AddStamina(-staminaAfterExcute);

                GameManager.RemoveActionBeingPerformed();

                //Debug.Log("Execute RemoveActionBeingPerformed");
            }
        }

        //Debug.Log("Execute OnComplete");

        CustomGameManager.Instance.SetNextTurn();

        OnComplete?.Invoke();

        yield break;
    }

    public virtual IEnumerator ExecuteSpecAbility(GridUnit InCasterUnit, ILevelCell InTarget, UnityEvent OnComplete = null)
    {
        if (GetShape())
        {
            //Caculate Skill Stamina

            ParameterInGame atkParameterInGame = InCasterUnit.GameMonster.InGameParameter;
            MonsterInGame monsterAtk = InCasterUnit.GameMonster;
            //MonsterDataScriptObj monsterAtkScriptObject = PlayerStats.instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.playerMonsterInfo.monsterRace);
            //BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();
            //InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();
            SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(GetSkillID());

            Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[0];

            //End caculate skill stamina

            List<ILevelCell> abilityCells = GetAbilityCells(InCasterUnit);

            List<ILevelCell> PathCells = InCasterUnit.GetPathTo(InTarget, abilityCells);
            int distanceAtkToDef = PathCells.Count - 1;

            if (abilityCells.Contains(InTarget) || GetExcuteSeftInSelect())
            {
                InternalHandlePreExcuteEffectedCell(InCasterUnit, InTarget);

                if (InCasterUnit.GetCell() != InTarget)
                {
                    InCasterUnit.LookAtCell(InTarget);
                }

                List<ILevelCell> EffectCellList = new();
                EffectCellList.AddRange(GetEffectedCells(InCasterUnit, InTarget));

                if (!EffectCellList.Contains(InTarget))
                {
                    EffectCellList.Add(InTarget);
                }

                GameManager.AddActionBeingPerformed();
                //Debug.Log("Execute AddActionBeingPerformed");

                InCasterUnit.RemoveAbilityPoints(m_ActionPointCost);

                UnitAbilityPlayerData SelectedPlayerData = InCasterUnit.GetUnitAbilityPlayerData(this);

                AnimationClip AbilityAnimation = SelectedPlayerData.AssociatedAnimation;
                if (AbilityAnimation)
                {
                    //Get MonsterSpecialDeBufferType.睡眠_Sleep buff cells

                    if (atk_skillDetail.CheckDeBuffCount(MonsterSpecialDeBufferType.睡眠_Sleep) > 0)
                    {
                        //Debug.Log("Play animation get Debuff 睡眠_Sleep = " + InCasterUnit.name);

                        InCasterUnit.PlayAnimation(AbilityAnimation, SelectedPlayerData.ExecuteAfterTime, false);
                    }
                    else
                    {
                        InCasterUnit.PlayAnimation(AbilityAnimation, SelectedPlayerData.ExecuteAfterTime, true);
                    }
                }

                AudioClip startAudioClip = SelectedPlayerData.AudioOnStart;
                if (startAudioClip)
                {
                    AudioPlayData audioData = new(startAudioClip);
                    AudioHandler.PlayAudio(audioData, InCasterUnit.transform.position);
                }

                yield return new WaitForSeconds(SelectedPlayerData.ExecuteAfterTime);

                AudioClip executeAudioClip = SelectedPlayerData.AudioOnExecute;
                if (executeAudioClip)
                {
                    AudioPlayData audioData = new(executeAudioClip);
                    AudioHandler.PlayAudio(audioData, InCasterUnit.transform.position);
                }

                foreach (ILevelCell EffectCell in EffectCellList)
                {
                    InternalHandleEffectedCell(InCasterUnit, EffectCell, InTarget);
                }

                if (AbilityAnimation)
                {
                    float timeRemaining = (AbilityAnimation.length - SelectedPlayerData.ExecuteAfterTime);
                    timeRemaining = Mathf.Max(0, timeRemaining);

                    yield return new WaitForSeconds(timeRemaining);
                }

                //Debug.Log("Skill Excute ability staminaCost = " + atk_skillDetail.cost);

                //Caculate Skill Delay

                atkParameterInGame.AddSkillDelay(atk_skillDetail.delay);

                //End Caculate Skill Delay

                atkParameterInGame.AddStamina(-atk_skillDetail.consumedST);

                float staminaAfterExcute = atk_skillDetail.consumedST * atk_inGameDeBuffDetail.Stamina_Skill_Correction;
                //Debug.Log("Skill after Excute staminaCost buff = " + staminaAfterExcute);
                atkParameterInGame.AddStamina(-staminaAfterExcute);

                GameManager.RemoveActionBeingPerformed();

                //Debug.Log("Execute RemoveActionBeingPerformed");
            }
        }

        //Debug.Log("Execute OnComplete");

        CustomGameManager.Instance.SetNextTurn();

        OnComplete?.Invoke();

        yield break;
    }

    public void InternalHandlePreExcuteEffectedCell(GridUnit InCasterUnit, ILevelCell InEffectCell)
    {
        Debug.Log("InternalHandlePreExcuteEffectedCell");

        GridObject targetObj = InEffectCell.GetObjectOnCell();
        GridUnit targetExecuteUnit = InEffectCell.GetUnitOnCell();

        if (targetExecuteUnit && InCasterUnit != targetExecuteUnit)
        {
            targetExecuteUnit.LookAtCell(InCasterUnit.GetCell());
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnCaster)
        {
            if (abilityParticle.spawnOnStart && InCasterUnit != null /*&& InEffectCell.GetUnitOnCell() != null*/)
            {
                Vector3 pos = InCasterUnit.GetCell().GetAllignPos(InCasterUnit);
                Quaternion rot = abilityParticle.transform.rotation;
                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(this, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnTarget)
        {
            if (abilityParticle.spawnOnStart && InCasterUnit != null /*&& InEffectCell.GetUnitOnCell() != null*/)
            {
                Vector3 pos = InEffectCell.gameObject.transform.position;

                if (targetObj)
                {
                    pos = InEffectCell.GetAllignPos(targetObj);
                }
                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(this, InCasterUnit, InEffectCell);
            }
        }
    }

    public void InternalHandleEffectedCell(GridUnit InCasterUnit, ILevelCell InEffectCell, ILevelCell SelectedCell)
    {
        GridObject targetObj = InEffectCell.GetObjectOnCell();
        GridUnit targetExecuteUnit = InEffectCell.GetUnitOnCell();

        if (targetExecuteUnit && InCasterUnit != targetExecuteUnit)
        {
            targetExecuteUnit.LookAtCell(InCasterUnit.GetCell());
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnCaster)
        {
            if (abilityParticle.spawnOnStart == false && InCasterUnit != null && InEffectCell.GetUnitOnCell() != null)
            {
                Vector3 pos = InCasterUnit.GetCell().GetAllignPos(InCasterUnit);

                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(this, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnTarget)
        {
            if (abilityParticle.spawnOnStart == false && InCasterUnit != null && InEffectCell.GetUnitOnCell() != null)
            {
                Vector3 pos = InEffectCell.gameObject.transform.position;

                if (targetObj)
                {
                    pos = InEffectCell.GetAllignPos(targetObj);
                }

                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(this, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParam param in m_Params)
        {
            if (targetObj)
            {
                param.ApplyTo(InCasterUnit, targetObj, SelectedCell, this);
            }

            param.ApplyTo(InCasterUnit, InEffectCell, SelectedCell, this);
        }

        foreach (Ailment ailment in m_Ailments)
        {
            if (ailment)
            {
                if (targetExecuteUnit)
                {
                    targetExecuteUnit.GetAilmentContainer().AddAilment(InCasterUnit, ailment);
                }

                CellAilment cellAilment = ailment as CellAilment;
                if (cellAilment)
                {
                    InEffectCell.GetAilmentContainer().AddAilment(InCasterUnit, cellAilment, InEffectCell);
                }
            }
        }
    }

    public float CalculateAbilityTime(GridUnit InUnit)
    {
        UnitAbilityPlayerData SelectedPlayerData = InUnit.GetUnitAbilityPlayerData(this);
        return SelectedPlayerData.AssociatedAnimation ? SelectedPlayerData.AssociatedAnimation.length : 0;
    }
}
