﻿using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.UI.BattleUI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum UnitState
{
    Idle,
    Moving,
    UsingAbility
}

public class GridUnit : GridObject
{
    [SerializeField] private bool AI_activated = false;

    private UnitData m_UnitData;
    private MonsterInGame gameMonster;

    private UnitState m_CurrentState;
    private UnitAbility m_CurrentAbility;

    [SerializeField] private int m_CurrentMovementPoints;
    [SerializeField] private int m_CurrentST;

    private bool m_bIsTarget = false;
    private bool m_bIsMoving = false;
    private bool m_bIsAttacking = false;
    private bool m_bActivated = false;

    private bool m_bIsDead = false;

    private UnityEvent OnMovementComplete = new();

    private List<ILevelCell> m_EditedCells = new();

    [SerializeField]private float moveSpeed = 0f;
    public MonsterInGame GameMonster => gameMonster;

    public override void Initalize()
    {
        base.Initalize();
        gameMonster = GetComponent<MonsterInGame>();
        ParameterInGame inGamePara = gameObject.AddComponent<ParameterInGame>();
        if (inGamePara)
        {
            inGamePara.OnHealthDepleted.AddListener(Kill);
            inGamePara.OnHit.AddListener(HandleHit);
            inGamePara.OnHeal.AddListener(HandleHeal);
        }
        gameMonster.SetGridUnit(this);
        gameMonster.SetGameParameter(inGamePara);
    }

    public override void PostInitalize()
    {
        GetCell().HandleVisibilityChanged();

        //Debug.Log("PostInitalize m_IdleAnimation = " + gameObject.name);
        PlayAnimation(GetUnitData().m_IdleAnimation);
    }

    public bool IsAI() => AI_activated;

    public void SetSpeed(float speedSet) => moveSpeed = speedSet;

    public float GetSpeed() => moveSpeed;

    public void SetAIActive(bool active) => AI_activated = active;

    public virtual void SelectUnit()
    {
        SetupMovement();

        //Debug.Log("SelectUnit SetupMovement");
    }

    public void CleanUp()
    {
        m_CurrentState = UnitState.Idle;

        foreach (ILevelCell cell in m_EditedCells)
        {
            if (cell)
                GameManager.ResetCellState(cell);
        }

        m_EditedCells.Clear();
    }

    public void LookAtCell(ILevelCell InCell)
    {
        if (InCell && ShouldLookAtTargets())
        {
            gameObject.transform.LookAt(GetCellLookAtPos(InCell));
        }
    }

    public void AddAI(UnitAI InAIData)
    {
        if(!gameObject.TryGetComponent(out UnitAIComponent unitAI))
        {
            unitAI = gameObject.AddComponent<UnitAIComponent>();
        }
        unitAI.SetAIData(InAIData);
        gameMonster.SetUnitAI(unitAI);
    }

    public void CheckCellVisibility(ILevelCell InCell)
    {
        if (InCell)
        {
            bool bDead = IsDead();
            bool bCellIsVisible = InCell.IsVisible();

            SetVisible(bCellIsVisible && !bDead);
        }
    }

    public void CheckCellVisibility()
    {
        CheckCellVisibility(GetCell());
    }

    #region Events

    public void BindToOnMovementComplete(UnityAction InAction) => OnMovementComplete.AddListener(InAction);

    public void UnBindFromOnMovementComplete(UnityAction InAction) => OnMovementComplete.RemoveListener(InAction);

    #endregion

    #region Setters

    public void SetUnitData(UnitData InUnitData) => m_UnitData = InUnitData;

    public void SetActivated(bool bInNewActivateState)
    {
        if (m_bActivated == bInNewActivateState) return;
        m_bActivated = bInNewActivateState;
        if (!m_bActivated)return;
        HandleActivation();
    }

    public void SetAsTarget(bool bInIsTarget) => m_bIsTarget = bInIsTarget;

    #endregion

    #region Getters

    public UnitData GetUnitData() => m_UnitData;

    public AilmentContainer GetAilmentContainer()
    {
        AilmentContainer ailmentHandler = GetComponent<AilmentContainer>();

        if (!ailmentHandler)
        {
            ailmentHandler = gameObject.AddComponent<AilmentContainer>();
        }

        return ailmentHandler;
    }

    public UnitState GetCurrentState() => m_CurrentState;

    public void SetCurrentState(UnitState setState)
    {
        m_CurrentState = setState;
    }

    public bool IsMoving() => m_bIsMoving;

    public bool IsAttacking() => m_bIsAttacking;

    public bool IsTarget() => m_bIsTarget;

    public bool IsActivated() => m_bActivated;

    public bool IsFlying() => GetUnitData().m_bIsFlying;

    public bool ShouldLookAtTargets() => GetUnitData().m_bLookAtTargets;

    public bool IsDead() => m_bIsDead;

    public int GetCurrentMovementPoints() => m_CurrentMovementPoints;

    public int GetCurrentST() => m_CurrentST;

    public UnitAbilityPlayerData GetUnitAbilityPlayerData(UnitAbility InAbility)
    {
        for (int i = 0; i < GetUnitData().m_Abilities.Length; i++)
        {
            UnitAbilityPlayerData abilityData = GetUnitData().m_Abilities[i];
            if (abilityData.unitAbility == InAbility)
            {
                return abilityData;
            }
        }

        for (int i = 0; i < GetUnitData().m_SpecAbilities.Length; i++)
        {
            UnitAbilityPlayerData abilityData = GetUnitData().m_SpecAbilities[i];
            if (abilityData.unitAbility == InAbility)
            {
                return abilityData;
            }
        }

        return new();
    }

    public Vector3 GetCellAllignPos(ILevelCell InCell)
    {
        if (!InCell) return Vector3.zero;

        GridObject Obj = InCell.GetObjectOnCell();
        if (Obj) return InCell.GetAllignPos(Obj);

        Vector3 AllignPos = InCell.gameObject.transform.position;
        AllignPos.y = gameObject.transform.position.y;
        return AllignPos;
    }

    Vector3 GetCellLookAtPos(ILevelCell InCell)
    {
        if (!InCell) return Vector3.zero;

        Vector3 allignPos = InCell.GetAllignPos(this);
        allignPos.y = gameObject.transform.position.y;

        return allignPos;
    }

    #endregion

    #region AbilityStuff
    public List<ILevelCell> GetAbilityHoverCells(ILevelCell InCell)
    {
        List<ILevelCell> outCells = new();

        if (GetCurrentState() != UnitState.UsingAbility) return outCells;

        UnitAbility ability = GetCurrentAbility();
        if (!ability) return outCells;

        List<ILevelCell> abilityCells = ability.GetAbilityCells(this);
        List<ILevelCell> effectedCells = ability.GetEffectedCells(this, InCell);

        if (!abilityCells.Contains(InCell)) return outCells;

        for (int i = 0; i < effectedCells.Count; i++)
        {
            ILevelCell currCell = effectedCells[i];
            if (!currCell) continue;

            GameTeam EffectedTeam = (currCell == InCell) ? ability.GetEffectedTeam() : GameTeam.All;

            if (!GameManager.CanCasterEffectTarget(GetCell(), currCell, EffectedTeam, ability.DoesAllowBlocked())) continue;

            outCells.Add(currCell);
        }

        return outCells;
    }

    public List<UnitAbilityPlayerData> GetAbilities() => new(m_UnitData.m_Abilities);

    public List<UnitAbilityPlayerData> GetSpecAbilities() => new(m_UnitData.m_SpecAbilities);

    public UnitAbility GetCurrentAbility() => m_CurrentAbility;

    public void SetCurrentAbility(UnitAbility InAbility) => m_CurrentAbility = InAbility;

    public void SetupAbility(int abilityIndex)
    {
        if (abilityIndex < m_UnitData.m_Abilities.Length)
            SetupAbility(m_UnitData.m_Abilities[abilityIndex].unitAbility);
    }

    public void SetupAbility(UnitAbility InAbility)
    {
        if (IsMoving() || GameManager.IsActionBeingPerformed()) return;

        if (!InAbility) return;

        if (InAbility.GetActionPointCost() > m_CurrentST) return;

        CleanUp();

        m_CurrentAbility = InAbility;
        m_CurrentState = UnitState.UsingAbility;

        List<ILevelCell> EditedAbilityCells = m_CurrentAbility.Setup(this);

        m_EditedCells.AddRange(EditedAbilityCells);

        GameManager.Get().UpdateHoverCells();

        if (InAbility.GetExcuteSeftInSelect())
        {
            ExecuteAbility(GetCell());
        }
    }

    public void SetupAbilityHover(UnitAbility InAbility)
    {
        if (IsMoving() || GameManager.IsActionBeingPerformed()) return;

        if (!InAbility) return;

        if (InAbility.GetActionPointCost() > m_CurrentST) return;

        CleanUp();

        m_CurrentAbility = InAbility;
        m_CurrentState = UnitState.UsingAbility;

        List<ILevelCell> EditedAbilityCells = m_CurrentAbility.Setup(this);

        //Custom excute in selected ability
        if (InAbility.GetExcuteSeftInSelect() && EditedAbilityCells.Count == 0)
        {
            ILevelCell unitCell = GetCell();

            CellState AbilityState = (InAbility.GetEffectedTeam() != CustomGameManager.Instance.GetPlayerTeam()) ? (CellState.eNegative) : (CellState.ePositive);

            GameManager.SetCellState(unitCell, AbilityState);

            m_EditedCells.Add(unitCell);
        }
        //End custom excute in selected ability

        m_EditedCells.AddRange(EditedAbilityCells);

        GameManager.Get().UpdateHoverCells();
    }

    public void ExecuteAbility(ILevelCell InCell)
    {
        if (m_CurrentAbility)
        {
            ExecuteAbility(m_CurrentAbility, InCell);
            m_CurrentAbility = null;
            return;
        }
        CleanUp();
        HandleAbilityFinished();
    }

    public void ExecuteAbility(UnitAbility InAbility, ILevelCell InCell)
    {
        if (!IsMoving() && InAbility)
        {
            m_bIsAttacking = true;

            UnityEvent OnAbilityComplete = new();
            //OnAbilityComplete.AddListener(HandleAbilityFinished); Locked by next turn after ability executed

            OnAbilityComplete.AddListener(SetOutAttack);

            int normalSkillCount = 0;

            foreach (UnitAbilityPlayerData abilityData in GetUnitData().m_Abilities)
            {
                if (abilityData.unitAbility.GetSkillID() == InAbility.GetSkillID())
                {
                    normalSkillCount++;
                }
            }

            if (normalSkillCount > 0)
            {
                StartCoroutine(InAbility.Execute(this, InCell, OnAbilityComplete));
            }
            else
            {
                StartCoroutine(InAbility.ExecuteSpecAbility(this, InCell, OnAbilityComplete));
            }
        }

        CleanUp();
    }

    public void RemoveAbilityPoints(int InAbilityPoints)
    {
        m_CurrentST -= InAbilityPoints;
        if (m_CurrentST < 0)
        {
            m_CurrentST = 0;
        }
    }

    #endregion

    #region MovementStuff

    public List<ILevelCell> GetAllowedMovementCells() => m_UnitData.m_MovementShape.GetCellList(this, GetCell(), m_CurrentMovementPoints, m_UnitData.m_bIsFlying);

    public void SetupMovement()
    {
        if (IsMoving() || GameManager.IsActionBeingPerformed()) return;

        CleanUp();

        if (!m_UnitData.m_MovementShape) return;

        m_CurrentState = UnitState.Moving;
        List<ILevelCell> abilityCells = GetAllowedMovementCells();

        if(abilityCells != null)
        {
            foreach (ILevelCell cell in abilityCells)
            {
                if (cell && cell.IsVisible())
                {
                    GameManager.SetCellState(cell, CellState.eMovement);
                }
            }

            m_EditedCells.AddRange(abilityCells);
        }

        GameManager.Get().UpdateHoverCells();
    }

    public bool ExecuteMovement(ILevelCell TargetCell, UnityEvent InOnMovementComplete)
    {
        if (IsMoving()) return false;

        if (!m_UnitData.m_MovementShape || !TargetCell) return false;

        if (!TargetCell.IsVisible()) return false;

        List<ILevelCell> abilityCells = GetAllowedMovementCells();
        if (!abilityCells.Contains(TargetCell)) return false;

        InOnMovementComplete.AddListener(HandleMovementFinished);
        TraverseTo(TargetCell, InOnMovementComplete, abilityCells);

        GameManager.CheckWinConditions();

        CleanUp();
        return true;
    }

    public bool ExecuteMovement(ILevelCell TargetCell)
    {
        UnityEvent OnMovementComplete = new();
        return ExecuteMovement(TargetCell, OnMovementComplete);
    }

    public void TraverseTo(ILevelCell InTargetCell, UnityEvent OnMovementComplete = null, List<ILevelCell> InAllowedCells = null) => StartCoroutine(EnumeratorTraverseTo(InTargetCell, OnMovementComplete, InAllowedCells));

    public void MoveTo(ILevelCell InTargetCell, UnityEvent OnMovementComplete = null) => StartCoroutine(InternalMoveTo(InTargetCell, OnMovementComplete));

    public IEnumerator EnumeratorTraverseTo(ILevelCell InTargetCell, UnityEvent OnMovementComplete = null, List<ILevelCell> InAllowedCells = null)
    {
        if (!InTargetCell) yield break;

        m_bIsMoving = true;

        //Debug.Log("EnumeratorTraverseTo m_MovementAnimation");
        PlayAnimation(GetUnitData().m_MovementAnimation);

        GameManager.AddActionBeingPerformed();
        //Debug.Log("EnumeratorTraverseTo AddActionBeingPerformed");

        List<ILevelCell> cellPath = GetPathTo(InTargetCell, InAllowedCells);

        if (cellPath.Count > 1) cellPath.RemoveAt(0);

        if (cellPath.Count > 1) cellPath[^1].SetBlocked(true);

        Vector3 StartPos = GetCell().GetAllignPos(this);

        int MovementCount = 0;

        ILevelCell FinalCell = InTargetCell;

        ILevelCell StartingCell = GetCell();

        ILevelCell previousCell = StartingCell;

        foreach (ILevelCell cell in cellPath)
        {
            FogOfWar fogOfWar = GameManager.GetFogOfWar();
            if (fogOfWar)
                if (GetTeam() == GameTeam.Blue)
                    fogOfWar.CheckPoint(cell);
                else
                    CheckCellVisibility(cell);

            float TimeTo = 0;
            Vector3 EndPos = cell.GetAllignPos(this);
            while (TimeTo < 1.5f)
            {
                float speed = 0f;

                if (moveSpeed == 0f)
                    speed = AIManager.GetMovementSpeed();
                else
                    speed = moveSpeed;

                TimeTo += Time.deltaTime * speed;
                gameObject.transform.position = Vector3.MoveTowards(StartPos, EndPos, TimeTo);

                LookAtCell(cell);

                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }

            gameObject.transform.position = EndPos;
            StartPos = cell.GetAllignPos(this);
            yield return new WaitForSeconds(AIManager.GetWaitTime());

            if (cell != previousCell)
            {
                SetCurrentCell(cell);
                cell.SetMaterial(previousCell.GetCellState());

                previousCell.SetMaterial(previousCell.GetCellState());
                previousCell = cell;

                AilmentHandler.HandleUnitOnCell(this, cell);
                PlayTravelAudio();
            }

            if (IsDead())
            {
                break;
            }

            if (MovementCount++ >= m_CurrentMovementPoints)
            {
                FinalCell = cell;
                break;
            }
        }

        if (!IsDead())
        {
            SetCurrentCell(FinalCell);
            //RemoveMovementPoints(cellPath.Count - 1);
            RemoveMovementPoints(cellPath.Count);

            Debug.Log("EnumeratorTraverseTo m_IdleAnimation");
            PlayAnimation(GetUnitData().m_IdleAnimation);
        }

        GameManager.RemoveActionBeingPerformed();
        //Debug.Log("EnumeratorTraverseTo RemoveActionBeingPerformed");

        m_bIsMoving = false;

        if (cellPath.Count > 1)
        {
            cellPath[^1].SetBlocked(false);
        }

        //Debug.Log("EnumeratorTraverseTo OnMovementComplete");

        OnMovementComplete?.Invoke();

        yield break;
    }

    public List<ILevelCell> GetPathTo(ILevelCell InTargetCell, List<ILevelCell> InAllowedCells = null)
    {
        AIPathInfo pathInfo = new()
        {
            StartCell = GetCell(),
            TargetCell = InTargetCell,
            bIgnoreUnits = true,
            bTakeWeightIntoAccount = GameManager.IsTeamAI(GetTeam()),
            AllowedCells = InAllowedCells,
            bAllowBlocked = m_UnitData.m_bIsFlying
        };

        List<ILevelCell> cellPath = AIManager.GetPath(pathInfo);

        return cellPath;
    }

    public List<ILevelCell> GetEffectPathTo(ILevelCell startCell, ILevelCell InTargetCell, List<ILevelCell> InAllowedCells = null)
    {
        AIPathInfo pathInfo = new()
        {
            StartCell = startCell,
            TargetCell = InTargetCell,
            bIgnoreUnits = true,
            bTakeWeightIntoAccount = GameManager.IsTeamAI(GetTeam()),
            AllowedCells = InAllowedCells,
            bAllowBlocked = m_UnitData.m_bIsFlying
        };

        List<ILevelCell> cellPath = AIManager.GetPath(pathInfo);

        return cellPath;
    }

    IEnumerator InternalMoveTo(ILevelCell InTargetCell, UnityEvent OnMovementComplete = null)
    {
        if (InTargetCell)
        {
            GameManager.AddActionBeingPerformed();
            Debug.Log("InternalMoveTo AddActionBeingPerformed");

            AIPathInfo pathInfo = new()
            {
                StartCell = GetCell(),
                TargetCell = InTargetCell,
                bIgnoreUnits = true,
                bTakeWeightIntoAccount = false
            };

            List<ILevelCell> cellPath = AIManager.GetPath(pathInfo);

            if (cellPath.Count > 1)
            {
                cellPath.RemoveAt(0);
            }

            ILevelCell StartingCell = GetCell();

            Vector3 StartPos = GetCell().GetAllignPos(this);

            SetCurrentCell(InTargetCell);

            ILevelCell previousCell = StartingCell;

            foreach (ILevelCell cell in cellPath)
            {
                FogOfWar fogOfWar = GameManager.GetFogOfWar();
                if (fogOfWar)
                {
                    if (GetTeam() == GameTeam.Blue)
                    {
                        fogOfWar.CheckPoint(cell);
                    }
                    else
                    {
                        CheckCellVisibility(cell);
                    }
                }

                float TimeTo = 0;
                Vector3 EndPos = cell.GetAllignPos(this);
                while (TimeTo < 1.5f)
                {
                    float speed = 0f;

                    if (moveSpeed == 0f)
                    {
                        speed = AIManager.GetMovementSpeed();
                    }
                    else
                    {
                        speed = moveSpeed;
                    }

                    TimeTo += Time.deltaTime * speed;
                    gameObject.transform.position = Vector3.MoveTowards(StartPos, EndPos, TimeTo);
                    yield return new WaitForSeconds(0.00001f);
                }

                gameObject.transform.position = EndPos;
                StartPos = cell.GetAllignPos(this);
                yield return new WaitForSeconds(AIManager.GetWaitTime());

                if (cell != previousCell)
                {
                    SetCurrentCell(cell);
                    cell.SetMaterial(previousCell.GetCellState());

                    previousCell.SetMaterial(previousCell.GetCellState());
                    previousCell = cell;

                    AilmentHandler.HandleUnitOnCell(this, cell);
                }
            }

            OnMovementComplete?.Invoke();

            GameManager.RemoveActionBeingPerformed();

            Debug.Log("InternalMoveTo RemoveActionBeingPerformed");
        }

        yield break;
    }

    public void RemoveMovementPoints(int InMoveCount)
    {
        m_CurrentMovementPoints -= InMoveCount;
        if (m_CurrentMovementPoints < 0)
        {
            m_CurrentMovementPoints = 0;
        }

        GameManager.GetRules().OnMovePosition(this, InMoveCount);
    }

    #endregion

    #region Animation

    int CurrPlayClipID = 0;

    Dictionary<int, AnimationClip> m_ClipIdToAnim = new();
    public void PlayAnimation(AnimationClip InClip, bool bInPlayIdleAfter = false)
    {
        if (InClip)
        {
            if (m_ClipIdToAnim.ContainsKey(CurrPlayClipID))
            {
                if (m_ClipIdToAnim[CurrPlayClipID] == InClip)
                {
                    return;
                }
            }

            Animator animator = GetComponent<Animator>();
            if (animator)
            {
                animator.Play(InClip.name);
            }

            if (bInPlayIdleAfter)
            {
                StartCoroutine(PlayClipAfterTime(GetUnitData().m_IdleAnimation, InClip.length, ++CurrPlayClipID));
            }
        }
    }

    public void PlayAnimation(AnimationClip InClip, float customLeght, bool bInPlayIdleAfter = false)
    {
        if (InClip)
        {
            if (m_ClipIdToAnim.ContainsKey(CurrPlayClipID))
            {
                if (m_ClipIdToAnim[CurrPlayClipID] == InClip)
                {
                    return;
                }
            }

            Animator animator = GetComponent<Animator>();
            if (animator)
            {
                animator.Play(InClip.name);
            }

            if (bInPlayIdleAfter)
            {
                StartCoroutine(PlayClipAfterTime(GetUnitData().m_IdleAnimation, customLeght, ++CurrPlayClipID));
            }
        }
    }

    IEnumerator PlayClipAfterTime(AnimationClip InClip, float InTime, int PlayClipID)
    {
        //Debug.Log("Play animation clip = " + InClip.name);

        //ParameterInGame parameterInGame = GetComponent<ParameterInGame>();

        //if (parameterInGame != null)
        //{
        //    //Get MonsterSpecialDeBufferType.睡眠_Sleep buff cells

        //    if (parameterInGame.CheckDeBuffCount(MonsterSpecialDeBufferType.睡眠_Sleep) > 0)
        //    {
        //        Debug.Log("Play animation get Debuff 睡眠_Sleep = " + gameObject.name);

        //        yield break;
        //    }
        //}

        m_ClipIdToAnim.Add(PlayClipID, InClip);

        yield return new WaitForSeconds(InTime);

        m_ClipIdToAnim.Remove(PlayClipID);

        if (CurrPlayClipID == PlayClipID)
        {
            if (InClip)
            {
                Animator animator = GetComponent<Animator>();
                if (animator)
                {
                    AnimationClip MovementClip = GetUnitData().m_MovementAnimation;
                    if (IsMoving() && MovementClip)
                    {
                        animator.Play(GetUnitData().m_MovementAnimation.name);
                    }
                    else
                    {
                        animator.Play(InClip.name);
                    }

                }
            }
        }

        yield break;
    }

    #endregion

    #region EventListeners

    public virtual void HandleTurnStarted()
    {
        //m_CurrentMovementPoints = m_UnitData.m_MovementPoints;
        //m_CurrentAbilityPoints = m_UnitData.m_AbilityPoints;
    }

    public void SetMovementPoints(int movementPoints) => m_CurrentMovementPoints = movementPoints;

    public void SetAbilityPoints(int abilityPoints)
    {
        m_CurrentST = abilityPoints;

        GameManager.GetRules().HandleSTChanged(abilityPoints);
    }

    public void SetOutAttack()
    {
        m_bIsAttacking = false;
    }

    public void HandleAbilityFinished()
    {
        m_bIsAttacking = false;

        GameTeam team = GameManager.GetUnitTeam(this);
        if ( /*GameManager.IsTeamHuman(team) &&*/ GameManager.IsPlaying() && !IsDead() && !IsAI())
        {
            Debug.Log("HandleAbilityFinished SetupMovement team = " + team.ToString());
            UIBattleMonsterStatsContainer.OnActionPerformed?.Invoke();
            SetupMovement();
        }
    }

    private void HandleMovementFinished()
    {
        if (GameManager.IsPlaying() && !IsDead())
        {
            Debug.Log("HandleMovementFinished SetupMovement");

            SetupMovement();
        }

        OnMovementComplete.Invoke();
    }

    public void Kill()
    {
        CleanUp();

        GameManager.GetRules().UnitDead(this);

        ILevelCell tempCell = m_CurrentCell;

        if (m_CurrentCell)
        {
            Debug.Log("Set dead unit cell to null");

            m_CurrentCell.SetObjectOnCell(null);

            m_CurrentCell = null;
        }

        m_bIsDead = true;


        //Debug.Log("hide player after dead");
        //SetVisible( false );
        //Debug.Log("Kill m_DeadAnimation");
        PlayAnimation(GetUnitData().m_DeadAnimation);

        CheckCellVisibility();

        HandleDeath();

        AudioClip clip = GetUnitData().m_DeathSound;
        if (clip)
        {
            AudioPlayData audioData = new(clip);
            AudioHandler.PlayAudio(audioData, gameObject.transform.position);
        }

        if (GameManager.IsActionBeingPerformed())
        {
            GameManager.BindToOnFinishedPerformedActions(DestroyObj);
        }
        else
        {
            DestroyObj();
        }

        if (tempCell != null)
        {
            tempCell.SetMaterial(tempCell.GetCellState());
        }
    }

    protected virtual void HandleDeath()
    {
        gameObject.transform.DOMoveY(-1, 3).SetEase(Ease.OutBack).SetDelay(3);
        gameObject.transform.DOScale(Vector3.zero, 3).SetDelay(3).SetEase(Ease.Linear);
        //Debug.Log("dead unit = " + gameObject.name);
        GameManager.HandleUnitDeath(this);
    }

    private void DestroyObj()
    {
        GameManager.UnBindFromOnFinishedPerformedActions(DestroyObj);
        //Destroy(gameObject);

        Debug.Log("Destroy grid Unit");
    }

    private void HandleHit()
    {
        bool bShowHitAnimationOnMove = GameManager.GetRules().GetGameplayData().bShowHitAnimOnMove;
        if (!IsMoving() || bShowHitAnimationOnMove)
        {
            if (IsDead() == false)
            {
                //PlayAnimation(GetUnitData().m_DamagedAnimation, true);

                //Debug.Log("Damage animation locked");
            }
        }

        PlayDamagedAudio();
    }

    private void HandleHeal()
    {
        //Debug.Log("HandleHeal m_HealAnimation");
        //PlayAnimation(GetUnitData().m_HealAnimation, true);

        Debug.Log("m_HealAnimation Blocked");

        PlayHealAudio();

        AbilityParticle[] particles = GetUnitData().m_SpawnOnHeal;
        foreach (AbilityParticle particle in particles)
        {
            if (particle)
            {
                Vector3 pos = GetCell().GetAllignPos(this);

                AbilityParticle CreatedAbilityParticle = Instantiate(particle.gameObject, pos, GetCell().transform.rotation).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(null, this, GetCell());
            }
        }
        UIBattleMonsterStatsContainer.OnHealthChanged?.Invoke(GameMonster.GetBattleID());
    }

    private void PlayDamagedAudio()
    {
        AudioClip clip = GetUnitData().m_DamagedSound;
        if (clip)
        {
            AudioPlayData audioData = new(clip);
            AudioHandler.PlayAudio(audioData, gameObject.transform.position);
        }
    }

    private void PlayHealAudio()
    {
        AudioClip clip = GetUnitData().m_HealSound;
        if (clip)
        {
            AudioPlayData audioData = new(clip);
            AudioHandler.PlayAudio(audioData, gameObject.transform.position);
        }
    }

    public void PlayTravelAudio()
    {
        AudioClip clip = GetUnitData().m_TravelSound;
        if (clip)
        {
            AudioPlayData audioData = new(clip);
            AudioHandler.PlayAudio(audioData, gameObject.transform.position);
        }
    }

    private void HandleActivation()
    {
        GameManager.HandleUnitActivated(this);
    }

    #endregion

}
