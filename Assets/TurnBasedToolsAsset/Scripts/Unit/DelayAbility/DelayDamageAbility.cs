using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "DelayDamageAbility", menuName = "TurnBasedTools/Delay Ability/Create New DelayDamageAbility", order = 1)]
public class DelayDamageAbility : DelayAbility
{
    float executeTime = 0f;

    public override IEnumerator Execute(UnitAbility unitAbility, GridUnit InCasterUnit, GridUnit TargetUnit, ILevelCell SelectedCell, UnityEvent OnComplete = null)
    {
        InternalHandlePreExcuteEffectedCell(unitAbility, InCasterUnit, TargetUnit.GetCell());

        yield return new WaitForSeconds(executeTime);

        InternalHandleEffectedCell(unitAbility, InCasterUnit, TargetUnit.GetCell(), SelectedCell);

        yield break;
    }
}
