﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class DelayAbility : ScriptableObject
{
    [System.Serializable]
    public class DelayAbilityInfo
    {
        public int fromTurn = 0;

        public GridUnit targetUnit = null;

        public ILevelCell selectedCell = null;

        public UnitAbility unitAbility = null;

        public DelayAbility delayAbility = null;
    }

    //delay turn = 0 mean that it will be active next turn
    public int delayTurn = 1;

    [SerializeField]
    AbilityParticle[] m_SpawnOnCaster;

    [SerializeField]
    AbilityParticle[] m_SpawnOnTarget;

    [SerializeField]
    AbilityParam[] m_Params;

    public virtual IEnumerator Execute(UnitAbility unitAbility, GridUnit InCasterUnit, GridUnit TargetUnit, ILevelCell SelectedCell, UnityEvent OnComplete = null)
    {
        yield break;
    }

    public void InternalHandlePreExcuteEffectedCell(UnitAbility unitAbility, GridUnit InCasterUnit, ILevelCell InEffectCell)
    {
        Debug.Log("DelayAbility InternalHandlePreExcuteEffectedCell");

        GridObject targetObj = InEffectCell.GetObjectOnCell();
        GridUnit targetExecuteUnit = InEffectCell.GetUnitOnCell();

        if (targetExecuteUnit && InCasterUnit != targetExecuteUnit)
        {
            targetExecuteUnit.LookAtCell(InCasterUnit.GetCell());
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnCaster)
        {
            if (abilityParticle.spawnOnStart && InCasterUnit != null /*&& InEffectCell.GetUnitOnCell() != null*/)
            {
                Vector3 pos = InCasterUnit.GetCell().GetAllignPos(InCasterUnit);
                Quaternion rot = abilityParticle.transform.rotation;
                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(unitAbility, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnTarget)
        {
            if (abilityParticle.spawnOnStart && InCasterUnit != null /*&& InEffectCell.GetUnitOnCell() != null*/)
            {
                Vector3 pos = InEffectCell.gameObject.transform.position;

                if (targetObj)
                {
                    pos = InEffectCell.GetAllignPos(targetObj);
                }
                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(unitAbility, InCasterUnit, InEffectCell);
            }
        }
    }

    public void InternalHandleEffectedCell(UnitAbility unitAbility, GridUnit InCasterUnit, ILevelCell InEffectCell, ILevelCell SelectedCell)
    {
        GridObject targetObj = InEffectCell.GetObjectOnCell();
        GridUnit targetExecuteUnit = InEffectCell.GetUnitOnCell();

        if (targetExecuteUnit && InCasterUnit != targetExecuteUnit)
        {
            targetExecuteUnit.LookAtCell(InCasterUnit.GetCell());
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnCaster)
        {
            if (abilityParticle.spawnOnStart == false && InCasterUnit != null && InEffectCell.GetUnitOnCell() != null)
            {
                Vector3 pos = InCasterUnit.GetCell().GetAllignPos(InCasterUnit);

                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(unitAbility, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParticle abilityParticle in m_SpawnOnTarget)
        {
            if (abilityParticle.spawnOnStart == false && InCasterUnit != null && InEffectCell.GetUnitOnCell() != null)
            {
                Vector3 pos = InEffectCell.gameObject.transform.position;

                if (targetObj)
                {
                    pos = InEffectCell.GetAllignPos(targetObj);
                }

                Quaternion rot = abilityParticle.transform.rotation;

                AbilityParticle CreatedAbilityParticle = Instantiate(abilityParticle.gameObject, pos, rot).GetComponent<AbilityParticle>();
                CreatedAbilityParticle.Setup(unitAbility, InCasterUnit, InEffectCell);
            }
        }

        foreach (AbilityParam param in m_Params)
        {
            if (targetObj)
            {
                param.ApplyTo(InCasterUnit, targetObj, SelectedCell, unitAbility);
            }

            param.ApplyTo(InCasterUnit, InEffectCell, SelectedCell, unitAbility);
        }
    }
}
