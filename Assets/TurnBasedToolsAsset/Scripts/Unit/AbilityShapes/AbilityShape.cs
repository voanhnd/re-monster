﻿using System.Collections.Generic;
using UnityEngine;

public class AbilityShape : ScriptableObject
{
    public virtual List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked = true, GameTeam m_EffectedTeam = GameTeam.None)
    {
        return new List<ILevelCell>();
    }

    public List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InRange > 0)
        {
            ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

            int Count = 0;
            while (CurserCell)
            {
                if (CurserCell.IsBlocked() && !bAllowBlocked)
                {
                    break;
                }

                GridObject gridObj = CurserCell.GetObjectOnCell();
                if (gridObj)
                {
                    if (m_EffectedTeam == GameTeam.None)
                    {
                        break;
                    }

                    GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                    if (ObjAffinity == GameTeam.Blue && m_EffectedTeam == GameTeam.Red)
                    {
                        break;
                    }
                }

                cells.Add(CurserCell);
                CurserCell = CurserCell.GetAdjacentCell(Dir);
                Count++;
                if (InRange != -1 && Count >= InRange)
                {
                    break;
                }
            }
        }

        return cells;
    }
}
