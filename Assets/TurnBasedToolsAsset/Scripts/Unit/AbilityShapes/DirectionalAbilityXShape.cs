using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DirectionalAbilityXShape", menuName = "TurnBasedTools/Ability/Shapes/Create DirectionalAbilityXShape", order = 1)]
public class DirectionalAbilityXShape : AbilityShape
{
    [SerializeField]
    bool m_bOnlyMyEnemies;

    public override List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        Vector3 targetPos = new Vector3(InCaster.transform.position.x, InCell.transform.position.y, InCaster.transform.position.z);

        Vector3 targetDir = targetPos - InCell.transform.position;

        ////180 to -180 angle
        float angle = Vector3.SignedAngle(targetDir.normalized, new Vector3(0f, 0f, 1f), Vector3.down);
        //                                //Look direction     /Look vector             /Rotate vector

        CompassDir compassDir = ParameterInGame.GetHexFaceTo(Quaternion.Euler(0f, angle, 0f).eulerAngles.y);

        if (InCell && InRange > 0)
        {
            switch (compassDir)
            {
                case CompassDir.NW:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
                        break;
                    }

                case CompassDir.NE:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
                        break;
                    }

                case CompassDir.E:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
                        break;
                    }

                case CompassDir.SE:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
                        break;
                    }

                case CompassDir.SW:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
                        break;
                    }

                case CompassDir.W:
                    {
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
                        cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
                        break;
                    }
            }

            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.N, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.S, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
            //cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
        }

        if (m_bOnlyMyEnemies)
        {
            List<ILevelCell> enemyCells = new List<ILevelCell>();
            foreach (var currCell in cells)
            {
                GridUnit unitOnCell = currCell.GetUnitOnCell();
                if (unitOnCell)
                {
                    GameTeam AffinityToCaster = GameManager.GetTeamAffinity(InCaster.GetTeam(), unitOnCell.GetTeam());
                    if (AffinityToCaster == GameTeam.Red)
                    {
                        enemyCells.Add(currCell);
                    }
                }
            }

            return enemyCells;
        }
        else
        {
            return cells;
        }
    }
}
