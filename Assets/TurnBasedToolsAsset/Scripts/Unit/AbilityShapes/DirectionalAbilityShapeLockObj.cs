using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DirectionalAbilityShapeLockObj", menuName = "TurnBasedTools/Ability/Shapes/Create DirectionalAbilityShapeLockObj", order = 1)]
public class DirectionalAbilityShapeLockObj : AbilityShape
{
    [SerializeField]
    bool m_bOnlyMyEnemies;

    public override List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new();

        if (InCell && InRange > 0)
        {
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.N, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.S, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
        }

        //if (InCaster.GetCurrentState() == UnitState.UsingAbility)
        //{
        //    List<GridUnit> turnListUnit = turnListUnit;

        //    foreach (ILevelCell levelCell in cells)
        //    {

        //    }
        //}

        if (m_bOnlyMyEnemies)
        {
            List<ILevelCell> enemyCells = new List<ILevelCell>();
            foreach (var currCell in cells)
            {
                GridUnit unitOnCell = currCell.GetUnitOnCell();
                if (unitOnCell)
                {
                    GameTeam AffinityToCaster = GameManager.GetTeamAffinity(InCaster.GetTeam(), unitOnCell.GetTeam());
                    if (AffinityToCaster == GameTeam.Red)
                    {
                        enemyCells.Add(currCell);
                    }
                }
            }

            return enemyCells;
        }
        else
        {
            return cells;
        }
    }

    private new List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new();

        if (InRange > 0)
        {
            ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

            int Count = 0;
            try
            {
                while (CurserCell)
                {
                    if (CurserCell.IsBlocked() && !bAllowBlocked)
                    {
                        break;
                    }

                    GridObject gridObj = CurserCell.GetObjectOnCell();
                    if (gridObj)
                    {
                        if (m_EffectedTeam == GameTeam.None)
                        {
                            break;
                        }

                        GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                        if (ObjAffinity == CustomGameManager.Instance.GetPlayerTeam() && m_EffectedTeam != CustomGameManager.Instance.GetPlayerTeam())
                        {
                            break;
                        }
                    }

                    cells.Add(CurserCell);

                    if (CurserCell.GetUnitOnCell())
                    {
                        break;
                    }

                    CurserCell = CurserCell.GetAdjacentCell(Dir);
                    Count++;
                    if (InRange != -1 && Count >= InRange)
                    {
                        break;
                    }
                }
            }
            catch { }
        }

        return cells;
    }
}
