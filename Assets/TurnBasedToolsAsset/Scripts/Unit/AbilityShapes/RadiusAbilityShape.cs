﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAbilityShape", menuName = "TurnBasedTools/Ability/Shapes/Create RadiusAbilityShape", order = 1)]
public class RadiusAbilityShape : AbilityShape
{
    [SerializeField]
    bool m_bStopAtBlocked = true;

    [SerializeField]
    bool m_bOnlyMyEnemies;

    public override List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        if (InCaster == null)
        {
            Debug.Log("InCaster null");

            return null;
        }

        if (InCell == null)
        {
            Debug.Log("InCell null");

            return null;
        }

        GridUnit Caster = InCell.GetUnitOnCell();

        AIRadiusInfo radiusInfo = new(InCell, InRange)
        {
            Caster = Caster,
            bAllowBlocked = bAllowBlocked,
            bStopAtBlockedCell = m_bStopAtBlocked,
            EffectedTeam = m_EffectedTeam
        };

        List<ILevelCell> radCells = AIManager.GetRadius(radiusInfo);

        if (m_bOnlyMyEnemies)
        {
            List<ILevelCell> enemyCells = new();
            for (int i = 0; i < radCells.Count; i++)
            {
                ILevelCell currCell = radCells[i];
                GridUnit unitOnCell = currCell.GetUnitOnCell();
                if (!unitOnCell)
                    continue;
                GameTeam AffinityToCaster = GameManager.GetTeamAffinity(InCaster.GetTeam(), unitOnCell.GetTeam());
                if (AffinityToCaster == GameTeam.Red)
                {
                    enemyCells.Add(currCell);
                }
            }

            return enemyCells;
        }
        else
        {
            return AIManager.GetRadius(radiusInfo);
        }
    }
}
