using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RadiusAbilityShapeFromSeft", menuName = "TurnBasedTools/Ability/Shapes/Create RadiusAbilityShapeFromSeft", order = 1)]
public class RadiusAbilityShapeFromSeft : AbilityShape
{
    [SerializeField]
    bool m_bStopAtBlocked = true;

    [SerializeField]
    bool m_bOnlyMyEnemies;

    public override List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        if (InCaster == null)
        {
            Debug.Log("InCaster null");

            return null;
        }

        AIRadiusInfo radiusInfo = new AIRadiusInfo(InCaster.GetCell(), InRange);
        radiusInfo.Caster = InCaster;
        radiusInfo.bAllowBlocked = bAllowBlocked;
        radiusInfo.bStopAtBlockedCell = m_bStopAtBlocked;
        radiusInfo.EffectedTeam = m_EffectedTeam;

        List<ILevelCell> radCells = AIManager.GetRadius(radiusInfo);

        if (m_bOnlyMyEnemies)
        {
            List<ILevelCell> enemyCells = new List<ILevelCell>();
            foreach (var currCell in radCells)
            {
                GridUnit unitOnCell = currCell.GetUnitOnCell();
                if (unitOnCell)
                {
                    GameTeam AffinityToCaster = GameManager.GetTeamAffinity(InCaster.GetTeam(), unitOnCell.GetTeam());
                    if (AffinityToCaster == GameTeam.Red)
                    {
                        enemyCells.Add(currCell);
                    }
                }
            }

            return enemyCells;
        }
        else
        {
            return AIManager.GetRadius(radiusInfo);
        }
    }
}
