using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LineAbilityShape", menuName = "TurnBasedTools/Ability/Shapes/Create LineAbilityShape", order = 1)]
public class LineAbilityShape : AbilityShape
{
    [SerializeField]
    bool m_bOnlyMyEnemies;

    public override List<ILevelCell> GetCellList(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        //CompassDir compassDir = ParameterInGame.GetFaceTo(InCaster.transform.localEulerAngles.y);

        if (InCell && InRange > 0)
        {
            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.NE, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }

            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.E, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }

            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.SE, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }

            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.NW, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }

            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.W, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }

            if (cells.Count == 0)
            {
                List<ILevelCell> tempCells = new List<ILevelCell>();

                tempCells.AddRange(GetCellsInDirection(InCaster.GetCell(), InRange, CompassDir.SW, true, m_EffectedTeam));

                if (tempCells.Contains(InCell))
                {
                    cells = GetTrueCells(InCell, tempCells);
                }
            }
        }

        if (m_bOnlyMyEnemies)
        {
            List<ILevelCell> enemyCells = new List<ILevelCell>();
            foreach (var currCell in cells)
            {
                GridUnit unitOnCell = currCell.GetUnitOnCell();
                if (unitOnCell)
                {
                    GameTeam AffinityToCaster = GameManager.GetTeamAffinity(InCaster.GetTeam(), unitOnCell.GetTeam());
                    if (AffinityToCaster == GameTeam.Red)
                    {
                        enemyCells.Add(currCell);
                    }
                }
            }

            return enemyCells;
        }
        else
        {
            return cells;
        }
    }

    public List<ILevelCell> GetTrueCells(ILevelCell inCell, List<ILevelCell> cells)
    {
        List<ILevelCell> trueCells = new List<ILevelCell>();

        foreach (ILevelCell levelCell in cells)
        {
            trueCells.Add(levelCell);

            if (levelCell == inCell)
            {
                break;
            }
        }

        return trueCells;
    }

    List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InRange > 0)
        {
            ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

            int Count = 0;
            while (CurserCell)
            {
                if (CurserCell.IsBlocked() && !bAllowBlocked)
                {
                    break;
                }

                GridObject gridObj = CurserCell.GetObjectOnCell();
                if (gridObj)
                {
                    if (m_EffectedTeam == GameTeam.None)
                    {
                        break;
                    }

                    GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                    if (ObjAffinity == GameTeam.Blue && m_EffectedTeam == GameTeam.Red)
                    {
                        break;
                    }
                }

                cells.Add(CurserCell);
                CurserCell = CurserCell.GetAdjacentCell(Dir);
                Count++;
                if (InRange != -1 && Count >= InRange)
                {
                    break;
                }
            }
        }

        return cells;
    }
}
