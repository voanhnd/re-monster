﻿using UnityEngine;

public class AbilityParam : ScriptableObject
{
    public virtual void ApplyTo(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility)
    {

    }

    public virtual void ApplyTo(GridUnit InCaster, ILevelCell InCell, ILevelCell selectedCell, UnitAbility unitAbility)
    {

    }

    public virtual string GetAbilityInfo()
    {
        return name;
    }

    public static bool IsEffectTeam(GridUnit InCaster, GridObject InObject, UnitAbility unitAbility)
    {
        if (unitAbility.GetEffectedTeam() == GameTeam.Red && InCaster.GetTeam() != InObject.GetTeam())
        {
            return true;
        }
        else
        if (unitAbility.GetEffectedTeam() == GameTeam.Blue && InCaster.GetTeam() == InObject.GetTeam())
        {
            return true;
        }
        else
        if (unitAbility.GetEffectedTeam() == GameTeam.All)
        {
            return true;
        }

        Debug.Log("IsEffectTeam false");

        return false;
    }

    public static bool IsEffectTeam(GridUnit InCaster, GridUnit InTarget, UnitAbility unitAbility)
    {
        if (unitAbility.GetEffectedTeam() == GameTeam.Red && InCaster.GetTeam() != InTarget.GetTeam())
        {
            return true;
        }
        else
        if (unitAbility.GetEffectedTeam() == GameTeam.Blue && InCaster.GetTeam() == InTarget.GetTeam())
        {
            return true;
        }
        else
        if (unitAbility.GetEffectedTeam() == GameTeam.All)
        {
            return true;
        }

        return false;
    }
}
