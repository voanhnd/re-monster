﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewDamageAbilityParam", menuName = "TurnBasedTools/Ability/Parameters/ Create DamageAbilityParam", order = 1)]
public class DamageAbilityParam : AbilityParam
{
    public int m_Damage;

    public override void ApplyTo(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility)
    {
        ParameterInGame parameterInGame = InObject.GetComponent<ParameterInGame>();
        if (parameterInGame)
        {
            parameterInGame.Damage(m_Damage);
        }
    }

    public override string GetAbilityInfo()
    {
        return "Damage" + m_Damage.ToString();
    }
}
