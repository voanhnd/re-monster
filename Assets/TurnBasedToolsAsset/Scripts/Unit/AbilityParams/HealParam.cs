﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewHealAbilityParam", menuName = "TurnBasedTools/Ability/Parameters/ Create HealAbilityParam", order = 1)]
public class HealParam : AbilityParam
{
    public int m_HealAmount;

    public override void ApplyTo(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility)
    {
        if (InObject)
        {
            ParameterInGame parameterInGame = InObject.GetComponent<ParameterInGame>();
            if (parameterInGame)
            {
                parameterInGame.Heal_Health(m_HealAmount);
            }
        }
    }

    public override string GetAbilityInfo()
    {
        return "Heal Target by: " + m_HealAmount;
    }
}
