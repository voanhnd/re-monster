﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPushParam", menuName = "TurnBasedTools/Ability/Parameters/ Create PushAbilityParam", order = 1)]
public class PushAbilityParam : AbilityParam
{
    public int m_Distance;

    public override void ApplyTo(GridUnit InCaster, ILevelCell InCell, ILevelCell selectedCell, UnitAbility unitAbility)
    {
        Debug.Log("InCell = " + InCell.name);

        GridUnit TargetUnit = InCell.GetUnitOnCell();

        //Check team
        if (!IsEffectTeam(InCaster, TargetUnit, unitAbility))
        {
            return;
        }
        //

        if (TargetUnit && InCaster)
        {
            //CompassDir PushDirection = InCaster.GetCell().GetDirectionToAdjacentCell( InCell );

            ILevelCell targetCell = TargetUnit.GetCell();
            Debug.Log("targetCell = " + targetCell.name);

            CompassDir PushDirection = GetDirectionToTarget(InCaster, InCaster.GetCell(), unitAbility.GetRadius(), unitAbility.DoesAllowBlocked(), unitAbility.GetEffectedTeam(), targetCell);
            Debug.Log("Push dir = " + PushDirection.ToString());

            ILevelCell dirCell = targetCell;

            Debug.Log("m_Distance = " + m_Distance);

            for (int i = 0; i < m_Distance; i++)
            {
                if (dirCell)
                {
                    Debug.Log("dirCell = " + dirCell.name);

                    dirCell = dirCell.GetAdjacentCell(PushDirection);
                }

                if (dirCell && dirCell.IsCellAccesible())
                {
                    targetCell = dirCell;
                }
            }

            TargetUnit.MoveTo(targetCell);
        }
    }

    public override string GetAbilityInfo()
    {
        return "Push Back: " + m_Distance.ToString() + " Space" + ((m_Distance > 1) ? "s" : "");
    }

    CompassDir GetDirectionToTarget(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam, ILevelCell targetCell)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InCell && InRange > 0)
        {
            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.N, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.N.ToString());
                return CompassDir.N;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.E.ToString());
                return CompassDir.E;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.NE.ToString());
                return CompassDir.NE;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.NW.ToString());
                return CompassDir.NW;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.SE.ToString());
                return CompassDir.SE;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.S, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.S.ToString());
                return CompassDir.S;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.SW.ToString());
                return CompassDir.SW;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.W.ToString());
                return CompassDir.W;
            }
        }

        Debug.Log("Can not caculate direction");

        return CompassDir.N;
    }

    List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InRange > 0)
        {
            ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

            int Count = 0;
            while (CurserCell)
            {
                if (CurserCell.IsBlocked() && !bAllowBlocked)
                {
                    break;
                }

                GridObject gridObj = CurserCell.GetObjectOnCell();
                if (gridObj)
                {
                    if (m_EffectedTeam == GameTeam.None)
                    {
                        break;
                    }

                    GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                    if (ObjAffinity == GameTeam.Blue && m_EffectedTeam == GameTeam.Red)
                    {
                        break;
                    }
                }

                cells.Add(CurserCell);
                CurserCell = CurserCell.GetAdjacentCell(Dir);
                Count++;
                if (InRange != -1 && Count >= InRange)
                {
                    break;
                }
            }
        }

        return cells;
    }

    bool isTargetOnDirection(List<ILevelCell> directionCellList, ILevelCell targetCell)
    {
        foreach (ILevelCell cell in directionCellList)
        {
            if (cell == targetCell)
            {
                Debug.Log("Target cell on direction cell = " + cell.name);

                return true;
            }
        }

        return false;
    }
}
