﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewDamageCasterAbilityParam", menuName = "TurnBasedTools/Ability/Parameters/ Create DamageCasterAbilityParam", order = 1)]
public class DamageCasterParam : AbilityParam
{
    public int m_Damage;
    public bool m_bMagicalDamage;

    public override void ApplyTo(GridUnit InCaster, ILevelCell InObject, ILevelCell selectedCell, UnitAbility unitAbility)
    {
        ParameterInGame parameterInGame = InCaster.GetComponent<ParameterInGame>();
        if (parameterInGame)
        {
            if (m_bMagicalDamage)
            {
                //parameterInGame.MagicDamage(m_Damage);
            }
            else
            {
                parameterInGame.Damage(m_Damage);
            }
        }
    }

    public override string GetAbilityInfo()
    {
        return "Damage Self " + (m_bMagicalDamage ? "(Magical)" : "") + " " + m_Damage.ToString();
    }
}
