﻿using Assets.Scripts.Monster;
using UnityEngine;

[System.Serializable]
public struct UnitAbilityPlayerData
{
    public UnitAbility unitAbility;

    public AnimationClip AssociatedAnimation;

    public float ExecuteAfterTime;

    public AudioClip AudioOnStart;

    public AudioClip AudioOnExecute;

    public AnimationClip GetAnimation()
    {
        return AssociatedAnimation;
    }
}

[CreateAssetMenu(fileName = "NewUnitData", menuName = "TurnBasedTools/Create UnitData", order = 1)]
public class UnitData : ScriptableObject
{
    public MonsterDataScriptObj monsterDataScriptObj;
    public GameObject m_Model;

    [SerializeField]
    public string m_UnitClass;

    [Space(5)]

    [Header("Animations")]
    public AnimationClip m_IdleAnimation;
    public AnimationClip m_MovementAnimation;
    public AnimationClip m_DamagedAnimation;
    public AnimationClip m_HealAnimation;
    public AnimationClip m_DeadAnimation;
    public float executeBlowAwayDelay = 0f;
    public AnimationClip m_BlowAwayAnimation;

    [Space(5)]

    [Header("Sounds")]
    public AudioClip m_TravelSound;
    public AudioClip m_DamagedSound;
    public AudioClip m_HealSound;
    public AudioClip m_DeathSound;

    [Space(5)]
    [Header("Misc")]

    public bool m_bLookAtTargets;

    [Space(5)]

    public bool m_bIsFlying;
    public float m_HeightOffset;

    [Space(5)]

    public AbilityShape m_MovementShape;

    [Header("Ability")]
    public UnitAbilityPlayerData[] m_Abilities;

    [Header("Spec Ability")]
    public UnitAbilityPlayerData[] m_SpecAbilities;

    [Space(5)]

    public AbilityParticle[] m_SpawnOnHeal;

    void Reset()
    {
        m_bLookAtTargets = true;
    }
}
