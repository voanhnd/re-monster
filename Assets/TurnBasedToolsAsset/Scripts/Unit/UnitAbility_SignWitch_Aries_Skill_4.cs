﻿using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "UnitAbility_SignWitch_Aries_Skill_4", menuName = "TurnBasedTools/Ability/Create New UnitAbility_SignWitch_Aries_Skill_4", order = 1)]
public class UnitAbility_SignWitch_Aries_Skill_4 : UnitAbility
{
    public int KeepDistance = 1;

    public AnimationClip startAnim;
    public AnimationClip moveAnim;
    public AnimationClip endAnim;
    public AnimationClip attackAnim;

    public override List<ILevelCell> Setup(GridUnit InCasterUnit)
    {
        List<ILevelCell> abilityCells = base.Setup(InCasterUnit);

        List<ILevelCell> editedCells = new List<ILevelCell>();

        ILevelCell casterCell = InCasterUnit.GetCell();

        for (int i = 0; i < abilityCells.Count; i++)
        {
            ILevelCell lookCell = abilityCells[i];

            GridUnit gridUnit = lookCell.GetUnitOnCell();

            if (editedCells.Contains(lookCell) == false && gridUnit != null && gridUnit != InCasterUnit && IsEffectTeam(InCasterUnit, gridUnit))
            {
                CompassDir compassDir = GetFaceToCaster(casterCell, lookCell);

                List<ILevelCell> keepDistanceCells = GetKeepDistanceCells(compassDir, lookCell, KeepDistance);

                GridUnit lastUnit = null;

                if(keepDistanceCells.Count > 0)
                {
                    lastUnit = keepDistanceCells[keepDistanceCells.Count - 1].GetUnitOnCell();
                }

                //if (keepDistanceCells.Count == KeepDistance + 2 && lastUnit == null
                //    || keepDistanceCells.Count == KeepDistance + 2 && lastUnit != null && lastUnit == InCasterUnit)
                //{

                //}
                //else 
                if (keepDistanceCells.Count > 0 && keepDistanceCells.Count < KeepDistance + 2 || keepDistanceCells.Count == KeepDistance + 2 && lastUnit != null && lastUnit != InCasterUnit)
                {
                    List<ILevelCell> resetList = GetShape().GetCellsInDirection(casterCell, GetRadius(), ParameterInGame.GetHexFlipFaceTo(compassDir), DoesAllowBlocked(), GameTeam.All);

                    resetList.Add(lookCell);

                    foreach (ILevelCell cell in resetList)
                    {
                        if(cell != casterCell)
                        {
                            GameManager.ResetCellState(cell);

                            Debug.Log("REset cell state = " + cell.name);
                        }
                    }
                }
            }

            editedCells.Add(lookCell);
        }

        return abilityCells;
    }

    public override bool CustomExcuteCondition(GridUnit InCasterUnit, GridUnit targetUnit)
    {
        ILevelCell casterCell = InCasterUnit.GetCell();

        ILevelCell targetCell = targetUnit.GetCell();

        if (casterCell == null)
        {
            Debug.Log("fit target cell null");
        }

        if (targetCell == null)
        {
            Debug.Log("fit target cell null");
        }

        Debug.Log("fit targetUnit team = " + targetUnit.GetTeam().ToString());
        Debug.Log("fit GetEffectedTeam = " + GetEffectedTeam().ToString());

        if (targetUnit != null && targetUnit != InCasterUnit && IsEffectTeam(InCasterUnit, targetUnit))
        {
            Debug.Log("Fit CustomExcuteCondition");

            CompassDir compassDir = GetFaceToCaster(casterCell, targetCell);

            List<ILevelCell> keepDistanceCells = GetKeepDistanceCells(compassDir, targetCell, KeepDistance);

            GridUnit lastUnit = null;

            if (keepDistanceCells.Count > 0)
            {
                lastUnit = keepDistanceCells[keepDistanceCells.Count - 1].GetUnitOnCell();
            }

            if (keepDistanceCells.Count > 0 && keepDistanceCells.Count < KeepDistance + 2 || keepDistanceCells.Count == KeepDistance + 2 && lastUnit != null && lastUnit != InCasterUnit)
            {
                Debug.Log("not Fit conditions");

                return false;
            }

            Debug.Log("Fit conditions");

            return true;
        }

        return false;
    }

    public override List<ILevelCell> GetSetupCells(GridUnit InCasterUnit)
    {
        List<ILevelCell> abilityCells = base.GetSetupCells(InCasterUnit);

        List<ILevelCell> editedCells = new List<ILevelCell>();

        ILevelCell casterCell = InCasterUnit.GetCell();

        for (int i = 0; i < abilityCells.Count; i++) 
        {
            ILevelCell lookCell = abilityCells[i];

            GridUnit gridUnit = lookCell.GetUnitOnCell();

            if (editedCells.Contains(lookCell) == false && gridUnit != null && gridUnit != InCasterUnit && IsEffectTeam(InCasterUnit, gridUnit))
            {
                CompassDir compassDir = GetFaceToCaster(casterCell, lookCell);

                List<ILevelCell> keepDistanceCells = GetKeepDistanceCells(compassDir, lookCell, KeepDistance);

                GridUnit lastUnit = null;

                if (keepDistanceCells.Count > 0)
                {
                    lastUnit = keepDistanceCells[keepDistanceCells.Count - 1].GetUnitOnCell();
                }

                //if (keepDistanceCells.Count == KeepDistance + 2 && lastUnit == null
                //    || keepDistanceCells.Count == KeepDistance + 2 && lastUnit != null && lastUnit == InCasterUnit)
                //{

                //}
                //else 
                if (keepDistanceCells.Count > 0 && keepDistanceCells.Count < KeepDistance + 2 || keepDistanceCells.Count == KeepDistance + 2 && lastUnit != null && lastUnit != InCasterUnit)
                {
                    List<ILevelCell> resetList = GetShape().GetCellsInDirection(casterCell, GetRadius(), ParameterInGame.GetHexFlipFaceTo(compassDir), DoesAllowBlocked(), GameTeam.All);

                    resetList.Add(lookCell);

                    foreach (ILevelCell cell in resetList)
                    {
                        if (cell != casterCell)
                        {
                            abilityCells.Remove(cell);
                        }
                    }
                }
            }

            editedCells.Add(lookCell);
        }

        return abilityCells;
    }

    CompassDir GetFaceToCaster(ILevelCell casterCell, ILevelCell lookCell)
    {
        Vector3 targetDir = casterCell.transform.position - lookCell.transform.position;

        ////180 to -180 angle
        float angle = Vector3.SignedAngle(targetDir.normalized, new Vector3(0f, 0f, 1f), Vector3.down);
        //                                //Look direction     /Look vector             /Rotate vector

        CompassDir compassDir = ParameterInGame.GetHexFaceTo(Quaternion.Euler(0f, angle, 0f).eulerAngles.y);

        return compassDir;
    }

    public List<ILevelCell> GetKeepDistanceCells(CompassDir compassDir, ILevelCell lookCell, int distance)
    {
        List<ILevelCell> dirCells = GetShape().GetCellsInDirection(lookCell, (GetRadius() * 2) + 1, compassDir, DoesAllowBlocked(), GameTeam.All);

        List<ILevelCell> moveBackCells = new List<ILevelCell>();
        moveBackCells.Add(lookCell);

        Debug.Log("Back cell compassDir = " + compassDir.ToString());

        Debug.Log("moveBackCells 0 = " + moveBackCells[0].name);

        for (int i = 0; i < dirCells.Count; i++)
        {
            if(i <= distance)
            {
                Debug.Log("Back cell = " + dirCells[i].name);

                moveBackCells.Add(dirCells[i]);
            }
            else
            {
                break;
            }
        }

        return moveBackCells;
    }

    public override IEnumerator Execute(GridUnit InCasterUnit, ILevelCell InTarget, UnityEvent OnComplete = null)
    {
        Debug.Log("CustomAbility Execute");

        GameManager.AddActionBeingPerformed();

        UnityEvent internalMoveEvent = new UnityEvent();

        yield return GameManager.Get().StartCoroutine(InternalMoveTo(InCasterUnit, InTarget, internalMoveEvent));

        yield return base.Execute(InCasterUnit, InTarget, OnComplete);

        InCasterUnit.PlayAnimation(endAnim);

        yield return new WaitForSeconds(endAnim.length);

        GameManager.RemoveActionBeingPerformed();

        Debug.Log("Execute RemoveActionBeingPerformed");

        yield break;
    }

    IEnumerator InternalMoveTo(GridUnit InCasterUnit, ILevelCell InTargetCell, UnityEvent OnMovementComplete = null)
    {
        if (InTargetCell)
        {
            GameManager.AddActionBeingPerformed();
            Debug.Log("CustomAbility InternalMoveTo AddActionBeingPerformed");

            ILevelCell onCell = InCasterUnit.GetCell();

            CompassDir compassDir = GetFaceToCaster(onCell, InTargetCell);

            List<ILevelCell> keepDistanceCells = GetKeepDistanceCells(compassDir, InTargetCell, KeepDistance);

            ILevelCell moveToCell = keepDistanceCells[keepDistanceCells.Count - 1];

            if(onCell != moveToCell)
            {
                List<ILevelCell> cellPath = InCasterUnit.GetPathTo(moveToCell, GetSetupCells(InCasterUnit));

                foreach (ILevelCell cell in cellPath)
                {
                    GameManager.ResetCellState(cell);
                }

                if (cellPath.Count > 1) cellPath.RemoveAt(0);

                Debug.Log("Full cell path = " + cellPath.Count);

                if (cellPath.Count > 0)
                {
                    InCasterUnit.PlayAnimation(startAnim);

                    yield return new WaitForSeconds(startAnim.length);

                    InCasterUnit.PlayAnimation(moveAnim);
                }

                Vector3 StartPos = onCell.GetAllignPos(InCasterUnit);

                ILevelCell StartingCell = onCell;

                ILevelCell previousCell = StartingCell;

                foreach (ILevelCell cell in cellPath)
                {
                    float TimeTo = 0;
                    Vector3 EndPos = cell.GetAllignPos(InCasterUnit);
                    while (TimeTo < 1.5f)
                    {
                        float speed = 0f;

                        if (InCasterUnit.GetSpeed() == 0f)
                            speed = AIManager.GetMovementSpeed();
                        else
                            speed = InCasterUnit.GetSpeed();

                        TimeTo += Time.deltaTime * speed;
                        InCasterUnit.transform.position = Vector3.MoveTowards(StartPos, EndPos, TimeTo);

                        InCasterUnit.LookAtCell(cell);

                        yield return new WaitForSeconds(Time.fixedDeltaTime);
                    }

                    InCasterUnit.transform.position = EndPos;
                    StartPos = cell.GetAllignPos(InCasterUnit);
                    yield return new WaitForSeconds(AIManager.GetWaitTime());

                    if (cell != StartingCell)
                    {
                        InCasterUnit.SetCurrentCell(cell);
                        cell.SetMaterial(previousCell.GetCellState());

                        previousCell.SetMaterial(previousCell.GetCellState());
                        previousCell = cell;

                        AilmentHandler.HandleUnitOnCell(InCasterUnit, cell);
                    }

                    if (InCasterUnit.IsDead())
                    {
                        break;
                    }
                }

                if (cellPath.Count > 0)
                {
                    InCasterUnit.SetCurrentCell(moveToCell);
                }

                //Debug.Log("CustomAbility InternalMoveTo m_IdleAnimation");
                InCasterUnit.PlayAnimation(InCasterUnit.GetUnitData().m_IdleAnimation);

                if (OnMovementComplete != null)
                {
                    OnMovementComplete.Invoke();
                }
            }

            GameManager.RemoveActionBeingPerformed();

            Debug.Log("CustomAbility InternalMoveTo RemoveActionBeingPerformed");
        }

        yield break;
    }
}
