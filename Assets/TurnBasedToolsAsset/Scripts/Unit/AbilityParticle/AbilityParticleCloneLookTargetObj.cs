using UnityEngine;

public class AbilityParticleCloneLookTargetObj : MonoBehaviour
{
    Transform fromPoint;
    Transform toPoint;

    public AnimationClip slashAnimClip;

    float startTime = 0f;

    public float activeDelay = 0f;
    bool active = false;

    public float moveToTime = 0.25f;
    bool onStayPoint = false;

    public float slashDelay = 0.25f;
    bool slashDone = false;

    public Transform Bullet;

    public ParticleSystem[] muzzleFXs;
    public ParticleSystem[] trailFXs;
    public ParticleSystem[] impactFXs;

    public Animator Character;

    public void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget, Transform fromP, Transform toP)
    {
        fromPoint = fromP;
        transform.position = fromPoint.position;

        toPoint = toP;

        startTime = Time.time;

        Bullet.gameObject.SetActive(false);
        Bullet.transform.position = fromP.position;

        Character.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (active == false && Time.time - startTime > activeDelay)
        {
            active = true;

            startTime = Time.time;

            Bullet.gameObject.SetActive(true);

            foreach (var AllPs in muzzleFXs)
            {
                if (!AllPs.isPlaying) AllPs.Play();
            }

            foreach (var AllPs in trailFXs)
            {
                if (!AllPs.isPlaying) AllPs.Play();
            }
        }

        if (active && onStayPoint == false)
        {
            float normalVal = Mathf.Clamp((Time.time - startTime) / moveToTime, 0f, 1f);

            //float authorHeightNormal = 1 - (Mathf.Abs(normalVal - 0.5f) / 0.5f);

            transform.position = Vector3.Lerp(fromPoint.position, toPoint.position, normalVal);

            if (Time.time - startTime > moveToTime)
            {
                onStayPoint = true;

                startTime = Time.time;

                Character.gameObject.SetActive(true);

                foreach (var AllPs in impactFXs)
                {
                    if (!AllPs.isPlaying) AllPs.Play();
                }
            }
        }

        if (onStayPoint && slashDone == false && Time.time - startTime > slashDelay)
        {
            slashDone = true;

            Character.Play(slashAnimClip.name);
        }
    }
}
