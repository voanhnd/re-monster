//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityParticlePlaySound : MonoBehaviour
{
    float startTime;

    [System.Serializable]
    public class PlayClipData
    {
        public float playDelay;
        public AudioClip clip;
    }

    public List<PlayClipData> playClipDatas = new List<PlayClipData>();

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        List<PlayClipData> tempClips = new List<PlayClipData>();
        tempClips.AddRange(playClipDatas);

        foreach (PlayClipData clipData in tempClips)
        {
            if (Time.time - startTime >= clipData.playDelay)
            {
                AudioPlayData audioData = new AudioPlayData(clipData.clip);
                AudioHandler.PlayAudio(audioData, transform.position);

                playClipDatas.Remove(clipData);
            }
        }
    }
}
