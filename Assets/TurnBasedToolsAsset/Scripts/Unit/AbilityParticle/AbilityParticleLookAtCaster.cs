using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityParticleLookAtCaster : AbilityParticle
{
    Vector3 offset = new Vector3(0f, -90f, 0f); //Look by X axis

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        transform.LookAt(InCaster.transform);
        transform.rotation *= Quaternion.Euler(offset);
    }
}
