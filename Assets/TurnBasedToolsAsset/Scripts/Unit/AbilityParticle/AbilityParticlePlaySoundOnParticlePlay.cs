using System.Collections.Generic;
using UnityEngine;

public class AbilityParticlePlaySoundOnParticlePlay : MonoBehaviour
{
    public ParticleSystem mainParticle;

    bool startTimeSet = false;
    float startTime;

    [System.Serializable]
    public class PlayClipData
    {
        public float playDelay;
        public AudioClip clip;
    }

    public List<PlayClipData> playClipDatas = new List<PlayClipData>();

    // Update is called once per frame
    void Update()
    {

        if (startTimeSet)
        {
            List<PlayClipData> tempClips = new List<PlayClipData>();
            tempClips.AddRange(playClipDatas);

            foreach (PlayClipData clipData in tempClips)
            {
                if (Time.time - startTime >= clipData.playDelay)
                {
                    AudioPlayData audioData = new AudioPlayData(clipData.clip);
                    AudioHandler.PlayAudio(audioData, transform.position);

                    playClipDatas.Remove(clipData);
                }
            }
        }
        else if (mainParticle.isPlaying && startTimeSet == false)
        {
            startTimeSet = true;

            startTime = Time.time;
        }
    }

    private void OnValidate()
    {
        if (Application.isPlaying == false && mainParticle == null)
        {
            mainParticle = gameObject.GetComponent<ParticleSystem>();
        }
    }
}
