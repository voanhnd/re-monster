using UnityEngine;

public class AbilityParticleSlimeJumpToHit : AbilityParticle
{
    Transform author;
    Vector3 authorOriginPos;

    Transform target;

    float maxHeight = 1f;

    public float activeDelay = 0f;
    public float timeToTarget = 0.5f;
    public float delayOnTarget = 0f;
    public float backToOriginTime = 0.25f;

    float startTime = 0f;

    public float doneDelay = 3f;

    bool active = false;
    bool onTarget = false;
    bool backOrigin = false;
    bool done = false;

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        author = InCaster.transform;
        authorOriginPos = author.position;

        target = InTarget.transform;

        startTime = Time.time;

        active = false;
        onTarget = false;
        backOrigin = false;
        done = false;
    }

    private void Update()
    {
        if (active == false && Time.time - startTime >= activeDelay)
        {
            active = true;

            startTime = Time.time;
        }

        if (active && onTarget == false)
        {
            float normalVal = Mathf.Clamp((Time.time - startTime) / timeToTarget, 0f, 1f);

            float authorHeightNormal = 1 - (Mathf.Abs(normalVal - 0.5f) / 0.5f);

            Debug.Log("normalVal = " + normalVal);

            Vector3 currentAuthorPos = Vector3.Lerp(authorOriginPos, target.position, normalVal);
            author.position = new Vector3(currentAuthorPos.x, maxHeight * authorHeightNormal, currentAuthorPos.z);

            if (Time.time - startTime >= timeToTarget)
            {
                startTime = Time.time;

                onTarget = true;
            }
        }

        if (onTarget && backOrigin == false && Time.time - startTime >= delayOnTarget)
        {
            backOrigin = true;
        }

        if (backOrigin && done == false)
        {
            float normalVal = Mathf.Clamp((Time.time - startTime) / backToOriginTime, 0f, 1f);

            Vector3 currentAuthorPos = Vector3.Lerp(target.position, authorOriginPos, normalVal);
            author.position = currentAuthorPos;

            if (normalVal >= 1f)
            {
                done = true;

                author.position = authorOriginPos;
            }
        }
    }
}
