using System.Collections.Generic;

public class AbilityParticleSlimeStorm : AbilityParticle
{
    public AbilityParticleSlimeStormObj iceCreamPrefabs;

    public List<ILevelCell> cells = new List<ILevelCell>();

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        cells = InAbility.GetEffectedCells(InCaster, InTarget);

        foreach (ILevelCell c in cells)
        {
            Instantiate(iceCreamPrefabs, c.transform.position, iceCreamPrefabs.transform.rotation);
        }
    }
}
