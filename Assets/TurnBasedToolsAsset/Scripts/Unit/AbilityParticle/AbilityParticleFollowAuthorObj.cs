using UnityEngine;

public class AbilityParticleFollowAuthorObj : AbilityParticle
{
    public string followObjName;
    Transform followObj;

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        GameObject go = GameobjectExtension.GetChildGameObjectByName(InCaster.gameObject, followObjName);

        if (go == null)
        {
            followObj = InCaster.transform;
        }
        else
        {
            followObj = go.transform;
        }
    }

    private void Update()
    {
        if (followObj == null) return;

        transform.position = followObj.transform.position;
        transform.rotation = followObj.transform.rotation;
    }
}
