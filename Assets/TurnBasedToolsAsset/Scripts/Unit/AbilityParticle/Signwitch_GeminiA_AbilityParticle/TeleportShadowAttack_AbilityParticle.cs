using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportShadowAttack_AbilityParticle : AbilityParticle
{
    Vector3 lookOffset = new Vector3(0f, -90f, 0f); //Look by X axis

    float startTime = 0f;

    GridUnit caster;
    ILevelCell target;

    public Transform startPos;
    public Vector3 startHideOffset;
    public float timeToHide = 0.25f;
    public ParticleSystem startShadowFX;
    bool startToHide = false;
    bool hideToStart = false;

    public Transform showContainer;
    public Transform showPos;
    public Vector3 showOffset;
    public float timeToShow = 0.25f;
    public ParticleSystem showShadowFX;
    bool hideToShow = false;
    bool showToHide = false;

    public bool attacked = false;
    public AnimationClip attackAnimation;

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        caster = InCaster;
        startPos.transform.position = caster.transform.position;

        target = InTarget;

        transform.LookAt(target.transform.position);
        transform.rotation *= Quaternion.Euler(lookOffset);

        showContainer.position = target.transform.position;

        startShadowFX.Play();

        showShadowFX.Play();

        startTime = Time.time;

        startToHide = false;
        hideToStart = false;

        hideToShow = false;
        showToHide = false;

        attacked = false;
    }

    private void Update()
    {
        float timePass = Time.time - startTime;

        if (startToHide == false)
        {
            float lerpVal = Mathf.Clamp(timePass / timeToHide, 0f, 1f);

            caster.transform.position = Vector3.Lerp(startPos.position, startPos.position + startHideOffset, lerpVal);

            if (lerpVal >= 1f)
            {
                startToHide = true;

                startTime = Time.time;
            }
        }
        else
        if (hideToShow == false)
        {
            float lerpVal = Mathf.Clamp(timePass / timeToShow, 0f, 1f);

            caster.transform.position = Vector3.Lerp(showPos.position + showOffset, showPos.position, lerpVal);

            if (lerpVal >= 1f)
            {
                hideToShow = true;

                startTime = Time.time;
            }
        }
        else
        if (attacked == false)
        {
            caster.PlayAnimation(attackAnimation, true);

            if (timePass >= attackAnimation.length)
            {
                attacked = true;

                startTime = Time.time;
            }
        }
        else
        if (showToHide == false)
        {
            float lerpVal = Mathf.Clamp(timePass / timeToShow, 0f, 1f);

            caster.transform.position = Vector3.Lerp(showPos.position, showPos.position + showOffset, lerpVal);

            if (lerpVal >= 1f)
            {
                showToHide = true;

                startTime = Time.time;
            }
        }
        else
        if (hideToStart == false)
        {
            float lerpVal = Mathf.Clamp(timePass / timeToHide, 0f, 1f);

            caster.transform.position = Vector3.Lerp(startPos.position + startHideOffset, startPos.position, lerpVal);

            if (lerpVal >= 1f)
            {
                hideToStart = true;

                startTime = Time.time;
            }
        }
    }
}
