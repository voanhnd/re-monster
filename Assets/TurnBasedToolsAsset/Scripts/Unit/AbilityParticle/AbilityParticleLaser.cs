using System.Collections.Generic;
using UnityEngine;

public class AbilityParticleLaser : AbilityParticle
{
    public string laserPointObjName;
    Transform lasePoint;

    public float activeDelay = 0f;
    bool activated = false;
    float curActiveTime = 0f;

    public Vector3 fromPos;
    public Vector3 toPos;

    Vector3 lookAtOffset = new Vector3(0f, -90f, 0f); //Look by X axis

    public GameObject HitEffect;
    public Vector3 HitOffset;
    public bool useLaserRotation = false;

    public LineRenderer Laser;

    public bool scan = true;
    [SerializeField]
    float startScanTime = 0f;
    [SerializeField]
    public float scanTime = 1.5f;
    [SerializeField]
    float timePerUnit = 0f;
    [SerializeField]
    int scanIndex = 0;
    [SerializeField]
    List<ILevelCell> EffectCellList = new List<ILevelCell>();

    public float MainTextureLength = 1f;
    public float NoiseTextureLength = 1f;
    private Vector4 Length = new Vector4(1, 1, 1, 1);

    public ParticleSystem[] Effects;
    public ParticleSystem[] Hit;

    public float startFadeTime = 0f;
    public float fadeAtTime = 1.5f;
    public float fadeTime = 0.5f;
    bool fade = false;

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        lasePoint = GetChildGameObject(InCaster.gameObject, laserPointObjName).transform;

        toPos = InTarget.transform.position + HitOffset;

        Laser.enabled = false;

        startFadeTime = Time.time;

        if (scan)
        {
            EffectCellList = new List<ILevelCell>();
            EffectCellList.AddRange(InAbility.GetEffectedCells(InCaster, InTarget));

            if (!EffectCellList.Contains(InTarget))
            {
                EffectCellList.Add(InTarget);
            }

            timePerUnit = scanTime / EffectCellList.Count;
        }
    }

    private void Update()
    {
        if (activated == false)
        {
            curActiveTime += Time.deltaTime;

            if (curActiveTime >= activeDelay)
            {
                activated = true;

                Laser.enabled = true;

                foreach (var AllPs in Effects)
                {
                    if (!AllPs.isPlaying) AllPs.Play();
                }

                foreach (var AllPs in Hit)
                {
                    if (!AllPs.isPlaying) AllPs.Play();
                }

                startScanTime = Time.time;
            }
        }

        if (fade == false)
        {
            fromPos = lasePoint.transform.position;

            transform.position = lasePoint.transform.position;
            transform.LookAt(toPos);
            transform.rotation *= Quaternion.Euler(lookAtOffset);

            Laser.material.SetTextureScale("_MainTex", new Vector2(Length[0], Length[1]));
            Laser.material.SetTextureScale("_Noise", new Vector2(Length[2], Length[3]));

            Laser.SetPosition(0, fromPos);

            if (activated && scan && EffectCellList.Count > 1)
            {
                float normalTime = (Time.time - startScanTime) / timePerUnit;

                scanIndex = Mathf.Clamp(scanIndex, 0, EffectCellList.Count - 2);

                Vector3 pos = Vector3.Lerp(EffectCellList[0].transform.position, EffectCellList[scanIndex + 1].transform.position, normalTime);

                Laser.SetPosition(1, pos);

                HitEffect.transform.position = pos;
                HitEffect.transform.LookAt(pos);

                //Texture tiling
                Length[0] = MainTextureLength * (Vector3.Distance(transform.position, pos));
                Length[2] = NoiseTextureLength * (Vector3.Distance(transform.position, pos));

                if ((Time.time - startScanTime) > timePerUnit && scanIndex < EffectCellList.Count - 1)
                {
                    startScanTime = Time.time;

                    scanIndex++;
                }
            }
            else
            {
                Laser.SetPosition(1, toPos);

                HitEffect.transform.position = toPos;
                HitEffect.transform.LookAt(toPos);

                //Texture tiling
                Length[0] = MainTextureLength * (Vector3.Distance(transform.position, toPos));
                Length[2] = NoiseTextureLength * (Vector3.Distance(transform.position, toPos));
            }
        }

        if (fade == false && Time.time - startFadeTime >= fadeAtTime)
        {
            fade = true;

            startFadeTime = Time.time;
        }

        if (fade == true && Time.time - startFadeTime <= fadeTime)
        {
            float normalTime = (Time.time - startFadeTime) / fadeTime;

            fromPos = Vector3.Lerp(fromPos, toPos, normalTime);

            Laser.SetPosition(0, fromPos);

            Laser.SetPosition(1, toPos);
        }
    }

    public GameObject GetChildGameObject(GameObject fromGameObject, string withName)
    {
        Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts) if (t.gameObject.name == withName) return t.gameObject;
        return null;
    }
}
