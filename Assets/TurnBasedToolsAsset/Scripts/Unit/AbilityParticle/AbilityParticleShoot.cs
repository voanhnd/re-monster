using UnityEngine;

public class AbilityParticleShoot : AbilityParticle
{
    Vector3 lookOffset = new Vector3(0f, -90f, 0f); //Look by X axis

    public float activeDelay = 0f;
    float curActiveTime;
    bool activated;

    public Transform bullet;
    public string bulletSpawnObjName = string.Empty;
    public bool deaciveHitBullet = true;
    public float bulletSpeed = 5f;

    public Transform muzzleObj;
    public ParticleSystem[] muzzleFXs;
    Transform bulletSpawnPoint;

    public Transform hitObj;
    public ParticleSystem[] hitFXs;
    Vector3 hitPos;
    public Vector3 hitPosOffset = new Vector3(0f, 0.5f, 0f);

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        transform.LookAt(InTarget.transform.position);
        transform.rotation *= Quaternion.Euler(lookOffset);

        if (bulletSpawnObjName != string.Empty)
        {
            bulletSpawnPoint = GameobjectExtension.GetChildGameObjectByName(InCaster.gameObject, bulletSpawnObjName).transform;

            bullet.transform.position = bulletSpawnPoint.position;
        }

        hitPos = InTarget.transform.position + hitPosOffset;

        bullet.gameObject.SetActive(false);

        muzzleObj.transform.position = bullet.transform.position;
        foreach (var AllPs in muzzleFXs)
        {
            if (!AllPs.isPlaying) AllPs.Play();
        }
    }

    private void Update()
    {
        if (activated == false)
        {
            curActiveTime += Time.deltaTime;

            if (curActiveTime >= activeDelay)
            {
                activated = true;

                bullet.gameObject.SetActive(true);
                bullet.transform.LookAt(hitPos);
            }
        }

        if (activated && bullet.transform.position != hitPos)
        {
            bullet.transform.position = Vector3.MoveTowards(bullet.transform.position, hitPos, Time.deltaTime * bulletSpeed);

            if (bullet.transform.position == hitPos)
            {
                if (deaciveHitBullet)
                {
                    bullet.gameObject.SetActive(false);
                }

                hitObj.position = bullet.transform.position;
                foreach (var AllPs in hitFXs)
                {
                    if (!AllPs.isPlaying) AllPs.Play();
                }
            }
        }
    }
}
