using UnityEngine;

public class AbilityParticleSlimeStormObj : MonoBehaviour
{
    public Vector3 scaleFrom;
    public Vector3 scaleTo;

    float startTime;

    public float scaleToTime = 0.5f;

    public float desTroyTime = 0f;

    private void Start()
    {
        startTime = Time.time;

        transform.localScale = scaleFrom;

        Destroy(gameObject, desTroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        float normalVal = Mathf.Clamp((Time.time - startTime) / scaleToTime, 0f, 1f);

        Vector3 currentScale = Vector3.Lerp(scaleFrom, scaleTo, normalVal);
        transform.localScale = currentScale;
    }
}
