﻿using UnityEngine;

public class AbilityParticle : MonoBehaviour
{
    public bool spawnOnStart = false;

    public float DeleteAfterTime;

    public virtual void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        Destroy(gameObject, DeleteAfterTime);
    }
}
