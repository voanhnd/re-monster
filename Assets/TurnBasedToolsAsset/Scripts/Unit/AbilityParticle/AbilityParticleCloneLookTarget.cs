using System.Collections.Generic;
using UnityEngine;

public class AbilityParticleCloneLookTarget : AbilityParticle
{
    Vector3 offset = new Vector3(0f, -90f, 0f); //Look by X axis

    [System.Serializable]
    public class CloneGroup
    {
        public Transform container;

        public List<AbilityParticleCloneLookTargetObj> clones = new List<AbilityParticleCloneLookTargetObj>();

        public List<Transform> spawnPoints = new List<Transform>();

        public List<Transform> toPoints = new List<Transform>();
    }

    public List<CloneGroup> cloneGroups = new List<CloneGroup>();

    public override void Setup(UnitAbility InAbility, GridUnit InCaster, ILevelCell InTarget)
    {
        base.Setup(InAbility, InCaster, InTarget);

        transform.position = InCaster.transform.position;

        List<ILevelCell> EffectCellList = new List<ILevelCell>();

        foreach (ILevelCell targetCell in InAbility.GetEffectedCells(InCaster, InTarget))
        {
            if (targetCell.GetUnitOnCell() != null && AbilityParam.IsEffectTeam(InCaster, targetCell.GetUnitOnCell(), InAbility))
            {
                EffectCellList.Add(targetCell);
            }
        }

        for (int i = 0; i < cloneGroups.Count; i++)
        {
            CloneGroup g = cloneGroups[i];

            if (i < EffectCellList.Count)
            {
                g.container.gameObject.SetActive(true);

                ILevelCell cell = EffectCellList[i];

                g.container.position = cell.transform.position;
                g.container.LookAt(InCaster.transform);
                g.container.rotation *= Quaternion.Euler(-offset);

                for (int k = 0; k < g.clones.Count; k++)
                {
                    g.clones[k].Setup(InAbility, InCaster, cell, g.spawnPoints[k], g.toPoints[k]);
                }
            }
            else
            {
                g.container.gameObject.SetActive(false);
            }
        }
    }
}
