﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct IndexToMaterial
{
    public int m_Index;
    public Material m_Material;

    public IndexToMaterial(int InIndex, Material InMaterial)
    {
        m_Index = InIndex;
        m_Material = InMaterial;
    }
}

public class CellStyleInfo : MonoBehaviour
{
    [SerializeField]
    List<IndexToMaterial> m_HoverMatStates;

    [SerializeField]
    List<IndexToMaterial> m_PositiveMatStates;

    [SerializeField]
    List<IndexToMaterial> m_NegativeMatStates;

    [SerializeField]
    List<IndexToMaterial> m_MovementMatStates;

    [SerializeField]
    List<IndexToMaterial> m_BlockedMatStates;

    [SerializeField]
    List<IndexToMaterial> m_FriendlySpawnMatStates;

    [SerializeField]
    List<IndexToMaterial> m_HostileSpawnMatStates;

    List<IndexToMaterial> m_Custom_NormalMatStates = new List<IndexToMaterial>();

    List<IndexToMaterial> m_NormalMatStates = new List<IndexToMaterial>();

    private void Awake()
    {
        m_NormalMatStates = GenerateCurrentCellMatState();

        m_Custom_NormalMatStates = GenerateCurrentCellMatState();
    }

    //void Start()
    //{

    //}

    public List<IndexToMaterial> GenerateCurrentCellMatState()
    {
        Material[] Mats = GetComponent<MeshRenderer>().materials;

        List<IndexToMaterial> OutMatIndexMap = new List<IndexToMaterial>();
        for (int i = 0; i < Mats.Length; i++)
        {
            if (Mats.Length > i)
            {
                OutMatIndexMap.Add(new IndexToMaterial(i, Mats[i]));
            }
        }
        return OutMatIndexMap;
    }

    public List<IndexToMaterial> GetCellMaterialNormal()
    {
        return m_Custom_NormalMatStates;
    }

    public List<IndexToMaterial> GetCellMaterialFriendlySpawn()
    {
        return m_FriendlySpawnMatStates;
    }

    public List<IndexToMaterial> GetCellMaterialHostileSpawn()
    {
        return m_HostileSpawnMatStates;
    }

    public List<IndexToMaterial> GetCellMaterialBlocked()
    {
        return m_BlockedMatStates;
    }

    public List<IndexToMaterial> GetCellMaterialState(CellState InCellState)
    {
        ILevelCell levelCell = GetComponent<ILevelCell>();

        GridUnit gridUnit = levelCell.GetUnitOnCell();

        switch (InCellState)
        {
            case CellState.eHover:
                return m_HoverMatStates;

            case CellState.ePositive:
                return m_PositiveMatStates;

            case CellState.eNegative:
                return m_NegativeMatStates;

            case CellState.eMovement:
                return m_MovementMatStates;

            case CellState.eBlocked:
                return GetCellMaterialBlocked();
        }

        if (gridUnit != null && gridUnit.IsDead() == false)
        {
            if (gridUnit.GetTeam() == CustomGameManager.Instance.GetPlayerTeam())
            {
                return m_FriendlySpawnMatStates;
            }
            else
            {
                return m_HostileSpawnMatStates;
            }
        }
        else
        {
            return m_NormalMatStates;
        }
    }

    ILevelCell GetILevelCell()
    {
        return GetComponent<ILevelCell>();
    }

    public void CustomSetNormalMaterials(List<IndexToMaterial> mats)
    {
        m_NormalMatStates = mats;
    }
}
