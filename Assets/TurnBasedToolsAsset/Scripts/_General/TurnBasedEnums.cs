﻿public enum CompassDir
{
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW
}
