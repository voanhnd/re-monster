window.addEventListener("DOMContentLoaded", function() {
  var unityContainer = document.getElementById("unity-container");

  function updateOrientation() {
    if (window.matchMedia("(orientation: portrait)").matches) {
      unityContainer.classList.add("rotate");
    } else {
      unityContainer.classList.remove("rotate");
    }
  }

  updateOrientation();

  window.addEventListener("orientationchange", function() {
    updateOrientation();
  });
});
