using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.TrainingArea
{
    [CreateAssetMenu(fileName = "TrainingAreaDataManagerScriptObj", menuName = "ScriptableObjects/TrainingArea/TrainingAreaDataManagerScriptObj", order = 1)]
    public class TrainingAreaDataManagerScriptObj : ScriptableObject
    {
        public Dictionary<TrainingArea.TrainingAreaType, TrainingAreaDataScriptObj> trainingAreaDataDictionary = new Dictionary<TrainingArea.TrainingAreaType, TrainingAreaDataScriptObj>();

        [SerializeField]
        List<TrainingAreaDataScriptObj> trainingAreaDataList = new List<TrainingAreaDataScriptObj>();

        public TrainingAreaDataScriptObj GetTrainingAreaDataObj(TrainingArea.TrainingAreaType trainingAreaType)
        {
            TrainingAreaDataScriptObj trainingAreaDataObj = null;

            if (!trainingAreaDataDictionary.TryGetValue(trainingAreaType, out trainingAreaDataObj))
            {
                // the key isn't in the dictionary.
                return trainingAreaDataObj; // or whatever you want to do
            }

            return trainingAreaDataObj;
        }

        private void OnValidate()
        {
            if (Application.isEditor && Application.isPlaying == false)
            {
                //Debug.Log("OnValidate");

                CreateDictionary();
            }
        }

        public void CreateDictionary()
        {
            if (trainingAreaDataList.Count > 0)
            {
                trainingAreaDataDictionary.Clear();

                for (int i = 0; i < trainingAreaDataList.Count; i++)
                {
                    trainingAreaDataDictionary.Add(trainingAreaDataList[i].TrainingArea.trainingAreaType, trainingAreaDataList[i]);
                }
            }
        }
    }
}