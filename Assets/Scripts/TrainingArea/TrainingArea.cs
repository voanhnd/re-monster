﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.TrainingArea
{
    [Serializable]
    public class TrainingArea
    {
        public enum TrainingAreaType
        {
            Empty, PlainA, PlainB, ForestA, ForestB, WaterfrontA, WaterfrontB, Desert, Wilderness, Mountain, Volcano
        }

        public enum TerrainType
        {
            Empty, Plain, Forest, Waterfront, Desert, WildLand, Mountain, Volcano
        }

        public enum TrainingType
        {
            success, huge_success, failure
        }

        public TrainingAreaType trainingAreaType;

        public TerrainType trainingTerrainType;

        public RankTypeEnums rankType;

        public string areaName = "";

        public float AreaPrice = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float fatigueCorrection = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float stressCorrection = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float injuryCorrection = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float diseaseCorrection = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float fatigueprobability = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float stressprobability = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float injuryprobability = 0f;

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        public float diseaseprobability = 0f;

        public float recommendedTotal = 0f;

        [Header("This parameter is %, 100% = 100f")]
        /// <summary>
        /// This parameter is %, 100% = 100f
        /// </summary>
        public BasicParameters parametersCorrection;


        //[Header("This parameter is %, 100% = 100f")]
        ///// <summary>
        ///// This parameter is %, 100% = 100f
        ///// </summary>
        //public BasicParameters skillParametersCorrection;

        /// <summary>
        /// This is main parameters to training > 1f mean it will be trained = 0f mean it is not trained
        /// </summary>
        public BasicParameters mainParameters;

        /// <summary>
        /// This is sub parameters to training > 1f mean it will be trained = 0f mean it is not trained
        /// </summary>
        public BasicParameters subParameters;

        public List<AcquiredTrait.AcquiredTraitType> acquiredTraitTypes = new();

        [Header("Exploration settings")]

        public RankTypeEnums explorationRankType;

        public float explorationAreaPrice = 0f;

        public float explorationRecommendedTotal = 0f;

        [Header("This parameter is %, 100% = 100f")]
        /// <summary>
        /// This parameter is %, 100% = 100f
        /// </summary>
        public BasicParameters explorationParametersCorrection;

        public BasicParameters explorationRiseParameter;

        #region Training

        /// <summary>
        /// <para> succeed 82% </para>
        /// <para> Big success 3% </para>
        /// <para> failure 15% </para>
        /// </summary>
        /// <returns></returns>
        public static TrainingType Get_Training_Random_Success_Failure(float successRate, float huge_successRate, float failureRate)
        {
            float randomValue = UnityEngine.Random.Range(0f, 100f);

            TrainingType selectedTrainingType = TrainingType.failure;

            if (randomValue < huge_successRate)
            {
                selectedTrainingType = TrainingType.huge_success;
            }
            else if (randomValue < successRate + huge_successRate)
            {
                selectedTrainingType = TrainingType.success;
            }
            else if (randomValue < successRate + huge_successRate + failureRate)
            {
                selectedTrainingType = TrainingType.failure;
            }

            Debug.Log("Get_Training_Random_Success_Failure TrainingType = " + selectedTrainingType);

            Debug.Log("successRate = " + successRate);
            Debug.Log("huge_successRate = " + huge_successRate);
            Debug.Log("failureRate = " + failureRate);

            return selectedTrainingType;
        }

        /// <summary>
        /// return value is percentage, 100% = 100f
        /// <para> succeed 82% </para>
        /// <para> Big success 3% </para>
        /// <para> failure 15% </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Training_Basic_Success_Failure_Rate(TrainingType trainingType)
        {
            float percent = 0f;

            switch (trainingType)
            {
                case TrainingType.success:
                    {
                        percent = 82f;

                        break;
                    }

                case TrainingType.huge_success:
                    {
                        percent = 3f;

                        break;
                    }

                case TrainingType.failure:
                    {
                        percent = 15f;

                        break;
                    }
            }

            return percent;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="trainingType"></param>
        /// <returns></returns>
        public static float Get_Training_Up_Percent(TrainingType trainingType)
        {
            float percent = 0f;

            switch (trainingType)
            {
                case TrainingType.success:
                    {
                        percent = 100f;

                        break;
                    }

                case TrainingType.huge_success:
                    {
                        percent = 150f;

                        break;
                    }

                case TrainingType.failure:
                    {
                        percent = 50f;

                        break;
                    }
            }

            return percent / 100f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="trainingType"></param>
        /// <returns></returns>
        public static float Get_Training_Down_Percent(TrainingType trainingType)
        {
            float percent = 0f;

            switch (trainingType)
            {
                case TrainingType.success:
                    {
                        percent = 100f;

                        break;
                    }

                case TrainingType.huge_success:
                    {
                        percent = 50f;

                        break;
                    }

                case TrainingType.failure:
                    {
                        percent = 150f;

                        break;
                    }
            }

            return percent / 100f;
        }

        /// <summary>
        /// <para> Parameter increase calculation formula </para>
        /// <para> Basic parameter increment: T (Training Effects) </para>
        /// <para> Farm Training Enhancement Value: E (Enhanced value) </para>
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Injury correction: IC (Injury correction) </para>
        /// <para> Disease correction: SC (Sickness correction) </para>
        /// <para> Monster growth aptitude correction: GP (Growth potential) </para>
        /// <para> Training Outcome: O (Outocome) </para>
        /// <para> Growth Stage Correction: G (Growth correction) </para>
        /// <para> Last increment: R(Result) </para>
        /// <para> R = T × ( 1 + E × 0.3 / 100 + C + CC + IC + SC + GP ) × ( 1 + G ) × O × 0.95~1.05 </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Training_Parameter_Rise_Up(float basicTrainingMainParameter, RankTypeEnums parameterGrowthAptitudeRank, float farmEnhancedValue, int mainStat, int subStat, float personalityTraitCorrection, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType, Monster.Monster.GrowthType growthType, float currentLifeSpan, float maxLifeSpan, BasicParameters currentBasicParameters, TrainingType trainingType)
        {
            float injuryCorrection = 0f;
            if (injuryType == Monster.Monster.InjuryType.Injury || injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                injuryCorrection = Get_Training_Injured_Probability_Rate_By_Stat(3);
            }

            float diseasesCorrection = 0f;
            if (diseasesType == Monster.Monster.DiseasesType.Diseases || diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                diseasesCorrection = Get_Training_Diseases_Probability_Rate_By_Stat(3);
            }

            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float growthAppropriate = Monster.Monster.Get_Growth_Aptitude_Increase_Correction_Value(parameterGrowthAptitudeRank);

            float trainingOutcomeCorrection = Get_Training_Up_Percent(trainingType);

            float growthStageCorrection = Monster.Monster.Get_Growth_Stage_Training_Correction(growthType, currentLifeSpan, maxLifeSpan);

            float ranValue = UnityEngine.Random.Range(0.95f, 1.05f);

            //Debug.Log($"_____{basicTrainingMainParameter} * (1f + {farmEnhancedValue} * 0.3f / 100f + {terrainCompatCorrection} + {characteristicCorrection} + {injuryCorrection} + {diseasesCorrection} + {growthAppropriate}) * (1f + {growthStageCorrection}) * {trainingOutcomeCorrection} * {ranValue}______");

            float result = basicTrainingMainParameter * (1f + farmEnhancedValue * 0.3f / 100f + terrainCompatCorrection + personalityTraitCorrection + injuryCorrection + diseasesCorrection + growthAppropriate) * (1f + growthStageCorrection) * trainingOutcomeCorrection * ranValue;

            //if (basicTrainingMainParameter != 0f)
            //{
            //    Debug.Log("Get_Training_Parameter_Rise_Up");
            //    Debug.Log("basicTrainingMainParameter = " + basicTrainingMainParameter);
            //    Debug.Log("farmEnhancedValue = " + farmEnhancedValue);
            //    Debug.Log("terrainCompatCorrection = " + terrainCompatCorrection);
            //    Debug.Log("growthAppropriate = " + growthAppropriate);
            //    Debug.Log("trainingOutcomeCorrection = " + trainingOutcomeCorrection);
            //    Debug.Log("growthStageCorrection = " + growthStageCorrection);
            //    Debug.Log("injuryCorrection = " + injuryCorrection);
            //    Debug.Log("diseasesCorrection = " + diseasesCorrection);
            //    Debug.Log("ranValue = " + ranValue);
            //    Debug.Log("result = " + result);
            //}

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Parameter increase");
            DebugPopup.LogRaise($"Basic parameter increase value: T = {basicTrainingMainParameter}");
            DebugPopup.LogRaise($"Farm training boost value: E = {farmEnhancedValue}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Personality Trait Correction: TC = {personalityTraitCorrection}");
            DebugPopup.LogRaise($"Injury correction: IC = {injuryCorrection}");
            DebugPopup.LogRaise($"Sickness correction: SC = {diseasesCorrection}");
            DebugPopup.LogRaise($"Monster growth potential rank = {parameterGrowthAptitudeRank}");
            DebugPopup.LogRaise($"Monster growth potential correction: GP = {growthAppropriate}");
            DebugPopup.LogRaise($"Training Outcome: O = {trainingOutcomeCorrection}");
            DebugPopup.LogRaise($"Growth stage correction: G = {growthStageCorrection}");
            DebugPopup.LogRaise($"current LifeSpan = {currentLifeSpan}");
            DebugPopup.LogRaise($"max LifeSpan = {maxLifeSpan}");
            DebugPopup.LogRaise($"Final increment value: R = {result}");

            result = Mathf.Round(result);

            return result;
        }

        /// <summary>
        /// <para> Parameter reduction formula </para>
        /// <para> Basic parameter increment: T (Training Effects) </para>
        /// <para> Farm Training Enhancement Value: E (Enhanced value) </para>
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Injury correction: IC (Injury correction) </para>
        /// <para> Disease correction: SC (Sickness correction) </para>
        /// <para> <para> Monster growth aptitude correction: GP (Growth potential) </para>
        /// <para> Training Outcome: O (Outocome) </para>
        /// <para> Growth Stage Correction: G (Growth correction) </para>
        /// <para> Last increment: R(Result) </para>
        /// <para> R = T × ( 1 - E × 0.3 / 100 + C + CC + IC + SC + GP ) × ( 1 + G ) × O × 0.95~1.05 </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Training_Parameter_Rise_Down(float basicTrainingMainParameter, RankTypeEnums parameterGrowthAptitudeRank, float farmEnhancedValue, int mainStat, int subStat, float personalityTraitCorrection, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType, Monster.Monster.GrowthType growthType, float currentLifeSpan, float maxLifeSpan, BasicParameters currentBasicParameters, TrainingType trainingType)
        {
            float injuryCorrection = 0f;
            if (injuryType == Monster.Monster.InjuryType.Injury || injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                injuryCorrection = Get_Training_Injured_Probability_Rate_By_Stat(3);
            }

            float diseasesCorrection = 0f;
            if (diseasesType == Monster.Monster.DiseasesType.Diseases || diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                diseasesCorrection = Get_Training_Diseases_Probability_Rate_By_Stat(3);
            }

            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float growthAppropriate = Monster.Monster.Get_Growth_Aptitude_Decrease_Correction_Value(parameterGrowthAptitudeRank);

            float trainingOutcomeCorrection = Get_Training_Down_Percent(trainingType);

            float growthStageCorrection = Monster.Monster.Get_Growth_Stage_Training_Correction(growthType, currentLifeSpan, maxLifeSpan);

            float ranValue = UnityEngine.Random.Range(0.95f, 1.05f);

            float result = basicTrainingMainParameter * (1f - farmEnhancedValue * 0.3f / 100f - terrainCompatCorrection - personalityTraitCorrection - growthAppropriate - injuryCorrection - diseasesCorrection) * (1f + growthStageCorrection) * trainingOutcomeCorrection * ranValue;

            //if (basicTrainingMainParameter != 0f)
            //{
            //    Debug.Log("Get_Training_Parameter_Rise_Down");
            //    Debug.Log("basicTrainingMainParameter = " + basicTrainingMainParameter);
            //    Debug.Log("farmEnhancedValue = " + farmEnhancedValue);
            //    Debug.Log("terrainCompatCorrection = " + terrainCompatCorrection);
            //    Debug.Log("growthAppropriate = " + growthAppropriate);
            //    Debug.Log("trainingOutcomeCorrection = " + trainingOutcomeCorrection);
            //    Debug.Log("growthStageCorrection = " + growthStageCorrection);
            //    Debug.Log("injuryCorrection = " + injuryCorrection);
            //    Debug.Log("diseasesCorrection = " + diseasesCorrection);
            //    Debug.Log("ranValue = " + ranValue);
            //    Debug.Log("result = " + result);
            //}

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Parameter reduction");
            DebugPopup.LogRaise($"Basic parameter increase value: T = {basicTrainingMainParameter}");
            DebugPopup.LogRaise($"Farm training boost value: E = {farmEnhancedValue}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Personality Trait Correction: TC = {personalityTraitCorrection}");
            DebugPopup.LogRaise($"Injury correction: IC = {injuryCorrection}");
            DebugPopup.LogRaise($"Sickness correction: SC = {diseasesCorrection}");
            DebugPopup.LogRaise($"Monster growth potential rank = {parameterGrowthAptitudeRank}");
            DebugPopup.LogRaise($"Monster growth potential correction: GP = {growthAppropriate}");
            DebugPopup.LogRaise($"Training Outcome: O = {trainingOutcomeCorrection}");
            DebugPopup.LogRaise($"Growth stage correction: G = {growthStageCorrection}");
            DebugPopup.LogRaise($"current LifeSpan = {currentLifeSpan}");
            DebugPopup.LogRaise($"max LifeSpan = {maxLifeSpan}");
            DebugPopup.LogRaise($"Final increment value: R = {result}");

            result = Mathf.Round(result);

            return result;
        }

        /// <summary>
        /// <para> Training probability calculation formula </para>
        /// <para> Base failure rate: 15% </para>
        /// <para> Basic big success rate: 3%. </para>
        /// <para> Final Success Rate: S(Success) </para>
        /// <para> Final Huge Success Rate: HS (Huge success). </para>
        /// <para> Final failure rate: F(Failure) </para>
        /// <para> Correction of training policy: T (Traning policy) </para>
        /// <para> Affection: L (Love) </para>
        /// <para> Huge success characteristic correction (HSCC) </para>
        /// <para> Failure characteristic correction (FCC) </para>
        /// <para> Injury correction: IC (Injury correction) </para>
        /// <para> Disease correction: SC (Sickness correction) </para>
        /// <para> HS = 3% + T + L / 2 + HSCC + IC + SC </para>
        /// <para> F = 15% + T -  L + FCC + IC - SC </para>
        /// <para> S = 100% - HS - F </para>
        /// </summary>
        /// <param name="trainingType"></param>
        /// <returns></returns>
        public static TrainingType Get_Training_Sucess_Failure_Probability(float trainingPolicyValue, float affectionValue, float hugeSuccessTraitCorrection, float failureTraitCorrection, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType)
        {
            Debug.Log("Get_Training_Sucess_Failure_Probability");

            Debug.Log("affectionValue = " + affectionValue);
            Debug.Log("trainingPolicyValue = " + trainingPolicyValue);

            float hsRate = Get_Training_Basic_Success_Failure_Rate(TrainingType.huge_success) / 100f;

            Debug.Log("Basic hsRate = " + hsRate);

            float fRate = Get_Training_Basic_Success_Failure_Rate(TrainingType.failure) / 100f;

            Debug.Log("Basic fRate = " + fRate);

            float success_trainingPolicyCorrection = Monster.Monster.Get_TrainingPolicy_Reduction_HugeSuccess_Rate(trainingPolicyValue);
            Debug.Log("success_trainingPolicyCorrection = " + success_trainingPolicyCorrection);

            float failure_trainingPolicyCorrection = Monster.Monster.Get_TrainingPolicy_Reduction_Training_Failure_Rate(trainingPolicyValue);
            Debug.Log("failure_trainingPolicyCorrection = " + success_trainingPolicyCorrection);

            float hsInjuryCorrection = 0f;
            float fInjuryCorrection = 0f;

            if (injuryType == Monster.Monster.InjuryType.Injury || injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                hsInjuryCorrection = -Get_Training_Injured_Probability_Rate_By_Stat(3);
                fInjuryCorrection = Get_Training_Injured_Probability_Rate_By_Stat(2);

                Debug.Log("hsInjuryCorrection = " + hsInjuryCorrection);
                Debug.Log("fInjuryCorrection = " + fInjuryCorrection);
            }

            float hsDiseasesCorrection = 0f;
            float fDiseasesCorrection = 0f;
            if (diseasesType == Monster.Monster.DiseasesType.Diseases || diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                hsDiseasesCorrection = -Get_Training_Diseases_Probability_Rate_By_Stat(3);
                fDiseasesCorrection = Get_Training_Diseases_Probability_Rate_By_Stat(2);

                Debug.Log("hsDiseasesCorrection = " + hsDiseasesCorrection);
                Debug.Log("fDiseasesCorrection = " + fDiseasesCorrection);
            }

            float hs = hsRate + success_trainingPolicyCorrection + Mathf.Sqrt(affectionValue) / 2f / 100 + hugeSuccessTraitCorrection + (hsInjuryCorrection + hsDiseasesCorrection);

            float f = fRate + failure_trainingPolicyCorrection - Mathf.Sqrt(affectionValue) / 100 + failureTraitCorrection - (fInjuryCorrection + fDiseasesCorrection);

            float s = 1f - hs - f;

            float trueHS = hs * 100f;
            float trueF = f * 100f;
            float trueS = s * 100f;

            Debug.Log("hs = " + hs);
            Debug.Log("f = " + f);
            Debug.Log("s = " + s);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Training probability");
            DebugPopup.LogRaise($"Base failure rate = {fRate}");
            DebugPopup.LogRaise($"Basic huge success rate = {hsRate}");
            DebugPopup.LogRaise($"Final success rate: S = {s}");
            DebugPopup.LogRaise($"Final Huge Success Rate: HS = {hs}");
            DebugPopup.LogRaise($"Final failure rate: F = {f}");
            DebugPopup.LogRaise($"Training policy correction: T");
            DebugPopup.LogRaise($"success trainingPolicy Correction = {success_trainingPolicyCorrection}");
            DebugPopup.LogRaise($"failure trainingPolicy Correction = {failure_trainingPolicyCorrection}");
            DebugPopup.LogRaise($"huge sucess Injury Correction = {hsInjuryCorrection}");
            DebugPopup.LogRaise($"failure Injury Correction = {fInjuryCorrection}");
            DebugPopup.LogRaise($"huge sucess Diseases Correction = {hsDiseasesCorrection}");
            DebugPopup.LogRaise($"failure Diseases Correction = {fDiseasesCorrection}");
            DebugPopup.LogRaise($"Degree of Friendship: L = {affectionValue}");
            DebugPopup.LogRaise($"Huge success trait correction: HSTC = {hugeSuccessTraitCorrection}");
            DebugPopup.LogRaise($"Failure Trait correction: FTC = {failureTraitCorrection}");

            return Get_Training_Random_Success_Failure(trueS, trueHS, trueF);
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> 0 = 10% increase decreases </para>
        /// <para> 1 = 10% decrease increases </para>
        /// <para> 2 = increases the probability of failure by 10% </para>
        /// <para> 3 = decreases the probability of success by 10% </para>
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public static float Get_Training_Injured_Probability_Rate_By_Stat(int stat)
        {
            switch (stat)
            {
                case 0:
                    {
                        return 0.1f;
                    }

                case 1:
                    {
                        return 0.1f;
                    }

                case 2:
                    {
                        return 0.1f;
                    }

                case 3:
                    {
                        return 0.1f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> 0 = 10% increase decreases </para>
        /// <para> 1 = 10% decrease increases </para>
        /// <para> 2 = increases the probability of failure by 10% </para>
        /// <para> 3 = decreases the probability of success by 10% </para>
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        public static float Get_Training_Diseases_Probability_Rate_By_Stat(int stat)
        {
            switch (stat)
            {
                case 0:
                    {
                        return 0.1f;
                    }

                case 1:
                    {
                        return 0.1f;
                    }

                case 2:
                    {
                        return 0.1f;
                    }

                case 3:
                    {
                        return 0.1f;
                    }
            }

            return 0f;
        }

        #endregion

        #region

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Traits Acquisition or Level Up Calculation Formula </para>
        /// <para> Monster's reference parameter 1: MP1 (Monster's parameter1) </para>
        /// <para> Monster's reference parameter 2: MP2 (Monster's parameter2) </para>
        /// <para> Monster's reference parameter 3: MP3 (Monster's parameter3) </para>
        /// <para> Monster's reference parameter 4: MP4 (Monster's parameter4) </para>
        /// <para> Monster's reference parameter 5: MP5 (Monster's parameter5) </para>
        /// <para> Monster's reference parameter 6: MP6 (Monster's parameter6) </para>
        /// <para> Requested Parameter 1: PR1 (Requested Parameter1) </para>
        /// <para> Requested Parameter 2: PR2 (Requested Parameter2) </para>
        /// <para> Requested Parameter 3: PR3 (Requested Parameter3) </para>
        /// <para> Requested Parameter 4: PR4 (Requested Parameter4) </para>
        /// <para> Requested Parameter 5: PR5 (Requested Parameter5) </para>
        /// <para> Requested Parameter 6: PR6 (Requested Parameter6) </para>
        /// <para> Number of request parameters: N (Number of request parameters) </para>
        /// <para> Trait Correction: TC (Trait Correction) </para>
        /// <para> Acquisition or level increase probability: R (Result) </para>
        /// <para> R = ( MIN ( 1, MP1 / PR1 ) + MIN ( 1, MP2 / PR2 ) + MIN ( 1, MP3 / PR3 ) + MIN ( 1, MP4 / PR4 ) + MIN ( 1, MP5 / PR5 ) + MIN ( 1, MP6 / PR6 ) ) / N × ( 1 + TC ) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Intensive_Acquisition_Level_Up_Rate(BasicParameters monsterComplexParameter, BasicParameters terrainParameters, BasicParameters requestParameters, float traitCorrection)
        {
            float HPxHPR = 0f;
            float STRxSTRR = 0f;
            float INTxINTR = 0f;
            float DEXxDEXR = 0f;
            float AGIxAGIR = 0f;
            float VITxVITR = 0f;

            int numberOfRequestPara = 0;

            if (terrainParameters.health != 0f)
            {
                HPxHPR = Mathf.Min(1f, monsterComplexParameter.health / requestParameters.health);

                DebugPopup.LogRaise($"health = {monsterComplexParameter.health}");
                DebugPopup.LogRaise($"Request health = {requestParameters.health}");

                numberOfRequestPara++;
            }
            if (terrainParameters.strenght != 0f)
            {
                STRxSTRR = Mathf.Min(1f, monsterComplexParameter.strenght / requestParameters.strenght);

                DebugPopup.LogRaise($"strenght = {monsterComplexParameter.strenght}");
                DebugPopup.LogRaise($"Request strenght = {requestParameters.strenght}");

                numberOfRequestPara++;
            }
            if (terrainParameters.intelligent != 0f)
            {
                INTxINTR = Mathf.Min(1f, monsterComplexParameter.intelligent / requestParameters.intelligent);

                DebugPopup.LogRaise($"intelligent = {monsterComplexParameter.intelligent}");
                DebugPopup.LogRaise($"Request intelligent = {requestParameters.intelligent}");

                numberOfRequestPara++;
            }
            if (terrainParameters.dexterity != 0f)
            {
                DEXxDEXR = Mathf.Min(1f, monsterComplexParameter.dexterity / requestParameters.dexterity);

                DebugPopup.LogRaise($"dexterity = {monsterComplexParameter.dexterity}");
                DebugPopup.LogRaise($"Request dexterity = {requestParameters.dexterity}");

                numberOfRequestPara++;
            }
            if (terrainParameters.agility != 0f)
            {
                AGIxAGIR = Mathf.Min(1f, monsterComplexParameter.agility / requestParameters.agility);

                DebugPopup.LogRaise($"agility = {monsterComplexParameter.agility}");
                DebugPopup.LogRaise($"Request agility = {requestParameters.agility}");

                numberOfRequestPara++;
            }
            if (terrainParameters.vitality != 0f)
            {
                VITxVITR = Mathf.Min(1f, monsterComplexParameter.vitality / requestParameters.vitality);

                DebugPopup.LogRaise($"vitality = {monsterComplexParameter.vitality}");
                DebugPopup.LogRaise($"Request vitality = {requestParameters.vitality}");

                numberOfRequestPara++;
            }

            DebugPopup.LogRaise($"Number of request parameters: N = {numberOfRequestPara}");

            DebugPopup.LogRaise($"Trait Correction: TC = {traitCorrection}");

            float result = (HPxHPR + STRxSTRR + INTxINTR + DEXxDEXR + AGIxAGIR + VITxVITR) / numberOfRequestPara * (1f + traitCorrection);

            DebugPopup.LogRaise($"Acquisition or level increase probability: R = {result}");

            return result;
        }

        #endregion

        #region Exploration

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> AA = 30%  × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// <para> AB = 10% × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// <para> AC = 5% × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Exploration_Item_Basic_Probability(ExplorationItem.ItemAcquisitionType itemAcquisitionType)
        {
            float correctionRate = 0f;

            switch (itemAcquisitionType)
            {
                case ExplorationItem.ItemAcquisitionType.a:
                    {
                        correctionRate = 0.3f;

                        break;
                    }

                case ExplorationItem.ItemAcquisitionType.b:
                    {
                        correctionRate = 0.1f;

                        break;
                    }

                case ExplorationItem.ItemAcquisitionType.c:
                    {
                        correctionRate = 0.05f;

                        break;
                    }
            }

            return correctionRate;
        }

        /// <summary>
        /// <para> iTemFoundRate: 100% = 1f </para>
        /// <para> rank2InscreaRate: 100% = 100f </para>
        /// <para> rank3InscreaRate: 100% = 100f </para>
        /// </summary>
        /// <returns></returns>
        public static ExplorationItem Get_Exploration_Item_Found(ExplorationItem.ItemAcquisitionType itemAcquisitionType, float iTemFoundRate, float rank2InscreaRate, float rank3InscreaRate)
        {
            float ran = UnityEngine.Random.Range(0f, 1f);

            if (ran <= iTemFoundRate)
            {
                ExplorationItem foundItem = new()
                {
                    discoveryRank = 1
                };

                float upToRank2Rate = iTemFoundRate * (rank2InscreaRate / 100f);

                ran = UnityEngine.Random.Range(0f, iTemFoundRate);

                if (ran <= upToRank2Rate)
                {
                    foundItem.discoveryRank = 2;

                    float upToRank3Rate = upToRank2Rate * (rank3InscreaRate / 100f);

                    ran = UnityEngine.Random.Range(0f, upToRank2Rate);

                    if (ran <= upToRank3Rate)
                    {
                        foundItem.discoveryRank = 3;
                    }
                }

                return foundItem;
            }

            return null;
        }

        /// <summary>
        /// <para> Item discovery rate calculation formula </para>
        /// 
        /// <para> Item A Probability of Discovery : AA (Acquisition A) </para>
        /// <para> Item A Probability of Discovery : AB (Acquisition B) </para>
        /// <para> Item A Probability of Discovery : AC (Acquisition C) </para>
        /// <para> HP : MonsterHP </para>
        /// <para> STR : MonsterSTR </para>
        /// <para> INT : MonsterINT </para>
        /// <para> DEX : MonsterDEX </para>
        /// <para> AGI : MonsterAGI </para>
        /// <para> VIT : MonsterVIT </para>
        /// <para> HP </para>
        /// <para> STR </para>
        /// <para> INT </para>
        /// <para> DEX </para>
        /// <para> AGI </para>
        /// <para> VIT </para>
        /// <para> Recommended total parameters: RP (Recommended total parameters) </para>
        /// <para> Parameter correction: PC (Total parameter correction) </para>
        /// <para> Acquisition probability correction: AP (Acquisition probability correction) </para>
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para>
        /// <para> Injury correction: IC (Injury correction) </para>
        /// <para> Disease correction: SC (Sick correction) </para>
        /// <para> Number of discovery tickets consumed: DT (Number of discovery tickets consumed) </para>
        /// <para> Item acquisition characteristic correction: IA (Item acquisition compensation) </para>
        /// <para> PC = ( HP × HPC + STR × STRC + INT  × INTC + DEX × DEXC + AGI × AGIC + VIT × VITC ) × ( 1+ IC + SC )  / RP -1 </para>
        /// <para> IF -0.4 <= PC + C <= 0.4 </para>
        /// <para> AP = PC + C </para>
        /// <para> IF PC + C > 0.4 </para>
        /// <para> AP = 0.4 </para>
        /// <para> IF PC + C < -0.4 </para>
        /// <para> AP = -0.4 </para>
        /// <para> AA = 30%  × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// <para> AB = 10% × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// <para> AC = 5% × ( 1 + AP + 0.2 × DT + IA ) </para>
        /// 
        /// <para> Item rank increase rate calculation formula </para>
        /// 
        /// <para> Probability of rank 2 increase: R2 (Probability of rank 2 increase) </para> 
        /// <para> Probability of rank 3 increase: R3 (Probability of rank 3 increase) </para> 
        /// <para> Rank 2 increase tickets consumed: RT2 (Number of rank 2 increase tickets consumed) </para> 
        /// <para> Rank 3 increase tickets consumed: RT3 (Number of rank 3 increase tickets consumed) </para>
        /// <para> Rank 2 increase characteristic correction (characteristic): RI2 (Rank 2 increase compensation) </para>
        /// <para> Rank 3 increase characteristic correction (characteristic): RI3 (Rank 3 increase compensation) </para>
        /// <para> R2 = 20% + RI2 + 20% × RT2 </para>
        /// <para> R3 = 20% + RI3 + 20% × RT3 </para>
        /// <para> return value is 100% = 1f </para>
        /// </summary>
        /// <param name="monsterParameters"></param>
        /// <param name="explorationParameterCorrections"></param>
        /// <param name="injuryCorrection"></param>
        /// <param name="diseaseCorrection"></param>
        /// <param name="numberOfDiscoveryTicketsConsumed"></param>
        /// <param name="itemAcquisitionTraitCorrection"></param>
        /// <returns></returns>
        public static float Get_Exploration_Item_Discovery_Probability
            (
            BasicParameters monsterParameters, BasicParameters explorationParameterCorrections, float recommendedTotal,
            float injuryCorrection, Monster.Monster.InjuryType injuryType, float diseaseCorrection, Monster.Monster.DiseasesType diseasesType,
            int mainStat, int subStat,
            float numberOfDiscoveryTicketsConsumed, float itemAcquisitionTraitCorrection,
            ExplorationItem.ItemAcquisitionType itemAcquisitionType
            )
        {
            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float HPxHPC = monsterParameters.health * explorationParameterCorrections.health;
            float STRxSTRC = monsterParameters.strenght * explorationParameterCorrections.strenght;
            float INTxINTC = monsterParameters.intelligent * explorationParameterCorrections.intelligent;
            float DEXxDEXC = monsterParameters.dexterity * explorationParameterCorrections.dexterity;
            float AGIxAGIC = monsterParameters.agility * explorationParameterCorrections.agility;
            float VITxVITC = monsterParameters.vitality * explorationParameterCorrections.vitality;

            float totalParameters = HPxHPC + STRxSTRC + INTxINTC + DEXxDEXC + AGIxAGIC + VITxVITC;

            totalParameters /= 100f;

            float PC = totalParameters * (1f + injuryCorrection + diseaseCorrection) / recommendedTotal - 1f;

            Debug.Log("Get_Exploration_Item_Discovery PC = " + PC);

            float AP = 0f;

            if (PC + terrainCompatCorrection < -0.4f)
            {
                AP = -0.4f;
            }
            else if (PC + terrainCompatCorrection > 0.4f)
            {
                AP = 0.4f;
            }
            else
            {
                AP = PC + terrainCompatCorrection;
            }

            float itemFoundRate = Get_Exploration_Item_Basic_Probability(itemAcquisitionType) * (1f + AP + 0.2f * numberOfDiscoveryTicketsConsumed + itemAcquisitionTraitCorrection);

            Debug.Log("Item found rate = " + itemFoundRate);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Item discovery rate");
            DebugPopup.LogRaise("Item discovery probability = " + Get_Exploration_Item_Basic_Probability(itemAcquisitionType));
            DebugPopup.LogRaise($"hp = {monsterParameters.health}");
            DebugPopup.LogRaise($"str = {monsterParameters.strenght}");
            DebugPopup.LogRaise($"int = {monsterParameters.intelligent}");
            DebugPopup.LogRaise($"dex = {monsterParameters.dexterity}");
            DebugPopup.LogRaise($"agi = {monsterParameters.agility}");
            DebugPopup.LogRaise($"vit = {monsterParameters.vitality}");
            DebugPopup.LogRaise($"hp correction = {explorationParameterCorrections.health}");
            DebugPopup.LogRaise($"str correction = {explorationParameterCorrections.strenght}");
            DebugPopup.LogRaise($"int correction = {explorationParameterCorrections.intelligent}");
            DebugPopup.LogRaise($"dex correction = {explorationParameterCorrections.dexterity}");
            DebugPopup.LogRaise($"agi correction = {explorationParameterCorrections.agility}");
            DebugPopup.LogRaise($"vit correction = {explorationParameterCorrections.vitality}");
            DebugPopup.LogRaise($"Recommended total parameters: RP = {recommendedTotal}");
            DebugPopup.LogRaise($"Acquisition probability correction: AP = {AP}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Injury correction: IC = {injuryCorrection}");
            DebugPopup.LogRaise($"Sick correction: SC = {diseaseCorrection}");
            DebugPopup.LogRaise($"Number of discovery tickets consumed: DT = {numberOfDiscoveryTicketsConsumed}");
            DebugPopup.LogRaise($"Item Acquisition Compensation: IA = {itemAcquisitionTraitCorrection}");
            DebugPopup.LogRaise($"Item found rate = {itemFoundRate}");

            return itemFoundRate;
        }

        /// <summary>
        /// <para> RT2: is number of rank 2 ticket </para>
        /// <para> RT3: is number of rank 3 ticket </para>
        /// <para> RI2: 100% = 100f </para>
        /// <para> RI3: 100% = 100f </para>
        /// <para> vector2.x = r2 </para>
        /// <para> vector2.y = r3 </para>
        /// </summary>
        /// <param name="RT2"></param>
        /// <param name="RT3"></param>
        /// <param name="RI2"></param>
        /// <param name="RI3"></param>
        /// <returns></returns>
        public static Vector2 Get_Exploration_Item_Rank_Increase_Rate(float RT2, float RT3, float RI2, float RI3)
        {
            float r2 = 20f + RI2 + 20f * RT2;
            float r3 = 20f + RI3 + 20f * RT3;

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Item rank increase rate");
            DebugPopup.LogRaise($"Rank 2 increase probability: R2 = {r2}");
            DebugPopup.LogRaise($"Rank 3 increase probability: R3 = {r3}");
            DebugPopup.LogRaise($"Number of rank 2 increase tickets consumed: RT2 = {RT2}");
            DebugPopup.LogRaise($"Number of rank 3 increase tickets consumed: RT3 = {RT3}");
            DebugPopup.LogRaise($"Rank 2 increase compensation (trait): RI2 = {RI2}");
            DebugPopup.LogRaise($"Rank 3 increase compensation (trait): RI3 = {RI3}");

            return new Vector2(r2, r3);
        }

        #region Distress

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> 0 = If you are injured, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> 1 = If you are sick, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> 2 = Even if you are seriously injured, you will not be forced to return, and the parameters at the time of judgment will be reduced by 20%. </para>
        /// <para> 3 = Even if you are seriously ill, you will not be forced to return, and the parameters at the time of judgment will be reduced by 20%. </para>
        /// </summary>
        public static float Get_Exploration_Distress_Reduced_Percent_By_Stat(int stat)
        {
            switch (stat)
            {
                case 0:
                    {
                        return 0.1f;
                    }

                case 1:
                    {
                        return 0.1f;
                    }

                case 2:
                    {
                        return 0.2f;
                    }

                case 3:
                    {
                        return 0.2f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// 100% = 1f
        /// </summary>
        /// <returns></returns>
        public static float Get_Exploration_Distress_Basic_Lost_Probability()
        {
            return 0.02f;
        }

        /// <summary>
        /// 100% = 1f
        /// </summary>
        /// <returns></returns>
        public static float Get_Exploration_Distress_Basic_Return_Probability()
        {
            return 0.5f;
        }

        /// <summary>
        /// <para> return value 100f = 1f </para>
        /// <para> Distress probability calculation formula </para>
        /// <para> Basic Distress Chance: 2% </para>
        /// <para> Basic return probability: 50% formula </para>
        /// <para> PC = ( ( HP × HPC + STR × STRC + INT × INTC + DEX × DEXC + AGI × AGIC + VIT × VITC ) × ( 1 + C ) )^3 / RP^3 </para>
        /// <para> L = 2% / PC x (1 - CC - 0.05 x DT) </para>
        /// <para> R1 = MIN( 100% , 50% × PC × ( 1 + CC + 0.05 × DT ) ) </para>
        /// <para> R2 = MIN( 100% , R1 * 1.15 ) </para>
        /// <para> R3 = MIN( 100% , R2 * 1.15 ) </para>
        /// <para> Repeat until found </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Exploration_Distress_Total_Parameter_Correction
            (
            BasicParameters monsterComplexParameters, BasicParameters explorationParameterCorrections, float recommendedTotal,
            int mainStat, int subStat
            )
        {
            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float HPxHPC = monsterComplexParameters.health * explorationParameterCorrections.health;
            float STRxSTRC = monsterComplexParameters.strenght * explorationParameterCorrections.strenght;
            float INTxINTC = monsterComplexParameters.intelligent * explorationParameterCorrections.intelligent;
            float DEXxDEXC = monsterComplexParameters.dexterity * explorationParameterCorrections.dexterity;
            float AGIxAGIC = monsterComplexParameters.agility * explorationParameterCorrections.agility;
            float VITxVITC = monsterComplexParameters.vitality * explorationParameterCorrections.vitality;

            float totalParameters = HPxHPC + STRxSTRC + INTxINTC + DEXxDEXC + AGIxAGIC + VITxVITC;

            totalParameters /= 100f;

            double PC = Math.Pow(totalParameters * (1 + terrainCompatCorrection), 3) / Math.Pow(recommendedTotal, 3);

            //double PC = Math.Pow(((500 * 1.3) + (450 * 1) + (200 * 1.2) + (450 * 1) + (280 * 1) + (500 * 1.2)) * (1 + 0.03), 3) / Math.Pow(2200, 3);

            //Debug.Log("PC = " + PC);

            Debug.Log("Get_Exploration_Distress_Total_Parameter_Correction = " + (float)PC);

            return (float)PC;
        }

        /// <summary>
        /// <para> Total_Parameter_Correction: 100% = 1f </para>
        /// <para> L = 2% / PC x (1 - CC - 0.05 x DT) </para>
        /// <para> If you are injured, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> If you are sick, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> Even if you are seriously injured or seriously ill, you will not be forced to return, and the parameters at the time of judgment will be reduced by 20%. </para>
        /// </summary>
        /// <param name="Total_Parameter_Correction"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static bool Get_Exploration_Distress_Is_Lost(float Total_Parameter_Correction, float traitCorrection, float numberOfTicketConsumed, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType)
        {
            float basicLostProbability = Get_Exploration_Distress_Basic_Lost_Probability();

            float L = basicLostProbability / Total_Parameter_Correction * (1f - traitCorrection - 0.05f * numberOfTicketConsumed);

            L *= 100f;

            float totalCorrection = 0f;

            if (injuryType == Monster.Monster.InjuryType.Injury)
            {
                totalCorrection += 0.1f;
            }
            else
            if (injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                totalCorrection += 0.2f;
            }

            if (diseasesType == Monster.Monster.DiseasesType.Diseases)
            {
                totalCorrection += 0.1f;
            }
            else
            if (diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                totalCorrection += 0.2f;
            }

            L += L * totalCorrection;

            Debug.Log("Lost Rate = " + L);

            float ran = UnityEngine.Random.Range(0f, 100f);

            if (ran <= L)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// <para> Total_Parameter_Correction: 100% = 1f </para>
        /// <para> R2 = MIN( 100% , R1 * 1.15 ) </para>
        /// <para> R3 = MIN( 100% , R2 * 1.15 )</para>
        /// <para> Repeat until found </para>
        /// <para> If you are injured, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> If you are sick, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> Even if you are seriously injured or seriously ill, you will not be forced to return, and the parameters at the time of judgment will be reduced by 20%. </para>
        /// </summary>
        /// <param name="Total_Parameter_Correction"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Exploration_Distress_First_Week_Return_Probability(float Total_Parameter_Correction, float traitCorrection, float numberOfTicketConsumed, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType)
        {
            float basicReturnProbability = Get_Exploration_Distress_Basic_Return_Probability();

            float caculatedR = basicReturnProbability * Total_Parameter_Correction * (1f + traitCorrection + 0.05f * numberOfTicketConsumed);

            float totalCorrection = 0f;

            if (injuryType == Monster.Monster.InjuryType.Injury)
            {
                totalCorrection += 0.1f;
            }
            else
            if (injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                totalCorrection += 0.2f;
            }

            if (diseasesType == Monster.Monster.DiseasesType.Diseases)
            {
                totalCorrection += 0.1f;
            }
            else
            if (diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                totalCorrection += 0.2f;
            }

            caculatedR -= caculatedR * totalCorrection;

            float R = MathF.Min(1f, caculatedR);

            Debug.Log("First_Week_Return Rate = " + R);

            return R;
        }

        /// <summary>
        /// <para> Total_Parameter_Correction: 100% = 1f </para>
        /// <para> R2 = MIN( 100% , R1 * 1.15 ) </para>
        /// <para> R3 = MIN( 100% , R2 * 1.15 )</para>
        /// <para> Repeat until found </para>
        /// <para> If you are injured, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> If you are sick, the parameters at the time of judgment are reduced by 10%. </para>
        /// <para> Even if you are seriously injured or seriously ill, you will not be forced to return, and the parameters at the time of judgment will be reduced by 20%. </para>
        /// </summary>
        /// <param name="firstWeek"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Exploration_Distress_N_Week_Return_Probability(float firstWeekProbability, int weekTurn, Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType)
        {
            float caculatedPC = firstWeekProbability;

            for (int i = 0; i < weekTurn; i++)
            {
                caculatedPC = caculatedPC * 1.15f;
            }

            float totalCorrection = 0f;

            if (injuryType == Monster.Monster.InjuryType.Injury)
            {
                totalCorrection += 0.1f;
            }
            else
            if (injuryType == Monster.Monster.InjuryType.Serious_Injury)
            {
                totalCorrection += 0.2f;
            }

            if (diseasesType == Monster.Monster.DiseasesType.Diseases)
            {
                totalCorrection += 0.1f;
            }
            else
            if (diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
            {
                totalCorrection += 0.2f;
            }

            caculatedPC -= caculatedPC * totalCorrection;

            float R = MathF.Min(1f, caculatedPC);

            Debug.Log("Return " + weekTurn + " Rate = " + R);

            return R;
        }

        /// <summary>
        /// <para> Return_Probability: 100% = 1f </para>
        /// <para> R2 = MIN( 100% , R1 * 1.15 ) </para>
        /// <para> R3 = MIN( 100% , R2 * 1.15 )</para>
        /// <para> Repeat until found </para>
        /// </summary>
        /// <param name="Return_Probability"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static bool Get_Exploration_Distress_Is_Return(float Return_Probability, float traitCorrection, float numberOfTicketConsumed)
        {
            Return_Probability *= 100f;

            float ran = UnityEngine.Random.Range(0f, 100f);

            if (ran <= Return_Probability)
            {
                return true;
            }

            return false;
        }



        #endregion

        #endregion
    }
}