using UnityEngine;

namespace Assets.Scripts.TrainingArea
{
    [CreateAssetMenu(fileName = "TrainingAreaDataScriptObj", menuName = "ScriptableObjects/TrainingArea/TrainingAreaDataScriptObj", order = 1)]
    public class TrainingAreaDataScriptObj : ScriptableObject
    {
        [SerializeField]
        private TrainingArea trainingArea;

        public TrainingArea TrainingArea { get => trainingArea; private set => trainingArea = value; }
    }
}