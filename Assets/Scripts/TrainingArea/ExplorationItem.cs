﻿using System;

namespace Assets.Scripts.TrainingArea
{
    [Serializable]
    public class ExplorationItem
    {
        public enum ItemAcquisitionType { a, b, c }

        public ItemAcquisitionType itemAcquisitionType;

        //Discovey rank from 1,2,3
        public int discoveryRank;
    }
}