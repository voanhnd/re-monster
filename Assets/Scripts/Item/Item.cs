using System;

[Serializable]
public class Item
{
    public enum ItemType { itemA, itemB, itemC }

    public ItemType itemType;

    public string itemID = "";

    public string nftID = "";
}
