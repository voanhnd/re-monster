using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FarmDataManagerScriptObj", menuName = "ScriptableObjects/Farm/FarmDataManagerScriptObj", order = 1)]
public class FarmDataManagerScriptObj : ScriptableObject
{
    public Dictionary<Farm.FarmType, FarmDataScriptObj> farmDataDictionary = new Dictionary<Farm.FarmType, FarmDataScriptObj>();

    [SerializeField]
    List<FarmDataScriptObj> farmDataList = new List<FarmDataScriptObj>();

    public FarmDataScriptObj GetFarmDataObj(Farm.FarmType farmType)
    {
        FarmDataScriptObj farmDataObj = null;

        if (!farmDataDictionary.TryGetValue(farmType, out farmDataObj))
        {
            // the key isn't in the dictionary.
            return farmDataObj; // or whatever you want to do
        }

        return farmDataObj;
    }

    private void OnValidate()
    {
        if (Application.isEditor && Application.isPlaying == false)
        {
            //Debug.Log("OnValidate");

            CreateFarmDictionary();
        }
    }

    public void CreateFarmDictionary()
    {
        if (farmDataList.Count > 0)
        {
            farmDataDictionary.Clear();

            for (int i = 0; i < farmDataList.Count; i++)
            {
                farmDataDictionary.Add(farmDataList[i].Farm.FarmTypeVal, farmDataList[i]);
            }
        }
    }
}
