using UnityEngine;

[CreateAssetMenu(fileName = "FarmDataScriptObj", menuName = "ScriptableObjects/Farm/FarmDataScriptObj", order = 1)]
public class FarmDataScriptObj : ScriptableObject
{
    [SerializeField]
    private Farm farm;

    public Farm Farm => farm;
}
