using Assets.Scripts.Monster;
using Assets.Scripts.TrainingArea;
using System;
using UnityEngine;

[Serializable]
public class Farm
{
    [Serializable]
    public class FarmEnhanced
    {
        [Range(0f, 100f)]
        public float StartEnhancementValue = 0f;

        public RankTypeEnums aptitudeRank;
    }

    public RankTypeEnums rankType;

    public enum FarmType
    {
        Empty,
        Plain,
        Mountain,
        WildLand
    }

    [SerializeField]
    public FarmType farmType;

    [SerializeField]
    public TrainingArea.TerrainType TerrainType;

    [SerializeField]
    private string farmName;

    [SerializeField]
    private float farmGoldPrice;
    [SerializeField]
    private float farmOASPrice;
    [SerializeField] private Sprite farmBackgroundSprite;

    /// <summary>
    /// The value inside must be limited from 0 to 100
    /// </summary>
    public BasicParameters enhancedParameters;

    [Header("Value in this parameters 100%")]
    public BasicParameters correctionParameters;

    public float fatigueEnhancValue = 0f;
    public float stressEnhancValue = 0f;
    public float restEnhanceValue = 0f;

    [Header("Special Training")]
    public ParameterType mainParameter;
    public ParameterType subParameter;
    public ParameterType deParameter;

    public Sprite img_Potrait;
    public Sprite img_Landscape;

    public FarmType FarmTypeVal => farmType;
    public string FarmName => farmName;
    public float FarmGoldPrice => farmGoldPrice;
    public float FarmOASPrice => farmOASPrice;
    public Sprite FarmBackgroundSprite => farmBackgroundSprite;

    #region Farm Rank

    /// <summary>
    /// <para> They are ranked into F~S according to numbers.  </para>
    /// <para> F : 1~15 </para>
    /// <para> E : 16~30 </para>
    /// <para> D : 31~45 </para>
    /// <para> C : 46~60  </para>
    /// <para> B : 61~75  </para>
    /// <para> A : 76~90  </para>
    /// <para> S : 91~100 </para>
    /// </summary>
    /// <param name="rankType"></param>
    /// <returns></returns>
    public static float Get_Farm_Max_Parameter_Value_ByRank(RankTypeEnums rankType)
    {
        switch (rankType)
        {
            case RankTypeEnums.f:
                {
                    return 15f;
                }

            case RankTypeEnums.e:
                {
                    return 30f;
                }

            case RankTypeEnums.d:
                {
                    return 45f;
                }

            case RankTypeEnums.c:
                {
                    return 60f;
                }

            case RankTypeEnums.b:
                {
                    return 75f;
                }

            case RankTypeEnums.a:
                {
                    return 90f;
                }

            case RankTypeEnums.s:
                {
                    return 100f;
                }
        }

        return 0f;
    }

    /// <summary>
    /// <para> They are ranked into F~S according to numbers.  </para>
    /// <para> F : 1~15 </para>
    /// <para> E : 16~30 </para>
    /// <para> D : 31~45 </para>
    /// <para> C : 46~60  </para>
    /// <para> B : 61~75  </para>
    /// <para> A : 76~90  </para>
    /// <para> S : 91~100 </para>
    /// </summary>
    /// <param name="rankType"></param>
    /// <returns></returns>
    public static RankTypeEnums Get_Farm_Parameter_ByRank_By_Value(float raparameterValue)
    {
        RankTypeEnums selectedRankType = RankTypeEnums.f;

        if (raparameterValue <= 15f)
        {
            selectedRankType = RankTypeEnums.f;
        }
        else if (raparameterValue <= 30f)
        {
            selectedRankType = RankTypeEnums.e;
        }
        else if (raparameterValue <= 45f)
        {
            selectedRankType = RankTypeEnums.d;
        }
        else if (raparameterValue <= 60f)
        {
            selectedRankType = RankTypeEnums.c;
        }
        else if (raparameterValue <= 75f)
        {
            selectedRankType = RankTypeEnums.b;
        }
        else if (raparameterValue <= 90f)
        {
            selectedRankType = RankTypeEnums.a;
        }
        else if (raparameterValue <= 100f)
        {
            selectedRankType = RankTypeEnums.s;
        }

        return selectedRankType;
    }

    /// <summary>
    /// <para> 100f = 1f </para>
    /// <param name="mainStats"> -1=bad, 0=empty 1=good </param>
    /// <param name="subStats"> -1=bad, 0=empty 1=good </param>
    /// </summary>
    /// <returns></returns>
    public static float Get_Terrain_Compatibility_Correction(int mainStats, int subStats)
    {

        if (mainStats == 1 && subStats == 1)
        {
            return 0.1f;
        }
        else
        if (mainStats == 1 && subStats == 0)
        {
            return 0.07f;
        }
        else
        if (mainStats == 1 && subStats == -1)
        {
            return 0.04f;
        }
        else
        if (mainStats == 0 && subStats == 1)
        {
            return 0.03f;
        }
        else
            if (mainStats == 0 && subStats == 0)
        {
            return 0f;
        }
        else
        if (mainStats == 0 && subStats == -1)
        {
            return -0.03f;
        }
        else
        if (mainStats == -1 && subStats == 1)
        {
            return -0.04f;
        }
        else
        if (mainStats == -1 && subStats == 0)
        {
            return -0.07f;
        }
        else
        if (mainStats == -1 && subStats == -1)
        {
            return -0.1f;
        }

        return 0f;
    }

    /// <summary>
    /// <para> -1 = no suit, = 0 normal, 1 = suit </para>
    /// </summary>
    /// <returns></returns>
    public static int Get_Race_SuitTerrain_Stats
        (
        TrainingArea.TerrainType inSuit, TrainingArea.TerrainType inNoSuit, TrainingArea.TerrainType checkTerrain
        )
    {
        int suit = 0;

        if (inSuit != TrainingArea.TerrainType.Empty && inSuit == checkTerrain)
        {
            return 1;
        }
        else if (inNoSuit != TrainingArea.TerrainType.Empty && inNoSuit == checkTerrain)
        {
            return -1;
        }

        return suit;
    }

    #endregion
}
