﻿using UnityEngine;

public class MonsterFarmAnimationControl : MonoBehaviour
{
    private Animator monster_Animator;

    private void Awake()
    {
        monster_Animator = GetComponent<Animator>();
    }

    public void SetMonsterIsTired(bool isTired = false)
    {
        monster_Animator.SetBool("IsTired", isTired);
    }
}