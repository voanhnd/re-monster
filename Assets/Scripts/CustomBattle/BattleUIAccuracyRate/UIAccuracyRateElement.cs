using UnityEngine;
using TMPro;

public class UIAccuracyRateElement : MonoBehaviour
{
    public TextMeshProUGUI rateText;

    public void UpdatePos(Camera worldCam, Camera uiCam, Canvas canvas, Transform target, float accuryRate)
    {
        gameObject.SetActive(true);

        rateText.text = "" + accuryRate + @"%";

        var screen = worldCam.WorldToScreenPoint(target.transform.position);
        screen.z = (canvas.transform.position - uiCam.transform.position).magnitude;
        var position = uiCam.ScreenToWorldPoint(screen);
        transform.position = position;
    }

    public void Deactive()
    {
        gameObject.SetActive(false);
    }
}
