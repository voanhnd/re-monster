using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Skill;
using System.Collections.Generic;
using UnityEngine;

public class UIAccuracyRate : StaticInstance<UIAccuracyRate>
{
    public Transform container;

    public Camera worldCam;
    public Camera uiCam;

    public Canvas canvas;

    [SerializeField] private List<UIAccuracyRateElement> uiAccuracyRateElements = new();

    private void Start()
    {
        SetActive(false);
    }

    public void SetActive(bool active)
    {
        if (active == false && container.gameObject.activeSelf == false)
        {
            for (int i = 0; i < uiAccuracyRateElements.Count; i++)
            {
                UIAccuracyRateElement uIAccuracyRateElement = uiAccuracyRateElements[i];

                uIAccuracyRateElement.Deactive();
            }
        }

        ActiveContainer(active);
    }

    public void SetTargetsAccury(UnitAbility unitAbility, GridUnit InCaster, List<ILevelCell> EditedAbilityCells, ILevelCell hoverCell)
    {
        //Debug.Log("SetTargetsAccury");

        if (unitAbility == null || InCaster == null || EditedAbilityCells == null || EditedAbilityCells.Count < 1 || hoverCell == null)
        {
            ActiveContainer(false);

            return;
        }

        if (unitAbility.GetEffectedTeam() != CustomGameManager.Instance.GetPlayerTeam())
        {
            ActiveContainer(true);

            List<GridUnit> targetList = new();

            for (int i = 0; i < EditedAbilityCells.Count; i++)
            {
                ILevelCell cell = EditedAbilityCells[i];
                GridUnit gridUnit = cell.GetUnitOnCell();

                if (gridUnit != null && gridUnit.IsDead() == false && AbilityParam.IsEffectTeam(InCaster, gridUnit, unitAbility) && !targetList.Contains(gridUnit))
                    targetList.Add(gridUnit);
            }

            if (targetList.Count < 1)
            {
                ActiveContainer(false);

                return;
            }

            int usedAccuracyElementCount = 0;

            for (int i = 0; i < targetList.Count; i++)
            {
                UIAccuracyRateElement uiAccuracyRateElement = uiAccuracyRateElements[usedAccuracyElementCount++];
                float accuryRate = Mathf.FloorToInt(GetAccuryRate(unitAbility, InCaster, targetList[i], hoverCell));
                uiAccuracyRateElement.UpdatePos(worldCam, uiCam, canvas, targetList[i].GameMonster.uiStatsPos, accuryRate);
            }

            for (int i = usedAccuracyElementCount; i < uiAccuracyRateElements.Count; i++)
            {
                uiAccuracyRateElements[i].Deactive();
            }
            return;
        }
        ActiveContainer(false);
    }

    public void ActiveContainer(bool active) => container.gameObject.SetActive(active);

    /// <summary>
    /// <para> Reuturn value will be converted to 100% = 100f </para>
    /// </summary>
    /// <param name="unitAbility"></param>
    /// <param name="InCaster"></param>
    /// <param name="InTarget"></param>
    public float GetAccuryRate(UnitAbility unitAbility, GridUnit InCaster, GridUnit InTarget, ILevelCell hoverCell)
    {
        ParameterInGame atkParameterInGame = InCaster.GameMonster.InGameParameter;
        MonsterInGame monsterAtk = InCaster.GameMonster;
        MonsterDataScriptObj monsterAtkScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.MonsterInfo.monsterID);
        BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();

        ParameterInGame defParameterInGame = InTarget.GameMonster.InGameParameter;
        MonsterInGame monsterDef = InTarget.GameMonster;
        MonsterDataScriptObj monsterDefScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterDef.MonsterInfo.monsterID);
        BasicParameters def_cur_ComplexParameter = defParameterInGame.GetCurrentComplexParameter();

        //GetComplex buff and debuff befor add new buff

        InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
        InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();

        InGameBuffDetail def_InGameBuffDetail = defParameterInGame.GetInGameComplexBuff();
        InGameDeBuffDetail def_InGameDeBuffDetail = defParameterInGame.GetInGameComplexDeBuff();

        //End 

        #region ATK Trait

        DebugPopup.Log("//////////");
        DebugPopup.Log("ATK Trait");
        DebugPopup.Log("__________");
        InnateTrait.InnateTraitInfo atk_innateTraitInfo = atkParameterInGame.GetInnateTrait();
        DebugPopup.Log($"InnateTrait{atk_innateTraitInfo.innateTraitType}");

        List<AcquiredTrait.AcquiredTraitInfo> attackerAcuiredTraits = atkParameterInGame.GetAcquiredTraits();
        for (int i = 0; i < attackerAcuiredTraits.Count; i++)
        {
            AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo = attackerAcuiredTraits[i];
            DebugPopup.Log($"AcquiredTrait{acquiredTraitInfo.acquiredTraitType}");
        }
        AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo = atkParameterInGame.GetAcquiredTraitComplex();

        FarmTrait.FarmTraitInfo atk_farmTraitInfo = atkParameterInGame.GetFarmTrait();
        DebugPopup.Log($"FarmTrait{atk_farmTraitInfo.farmTraitType}");

        #endregion ATK trait

        #region DEF Trait

        DebugPopup.Log("//////////");
        DebugPopup.Log("DEF Trait");
        DebugPopup.Log("__________");
        InnateTrait.InnateTraitInfo def_innateTraitInfo = defParameterInGame.GetInnateTrait();
        DebugPopup.Log($"InnateTrait{def_innateTraitInfo.innateTraitType}");

        List<AcquiredTrait.AcquiredTraitInfo> defenderAcquiredTraits = defParameterInGame.GetAcquiredTraits();
        for (int i = 0; i < defenderAcquiredTraits.Count; i++)
        {
            AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo = defenderAcquiredTraits[i];
            DebugPopup.Log($"AcquiredTrait{acquiredTraitInfo.acquiredTraitType}");
        }
        AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo = defParameterInGame.GetAcquiredTraitComplex();

        FarmTrait.FarmTraitInfo def_farmTraitInfo = defParameterInGame.GetFarmTrait();
        DebugPopup.Log($"FarmTrait{def_farmTraitInfo.farmTraitType}");

        #endregion DEF trait

        //Basic parameter

        float atk_str = Mathf.Clamp(atk_cur_ComplexParameter.strenght + atk_inGameBuffDetail.STR_Inscrease + atk_inGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
        float atk_int = Mathf.Clamp(atk_cur_ComplexParameter.intelligent + atk_inGameBuffDetail.INT_Inscrease + atk_inGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
        float atk_dex = Mathf.Clamp(atk_cur_ComplexParameter.dexterity + atk_inGameBuffDetail.DEX_Inscrease + atk_inGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
        float atk_agi = Mathf.Clamp(atk_cur_ComplexParameter.agility + atk_inGameBuffDetail.AGI_Inscrease + atk_inGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
        float atk_vit = Mathf.Clamp(atk_cur_ComplexParameter.vitality + atk_inGameBuffDetail.VIT_Inscrease + atk_inGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

        float def_str = Mathf.Clamp(def_cur_ComplexParameter.strenght + def_InGameBuffDetail.STR_Inscrease + def_InGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
        float def_int = Mathf.Clamp(def_cur_ComplexParameter.intelligent + def_InGameBuffDetail.INT_Inscrease + def_InGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
        float def_dex = Mathf.Clamp(def_cur_ComplexParameter.dexterity + def_InGameBuffDetail.DEX_Inscrease + def_InGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
        float def_agi = Mathf.Clamp(def_cur_ComplexParameter.agility + def_InGameBuffDetail.AGI_Inscrease + def_InGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
        float def_vit = Mathf.Clamp(def_cur_ComplexParameter.vitality + def_InGameBuffDetail.VIT_Inscrease + def_InGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

        //End basicParameter

        SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(unitAbility.GetSkillID());
        int skillLvl = Skill.GetSkillLevel(monsterAtk.MonsterInfo, atk_skillDataScriptObj);

        Debug.Log("Use skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
        Debug.Log("skill level = " + skillLvl);

        if (skillLvl <= 0)
        {
            skillLvl = 1;
        }

        Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[skillLvl - 1];

        DebugPopup.RegLog(InCaster.GetTeam());
        DebugPopup.Log("Caculate Attack ability monster = " + monsterAtkScriptObject.Monster.monsterName);
        DebugPopup.Log("Use Skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
        DebugPopup.Log("Target = " + monsterDefScriptObject.Monster.monsterName);

        List<ILevelCell> abilityCells = unitAbility.GetAbilityCells(InCaster);
        List<ILevelCell> PathCells = InCaster.GetPathTo(InTarget.GetCell(), abilityCells);
        int distanceAtkToDef = PathCells.Count - 1;

        float a_DistanceCorrection = MonsterBattle.Get_Caculated_Distance_Correction(InCaster, InTarget, hoverCell, unitAbility, atk_skillDetail.occurrenceType);
        Debug.Log("PathCells distance count = " + PathCells.Count);
        Debug.Log("skill Distance Correction = " + a_DistanceCorrection);

        /////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        //Caculate stamina

        DebugPopup.Log("----------");
        DebugPopup.Log("AccuracyRateUI");
        Debug.Log("Attacker current Stamina = " + atkParameterInGame.GetCurrentStamina());
        float consumedStaminaTraitCorrection = atk_innateTraitInfo.ST_Consumption_Correction;
        float consumedStamina = MonsterBattle.Get_Stamina_Consumption(atk_skillDetail.consumedST, consumedStaminaTraitCorrection);
        float a_RemainingStaminaCorrection = MonsterBattle.Get_Stamina_Performance_Improvement(atkParameterInGame.GetCurrentStamina(), consumedStamina);
        Debug.Log("a_RemainingStaminaCorrection = " + a_RemainingStaminaCorrection);
        DebugPopup.Log("End AccuracyRateUI");
        DebugPopup.Log("----------");

        //End Caculate stamina

        /////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        //Caculate Acuracy

        //it must be converted to 100% = 1f
        float a_SkillHitRate = atk_skillDetail.hit / 100f;

        float a_TraitHitCorrection = atk_innateTraitInfo.Accurancy + atk_acquiredTraitInfo.accuracy + atk_farmTraitInfo.accuracy;

        float d_TraitHitCorrection = def_innateTraitInfo.Evasion + def_acquiredTraitInfo.evasion + def_farmTraitInfo.evansion;

        float a_accuracyStateChangeCorrection = atk_inGameBuffDetail.atk_AccuracyRate_Correction + atk_inGameDeBuffDetail.atk_AccuracyRate_Correction;

        float d_accuracyStateChangeCorrection = def_InGameBuffDetail.def_AccuracyRate_Correction + def_InGameDeBuffDetail.def_AccuracyRate_Correction;

        DebugPopup.Log("----------");
        DebugPopup.Log("AccuracyRateUI");
        float acuracyRate = MonsterBattle.Get_Accuracy(a_SkillHitRate, a_DistanceCorrection, a_accuracyStateChangeCorrection, a_TraitHitCorrection, a_RemainingStaminaCorrection, atk_dex, d_accuracyStateChangeCorrection, d_TraitHitCorrection, def_agi);
        DebugPopup.Log("End AccuracyRateUI");
        DebugPopup.Log("----------");

        if (acuracyRate > 1f)
        {
            acuracyRate = 1f;
        }

        return Mathf.RoundToInt(acuracyRate * 100f);
    }
}
