using Assets.Scripts.Monster.Skill;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.CustomBattle.BattleSelectSkillUI
{
    public class BattleSelectSkillsUIButton : MonoBehaviour
    {
        public BattleSelectSkillsUI battleSelectSkillsUI;

        SkillDataScriptObj SkillDataScriptObj;

        Skill.SkillDetail skillDetail;

        Monster.Monster.MonsterSkillInfo monsterSkillInfo;

        int skillLevel = 0;

        bool selected = false;

        public Image skillIMG;

        public Transform noSelectBG, selectBG;

        public TextMeshProUGUI skillNameTxt, skillLevelTxt, skillStaminaTxt;

        public Button selectButton;

        private void Start()
        {
            selectButton.onClick.AddListener(() => { SelectButton(); });
        }

        public void SetUp(SkillDataScriptObj t_skillDataScriptObj, Monster.Monster.MonsterSkillInfo t_monsterSkillInfo, int t_skillLevel, Sprite skillAvatar, bool t_selected)
        {
            if (t_skillLevel > 0)
            {
                Active();

                SkillDataScriptObj = t_skillDataScriptObj;

                skillDetail = t_skillDataScriptObj.Skill.skillDetails[t_skillLevel - 1];

                monsterSkillInfo = t_monsterSkillInfo;

                skillLevel = t_skillLevel;

                selected = t_selected;

                skillIMG.sprite = skillAvatar;

                skillNameTxt.text = t_skillDataScriptObj.Skill.skillName;

                skillLevelTxt.text = "Lvl " + skillLevel;

                skillStaminaTxt.text = "ST " + skillDetail.consumedST;

                if (selected)
                {
                    noSelectBG.gameObject.SetActive(false);
                    selectBG.gameObject.SetActive(true);
                }
                else
                {
                    noSelectBG.gameObject.SetActive(true);
                    selectBG.gameObject.SetActive(false);
                }
            }
            else
            {
                Deactive();
            }
        }

        public Monster.Monster.MonsterSkillInfo GetSkillInfo() => monsterSkillInfo;

        public void SelectButton() => battleSelectSkillsUI.SetSelected(this);

        public void SelectedInOtherSlot() => Deactive();

        private void Active() => gameObject.SetActive(true);

        private void Deactive() => gameObject.SetActive(false);
    }
}