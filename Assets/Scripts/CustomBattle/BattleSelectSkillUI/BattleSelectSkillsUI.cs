using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.CustomBattle.BattleSelectSkillUI
{
    public class BattleSelectSkillsUI : UIMenuStruct
    {
        [SerializeField] private List<BattleSelectSkillsUIButton> battleSelectSkillsUIButtons = new();
        private List<Monster.Monster.MonsterSkillInfo> usingList = new();
        private MonsterSpawnUI monsterSpawnUI;
        private bool isReplacingSkill;
        private BattleSelectSkillsUIButton selectedButton;
        private int targetSlotIndex = 0;

        public MonsterSpawnUI MonsterSpawnUI => monsterSpawnUI;
        public List<Monster.Monster.MonsterSkillInfo> UsingList => usingList;
        public BattleSelectSkillsUIButton SelectedButton => selectedButton;
        public List<BattleSelectSkillsUIButton> BattleSelectSkillsUIButtons => battleSelectSkillsUIButtons;

        public void SetupMenu(MonsterSpawnUI t_monsterSpawnUI, Monster.Monster.MonsterSkillInfo t_selectedSkillInfo, int skillSlotIndex)
        {
            selectedButton = null;
            isReplacingSkill = false;

            monsterSpawnUI = t_monsterSpawnUI;
            targetSlotIndex = skillSlotIndex;
            usingList = new List<Monster.Monster.MonsterSkillInfo>();
            usingList.Clear();
            usingList.AddRange(t_monsterSpawnUI.GetUsingSkillInfos());

            UpdateBattleSelectSkillsUIButton(t_selectedSkillInfo);
            OpenMenu();
        }

        public override void CloseMenu()
        {
            if (usingList != monsterSpawnUI.GetUsingSkillInfos())
            {
                PlayerInfo.PlayerMonsterInfo playerMonsterInfo = monsterSpawnUI.GetMonsterInfo();
                List<string> useSkillList = new();
                for (int i = 0; i < usingList.Count; i++)
                {
                    useSkillList.Add(usingList[i].skillID);
                }
                playerMonsterInfo.SelectBattleSkill(useSkillList);

                monsterSpawnUI.UpdateUsingSkillInfos(usingList);

                PlayerStats.Instance.Save();
            }
            base.CloseMenu();
        }

        private void UpdateBattleSelectSkillsUIButton(Monster.Monster.MonsterSkillInfo t_selectedSkillInfo)
        {
            MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterSpawnUI.GetMonsterInfo().monsterID);

            for (int i = 0; i < battleSelectSkillsUIButtons.Count; i++)
            {
                Monster.Monster.MonsterSkillInfo monsterSkillInfo = monsterSpawnUI.GetMonsterInfo().MonsterSkillInfos[i];

                SkillDataScriptObj skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(monsterSkillInfo.skillID);

                UnitAbility unitAbility = Skill.GetUnitAbility(skillDataScriptObj.Skill.skillID, monsterDataScriptObj);
                if (!usingList.Contains(monsterSpawnUI.GetMonsterInfo().MonsterSkillInfos[i]))
                {
                    battleSelectSkillsUIButtons[i].SetUp(skillDataScriptObj, monsterSkillInfo, monsterSkillInfo.skillLevel, unitAbility.GetIconSprite(), false);
                    continue;
                }
                if (monsterSkillInfo != t_selectedSkillInfo)
                {
                    battleSelectSkillsUIButtons[i].SelectedInOtherSlot();
                    continue;
                }
                selectedButton = battleSelectSkillsUIButtons[i];

                battleSelectSkillsUIButtons[i].SetUp(skillDataScriptObj, monsterSkillInfo, monsterSkillInfo.skillLevel, unitAbility.GetIconSprite(), true);
            }
        }

        public void SetSelected(BattleSelectSkillsUIButton battleSelectSkillsUIButton)
        {
            if (selectedButton == battleSelectSkillsUIButton) return;
            isReplacingSkill = selectedButton != null;

            selectedButton = battleSelectSkillsUIButton;

            if (!isReplacingSkill)
            {
                usingList.Add(selectedButton.GetSkillInfo());
            }
            else
            {
                usingList[targetSlotIndex] = selectedButton.GetSkillInfo();
            }

            UpdateBattleSelectSkillsUIButton(selectedButton.GetSkillInfo());

            CloseMenu();
        }
    }
}