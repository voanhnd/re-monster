using System.Collections.Generic;
using UnityEngine;

public class CustomGridManager : StaticInstance<CustomGridManager>
{
    public HexagonGrid hexagonGrid;

    public List<LevelCell> friendlySpawnCells;
    public List<LevelCell> hostileSpawnCells;

    public List<ILevelCell> canSpawnCells;

    [System.Serializable]
    public class DeadBoundData
    {
        public int deadBoundTurn;
        public List<LevelCell> deadBoundCells;
    }

    public List<DeadBoundData> deadBoundDatas;

    //private void Update()
    //{
    //    ReselectAfterUpdate();
    //}

    //GridUnit reselectedUnit = null;
    //void ReselectAfterUpdate()
    //{
    //    if(reselectedUnit && GameManager.IsActionBeingPerformed() == false)
    //    {
    //        reselectedUnit.

    //        reselectedUnit = null;
    //    }
    //}

    public void SetCellsToDefault()
    {
        List<ILevelCell> cellList = hexagonGrid.GetAllCells();
        for (int i = 0; i < cellList.Count; i++)
        {
            LevelCell cell = (LevelCell)cellList[i];
            cell.CustomSetmaterials(0);

            if(cell.GetUnitOnCell() != null)
            {
                cell.SetMaterial(cell.GetNormalState());
            }
        }
    }

    public void UpdateDeadBound(int turn)
    {
        Debug.Log("UpdateDeadBound");

        List<ILevelCell> blockedList = new();

        List<ILevelCell> blockedGridUnitList = new();

        for (int i = 0; i < deadBoundDatas.Count; i++)
        {
            if (deadBoundDatas[i].deadBoundTurn > turn)
            {
                continue;
            }
            for (int j = 0; j < deadBoundDatas[i].deadBoundCells.Count; j++)
            {
                LevelCell cell = deadBoundDatas[i].deadBoundCells[j];
                cell.CustomSetmaterials(3);

                if (cell.GetUnitOnCell())
                {
                    blockedGridUnitList.Add(cell);
                }
                else
                {
                    cell.SetBlocked(true);
                }

                blockedList.Add(cell);
            }
        }

        Debug.Log("blockedGridUnitList count = " + blockedGridUnitList.Count);

        //Sort blockedGridUnitList which is caculated first by z axis value
        int count = 0;
        for (int i = 0; i < blockedGridUnitList.Count - 2; i++)
        {
            for (int k = 0; k < blockedGridUnitList.Count - 2; k++)
            {
                count++;

                if (blockedGridUnitList[k].transform.position.z < blockedGridUnitList[k + 1].transform.position.z)
                {
                    ILevelCell temp = blockedGridUnitList[k + 1];
                    blockedGridUnitList[k + 1] = blockedGridUnitList[k];
                    blockedGridUnitList[k] = temp;
                }
            }
        }

        //Caculate can snap cell for grid unit on blocked cell
        for (int i = 0; i < blockedGridUnitList.Count; i++)
        {
            ILevelCell cell = blockedGridUnitList[i];
            GridUnit gridUnit = cell.GetUnitOnCell();

            ParameterInGame parameterInGame = gridUnit.GameMonster.InGameParameter;

            ILevelCell snapToCell = GetChangeCellFromBlockedCell(cell);

            if (snapToCell)
            {
                Debug.Log("snap to cell not null");
                //GameManager.SnapUnitCell(gridUnit, snapToCell, compassDir);

                gridUnit.MoveTo(snapToCell);

                cell.SetBlocked(true);
            }
            else
            {
                parameterInGame.DamgeOnBlockedCell();

                Debug.Log("snap to cell null");
            }
        }
    }

    ILevelCell GetChangeCellFromBlockedCell(ILevelCell blockedCell)
    {
        Debug.Log("selected blocked cell = " + blockedCell.name);

        ILevelCell selectedCell = null;

        List<ILevelCell> allCells = blockedCell.GetAllAdjacentCells();

        Debug.Log("allCells count = " + allCells.Count);

        List<ILevelCell> canUseCells = new();

        foreach (ILevelCell cell in allCells)
        {
            Debug.Log("cell around = " + cell.name);

            if (cell.GetUnitOnCell() == null && cell.IsBlocked() == false)
            {
                canUseCells.Add(cell);
            }
        }

        Debug.Log("canUseCells count = " + canUseCells.Count);

        if (canUseCells.Count > 0)
        {
            selectedCell = canUseCells[0];
            float zDist = selectedCell.transform.position.z;
            for (int i = 0; i < canUseCells.Count; i++)
            {
                if (canUseCells[i].transform.position.z > zDist)
                {
                    zDist = canUseCells[i].transform.position.z;

                    selectedCell = canUseCells[i];
                }
            }
        }

        return selectedCell;
    }

    public void ActiveMonsterSpawnMode(GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            for (int i = 0; i < friendlySpawnCells.Count; i++)
            {
                LevelCell cell = friendlySpawnCells[i];
                cell.CustomSetmaterials(1);

                canSpawnCells.Add(cell);
            }

            for (int i = 0; i < hostileSpawnCells.Count; i++)
            {
                LevelCell cell = hostileSpawnCells[i];
                cell.CustomSetmaterials(2);
            }
        }
        else if (gameTeam == GameTeam.Red)
        {
            for (int i = 0; i < hostileSpawnCells.Count; i++)
            {
                LevelCell cell = hostileSpawnCells[i];
                cell.CustomSetmaterials(1);
                canSpawnCells.Add(cell);
            }

            for (int i = 0; i < friendlySpawnCells.Count; i++)
            {
                LevelCell cell = friendlySpawnCells[i];
                cell.CustomSetmaterials(2);
            }
        }
    }

    public List<ILevelCell> GetCanSpawnCells(GameTeam gameTeam)
    {
        List<ILevelCell> levelCells = new();

        if (gameTeam == GameTeam.Blue)
        {
            levelCells.AddRange(friendlySpawnCells);
        }
        else if (gameTeam == GameTeam.Red)
        {
            levelCells.AddRange(hostileSpawnCells);
        }

        return levelCells;
    }
}
