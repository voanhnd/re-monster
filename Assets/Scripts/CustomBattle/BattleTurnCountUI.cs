using TMPro;
using UnityEngine;

public class BattleTurnCountUI : MonoBehaviour
{
    public TextMeshProUGUI countText;

    // Update is called once per frame
    void Update()
    {
        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        countText.text = "" + customGameRules.GetCurrentTurn() + "/20";
    }
}
