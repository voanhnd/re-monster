﻿using UnityEngine;

public partial class CustomGameManager
{
    [System.Serializable]
    public class SpawnListInfo
    {
        public PlayerInfo.PlayerMonsterInfo playerMonsterInfo = null;

        public ILevelCell levelCell = null;

        public GameObject spawnedModel = null;

        public bool spawnPosSet = false;
    }
}
