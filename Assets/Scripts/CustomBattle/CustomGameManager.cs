using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class CustomGameManager : StaticInstance<CustomGameManager>
{
    #region Private variables
    [SerializeField] private GameTeam playingSide = GameTeam.Blue;
    [SerializeField] private bool isPlaying = false;
    [SerializeField] private List<GridUnit> turnList = new();
    [SerializeField] private List<GridUnit> currentTurnList = new();
    [SerializeField] private List<GridUnit> nextTurnList = new();
    private bool checkNextTurn = false;
    private bool friendlyAIActivated = false;
    private bool hostileAIActivated = false;
    private List<SpawnListInfo> friendlySpawnListInfo = new();
    private List<SpawnListInfo> enemySpawnListInfos = new();
    private GridUnit currentUnit;
    //[SerializeField] private GridUnit currentTurnUnit = null;
    #endregion

    #region Public variables
    public MonsterSpawnListController monsterSpawnListController;

    /// <summary>
    /// <para> turnType = 1 mean SPD turn </para>
    /// <para> turnType = 2 mean Delay turn </para>
    /// </summary>
    public List<SpawnListInfo> FriendlySpawnListInfo => friendlySpawnListInfo;
    public List<SpawnListInfo> EnemySpawnListInfos => enemySpawnListInfos;
    #endregion

    private void Start()
    {
        SetPlayerTeam(GameTeam.Blue);
        GamePlaySetup();
    }

    private void Update()
    {
        CheckNextTurn();

        SetSelectedAfterActionPerformed();
    }

    private void GamePlaySetup()
    {
        CustomGameUIManager.instance.Active_MonsterSpawnUIContainer(true);

        IngameCameraManager.instance.MoveToSpawnPos();

        CustomGridManager.Instance.ActiveMonsterSpawnMode(playingSide);

        monsterSpawnListController.SpawnMonsterToChoice();

        CustomGameRules gameRules = GameManager.GetRules() as CustomGameRules;

        gameRules.CustomInit();

        monsterSpawnListController.CreateAIEnemyMonster();

        //GameManager.Get().OnTeamWon.AddListener(CaculateMonsterGrowthStatsAfterBattle);
    }

    private void SetupMonsterID()
    {
        List<GridUnit> gridUnits = new();
        gridUnits.AddRange(GameManager.GetUnitsOnTeam(playingSide));
        gridUnits.AddRange(GameManager.GetUnitsOnTeam(GetEnemyTeam(playingSide)));

        for (int i = 0; i < gridUnits.Count; i++)
            gridUnits[i].GetComponent<MonsterInGame>().SetGameID(i);
    }

    public GameTeam GetPlayerTeam() => playingSide;

    public void SetPlayerTeam(GameTeam side) => playingSide = side;

    public static GameTeam GetEnemyTeam(GameTeam gameTeam)
    {
        GameTeam enemyTeam = GameTeam.None;

        if (gameTeam == GameTeam.Blue)
            enemyTeam = GameTeam.Red;
        else if (gameTeam == GameTeam.Red)
            enemyTeam = GameTeam.Blue;

        return enemyTeam;
    }

    private void CustomFinishTurn(GameTeam nextTeam)
    {
        Debug.Log("CustomFinishTurn");

        GameRules gameRules = GameManager.GetRules();

        gameRules.CustomEndTurn(nextTeam);

        GameManager.CheckWinConditions();
    }

    private void SetSelectedAfterActionPerformed()
    {
        if (!GameManager.IsPlaying() || currentUnit == null || GameManager.IsActionBeingPerformed() != false) return;

        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        if (currentUnit.GetTeam() == playingSide)
            customGameRules.CustomHandlePlayerSelected(currentUnit);

        currentUnit = null;
    }

    public void SetNextTurn() => checkNextTurn = true;

    private void CheckNextTurn()
    {
        if (checkNextTurn && GameManager.IsPlaying() && GameManager.IsActionBeingPerformed() == false)
        {
            checkNextTurn = false;

            NextTurn();
        }
    }

    /// <summary>
    /// <para> turnType 1 caculate by speed </para>
    /// <para> turnType 2 caculate by delay </para>
    /// </summary>
    /// <param name="turnNum"></param>
    private void NextTurn()
    {
        currentUnit = null;

        if (!GameManager.IsPlaying()) return;

        Debug.Log("Caculate turn");
        CreateNextActionOrderList();
        SetNextMonsterAction();
    }

    private void SetNextMonsterAction()
    {
        if (currentTurnList.Count > 0)
        {
            //Check dead
            for (int i = 0; i < currentTurnList.Count; i++)
            {
                GridUnit unit = currentTurnList[i];
                if (unit.IsDead())
                {
                    currentTurnList.Remove(unit);
                    nextTurnList.Remove(unit);
                }
            }

            SortTurnList(ref nextTurnList);

            if (currentTurnList.Count > 0)
            {
                Debug.Log("Caculate next action turn");

                CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

                currentUnit = currentTurnList[0];
                nextTurnList.Add(currentUnit);
                SortTurnList(ref nextTurnList);

                CustomFinishTurn(currentUnit.GetTeam());

                customGameRules.CustomUnitTurn(currentUnit);

                currentTurnList.RemoveAt(0);
                UpdateCurrentTurnListByDelay();

                CustomGameUIManager.instance.battleMonsterTurnUI.UpdateMonsterTurnUIList(currentUnit, currentTurnList, nextTurnList);
            }
            else
            {
                NextTurn();
            }
        }
    }

    public void SetDeadMonster(ParameterInGame parameterInGame)
    {
        Debug.Log("Set dead monster in turn");

        nextTurnList.Remove(parameterInGame.MonsterInGame.Unit);

        CustomGameUIManager.instance.battleMonsterTurnUI.UpdateMonsterTurnUIList(currentUnit, currentTurnList, nextTurnList);
    }

    private void CreateNextActionOrderList()
    {
        if (currentTurnList.Count <= 0)
        {
            Debug.Log("Caculate new turn");

            List<GridUnit> unitList = new();

            unitList.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Blue));

            unitList.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Red));

            Debug.Log("unitList count = " + unitList.Count);

            List<GridUnit> canUseGridUnits = new();

            for (int i = 0; i < unitList.Count; i++)
            {
                GridUnit gridUnit = unitList[i];
                ParameterInGame monsterInGame = gridUnit.GameMonster.InGameParameter;

                if (gridUnit.IsDead() == false && monsterInGame != null)
                {
                    canUseGridUnits.Add(gridUnit);
                }
            }
            int count = 0;

            for (int j = 0; j <= canUseGridUnits.Count - 2; j++)
            {
                for (int k = 0; k <= canUseGridUnits.Count - 2; k++)
                {
                    count++;

                    ParameterInGame nowMonsterInGame = canUseGridUnits[k].GameMonster.InGameParameter;
                    ParameterInGame nextMonsterInGame = canUseGridUnits[k + 1].GameMonster.InGameParameter;

                    float curSPD = nowMonsterInGame.GetCurrentBattleParametersSpeed() - nowMonsterInGame.GetSkillDelay();
                    float nextSPD = nextMonsterInGame.GetCurrentBattleParametersSpeed() - nextMonsterInGame.GetSkillDelay();

                    if (curSPD >= nextSPD) continue;

                    GridUnit temp = canUseGridUnits[k + 1];
                    canUseGridUnits[k + 1] = canUseGridUnits[k];
                    canUseGridUnits[k] = temp;
                }
            }

            turnList = canUseGridUnits;

            CustomGameRules gameRules = GameManager.GetRules() as CustomGameRules;

            gameRules.CustomCountTurn();

            if (nextTurnList.Count > 0)
            {
                currentTurnList.AddRange(nextTurnList);
                nextTurnList.Clear();
                CustomGameUIManager.instance.battleMonsterTurnUI.UpdateMonsterTurnUIList(currentTurnList[0], currentTurnList, nextTurnList);
            }
            else
            {
                currentTurnList.AddRange(turnList);
                ShuffleTurnList(ref currentTurnList);
                CustomGameUIManager.instance.battleMonsterTurnUI.AssignMonsterData(currentTurnList);
            }
        }
    }

    private void ShuffleTurnList(ref List<GridUnit> turnList)
    {
        ListScrambler<GridUnit> scrambler = new(turnList);
        scrambler.Scramble();
        turnList.Clear();
        List<GridUnit> scrambleList = scrambler.GetScrambledList();
        turnList = scrambleList.OrderByDescending(x => x.GetSpeed()).ToList();
    }

    private void SortTurnList(ref List<GridUnit> turnList)
    {
        List<GridUnit> turnListCopy = new();
        turnListCopy.AddRange(turnList);

        if (turnListCopy.Count < 2)
        {
            return;
        }

        turnList.Clear();
        turnList.AddRange(turnListCopy.OrderByDescending(x => x.GetSpeed()));
    }

    public void UpdateCurrentTurnListByDelay()
    {
        //Resort turn
        for (int j = 0; j <= currentTurnList.Count - 2; j++)
        {
            for (int k = 0; k <= currentTurnList.Count - 2; k++)
            {
                ParameterInGame nowMonsterInGame = currentTurnList[k].GameMonster.InGameParameter;
                ParameterInGame nextMonsterInGame = currentTurnList[k + 1].GameMonster.InGameParameter;

                float curSPD = nowMonsterInGame.GetCurrentBattleParametersSpeed() - nowMonsterInGame.GetSkillDelay();
                float nextSPD = nextMonsterInGame.GetCurrentBattleParametersSpeed() - nextMonsterInGame.GetSkillDelay();

                if (curSPD >= nextSPD) continue;

                GridUnit temp = currentTurnList[k + 1];
                currentTurnList[k + 1] = currentTurnList[k];
                currentTurnList[k] = temp;
            }
        }
        //CustomGameUIManager.instance.battleMonsterTurnUI.SetActionOrder()
    }

    /*public void CaculateMonsterGrowthStatsAfterBattle(GameTeam gameTeam)
    {
        Debug.Log("Caculate Monster Growth Stats After Battle");

        Debug.Log("won team = " + gameTeam);

        Debug.Log("player team = " + playingSide);

        //List<GridUnit> tempList = GameManager.GetUnitsOnTeam(playingSide);

        //foreach (GridUnit unit in tempList)
        //{
        //    ParameterInGame parameterInGame = unit.GetComponent<ParameterInGame>();

        //    MonsterInGame monsterInGame = unit.GetComponent<MonsterInGame>();

        //    MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInGame.playerMonsterInfo.monsterID);

        //    PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(monsterInGame.playerMonsterInfo.usingFarmID);

        //    FarmDataScriptObj farmDataScriptObj = PlayerStats.Instance.farmDataManagerScriptObj.GetFarmDataObj(playerFarmInfo.usingFarmType);

        //    int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterDataScriptObj.Monster.mainSuitTerrain, monsterDataScriptObj.Monster.mainNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        //    int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterDataScriptObj.Monster.subSuitTerrain, monsterDataScriptObj.Monster.subNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        //    float characteristicCorrection = 0f;

        //    //Caculate Fatigue

        //    float totalFatigueCorrection = Monster.Get_BodyType_Increase_Fatigue_Correction(monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue);

        //    totalFatigueCorrection += Monster.Get_TrainingPolicy_Increase_Fatigue_Correction(monsterInGame.playerMonsterInfo.growthParameters.trainingPolicyValue);

        //    float totalFatigueIncrease = Monster.Get_Fatigue_IncreaseOrDecrease_Value(2);

        //    float caculatedFatigueIncrease = Monster.Get_Fatigue_Increase_Caculated
        //        (
        //            totalFatigueIncrease,
        //            Monster.Get_Fatigue_Farm_Reduction_Enhancement_Value(farmDataScriptObj.Farm.fatigueEnhancValue),
        //            mainRaceStat,
        //            subRaceStat,
        //            characteristicCorrection,
        //            monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue,
        //            totalFatigueCorrection
        //        );

        //    Monster.FatigueType fatigueType = Monster.Get_Fatigue_Type_By_Value(monsterInGame.playerMonsterInfo.growthParameters.fatigue + caculatedFatigueIncrease);

        //    //End Caculate Fatigue

        //    //Caculated Stress

        //    float totalStressCorrection = Monster.Get_BodyType_Increase_Stress_Correction(monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue);

        //    totalStressCorrection += Monster.Get_TrainingPolicy_Increase_Stress_Correction(monsterInGame.playerMonsterInfo.growthParameters.trainingPolicyValue);

        //    float totalStressIncrease = 0f;
        //    if (gameTeam == playingSide)
        //    {
        //        //Win
        //        totalStressIncrease += Monster.Get_Stress_IncreaseOrDecrease_Value(3);
        //    }
        //    else
        //    {
        //        //Lose
        //        totalStressIncrease += Monster.Get_Stress_IncreaseOrDecrease_Value(2);
        //    }

        //    float caculatedStressIncrease = Monster.Get_Stress_Increase_Caculated
        //    (
        //        totalStressIncrease,
        //        Monster.Get_Stress_Farm_Reduction_Enhancement_Value(farmDataScriptObj.Farm.stressEnhancValue),
        //        mainRaceStat,
        //        subRaceStat,
        //        characteristicCorrection,
        //        monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue,
        //        totalStressCorrection
        //    );

        //    Monster.StressType stressType = Monster.Get_Stress_Type_By_Value(monsterInGame.playerMonsterInfo.growthParameters.stress + caculatedStressIncrease);

        //    //End Caculated Stress

        //    //Caculate Injury
        //    Monster.InjuryType injuryType = monsterInGame.playerMonsterInfo.growthParameters.injuryType;

        //    float basicInjuryRate = Monster.Get_Injuries_Probability(4);

        //    float lostHealthPercent = 1 - parameterInGame.GetCurrentHealth() / parameterInGame.GetDefaultHealth();

        //    if (lostHealthPercent < 0.3f)
        //    {
        //        basicInjuryRate += Monster.Get_Injuries_Probability(5);
        //    }
        //    else if (lostHealthPercent < 0.5f)
        //    {
        //        basicInjuryRate += Monster.Get_Injuries_Probability(6);
        //    }
        //    else if (lostHealthPercent < 0.8f)
        //    {
        //        basicInjuryRate += Monster.Get_Injuries_Probability(7);
        //    }
        //    else
        //    {
        //        basicInjuryRate += Monster.Get_Injuries_Probability(8);
        //    }

        //    //Caculate monster will be injury or not
        //    {
        //        Vector2 injuryRate = Monster.Get_Injury_Probability_Rate(basicInjuryRate * 100f, characteristicCorrection, fatigueType, 0f);

        //        injuryType = Monster.Get_Injury_Caculate_Is_Injury(injuryType, injuryRate.x, injuryRate.y);

        //        Debug.Log("injuryType = " + injuryType);
        //    }

        //    //End Caculate Injury

        //    //Caculate LifeSpan

        //    float totalLifeSpan = 0f;

        //    //100% = 1f
        //    float lifeSpanConsumPercent = 0f;

        //    //Check life span concum percent if it is fatigue or stress
        //    lifeSpanConsumPercent += Monster.Get_Fatigue_LifeSpan_Percent(fatigueType);

        //    lifeSpanConsumPercent += Monster.Get_Stress_LifeSpan_Percent(stressType);

        //    totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(3);

        //    totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(8);

        //    if (injuryType == Monster.InjuryType.Injury)
        //    {
        //        totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(4);
        //    }
        //    else if (injuryType == Monster.InjuryType.Serious_Injury)
        //    {
        //        totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(5);
        //    }

        //    if (monsterInGame.playerMonsterInfo.growthParameters.diseasesType == Monster.DiseasesType.Diseases)
        //    {
        //        totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(6);
        //    }
        //    else if (monsterInGame.playerMonsterInfo.growthParameters.diseasesType == Monster.DiseasesType.Serious_Diseases)
        //    {
        //        totalLifeSpan += Monster.Get_LifeSpan_Decrease_Value(7);
        //    }

        //    //life span concum percent if it is fatigue or stress
        //    totalLifeSpan += totalLifeSpan * lifeSpanConsumPercent;

        //    //End Caculate LifeSpan

        //    //Apply to player

        //    Debug.Log("totalLifeSpan = " + totalLifeSpan);
        //    monsterInGame.playerMonsterInfo.growthParameters.lifeSpan = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.lifeSpan + totalLifeSpan, 0f, monsterInGame.playerMonsterInfo.growthParameters.lifeSpan + totalLifeSpan);

        //    float totalBodyTypeValue = Monster.Get_BodyType_IncreaseOrDecrease_Value(2);
        //    Vector2 bodyTypeValueMinMax = Monster.Get_BodyType_Value_MinMax();
        //    Debug.Log("totalBodyTypeValue = " + totalBodyTypeValue);
        //    monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.bodyTypeValue + totalBodyTypeValue, bodyTypeValueMinMax.x, bodyTypeValueMinMax.y);

        //    float totalTrainingPolicy = 0f;
        //    if (gameTeam == playingSide)
        //    {
        //        //Win
        //        totalTrainingPolicy += 2f;
        //    }
        //    else
        //    {
        //        //Lose
        //        totalTrainingPolicy += -2f;
        //    }
        //    Vector2 trainingPolicyMinMax = Monster.Get_TrainingPolicy_Value_MinMax();
        //    Debug.Log("totalTrainingPolicy = " + totalTrainingPolicy);
        //    monsterInGame.playerMonsterInfo.growthParameters.trainingPolicyValue = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.trainingPolicyValue + totalTrainingPolicy, trainingPolicyMinMax.x, trainingPolicyMinMax.y);

        //    float totalAffection = 0f;
        //    if (gameTeam == playingSide)
        //    {
        //        //Win
        //        totalAffection += 2f;
        //        totalAffection += Monster.Get_Affection_IncreaseOrDecrease_Value(12);
        //    }
        //    else
        //    {
        //        //Lose
        //        totalAffection += -2f;
        //        totalAffection += Monster.Get_Affection_IncreaseOrDecrease_Value(13);
        //    }
        //    Vector2 affectionMinMax = Monster.Get_Affection_Value_MinMax();
        //    Debug.Log("totalAffection = " + totalAffection);
        //    monsterInGame.playerMonsterInfo.growthParameters.affection = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.affection + totalAffection, affectionMinMax.x, affectionMinMax.y);

        //    Vector2 fatigueMinMax = Monster.Get_Fatigue_Min_Max();
        //    Debug.Log("totalFatigueIncrease = " + totalFatigueIncrease);
        //    monsterInGame.playerMonsterInfo.growthParameters.fatigue = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.fatigue + totalFatigueIncrease, fatigueMinMax.x, fatigueMinMax.y);

        //    Vector2 stressMinMax = Monster.Get_Stress_Min_Max();
        //    Debug.Log("totalStressIncrease = " + totalStressIncrease);
        //    monsterInGame.playerMonsterInfo.growthParameters.stress = Mathf.Clamp(monsterInGame.playerMonsterInfo.growthParameters.stress + totalStressIncrease, stressMinMax.x, stressMinMax.y);

        //    Debug.Log("injuryType = " + injuryType.ToString());
        //    monsterInGame.playerMonsterInfo.growthParameters.injuryType = injuryType;

        //    Debug.Log("monster in battle = " + unit.name);

        //    PlayerStats.Instance.Save();
        //}
    }*/

    public List<GridUnit> GetTurnList() => turnList;

    public List<GridUnit> GetCurrentTurnList() => currentTurnList;

    public void SetPlay()
    {
        CustomGridManager.Instance.SetCellsToDefault();

        CustomGameUIManager.instance.Active_PlayUIContainer(true);

        SetPlayingBool(true);

        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        customGameRules.StartGame();

        SetupMonsterID();

        NextTurn();

        IngameCameraManager.instance.MoveToPlayPos();

        BattleStatsCollector.Instance.Setup();
    }

    public bool GetIsPlayingBool() => isPlaying;

    void SetPlayingBool(bool stat) => isPlaying = stat;

    public void SetActiveAI(GameTeam gameTeam, bool active)
    {
        foreach (GridUnit gridUnit in GameManager.GetUnitsOnTeam(gameTeam))
        {
            gridUnit.SetAIActive(active);
        }
        if (gameTeam == GameTeam.Blue)
        {
            friendlyAIActivated = active;
            return;
        }
        if (gameTeam == GameTeam.Red)
        {
            hostileAIActivated = active;
            return;
        }
    }

    public bool IsAIActivated(GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue) return friendlyAIActivated;
        if (gameTeam == GameTeam.Red) return hostileAIActivated;

        return false;
    }
}
