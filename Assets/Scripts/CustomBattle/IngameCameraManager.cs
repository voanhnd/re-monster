using UnityEngine;

public class IngameCameraManager : MonoBehaviour
{
    public static IngameCameraManager instance;

    public TweenTransform tweenTransform;

    public Transform spawnMonsterPos;
    public Transform playPos;

    private void Awake()
    {
        instance = this;
    }

    public void MoveToSpawnPos()
    {
        tweenTransform.transform.position = spawnMonsterPos.position;
        tweenTransform.transform.rotation = spawnMonsterPos.rotation;
    }

    public void MoveToPlayPos()
    {
        tweenTransform.from = spawnMonsterPos;
        tweenTransform.to = playPos;

        tweenTransform.duration = 0.5f;

        tweenTransform.ResetToBeginning();

        tweenTransform.PlayForward();
    }
}
