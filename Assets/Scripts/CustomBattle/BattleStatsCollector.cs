using System.Collections.Generic;
using UnityEngine;

public class BattleStatsCollector : StaticInstance<BattleStatsCollector>
{
    [System.Serializable]
    public class BattleCollectorInfo
    {
        public GridUnit unit;
        public float damageReceived = 0f;
        public float damageCaused = 0f;

        public BattleCollectorInfo(GridUnit t_unit)
        {
            unit = t_unit;
            damageReceived = 0f;
            damageCaused = 0f;
        }
    }

    [SerializeField] private List<BattleCollectorInfo> frienlyCollectorInfos = new();
    [SerializeField] private List<BattleCollectorInfo> hostileCollectorInfos = new();

    public List<BattleCollectorInfo> FrienlyCollectorInfos => frienlyCollectorInfos;
    public List<BattleCollectorInfo> HostileCollectorInfos => hostileCollectorInfos;

    public void Clear()
    {
        frienlyCollectorInfos.Clear();
        hostileCollectorInfos.Clear();
    }

    public void Setup()
    {
        Clear();

        foreach (CustomGameManager.SpawnListInfo info in CustomGameManager.Instance.EnemySpawnListInfos)
        {
            GridUnit unit = info.levelCell.GetUnitOnCell();

            AddBattleCollectMonster(unit, unit.GetTeam());
        }

        foreach (CustomGameManager.SpawnListInfo info in CustomGameManager.Instance.FriendlySpawnListInfo)
        {
            GridUnit unit = info.levelCell.GetUnitOnCell();

            AddBattleCollectMonster(unit, unit.GetTeam());
        }
    }

    public void AddBattleCollectMonster(GridUnit unit, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            frienlyCollectorInfos.Add(new BattleCollectorInfo(unit));
        }
        else
        {
            hostileCollectorInfos.Add(new BattleCollectorInfo(unit));
        }
    }

    public void AddDamageReceived(GridUnit unit, float damage, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                if (info.unit == unit)
                {
                    info.damageReceived += damage;
                    break;
                }
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                if (info.unit == unit)
                {
                    info.damageReceived += damage;
                    break;
                }
            }
        }
    }

    public void AddDamageTake(GridUnit unit, float damage, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                if (info.unit == unit)
                {
                    info.damageCaused += damage;
                    break;
                }
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                if (info.unit == unit)
                {
                    info.damageCaused += damage;
                    break;
                }
            }
        }
    }

    public float GetMonsterDamageReceived(GridUnit unit, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                if (info.unit == unit)
                {
                    return info.damageReceived;
                }
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                if (info.unit == unit)
                {
                    return info.damageReceived;
                }
            }
        }


        return 0f;
    }

    public float GetMonsterDamageCaused(GridUnit unit, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                if (info.unit == unit)
                {
                    return info.damageCaused;
                }
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                if (info.unit == unit)
                {
                    return info.damageCaused;
                }
            }
        }


        return 0f;
    }

    public float GetTeamDamageCaused(GameTeam gameTeam)
    {
        float damage = 0f;
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                damage += info.damageCaused;
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                damage += info.damageCaused;
            }
        }

        return damage;
    }

    public float GetTeamDamageTake(GameTeam gameTeam)
    {
        float damage = 0f;
        if (gameTeam == GameTeam.Blue)
        {
            foreach (BattleCollectorInfo info in frienlyCollectorInfos)
            {
                damage += info.damageReceived;
            }
        }
        else
        {
            foreach (BattleCollectorInfo info in hostileCollectorInfos)
            {
                damage += info.damageReceived;
            }
        }

        return damage;
    }
}
