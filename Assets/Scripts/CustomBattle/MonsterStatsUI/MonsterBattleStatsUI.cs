using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MonsterBattleStatsUI : MonoBehaviour
{
    public Camera refCamera;
    [SerializeField] private Canvas refCanvas;
    public Transform followTarget;

    public Slider hpSlider;
    public Slider stSlider;

    public ParameterInGame parameterInGame;

    public UIGrid buffGrid;
    public TextMeshProUGUI buffExtTxt;
    public List<Image> buffImages;

    public UIGrid deBuffGrid;
    public TextMeshProUGUI deBuffExtTxt;
    public List<Image> deBuffImages;

    // Update is called once per frame
    void Update()
    {
        CheckActive();

        UpdatePositionToFollowTarget();

        UpdateInfo();
    }

    void CheckActive()
    {
        if (parameterInGame == null)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }

    void UpdatePositionToFollowTarget()
    {
        if (refCamera == null)
        {
            refCamera = Camera.main;

            return;
        }

        if (parameterInGame == null)
            return;

        var screen = Camera.main.WorldToScreenPoint(followTarget.transform.position);
        screen.z = (refCanvas.transform.position - refCamera.transform.position).magnitude;
        Vector3 position = refCamera.ScreenToWorldPoint(screen);
        transform.position = position;
    }

    void UpdateInfo()
    {
        if (parameterInGame == null)
            return;

        hpSlider.value = parameterInGame.GetCurrentComplexParameter().health / parameterInGame.GetDefaultComplexParameter().health;
        stSlider.value = parameterInGame.GetCurrentStamina() / 100f;
    }

    public void SetMonster(GridUnit gridUnit)
    {
        if (gridUnit != null)
        {
            parameterInGame = gridUnit.GetComponent<ParameterInGame>();

            followTarget = gridUnit.GetComponent<MonsterInGame>().uiStatsPos;
        }
        else
        {
            parameterInGame = null;
        }

        CheckActive();
    }

    public void UpdateBuffImage()
    {
        if (parameterInGame == null || parameterInGame.inGameBuffDetails.Count <= 0)
        {
            buffGrid.gameObject.SetActive(false);

            return;
        }
        else
        {
            buffGrid.gameObject.SetActive(true);
        }

        int space = Mathf.Clamp(parameterInGame.inGameBuffDetails.Count - buffImages.Count, 0, int.MaxValue);

        for (int i = 0; i < buffImages.Count; i++)
        {
            int buffIndex = parameterInGame.inGameBuffDetails.Count - (1 + i);

            if (buffIndex >= 0)
            {
                buffImages[i].gameObject.SetActive(true);

                buffImages[i].sprite = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffSprite(parameterInGame.inGameBuffDetails[buffIndex].monsterSpecialBufferType).sprite;
            }
            else
            {
                buffImages[i].gameObject.SetActive(false);
            }
        }

        if (space <= 0)
        {
            buffExtTxt.gameObject.SetActive(false);
        }
        else
        {
            buffExtTxt.text = "x" + space;
        }

        buffGrid.Reposition();
    }

    public void UpdateDeBuffImage()
    {
        if (parameterInGame == null || parameterInGame.inGameDeBuffDetails.Count <= 0)
        {
            deBuffGrid.gameObject.SetActive(false);

            return;
        }
        else
        {
            deBuffGrid.gameObject.SetActive(true);
        }

        int space = Mathf.Clamp(parameterInGame.inGameDeBuffDetails.Count - deBuffImages.Count, 0, int.MaxValue);

        for (int i = 0; i < deBuffImages.Count; i++)
        {
            int buffIndex = parameterInGame.inGameDeBuffDetails.Count - (1 + i);

            if (buffIndex >= 0)
            {
                deBuffImages[i].gameObject.SetActive(true);

                Sprite sprite = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDeBuffSprite(parameterInGame.inGameDeBuffDetails[buffIndex].monsterSpecialDeBufferType).sprite;

                //if (sprite == null)
                //{
                //    Debug.Log("Sprite null ");
                //}
                //else
                //{
                //    Debug.Log("Sprite " + sprite.name);
                //}

                deBuffImages[i].sprite = sprite;
            }
            else
            {
                deBuffImages[i].gameObject.SetActive(false);
            }
        }

        if (space <= 0)
        {
            deBuffExtTxt.gameObject.SetActive(false);
        }
        else
        {
            deBuffExtTxt.text = "x" + space;
        }

        deBuffGrid.Reposition();
    }

    //private void OnValidate()
    //{
    //    UpdatePositionToFollowTarget();
    //}
}
