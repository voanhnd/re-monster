using System.Collections.Generic;
using UnityEngine;

public class MonsterBattleStatsUIController : MonoBehaviour
{
    public List<MonsterBattleStatsUI> monsterBattleStatsUIs = new List<MonsterBattleStatsUI>();

    public void SetupBattleStatUIs()
    {
        List<GridUnit> gridUnits = new List<GridUnit>();
        gridUnits.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Blue));

        gridUnits.AddRange(GameManager.GetUnitsOnTeam(GameTeam.Red));

        List<GridUnit> canUseList = new List<GridUnit>();

        foreach (GridUnit unit in gridUnits)
        {
            if (unit.IsDead() == false)
            {
                canUseList.Add(unit);
            }
        }

        for (int i = 0; i < monsterBattleStatsUIs.Count; i++)
        {
            if (i <= canUseList.Count - 1)
            {
                monsterBattleStatsUIs[i].SetMonster(canUseList[i]);
            }
            else
            {
                monsterBattleStatsUIs[i].SetMonster(null);
            }
        }
    }

    public void SetDeadUnit(GridUnit gridUnit)
    {
        for (int i = 0; i < monsterBattleStatsUIs.Count; i++)
        {
            if (monsterBattleStatsUIs[i].parameterInGame != null && monsterBattleStatsUIs[i].parameterInGame.GetComponent<GridUnit>() == gridUnit)
            {
                monsterBattleStatsUIs[i].SetMonster(null);

                break;
            }
        }
    }

    public void UpdateBuffsAndDebuffs()
    {
        for (int i = 0; i < monsterBattleStatsUIs.Count; i++)
        {
            monsterBattleStatsUIs[i].UpdateBuffImage();
            monsterBattleStatsUIs[i].UpdateDeBuffImage();
        }
    }
}
