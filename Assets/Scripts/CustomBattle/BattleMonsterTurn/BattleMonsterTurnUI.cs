using Assets.Scripts.Monster;
using System.Collections.Generic;
using UnityEngine;

public class BattleMonsterTurnUI : MonoBehaviour
{
    [SerializeField] private List<BattleMonsterTurnUIElement> battleMonsterTurnUIElements = new();
    public List<BattleMonsterTurnUIElement> BattleMonsterTurnUIElements => battleMonsterTurnUIElements;

    /// <summary>
    /// Only run at first time
    /// </summary>
    /// <param name="unitList"></param>
    public void AssignMonsterData(List<GridUnit> unitList)
    {
        for (int i = 0; i < battleMonsterTurnUIElements.Count; i++)
        {
            if (i < unitList.Count)
            {
                MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(unitList[i].GameMonster.MonsterInfo.monsterID);

                battleMonsterTurnUIElements[i].SetInfo(monsterDataScriptObj.Monster.monsterAvatar, unitList[i].GameMonster.GetBattleID(), unitList[i].GetTeam(), CustomGameManager.Instance.GetPlayerTeam());

                battleMonsterTurnUIElements[i].Active();

                CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

                battleMonsterTurnUIElements[i].SetTurnOrder(i, customGameRules.GetCurrentTurn());
            }
            else
            {
                battleMonsterTurnUIElements[i].Deactive();
            }
        }
    }

    public void UpdateMonsterTurnUIList(GridUnit turnUnit, List<GridUnit> currentTurnList, List<GridUnit> nextTurnList)
    {
        if (turnUnit == null)
        {
            return;
        }

        List<int> addedMonsterIdList = new();
        int order = 1; //current one always set as first order
        for (int i = 0; i < battleMonsterTurnUIElements.Count; i++)
        {
            if (turnUnit.GameMonster.GetBattleID() != battleMonsterTurnUIElements[i].GetMonsterBattleID()) continue;
            battleMonsterTurnUIElements[i].SetOnTurn();
            battleMonsterTurnUIElements[i].CachedTransform.SetAsFirstSibling();
            addedMonsterIdList.Add(battleMonsterTurnUIElements[i].GetMonsterBattleID());
            battleMonsterTurnUIElements[i].Active();
            break;
        }
        for (int i = 0; i < currentTurnList.Count; i++)
        {
            for (int j = 0; j < battleMonsterTurnUIElements.Count; j++)
            {
                ShiftTurnOrder(turnUnit, currentTurnList[i], battleMonsterTurnUIElements[j], ref order, ref addedMonsterIdList);
            }
        }
        for (int i = 0; i < nextTurnList.Count; i++)
        {
            for (int j = 0; j < battleMonsterTurnUIElements.Count; j++)
            {
                ShiftTurnOrder(turnUnit, nextTurnList[i], battleMonsterTurnUIElements[j], ref order, ref addedMonsterIdList);
            }
        }

        for (int j = 0; j < battleMonsterTurnUIElements.Count; j++)
        {
            if (addedMonsterIdList.Contains(battleMonsterTurnUIElements[j].GetMonsterBattleID())) continue;
            battleMonsterTurnUIElements[j].Deactive();
        }
    }

    private void ShiftTurnOrder(GridUnit currentTurnUnit, GridUnit turnUnit, BattleMonsterTurnUIElement battleMonsterTurnUIElement, ref int order, ref List<int> addedMonsterIdList)
    {
        if (turnUnit.GameMonster.GetBattleID() != battleMonsterTurnUIElement.GetMonsterBattleID()) return;
        if (currentTurnUnit == turnUnit) return;
        battleMonsterTurnUIElement.SetNotOnTurn();
        battleMonsterTurnUIElement.SetDisplayOrder(order++);
        addedMonsterIdList.Add(battleMonsterTurnUIElement.GetMonsterBattleID());
    }
}
