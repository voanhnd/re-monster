using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleMonsterTurnUIElement : MonoBehaviour
{
    private int monsterBattleId = -1;
    private int orderIndex;
    private int turnIndex;


    [SerializeField] private RectTransform container;
    [SerializeField] private Color blueTeamColor;
    [SerializeField] private Color redTeamColor;
    [SerializeField] private Vector2 currentTurnSize;
    [SerializeField] private Vector2 idleTurnSize;
    [SerializeField] private Image teamGraphic;
    [SerializeField] private TextMeshProUGUI idText;
    [SerializeField] private Image monsterAvatar;

    private Transform cachedTransform;

    public RectTransform Container => container;
    public int TurnIndex => turnIndex;

    public Transform CachedTransform=> cachedTransform;

    private void Awake()
    {
        cachedTransform = transform;
    }

    public void SetInfo(Sprite monsterAvatarSprite, int m_monsterID, GameTeam monsterTeam, GameTeam playerTeam)
    {
        gameObject.SetActive(true);

        monsterBattleId = m_monsterID;

        monsterAvatar.sprite = monsterAvatarSprite;

        idText.text = Helpers.ConvertNumberToAlphabet(monsterBattleId);

        if (monsterTeam == playerTeam)
            teamGraphic.color = blueTeamColor;
        else
            teamGraphic.color = redTeamColor;
    }

    public int GetMonsterBattleID() => monsterBattleId;

    public void Deactive() => gameObject.SetActive(false);

    public void Active() => gameObject.SetActive(true);

    public void SetNotOnTurn()
    {
        container.sizeDelta = idleTurnSize;
    }

    public void SetOnTurn()
    {
        container.sizeDelta = currentTurnSize;
    }

    public void SetTurnOrder(int newOrderIndex, int turnOrder)
    {
        turnIndex = turnOrder;
        orderIndex = newOrderIndex;
        cachedTransform.SetSiblingIndex(orderIndex);
    }

    public void SetDisplayOrder(int order)
    {
        cachedTransform.SetSiblingIndex(order);
    }
}
