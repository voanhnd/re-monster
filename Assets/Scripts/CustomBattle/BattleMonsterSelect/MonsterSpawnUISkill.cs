using UnityEngine;
using UnityEngine.UI;

public class MonsterSpawnUISkill : MonoBehaviour
{
    private MonsterSpawnUI monsterSpawnUI;

    public int skillIndex = -1;

    public Image skillImage;

    public Button selectButton;
    private Transform cachedTransform;

    private void Awake()
    {
        cachedTransform = transform;
    }

    private void Start()
    {
        selectButton.onClick.AddListener(() => { ChangeSkill(); });
    }

    public void ActiveSkill(Sprite monsterSprite)
    {
        skillImage.sprite = monsterSprite;

        skillImage.gameObject.SetActive(true);
    }

    public void DeactiveSkill() => skillImage.gameObject.SetActive(false);

    public void ChangeSkill() => monsterSpawnUI.ChangeSkill(skillIndex, cachedTransform.GetSiblingIndex());

    public void SetMonsterSpawnUI(MonsterSpawnUI t_monsterSpawnUI) => monsterSpawnUI = t_monsterSpawnUI;
}
