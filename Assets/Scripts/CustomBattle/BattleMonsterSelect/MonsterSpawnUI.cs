using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.Skill;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MonsterSpawnUI : MonoBehaviour
{
    public MonsterSpawnListController monsterSpawnListController;

    public Image avatarIMG;
    public TextMeshProUGUI monsterNameTxt;

    PlayerInfo.PlayerMonsterInfo playerMonsterInfo = null;

    public TextMeshProUGUI hp_txt;
    public TextMeshProUGUI str_txt;
    public TextMeshProUGUI int_txt;
    public TextMeshProUGUI dex_txt;
    public TextMeshProUGUI agi_txt;
    public TextMeshProUGUI vit_txt;

    public Button selectBttn;
    public Button spawnedBttn;

    public Transform selectBG;
    public Transform blockBG;

    public List<MonsterSpawnUISkill> monsterSpawnUISkills = new();

    [SerializeField]
    List<Monster.MonsterSkillInfo> usingSkillInfos = new();

    private void Start()
    {
        selectBttn.onClick.AddListener(() => Select());
        spawnedBttn.onClick.AddListener(() => Select());

        spawnedBttn.gameObject.SetActive(false);
        selectBG.gameObject.SetActive(false);
        blockBG.gameObject.SetActive(false);
    }

    public PlayerInfo.PlayerMonsterInfo GetMonsterInfo()
    {
        return playerMonsterInfo;
    }

    public void SetMonsterInfo(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        this.playerMonsterInfo = playerMonsterInfo;

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        FarmTrait.FarmTraitInfo farmTrait = FarmTrait.GetFarmTraitInfo(playerFarmInfo.farmTraitType);

        InnateTrait.InnateTraitInfo innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);

        List<AcquiredTrait.AcquiredTraitInfo> acquiredTraitInfos = AcquiredTrait.GetAcquiredTraitList(playerMonsterInfo.acquiredTraits);

        AcquiredTrait.AcquiredTraitInfo complexAcquiredTrait = AcquiredTrait.GetComplexAcquiredTrait(acquiredTraitInfos);

        MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        BasicParameters basicParametersByTrait = new BasicParameters
            (
             farmTrait.health + complexAcquiredTrait.health,
             farmTrait.strenght + complexAcquiredTrait.strenght,
             farmTrait.intelligent + complexAcquiredTrait.intelligent,
             farmTrait.dexterity + complexAcquiredTrait.dexterity,
             farmTrait.agility + complexAcquiredTrait.agility,
             farmTrait.vitality + complexAcquiredTrait.vitality
            );

        BasicParameters playerMonsterInfoParameters = Monster.Get_Complex_Parameter(Monster.Get_Battle_PlayerInfo_Basic_Parameter(playerMonsterInfo, monsterDataScriptObj), basicParametersByTrait);

        BasicParameters complexParameters = Monster.Get_Complex_Parameter(playerMonsterInfoParameters, monsterDataScriptObj.Monster.basicParameters);

        avatarIMG.sprite = monsterDataScriptObj.Monster.monsterAvatar;

        monsterNameTxt.text = monsterDataScriptObj.Monster.monsterName;

        hp_txt.text = "" + Mathf.FloorToInt(complexParameters.health);
        str_txt.text = "" + Mathf.FloorToInt(complexParameters.strenght);
        int_txt.text = "" + Mathf.FloorToInt(complexParameters.intelligent);
        dex_txt.text = "" + Mathf.FloorToInt(complexParameters.dexterity);
        agi_txt.text = "" + Mathf.FloorToInt(complexParameters.agility);
        vit_txt.text = "" + Mathf.FloorToInt(complexParameters.vitality);

        if (playerMonsterInfo.SelectedBattleSkillsId.Count == 0)
        {
            List<string> usingSkillList = new List<string>();

            usingSkillList.Add(playerMonsterInfo.MonsterSkillInfos[0].skillID);
            usingSkillList.Add(playerMonsterInfo.MonsterSkillInfos[1].skillID);

            playerMonsterInfo.SelectBattleSkill(usingSkillList);
        }

        UpdateUsingSkillInfos(Skill.GetUsingMonsterSkillInfos(playerMonsterInfo, playerMonsterInfo.SelectedBattleSkillsId));

    }

    public void UpdateUsingSkillInfos(List<Monster.MonsterSkillInfo> inList)
    {
        usingSkillInfos = new List<Monster.MonsterSkillInfo>();
        usingSkillInfos.AddRange(inList);

        MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        for (int i = 0; i < monsterSpawnUISkills.Count; i++)
        {
            MonsterSpawnUISkill ui = monsterSpawnUISkills[i];

            ui.SetMonsterSpawnUI(this);

            ui.skillIndex = i;

            if (i < usingSkillInfos.Count && usingSkillInfos[i] != null)
            {
                int skillLevel = usingSkillInfos[i].skillLevel;

                if (skillLevel > 0)
                {
                    UnitAbility unitAbility = Skill.GetUnitAbility(usingSkillInfos[i].skillID, monsterDataScriptObj);

                    ui.ActiveSkill(unitAbility.GetIconSprite());
                }
                else
                {
                    ui.DeactiveSkill();
                }
            }
            else
            {
                ui.DeactiveSkill();
            }
        }
    }

    public List<Monster.MonsterSkillInfo> GetUsingSkillInfos()
    {
        return usingSkillInfos;
    }

    public void Select()
    {
        MonsterSpawnUI selectedSpawnUI = monsterSpawnListController.GetSelectedMonsterSpawnUI();

        if (selectedSpawnUI == null || selectedSpawnUI.playerMonsterInfo.monsterID != playerMonsterInfo.monsterID)
        {
            monsterSpawnListController.SetSelectMonster(this);
        }
    }

    public void ChangeSkill(int skillIndex, int skillSlotIndex)
    {
        if (skillIndex < usingSkillInfos.Count)
        {
            monsterSpawnListController.BattleSelectSkillsUI.SetupMenu(this, usingSkillInfos[skillIndex], skillSlotIndex);
        }
        else
        {
            monsterSpawnListController.BattleSelectSkillsUI.SetupMenu(this, null, skillSlotIndex);
        }
    }

    public void CheckButtonStats()
    {
        MonsterSpawnUI selectedSpawnUI = monsterSpawnListController.GetSelectedMonsterSpawnUI();

        if (selectedSpawnUI != null)
        {
            if (selectedSpawnUI == this)
            {
                selectBttn.gameObject.SetActive(false);
                selectBG.gameObject.SetActive(true);
                blockBG.gameObject.SetActive(false);
                spawnedBttn.gameObject.SetActive(false);
            }
            else
            {
                selectBttn.gameObject.SetActive(false);
                selectBG.gameObject.SetActive(false);
                blockBG.gameObject.SetActive(true);
                spawnedBttn.gameObject.SetActive(false);
            }
        }
        else
        {
            if (CheckInSpawnInfoList())
            {
                selectBttn.gameObject.SetActive(false);
                selectBG.gameObject.SetActive(false);
                blockBG.gameObject.SetActive(false);
                spawnedBttn.gameObject.SetActive(true);
            }
            else
            {
                if (monsterSpawnListController.IsLimited())
                {
                    selectBttn.gameObject.SetActive(false);
                    selectBG.gameObject.SetActive(false);
                    blockBG.gameObject.SetActive(true);
                    spawnedBttn.gameObject.SetActive(false);
                }
                else
                {
                    selectBttn.gameObject.SetActive(true);
                    selectBG.gameObject.SetActive(false);
                    blockBG.gameObject.SetActive(false);
                    spawnedBttn.gameObject.SetActive(false);
                }
            }
        }
    }

    bool CheckInSpawnInfoList()
    {
        foreach (CustomGameManager.SpawnListInfo info in CustomGameManager.Instance.FriendlySpawnListInfo)
        {
            if (info.playerMonsterInfo.monsterID == playerMonsterInfo.monsterID)
            {
                return true;
            }
        }

        return false;
    }
}
