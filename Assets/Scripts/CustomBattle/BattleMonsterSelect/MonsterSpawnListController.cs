using Assets.Scripts.CustomBattle.BattleSelectSkillUI;
using Assets.Scripts.Monster;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MonsterSpawnListController : MonoBehaviour
{
    [SerializeField] private BattleSelectSkillsUI battleSelectSkillsUI;

    [SerializeField] private MonsterSpawnUI monsterSpawnUIPrefab;

    [SerializeField] private MonsterSpawnUI selectedMonsterSpawnUI;
    private CustomGameManager.SpawnListInfo selectedSpawnInfo;

    [SerializeField] private Transform monsterObjectContainer;

    [SerializeField] private TextMeshProUGUI monsterPlaceNumTxt;

    [SerializeField] private Button spawnCancelButton;
    [SerializeField] private Button spawnDoneButton;
    [SerializeField] private Button selectMonsterDoneButton;
    [SerializeField] private Button backToHomeBtn;

    [SerializeField] private UnitAI unitAI;

    [SerializeField] VerticalLayoutGroup monsterSpawnUIContainer;
    [SerializeField] private List<MonsterSpawnUI> monsterSpawnUIs = new();

    private int maxMonsterToSpawn = 3;

    public BattleSelectSkillsUI BattleSelectSkillsUI => battleSelectSkillsUI;

    private void Awake()
    {
        backToHomeBtn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        });
        spawnCancelButton.onClick.AddListener(() => CancelMonsterSpawnButton());

        spawnDoneButton.onClick.AddListener(() => ConfirmMonsterSpawnButton());

        selectMonsterDoneButton.onClick.AddListener(() => ConfirmSelectMonsterDoneBttn());
    }

    private void Start()
    {
        battleSelectSkillsUI.gameObject.SetActive(false);
    }

    private void Update()
    {
        UpdateSpawnCell();
    }

    private void UpdateSpawnCell()
    {
        if (CustomGameManager.Instance.GetIsPlayingBool()) { return; }

        if (selectedMonsterSpawnUI == null || selectedSpawnInfo == null) { return; }

        ILevelCell hoverCell = GameManager.Get().GetHoverCell();

        if (hoverCell == null || !CustomGridManager.Instance.canSpawnCells.Contains(hoverCell) || !CheckCanMoveOn(hoverCell, selectedSpawnInfo, CustomGameManager.Instance.FriendlySpawnListInfo)) { return; }

        if (selectedSpawnInfo.levelCell != hoverCell && !selectedSpawnInfo.spawnPosSet)
        {
            selectedSpawnInfo.levelCell = hoverCell;

            MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(selectedSpawnInfo.playerMonsterInfo.monsterID);

            selectedSpawnInfo.spawnedModel.transform.position = GetAllignPos(monsterDataScriptObj.unitData, selectedSpawnInfo.levelCell);

            SetLookAtCell(selectedSpawnInfo.spawnedModel, selectedSpawnInfo.levelCell, CustomGameManager.Instance.GetPlayerTeam());
        }

        if (Input.GetMouseButtonDown(0) && !selectedSpawnInfo.spawnPosSet)
        {
            selectedSpawnInfo.spawnPosSet = true;

            ConfirmMonsterSpawnButton();
        }
        else if (Input.GetMouseButtonDown(0) && selectedSpawnInfo.spawnPosSet)
        {
            selectedSpawnInfo.spawnPosSet = false;
        }
    }

    private bool CheckCanMoveOn(ILevelCell hoverCell, CustomGameManager.SpawnListInfo checkInfo, List<CustomGameManager.SpawnListInfo> spawnList)
    {
        for (int i = 0; i < spawnList.Count; i++)
        {
            CustomGameManager.SpawnListInfo info = spawnList[i];
            if (info != checkInfo && info.levelCell == hoverCell)
            {
                return false;
            }
        }

        return true;
    }

    private Vector3 GetAllignPos(UnitData unitData, ILevelCell SpawnOnCell)
    {
        float objectHeightOffset = 0.0f;

        objectHeightOffset += unitData.m_HeightOffset;

        Renderer CellRenderer = SpawnOnCell.GetRenderer();
        if (CellRenderer)
        {
            float cellHeight = CellRenderer.bounds.size.y;
            objectHeightOffset += (cellHeight * 0.5f);

            Vector3 CellPosition = SpawnOnCell.transform.position;
            return CellPosition + new Vector3(0, objectHeightOffset, 0); ;
        }

        return SpawnOnCell.transform.position;
    }

    private void SetLookAtCell(GameObject lookObj, ILevelCell spawnCell, GameTeam gameTeam)
    {
        if (gameTeam == GameTeam.Blue)
        {
            Debug.Log("Look to red");

            Quaternion rotation = Quaternion.LookRotation(spawnCell.transform.right, lookObj.transform.up);
            lookObj.transform.rotation = rotation;
            return;
        }
        if (gameTeam == GameTeam.Red)
        {
            Debug.Log("Look to blue");

            Quaternion rotation = Quaternion.LookRotation(-spawnCell.transform.right, lookObj.transform.up);
            lookObj.transform.rotation = rotation;
            return;
        }
    }

    public void SpawnMonsterToChoice()
    {
        for (int i = 0; i < PlayerStats.Instance.playerInfo.PlayerMonsterInfos.Count; i++)
        {
            PlayerInfo.PlayerMonsterInfo playerMonsterInfo = PlayerStats.Instance.playerInfo.PlayerMonsterInfos[i];
            if (playerMonsterInfo.usingFarmID != string.Empty)
            {
                monsterSpawnUIPrefab.monsterSpawnListController = this;

                MonsterSpawnUI monsterSpawnUI = Instantiate(monsterSpawnUIPrefab, monsterSpawnUIContainer.transform);

                monsterSpawnUI.SetMonsterInfo(playerMonsterInfo);

                monsterSpawnUIs.Add(monsterSpawnUI);
            }
        }

        CheckButtonStats();
    }

    public void CreateAIEnemyMonster()
    {
        GameTeam enemyTeam = CustomGameManager.GetEnemyTeam(CustomGameManager.Instance.GetPlayerTeam());

        Debug.Log("Enemy team = " + enemyTeam.ToString());

        List<ILevelCell> canSpawnCells = CustomGridManager.Instance.GetCanSpawnCells(enemyTeam);

        for (int i = 0; i < 3; i++)
        {
            int ranIndex = Random.Range(0, PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterList().Count);

            MonsterDataScriptObj selected = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterList()[ranIndex];

            CustomGameManager.Instance.EnemySpawnListInfos.Add(CreateMonsterAI(selected.Monster.monsterRace, GetRandomSpawnCell(canSpawnCells, CustomGameManager.Instance.EnemySpawnListInfos),
                enemyTeam, selected.Monster.monsterID, "ai_" + i + "_" + selected.Monster.monsterRace.mainRace.ToString() + selected.Monster.monsterRace.subRace.ToString()));
        }

        ////Test
        //{
        //    string monsterID = "IceLime_Eien";
        //    MonsterDataScriptObj selected = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterID);

        //    CustomGameManager.Instance.EnemySpawnListInfos.Add(CreateMonsterAI(selected.Monster.monsterRace, GetRandomSpawnCell(canSpawnCells, CustomGameManager.Instance.EnemySpawnListInfos),
        //        enemyTeam, selected.Monster.monsterID, "ai_" + 0 + "_" + selected.Monster.monsterRace.mainRace.ToString() + selected.Monster.monsterRace.subRace.ToString()));
        //}

        //{
        //    string monsterID = "IceLime_Eien";
        //    MonsterDataScriptObj selected = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterID);

        //    CustomGameManager.Instance.EnemySpawnListInfos.Add(CreateMonsterAI(selected.Monster.monsterRace, GetRandomSpawnCell(canSpawnCells, CustomGameManager.Instance.EnemySpawnListInfos),
        //        enemyTeam, selected.Monster.monsterID, "ai_" + 1 + "_" + selected.Monster.monsterRace.mainRace.ToString() + selected.Monster.monsterRace.subRace.ToString()));
        //}

        //{
        //    string monsterID = "Kidsfight_Hero";
        //    MonsterDataScriptObj selected = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterID);

        //    CustomGameManager.Instance.EnemySpawnListInfos.Add(CreateMonsterAI(selected.Monster.monsterRace, GetRandomSpawnCell(canSpawnCells, CustomGameManager.Instance.EnemySpawnListInfos),
        //        enemyTeam, selected.Monster.monsterID, "ai_" + 2 + "_" + selected.Monster.monsterRace.mainRace.ToString() + selected.Monster.monsterRace.subRace.ToString()));
        //}
    }

    public CustomGameManager.SpawnListInfo CreateMonsterAI(Monster.MonsterRace monsterRace, ILevelCell spawnCell, GameTeam gameTeam, string monsterID, string monsterName)
    {
        CustomGameManager.SpawnListInfo newSpawnInfo = new()
        {
            levelCell = spawnCell,

            playerMonsterInfo = PlayerStats.Instance.monsterDataManagerScriptObj.CreatPlayerMonsterInfo(monsterID)
        };

        newSpawnInfo.playerMonsterInfo.monsterID = monsterID;

        newSpawnInfo.playerMonsterInfo.monsterName = monsterName;

        for (int i = 0; i < newSpawnInfo.playerMonsterInfo.MonsterSkillInfos.Count; i++)
        {
            int monsterSkillIndex = i;
            Monster.MonsterSkillInfo monsterSkillInfo = newSpawnInfo.playerMonsterInfo.MonsterSkillInfos[monsterSkillIndex];
            monsterSkillInfo.skillLevel = 1;
        }

        MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(newSpawnInfo.playerMonsterInfo.monsterID);

        newSpawnInfo.spawnedModel = SpawnMonsterModel(monsterDataScriptObj, newSpawnInfo.levelCell, gameTeam);

        return newSpawnInfo;
    }

    public ILevelCell GetRandomSpawnCell(List<ILevelCell> cellList, List<CustomGameManager.SpawnListInfo> spawnList)
    {
        List<ILevelCell> canUseCells = new();
        canUseCells.AddRange(cellList);

        for (int i = 0; i < spawnList.Count; i++)
        {
            CustomGameManager.SpawnListInfo info = spawnList[i];
            if (info.levelCell != null)
            {
                canUseCells.Remove(info.levelCell);
            }
        }

        if (canUseCells.Count > 0)
        {
            int ran = Random.Range(0, canUseCells.Count);

            return canUseCells[ran];
        }

        return null;
    }

    public GameObject SpawnMonsterModel(MonsterDataScriptObj monsterDataScriptObj, ILevelCell spawnCell, GameTeam gameTeam)
    {
        Debug.Log("Spawn monster " + monsterDataScriptObj.Monster.monsterName + " team = " + gameTeam.ToString());

        GameObject spawnedModel = Instantiate(monsterDataScriptObj.unitData.m_Model, monsterObjectContainer);

        spawnedModel.transform.position = GetAllignPos(monsterDataScriptObj.unitData, spawnCell);

        SetLookAtCell(spawnedModel, spawnCell, gameTeam);

        return spawnedModel;
    }

    public MonsterSpawnUI GetSelectedMonsterSpawnUI()
    {
        if (selectedMonsterSpawnUI != null)
        {
            return selectedMonsterSpawnUI;
        }

        return null;
    }

    public void SetSelectMonster(MonsterSpawnUI monsterSpawnUI)
    {
        selectedMonsterSpawnUI = monsterSpawnUI;

        bool infoExisted = false;

        for (int i = 0; i < CustomGameManager.Instance.FriendlySpawnListInfo.Count; i++)
        {
            CustomGameManager.SpawnListInfo spawnInfo = CustomGameManager.Instance.FriendlySpawnListInfo[i];
            if (selectedMonsterSpawnUI.GetMonsterInfo().monsterID == spawnInfo.playerMonsterInfo.monsterID)
            {
                infoExisted = true;

                selectedSpawnInfo = spawnInfo;

                break;
            }
        }

        if (!infoExisted)
        {
            CustomGameManager.SpawnListInfo newSpawnInfo = new()
            {
                playerMonsterInfo = selectedMonsterSpawnUI.GetMonsterInfo(),

                levelCell = GetRandomSpawnCell(CustomGridManager.Instance.canSpawnCells, CustomGameManager.Instance.FriendlySpawnListInfo)
            };

            MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(newSpawnInfo.playerMonsterInfo.monsterID);

            newSpawnInfo.spawnedModel = SpawnMonsterModel(monsterDataScriptObj, newSpawnInfo.levelCell, CustomGameManager.Instance.GetPlayerTeam());

            selectedSpawnInfo = newSpawnInfo;

            CustomGameManager.Instance.FriendlySpawnListInfo.Add(selectedSpawnInfo);
        }

        for (int i = 0; i < monsterSpawnUIs.Count; i++)
        {
            int monsterIndex = i; // to prevent wrong data transfer
            monsterSpawnUIs[monsterIndex].CheckButtonStats();
        }

        CheckButtonStats();
    }

    public void EditSpawnedMonster(ILevelCell levelCell)
    {
        CustomGameManager.SpawnListInfo selectedSpawnInfo = null;

        foreach (CustomGameManager.SpawnListInfo spawnInfo in CustomGameManager.Instance.FriendlySpawnListInfo)
        {
            if (spawnInfo.levelCell == levelCell)
            {
                selectedSpawnInfo = spawnInfo;

                break;
            }
        }

        if (selectedSpawnInfo != null)
        {
            foreach (MonsterSpawnUI monsterSpawnUI in monsterSpawnUIs)
            {
                if (monsterSpawnUI.GetMonsterInfo().monsterID == selectedSpawnInfo.playerMonsterInfo.monsterID)
                {
                    monsterSpawnUI.Select();

                    break;
                }
            }
        }
    }

    public void CancelMonsterSpawnButton()
    {
        if (selectedMonsterSpawnUI != null)
        {
            CustomGameManager.SpawnListInfo cancelSpawnInfo = null;

            for (int i = 0; i < CustomGameManager.Instance.FriendlySpawnListInfo.Count; i++)
            {
                CustomGameManager.SpawnListInfo spawnInfo = CustomGameManager.Instance.FriendlySpawnListInfo[i];
                if (selectedMonsterSpawnUI.GetMonsterInfo().monsterID == spawnInfo.playerMonsterInfo.monsterID)
                {
                    cancelSpawnInfo = spawnInfo;
                    break;
                }
            }

            if (cancelSpawnInfo != null)
            {
                CustomGameManager.Instance.FriendlySpawnListInfo.Remove(cancelSpawnInfo);

                Destroy(cancelSpawnInfo.spawnedModel);

                selectedMonsterSpawnUI = null;
                selectedSpawnInfo = null;

                for (int i = 0; i < monsterSpawnUIs.Count; i++)
                {
                    MonsterSpawnUI monsterUI = monsterSpawnUIs[i];
                    monsterUI.CheckButtonStats();
                }
            }
        }

        CheckButtonStats();
    }

    public void ConfirmMonsterSpawnButton()
    {
        if (selectedMonsterSpawnUI != null)
        {
            selectedMonsterSpawnUI = null;
            selectedSpawnInfo = null;

            for (int i = 0; i < monsterSpawnUIs.Count; i++)
            {
                MonsterSpawnUI monsterUI = monsterSpawnUIs[i];
                monsterUI.CheckButtonStats();
            }
        }

        CheckButtonStats();
    }

    public void ConfirmSelectMonsterDoneBttn()
    {
        CustomGameRules customGameRules = GameManager.GetRules() as CustomGameRules;

        customGameRules.PlaceUnits(CustomGameManager.Instance.GetPlayerTeam(), CustomGameManager.Instance.FriendlySpawnListInfo, unitAI);

        GameTeam enemyTeam = CustomGameManager.GetEnemyTeam(CustomGameManager.Instance.GetPlayerTeam());
        customGameRules.PlaceUnits(enemyTeam, CustomGameManager.Instance.EnemySpawnListInfos, unitAI);

        CustomGameManager.Instance.SetActiveAI(enemyTeam, true);

        CustomGameManager.Instance.SetPlay();
    }

    public bool IsLimited() => CustomGameManager.Instance.FriendlySpawnListInfo.Count >= maxMonsterToSpawn;

    private void CheckButtonStats()
    {
        monsterPlaceNumTxt.text = "Monster placement " + CustomGameManager.Instance.FriendlySpawnListInfo.Count + @"/" + maxMonsterToSpawn;

        if (selectedMonsterSpawnUI != null)
        {
            spawnCancelButton.interactable = true;
            spawnDoneButton.interactable = true;
            selectMonsterDoneButton.gameObject.SetActive(false);
        }
        else
        {
            if (CustomGameManager.Instance.FriendlySpawnListInfo.Count > 0)
            {
                spawnCancelButton.interactable = false;
                spawnDoneButton.interactable = false;
                selectMonsterDoneButton.gameObject.SetActive(true);
            }
            else
            {
                spawnCancelButton.interactable = false;
                spawnDoneButton.interactable = false;
                selectMonsterDoneButton.gameObject.SetActive(false);
            }
        }
    }
}
