using UnityEngine;

public class CustomGameUIManager : MonoBehaviour
{
    public static CustomGameUIManager instance;

    public GameObject playUIContainer;
    public GameObject monsterSpawnUIContainer;

    public BattleMonsterTurnUI battleMonsterTurnUI;

    public AbilityListUIElement abilityListUIElement;

    //public MonsterBattleStatsUIController monsterBattleStatsUIController;

    private void Awake()
    {
        instance = this;
    }

    public void Active_PlayUIContainer(bool active)
    {
        playUIContainer.SetActive(active);

        if (active)
        {
            Active_MonsterSpawnUIContainer(false);
        }
        else
        {

        }
    }

    public void Active_MonsterSpawnUIContainer(bool active)
    {
        monsterSpawnUIContainer.SetActive(active);

        if (active)
        {
            Active_PlayUIContainer(false);
        }
        else
        {

        }
    }
}
