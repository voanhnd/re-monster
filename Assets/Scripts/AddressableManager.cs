using System;
using UnityEngine;
//using UnityEngine.AddressableAssets;
//using UnityEngine.AddressableAssets.ResourceLocators;
//using UnityEngine.ResourceManagement.AsyncOperations;

//[Serializable]
//public class AssetReferenceAudioClip : AssetReferenceT<AudioClip>
//{
//    AssetReferenceAudioClip(string guid) : base(guid)
//    {

//    }
//}

[Serializable]
public class AssetReferenceUI
{
    //[Header("Home UI")]
    //public AssetReference UI_HomeUI_SlotMonster;

    //public UI_Home_SlotMonster UI_HomeUI_SlotMonster;

    //[Header("Raise UI")]
    //public AssetReference UI_RaiseUI_SlotMonster;
    //public UI_RaiseUI_SlotMonster UI_RaiseUI_SlotMonster;
}

public class AddressableManager : MonoBehaviour
{
    public static AddressableManager instance;

    public AssetReferenceUI assetReference_UI;

    //public AssetReferenceAudioClip AssetReference_AudioClips;

    private void Awake()
    {
        _MakeSingleton();
    }

    // Start is called before the first frame update
    //void Start()
    //{
    //    Addressables.InitializeAsync().Completed += AddressableManager_Completed; ;
    //}

    //private void AddressableManager_Completed(AsyncOperationHandle<IResourceLocator> obj)
    //{
    //    Debug.Log("Asset Async completed");
    //}


    // Update is called once per frame
    void Update()
    {

    }

    void _MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
    }
}
