using Proyecto26;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogIn : StaticInstance<LogIn>
{
    private const string url = "https://demo.remonster.world";
    private const string loginApi = "/api/user/walletlogin";
    private const string message = "Login with Re.Monster";

    private string _token;
    private string _walletAddress;
    private string _signature;

    public GameObject ErrorPanel;

    public string Url => url;
    public string LoginApi => loginApi;
    public string Message => message;
    public string Token { get => _token; set => _token = value; }
    public string WalletAddress { get => _walletAddress; set => _walletAddress = value; }
    public string Signature { get => _signature; set => _signature = value; }

    private void Start()
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = false;
#endif
    }

    public async void SignMsg()
    {
        _signature = await ThirdwebManager.Instance.SDK.wallet.Sign(message);
        _walletAddress = await ThirdwebManager.Instance.SDK.wallet.GetAddress();
        ApiLogInWithWallet();
    }

    public async void SendMsg()
    {
        //var data = await ThirdwebManager.Instance.SDK.wallet.SendRawTransaction(new TransactionRequest()
        //{
        //    from = WalletAddress, // The address the transaction is sent from.
        //    to = "0x275Ae07CAF229C824563220fcD303F22034c8f33", // The address the transaction is directed to.
        //    data = "0x6057361d0000000000000000000000000000000000000000000000000000000000000001", // The data to send with the transaction
        //    value = "0x0" // send native tokens with the contract call
        //});
    }

    public void ApiLogInWithWallet()
    {
        var apiUrl = url + loginApi;

        Debug.Log($"{_walletAddress} {_signature} {message}");
        RestClient.Post<ServerResponse>(apiUrl, new UserID
        {
            address_wallet = _walletAddress,
            signature = _signature,
            message = message

        }).Then(response =>
        {
            Debug.Log("success: " + response.success + " Ok");
            Debug.Log("message: " + response.message + " Ok");
            Debug.Log("code: " + response.code + " Ok");
            _token = response.data.token;
            PlayerPrefs.SetString("balance", response.data.oas_balance);
            //PlayerStats.Instance.playerInfo.oas = response.data.oas_balance;
            if (response.data.has_chip)
            {
                LoadScene1();
            }
            else
            {
                ErrorPanel.SetActive(true);
                LoadScene1();
            }
        });
    }

    public void LoadScene1() => SceneManager.LoadScene(1);
}

[Serializable]
public class ServerResponse
{
    public bool success;
    public string message;
    public Data data;
    public int code;
}
[Serializable]
public class UserID
{
    public string address_wallet;
    public string signature;
    public string message;
}
[Serializable]
public class User
{
    public string STATUS;
    public string USER_NAME;
    public string WALLET_ADDRESS;
}
[Serializable]
public class Data
{
    public User user;
    public string token;
    public bool has_chip;
    public string oas_balance;
}