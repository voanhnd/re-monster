﻿using System;
using UnityEngine;

namespace Assets.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "New enviroment icon", menuName = "ScriptableObjects/Create enviroment icon")]
    public class UIMonsterEnviromentIcon : ScriptableObject
    {
        [SerializeField] private Sprite enviromentIcon;
        [SerializeField] private Color enviromentColor;
        [SerializeField] private TrainingArea.TrainingArea.TerrainType enviroment;

        public Sprite EnviromentIcon => enviromentIcon;
        public Color EnviromentColor => enviromentColor;
        public TrainingArea.TrainingArea.TerrainType Enviroment => enviroment;

        public static UIMonsterEnviromentIcon GetEnviromentIcon(UIMonsterEnviromentIcon[] enviromentArray, TrainingArea.TrainingArea.TerrainType enviroment)
        {
            for (int i = 0; i < enviromentArray.Length; i++)
            {
                if (enviromentArray[i].enviroment == enviroment)
                    return enviromentArray[i];
            }
            return null;
        }
    }
}