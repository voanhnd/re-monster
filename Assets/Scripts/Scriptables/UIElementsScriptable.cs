﻿using UnityEngine;

namespace Assets.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "New element data", menuName = "ScriptableObjects/Create element data")]
    public class UIElementsScriptable : ScriptableObject
    {
        [SerializeField] private Sprite elementIcon;
        [SerializeField] private Color elementColor;
        [SerializeField] private AttributeType element;
        [SerializeField] private string elementNameJp;

        public Sprite ElementIcon => elementIcon;
        public Color ElementColor => elementColor;
        public AttributeType Element => element;
        public string ElementNameJp => elementNameJp;
        public static UIElementsScriptable GetElementData(UIElementsScriptable[] elementArray, AttributeType element)
        {
            for (int i = 0; i < elementArray.Length; i++)
            {
                if (elementArray[i].element == element)
                    return elementArray[i];
            }
            return null;
        }
    }
}