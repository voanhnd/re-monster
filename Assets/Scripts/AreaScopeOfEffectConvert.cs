﻿
using Assets.Scripts.Monster.Skill;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class AreaScopeOfEffectConvert : Singleton<AreaScopeOfEffectConvert>
    {
        [SerializeField] private List<EffectScopeName> scopeConvertList = new();
        [SerializeField] private List<ExecutionTypeName> executionConvertList = new();
        private Dictionary<Skill.AreaOfEffectType, EffectScopeName> scopeConvertDictionary = new();
        private Dictionary<Skill.OccurrenceType, ExecutionTypeName> executionDictionary = new();

        protected override void Awake()
        {
            base.Awake();
            for (int i = 0; i < scopeConvertList.Count; i++)
            {
                if (!scopeConvertDictionary.TryAdd(scopeConvertList[i].EffectScope, scopeConvertList[i]))
                {
                    Debug.Log($"{scopeConvertList[i].EffectScope} dupplicated");
                }
            }

            for (int i = 0; i < scopeConvertList.Count; i++)
            {
                if (!executionDictionary.TryAdd(executionConvertList[i].ExecutionType, executionConvertList[i]))
                {
                    Debug.Log($"{executionConvertList[i].ExecutionType} dupplicated");
                }
            }
        }

        public string ConvertScopeEng(Skill.AreaOfEffectType effectScope)
        {
            if (scopeConvertDictionary.TryGetValue(effectScope, out EffectScopeName result))
            {
                return result.EnNameConvert;
            }
            return string.Empty;
        }

        public string ConvertScopeEng(Skill.OccurrenceType executeionType)
        {
            if(executionDictionary.TryGetValue(executeionType, out ExecutionTypeName result))
            {
                return result.EnNameConvert;
            }
            return string.Empty;
        }

        private void Start()
        {
            Destroy(gameObject);
        }

        #region Serializable
        [Serializable]
        private class EffectScopeName
        {
            [SerializeField] private Skill.AreaOfEffectType effectScope;
            [SerializeField] private string enNameConvert;
            [SerializeField] private string jpNameConvert;

            public Skill.AreaOfEffectType EffectScope => effectScope;
            public string EnNameConvert => enNameConvert;
            public string JpNameConvert => jpNameConvert;
        }

        [Serializable]
        private class ExecutionTypeName
        {
            [SerializeField] private Skill.OccurrenceType executionType;
            [SerializeField] private string enNameConvert;
            [SerializeField] private string jpNameConvert;

            public Skill.OccurrenceType ExecutionType => executionType;
            public string EnNameConvert => enNameConvert;
            public string JpNameConvert => jpNameConvert;
        }
        #endregion
    }
}