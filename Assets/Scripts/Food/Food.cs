using UnityEngine;

[System.Serializable]
public class Food
{
    public enum FoodType
    {
        Empty,
        Vegetable,
        Fish,
        Meat,
        Food_Supplement,
        Dessert_Food,
        Premium_Vegetable,
        Premium_Fish,
        Premium_Meat,
        Premium_Food_Supplement,
        Premium_Dessert_Food,
    }

    [SerializeField]
    private FoodType foodType;
    [SerializeField]
    private string foodId;
    [SerializeField]
    private string foodName;
    [SerializeField]
    private float foodPrice;

    [SerializeField]
    float energy = 0f;
    [SerializeField]
    float body = 0f;
    [SerializeField]
    float condition = 0f;
    [SerializeField]
    float bodyTypeValue = 0f;
    [SerializeField]
    float fatigue = 0f;
    [SerializeField]
    float stress = 0f;

    public FoodType FoodTypeVal { get => foodType; private set => foodType = value; }
    public string FoodId { get => foodId; private set => foodId = value; }
    public string FoodName { get => foodName; private set => foodName = value; }
    public float FoodPrice { get => foodPrice; private set => foodPrice = value; }
    public float Energy { get => energy; private set => energy = value; }
    public float Body { get => body; private set => body = value; }
    public float Condition { get => condition; private set => condition = value; }
    public float BodyTypeValue { get => bodyTypeValue; private set => bodyTypeValue = value; }
    public float Fatigue { get => fatigue; private set => fatigue = value; }
    public float Stress { get => stress; private set => stress = value; }
}
