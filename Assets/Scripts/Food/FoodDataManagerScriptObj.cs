using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FoodDataScriptObj", menuName = "ScriptableObjects/FoodData/FoodDataScriptObj", order = 1)]
public class FoodDataManagerScriptObj : ScriptableObject
{
    public Dictionary<Food.FoodType, Food> foodDictionary = new Dictionary<Food.FoodType, Food>();

    [SerializeField]
    List<Food> foodList = new List<Food>();

    public Food GetFood(Food.FoodType foodType)
    {
        Food food = null;

        if (!foodDictionary.TryGetValue(foodType, out food))
        {
            // the key isn't in the dictionary.
            // or whatever you want to do

            //Debug.Log("Food " + foodType.ToString() + " not found");
        }
        else
        {
            //Debug.Log("Food " + foodType.ToString() + " exist");

            //Debug.Log("food price = " + food.FoodPrice);
        }

        return food;
    }

    private void OnValidate()
    {
        if (Application.isEditor && Application.isPlaying == false)
        {
            //Debug.Log("OnValidate");

            CreateFoodDictionary();

            foreach (Food f in foodList)
            {
                GetFood(f.FoodTypeVal);
            }
        }
    }

    public void CreateFoodDictionary()
    {
        if (foodList.Count > 0)
        {
            foodDictionary.Clear();

            for (int i = 0; i < foodList.Count; i++)
            {
                foodDictionary.Add(foodList[i].FoodTypeVal, foodList[i]);
            }
        }
    }

    private void CreateData()
    {
        Debug.Log("Create Data");

        //    foodDictionary = new Dictionary<int, Food>
        //{
        //    { (int)Food.FoodType.Empty, new Food((int)Food.FoodType.Empty, "Empty", 0)},
        //    { (int)Food.FoodType.Vegetable, new Food((int)Food.FoodType.Vegetable, "Vegetable", 0) },
        //    { (int)Food.FoodType.Fish, new Food((int)Food.FoodType.Fish, "Fish", 0) },
        //    { (int)Food.FoodType.Food_Supplement, new Food((int)Food.FoodType.Food_Supplement, "Food Supplement", 0) }
        //        ,
        //    { (int)Food.FoodType.Dessert_Food, new Food((int)Food.FoodType.Dessert_Food, "Dessert Food", 0) }
        //};

    }
}
