﻿using System;

namespace Assets.Scripts.Interfaces
{
    public interface IRemoveablePoolObject
    {
        public Action OnDispose { get; set; }
    }
}