using UnityEngine;
using UnityEngine.Events;

public class UIMainMenuTimeChecker : MonoBehaviour
{
    public static UIMainMenuTimeChecker instance;

    public UnityEvent time1CheckEvent;

    public bool firstCheck = false;

    float startTime = 0f;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //time1CheckEvent.AddListener(CheckMonsterActions);
    }

    // Update is called once per frame
    void Update()
    {
        if (firstCheck == false)
        {
            firstCheck = true;

            time1CheckEvent.Invoke();

            Debug.Log("First time Check");

            startTime = Time.time;
        }

        if (Time.time - startTime >= 10f)
        {
            startTime = Time.time;

            time1CheckEvent.Invoke();

            Debug.Log("Time Check");
        }
    }
/*
    public void CheckMonsterActions()
    {
        foreach (PlayerInfo.PlayerMonsterInfo info in PlayerStats.Instance.playerInfo.playerMonsterInfos)
        {
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.BasicTraining && info.lastMonsterAction.actionDone == false)
            {
                UIBasicTrainingCaculator.CaculateTrainingAction(info);
            }
            else
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.SpecialTraining && info.lastMonsterAction.actionDone == false)
            {
                UISpecialTrainingCaculator.CaculateTrainingAction(info);
            }
            else
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.IntensiveTrainning && info.lastMonsterAction.actionDone == false)
            {
                UIIntensiveTrainingCaculator.CaculateTrainingAction(info);
            }
            else
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Exploration && info.lastMonsterAction.actionDone == false)
            {
                UIExplorationCaculator.CaculateTrainingAction(info);
            }
            else
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Rest && info.lastMonsterAction.actionDone == false)
            {
                UIRestCaculator.CaculateTrainingAction(info);
            }
            else
            if (info.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Hospital && info.lastMonsterAction.actionDone == false)
            {
                UIHospitalCaculator.CaculateTrainingAction(info);
            }
        }
    }*/
}
