﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class UIMonster3dDisplayRender : MonoBehaviour
    {
        [SerializeField] private List<MonsterDisplayHolder> monsterContainer = new();

        public void SetMonsterToDisplay(int displayIndex, GameObject monsterPrefab)
        {
            Helpers.DestroyChildren(monsterContainer[displayIndex].HolderContainer);
            GameObject monsterGameObject = Instantiate(monsterPrefab, monsterContainer[displayIndex].HolderContainer);
            Transform monsterTransform = monsterGameObject.transform;
            monsterContainer[displayIndex].MonsterFarmAnimationControl = monsterGameObject.AddComponent<MonsterFarmAnimationControl>();
            monsterTransform.localScale *= 0.5f;
            monsterTransform.localRotation = Quaternion.Euler(Vector3.zero);
        }

        public void SetMonsterState(int monsterIndex, bool isTired = false)
        {
            monsterContainer[monsterIndex].MonsterFarmAnimationControl.SetMonsterIsTired(isTired);
        }

        [Serializable]
        private class MonsterDisplayHolder
        {
            [SerializeField] private Transform holderContainer;
            [SerializeField] private MonsterFarmAnimationControl monsterFarmAnimationControl;

            public Transform HolderContainer => holderContainer;
            public MonsterFarmAnimationControl MonsterFarmAnimationControl { get => monsterFarmAnimationControl; set => monsterFarmAnimationControl = value; }
        }
    }

}