﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class UIScrollrectControlButton : MonoBehaviour
    {
        [SerializeField] private ScrollRect controlScrollrect;
        [Header("Horizontal by default, false = verticle")]
        [SerializeField] private bool isCheckHorizontal = true;
        [Header("Set scroll to top or bottom")]
        [SerializeField][Range(0, 1)] private float conditionCheck = 0;
        [SerializeField] private Button controlButton;
        [SerializeField][Range(-1, 1)] private float scrollAmount = 0.25f;
        [SerializeField] private float scrollTime = 0.2f;
        [SerializeField] private Ease scrollEase = Ease.OutQuad;
        private Vector2 scrollPosition;

        private void Awake()
        {
            controlScrollrect.onValueChanged.AddListener((Vector2 scrollValue) =>
            {
                CheckScrollCondition(scrollValue);
            });

            controlButton.onClick.AddListener(() =>
            {
                AutoScroll();
            });
        }

        private void AutoScroll()
        {
            Vector2 scrollTarget;
            if (isCheckHorizontal)
                scrollTarget = controlScrollrect.normalizedPosition + new Vector2(scrollAmount, 0);
            else
                scrollTarget = controlScrollrect.normalizedPosition + new Vector2(0, scrollAmount);
            controlScrollrect.DONormalizedPos(scrollTarget, scrollTime).SetEase(scrollEase);
        }

        private void CheckScrollCondition(Vector2 scrollValue)
        {
            scrollPosition = scrollValue;
            if (isCheckHorizontal)
            {
                if (conditionCheck >= scrollValue.x && conditionCheck < 0.5f)
                {
                    gameObject.SetActive(false);
                    return;
                }
                if(conditionCheck<=scrollValue.x && conditionCheck > 0.5f)
                {
                    gameObject.SetActive(false);
                    return;
                }
                gameObject.SetActive(true);
                return;
            }
            else
            {
                if (conditionCheck >= scrollValue.y && conditionCheck < 0.5f)
                {
                    gameObject.SetActive(false);
                    return;
                }
                if (conditionCheck <= scrollValue.y && conditionCheck > 0.5f)
                {
                    gameObject.SetActive(false);
                    return;
                }
                gameObject.SetActive(true);
            }
        }
    }
}