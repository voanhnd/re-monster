﻿using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    [Serializable]
    public class UIMonsterDetailInfoBattleInfo
    {
        [Header("Attribute res")]
        [SerializeField] private TextMeshProUGUI hitResValueText;
        [SerializeField] private TextMeshProUGUI cutResValueText;
        [SerializeField] private TextMeshProUGUI thrustResValueText;
        [SerializeField] private TextMeshProUGUI waterResValueText;
        [SerializeField] private TextMeshProUGUI flameResValueText;
        [SerializeField] private TextMeshProUGUI windResValueText;
        [SerializeField] private TextMeshProUGUI soilResValueText;

        [Header("Aliment res")]
        [SerializeField] private TextMeshProUGUI poisonResValueText;
        [SerializeField] private TextMeshProUGUI weaknessResValueText;
        [SerializeField] private TextMeshProUGUI blindessResValueText;
        [SerializeField] private TextMeshProUGUI slowdownResValueText;
        [SerializeField] private TextMeshProUGUI paralysResValueText;
        [SerializeField] private TextMeshProUGUI sleepResValueText;
        [SerializeField] private TextMeshProUGUI angerResValueText;
        [SerializeField] private TextMeshProUGUI enchantmentResValueText;
        [SerializeField] private TextMeshProUGUI chaosResValueText;
        [SerializeField] private TextMeshProUGUI fearResValueText;

        public void SetAttributeRes(float hitValue, float cutValue, float thrustValue, float waterValue, float flameValue, float windValue, float soilValue)
        {
            hitResValueText.text = hitValue.ToString("0");
            cutResValueText.text = cutValue.ToString("0");
            thrustResValueText.text = thrustValue.ToString("0");
            waterResValueText.text = waterValue.ToString("0");
            flameResValueText.text = flameValue.ToString("0");
            windResValueText.text = windValue.ToString("0");
            soilResValueText.text = soilValue.ToString("0");
        }

        public void SetAlimentRes(float poisonValue, float weaknessValue, float blindnessValue, float slowdownValue, float paralysValue,float sleepValue, float angerValue, float enchantmentValue, float chaosValue, float fearValue)
        {
            poisonResValueText.text = poisonValue.ToString("0");
            weaknessResValueText.text = weaknessValue.ToString("0");
            blindessResValueText.text = blindnessValue.ToString("0");
            slowdownResValueText.text = slowdownValue.ToString("0");
            paralysResValueText.text = paralysValue.ToString("0");
            sleepResValueText.text = sleepValue.ToString("0");
            angerResValueText.text = angerValue.ToString("0");
            enchantmentResValueText.text = enchantmentValue.ToString("0");
            chaosResValueText.text = chaosValue.ToString("0");
            fearResValueText.text = fearValue.ToString("0");
        }
    }
}