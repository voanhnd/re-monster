﻿using Assets.Scripts.Scriptables;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    [Serializable]
    public class UIMonsterDetailInfoEnviroment
    {
        [SerializeField] private TextMeshProUGUI compatibleMainTerrainText;
        [SerializeField] private TextMeshProUGUI compatibleSubTerrainText;
        [SerializeField] private Image compatibleMainTerrainGraphic;
        [SerializeField] private Image compatibleSubTerrainGraphic;

        [SerializeField] private TextMeshProUGUI incompatibleMainTerrainText;
        [SerializeField] private TextMeshProUGUI incompatibleSubTerrainText;
        [SerializeField] private Image incompatibleMainTerrainGraphic;
        [SerializeField] private Image incompatibleSubTerrainGraphic;

        [SerializeField] private UIMonsterEnviromentIcon[] enviromentIcons;

        public void SetTerrains(TrainingArea.TrainingArea.TerrainType compatibleTerrainMain, TrainingArea.TrainingArea.TerrainType compatibleTerrainSub, TrainingArea.TrainingArea.TerrainType incompatibleTerrainMain, TrainingArea.TrainingArea.TerrainType incompatibleTerrainSub)
        {
            compatibleMainTerrainText.text = $"{compatibleTerrainMain}";
            compatibleSubTerrainText.text = $"{compatibleTerrainSub}";
            SetGraphicEnviroment(compatibleMainTerrainGraphic, compatibleTerrainMain);
            SetGraphicEnviroment(compatibleSubTerrainGraphic, compatibleTerrainSub);

            incompatibleMainTerrainText.text = $"{incompatibleTerrainMain}";
            incompatibleSubTerrainText.text = $"{incompatibleTerrainSub}";
            SetGraphicEnviroment(incompatibleMainTerrainGraphic, incompatibleTerrainMain);
            SetGraphicEnviroment(incompatibleSubTerrainGraphic, incompatibleTerrainSub);
        }

        private void SetGraphicEnviroment(Image targetGraphic, TrainingArea.TrainingArea.TerrainType enviroment)
        {
            UIMonsterEnviromentIcon enviromentIcon = UIMonsterEnviromentIcon.GetEnviromentIcon(enviromentIcons, enviroment);
            if(enviromentIcon != null)
            {
                targetGraphic.sprite = enviromentIcon.EnviromentIcon;
                targetGraphic.color = enviromentIcon.EnviromentColor;
                return;
            }
            targetGraphic.sprite = null;
            targetGraphic.color = Color.clear;
        }
    }
}