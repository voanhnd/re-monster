﻿using Assets.Scripts.Monster;
using Assets.Scripts.UI.HomeMenu;
using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    public class UIMonsterDetailInfo : UIMenuStruct
    {
        [SerializeField] private RawImage monsterRawImage;
        [SerializeField] private TextMeshProUGUI monsterName;
        [SerializeField] private TextMeshProUGUI monsterSecondaryName;
        [SerializeField] private Image monsterRankImage;
        [SerializeField] private Image monsterEnviromentBackground;
        [SerializeField] private GameObject monsterEnviromentContainer;
        [SerializeField] private List<UIMonsterDetailInfoBasicParameters> basicParametersInfo = new();
        [SerializeField] private UIMonsterDetailInfoGrowth growthInfos;
        [SerializeField] private UIMonsterDetailInfoBattleInfo battleInfos;
        [SerializeField] private UIMonsterDetailInfoSkills skillInfos;
        [SerializeField] private UIMonsterSkillDetail skillDetailMenu;

        private void Awake()
        {
            skillInfos.AddSkillDetailUI(skillDetailMenu);
        }

        public void SetMonster(PlayerInfo.PlayerMonsterInfo monsterInfo, MonsterDataScriptObj monsterScriptable, FarmDataScriptObj farmScriptable, Texture monsterRefImage)
        {
            BasicParameters complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, monsterScriptable.Monster.basicParameters);

            monsterEnviromentContainer.SetActive(true);
            monsterEnviromentBackground.sprite = farmScriptable.Farm.FarmBackgroundSprite;

            for (int i = 0; i < basicParametersInfo.Count; i++)
            {
                switch (basicParametersInfo[i].Parameter)
                {
                    case ParameterType.HP:
                        float hp = complexBasicParameters.health;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(hp), monsterScriptable.Monster.HP_GA_Rank, hp, 999);
                        break;
                    case ParameterType.STR:
                        float str = complexBasicParameters.strenght;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(str), monsterScriptable.Monster.STR_GA_Rank, str, 999);
                        break;
                    case ParameterType.INT:
                        float intel = complexBasicParameters.intelligent;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(intel), monsterScriptable.Monster.INT_GA_Rank, intel, 999);
                        break;
                    case ParameterType.DEX:
                        float dex = complexBasicParameters.dexterity;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(dex), monsterScriptable.Monster.DEX_GA_Rank, dex, 999);
                        break;
                    case ParameterType.AGI:
                        float agi = complexBasicParameters.agility;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(agi), monsterScriptable.Monster.AGI_GA_Rank, agi, 999);
                        break;
                    case ParameterType.VIT:
                        float vit = complexBasicParameters.vitality;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(vit), monsterScriptable.Monster.VIT_GA_Rank, vit, 999);
                        break;
                }
            }

            growthInfos.SetBattleParameters(monsterScriptable.Monster.battleParameters.StaminaRecovery, monsterScriptable.Monster.battleParameters.Speed, monsterScriptable.Monster.battleParameters.Move);
            growthInfos.SetGrowthParameters(100, monsterInfo.growthParameters.bodyTypeValue, 100, monsterInfo.growthParameters.trainingPolicyValue, 100, monsterInfo.growthParameters.affection, monsterScriptable.Monster.growthParameters.lifeSpan, monsterScriptable.Monster.loveFoods[0], monsterScriptable.Monster.hateFoods[0]);
            growthInfos.SetTerrains(monsterScriptable.Monster.mainSuitTerrain, monsterScriptable.Monster.subSuitTerrain, monsterScriptable.Monster.mainNoSuitTerrain, monsterScriptable.Monster.subNoSuitTerrain);
            growthInfos.SetGrowthType(monsterScriptable.Monster.growthType);
            growthInfos.SetSpecies(monsterScriptable.Monster.monsterRace.mainRace, monsterScriptable.Monster.monsterRace.subRace);

            battleInfos.SetAttributeRes(monsterScriptable.Monster.Hit, monsterScriptable.Monster.Cut,
                                        monsterScriptable.Monster.Thrust, monsterScriptable.Monster.Water,
                                        monsterScriptable.Monster.Flame, monsterScriptable.Monster.Wind,
                                        monsterScriptable.Monster.Soil);
            battleInfos.SetAlimentRes(monsterScriptable.Monster.Poison, monsterScriptable.Monster.Weakness,
                                      monsterScriptable.Monster.Blindness, monsterScriptable.Monster.Slowdown,
                                      monsterScriptable.Monster.Paralysis, monsterScriptable.Monster.Sleep,
                                      monsterScriptable.Monster.Anger, monsterScriptable.Monster.Enchantment,
                                      monsterScriptable.Monster.Chaos, monsterScriptable.Monster.Fear);

            skillInfos.SetSkillInfo(monsterInfo.MonsterSkillInfos, monsterScriptable);

            monsterRawImage.texture = monsterRefImage;
            monsterName.text = monsterScriptable.Monster.monsterName;
            monsterRankImage.sprite = RankText.Instance.GetRank(monsterInfo.rankType);
            monsterSecondaryName.text = monsterScriptable.Monster.monsterName;
            OpenMenu();
        }

        public void SetMonster(PlayerInfo.PlayerMonsterInfo monsterInfo, MonsterDataScriptObj monsterScriptable, Texture monsterRefImage)
        {
            BasicParameters complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, monsterScriptable.Monster.basicParameters);

            monsterEnviromentContainer.SetActive(false);

            for (int i = 0; i < basicParametersInfo.Count; i++)
            {
                switch (basicParametersInfo[i].Parameter)
                {
                    case ParameterType.HP:
                        float hp = complexBasicParameters.health;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(hp), monsterScriptable.Monster.HP_GA_Rank, hp, 999);
                        break;
                    case ParameterType.STR:
                        float str = complexBasicParameters.strenght;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(str), monsterScriptable.Monster.STR_GA_Rank, str, 999);
                        break;
                    case ParameterType.INT:
                        float intel = complexBasicParameters.intelligent;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(intel), monsterScriptable.Monster.INT_GA_Rank, intel, 999);
                        break;
                    case ParameterType.DEX:
                        float dex = complexBasicParameters.dexterity;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(dex), monsterScriptable.Monster.DEX_GA_Rank, dex, 999);
                        break;
                    case ParameterType.AGI:
                        float agi = complexBasicParameters.agility;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(agi), monsterScriptable.Monster.AGI_GA_Rank, agi, 999);
                        break;
                    case ParameterType.VIT:
                        float vit = complexBasicParameters.vitality;
                        basicParametersInfo[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(vit), monsterScriptable.Monster.VIT_GA_Rank, vit, 999);
                        break;
                }
            }

            growthInfos.SetBattleParameters(monsterScriptable.Monster.battleParameters.StaminaRecovery, monsterScriptable.Monster.battleParameters.Speed, monsterScriptable.Monster.battleParameters.Move);
            growthInfos.SetGrowthParameters(100, monsterInfo.growthParameters.bodyTypeValue, 100, monsterInfo.growthParameters.trainingPolicyValue, 100, monsterInfo.growthParameters.affection, monsterScriptable.Monster.growthParameters.lifeSpan, monsterScriptable.Monster.loveFoods[0], monsterScriptable.Monster.hateFoods[0]);
            growthInfos.SetTerrains(monsterScriptable.Monster.mainSuitTerrain, monsterScriptable.Monster.subSuitTerrain, monsterScriptable.Monster.mainNoSuitTerrain, monsterScriptable.Monster.subNoSuitTerrain);

            skillInfos.SetSkillInfo(monsterInfo.MonsterSkillInfos, monsterScriptable);

            battleInfos.SetAttributeRes(monsterScriptable.Monster.Hit, monsterScriptable.Monster.Cut,
                                        monsterScriptable.Monster.Thrust, monsterScriptable.Monster.Water,
                                        monsterScriptable.Monster.Flame, monsterScriptable.Monster.Wind,
                                        monsterScriptable.Monster.Soil);
            battleInfos.SetAlimentRes(monsterScriptable.Monster.Poison, monsterScriptable.Monster.Weakness,
                                      monsterScriptable.Monster.Blindness, monsterScriptable.Monster.Slowdown,
                                      monsterScriptable.Monster.Paralysis, monsterScriptable.Monster.Sleep,
                                      monsterScriptable.Monster.Anger, monsterScriptable.Monster.Enchantment,
                                      monsterScriptable.Monster.Chaos, monsterScriptable.Monster.Fear);

            monsterRawImage.texture = monsterRefImage;
            OpenMenu();
        }
    }
}