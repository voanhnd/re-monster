﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.HomeMenu;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    [Serializable]
    public class UIMonsterDetailInfoSkills
    {
        UIMonsterSkillDetail skillDetailMenu;
        [Serializable]
        private class UIMonsterSkillSummaryBtn
        {
            [SerializeField] private TextMeshProUGUI skillNameText;
            [SerializeField] private TextMeshProUGUI skillLevelText;
            [SerializeField] private Button skillDetailBtn;

            public Button SkillDetailBtn => skillDetailBtn;
            public TextMeshProUGUI SkillLevelText => skillLevelText;
            public TextMeshProUGUI SkillNameText => skillNameText;
            public Monster.Monster.MonsterSkillInfo MonsterSkillInfo { get; set; }
            public MonsterDataScriptObj MonsterScriptable { get; set; }
        }

        [SerializeField] private List<UIMonsterSkillSummaryBtn> skillSummaryBtn;

        internal void SetSkillInfo(List<Monster.Monster.MonsterSkillInfo> monsterSkillInfos, MonsterDataScriptObj monsterScriptable)
        {
            for (int i = 0; i < monsterSkillInfos.Count; i++)
            {
                SkillDataScriptObj skillScriptable = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(monsterSkillInfos[i].skillID);
                skillSummaryBtn[i].SkillLevelText.text = $"Lvl {monsterSkillInfos[i].skillLevel}/{skillScriptable.Skill.skillDetails.Count}";
                skillSummaryBtn[i].SkillNameText.text = skillScriptable.Skill.skillName;
                skillSummaryBtn[i].MonsterSkillInfo = monsterSkillInfos[i];
                skillSummaryBtn[i].SkillDetailBtn.interactable = monsterSkillInfos[i].skillLevel > 0;
                skillSummaryBtn[i].MonsterScriptable = monsterScriptable;
            }
        }

        internal void AddSkillDetailUI(UIMonsterSkillDetail skillDetailMenu)
        {
            this.skillDetailMenu = skillDetailMenu;
            for (int i = 0; i < skillSummaryBtn.Count; i++)
            {
                int buttonIndex = i;
                skillSummaryBtn[buttonIndex].SkillDetailBtn.onClick.AddListener(() =>
                {
                    this.skillDetailMenu.SetSkillDisplay(PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(skillSummaryBtn[buttonIndex].MonsterSkillInfo.skillID),
                                                         skillSummaryBtn[buttonIndex].MonsterScriptable.unitData.m_Abilities[buttonIndex].unitAbility,
                                                         skillSummaryBtn[buttonIndex].MonsterSkillInfo.skillLevel);
                });
            }
        }
    }
}