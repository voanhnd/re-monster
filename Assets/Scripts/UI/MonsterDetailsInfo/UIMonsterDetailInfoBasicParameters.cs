﻿using Assets.Scripts.UI.Utilities;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    [Serializable]
    public class UIMonsterDetailInfoBasicParameters
    {
        [SerializeField] private TextMeshProUGUI parameterText;
        [SerializeField] private TextMeshProUGUI parameterValueText;
        [SerializeField] private Image currentRankImage;
        [SerializeField] private Image fitRankImage;
        [SerializeField] private Image parameterSliderFill;
        [SerializeField] private Color parameterColor;
        [SerializeField] private Slider parameterSlider;
        [SerializeField] private ParameterType parameter;

        public ParameterType Parameter => parameter;

        public void SetParameterValues(RankTypeEnums currentRank, RankTypeEnums fitRank, float parameterValue, float maxParameterValue)
        {
            parameterText.text = parameter.ToString();
            parameterValueText.text = Mathf.Min(parameterValue, 999).ToString("0");

            parameterSlider.maxValue = maxParameterValue;
            parameterSlider.value = parameterValue;

            fitRankImage.sprite = RankText.Instance.GetRank(fitRank);
            currentRankImage.sprite = RankText.Instance.GetRank(currentRank);

            parameterSliderFill.color = parameterColor;
            parameterText.color = parameterColor;
            //parameterValueText.color = parameterColor;
        }

        public void SetParameterValues(RankTypeEnums currentRank, float parameterValue, float maxParameterValue)
        {
            parameterText.text = parameter.ToString();
            parameterValueText.text = SpecialFloatToInt.Convert(Mathf.Min(parameterValue, 999)).ToString("0");

            parameterSlider.maxValue = maxParameterValue;
            parameterSlider.value = parameterValue;

            currentRankImage.sprite = RankText.Instance.GetRank(currentRank);

            parameterSliderFill.color = parameterColor;
            parameterText.color = parameterColor;
            //parameterValueText.color = parameterColor;
        }
    }
}