﻿using Assets.Scripts.Scriptables;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.MonsterDetailsInfo
{
    [Serializable]
    public class UIMonsterDetailInfoGrowth
    {
        #region Private variables
        [SerializeField] private TextMeshProUGUI friendshipValueText;
        [SerializeField] private TextMeshProUGUI physicalValueText;
        [SerializeField] private TextMeshProUGUI policyValueText;
        [SerializeField] private TextMeshProUGUI lifeSpawnValueText;
        [SerializeField] private TextMeshProUGUI growthTypeValueText;
        [SerializeField] private TextMeshProUGUI stRecoverValueText;
        [SerializeField] private TextMeshProUGUI spdValueText;
        [SerializeField] private TextMeshProUGUI moveValueText;
        [SerializeField] private TextMeshProUGUI likeMealText;
        [SerializeField] private TextMeshProUGUI dislikeMealText;

        [Space(5)]
        [SerializeField] private Slider physicalSlider;
        [SerializeField] private Slider policySlider;
        [SerializeField] private Slider friendshipSlider;

        [Space(5)]
        [SerializeField] private TextMeshProUGUI compatibleMainTerrainText;
        [SerializeField] private TextMeshProUGUI compatibleSubTerrainText;
        [SerializeField] private Image compatibleMainTerrainGraphic;
        [SerializeField] private Image compatibleSubTerrainGraphic;
        [SerializeField] private TextMeshProUGUI incompatibleMainTerrainText;
        [SerializeField] private TextMeshProUGUI incompatibleSubTerrainText;
        [SerializeField] private Image incompatibleMainTerrainGraphic;
        [SerializeField] private Image incompatibleSubTerrainGraphic;
        [SerializeField] private UIMonsterEnviromentIcon[] enviromentIconArray;

        [Space(5)]
        [SerializeField] private TextMeshProUGUI mainSpecText;
        [SerializeField] private TextMeshProUGUI subSpecText;
        #endregion

        #region Data set
        public void SetGrowthParameters(float physicalMaxValue, float physicalValue, float policyMaxValue, float policyValue, float friendshipMaxValue, float friendshipValue, float lifespawnValue, Food.FoodType likeMeal, Food.FoodType dislikeMeal)
        {
            physicalSlider.maxValue = physicalMaxValue;
            physicalSlider.value = physicalValue;
            physicalValueText.text = physicalValue.ToString("0");

            policySlider.maxValue = policyMaxValue;
            policySlider.value = policyValue;
            policyValueText.text = policyValue.ToString("0");

            friendshipSlider.maxValue = friendshipMaxValue;
            friendshipSlider.value = friendshipValue;

            friendshipValueText.text = friendshipValue.ToString("0");
            lifeSpawnValueText.text = lifespawnValue.ToString("0");

            likeMealText.text = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(likeMeal).FoodName;
            dislikeMealText.text = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(dislikeMeal).FoodName;
        }

        public void SetBattleParameters(float stRecoverAmount, float spdValue, float moveValue)
        {
            stRecoverValueText.text = $"{stRecoverAmount:0} ST/T";
            spdValueText.text = spdValue.ToString("0");
            moveValueText.text = moveValue.ToString("0");
        }

        public void SetTerrains(TrainingArea.TrainingArea.TerrainType compatibleTerrainMain, TrainingArea.TrainingArea.TerrainType compatibleTerrainSub, TrainingArea.TrainingArea.TerrainType incompatibleTerrainMain, TrainingArea.TrainingArea.TerrainType incompatibleTerrainSub)
        {
            compatibleMainTerrainText.text = $"{compatibleTerrainMain}";
            compatibleSubTerrainText.text = $"{compatibleTerrainSub}";
            UIMonsterEnviromentIcon uiMonsterMainEnviromentScriptableIcon = UIMonsterEnviromentIcon.GetEnviromentIcon(enviromentIconArray, compatibleTerrainMain);
            SetEnviroment(uiMonsterMainEnviromentScriptableIcon, compatibleMainTerrainGraphic);

            UIMonsterEnviromentIcon uiMonsterSubEnviromentIconScriptable = UIMonsterEnviromentIcon.GetEnviromentIcon(enviromentIconArray, compatibleTerrainSub);
            SetEnviroment(uiMonsterSubEnviromentIconScriptable, compatibleSubTerrainGraphic);

            incompatibleMainTerrainText.text = $"{incompatibleTerrainMain}";
            incompatibleSubTerrainText.text = $"{incompatibleTerrainSub}";
            uiMonsterMainEnviromentScriptableIcon = UIMonsterEnviromentIcon.GetEnviromentIcon(enviromentIconArray, incompatibleTerrainMain);
            SetEnviroment(uiMonsterMainEnviromentScriptableIcon, incompatibleMainTerrainGraphic);

            uiMonsterSubEnviromentIconScriptable = UIMonsterEnviromentIcon.GetEnviromentIcon(enviromentIconArray, incompatibleTerrainSub);
            SetEnviroment(uiMonsterSubEnviromentIconScriptable, incompatibleSubTerrainGraphic);
        }

        private void SetEnviroment(UIMonsterEnviromentIcon uIMonsterMainEnviromentScriptableIcon,Image targetGraphic)
        {
            if (uIMonsterMainEnviromentScriptableIcon != null)
            {
                targetGraphic.sprite = uIMonsterMainEnviromentScriptableIcon.EnviromentIcon;
                targetGraphic.color = uIMonsterMainEnviromentScriptableIcon.EnviromentColor;
            }
            else
            {
                targetGraphic.sprite = null;
                targetGraphic.color = Color.clear;
            }
        }

        public void SetGrowthType(Monster.Monster.GrowthType growthType)
        {
            growthTypeValueText.text = growthType.ToString();
        }

        public void SetSpecies(Monster.Monster.MonsterType mainRace, Monster.Monster.MonsterType subRace)
        {
            mainSpecText.text = RaceToStringConvert.Instance.ConvertRace(mainRace);
            subSpecText.text = RaceToStringConvert.Instance.ConvertRace(subRace);
        }
        #endregion
    }
}