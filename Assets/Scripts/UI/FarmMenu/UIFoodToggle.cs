﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIFoodToggle : MonoBehaviour
    {
        [SerializeField] private Toggle uiToggle;
        [SerializeField] private TextMeshProUGUI mealText;
        [SerializeField] private TextMeshProUGUI mealPrice;
        [SerializeField] private Image foodTypeGraphic;
        [SerializeField] private Sprite hateIcon;
        [SerializeField] private Sprite loveIcon;
        [SerializeField] private Color loveColor;
        [SerializeField] private Color hateColor;
        private Food.FoodType typeFood;

        public Toggle UiToggle => uiToggle;

        public Food.FoodType TypeFood => typeFood;

        public void SetupToggle(string mealName, string mealPrice, Food.FoodType foodValue)
        {
            mealText.text = mealName;
            this.mealPrice.text = mealPrice;
            this.typeFood = foodValue;
        }

        internal void SetAsHate()
        {
            foodTypeGraphic.sprite = hateIcon;
            foodTypeGraphic.color = hateColor;
        }

        internal void SetAsLove()
        {
            foodTypeGraphic.sprite = loveIcon;
            foodTypeGraphic.color = loveColor;
        }

        internal void SetAsNeutral() => foodTypeGraphic.color = Color.clear;

        internal void HideToggle() => gameObject.SetActive(false);

        internal void ShowToggle() => gameObject.SetActive(true);
    }
}