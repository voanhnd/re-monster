﻿using Assets.Scripts.Monster;
using Thirdweb.Contracts.TokenERC1155.ContractDefinition;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    [System.Serializable]
    public class UIMonsterFarmDetailsTrainingStatBase
    {
        [SerializeField] private Color increaseColor = Color.green;
        [SerializeField] private Color decreaseColor = Color.red;
        [SerializeField] private Color idleColor = Color.cyan;
        [Header("Affection stats")]
        [SerializeField] private Slider fatigueSlider;
        [SerializeField] private TextMeshProUGUI fatigueText;
        [SerializeField] private Slider friendshipSlider;
        [SerializeField] private TextMeshProUGUI friendshipText;
        [SerializeField] private Slider lifespanSlider;
        [SerializeField] private TextMeshProUGUI lifespanText;
        [SerializeField] private Slider physicalSlider;
        [SerializeField] private TextMeshProUGUI physicalText;
        [SerializeField] private Slider policySlider;
        [SerializeField] private TextMeshProUGUI policyText;
        [SerializeField] private Slider stressSlider;
        [SerializeField] private TextMeshProUGUI stressText;
        [SerializeField] private Slider energySlider;
        [SerializeField] private TextMeshProUGUI energyText;
        [SerializeField] private Slider bodySlider;
        [SerializeField] private TextMeshProUGUI bodyText;
        [SerializeField] private Slider conditionSlider;
        [SerializeField] private TextMeshProUGUI conditionText;

        public void LoadAffectionStats(float maxFatigue, float fatigueValue, float maxStress, float stressValue, float maxPhysic, float physicValue, float maxFriendship, float friendshipValue, float maxPolicy, float policyValue, float maxLifespawn, float lifespawnValue)
        {
            SetValue(fatigueSlider, fatigueText, maxFatigue, fatigueValue);
            SetValue(stressSlider, stressText, maxStress, stressValue);
            SetValue(physicalSlider, physicalText, maxPhysic, physicValue);
            SetValue(friendshipSlider, friendshipText, maxFriendship, friendshipValue);
            SetValue(policySlider, policyText, maxPolicy, policyValue);
            SetValue(lifespanSlider, lifespanText, maxLifespawn, lifespawnValue);
        }

        public void UpdateAffectionStatsNoAnim(GrowthParameters alteredGrowthParameters, GrowthParameters newGrowthParameters)
        {
            UpdateValue(friendshipSlider, friendshipText, newGrowthParameters.affection, alteredGrowthParameters.affection);
            UpdateValue(physicalSlider, physicalText, newGrowthParameters.bodyTypeValue, alteredGrowthParameters.bodyTypeValue);
            UpdateValue(policySlider, policyText, newGrowthParameters.trainingPolicyValue, alteredGrowthParameters.trainingPolicyValue);
            UpdateValue(lifespanSlider, lifespanText, newGrowthParameters.lifeSpan, alteredGrowthParameters.lifeSpan);

            UpdateReverseValue(fatigueSlider, fatigueText, newGrowthParameters.fatigue, alteredGrowthParameters.fatigue);
            UpdateReverseValue(stressSlider, stressText, newGrowthParameters.stress, alteredGrowthParameters.stress);
        }

        public void LoadMealConditionStats(float maxEnergy, float energyValue, float maxBody, float bodyValue, float maxCondition, float conditionValue)
        {
            SetValue(energySlider, energyText, maxEnergy, energyValue);
            SetValue(bodySlider, bodyText, maxBody, bodyValue);
            SetValue(conditionSlider, conditionText, maxCondition, conditionValue);
        }

        public void UpdateMealConditionStats(GrowthParameters alteredGrowthParameters, GrowthParameters newGrowthParameters)
        {
            UpdateValue(energySlider, energyText, newGrowthParameters.energy, alteredGrowthParameters.energy);
            UpdateValue(bodySlider, bodyText, newGrowthParameters.body, alteredGrowthParameters.body);
            UpdateValue(conditionSlider, conditionText, newGrowthParameters.condition, alteredGrowthParameters.condition);
        }

        private void SetValue(Slider slider, TextMeshProUGUI textmesh, float maxValue, float value)
        {
            slider.maxValue = maxValue;
            slider.value = value;
            slider.fillRect.GetComponent<Image>().color = idleColor;
            textmesh.text = value.ToString("0");
        }

        private void UpdateValue(Slider slider, TextMeshProUGUI textmesh, float newValue, float changedValue)
        {
            if (changedValue < 0)
            {
                slider.fillRect.GetComponent<Image>().color = decreaseColor;
                textmesh.text = $"{newValue:0} (<color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>{changedValue:0}</color>)";
            }

            if (changedValue > 0)
            {
                slider.fillRect.GetComponent<Image>().color = increaseColor;
                textmesh.text = $"{newValue:0} (<color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>+{changedValue:0}</color>)";
            }
        }

        private void UpdateReverseValue(Slider slider, TextMeshProUGUI textmesh, float newValue, float changedValue)
        {
            if (changedValue < 0)
            {
                slider.fillRect.GetComponent<Image>().color = increaseColor;
                textmesh.text = $"{newValue:0} (<color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>{changedValue:0}</color>)";
            }

            if (changedValue > 0)
            {
                slider.fillRect.GetComponent<Image>().color = decreaseColor;
                textmesh.text = $"{newValue:0} (<color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>+{changedValue:0}</color>)";
            }
        }
    }
}