﻿using Assets.Scripts.Monster;
using Assets.Scripts.UI.Monsterbag;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIMonsterFarmBagMenu : MonoBehaviour
    {
        [SerializeField] private RectTransform rectContainer;
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private UIMonsterBagMenu farmBag;
        [Header("Animations")]
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Ease openEase;
        [SerializeField] private Ease closeEase;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Button removeBtn;
        [Header("Events")]
        [SerializeField] private UnityEvent onMenuOpen;
        [SerializeField] private UnityEvent onMenuClose;
        [SerializeField] private UnityEvent onMonsterSet;
        [SerializeField] private UnityEvent onMonsterChange;
        [SerializeField] private UnityEvent onMonsterRemove;
        private string monsterId;

        public UnityEvent OnMenuOpen { get => onMenuOpen; set => onMenuOpen = value; }
        public UnityEvent OnMenuClose { get => onMenuClose; set => onMenuClose = value; }
        public UnityEvent OnMonsterSet { get => onMonsterSet; set => onMonsterSet = value; }
        public UnityEvent OnMonsterRemove { get => onMonsterRemove; set => onMonsterRemove = value; }
        public UnityEvent OnMonsterChange { get => onMonsterChange; set => onMonsterChange = value; }
        public string MonsterId => monsterId;


        private void Start()
        {
            closeBtn.onClick.AddListener(() => CloseMenu());
            rectContainer.sizeDelta = closeSize;
            gameObject.SetActive(false);
            removeBtn.onClick.AddListener(() =>
            {
                onMonsterRemove.Invoke();
                CloseMenu();
            });
            removeBtn.gameObject.SetActive(false);
        }

        public void OpenMenu()
        {
            gameObject.SetActive(true);
            onMenuOpen.Invoke();
            LoadMonsters();
            rectContainer.DOSizeDelta(openSize, openTime).SetEase(openEase).OnComplete(() =>
            {
                closeBtn.interactable = true;
                onMenuOpen.RemoveAllListeners();
            });
        }

        public void CloseMenu()
        {
            closeBtn.interactable = false;
            onMenuClose.Invoke();
            rectContainer.DOSizeDelta(closeSize, closeTime).SetEase(closeEase).OnComplete(() =>
            {
                gameObject.SetActive(false);
                onMenuClose.RemoveAllListeners();
                onMonsterSet.RemoveAllListeners();
                monsterId = string.Empty;
            });
            removeBtn.gameObject.SetActive(false);
        }

        private void LoadMonsters()
        {
            farmBag.Clearbag();
            List<PlayerInfo.PlayerMonsterInfo> monsterDataList = PlayerStats.Instance.playerInfo.PlayerMonsterInfos;
            for (int i = 0; i < monsterDataList.Count; i++)
            {
                int listIndex = i;
                if (string.IsNullOrEmpty(monsterDataList[listIndex].usingFarmID))
                {
                    MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterDataList[listIndex].monsterID);
                    UIMonsterListButton monsterButton = farmBag.AddMonsterToBag(monsterDataList[listIndex].rankType, monsterScriptable.Monster.monsterAvatar);
                    monsterButton.FunctionButton.onClick.AddListener(() =>
                    {
                        SetTargetMonster(monsterDataList[listIndex].monsterID);
                        SetMonster();
                    });
                }
            }
        }

        private void SetTargetMonster(string monsterId)
        {
            this.monsterId = monsterId;
        }

        private void SetMonster()
        {
            onMonsterChange.Invoke();
            onMonsterSet.Invoke();
            CloseMenu();
        }

        internal void SetRemoveMode()
        {
            removeBtn.gameObject.SetActive(true);
        }
    }
}