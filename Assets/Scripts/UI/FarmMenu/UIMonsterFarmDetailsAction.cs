﻿using Assets.Scripts.Monster;
using Assets.Scripts.TrainingArea;
using Assets.Scripts.UI.TrainningMenu;
using Assets.Scripts.UI.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    [System.Serializable]
    public class UIMonsterFarmDetailsAction
    {
        #region Private values
        [Header("Action Toggle")]
        [SerializeField] private Toggle basicToggle;
        [SerializeField] private Toggle specialToggle;
        [SerializeField] private Toggle intensiveToggle;
        [SerializeField] private Toggle exploreToggle;
        [SerializeField] private Toggle restToggle;
        [SerializeField] private Toggle hospitalToggle;
        [SerializeField] private TextMeshProUGUI hospitalToggleText;
        [SerializeField] private Button intensiveTrainningButton;
        [SerializeField] private Button exploreMapButton;
        [SerializeField] private GameObject intensiveTrainingContainer;
        [SerializeField] private GameObject exploreToggleContainer;
        [SerializeField] private UIToggleableRect toggleableRectContainer;
        [SerializeField] private TextMeshProUGUI trainingAreaText;
        [SerializeField] private TextMeshProUGUI exploreAreaText;
        [SerializeField] private UIBasicTrainningToggle basicTrainingToggle;

        private Toggle currentToggleAction;
        private UIMonsterFarmDetails farmDetail;
        #endregion

        #region Public values
        public Toggle CurrentToggleAction => currentToggleAction;
        public Toggle BasicToggle => basicToggle;
        public Toggle SpecialToggle => specialToggle;
        public Toggle IntensiveToggle => intensiveToggle;
        public Toggle ExploreToggle => exploreToggle;
        public Toggle RestToggle => restToggle;
        public Toggle HospitalToggle => hospitalToggle;
        public Button IntensiveTrainningButton => intensiveTrainningButton;
        public Button ExploreMapButton => exploreMapButton;
        public UIBasicTrainningToggle BasicTrainingToggle => basicTrainingToggle;
        #endregion

        internal void RemoveToggle() => currentToggleAction = null;

        internal void SetTargetFarm(UIMonsterFarmDetails farmDetail)
        {
            exploreToggleContainer.SetActive(false);

            this.farmDetail = farmDetail;
        }

        internal void SetActionToggles()
        {
            basicToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, basicToggle, MonsterAction.MonsterActionType.BasicTraining);
            });

            specialToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, specialToggle, MonsterAction.MonsterActionType.SpecialTraining);
            });

            hospitalToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, hospitalToggle, MonsterAction.MonsterActionType.Hospital);
            });

            restToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, restToggle, MonsterAction.MonsterActionType.Rest);
            });

            intensiveToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, intensiveToggle, MonsterAction.MonsterActionType.IntensiveTrainning);
            });

            exploreToggle.onValueChanged.AddListener((bool value) =>
            {
                CheckToggle(value, exploreToggle, MonsterAction.MonsterActionType.Exploration);
            });

            toggleableRectContainer.UiToggle.onValueChanged.AddListener((bool value) =>
            {
                if (value)
                    ShowToggles();
                else
                    HideToggles();
            });
        }

        private void CheckToggle(bool value, Toggle checkingToggle, MonsterAction.MonsterActionType actionSet)
        {
            if (value)
            {
                farmDetail.SetAction(actionSet);
                currentToggleAction = checkingToggle;
                //HideToggles();
            }
            else
            {
                if (currentToggleAction == checkingToggle)
                {
                    currentToggleAction = null;
                    ShowToggles();
                }
            }
            farmDetail.CheckActionBtn();
        }

        internal void SetExploreAction(TrainingArea.TrainingArea.TrainingAreaType areaType, float exploreCost)
        {
            if (areaType == TrainingArea.TrainingArea.TrainingAreaType.Empty)
            {
                if (exploreToggle.isOn)
                    basicToggle.isOn = true;
                return;
            }
            farmDetail.SetMapArea(areaType, exploreCost);
            farmDetail.SetAction(MonsterAction.MonsterActionType.Exploration);
            TrainingAreaDataScriptObj trainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(areaType);
            if (trainingAreaDataScriptObj != null)
                exploreAreaText.text = trainingAreaDataScriptObj.TrainingArea.areaName;
        }

        internal void SetIntensiveTrainning(TrainingArea.TrainingArea.TrainingAreaType areaType, float trainingCost)
        {
            if (areaType == TrainingArea.TrainingArea.TrainingAreaType.Empty)
            {
                if (intensiveToggle.isOn)
                    basicToggle.isOn = true;
                return;
            }

            farmDetail.SetMapArea(areaType, trainingCost);
            farmDetail.SetAction(MonsterAction.MonsterActionType.IntensiveTrainning);
            TrainingAreaDataScriptObj trainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(areaType);
            if (trainingAreaDataScriptObj != null)
                trainingAreaText.text = trainingAreaDataScriptObj.TrainingArea.areaName;
        }

        internal void SetMapActionButtons(UIIntensiveTrainningMenu uiIntensiveTrainningMenu, UIExploreMenu uiExploreMenu)
        {
            intensiveTrainningButton.onClick.AddListener(() =>
            {
                uiIntensiveTrainningMenu.OpenMenu();
                uiIntensiveTrainningMenu.SetTargetMonster(farmDetail);
            });

            exploreMapButton.onClick.AddListener(() =>
            {
                uiExploreMenu.OpenMenu();
                uiExploreMenu.SetTargetMonster(farmDetail);
            });
        }

        #region Toggles lock control
        internal void LockToggles()
        {
            basicToggle.interactable = false;
            specialToggle.interactable = false;
            hospitalToggle.interactable = false;
            intensiveToggle.interactable = false;
            exploreToggle.interactable = false;
            restToggle.interactable = false;
        }

        /// <summary>
        /// Should only call when monster is serious injured or sick
        /// </summary>
        /// <param name="value"></param>
        internal void LockToggles(bool value)
        {
            if (!value) return;
            basicToggle.interactable = false;
            specialToggle.interactable = false;
            intensiveToggle.interactable = false;
            exploreToggle.interactable = false;
            restToggle.interactable = false;
            hospitalToggle.interactable = true;

            intensiveTrainningButton.interactable = false;
            exploreMapButton.interactable = false;
        }

        /// <summary>
        /// Call to set time if monster if not sick or injured
        /// </summary>
        /// <param name="value"></param>
        internal void HospitalDateSet(bool value)
        {
            hospitalToggle.interactable = !value;

            if (farmDetail.MonsterInfoData.growthParameters.injuryType == Monster.Monster.InjuryType.Serious_Injury || farmDetail.MonsterInfoData.growthParameters.diseasesType == Monster.Monster.DiseasesType.Serious_Diseases)
                hospitalToggleText.text = "3 weeks";
            else
                hospitalToggleText.text = "1 week";
        }

        internal void UnlockToggles()
        {
            basicToggle.interactable = true;
            specialToggle.interactable = true;
            hospitalToggle.interactable = true;
            restToggle.interactable = true;
            exploreToggle.interactable = true;
            intensiveToggle.interactable = true;

            exploreMapButton.interactable = false;
            intensiveTrainningButton.interactable = true;
        }
        #endregion

        #region Toggle values set
        /// <summary>
        /// To set a toggle on by last action.
        /// </summary>
        /// <param name="lastAction"></param>
        internal void SetToggleValue(MonsterAction lastAction)
        {
            switch (lastAction.monsterActionType)
            {
                case MonsterAction.MonsterActionType.BasicTraining:
                    basicToggle.isOn = true;
                    basicToggle.onValueChanged.Invoke(basicToggle.isOn);
                    break;
                case MonsterAction.MonsterActionType.SpecialTraining:
                    specialToggle.isOn = true;
                    specialToggle.onValueChanged.Invoke(specialToggle.isOn);
                    break;
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    intensiveToggle.isOn = true;
                    intensiveToggle.onValueChanged.Invoke(intensiveToggle.isOn);
                    break;
                case MonsterAction.MonsterActionType.Exploration:
                    exploreToggle.isOn = true;
                    exploreToggle.onValueChanged.Invoke(exploreToggle.isOn);
                    break;
                case MonsterAction.MonsterActionType.Rest:
                    restToggle.isOn = true;
                    restToggle.onValueChanged.Invoke(restToggle.isOn);
                    break;
            }
        }

        internal void SetTogglesValue(bool value)
        {
            basicToggle.isOn = value;
            specialToggle.isOn = value;
            intensiveToggle.isOn = value;
            exploreToggle.isOn = value;
            restToggle.isOn = value;
            hospitalToggle.isOn = value;
        }

        internal void SetToggleValue(MonsterAction.MonsterActionType monsterActionType, ref MonsterAction monsterSelectAction, TrainingArea.TrainingArea.TrainingAreaType mapArea, UIBasicTrainningToggle basicTrainningToggleUI)
        {
            switch (monsterActionType)
            {
                case MonsterAction.MonsterActionType.BasicTraining:
                    if (basicTrainningToggleUI.CurrentToggle != null)
                    {
                        monsterSelectAction.basicTrainingMainParameter = basicTrainningToggleUI.CurrentBasicParameter;
                        basicToggle.isOn = true;
                        farmDetail.CheckActionBtn();
                    }
                    break;
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    monsterSelectAction.trainingAreaType = mapArea;
                    intensiveToggle.isOn = true;
                    break;
                case MonsterAction.MonsterActionType.Exploration:
                    monsterSelectAction.trainingAreaType = mapArea;
                    exploreToggle.isOn = true;
                    break;
            }
        }
        #endregion

        internal bool CheckBasicValidToggle() => currentToggleAction == basicToggle;

        internal void HideToggles()
        {
            HideToggle(basicToggle);
            HideToggle(specialToggle);
            basicTrainingToggle.gameObject.SetActive(basicToggle == currentToggleAction);
            intensiveTrainingContainer.SetActive(intensiveToggle == currentToggleAction);
            exploreToggleContainer.SetActive(false);
            HideToggle(restToggle);
            HideToggle(hospitalToggle);
        }

        private void HideToggle(Toggle targetToggle) => targetToggle.gameObject.SetActive(targetToggle == currentToggleAction);

        internal void ShowToggles()
        {
            basicToggle.gameObject.SetActive(true);
            specialToggle.gameObject.SetActive(true);
            basicTrainingToggle.gameObject.SetActive(true);
            intensiveTrainingContainer.SetActive(true);
            exploreToggleContainer.SetActive(false);
            restToggle.gameObject.SetActive(true);
            hospitalToggle.gameObject.SetActive(true);
        }
    }
}