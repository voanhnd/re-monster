﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIMealActionControl : MonoBehaviour
    {
        [SerializeField] private UISelectMeal mealUI;
        [SerializeField] private Toggle mealMenuToggle;
        [SerializeField] private Toggle actionMenuToggle;
        [SerializeField] private UIMonsterFarmDetails farmDetail;

        public Toggle MealMenuToggle => mealMenuToggle;
        public Toggle ActionMenuToggle => actionMenuToggle;

        private void Awake()
        {
            mealUI.OnMealSelected.AddListener(() =>
            {
                CheckActionMenu();
            });
            farmDetail.OnMenuOpen.AddListener(() =>
            {
                if (mealUI.CurrentFood == Food.FoodType.Empty)
                {
                    mealMenuToggle.isOn = true;
                    mealMenuToggle.interactable = false;
                    actionMenuToggle.isOn = false;
                    actionMenuToggle.interactable = false;
                }
            });
        }

        private void CheckActionMenu()
        {
            if (mealUI.CurrentFood == Food.FoodType.Empty)
            {
                mealMenuToggle.isOn = true;
                mealMenuToggle.interactable = false;
                actionMenuToggle.isOn = false;
                actionMenuToggle.interactable = false;
            }
            else
            {
                mealMenuToggle.isOn = false;
                mealMenuToggle.interactable = true;
                actionMenuToggle.isOn = true;
                actionMenuToggle.interactable = true;
            }
        }
    }
}