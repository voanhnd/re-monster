﻿using Assets.Scripts.Monster;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    [System.Serializable]
    public class UIMonsterFarmDetailsBattleStatsBase
    {
        [SerializeField] private Color increaseColor = Color.green;
        [SerializeField] private Color decreaseColor = Color.red;
        [SerializeField] private Color idleColor = Color.cyan;
        [SerializeField] private float animationTimer = 0.2f;
        [Header("Battle stats")]
        [SerializeField] private Slider hpSlider;
        [SerializeField] private Color hpColor = Color.white;
        [SerializeField] private Slider strSlider;
        [SerializeField] private Color strColor = Color.white;
        [SerializeField] private Slider intSlider;
        [SerializeField] private Color intColor = Color.white;
        [SerializeField] private Slider dexSlider;
        [SerializeField] private Color dexColor = Color.white;
        [SerializeField] private Slider agiSlider;
        [SerializeField] private Color agiColor = Color.white;
        [SerializeField] private Slider vitSlider;
        [SerializeField] private Color vitColor;
        [Space(5)]
        [SerializeField] private TextMeshProUGUI hpText;
        [SerializeField] private TextMeshProUGUI strText;
        [SerializeField] private TextMeshProUGUI intText;
        [SerializeField] private TextMeshProUGUI dexText;
        [SerializeField] private TextMeshProUGUI agiText;
        [SerializeField] private TextMeshProUGUI vitText;

        public void SetBattleStats(float maxHp, float hpValue, float maxStr, float strValue, float maxInt, float intValue, float maxDex, float dexValue, float maxAgi, float agiValue, float maxVit, float vitValue)
        {
            SetStatParameterValue(maxHp, hpValue, hpColor, hpSlider, hpText);
            SetStatParameterValue(maxStr, strValue, strColor, strSlider, strText);
            SetStatParameterValue(maxInt, intValue, intColor, intSlider, intText);
            SetStatParameterValue(maxDex, dexValue, dexColor, dexSlider, dexText);
            SetStatParameterValue(maxAgi, agiValue, agiColor, agiSlider, agiText);
            SetStatParameterValue(maxVit, vitValue, vitColor, vitSlider, vitText);
        }

        private void SetStatParameterValue(float maxValue, float value, Color idleColor, Slider parameterSlider, TextMeshProUGUI parameterText)
        {
            parameterSlider.DOKill();
            parameterText.DOKill();
            parameterSlider.maxValue = maxValue;
            parameterSlider.value = value;
            parameterSlider.fillRect.GetComponent<Image>().color = idleColor;
            parameterText.text = Mathf.CeilToInt(Mathf.Min(value, maxValue)).ToString();
        }

        /// <summary>
        /// <span>Monster last action stats</span>
        /// <span>Monster new data</span>
        /// <span>Farm enhance</span>
        /// </summary>
        /// <param name="alteredBasicParameter"></param>
        /// <param name="newBasicParameters"></param>
        /// <param name="farmEnhance"></param>
        public void UpdateActionStatAnim(BasicParameters alteredBasicParameter, BasicParameters newBasicParameters)
        {
            BasicParameters oldParameter = CalculateOldParameter(alteredBasicParameter, newBasicParameters);

            UpdateParameterAnim(oldParameter.health, newBasicParameters.health, newBasicParameters.health, alteredBasicParameter.health, 0, hpSlider, hpText, hpColor);
            UpdateParameterAnim(oldParameter.strenght, newBasicParameters.strenght, newBasicParameters.strenght, alteredBasicParameter.strenght, 0, strSlider, strText, strColor);
            UpdateParameterAnim(oldParameter.intelligent, newBasicParameters.intelligent, newBasicParameters.intelligent, alteredBasicParameter.intelligent, 0, intSlider, intText, intColor);
            UpdateParameterAnim(oldParameter.dexterity, newBasicParameters.dexterity, newBasicParameters.dexterity, alteredBasicParameter.dexterity, 0, dexSlider, dexText, dexColor);
            UpdateParameterAnim(oldParameter.agility, newBasicParameters.agility, newBasicParameters.agility, alteredBasicParameter.agility, 0, agiSlider, agiText, agiColor);
            UpdateParameterAnim(oldParameter.vitality, newBasicParameters.vitality, newBasicParameters.vitality, alteredBasicParameter.vitality, 0, vitSlider, vitText, vitColor);
        }

        public void UpdateActionStatAnim(BasicParameters alteredBasicParameter, BasicParameters newBasicParameters, float delayTime)
        {
            BasicParameters oldParameter = CalculateOldParameter(alteredBasicParameter, newBasicParameters);

            UpdateParameterAnim(oldParameter.health, newBasicParameters.health, newBasicParameters.health, alteredBasicParameter.health, delayTime, hpSlider, hpText, hpColor);

            UpdateParameterAnim(oldParameter.strenght, newBasicParameters.strenght, newBasicParameters.strenght, alteredBasicParameter.strenght, delayTime, strSlider, strText, strColor);

            UpdateParameterAnim(oldParameter.intelligent, newBasicParameters.intelligent, newBasicParameters.intelligent, alteredBasicParameter.intelligent, delayTime, intSlider, intText, intColor);

            UpdateParameterAnim(oldParameter.dexterity, newBasicParameters.dexterity, newBasicParameters.dexterity, alteredBasicParameter.dexterity, delayTime, dexSlider, dexText, dexColor);

            UpdateParameterAnim(oldParameter.agility, newBasicParameters.agility, newBasicParameters.agility, alteredBasicParameter.agility, delayTime, agiSlider, agiText, agiColor);

            UpdateParameterAnim(oldParameter.vitality, newBasicParameters.vitality, newBasicParameters.vitality, alteredBasicParameter.vitality, delayTime, vitSlider, vitText, vitColor);
        }

        private static BasicParameters CalculateOldParameter(BasicParameters alteredBasicParameter, BasicParameters newBasicParameters)
        {
            return new()
            {
                health = newBasicParameters.health - alteredBasicParameter.health,
                strenght = newBasicParameters.strenght - alteredBasicParameter.strenght,
                intelligent = newBasicParameters.intelligent - alteredBasicParameter.intelligent,
                agility = newBasicParameters.agility - alteredBasicParameter.agility,
                dexterity = newBasicParameters.dexterity - alteredBasicParameter.dexterity,
                vitality = newBasicParameters.vitality - alteredBasicParameter.vitality
            };
        }

        /// <summary>
        /// <span>old para</span>
        /// <span>new value is no farm enhance add</span>
        /// <span>final value = new value + farm enhance</span>
        /// <span>add value = training add</span>
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        /// <param name="finalValue"></param>
        /// <param name="addValue"></param>
        /// <param name="delayAnimationTime"></param>
        /// <param name="parameterSlider"></param>
        /// <param name="parameterTextMesh"></param>
        /// <param name="idleColor"></param>
        private void UpdateParameterAnim(float oldValue, float newValue, float finalValue, float addValue, float delayAnimationTime, Slider parameterSlider, TextMeshProUGUI parameterTextMesh, Color idleColor)
        {
            parameterSlider.DOKill();
            parameterTextMesh.DOKill();
            if (oldValue > newValue)
            {
                //parameterSlider.fillRect.GetComponent<Image>().color = decreaseColor;
                parameterSlider.DOValue(finalValue, animationTimer).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
                parameterTextMesh.DOText($"{Mathf.Clamp(Mathf.CeilToInt(finalValue), 0, 999)} (<color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>{Mathf.CeilToInt(addValue)}</color>)", animationTimer, true, ScrambleMode.Numerals).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
                return;
            }
            if (oldValue < newValue)
            {
                //parameterSlider.fillRect.GetComponent<Image>().color = increaseColor;
                parameterSlider.DOValue(finalValue, animationTimer).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
                if (finalValue >= 999)
                    parameterTextMesh.DOText($"{Mathf.Clamp(Mathf.CeilToInt(finalValue), 0, 999)} (<color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>+0</color>)", animationTimer, true, ScrambleMode.Numerals).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
                else
                    parameterTextMesh.DOText($"{Mathf.Clamp(Mathf.CeilToInt(finalValue), 0, 999)} (<color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>+{Mathf.CeilToInt(addValue)}</color>)", animationTimer, true, ScrambleMode.Numerals).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
                return;
            }
            parameterSlider.fillRect.GetComponent<Image>().color = idleColor;
            parameterSlider.DOValue(finalValue, animationTimer).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
            parameterTextMesh.DOText($"{Mathf.Clamp(Mathf.CeilToInt(finalValue), 0, 999)}", animationTimer, true, ScrambleMode.Numerals).SetDelay(delayAnimationTime).SetEase(Ease.OutQuad);
        }
    }
}