﻿using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UISelectMeal : MonoBehaviour
    {
        [SerializeField] private List<UIFoodToggle> mealToggles = new();
        [SerializeField] private GameObject foodTogglePrefab;
        [SerializeField] private Transform toggleContainer;
        [SerializeField] private ToggleGroup uiToggleGroup;
        [SerializeField] private UnityEvent onMealSelected;
        [SerializeField] private UIToggleableRect toggleableRect;
        private Food.FoodType currentFood;
        private UIFoodToggle currentFoodToggle;

        public Food.FoodType CurrentFood => currentFood;
        public UnityEvent OnMealSelected => onMealSelected;

        private void Awake()
        {
            toggleableRect.UiToggle.onValueChanged.AddListener((bool value) =>
            {
                if(value)
                {
                    ShowMealToggles();
                }
                else
                {
                    if(currentFoodToggle != null)
                        HideMealToggles(currentFoodToggle);
                }
            });
        }

        private void AddMealToggle()
        {
            foreach (KeyValuePair<Food.FoodType, Food> foodItem in PlayerStats.Instance.foodDataManagerScriptObj.foodDictionary)
            {
                if (foodItem.Value.FoodTypeVal == Food.FoodType.Empty) continue;
                GameObject mealObject = Instantiate(foodTogglePrefab, toggleContainer);
                UIFoodToggle uiFoodToggle = mealObject.GetComponent<UIFoodToggle>();

                mealToggles.Add(uiFoodToggle);
                uiFoodToggle.SetupToggle(foodItem.Value.FoodName, foodItem.Value.FoodPrice.ToString() + " B", foodItem.Value.FoodTypeVal);
                uiFoodToggle.UiToggle.group = uiToggleGroup;
                uiFoodToggle.UiToggle.onValueChanged.AddListener((bool isFoodSelected) =>
                {
                    OnFoodChanged(isFoodSelected, uiFoodToggle);
                });
            }
        }

        private void OnFoodChanged(bool isFoodSelected, UIFoodToggle uiToggle)
        {
            if (isFoodSelected)
            {
                // set meal
                currentFood = uiToggle.TypeFood;
                HideMealToggles(uiToggle);
                onMealSelected.Invoke();
                currentFoodToggle = uiToggle;
                return;
            }
            if (currentFood != Food.FoodType.Empty && currentFood == uiToggle.TypeFood)
            {
                currentFood = Food.FoodType.Empty;
                ShowMealToggles();
            }
            currentFoodToggle = null;
            onMealSelected.Invoke();
        }

        internal void ClearFoodOption()
        {
            for (int i = 0; i < mealToggles.Count; i++)
            {
                mealToggles[i].UiToggle.isOn = false;
            }
            currentFood = Food.FoodType.Empty;
            ShowMealToggles();
        }

        public void SetFavoriteFood(List<Food.FoodType> loveFood, List<Food.FoodType> hateFood)
        {
            if (mealToggles.Count == 0 || mealToggles == null)
            {
                AddMealToggle();
            }

            for (int i = 0; i < mealToggles.Count; i++)
            {
                if (loveFood.Contains(mealToggles[i].TypeFood))
                {
                    mealToggles[i].SetAsLove();
                    continue;
                }
                if (hateFood.Contains(mealToggles[i].TypeFood))
                {
                    mealToggles[i].SetAsHate();
                    continue;
                }
                mealToggles[i].SetAsNeutral();
            }
        }

        internal void SetMeal(Food.FoodType lastFoodType)
        {
            for (int i = 0; i < mealToggles.Count; i++)
            {
                mealToggles[i].UiToggle.isOn = false;
                if (mealToggles[i].TypeFood == lastFoodType)
                {
                    mealToggles[i].UiToggle.isOn = true;
                    return;
                }
            }
        }

        internal void LockFood(bool isLock)
        {
            for (int i = 0; i < mealToggles.Count; i++)
            {
                mealToggles[i].UiToggle.interactable = !isLock;
            }
        }

        private void HideMealToggles(UIFoodToggle targetToggle)
        {
            for (int i = 0; i < mealToggles.Count; i++)
            {
                if (mealToggles[i] == targetToggle) continue;
                mealToggles[i].HideToggle();
            }
        }

        private void ShowMealToggles()
        {
            for (int i = 0; i < mealToggles.Count; i++)
            {
                mealToggles[i].ShowToggle();
            }
        }
    }
}