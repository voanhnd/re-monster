﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIBasicTrainningToggle : MonoBehaviour
    {
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private Toggle toggleSetting;
        [SerializeField] private RectTransform container;
        [Header("Animation")]
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Ease closeEase;
        [SerializeField] private Ease openEase;

        [Header("Toggles")]
        [SerializeField] private Toggle hpToggle;
        [SerializeField] private Toggle strToggle;
        [SerializeField] private Toggle intToggle;
        [SerializeField] private Toggle dexToggle;
        [SerializeField] private Toggle agiToggle;
        [SerializeField] private Toggle vitToggle;

        private ParameterType currentBasicParameter;
        private UIMonsterFarmDetails farmDetails;
        private Action onBasicParameterChanged;
        private Toggle currentToggle;
        public ParameterType CurrentBasicParameter => currentBasicParameter;

        public Toggle CurrentToggle => currentToggle;

        private void Awake()
        {
            hpToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.HP, hpToggle, value);
            });
            strToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.STR, strToggle, value);
            });
            intToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.INT, intToggle, value);
            });
            dexToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.DEX, dexToggle, value);
            });
            agiToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.AGI, agiToggle, value);
            });
            vitToggle.onValueChanged.AddListener((bool value) =>
            {
                SetParameter(ParameterType.VIT, vitToggle, value);
            });
        }

        private void Start()
        {
            toggleSetting.onValueChanged.AddListener((bool value) =>
            {
                ToggleMenu(value);
            });
            ToggleMenu(toggleSetting.isOn);
        }

        private void ToggleMenu(bool isOpen)
        {
            if (isOpen)
            {
                container.DOSizeDelta(openSize, openTime).SetEase(openEase);
                if(CurrentToggle == null)
                    SetParameterToggle(farmDetails.MonsterInfoData.lastMonsterAction.basicTrainingMainParameter);
            }
            else
            {
                ClearToggleOptions();
                container.DOSizeDelta(closeSize, closeTime).SetEase(closeEase);
            }
        }

        internal void ClearToggleOptions()
        {
            hpToggle.isOn = false;
            strToggle.isOn = false;
            intToggle.isOn = false;
            dexToggle.isOn = false;
            agiToggle.isOn = false;
            vitToggle.isOn = false;
            currentToggle = null;
        }

        public void SetDetailSource(UIMonsterFarmDetails farmDetails, Action onParameterChanged)
        {
            this.farmDetails = farmDetails;
            onBasicParameterChanged = onParameterChanged;
        }

        public void SetParameterToggle(ParameterType targetParameter)
        {
            switch (targetParameter)
            {
                case ParameterType.HP:
                    hpToggle.isOn = true;
                    hpToggle.onValueChanged.Invoke(true);
                    break;
                case ParameterType.STR:
                    strToggle.isOn = true;
                    strToggle.onValueChanged.Invoke(true);
                    break;
                case ParameterType.INT:
                    intToggle.isOn = true;
                    intToggle.onValueChanged.Invoke(true);
                    break;
                case ParameterType.DEX:
                    dexToggle.isOn = true;
                    dexToggle.onValueChanged.Invoke(true);
                    break;
                case ParameterType.AGI:
                    agiToggle.isOn = true;
                    agiToggle.onValueChanged.Invoke(true);
                    break;
                case ParameterType.VIT:
                    vitToggle.isOn = true;
                    vitToggle.onValueChanged.Invoke(true);
                    break;
            }
        }

        private void SetParameter(ParameterType targetParameter, Toggle targetToggle, bool value)
        {
            currentBasicParameter = targetParameter;
            if(value)
            {
                currentToggle = targetToggle;
            }
            else
            {
                if(currentToggle == targetToggle)
                {
                    currentToggle = null;
                }
            }
            onBasicParameterChanged?.Invoke();
        }

        public void SetLock(bool isLock)
        {
            hpToggle.interactable = !isLock;
            strToggle.interactable = !isLock;
            intToggle.interactable = !isLock;
            dexToggle.interactable = !isLock;
            agiToggle.interactable = !isLock;
            vitToggle.interactable = !isLock;
        }
    }
}