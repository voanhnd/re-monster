﻿using Assets.Scripts.UI.Utilities;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UITrainingResult : UIMenuStruct
    {
        [SerializeField] private TextMeshProUGUI trainingTitleText;
        [SerializeField] private TextMeshProUGUI trainingResultText;
        [SerializeField] private UIMonsterFarmDetails farmDetail;
        [SerializeField] private UIFarmTransitionNotify notifyTranisition;
        [SerializeField] private Color increaseColor;
        [SerializeField] private Color decreaseColor;

        private void Awake()
        {
            OnMenuOpen.AddListener(() => CheckChangeParameters());
        }

        public override void Start()
        {
            base.Start();
            gameObject.SetActive(false);
        }

        public void CheckChangeParameters()
        {
            trainingResultText.text = string.Empty;
            switch (farmDetail.MonsterInfoData.lastMonsterAction.monsterActionType)
            {
                case Monster.MonsterAction.MonsterActionType.BasicTraining:
                    trainingTitleText.text = $"Basic training [{farmDetail.MonsterInfoData.lastMonsterAction.basicTrainingMainParameter}]";
                    break;
                case Monster.MonsterAction.MonsterActionType.SpecialTraining:
                    trainingTitleText.text = "Special training";
                    break;
                case Monster.MonsterAction.MonsterActionType.IntensiveTrainning:
                    trainingTitleText.text = $"Intensive training [{PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(farmDetail.MonsterSelectAction.trainingAreaType).TrainingArea.areaName}]";
                    break;
                case Monster.MonsterAction.MonsterActionType.Exploration:
                    trainingTitleText.text = $"Exploration [{PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(farmDetail.MonsterSelectAction.trainingAreaType).TrainingArea.areaName}]";
                    break;
                case Monster.MonsterAction.MonsterActionType.Rest:
                    trainingTitleText.text = "Rest";
                    break;
                case Monster.MonsterAction.MonsterActionType.Hospital:
                    trainingTitleText.text = "Hospital";
                    break;
            }
            PlayerInfo.PlayerMonsterInfo monsterInfo = farmDetail.MonsterInfoData;
            Monster.MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);
            ShowBasicPResult(monsterScriptable);
            ShowTrainingPResult();
            ShowSkillLevelUp();
        }

        private void ShowBasicPResult(Monster.MonsterDataScriptObj monsterScriptable)
        {
            string compoundStringResult = string.Empty;
            PlayerInfo.PlayerMonsterInfo monsterInfo = farmDetail.MonsterInfoData;
            compoundStringResult += SetBasicParameterString("HP", Mathf.RoundToInt(monsterInfo.basicParameters.health + monsterScriptable.Monster.basicParameters.health), Mathf.RoundToInt(monsterInfo.basicParameters.health - monsterInfo.lastMonsterAction.basicParameters.health + monsterScriptable.Monster.basicParameters.health));
            compoundStringResult += SetBasicParameterString("STR", Mathf.RoundToInt(monsterInfo.basicParameters.strenght + monsterScriptable.Monster.basicParameters.strenght), Mathf.RoundToInt(monsterInfo.basicParameters.strenght - monsterInfo.lastMonsterAction.basicParameters.strenght + monsterScriptable.Monster.basicParameters.strenght));
            compoundStringResult += SetBasicParameterString("INT", Mathf.RoundToInt(monsterInfo.basicParameters.intelligent + monsterScriptable.Monster.basicParameters.intelligent), Mathf.RoundToInt(monsterInfo.basicParameters.intelligent - monsterInfo.lastMonsterAction.basicParameters.intelligent + monsterScriptable.Monster.basicParameters.intelligent));
            compoundStringResult += SetBasicParameterString("DEX", Mathf.RoundToInt(monsterInfo.basicParameters.dexterity + monsterScriptable.Monster.basicParameters.dexterity), Mathf.RoundToInt(monsterInfo.basicParameters.dexterity - monsterInfo.lastMonsterAction.basicParameters.dexterity + monsterScriptable.Monster.basicParameters.dexterity));
            compoundStringResult += SetBasicParameterString("AGI", Mathf.RoundToInt(monsterInfo.basicParameters.agility + monsterScriptable.Monster.basicParameters.agility), Mathf.RoundToInt(monsterInfo.basicParameters.agility - monsterInfo.lastMonsterAction.basicParameters.agility + monsterScriptable.Monster.basicParameters.agility));
            compoundStringResult += SetBasicParameterString("VIT", Mathf.RoundToInt(monsterInfo.basicParameters.vitality + monsterScriptable.Monster.basicParameters.vitality), Mathf.RoundToInt(monsterInfo.basicParameters.vitality - monsterInfo.lastMonsterAction.basicParameters.vitality + monsterScriptable.Monster.basicParameters.vitality));
            trainingResultText.text += compoundStringResult;
        }

        private void ShowTrainingPResult()
        {
            string compoundStringResult = string.Empty;
            PlayerInfo.PlayerMonsterInfo monsterInfo = farmDetail.MonsterInfoData;
            compoundStringResult += GetGrowthParameterReverse("Fatigue", Mathf.RoundToInt(monsterInfo.growthParameters.fatigue), Mathf.RoundToInt(monsterInfo.growthParameters.fatigue - monsterInfo.lastMonsterAction.growthParameters.fatigue));
            compoundStringResult += GetGrowthParameterReverse("Stress", Mathf.RoundToInt(monsterInfo.growthParameters.stress), Mathf.RoundToInt(monsterInfo.growthParameters.stress - monsterInfo.lastMonsterAction.growthParameters.stress));
            compoundStringResult += SetGrowthParameter("Physical", Mathf.RoundToInt(monsterInfo.growthParameters.bodyTypeValue), Mathf.RoundToInt(monsterInfo.growthParameters.bodyTypeValue - monsterInfo.lastMonsterAction.growthParameters.bodyTypeValue));
            compoundStringResult += SetGrowthParameter("Friendship", Mathf.RoundToInt(monsterInfo.growthParameters.affection), Mathf.RoundToInt(monsterInfo.growthParameters.affection - monsterInfo.lastMonsterAction.growthParameters.affection));
            compoundStringResult += SetGrowthParameter("Policy", Mathf.RoundToInt(monsterInfo.growthParameters.trainingPolicyValue), Mathf.RoundToInt(monsterInfo.growthParameters.trainingPolicyValue - monsterInfo.lastMonsterAction.growthParameters.trainingPolicyValue));
            compoundStringResult += SetGrowthParameter("Lifespan", Mathf.RoundToInt(monsterInfo.growthParameters.lifeSpan), Mathf.RoundToInt(monsterInfo.growthParameters.lifeSpan - monsterInfo.lastMonsterAction.growthParameters.lifeSpan));
            compoundStringResult += SetGrowthParameter("Energy", Mathf.RoundToInt(monsterInfo.growthParameters.energy), Mathf.RoundToInt(monsterInfo.growthParameters.energy - monsterInfo.lastMonsterAction.growthParameters.energy));
            compoundStringResult += SetGrowthParameter("Body\t", Mathf.RoundToInt(monsterInfo.growthParameters.body), Mathf.RoundToInt(monsterInfo.growthParameters.body - monsterInfo.lastMonsterAction.growthParameters.body));
            compoundStringResult += SetGrowthParameter("Condition", Mathf.RoundToInt(monsterInfo.growthParameters.condition), Mathf.RoundToInt(monsterInfo.growthParameters.condition - monsterInfo.lastMonsterAction.growthParameters.condition));
            trainingResultText.text += compoundStringResult;
        }

        private void ShowSkillLevelUp()
        {
            string resultString = string.Empty;
            PlayerInfo.PlayerMonsterInfo monsterInfo = farmDetail.MonsterInfoData;
            if (monsterInfo.lastMonsterAction.skillUp)
            {
                Monster.Skill.SkillDataScriptObj skillData = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(monsterInfo.lastMonsterAction.trainingSkill);
                resultString = $"\t{skillData.Skill.skillName} level up to level {monsterInfo.GetPlayerMonsterSkillInfoByID(monsterInfo.lastMonsterAction.trainingSkill).skillLevel}\n";
                trainingResultText.text += resultString;
            }
        }

        private string SetBasicParameterString(string parameter, int newValue, int oldValue)
        {
            if (newValue > oldValue)
                return $"\t\t{parameter}\t\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>{newValue}</color>\n";
            if (newValue < oldValue)
                return $"\t\t{parameter}\t\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>{newValue}</color>\n";
            return string.Empty;
        }

        private string SetGrowthParameter(string parameter, int newValue, int oldValue)
        {
            if (newValue > oldValue)
                return $"\t\t{parameter}\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>{newValue}</color>\n";
            if (newValue < oldValue)
                return $"\t\t{parameter}\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>{newValue}</color>\n";
            return string.Empty;
        }

        private string GetGrowthParameterReverse(string parameter, int newValue, int oldValue)
        {
            if (newValue > oldValue)
                return $"\t\t{parameter}\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(decreaseColor)}>{newValue}</color>\n";
            if (newValue < oldValue)
                return $"\t\t{parameter}\t{oldValue} -> <color=#{ColorUtility.ToHtmlStringRGB(increaseColor)}>{newValue}</color>\n";
            return string.Empty;
        }
    }
}