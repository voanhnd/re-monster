﻿using Assets.Scripts.Monster;
using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIFarmMenu : UIMenuStruct
    {
        [SerializeField] private Transform cardHolder;
        [SerializeField] private GameObject monsterCardPrefab;
        [SerializeField] private UIPopupPanel popupPanel;
        [SerializeField] private List<RenderTexture> monsterRendering;

        [Header("Other menus")]
        [SerializeField] private UIMonsterFarmDetails farmDetailUI;
        [SerializeField] private UIMonsterFarmBagMenu farmBagMenu;
        [SerializeField] private UIMonster3dDisplayRender displayController;

        private List<UIMonsterFarmCard> monsterCards = new();
        public List<UIMonsterFarmCard> MonsterCards => monsterCards;

        private void Awake()
        {
            farmDetailUI.OnMonsterRemoved.AddListener((string monsterId, string farmId) =>
            {
                RemoveMonsterCard(farmId, monsterId);
            });
        }

        public override void Start()
        {
            base.Start();
            gameObject.SetActive(false);
        }

        public void UpdateModelView()
        {
            Debug.Log("Update model view");
            for (int i = 0; i < monsterCards.Count; i++)
            {
                PlayerInfo.PlayerFarmInfo farmInfoData = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(monsterCards[i].FarmId);
                PlayerInfo.PlayerMonsterInfo monsterInfoData = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(farmInfoData.MonsterId);
                if(monsterInfoData != null)
                    displayController.SetMonsterState(monsterCards[i].transform.GetSiblingIndex(), monsterInfoData.growthParameters.fatigue > 50 || monsterInfoData.growthParameters.stress > 50 || monsterInfoData.growthParameters.diseasesType != Monster.Monster.DiseasesType.None || monsterInfoData.growthParameters.injuryType != Monster.Monster.InjuryType.None);
            }
        }

        #region add card data
        public void AddCardData(string monsterId,
                                string monsterName, RankTypeEnums monsterRank,
                                string currentFarmLocation, Sprite backgroundScreenshot,
                                float fatigueMaxValue, float fatigueValue,
                                float hpMaxValue, float hpValue,
                                float stressMaxValue, float stressValue,
                                float typeMaxValue, float typeValue,
                                float loveMaxValue, float loveValue,
                                float policyMaxValue, float policyValue,
                                float strMaxValue, float strValue,
                                float intMaxValue, float intValue,
                                float dexMaxValue, float dexValue,
                                float agiMaxValue, float agiValue,
                                float vitMaxValue, float vitValue,
                                string farmId)
        {
            GameObject monsterGameObject = Instantiate(monsterCardPrefab, cardHolder);
            if (monsterGameObject.TryGetComponent(out UIMonsterFarmCard monsterCardBtn))
            {
                monsterCardBtn.SetTextureView(monsterRendering[monsterCards.Count]);
                monsterCardBtn.CurrentMonsterBtn.onClick.AddListener(() =>
                {
                    farmDetailUI.OpenMenu();
                    farmDetailUI.SetupMonsterImage(monsterCardBtn.MonsterImageRender.texture);
                    farmDetailUI.LoadMonsterData(monsterId, farmId);
                    farmDetailUI.SetFarmCard(monsterCardBtn);
                });

                monsterCardBtn.AddMonsterBtn.onClick.AddListener(() =>
                {
                    farmBagMenu.OpenMenu();
                    farmBagMenu.OnMonsterSet.AddListener(() =>
                    {
                        AddMonsterToFarm(farmBagMenu.MonsterId, monsterCardBtn.FarmId);
                        SetMonsterDisplay(monsterCardBtn, farmBagMenu.MonsterId);
                        LoadDetailView(monsterCardBtn, farmBagMenu.MonsterId, farmId);
                        PlayerStats.Instance.Save();
                    });
                });
                monsterCardBtn.SetDetailView(monsterName, monsterRank,
                                             currentFarmLocation, backgroundScreenshot,
                                             fatigueMaxValue, fatigueValue,
                                             hpMaxValue, hpValue,
                                             stressMaxValue, stressValue,
                                             typeMaxValue, typeValue,
                                             loveMaxValue, loveValue,
                                             policyMaxValue, policyValue,
                                             strMaxValue, strValue,
                                             intMaxValue, intValue,
                                             dexMaxValue, dexValue,
                                             agiMaxValue, agiValue,
                                             vitMaxValue, vitValue,
                                             farmId);
                SetMonsterDisplay(monsterCardBtn, monsterId);
                monsterCards.Add(monsterCardBtn);
            }
        }

        public void AddCardData(string farmId)
        {
            GameObject monsterGameObject = Instantiate(monsterCardPrefab, cardHolder);
            if (monsterGameObject.TryGetComponent(out UIMonsterFarmCard monsterCardBtn))
            {
                monsterCardBtn.SetTextureView(monsterRendering[monsterCards.Count]);
                monsterCardBtn.SetEmptyView(farmId);
                monsterCards.Add(monsterCardBtn);
            }
        }

        public void AddCardData(string currentFarmLocation, Sprite backgroundScreenshot, string farmId)
        {
            GameObject monsterGameObject = Instantiate(monsterCardPrefab, cardHolder);
            if (monsterGameObject.TryGetComponent(out UIMonsterFarmCard monsterCardBtn))
            {
                monsterCardBtn.SetTextureView(monsterRendering[monsterCards.Count]);
                monsterCardBtn.SetAddView(backgroundScreenshot, currentFarmLocation, farmId);
                monsterCards.Add(monsterCardBtn);
                monsterCardBtn.AddMonsterBtn.onClick.AddListener(() =>
                {
                    farmBagMenu.OpenMenu();
                    farmBagMenu.OnMonsterSet.AddListener(() =>
                    {
                        AddMonsterToFarm(farmBagMenu.MonsterId, monsterCardBtn.FarmId);
                        SetMonsterDisplay(monsterCardBtn, farmBagMenu.MonsterId);
                        LoadDetailView(monsterCardBtn, farmBagMenu.MonsterId, farmId);
                        PlayerStats.Instance.Save();
                    });
                });
            }
        }
        #endregion

        #region Card view controls
        private void AddMonsterToFarm(string monsterId, string farmId)
        {
            PlayerInfo.PlayerFarmInfo farmInfoData = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(farmId);
            farmInfoData.SetMonsterId(monsterId);
        }

        private void SetMonsterDisplay(UIMonsterFarmCard farmCard, string monsterId)
        {
            MonsterDataManagerScriptObj monsterScriptableManagerData = PlayerStats.Instance.monsterDataManagerScriptObj;
            PlayerInfo.PlayerMonsterInfo monsterInfoData = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            monsterInfoData.usingFarmID = farmCard.FarmId;
            MonsterDataScriptObj monsterScriptableData = monsterScriptableManagerData.GetMonsterDataObj(monsterInfoData.monsterID);

            displayController.SetMonsterToDisplay(farmCard.transform.GetSiblingIndex(), monsterScriptableData.unitData.m_Model);
        }

        private void LoadDetailView(UIMonsterFarmCard monsterCardBtn, string monsterId, string farmId)
        {
            PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            MonsterDataScriptObj monsterData = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);

            PlayerInfo.PlayerFarmInfo farmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(farmId);

            FarmDataScriptObj farmScriptable = PlayerStats.Instance.farmDataManagerScriptObj.farmDataDictionary[farmInfo.usingFarmType];
            BasicParameters complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, monsterData.Monster.basicParameters);

            monsterCardBtn.SetDetailView(monsterData.Monster.monsterName, monsterInfo.rankType,
                                 farmScriptable.Farm.FarmName, farmScriptable.Farm.FarmBackgroundSprite,
                                 100, monsterInfo.growthParameters.fatigue,
                                 100, monsterInfo.growthParameters.stress,
                                 100, monsterInfo.growthParameters.bodyTypeValue,
                                 100, monsterInfo.growthParameters.affection,
                                 100, monsterInfo.growthParameters.trainingPolicyValue,
                                 999, complexBasicParameters.health,
                                 999, complexBasicParameters.strenght ,
                                 999, complexBasicParameters.intelligent ,
                                 999, complexBasicParameters.dexterity,
                                 999, complexBasicParameters.agility,
                                 999, complexBasicParameters.vitality,
                                 monsterCardBtn.FarmId);
            monsterCardBtn.CurrentMonsterBtn.onClick.RemoveAllListeners();
            monsterCardBtn.CurrentMonsterBtn.onClick.AddListener(() =>
            {
                farmDetailUI.OpenMenu();
                farmDetailUI.SetupMonsterImage(monsterCardBtn.MonsterImageRender.texture);
                farmDetailUI.LoadMonsterData(monsterId, farmId);
                farmDetailUI.SetFarmCard(monsterCardBtn);
            });
            displayController.SetMonsterState(monsterCardBtn.transform.GetSiblingIndex(), monsterInfo.growthParameters.fatigue > 50 || monsterInfo.growthParameters.stress > 50 || monsterInfo.growthParameters.diseasesType != Monster.Monster.DiseasesType.None || monsterInfo.growthParameters.injuryType != Monster.Monster.InjuryType.None);
        }

        private void RemoveMonsterCard(string farmId, string monsterId)
        {
            PlayerStats.Instance.Save();
            for (int i = 0; i < monsterCards.Count; i++)
            {
                int index = i;
                if (monsterCards[index].FarmId == farmId)
                {
                    monsterCards[index].SetAddView(monsterCards[index].FarmSprite, monsterCards[i].FarmName, monsterCards[index].FarmId);
                    monsterCards[index].AddMonsterBtn.onClick.AddListener(() =>
                    {
                        farmBagMenu.OpenMenu();
                        farmBagMenu.OnMonsterSet.AddListener(() =>
                        {
                            AddMonsterToFarm(farmBagMenu.MonsterId, monsterCards[index].FarmId);
                            SetMonsterDisplay(monsterCards[index], farmBagMenu.MonsterId);
                            LoadDetailView(monsterCards[index], farmBagMenu.MonsterId, farmId);
                            PlayerStats.Instance.Save();
                        });
                    });
                    return;
                }
            }
        }
        #endregion
    }
}