﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIAlertNotify : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI notifyText;
        [SerializeField] private Image notifyGraphic;
        [SerializeField] private Color basicColor;
        [SerializeField] private Color alertColor;
        [SerializeField] private float blinkTime = 1;
        [SerializeField] private UIMonsterFarmDetails farmDetail;
        private float elapseBlinkTime;
        private bool isBlink;
        private bool isBasicColor = true;
        private Monster.Monster.InjuryType injuredCondition = Monster.Monster.InjuryType.None;
        private Monster.Monster.DiseasesType diseasesCondition = Monster.Monster.DiseasesType.None;

        private void Awake()
        {
            farmDetail.OnMenuOpen.AddListener(() =>
            {
                ResetBlinkData();
                LifespanCheck();
            });
            farmDetail.MealSelectUI.OnMealSelected.AddListener(() =>
            {
                MealConditionCheck();
                TrainingParamCheckCondition();
                CheckMonsterCondition();
            });
        }

        private void LifespanCheck()
        {
            if (farmDetail.MonsterInfoData.growthParameters.lifeSpan <= 0)
            {
                notifyText.text = $"{farmDetail.MonsterInfoData.monsterName}'s lifespan is not enough to perform training action!\n";
                return;
            }
            MealConditionCheck();
            TrainingParamCheckCondition();
            CheckMonsterCondition();
        }

        private void Update()
        {
            BlinkCountDown();
        }

        private void BlinkCountDown()
        {
            if (!isBlink) return;
            elapseBlinkTime += Time.deltaTime;
            if (elapseBlinkTime > blinkTime)
            {
                elapseBlinkTime = 0;
                isBasicColor = !isBasicColor;
                BlinkColorTime();
            }
        }

        private void OnDisable()
        {
            isBasicColor = true;
            notifyText.text = string.Empty;
            notifyGraphic.color = basicColor;
        }

        #region Alert blink control
        private void BlinkColorTime() => notifyGraphic.color = isBasicColor ? basicColor : alertColor;

        private void ResetBlinkData()
        {
            isBlink = false;
            notifyText.text = string.Empty;
            notifyGraphic.color = basicColor;
            elapseBlinkTime = 0;
            isBasicColor = true;
        }
        #endregion

        #region Condition checks
        private void CheckMonsterCondition()
        {
            if (string.IsNullOrEmpty(farmDetail.MonsterId)) return;

            injuredCondition = farmDetail.MonsterInfoData.growthParameters.injuryType;
            diseasesCondition = farmDetail.MonsterInfoData.growthParameters.diseasesType;
            if (injuredCondition == Monster.Monster.InjuryType.None && diseasesCondition == Monster.Monster.DiseasesType.None) return;

            string extraInfoCondition = string.Empty;
            switch (injuredCondition)
            {
                case Monster.Monster.InjuryType.Injury:
                    extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} is injured!\n";
                    break;
                case Monster.Monster.InjuryType.Serious_Injury:
                    extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} is seriously injured!\n";
                    break;
            }

            switch (diseasesCondition)
            {
                case Monster.Monster.DiseasesType.Diseases:
                    extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} is sick!\n";
                    break;
                case Monster.Monster.DiseasesType.Serious_Diseases:
                    extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} is seriously sick!\n";
                    break;
            }
            extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} need to go to hospital!";
            notifyText.text += extraInfoCondition;
            isBlink = true;
        }

        private void TrainingParamCheckCondition()
        {
            string stressInfo = StressCheckCondition();
            string fatigueInfo = FatigueCheckCondition();
            if (string.IsNullOrEmpty(stressInfo) && string.IsNullOrEmpty(fatigueInfo)) return;
            string extraInfoCondition = string.Empty;
            if (!string.IsNullOrEmpty(stressInfo))
            {
                extraInfoCondition += stressInfo + "\n";
            }
            if (!string.IsNullOrEmpty(fatigueInfo))
            {
                extraInfoCondition += fatigueInfo + "\n";
            }
            extraInfoCondition += $"{farmDetail.MonsterInfoData.monsterName} need to rest!\n\n";
            notifyText.text += extraInfoCondition;
            isBlink = true;
        }

        private string StressCheckCondition()
        {
            if (string.IsNullOrEmpty(farmDetail.MonsterId)) return string.Empty;
            if (injuredCondition == Monster.Monster.InjuryType.Serious_Injury || diseasesCondition == Monster.Monster.DiseasesType.Serious_Diseases) return string.Empty;
            if (farmDetail.MonsterInfoData.growthParameters.stress > 50 && farmDetail.MonsterInfoData.growthParameters.stress < 81)
                return $"{farmDetail.MonsterInfoData.monsterName} is highly stressed!";
            if (farmDetail.MonsterInfoData.growthParameters.stress > 80)
                return $"{farmDetail.MonsterInfoData.monsterName} is overly stressed!";
            return string.Empty;
        }

        private string FatigueCheckCondition()
        {
            if (string.IsNullOrEmpty(farmDetail.MonsterId)) return string.Empty;
            if (injuredCondition == Monster.Monster.InjuryType.Serious_Injury || diseasesCondition == Monster.Monster.DiseasesType.Serious_Diseases) return string.Empty;
            if (farmDetail.MonsterInfoData.growthParameters.fatigue > 50 && farmDetail.MonsterInfoData.growthParameters.fatigue < 81)
                return $"{farmDetail.MonsterInfoData.monsterName} is tired!";
            if (farmDetail.MonsterInfoData.growthParameters.fatigue > 80)
                return $"{farmDetail.MonsterInfoData.monsterName} is overworked!";
            return string.Empty;
        }

        private void MealConditionCheck()
        {
            if (string.IsNullOrEmpty(farmDetail.MonsterId)) return;
            if (farmDetail.MealSelectUI.CurrentFood == Food.FoodType.Empty)
            {
                notifyText.text = "Food is require for taking an action!\n";
            }
            else
            {
                notifyText.text = string.Empty;
            }
        }
        #endregion
    }
}