﻿using Assets.Scripts.Monster;
using Assets.Scripts.TrainingArea;
using UnityEngine;
using static PlayerInfo;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIPredictActionResult : MonoBehaviour
    {
        [SerializeField] private UIMonsterFarmDetails detailFarm;

        private BasicParameters mainValuePara;
        private BasicParameters subValuePara;
        private BasicParameters decValuePara;
        private BasicParameters finalValuePara;

        public BasicParameters MainValuePara => mainValuePara;
        public BasicParameters SubValuePara => subValuePara;
        public BasicParameters DecValuePara => decValuePara;
        public BasicParameters FinalValuePara => finalValuePara;

        public void PredictResult()
        {
            mainValuePara = new();
            subValuePara = new();
            decValuePara = new();
            if (detailFarm.ScriptableMonsterData == null) return;
            if (detailFarm.ScriptableFarmData == null) return;
            if (detailFarm.MonsterInfoData == null) return;
            if (string.IsNullOrEmpty(detailFarm.FarmId)) return;

            switch (detailFarm.MonsterSelectAction.monsterActionType)
            {
                case MonsterAction.MonsterActionType.BasicTraining:
                    CalculateBasicParaMain();
                    CalculateBasicParaSub();
                    break;
                case MonsterAction.MonsterActionType.SpecialTraining:
                    CalculateSpecialParaMain();
                    CalculateSpecialParaSub();
                    CalculateSpecialParaDec();
                    break;
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    mainValuePara = CalculateIntensiveTrainingParaMain();
                    subValuePara = CalculateIntensiveTrainingParaSub();
                    break;
                case MonsterAction.MonsterActionType.Exploration:
                    mainValuePara = CalculateExploreParaMain();
                    break;
            }
            finalValuePara = new()
            {
                health = Mathf.Min(mainValuePara.health + subValuePara.health + decValuePara.health, 999),
                strenght = Mathf.Min(mainValuePara.strenght + subValuePara.strenght + decValuePara.strenght, 999),
                intelligent = Mathf.Min(mainValuePara.intelligent + subValuePara.intelligent + decValuePara.intelligent, 999),
                dexterity = Mathf.Min(mainValuePara.dexterity + subValuePara.dexterity + decValuePara.dexterity, 999),
                agility = Mathf.Min(mainValuePara.agility + subValuePara.agility + decValuePara.agility, 999),
                vitality = Mathf.Min(mainValuePara.vitality + subValuePara.vitality + decValuePara.vitality, 999)
            };
        }

        private void CalculateBasicParaMain()
        {
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, farmScriptableData.TerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, farmScriptableData.TerrainType);
            switch (detailFarm.MonsterSelectAction.basicTrainingMainParameter)
            {
                case ParameterType.HP:
                    mainValuePara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.STR:
                    mainValuePara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                                         mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                                         monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                                         Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                                         TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.INT:
                    mainValuePara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                                            mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                                            monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                                            Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                                            TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.DEX:
                    mainValuePara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                                          mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                                          monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                                          Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                                          TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.AGI:
                    mainValuePara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                                        mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                                        monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                                        Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                                        TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.VIT:
                    mainValuePara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(8, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                                         mainRaceStat, subRaceStat, 0, 0, 0, scriptableMonsterData.growthType,
                                                                                         monsterInfo.growthParameters.lifeSpan, scriptableMonsterData.growthParameters.lifeSpan,
                                                                                         Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                                         TrainingArea.TrainingArea.TrainingType.success);
                    break;
            }
        }

        private void CalculateBasicParaSub()
        {
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, farmScriptableData.TerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, farmScriptableData.TerrainType);

            switch (detailFarm.MonsterSelectAction.basicTrainingMainParameter)
            {
                case ParameterType.HP:
                    subValuePara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.STR:
                    subValuePara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.INT:
                    subValuePara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.DEX:
                    subValuePara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.AGI:
                    subValuePara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.VIT:
                    subValuePara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(4, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
            }
        }

        private void CalculateSpecialParaMain()
        {
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, farmScriptableData.TerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, farmScriptableData.TerrainType);

            switch (farmScriptableData.mainParameter)
            {
                case ParameterType.HP:
                    mainValuePara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.STR:
                    mainValuePara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.INT:
                    mainValuePara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.DEX:
                    mainValuePara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.AGI:
                    mainValuePara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.VIT:
                    mainValuePara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(12, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
            }
        }

        private void CalculateSpecialParaSub()
        {
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, farmScriptableData.TerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, farmScriptableData.TerrainType);

            switch (farmScriptableData.subParameter)
            {
                case ParameterType.HP:
                    subValuePara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.STR:
                    subValuePara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.INT:
                    subValuePara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.DEX:
                    subValuePara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.AGI:
                    subValuePara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.VIT:
                    subValuePara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(6, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
            }
        }

        private void CalculateSpecialParaDec()
        {
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, farmScriptableData.TerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, farmScriptableData.TerrainType);

            switch (farmScriptableData.deParameter)
            {
                case ParameterType.HP:
                    decValuePara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.STR:
                    decValuePara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.INT:
                    decValuePara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.DEX:
                    decValuePara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.AGI:
                    decValuePara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
                case ParameterType.VIT:
                    decValuePara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Down(-6, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
                    break;
            }
        }

        private BasicParameters CalculateIntensiveTrainingParaMain()
        {
            BasicParameters trainingPara = new();
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(detailFarm.MonsterSelectAction.trainingAreaType);
            if (basicTrainingAreaDataScriptObj == null) return new();

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);


            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.health > 0)
            {
                trainingPara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.health, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.strenght > 0)
            {
                trainingPara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.strenght, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.intelligent > 0)
            {
                trainingPara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.intelligent, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.dexterity > 0)
            {
                trainingPara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.dexterity, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.agility > 0)
            {
                trainingPara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.agility, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.vitality > 0)
            {
                trainingPara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.vitality, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }
            /*trainingPara.health *= 4;
            trainingPara.strenght *= 4;
            trainingPara.intelligent *= 4;
            trainingPara.dexterity *= 4;
            trainingPara.agility *= 4;
            trainingPara.vitality *= 4;*/

            return trainingPara;
        }

        private BasicParameters CalculateIntensiveTrainingParaSub()
        {
            BasicParameters trainingPara = new();
            Monster.Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;

            TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(detailFarm.MonsterSelectAction.trainingAreaType);
            if (basicTrainingAreaDataScriptObj == null) return new();

            int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);


            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.health > 0)
            {
                trainingPara.health = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.health, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.strenght > 0)
            {
                trainingPara.strenght = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.strenght, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.intelligent > 0)
            {
                trainingPara.intelligent = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.intelligent, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.dexterity > 0)
            {
                trainingPara.dexterity = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.dexterity, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.agility > 0)
            {
                trainingPara.agility = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.agility, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.vitality > 0)
            {
                trainingPara.vitality = TrainingArea.TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.subParameters.vitality, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingArea.TrainingType.success);
            }
            /*trainingPara.health *= 4;
            trainingPara.strenght *= 4;
            trainingPara.intelligent *= 4;
            trainingPara.dexterity *= 4;
            trainingPara.agility *= 4;
            trainingPara.vitality *= 4;*/
            return trainingPara;
        }

        private BasicParameters CalculateExploreParaMain()
        {
            BasicParameters trainingPara = new();
            /*Monster scriptableMonsterData = detailFarm.ScriptableMonsterData.Monster;
            Farm farmScriptableData = detailFarm.ScriptableFarmData.Farm;
            PlayerMonsterInfo monsterInfo = detailFarm.MonsterInfoData;
*/
            TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(detailFarm.MonsterSelectAction.trainingAreaType);
            if (basicTrainingAreaDataScriptObj == null) return new();
            /*int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.mainSuitTerrain, scriptableMonsterData.mainNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);
            int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.subSuitTerrain, scriptableMonsterData.subNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);
*/
            trainingPara.health = basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health;
            trainingPara.strenght = basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght;
            trainingPara.intelligent = basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent;
            trainingPara.dexterity= basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity;
            trainingPara.agility= basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility;
            trainingPara.vitality= basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality;

            /*if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health > 0)
            {
                trainingPara.health = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health, scriptableMonsterData.HP_GA_Rank, farmScriptableData.enhancedParameters.health,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght > 0)
            {
                trainingPara.strenght = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght, scriptableMonsterData.STR_GA_Rank, farmScriptableData.enhancedParameters.strenght,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent > 0)
            {
                trainingPara.intelligent = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent, scriptableMonsterData.INT_GA_Rank, farmScriptableData.enhancedParameters.intelligent,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity > 0)
            {
                trainingPara.dexterity = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity, scriptableMonsterData.DEX_GA_Rank, farmScriptableData.enhancedParameters.dexterity,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility > 0)
            {
                trainingPara.agility = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility, scriptableMonsterData.AGI_GA_Rank, farmScriptableData.enhancedParameters.agility,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }

            if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality > 0)
            {
                trainingPara.vitality = TrainingArea.Get_Training_Parameter_Rise_Up(basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality, scriptableMonsterData.VIT_GA_Rank, farmScriptableData.enhancedParameters.vitality,
                                                                mainRaceStat, subRaceStat, 0, 0, 0,
                                                                scriptableMonsterData.growthType,
                                                                monsterInfo.growthParameters.lifeSpan,
                                                                scriptableMonsterData.growthParameters.lifeSpan,
                                                                Monster.Get_Complex_Parameter(scriptableMonsterData.basicParameters, scriptableMonsterData.basicParameters),
                                                                TrainingArea.TrainingType.success);
            }*/
            /*trainingPara.health *= 4;
            trainingPara.strenght *= 4;
            trainingPara.intelligent *= 4;
            trainingPara.dexterity *= 4;
            trainingPara.agility *= 4;
            trainingPara.vitality *= 4;*/

            return trainingPara;
        }
    }
}