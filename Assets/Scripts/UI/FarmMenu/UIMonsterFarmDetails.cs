﻿using Assets.Scripts.Monster;
using Assets.Scripts.TrainingArea;
using Assets.Scripts.UI.MonsterDetailsInfo;
using Assets.Scripts.UI.TrainningMenu;
using Assets.Scripts.UI.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIMonsterFarmDetails : UIMenuStruct
    {
        #region Private values
        [SerializeField] private Button acceptBtn;
        [SerializeField] private Button removeBtn;
        [SerializeField] private Button openMonsterDetailBtn;
        [SerializeField] private RawImage monsterImage;
        [SerializeField] private Image raiseBackground;
        [SerializeField] private TextMeshProUGUI bitLeftText;
        [SerializeField] private GameObject bitValueGameObject;

        [Header("Side menu groups")]
        [SerializeField] private UIMonsterDetailInfo monsterDetailUI;
        [SerializeField] private UISelectMeal mealSelectUI;
        [SerializeField] private UIExploreMenu uiExploreMenu;
        [SerializeField] private UIIntensiveTrainningMenu uiIntensiveTrainningMenu;
        [SerializeField] private UIMonsterFarmBagMenu uiMonsterBag;
        [SerializeField] private UIPredictActionResult actionPredict;
        [SerializeField] private UIMealActionControl uiMealAction;

        [Header("Menu events")]
        [SerializeField] private UnityEvent onAccecpt;
        [SerializeField] private UnityEvent onDeny;
        [SerializeField] private UnityEvent<string, string> onMonsterRemoved;//monsterid, farmid

        [Header("Detail stats")]
        [SerializeField] private TextMeshProUGUI monsterName;
        [SerializeField] private Image monsterRank;

        [Header("Extra components")]
        [SerializeField] private UIMonsterFarmDetailsBattleStatsBase battleStatsComponents;
        [SerializeField] private UIMonsterFarmDetailsTrainingStatBase affectionStatsComponents;
        [SerializeField] private UIMonsterFarmDetailsAction raiseActionComponents;

        [Header("Meal description")]
        [SerializeField] private Transform mealHolder;

        private string farmId;
        private string monsterId;
        private bool isUILock = false;
        private TrainingArea.TrainingArea.TrainingAreaType mapArea;
        private MonsterAction monsterSelectAction;
        private PlayerInfo.PlayerMonsterInfo monsterInfo;
        private UIMonsterFarmCard farmCard;
        private bool isRequestMonsterChange;
        private BasicParameters complexBasicParameters;
        private float mapPrice = 0;
        #endregion

        #region Public values
        public string FarmId => farmId;
        public string MonsterId => monsterId;
        public bool IsUILock => isUILock;
        public UnityEvent<string, string> OnMonsterRemoved { get => onMonsterRemoved; set => onMonsterRemoved = value; }
        public UnityEvent OnAccecpt { get => onAccecpt; set => onAccecpt = value; }
        public MonsterAction MonsterSelectAction => monsterSelectAction;
        public MonsterDataScriptObj ScriptableMonsterData { get; private set; }
        public FarmDataScriptObj ScriptableFarmData { get; private set; }
        public PlayerInfo.PlayerMonsterInfo MonsterInfoData => monsterInfo;
        public UIMonsterFarmDetailsAction RaiseActionComponents => raiseActionComponents;
        public UISelectMeal MealSelectUI => mealSelectUI;
        #endregion

        private void Awake()
        {
            raiseActionComponents.BasicTrainingToggle.SetDetailSource(this, () =>
            {
                SetAction(MonsterAction.MonsterActionType.BasicTraining);
            });
            raiseActionComponents.RemoveToggle();

            SetCallbackListeners();

            raiseActionComponents.SetTargetFarm(this);

            raiseActionComponents.SetMapActionButtons(uiIntensiveTrainningMenu, uiExploreMenu);

            raiseActionComponents.SetActionToggles();
            openMonsterDetailBtn.onClick.AddListener(() =>
            {
                monsterDetailUI.SetMonster(monsterInfo, ScriptableMonsterData, ScriptableFarmData, monsterImage.texture);
            });

            OnMenuOpen.AddListener(() =>
            {
                bitLeftText.text = $"{SpecialFloatToInt.Convert(PlayerStats.Instance.playerInfo.bit)}";
                SetPreviousAction();
                isRequestMonsterChange = true;
            });

            OnMenuClose.AddListener(() =>
            {
                ClearUI();
                mealSelectUI.ClearFoodOption();
                isRequestMonsterChange = false;
            });
        }

        public override void Start()
        {
            base.Start();
            gameObject.SetActive(false);
        }

        private void SetCallbackListeners()
        {
            removeBtn.onClick.AddListener(() => { SetMonsterChange(); });

            acceptBtn.onClick.AddListener(() => { AccecptAction(); });

            uiMonsterBag.OnMonsterChange.AddListener(() => { OnConfirmChanged(); });

            uiMonsterBag.OnMonsterRemove.AddListener(() => { OnConfirmRemove(); });

            mealSelectUI.OnMealSelected.AddListener(() => { CheckActionBtn(); });
        }

        #region Data view alter
        private void SetMonsterChange()
        {
            farmCard.AddMonsterBtn.onClick.Invoke();
            uiMonsterBag.SetRemoveMode();
        }

        public void SetupMonsterImage(Texture monsterTexture) => monsterImage.texture = monsterTexture;

        private void SetFoodAffection(MonsterDataScriptObj monsterScriptable) => mealSelectUI.SetFavoriteFood(monsterScriptable.Monster.loveFoods, monsterScriptable.Monster.hateFoods);

        internal void SetMapArea(TrainingArea.TrainingArea.TrainingAreaType mapArea, float mapPrice)
        {
            this.mapArea = mapArea;
            this.mapPrice = mapPrice;
        }

        public void SetAction(MonsterAction.MonsterActionType monsterActionType)
        {
            monsterSelectAction = new()
            {
                monsterActionType = monsterActionType
            };

            raiseActionComponents.SetToggleValue(monsterActionType, ref monsterSelectAction, mapArea, raiseActionComponents.BasicTrainingToggle);
        }
        #endregion

        #region Load data
        public void LoadMonsterData(string monsterId, string farmId)
        {
            this.monsterId = monsterId;
            this.farmId = farmId;

            this.monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            ScriptableMonsterData = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);

            PlayerInfo.PlayerFarmInfo farmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(farmId);
            ScriptableFarmData = PlayerStats.Instance.farmDataManagerScriptObj.farmDataDictionary[farmInfo.usingFarmType];

            complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, ScriptableMonsterData.Monster.basicParameters);

            raiseBackground.sprite = ScriptableFarmData.Farm.img_Landscape;

            battleStatsComponents.SetBattleStats(999, Mathf.RoundToInt(complexBasicParameters.health),
                                                 999, Mathf.RoundToInt(complexBasicParameters.strenght),
                                                 999, Mathf.RoundToInt(complexBasicParameters.intelligent),
                                                 999, Mathf.RoundToInt(complexBasicParameters.dexterity),
                                                 999, Mathf.RoundToInt(complexBasicParameters.agility),
                                                 999, Mathf.RoundToInt(complexBasicParameters.vitality));

            affectionStatsComponents.LoadAffectionStats(100, monsterInfo.growthParameters.fatigue,
                                                        100, monsterInfo.growthParameters.stress,
                                                        100, monsterInfo.growthParameters.bodyTypeValue,
                                                        100, monsterInfo.growthParameters.affection,
                                                        100, monsterInfo.growthParameters.trainingPolicyValue,
                                                        ScriptableMonsterData.Monster.growthParameters.lifeSpan, monsterInfo.growthParameters.lifeSpan);
            affectionStatsComponents.LoadMealConditionStats(100, monsterInfo.growthParameters.energy, 100, monsterInfo.growthParameters.body, 100, monsterInfo.growthParameters.condition);
            LoadMonsterBasicData(ScriptableMonsterData.Monster.monsterName, monsterInfo.rankType);

            SetFoodAffection(ScriptableMonsterData);

            uiExploreMenu.SetTargetMonster(this);
            uiIntensiveTrainningMenu.SetTargetMonster(this);

            //load non select ui
            UnlockUI();
            mealSelectUI.ClearFoodOption();
            battleStatsComponents.UpdateActionStatAnim(monsterInfo.lastMonsterAction.basicParameters, complexBasicParameters);
            affectionStatsComponents.UpdateAffectionStatsNoAnim(monsterInfo.lastMonsterAction.growthParameters, monsterInfo.growthParameters);
            affectionStatsComponents.UpdateMealConditionStats(monsterInfo.lastMonsterAction.growthParameters, monsterInfo.growthParameters);

            if (monsterInfo.growthParameters.lifeSpan <= 0)
            {
                LockUI();
                mealSelectUI.LockFood(true);
                raiseActionComponents.BasicTrainingToggle.SetLock(true);
                return;
            }

            LoadExploreData(TrainingArea.TrainingArea.TrainingAreaType.Empty);
            LoadIntensiveTrainningData(TrainingArea.TrainingArea.TrainingAreaType.Empty);
            mealSelectUI.LockFood(false);
            raiseActionComponents.BasicTrainingToggle.SetLock(false);
            acceptBtn.interactable = false;
            if (monsterInfo.lastMonsterAction.actionDone)
                SetPreivousMeal();
            RaiseActionComponents.HospitalDateSet(monsterInfo.growthParameters.injuryType == Monster.Monster.InjuryType.None && monsterInfo.growthParameters.diseasesType == Monster.Monster.DiseasesType.None);
            RaiseActionComponents.LockToggles(monsterInfo.growthParameters.injuryType == Monster.Monster.InjuryType.Serious_Injury || monsterInfo.growthParameters.diseasesType == Monster.Monster.DiseasesType.Serious_Diseases);
            raiseActionComponents.BasicTrainingToggle.SetLock(monsterInfo.growthParameters.injuryType == Monster.Monster.InjuryType.Serious_Injury || monsterInfo.growthParameters.diseasesType == Monster.Monster.DiseasesType.Serious_Diseases);
        }

        private void LoadMonsterBasicData(string monsterName, RankTypeEnums monsterRank)
        {
            this.monsterName.text = monsterName;
            this.monsterRank.sprite = RankText.Instance.GetRank(monsterRank);
        }

        private void LoadExploreData(TrainingArea.TrainingArea.TrainingAreaType areaType) => uiExploreMenu.SetTrainningArea(areaType, isUILock);

        private void LoadIntensiveTrainningData(TrainingArea.TrainingArea.TrainingAreaType areaType) => uiIntensiveTrainningMenu.SetTrainningArea(areaType, isUILock);

        #endregion

        #region Action control
        private void SetPreviousAction()
        {
            if (!monsterInfo.lastMonsterAction.actionDone) return;
            if (monsterInfo.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Hospital) return;
            if (monsterInfo.growthParameters.injuryType == Monster.Monster.InjuryType.Serious_Injury || monsterInfo.growthParameters.diseasesType == Monster.Monster.DiseasesType.Serious_Diseases) return;
            raiseActionComponents.SetToggleValue(monsterInfo.lastMonsterAction);
            switch (monsterInfo.lastMonsterAction.monsterActionType)
            {
                case MonsterAction.MonsterActionType.BasicTraining:
                    raiseActionComponents.BasicTrainingToggle.SetParameterToggle(monsterInfo.lastMonsterAction.basicTrainingMainParameter);
                    break;
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    TrainingAreaDataScriptObj trainingScriptable = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(monsterInfo.lastMonsterAction.trainingAreaType);
                    raiseActionComponents.SetIntensiveTrainning(monsterInfo.lastMonsterAction.trainingAreaType, trainingScriptable.TrainingArea.AreaPrice);
                    SetSkillLearn(monsterInfo.lastMonsterAction.trainingSkill);
                    uiIntensiveTrainningMenu.SetTrainningArea(monsterInfo.lastMonsterAction.trainingAreaType);
                    break;
                case MonsterAction.MonsterActionType.Exploration:
                    TrainingAreaDataScriptObj exploreScriptable = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(monsterInfo.lastMonsterAction.trainingAreaType);
                    raiseActionComponents.SetExploreAction(monsterInfo.lastMonsterAction.trainingAreaType, exploreScriptable.TrainingArea.AreaPrice);
                    uiExploreMenu.SetTrainningArea(monsterInfo.lastMonsterAction.trainingAreaType);
                    break;
            }
        }

        private void AccecptAction()
        {
            acceptBtn.interactable = false;
            PlayerInfo.PlayerMonsterInfo targetMonster = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            targetMonster.lastFoodType = mealSelectUI.CurrentFood;
            for (int i = 0; i < PlayerStats.Instance.playerInfo.PlayerMonsterInfos.Count; i++)
            {
                if (PlayerStats.Instance.playerInfo.PlayerMonsterInfos[i].monsterID == monsterId)
                {
                    PlayerStats.Instance.playerInfo.PlayerMonsterInfos[i].lastMonsterAction = monsterSelectAction;
                    break;
                }
            }
            PlayerStats.Instance.Save();
            onAccecpt.Invoke();
        }

        private void OnConfirmChanged()
        {
            if (!isRequestMonsterChange) return;
            monsterInfo.usingFarmID = string.Empty;
            onMonsterRemoved.Invoke(monsterId, farmId);
            CloseMenu();
        }

        private void OnConfirmRemove()
        {
            if (!isRequestMonsterChange) return;
            monsterInfo.usingFarmID = string.Empty;
            CloseMenu();
            onMonsterRemoved.Invoke(monsterId, farmId);
        }
        #endregion

        #region Menu control
        private void UnlockUI() => raiseActionComponents.UnlockToggles();

        private void LockUI()
        {
            raiseActionComponents.LockToggles();
            acceptBtn.interactable = false;
        }

        internal void CheckActionBtn()
        {
            float price = 0;
            if (mealSelectUI.CurrentFood == Food.FoodType.Empty)
            {
                acceptBtn.interactable = false;
                bitLeftText.DOKill();
                bitLeftText.DOText($"{SpecialFloatToInt.Convert(PlayerStats.Instance.playerInfo.bit)}", 0.2f, true, ScrambleMode.Numerals);
                battleStatsComponents.UpdateActionStatAnim(monsterInfo.lastMonsterAction.basicParameters, complexBasicParameters);
                Debug.Log("Check button");
                return;
            }
            price += PlayerStats.Instance.foodDataManagerScriptObj.GetFood(mealSelectUI.CurrentFood).FoodPrice;
            bitLeftText.DOKill();
            bitLeftText.DOText($"{Mathf.Max(0, SpecialFloatToInt.Convert(PlayerStats.Instance.playerInfo.bit - price))}({-SpecialFloatToInt.Convert(price)})", 0.2f, true, ScrambleMode.Numerals);
            if (raiseActionComponents.CheckBasicValidToggle())
            {
                acceptBtn.interactable = raiseActionComponents.BasicTrainingToggle.CurrentToggle != null;
                Debug.Log("Check button");
                //uiMealAction.ActionMenuToggle.isOn = !(basicTrainningToggleUI.CurrentToggle != null);
                CheckPreviewData(raiseActionComponents.BasicTrainingToggle.CurrentToggle != null);
                return;
            }

            if (monsterSelectAction != null)
            {
                switch (monsterSelectAction.monsterActionType)
                {
                    case MonsterAction.MonsterActionType.IntensiveTrainning:
                    case MonsterAction.MonsterActionType.Exploration:
                        price *= 4;
                        price += mapPrice;
                        break;
                    case MonsterAction.MonsterActionType.Hospital:
                        if (monsterInfo.growthParameters.diseasesType == Monster.Monster.DiseasesType.Serious_Diseases || monsterInfo.growthParameters.injuryType == Monster.Monster.InjuryType.Serious_Injury)
                        {
                            price *= 3;
                            price += (1000 * 3);
                        }
                        else
                        {
                            price += 1000;
                        }
                        break;
                }
            }
            bitLeftText.DOKill();
            bitLeftText.DOText($"{Mathf.Max(0, SpecialFloatToInt.Convert(PlayerStats.Instance.playerInfo.bit - price))}({-SpecialFloatToInt.Convert(price)})", 0.2f, true, ScrambleMode.Numerals);
            acceptBtn.interactable = raiseActionComponents.CurrentToggleAction != null && PlayerStats.Instance.playerInfo.bit > price;
            Debug.Log("Check button");

            //uiMealAction.ActionMenuToggle.isOn = !(raiseActionComponents.CurrentToggleAction != null);
            CheckPreviewData(raiseActionComponents.CurrentToggleAction != null);
        }

        internal void CheckPreviewData(bool canCheck = false)
        {
            if (!canCheck)
            {
                battleStatsComponents.UpdateActionStatAnim(monsterInfo.lastMonsterAction.basicParameters, complexBasicParameters);
                return;
            }

            actionPredict.PredictResult();
            BasicParameters predictPara = new()
            {
                health = actionPredict.FinalValuePara.health + complexBasicParameters.health,
                strenght = actionPredict.FinalValuePara.strenght + complexBasicParameters.strenght,
                intelligent = actionPredict.FinalValuePara.intelligent + complexBasicParameters.intelligent,
                dexterity = actionPredict.FinalValuePara.dexterity + complexBasicParameters.dexterity,
                agility = actionPredict.FinalValuePara.agility + complexBasicParameters.agility,
                vitality = actionPredict.FinalValuePara.vitality + complexBasicParameters.vitality
            };
            //check if value is change update view 
            if (ComparePara(complexBasicParameters, predictPara))
            {
                battleStatsComponents.UpdateActionStatAnim(actionPredict.FinalValuePara, predictPara);
                return;
            }
            battleStatsComponents.SetBattleStats(999, Mathf.RoundToInt(complexBasicParameters.health),
                                                 999, Mathf.RoundToInt(complexBasicParameters.strenght),
                                                 999, Mathf.RoundToInt(complexBasicParameters.intelligent),
                                                 999, Mathf.RoundToInt(complexBasicParameters.dexterity),
                                                 999, Mathf.RoundToInt(complexBasicParameters.agility),
                                                 999, Mathf.RoundToInt(complexBasicParameters.vitality));
        }

        private void ClearUI()
        {
            raiseActionComponents.BasicTrainingToggle.ClearToggleOptions();
            raiseActionComponents.SetTogglesValue(false);
        }

        internal void SetFarmCard(UIMonsterFarmCard farmCard) => this.farmCard = farmCard;
        #endregion

        private bool ComparePara(BasicParameters paraA, BasicParameters paraB)
        {
            if (paraA.health != paraB.health) { return true; }
            if (paraA.strenght != paraB.strenght) { return true; }
            if (paraA.intelligent != paraB.intelligent) { return true; }
            if (paraA.dexterity != paraB.dexterity) { return true; }
            if (paraA.agility != paraB.agility) { return true; }
            if (paraA.vitality != paraB.vitality) { return true; }
            return false;
        }

        public void PlayText() => battleStatsComponents.UpdateActionStatAnim(monsterInfo.lastMonsterAction.basicParameters, complexBasicParameters, 0);

        internal void SetSkillLearn(string learnSkill) => monsterSelectAction.trainingSkill = learnSkill;

        private void SetPreivousMeal() => mealSelectUI.SetMeal(monsterInfo.lastFoodType);
    }
}