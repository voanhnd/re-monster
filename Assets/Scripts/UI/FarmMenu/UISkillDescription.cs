﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UISkillDescription : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI skillName;
        [SerializeField] private TextMeshProUGUI skillDescription;
        [SerializeField] private Image skillGraphic;

        public void SetSkillInfo(string skillName, string skillDedscription, Sprite skillSprite)
        {
            this.skillName.text = skillName;
            this.skillDescription.text = skillDedscription;
            this.skillGraphic.sprite = skillSprite;
        }
    }
}