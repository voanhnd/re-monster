﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIMonsterFarmConditionAnimationControl : MonoBehaviour
    {
        [SerializeField] private Animator monsterAnimator;

        private void Awake()
        {
            monsterAnimator = GetComponent<Animator>();
        }
    }
}