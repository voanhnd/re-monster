﻿using Assets.Scripts.Monster;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UILoadDataFarm : MonoBehaviour
    {
        [SerializeField] private UIFarmMenu farmMenu;
        [SerializeField] private UIMonsterFarmDetails farmDetails;
        [SerializeField] private Sprite emptyFarmSprite;
        [SerializeField] private int maxFarmCount = 3;
        private List<UIMonsterFarmCard> farmCards = new();

        private void Start()
        {
            CreateFarmCard();
        }

        private void CreateFarmCard()
        {
            List<PlayerInfo.PlayerFarmInfo> farmInfoData = PlayerStats.Instance.playerInfo.PlayerFarmInfos;
            int farmCount = Mathf.Min(farmInfoData.Count, maxFarmCount);
            for (int i = 0; i < farmCount; i++)
            {
                int farmIndex = i;
                if (farmInfoData[farmIndex].usingFarmType == Farm.FarmType.Empty)
                {
                    farmMenu.AddCardData(farmInfoData[farmIndex].farmId);
                    continue;
                }

                FarmDataScriptObj farmScriptable = GetFarmData(farmInfoData[farmIndex].usingFarmType);
                PlayerInfo.PlayerMonsterInfo monsterInfo = GetMonsterInfoByFarm(farmInfoData[farmIndex].farmId);

                //set detail view
                if (monsterInfo != null)
                {
                    if (string.IsNullOrEmpty(monsterInfo.usingFarmID))
                    {
                        farmMenu.AddCardData(farmScriptable.Farm.FarmName, farmScriptable.Farm.FarmBackgroundSprite, farmInfoData[farmIndex].farmId);
                        continue;
                    }
                    SetFarmData(farmInfoData, farmIndex, farmScriptable, monsterInfo.monsterID);
                    continue;
                }
                //add farm card view data
                farmMenu.AddCardData(farmScriptable.Farm.FarmName, farmScriptable.Farm.FarmBackgroundSprite, farmInfoData[farmIndex].farmId);
            }
            farmCards.AddRange(farmMenu.MonsterCards);
            farmDetails.OnMenuClose.AddListener(RefreshCardsData);
        }

        private void SetFarmData(List<PlayerInfo.PlayerFarmInfo> farmInfoData, int index, FarmDataScriptObj farmScriptable, string monsterId)
        {
            PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            MonsterDataScriptObj monsterData = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);

            BasicParameters complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, monsterData.Monster.basicParameters);
            farmMenu.AddCardData(monsterInfo.monsterID,
                                 monsterData.Monster.monsterName, monsterInfo.rankType,
                                 farmScriptable.Farm.FarmName, farmScriptable.Farm.FarmBackgroundSprite,
                                 100, monsterInfo.growthParameters.fatigue,
                                 100, monsterInfo.growthParameters.stress,
                                 100, monsterInfo.growthParameters.bodyTypeValue,
                                 100, monsterInfo.growthParameters.affection,
                                 100, monsterInfo.growthParameters.trainingPolicyValue,
                                 999, complexBasicParameters.health,
                                 999, complexBasicParameters.strenght,
                                 999, complexBasicParameters.intelligent,
                                 999, complexBasicParameters.dexterity,
                                 999, complexBasicParameters.agility,
                                 999, complexBasicParameters.vitality,
                                 farmInfoData[index].farmId);
            farmMenu.UpdateModelView();
        }

        private FarmDataScriptObj GetFarmData(Farm.FarmType farm) => PlayerStats.Instance.farmDataManagerScriptObj.farmDataDictionary[farm];

        private PlayerInfo.PlayerMonsterInfo GetMonsterInfoByFarm(string farmId)
        {
            for (int i = 0; i < PlayerStats.Instance.playerInfo.PlayerMonsterInfos.Count; i++)
            {
                int listIndex = i;
                PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.PlayerMonsterInfos[listIndex];
                if (string.IsNullOrEmpty(monsterInfo.usingFarmID)) continue;
                if (monsterInfo.usingFarmID.CompareTo(farmId) == 0)
                {
                    return monsterInfo;
                }
            }
            return null;
        }

        private void RefreshCardsData()
        {
            List<PlayerInfo.PlayerFarmInfo> farmInfoData = PlayerStats.Instance.playerInfo.PlayerFarmInfos;
            for (int i = 0; i < farmCards.Count; i++)
            {
                if (string.IsNullOrEmpty(farmCards[i].MonsterName)) continue;
                int farmIndex = i;
                FarmDataScriptObj farmScriptable = GetFarmData(farmInfoData[farmIndex].usingFarmType);
                PlayerInfo.PlayerMonsterInfo monsterInfo = GetMonsterInfoByFarm(farmCards[farmIndex].FarmId);
                MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);

                BasicParameters complexBasicParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo.basicParameters, monsterScriptable.Monster.basicParameters);

                farmCards[i].SetDetailView(monsterScriptable.Monster.monsterName, monsterInfo.rankType,
                                 farmScriptable.Farm.FarmName, farmScriptable.Farm.FarmBackgroundSprite,
                                 100, monsterInfo.growthParameters.fatigue,
                                 100, monsterInfo.growthParameters.stress,
                                 100, monsterInfo.growthParameters.bodyTypeValue,
                                 100, monsterInfo.growthParameters.affection,
                                 100, monsterInfo.growthParameters.trainingPolicyValue,
                                 999, complexBasicParameters.health,
                                 999, complexBasicParameters.strenght,
                                 999, complexBasicParameters.intelligent ,
                                 999, complexBasicParameters.dexterity,
                                 999, complexBasicParameters.agility,
                                 999, complexBasicParameters.vitality,
                                 farmCards[i].FarmId);
            }
        }
    }
}