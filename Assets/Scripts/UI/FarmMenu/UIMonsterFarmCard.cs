﻿using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIMonsterFarmCard : MonoBehaviour
    {
        [SerializeField] private List<RectTransform> monsterStatPage;
        [SerializeField] private UIHorizontalScroll horizontalPageScroll;

        [Header("Functional buttons")]
        [SerializeField] private Button currentMonsterBtn;
        [SerializeField] private Button currentLocationBtn;
        [SerializeField] private Button addMonsterBtn;
        [SerializeField] private Button addFarmSlotBtn;
        [SerializeField] private Button removeMonsterBtn;
        [SerializeField] private Button monsterDetailBtn;
        [SerializeField] private Button changeMonsterBtn;

        [Header("UI components")]
        //[SerializeField] private TextMeshProUGUI monsterRankText;
        [SerializeField] private TextMeshProUGUI monsterNameText;
        [SerializeField] private TextMeshProUGUI farmLocationText;
        [SerializeField] private Image farmBackgroundImage;
        [SerializeField] private Image monsterRankImage;
        [SerializeField] private RawImage monsterImageRender;
        [Header("Sliders")]
        [Header("Training P")]
        [SerializeField] private Slider fatigueSlider;
        [SerializeField] private Slider stressSlider;
        [SerializeField] private Slider typeSlider;
        [SerializeField] private Slider loveSlider;
        [SerializeField] private Slider policySlider;
        [Header("Basic P")]
        [SerializeField] private Slider hpSlider;
        [SerializeField] private Slider strSlider;
        [SerializeField] private Slider intSlider;
        [SerializeField] private Slider dexSlider;
        [SerializeField] private Slider agiSlider;
        [SerializeField] private Slider vitSlider;

        [Header("Active objects")]
        [SerializeField] private List<GameObject> monsterFarmViewItems = new();
        [SerializeField] private List<GameObject> farmViewItems = new();
        [SerializeField] private List<GameObject> emptyViewItems = new();

        [Header("Deactive objects")]
        [SerializeField] private List<GameObject> monsterFarmViewDisable = new();
        [SerializeField] private List<GameObject> farmViewDisable = new();
        [SerializeField] private List<GameObject> emptyViewDisable = new();

        private string monsterName;
        private string farmId;
        private string farmName;
        private Sprite farmSprite;

        public Button CurrentMonsterBtn => currentMonsterBtn;
        public Button CurrentLocationBtn => currentLocationBtn;
        public Button AddMonsterBtn => addMonsterBtn;
        public Button AddFarmSlotBtn => addFarmSlotBtn;
        public RawImage MonsterImageRender => monsterImageRender;
        public string FarmId => farmId;
        public string FarmName => farmName;
        public string MonsterName => monsterName;
        public Sprite FarmSprite => farmSprite;

        public Button RemoveMonsterBtn => removeMonsterBtn;
        public Button MonsterDetailBtn => monsterDetailBtn;
        public Button ChangeMonsterBtn => changeMonsterBtn;

        #region Set farm view
        public void SetDetailView(string monsterName, RankTypeEnums monsterRank, string currentFarmLocation,
                                  Sprite backgroundScreenshot,
                                  float fatigueMaxValue, float fatigueValue,
                                  float stressMaxValue, float stressValue,
                                  float typeMaxValue, float typeValue,
                                  float loveMaxValue, float loveValue,
                                  float policyMaxValue, float policyValue,
                                  float hpMaxValue, float hpValue,
                                  float strMaxValue, float strValue,
                                  float intMaxValue, float intValue,
                                  float dexMaxValue, float dexValue,
                                  float agiMaxValue, float agiValue,
                                  float vitMaxValue, float vitValue,
                                  string farmId)
        {
            monsterRankImage.sprite = RankText.Instance.GetRank(monsterRank);
            monsterNameText.text = monsterName;
            this.monsterName = monsterName;

            this.farmId = farmId;
            this.farmName = currentFarmLocation;
            this.farmSprite = backgroundScreenshot;
            farmLocationText.text = currentFarmLocation;

            if (backgroundScreenshot != null)
            {
                farmBackgroundImage.sprite = backgroundScreenshot;
            }

            for (int i = 0; i < monsterFarmViewDisable.Count; i++)
            {
                monsterFarmViewDisable[i].SetActive(false);
            }

            for (int i = 0; i < monsterFarmViewItems.Count; i++)
            {
                monsterFarmViewItems[i].SetActive(true);
            }

            fatigueSlider.maxValue = fatigueMaxValue;
            fatigueSlider.value = fatigueValue;
            stressSlider.maxValue = stressMaxValue;
            stressSlider.value = stressValue;
            typeSlider.maxValue = typeMaxValue;
            typeSlider.value = typeValue;
            loveSlider.maxValue = loveMaxValue;
            loveSlider.value = loveValue;
            policySlider.maxValue = policyMaxValue;
            policySlider.value = policyValue;

            hpSlider.maxValue = hpMaxValue;
            hpSlider.value = hpValue;
            strSlider.maxValue = strMaxValue;
            strSlider.value = strValue;
            intSlider.maxValue = intMaxValue;
            intSlider.value = intValue;
            dexSlider.maxValue = dexMaxValue;
            dexSlider.value = dexValue;
            agiSlider.maxValue = agiMaxValue;
            agiSlider.value = agiValue;
            vitSlider.maxValue = vitMaxValue;
            vitSlider.value = vitValue;
        }

        public void SetAddView(Sprite farmSprite, string farmLocation, string farmId)
        {
            this.farmId = farmId;
            monsterName = string.Empty;
            farmLocationText.text = farmLocation;
            farmBackgroundImage.sprite = farmSprite;

            this.farmName = farmLocation;
            this.farmSprite = farmSprite;
            for (int i = 0; i < farmViewDisable.Count; i++)
            {
                farmViewDisable[i].SetActive(false);
            }
            for (int i = 0; i < farmViewItems.Count; i++)
            {
                farmViewItems[i].SetActive(true);
            }
        }

        public void SetEmptyView(string farmId)
        {
            this.farmId = farmId;
            monsterName = string.Empty;
            for (int i = 0; i < emptyViewDisable.Count; i++)
            {
                emptyViewDisable[i].SetActive(false);
            }
            for (int i = 0; i < emptyViewItems.Count; i++)
            {
                emptyViewItems[i].SetActive(true);
            }
        }
        #endregion

        public void SetTextureView(Texture renderTexture)
        {
            monsterImageRender.texture = renderTexture;
        }
    }
}