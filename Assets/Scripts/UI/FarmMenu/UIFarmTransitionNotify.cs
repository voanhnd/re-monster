﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.HomeMenu;
using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI.FarmMenu
{
    public class UIFarmTransitionNotify : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timeSpentText;
        [SerializeField] private TextMeshProUGUI actionSelectText;
        [SerializeField] private TextMeshProUGUI actionTimeText;

        [SerializeField] private TextMeshProUGUI successDetailText;
        [SerializeField] private TextMeshProUGUI detailDataText;
        [SerializeField] private RectTransform backgroundContainerRect;
        [Header("Requirements menu")]
        [SerializeField] private UIMonsterFarmDetails farmDetail;
        [SerializeField] private UIBasicTrainningToggle basicTrainingSelect;
        [SerializeField] private List<UIGameDate> gameDateUpdate = new();

        [Header("Animation")]
        [Header("Background")]
        [SerializeField] private float backgroundOpenTime;
        [SerializeField] private float backgroundCloseTime;
        [SerializeField] private float backgroundDelayOpenTime;
        [SerializeField] private float backgroundDelayCloseTime;
        [SerializeField] private Ease backgroundOpenEase;
        [SerializeField] private Ease backgroundCloseEase;
        [SerializeField] private Vector2 backgroundOpenSize;
        [SerializeField] private Vector2 backgroundCloseSize;

        [Header("Time pass")]
        [SerializeField] private float timeSpentOpenTime;
        [SerializeField] private float timeSpentCloseTime;
        [SerializeField] private float timeSpentDelayOpenTime;
        [SerializeField] private float timeSpentDelayCloseTime;
        [SerializeField] private Ease timeSpentOpenEase;
        [SerializeField] private Ease timeSpentCloseEase;
        [SerializeField] private Color timeSpentOpenColor;
        [SerializeField] private Color timeSpentCloseColor;

        [Header("Action select")]
        [SerializeField] private float actionOpenTime;
        [SerializeField] private float actionCloseTime;
        [SerializeField] private float actionDelayOpenTime;
        [SerializeField] private float actionDelayCloseTime;
        [SerializeField] private Ease actionOpenEase;
        [SerializeField] private Ease actionCloseEase;
        [SerializeField] private Color actionOpenColor;
        [SerializeField] private Color actionCloseColor;
        [SerializeField] private UnityEvent onAnimationDone;

        [Header("Success detail")]
        [SerializeField] private float successDetailOpenTime;
        [SerializeField] private float successDetailCloseTime;
        [SerializeField] private float successDetailDelayOpenTime;
        [SerializeField] private float successDetailDelayCloseTime;
        [SerializeField] private Ease successDetailOpenEase;
        [SerializeField] private Ease successDetailCloseEase;
        [SerializeField] private Color successDetailOpenColor;
        [SerializeField] private Color successDetailCloseColor;

        [Header("Extra detail")]
        [SerializeField] private float extraDetailOpenTime;
        [SerializeField] private float extraDetailCloseTime;
        [SerializeField] private float extraDetailDelayOpenTime;
        [SerializeField] private float extraDetailDelayCloseTime;
        [SerializeField] private Ease extraDetailOpenEase;
        [SerializeField] private Ease extraDetailCloseEase;
        [SerializeField] private Color extraDetailOpenColor;
        [SerializeField] private Color extraDetailCloseColor;

        [SerializeField] private UITrainingResult resultTraining;
        private string loadFarmId = string.Empty;
        private string loadMonsterId = string.Empty;
        private string extraResult = string.Empty;
        public UnityEvent OnAnimationDone { get => onAnimationDone; set => onAnimationDone = value; }

        private void Start()
        {
            actionSelectText.color = actionCloseColor;
            timeSpentText.color = timeSpentCloseColor;
            backgroundContainerRect.pivot = new Vector2(0, 0.5f);
            backgroundContainerRect.sizeDelta = backgroundCloseSize;
            farmDetail.OnAccecpt.AddListener(() =>
            {
                switch (farmDetail.MonsterSelectAction.monsterActionType)
                {
                    case MonsterAction.MonsterActionType.BasicTraining:
                        SetNotify(1, $"Basic training ({farmDetail.MonsterSelectAction.basicTrainingMainParameter})");
                        SetAnimationIn();
                        break;
                    case MonsterAction.MonsterActionType.SpecialTraining:
                        SetNotify(1, "Special training");
                        SetAnimationIn();
                        break;
                    case MonsterAction.MonsterActionType.Rest:
                        SetNotify(1, farmDetail.MonsterSelectAction.monsterActionType);
                        SetAnimationIn();
                        break;
                    case MonsterAction.MonsterActionType.Hospital:
                        SetNotify(1, farmDetail.MonsterSelectAction.monsterActionType);
                        SetAnimationIn();
                        break;
                    case MonsterAction.MonsterActionType.IntensiveTrainning:
                        SetNotify(0, PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(farmDetail.MonsterSelectAction.trainingAreaType).TrainingArea.areaName, "Intensive Training");
                        SetAnimationIn();
                        break;
                    case MonsterAction.MonsterActionType.Exploration:
                        SetNotify(0, PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(farmDetail.MonsterSelectAction.trainingAreaType).TrainingArea.areaName, "Exploration");
                        SetAnimationIn();
                        break;
                }
            });
            gameObject.SetActive(false);
        }

        #region Notify set
        private void SetNotify(float timeSpent, string areaName, string trainingType)
        {
            timeSpentText.text = $"{Mathf.RoundToInt(timeSpent)}";
            actionTimeText.text = " Weeks later";
            actionSelectText.text = $"{trainingType} [{areaName}]";
        }

        private void SetNotify(float timeSpent, ParameterType trainingParameter, MonsterAction.MonsterActionType actionType)
        {
            timeSpentText.text = $"{Mathf.RoundToInt(timeSpent)}";
            actionTimeText.text = " Week later";
            actionSelectText.text = $"{actionType.ToString().ToLower()} [{trainingParameter.ToString().ToLower()}]";
        }

        private void SetNotify(float timeSpent, MonsterAction.MonsterActionType actionType)
        {
            if (timeSpent != 1)
            {
                timeSpentText.text = $"{Mathf.RoundToInt(timeSpent)}";
                actionTimeText.text = " Weeks later";
            }
            else
            {
                timeSpentText.text = $"{Mathf.RoundToInt(timeSpent)}";
                actionTimeText.text = " Week later";
            }

            actionSelectText.text = $"{actionType.ToString().ToLower()}";
        }

        private void SetNotify(float timeSpent, string notifyString)
        {
            timeSpentText.text = $"{Mathf.RoundToInt(timeSpent)}";
            actionTimeText.text = " Week later";
            actionSelectText.text = notifyString;
        }
        #endregion

        #region Animations
        private void SetAnimationIn()
        {
            gameObject.SetActive(true);
            backgroundContainerRect.pivot = new Vector2(0, 0.5f);
            backgroundContainerRect.anchoredPosition = new Vector2(-960, 0);
            Sequence animationIn = DOTween.Sequence();
            loadFarmId = farmDetail.FarmId;
            loadMonsterId = farmDetail.MonsterId;

            animationIn.Append(backgroundContainerRect.DOSizeDelta(backgroundOpenSize, backgroundOpenTime).SetDelay(backgroundDelayOpenTime).SetEase(backgroundOpenEase).OnComplete(() => farmDetail.CloseMenu()))
                       .Insert(backgroundDelayOpenTime + backgroundOpenTime, timeSpentText.DOColor(timeSpentOpenColor, timeSpentOpenTime).SetDelay(timeSpentDelayOpenTime).SetEase(timeSpentOpenEase))
                       .Insert(backgroundDelayOpenTime + backgroundOpenTime, actionTimeText.DOColor(timeSpentOpenColor, timeSpentOpenTime).SetDelay(timeSpentDelayOpenTime).SetEase(timeSpentOpenEase))
                       .Insert(backgroundDelayOpenTime + backgroundOpenTime, actionSelectText.DOColor(actionOpenColor, actionOpenTime).SetDelay(actionDelayOpenTime).SetEase(actionOpenEase)).OnComplete(() =>
                       {
                           SetAnimationOut();
                       });
            extraResult = string.Empty;
            detailDataText.text = string.Empty;
            successDetailText.text = string.Empty;
            detailDataText.DOColor(extraDetailOpenColor, extraDetailOpenTime).SetDelay(extraDetailDelayOpenTime).SetEase(extraDetailOpenEase);
            successDetailText.DOColor(successDetailOpenColor, successDetailOpenTime).SetDelay(successDetailDelayOpenTime).SetEase(successDetailOpenEase);
        }

        private void SetAnimationOut()
        {
            backgroundContainerRect.pivot = new Vector2(1, 0.5f);
            backgroundContainerRect.anchoredPosition = new Vector2(960, 0);
            float timeSpent = farmDetail.MonsterInfoData.lastMonsterAction.weekPassed + farmDetail.MonsterInfoData.lastMonsterAction.lostWeek;
            for (int i = 0; i < gameDateUpdate.Count; i++)
                gameDateUpdate[i].AddWeekTime((int)timeSpent);
            ShowSuccessResult();
            ShowExtraData(farmDetail.MonsterInfoData.lastMonsterAction.growthParameters.injuryType, farmDetail.MonsterInfoData.lastMonsterAction.growthParameters.diseasesType, farmDetail.MonsterInfoData.lastMonsterAction.lostWeek, farmDetail.MonsterInfoData.growthParameters.injuryType, farmDetail.MonsterInfoData.growthParameters.diseasesType);
            if (timeSpent != 1)
            {
                actionTimeText.DOText(" Weeks later", 1);
                timeSpentText.DOText($"{farmDetail.MonsterInfoData.lastMonsterAction.weekPassed}", 1, true, ScrambleMode.Numerals).OnComplete(() =>
                {
                    ShowTimeSpent();
                    CloseAnimation();
                });
                return;
            }
            ShowTimeSpent();
            CloseAnimation();
        }

        private void CloseAnimation()
        {
            backgroundContainerRect.DOSizeDelta(backgroundCloseSize, backgroundCloseTime)
                                                         .SetDelay(backgroundDelayCloseTime)
                                                         .SetEase(backgroundCloseEase).OnComplete(() =>
                                                         {
                                                             //resultTraining.OpenMenu();
                                                             gameObject.SetActive(false);
                                                             OnAnimationDone.Invoke();
                                                         });
        }

        private void ShowTimeSpent()
        {
            Sequence animationOut = DOTween.Sequence();
            animationOut.Append(timeSpentText.DOColor(timeSpentCloseColor, timeSpentCloseTime)
                                             .SetDelay(timeSpentDelayCloseTime).SetEase(timeSpentCloseEase)
                                             .OnComplete(() => { farmDetail.LoadMonsterData(loadMonsterId, loadFarmId); farmDetail.OpenMenu(); }))
                        .Insert(0, actionSelectText.DOColor(timeSpentCloseColor, actionCloseTime)
                                                   .SetDelay(actionDelayCloseTime).SetEase(actionCloseEase))
                        .Insert(0, actionTimeText.DOColor(timeSpentCloseColor, actionCloseTime)
                                                   .SetDelay(actionDelayCloseTime).SetEase(actionCloseEase));
        }

        private void ShowExtraData(Monster.Monster.InjuryType injuryType, Monster.Monster.DiseasesType diseasesType, int lostWeek, Monster.Monster.InjuryType currentInjury, Monster.Monster.DiseasesType currentDiseases)
        {
            string extraDetail = string.Empty;
            switch (currentInjury)
            {
                case Monster.Monster.InjuryType.Injury:
                    if (injuryType == Monster.Monster.InjuryType.Injury)
                        extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is Injured.\n";
                    else
                        extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is Injuring.\n";
                    break;
                case Monster.Monster.InjuryType.Serious_Injury:
                    extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is seriously injured.\n";
                    break;
            }
            switch (currentDiseases)
            {
                case Monster.Monster.DiseasesType.Diseases:
                    if (diseasesType == Monster.Monster.DiseasesType.Diseases)
                        extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is sick.\n";
                    else
                        extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is sickening\n";
                    break;
                case Monster.Monster.DiseasesType.Serious_Diseases:
                    extraDetail += $"{farmDetail.MonsterInfoData.monsterName}'s is seriously sick.\n";
                    break;
            }

            this.extraResult += extraDetail;
            detailDataText.DOText(extraResult, extraDetailOpenTime, true, ScrambleMode.All);

            detailDataText.DOColor(extraDetailCloseColor, extraDetailCloseTime).SetDelay(extraDetailDelayCloseTime).SetEase(extraDetailCloseEase);
        }

        private void ShowSuccessResult()
        {
            switch (farmDetail.MonsterInfoData.lastMonsterAction.monsterActionType)
            {
                case MonsterAction.MonsterActionType.BasicTraining:
                    SetSuccessActionText("Monster basic training success", "Monster basic training huge success", "Monster basic training failed", farmDetail.MonsterInfoData.lastMonsterAction.trainingType);
                    break;
                case MonsterAction.MonsterActionType.SpecialTraining:
                    SetSuccessActionText("Monster special training success", "Monster special training huge success", "Monster special training failed", farmDetail.MonsterInfoData.lastMonsterAction.trainingType);
                    break;
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    CheckIntensiveSkill();
                    SetSuccessActionText("Monster intensive training success", "Monster intensive training huge success", "Monster intensive training failed", farmDetail.MonsterInfoData.lastMonsterAction.trainingType);
                    break;
                case MonsterAction.MonsterActionType.Exploration:
                    SetSuccessActionText("Monster exploration success", "Monster exploration success", "Monster exploration failed", farmDetail.MonsterInfoData.lastMonsterAction.trainingType);
                    break;
                case MonsterAction.MonsterActionType.Hospital:
                    successDetailText.DOText("Monster is cured", successDetailOpenTime, true, ScrambleMode.All);
                    break;
            }
            successDetailText.DOColor(successDetailCloseColor, successDetailCloseTime).SetDelay(successDetailDelayCloseTime).SetEase(successDetailCloseEase);
        }

        private void SetSuccessActionText(string successString, string hugeSuccessString, string failString, TrainingArea.TrainingArea.TrainingType successType)
        {
            switch (successType)
            {
                case TrainingArea.TrainingArea.TrainingType.success:
                    successDetailText.DOText(successString, successDetailOpenTime, true, ScrambleMode.All);
                    break;
                case TrainingArea.TrainingArea.TrainingType.huge_success:
                    successDetailText.DOText(hugeSuccessString, successDetailOpenTime, true, ScrambleMode.All);
                    break;
                case TrainingArea.TrainingArea.TrainingType.failure:
                    successDetailText.DOText(failString, successDetailOpenTime, true, ScrambleMode.All);
                    break;
            }
        }

        #endregion

        private void CheckIntensiveSkill()
        {
            PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(loadMonsterId);
            if (monsterInfo.lastMonsterAction.skillUp)
            {
                SkillDataScriptObj skillScriptable = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(monsterInfo.lastMonsterAction.trainingSkill);
                if (skillScriptable == null) return;
                for (int i = 0; i < monsterInfo.MonsterSkillInfos.Count; i++)
                {
                    if (!Equals(monsterInfo.MonsterSkillInfos[i].skillID, monsterInfo.lastMonsterAction.trainingSkill)) continue;
                    extraResult += $"{skillScriptable.Skill.skillName} level up to level {monsterInfo.MonsterSkillInfos[i].skillLevel}\n";
                    return;
                }
            }
        }
    }
}