﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.UI.TweenCustomize
{
    public class SingleTween : MonoBehaviour
    {
        private Transform transformTarget;
        private Vector3 originalScale;
        private Vector3 originalPos;
        [SerializeField] private TweenValues tweenValues;

        private void Awake()
        {
            transformTarget = transform;
            originalPos = transform.position;
            originalScale = transformTarget.localScale;
        }

        public void ActivateTweenIn()
        {
            switch (tweenValues.Type)
            {
                case TweenType.Scale:
                    if (tweenValues.IsLoop)
                        transformTarget.DOScale(tweenValues.ScaleTarget, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOScale(tweenValues.ScaleTarget, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn);
                    break;
                case TweenType.Position:
                    Vector3 movePos = tweenValues.TargetTrans ? tweenValues.TargetTrans.position : tweenValues.TargetPos;
                    if (tweenValues.IsLoop)
                        transformTarget.DOMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn);
                    break;
            }
        }
        public void ActivateTweenOut()
        {
            switch (tweenValues.Type)
            {
                case TweenType.Scale:
                    if (tweenValues.IsLoop)
                        transformTarget.DOScale(originalScale, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOScale(originalScale, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut);
                    break;
                case TweenType.Position:
                    if (tweenValues.IsLoop)
                        transformTarget.DOMove(originalPos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOMove(originalPos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut);
                    break;
            }
        }
    }
}