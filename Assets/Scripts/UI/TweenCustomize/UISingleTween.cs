﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.UI.TweenCustomize
{
    [RequireComponent(typeof(RectTransform))]
    public class UISingleTween : MonoBehaviour
    {
        private Transform transformTarget;
        private RectTransform rectTransformTarget;
        private Vector3 originalScale;
        private Vector3 originalPos;
        [SerializeField] private TweenValues tweenValues;
        [SerializeField] private bool resetToOriginScale = true;
        [SerializeField] private bool resetToOriginPos = true;

        private void Awake()
        {
            transformTarget = transform;
            rectTransformTarget = GetComponent<RectTransform>();
            originalPos = rectTransformTarget.position;
            originalScale = transformTarget.localScale;
        }

        private void OnEnable()
        {
            if (tweenValues.IsPlayOnAwake)
            {
                ActivateTweenIn();
            }
        }

        private void OnDisable()
        {
            transformTarget.DOKill();
            if (resetToOriginScale)
                transformTarget.localScale = originalScale;
            if (resetToOriginPos)
                rectTransformTarget.position = originalPos;
        }

        public void ActivateTweenIn()
        {
            switch (tweenValues.Type)
            {
                case TweenType.Scale:
                    if (tweenValues.IsLoop)
                        transformTarget.DOScale(tweenValues.ScaleTarget, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOScale(tweenValues.ScaleTarget, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn);
                    break;
                case TweenType.Position:
                    Vector3 movePos = (tweenValues.TargetTrans || !tweenValues.IsLocalSet) ? tweenValues.TargetTrans.position : tweenValues.TargetPos;
                    if (tweenValues.IsLocalSet)
                    {
                        movePos += transformTarget.localPosition;
                    }
                    if (tweenValues.IsLoop)
                    {
                        if (tweenValues.IsLocalSet)
                            transformTarget.DOLocalMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                        else
                            transformTarget.DOMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    }
                    else
                    {
                        if (tweenValues.IsLocalSet)
                            transformTarget.DOLocalMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn);
                        else
                            transformTarget.DOMove(movePos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseIn);
                    }
                    break;
            }
        }
        public void ActivateTweenOut()
        {
            switch (tweenValues.Type)
            {
                case TweenType.Scale:
                    if (tweenValues.IsLoop)
                        transformTarget.DOScale(originalScale, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOScale(originalScale, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut);
                    break;
                case TweenType.Position:
                    if (tweenValues.IsLoop)
                        transformTarget.DOMove(originalPos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut).SetLoops(tweenValues.LoopTime, tweenValues.TweenLoopType);
                    else
                        transformTarget.DOMove(originalPos, tweenValues.Duration).SetDelay(tweenValues.Delay).SetEase(tweenValues.TweenEaseOut);
                    break;
            }
        }
    }
}