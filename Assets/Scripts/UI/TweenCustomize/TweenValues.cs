﻿using DG.Tweening;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.TweenCustomize
{
    [Serializable]
    public class TweenValues
    {
        [SerializeField] private TweenType type;
        [SerializeField] private float duration;
        [SerializeField] private float delay;
        [SerializeField] private int loopTime = -1;
        [SerializeField] private Ease tweenEaseIn;
        [SerializeField] private Ease tweenEaseOut;
        [SerializeField] private LoopType tweenLoopType;
        [SerializeField] private bool isLoop;
        [SerializeField] private bool isPlayOnAwake = false;
        [Header("Remove target trans on local move")]
        [SerializeField] private bool isLocalSet = false;
        [SerializeField] private Vector3 scaleTarget;
        [SerializeField] private Vector3 targetPos;
        [SerializeField] private Transform targetTrans;

        public TweenType Type => type;
        public float Duration => duration;
        public float Delay => delay;
        public int LoopTime => loopTime;
        public Ease TweenEaseIn => tweenEaseIn;
        public Ease TweenEaseOut => tweenEaseOut;
        public LoopType TweenLoopType => tweenLoopType;
        public bool IsLoop => isLoop;
        public bool IsPlayOnAwake => isPlayOnAwake;
        public bool IsLocalSet => isLocalSet;
        public Vector3 TargetPos => targetPos;
        public Vector3 ScaleTarget => scaleTarget;
        public Transform TargetTrans => targetTrans;

    }

    public enum TweenType
    {
        Scale,
        Position
    }
}