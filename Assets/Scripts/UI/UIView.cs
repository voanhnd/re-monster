using UnityEngine;

public abstract class UIView : MonoBehaviour
{
    public bool activated { get; private set; }

    public bool onEnable = false;

    private void OnEnable()
    {
        onEnable = true;
    }

    private void OnDisable()
    {
        Deactive();
    }

    private void Update()
    {
        if (onEnable)
        {
            onEnable = false;

            Active();
        }
    }

    public virtual void Active()
    {
        activated = true;
    }

    public virtual void Deactive()
    {
        activated = false;
    }
}
