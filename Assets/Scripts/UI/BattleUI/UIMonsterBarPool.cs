﻿using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIMonsterBarPool : MonoBehaviour
    {
        private IObjectPool<UIMonsterBattleBar> pool;
        [SerializeField] private PoolType poolType;
        [SerializeField] private GameObject objectPrefab;
        [SerializeField] private int maxPoolCapital = 6;
        [SerializeField] private bool isCollectionCheckOn = true;

        public IObjectPool<UIMonsterBattleBar> Pool
        {
            get
            {
                if (pool == null)
                {
                    if (poolType == PoolType.Stack)
                        pool = new ObjectPool<UIMonsterBattleBar>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, 10, maxPoolCapital);
                    else
                        pool = new LinkedPool<UIMonsterBattleBar>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, maxPoolCapital);
                }
                return pool;
            }
        }

        private void OnDestroyFromPool(UIMonsterBattleBar obj)
        {
            Destroy(obj.gameObject);
        }

        private void OnReturnToPool(UIMonsterBattleBar obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void OnTakenFromPool(UIMonsterBattleBar obj)
        {
            obj.gameObject.SetActive(true);
        }

        private UIMonsterBattleBar CreatePoolItem()
        {
            GameObject spawnObject = Instantiate(this.objectPrefab, transform);

            UIMonsterBattleBar uiMonsterBar = spawnObject.GetComponent<UIMonsterBattleBar>();

            uiMonsterBar.pool = pool;

            return uiMonsterBar;
        }
    }
}