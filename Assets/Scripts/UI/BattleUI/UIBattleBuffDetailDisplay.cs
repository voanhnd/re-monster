﻿using Assets.Scripts.Interfaces;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    [Serializable]
    public class UIBattleBuffDetailDisplay : MonoBehaviour, IRemoveablePoolObject
    {
        public IObjectPool<UIBattleBuffDetailDisplay> Pool { get; set; }
        [SerializeField] private TextMeshProUGUI buffnameText;
        [SerializeField] private TextMeshProUGUI buffTimeLeftNumText;
        [SerializeField] private TextMeshProUGUI buffDescriptionText;
        [SerializeField] private Image buffGraphicImage;

        public Action OnDispose { get; set; }

        public void SetBuffDetail(string buffName, int buffTime, string buffDescription, Sprite buffSprite)
        {
            if (buffTime <= 0)
            {
                buffTime = 1;
            }
            buffnameText.text = buffName;
            buffTimeLeftNumText.text = $"{buffTime} turns";
            buffDescriptionText.text = buffDescription;
            buffGraphicImage.sprite = buffSprite;
        }

        private void Awake() => OnDispose += ReturnToPool;

        private void OnDestroy() => OnDispose -= ReturnToPool;

        private void ReturnToPool() => Pool.Release(this);
    }
}