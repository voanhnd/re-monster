﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleMonsterResult : MonoBehaviour
    {
        [SerializeField] private Image monsterGraphic;
        [SerializeField] private Slider monsterResultSlider;
        [SerializeField] private TextMeshProUGUI monsterNameText;
        [SerializeField] private TextMeshProUGUI monsterResultText;
        [SerializeField] private TextMeshProUGUI monsterResultPercentText;
        [SerializeField] private float valueChangeAnimationTime;
        private GridUnit unit;
        private ParameterInGame inGamePara;

        public GridUnit Unit => unit;

        public ParameterInGame InGamePara => inGamePara;

        public void SetMonsterBasicData(Sprite monsterSprite, string monsterName, GridUnit unit)
        {
            monsterGraphic.sprite = monsterSprite;
            monsterNameText.text = monsterName;
            this.unit = unit;
            inGamePara = unit.GetComponent<ParameterInGame>();
        }

        public void SetMonsterValue(float maxValue, float value)
        {
            monsterResultSlider.maxValue = maxValue;
            if (float.IsNaN(value))
                value = 0;
            float percentValue = (value / maxValue) * 100;
            if (float.IsNaN(percentValue))
                percentValue = 0;
            monsterResultSlider.DOValue(value, valueChangeAnimationTime).SetEase(Ease.OutQuad);
            monsterResultText.DOText(Mathf.CeilToInt(value).ToString(), valueChangeAnimationTime, true, ScrambleMode.Numerals);
            monsterResultPercentText.DOText(percentValue.ToString("0.#"), valueChangeAnimationTime, true, ScrambleMode.Numerals);
        }
    }
}