﻿using Assets.Scripts.Monster.Skill;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleBuffDetail : MonoBehaviour
    {
        [SerializeField] private Transform buffContainer;
        [SerializeField] private Transform debuffContainer;
        [SerializeField] private Transform mentalbuffContainer;
        [SerializeField] private UIBattleBuffPool poolScript;
        private List<UIBattleBuffDetailDisplay> poolObjectList = new();

        public void SetMonsterBuff(ParameterInGame monsterParameter)
        {
            ClearData();
            List<InGameBuffDetail> buffsDetail = monsterParameter.inGameBuffDetails;
            List<InGameDeBuffDetail> debuffsDetail = monsterParameter.inGameDeBuffDetails;

            if (buffsDetail != null && buffsDetail.Count > 0)
                SetBuff(buffsDetail);
            if (debuffsDetail != null && debuffsDetail.Count > 0)
                SetDebuff(debuffsDetail);
        }

        private void SetBuff(List<InGameBuffDetail> buffs)
        {
            for (int i = 0; i < buffs.Count; i++)
            {
                UIBattleBuffDetailDisplay poolObject = poolScript.Pool.Get();
                poolObjectList.Add(poolObject);

                SkillBuffImageManagerScriptObj.BuffEffectsData buffDetailData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffEffectData(buffs[i].monsterSpecialBufferType);
                Transform container = buffDetailData.IsMentalBuff ? mentalbuffContainer : buffContainer;
                if (buffDetailData.IsSkillDependent)
                {
                    poolObject.SetBuffDetail(buffDetailData.BuffJPName, buffs[i].buffTurn, SkillBuffImageManagerScriptObj.SkillSummaryConvert(buffDetailData.BuffSummary, buffs[i].EffectValue), buffDetailData.BuffSprite);
                }
                else
                {
                    poolObject.SetBuffDetail(buffDetailData.BuffJPName, buffs[i].buffTurn, buffDetailData.BuffSummary, buffDetailData.BuffSprite);
                }
                poolObject.transform.SetParent(container, false);
            }
        }

        private void SetDebuff(List<InGameDeBuffDetail> debuffs)
        {
            for (int i = 0; i < debuffs.Count; i++)
            {
                UIBattleBuffDetailDisplay poolObject = poolScript.Pool.Get();
                SkillBuffImageManagerScriptObj.DebuffEffectsData debuffDetailData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDebuffEffectData(debuffs[i].monsterSpecialDeBufferType);
                Transform container = debuffDetailData.IsMentalDebuff ? mentalbuffContainer : debuffContainer;
                if (debuffDetailData.IsSkillDependent)
                {
                    poolObject.SetBuffDetail(debuffDetailData.DebuffJPName, debuffs[i].deBuffTurn, SkillBuffImageManagerScriptObj.SkillSummaryConvert(debuffDetailData.DebuffSummary, debuffs[i].EffectValue), debuffDetailData.DebuffSprite);
                }
                else
                {
                    poolObject.SetBuffDetail(debuffDetailData.DebuffJPName, debuffs[i].deBuffTurn, debuffDetailData.DebuffSummary, debuffDetailData.DebuffSprite);
                }
                poolObject.transform.SetParent(container, false);
                poolObjectList.Add(poolObject);
            }
        }

        private void ClearData()
        {
            for (int i = 0; i < poolObjectList.Count; i++)
            {
                poolObjectList[i].OnDispose?.Invoke();
            }
            poolObjectList.Clear();
        }
    }
}