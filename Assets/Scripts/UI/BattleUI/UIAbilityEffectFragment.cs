﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIAbilityEffectFragment : MonoBehaviour
    {
        [SerializeField] private Image effectImage;
        [SerializeField] private Image effectTypeImage;
        [SerializeField] private Sprite buffSprite;
        [SerializeField] private Sprite debuffSprite;
        [SerializeField] private Color buffColor = Color.green;
        [SerializeField] private Color debuffColor = Color.red;
        [SerializeField] private TextMeshProUGUI effectSuccessRateText;

        public void SetEffectFragment(Sprite effectSprite, bool isBuff)
        {
            effectImage.sprite = effectSprite;
            if(isBuff )
            {
                effectTypeImage.sprite = buffSprite;
                effectTypeImage.color = buffColor;
            }
            else
            {
                effectTypeImage.sprite = debuffSprite;
                effectTypeImage.color = debuffColor;
            }
        }

        public void SetEffectSuccessRate(string value)
        {
            effectSuccessRateText.text = value;
        }
    }
}