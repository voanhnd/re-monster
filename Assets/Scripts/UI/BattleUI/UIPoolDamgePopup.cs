﻿using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIPoolDamgePopup : MonoBehaviour
    {
        private IObjectPool<UIDamagePopup> pool;
        [SerializeField] private PoolType poolType;
        [SerializeField] private GameObject damagePopupPrefab;
        [SerializeField] private Transform prefabContainer;
        [SerializeField] private int maxPoolCapital = 6;
        private bool isCollectionCheckOn = true;

        #region Public variables
        public IObjectPool<UIDamagePopup> Pool
        {
            get
            {
                if (pool == null)
                {
                    if (poolType == PoolType.Stack)
                        pool = new ObjectPool<UIDamagePopup>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, 10, maxPoolCapital);
                    else
                        pool = new LinkedPool<UIDamagePopup>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, maxPoolCapital);
                }
                return pool;
            }
        }
        #endregion

        #region Pool controller
        public UIDamagePopup CreatePoolItem()
        {
            GameObject damgePopupObject = Instantiate(this.damagePopupPrefab, prefabContainer);

            UIDamagePopup damagePopup = damgePopupObject.GetComponent<UIDamagePopup>();

            UIReturnDamagePopupToPool returnObjectToPool = damgePopupObject.AddComponent<UIReturnDamagePopupToPool>();

            returnObjectToPool.pool = pool;

            return damagePopup;
        }

        public void OnDestroyFromPool(UIDamagePopup item)
        {
            Destroy(item.gameObject);
        }

        public void OnReturnToPool(UIDamagePopup item)
        {
            item.gameObject.SetActive(false);
        }

        public void OnTakenFromPool(UIDamagePopup item)
        {
            item.gameObject.SetActive(true);
        }
        #endregion
    }
    enum PoolType
    {
        Stack, Link
    }
}