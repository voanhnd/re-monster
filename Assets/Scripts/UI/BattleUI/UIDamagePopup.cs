﻿using Assets.Scripts.Interfaces;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIDamagePopup : MonoBehaviour, IRemoveablePoolObject
    {
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private RectTransform damageTextRect;
        [SerializeField] private CanvasGroup group;
        [SerializeField] private float randomMinX;
        [SerializeField] private float randomMaxX;
        [SerializeField] private float randomMinY;
        [SerializeField] private float randomMaxY;
        [SerializeField] private float popupJumpHeight;
        [SerializeField] private Vector3 scaleDownSize;
        private Transform cachedTransform;

        [Header("Animation timing")]
        [SerializeField] private float scaleUpTime;
        [SerializeField] private float scaleDownTime;
        [SerializeField] private float fadeOutTime;
        [SerializeField] private float outDelayTime;
        [SerializeField] private Ease easeIn;
        [SerializeField] private Ease easeOut;

        public Action OnDispose { get; set; }
        public Transform CachedTransform
        {
            get
            {
                if (cachedTransform == null)
                {
                    cachedTransform = transform;
                }
                return cachedTransform;
            }
        }

        private void PlayAnimation()
        {
            Vector2 currentAnchor = damageTextRect.anchoredPosition;
            group.alpha = 1f;
            damageTextRect.localScale = Vector3.zero;
            float randomStartX = currentAnchor.x + UnityEngine.Random.Range(randomMinX, randomMaxX);

            float randomStartY = currentAnchor.y + UnityEngine.Random.Range(randomMinY, randomMaxY);

            Sequence animationSeq = DOTween.Sequence();

            Vector2 spawnAnchor = new(randomStartX, randomStartY);
            damageTextRect.anchoredPosition = spawnAnchor;
            animationSeq.Append(damageTextRect.DOScale(Vector3.one, scaleUpTime).SetEase(easeIn))
                .Insert(0, damageTextRect.DOAnchorPos(spawnAnchor + new Vector2(0, popupJumpHeight), scaleUpTime).SetEase(easeIn))
                .Append(group.DOFade(0, fadeOutTime).SetEase(easeOut).SetDelay(outDelayTime))
                .Insert(scaleUpTime, damageTextRect.DOAnchorPos(spawnAnchor, scaleDownTime).SetDelay(outDelayTime).SetEase(easeOut))
                .Insert(scaleUpTime, damageTextRect.DOScale(scaleDownSize, scaleDownTime).SetDelay(outDelayTime).SetEase(easeOut))
                .OnKill(() =>
                {
                    OnDispose?.Invoke();
                });
        }

        public void SetData(string value, Color textColor, Vector3 canvasPos)
        {
            damageText.color = textColor;
            damageText.text = value;
            damageTextRect.position = canvasPos;
            transform.SetAsFirstSibling();
            PlayAnimation();
        }
    }
}