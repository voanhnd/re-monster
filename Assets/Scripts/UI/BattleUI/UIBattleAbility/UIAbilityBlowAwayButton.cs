using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIAbilityBlowAwayButton : MonoBehaviour
{
    float staminaCost = 10;

    public AbilityShape abilityShape;

    int abilityDistance = 1;

    int moveBackDistance = 1;

    bool waitToNextTurn = false;

    GridUnit m_SelectedUnit;

    ParameterInGame parameterInGame;

    public Transform lockBG;

    public Button blowAwayBtn;

    private void Start()
    {
        blowAwayBtn.onClick.AddListener(() => { BlowAwayButton(); });
    }

    void Update()
    {
        bool isActionBeingPerformed = GameManager.IsActionBeingPerformed();

        blowAwayBtn.interactable = !isActionBeingPerformed;

        if (isActionBeingPerformed == false && waitToNextTurn)
        {
            waitToNextTurn = false;

            CustomGameManager.Instance.SetNextTurn();
        }
    }

    public void CheckCost(GridUnit unit)
    {
        m_SelectedUnit = unit;

        parameterInGame = m_SelectedUnit.GetComponent<ParameterInGame>();

        waitToNextTurn = false;

        if (m_SelectedUnit != null && parameterInGame != null)
        {
            if (parameterInGame.GetCurrentStamina() >= staminaCost)
            {
                lockBG.gameObject.SetActive(false);
            }
            else
            {
                lockBG.gameObject.SetActive(true);
            }
        }
    }

    public void BlowAwayButton()
    {
        if (m_SelectedUnit == null || parameterInGame == null) return;

        Debug.Log("Blow Away button");

        StartCoroutine(ExecuteAbility());
    }

    IEnumerator ExecuteAbility()
    {
        if (m_SelectedUnit)
        {
            GameManager.AddActionBeingPerformed();

            UnitData unitData = m_SelectedUnit.GetUnitData();

            m_SelectedUnit.PlayAnimation(unitData.m_BlowAwayAnimation, true);

            AnimationClip AbilityAnimation = unitData.m_BlowAwayAnimation;

            yield return new WaitForSeconds(unitData.executeBlowAwayDelay);

            List<ILevelCell> AbilityCells = abilityShape.GetCellList(m_SelectedUnit, m_SelectedUnit.GetCell(), abilityDistance, false, GameTeam.Red);

            //List<ILevelCell> canotUseCellList = new List<ILevelCell>();

            foreach (ILevelCell cell in AbilityCells)
            {
                GridUnit unit = cell.GetUnitOnCell();

                if (unit != null && unit != m_SelectedUnit)
                {
                    ApplyTo(m_SelectedUnit, unit);
                }
            }

            parameterInGame.AddStamina(-staminaCost);

            float timeRemaining = (AbilityAnimation.length - unitData.executeBlowAwayDelay);
            timeRemaining = Mathf.Max(0, timeRemaining);

            yield return new WaitForSeconds(timeRemaining);

            waitToNextTurn = true;

            UIAccuracyRate.Instance.SetActive(false);

            GameManager.RemoveActionBeingPerformed();
        }

        yield break;
    }

    public ILevelCell ApplyTo(GridUnit InCaster, GridUnit TargetUnit)
    {
        if (TargetUnit)
        {
            Debug.Log("Blow away TargetUnit = " + TargetUnit.name);
        }

        if (TargetUnit && InCaster)
        {
            ILevelCell targetCell = TargetUnit.GetCell();
            Debug.Log("targetCell = " + targetCell.name);

            CompassDir PushDirection = GetDirectionToTarget(InCaster, InCaster.GetCell(), 1, true, GameTeam.Red, targetCell);
            Debug.Log("Push dir = " + PushDirection.ToString());

            ILevelCell dirCell = targetCell;

            Debug.Log("moveBackDistance = " + moveBackDistance);

            for (int i = 0; i < moveBackDistance; i++)
            {
                if (dirCell)
                {
                    Debug.Log("dirCell = " + dirCell.name);

                    dirCell = dirCell.GetAdjacentCell(PushDirection);
                }

                if (dirCell && dirCell.IsCellAccesible())
                {
                    targetCell = dirCell;
                }
            }

            if (targetCell.GetUnitOnCell() == null)
            {
                TargetUnit.MoveTo(targetCell);
            }

            return targetCell;
        }

        return null;
    }

    CompassDir GetDirectionToTarget(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam, ILevelCell targetCell)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InCell && InRange > 0)
        {
            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.N, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.N.ToString());
                return CompassDir.N;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.E.ToString());
                return CompassDir.E;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.NE.ToString());
                return CompassDir.NE;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.NW.ToString());
                return CompassDir.NW;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.SE.ToString());
                return CompassDir.SE;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.S, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.S.ToString());
                return CompassDir.S;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.SW.ToString());
                return CompassDir.SW;
            }

            cells.Clear();
            cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
            if (isTargetOnDirection(cells, targetCell))
            {
                Debug.Log("Target cell on direction = " + CompassDir.W.ToString());
                return CompassDir.W;
            }
        }

        Debug.Log("Can not caculate direction");

        return CompassDir.N;
    }

    List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
    {
        List<ILevelCell> cells = new List<ILevelCell>();

        if (InRange > 0)
        {
            ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

            int Count = 0;
            while (CurserCell)
            {
                if (CurserCell.IsBlocked() && !bAllowBlocked)
                {
                    break;
                }

                GridObject gridObj = CurserCell.GetObjectOnCell();
                if (gridObj)
                {
                    if (m_EffectedTeam == GameTeam.None)
                    {
                        break;
                    }

                    GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                    if (ObjAffinity == GameTeam.Blue && m_EffectedTeam == GameTeam.Red)
                    {
                        break;
                    }
                }

                cells.Add(CurserCell);
                CurserCell = CurserCell.GetAdjacentCell(Dir);
                Count++;
                if (InRange != -1 && Count >= InRange)
                {
                    break;
                }
            }
        }

        return cells;
    }

    bool isTargetOnDirection(List<ILevelCell> directionCellList, ILevelCell targetCell)
    {
        foreach (ILevelCell cell in directionCellList)
        {
            if (cell == targetCell)
            {
                Debug.Log("Target cell on direction cell = " + cell.name);

                return true;
            }
        }

        return false;
    }
}
