using Assets.Scripts.Monster;
using UnityEngine;
using UnityEngine.UI;

public class UIAbilityDefenseButton : MonoBehaviour
{
    float staminaCost = 5;

    GridUnit m_SelectedUnit;

    ParameterInGame parameterInGame;

    public Transform lockBG;

    public Button defbtn;

    private void Start()
    {
        defbtn.onClick.AddListener(() => { DefenseButton(); });
    }

    void Update()
    {
        defbtn.interactable = !GameManager.IsActionBeingPerformed();
    }

    public void CheckCost(GridUnit unit)
    {
        m_SelectedUnit = unit;

        parameterInGame = m_SelectedUnit.GetComponent<ParameterInGame>();

        if (m_SelectedUnit != null && parameterInGame != null)
        {
            if (parameterInGame.GetCurrentStamina() >= staminaCost)
            {
                lockBG.gameObject.SetActive(false);
            }
            else
            {
                lockBG.gameObject.SetActive(true);
            }
        }
    }

    public void DefenseButton()
    {
        if (parameterInGame == null) return;

        Debug.Log("Defense button");

        BasicParameters defaultComplexParameters = parameterInGame.GetDefaultComplexParameter();

        float defAGI = defaultComplexParameters.agility * 0.2f;

        float defVIT = defaultComplexParameters.vitality * 0.2f;

        BasicParameters temp = new BasicParameters();

        temp.agility = defAGI;

        temp.vitality = defVIT;

        parameterInGame.AddCurrentParameters(temp);

        parameterInGame.AddStamina(-5);

        CustomGameManager.Instance.SetNextTurn();

        UIAccuracyRate.Instance.SetActive(false);
    }
}
