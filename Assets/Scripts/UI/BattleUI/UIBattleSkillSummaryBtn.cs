﻿using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.HomeMenu;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleSkillSummaryBtn : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI skillnameText;
        [SerializeField] private TextMeshProUGUI skillDescriptionText;
        [SerializeField] private Image skillImage;
        [SerializeField] private Image skillRankImage;
        [SerializeField] private Button skillButton;
        [SerializeField] private Button subSkillButton;
        private UIMonsterSkillDetail skillDetailUI;
        private SkillDataScriptObj skillScriptable;
        private UnitAbility unitAbilityScriptable;
        private int skillLevel;

        public Button SkillButton => skillButton;

        public void SetSkillData(SkillDataScriptObj skillScriptable, UnitAbility unitAbilityScriptable, int skillLevel)
        {
            this.skillScriptable = skillScriptable;
            this.unitAbilityScriptable = unitAbilityScriptable;
            this.skillLevel = skillLevel;

            skillnameText.text = skillScriptable.Skill.skillName;
            skillDescriptionText.text = skillScriptable.Skill.Description;
            skillImage.sprite = unitAbilityScriptable.GetIconSprite();
            skillRankImage.sprite = RankText.Instance.GetRank(skillScriptable.Skill.skillDetails[skillLevel - 1].rankType);
        }

        public void SetDetailUI(UIMonsterSkillDetail skillDetailUI)
        {
            this.skillDetailUI = skillDetailUI;
            subSkillButton.onClick.AddListener(() =>
            {
                OpenSkillDetailMenu();
            });
            skillButton.onClick.AddListener(() =>
            {
                OpenSkillDetailMenu();
            });
        }

        private void OpenSkillDetailMenu()
        {
            this.skillDetailUI.SetSkillDisplay(skillScriptable, unitAbilityScriptable, skillLevel);
            this.skillDetailUI.OpenMenu();
        }
    }
}