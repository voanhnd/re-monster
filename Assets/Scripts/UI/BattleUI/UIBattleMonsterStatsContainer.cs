﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleMonsterStatsContainer : MonoBehaviour
    {
        [SerializeField] private Transform friendlyHolder;
        [SerializeField] private Transform enemyHolder;
        [SerializeField] private GameObject infoPrefab;
        [SerializeField] private UIBattleMonsterDetails battleMonsterDetail;
        [SerializeField] private UIMonsterBarPool battleMonsterPool;
        [SerializeField] private Canvas parentCanvas;
        [SerializeField] private Camera uiCamera;
        private CustomGameManager customGamemanager;
        private static Action<int> onTurnSwitched;
        private static Action<int> onApplyDelay;
        private static Action<int> onHealthChanged;
        private static Action<int> onStaminaChanged;
        private static Action onActionPerformed;

        public static Action<int> OnTurnSwitched => onTurnSwitched;
        public static Action<int> OnHealthChanged => onHealthChanged;
        public static Action<int> OnStaminaChanged => onStaminaChanged;
        public static Action<int> OnApplyDelay => onApplyDelay;
        public static Action OnActionPerformed => onActionPerformed;

        private void Awake()
        {
            onTurnSwitched = null;
            onHealthChanged = null;
            onStaminaChanged = null;
        }
        
        private void Start()
        {
            customGamemanager = CustomGameManager.Instance;

            AddFriendlyStatUI();

            AddHostileStatUI();

            onTurnSwitched.Invoke(customGamemanager.GetTurnList()[0].GameMonster.GetBattleID());
        }

        private void AddFriendlyStatUI()
        {
            List<GridUnit> gridUnits = GameManager.GetUnitsOnTeam(CustomGameManager.Instance.GetPlayerTeam());

            for (int i = 0; i < gridUnits.Count; i++)
            {
                int monsterIndex = i;
                GameObject characterInfoObject = Instantiate(infoPrefab, friendlyHolder);
                if (characterInfoObject.TryGetComponent(out UIMonsterBattleInfo friendlyInfoUI))
                {
                    MonsterInGame monsterInGame = gridUnits[i].GameMonster;

                    int monsterId = monsterInGame.GetBattleID();
                    RankTypeEnums monsterRank = monsterInGame.MonsterInfo.rankType;

                    ParameterInGame unitIngameParameter = monsterInGame.InGameParameter;
                    BattleParameters unitCurrentParameter = unitIngameParameter.GetCurrentBattleParameters();

                    float recoverStaPoint = unitCurrentParameter.StaminaRecovery;
                    float delayPoint = unitIngameParameter.GetSkillDelay();
                    float movePoint = unitCurrentParameter.Move;

                    MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInGame.MonsterInfo.monsterID);

                    string monstername = monsterScriptable.Monster.monsterName;

                    UIUnitObserveData unitObserveData = new(monsterId, monstername, monsterRank, unitIngameParameter, recoverStaPoint, delayPoint, movePoint, friendlyInfoUI, monsterScriptable.Monster.monsterAvatar, GameTeam.Blue);
                    unitObserveData.SetBattleMonsterDetail(battleMonsterDetail);
                    UIMonsterBattleBar uiMonsterBattleBar = battleMonsterPool.Pool.Get();
                    uiMonsterBattleBar.SetCanvasMonster(uiCamera, parentCanvas, monsterInGame.transform);
                    unitObserveData.SetMonsterBar(uiMonsterBattleBar);
                    friendlyInfoUI.SetFriendly();

                    onTurnSwitched += unitObserveData.ObserveTurn;
                    onApplyDelay += unitObserveData.ObserveDelay;
                    onHealthChanged += unitObserveData.ObserveHealth;
                    onStaminaChanged += unitObserveData.ObserveStamina;
                    onActionPerformed += unitObserveData.ObserveBuff;
                }
            }
        }

        private void AddHostileStatUI()
        {
            List<GridUnit> gridUnits = GameManager.GetUnitsOnTeam(CustomGameManager.GetEnemyTeam(CustomGameManager.Instance.GetPlayerTeam()));

            for (int i = 0; i < gridUnits.Count; i++)
            {
                int monsterIndex = i;
                GameObject characterInfoObject = Instantiate(infoPrefab, enemyHolder);
                if (characterInfoObject.TryGetComponent(out UIMonsterBattleInfo hostileInfoUI))
                {
                    MonsterInGame monsterInGame = gridUnits[i].GetComponent<MonsterInGame>();

                    int monsterId = monsterInGame.GetBattleID();
                    RankTypeEnums monsterRank = monsterInGame.MonsterInfo.rankType;

                    ParameterInGame unitIngameParameter = monsterInGame.GetComponent<ParameterInGame>();
                    BattleParameters unitCurrentParameter = unitIngameParameter.GetCurrentBattleParameters();

                    float recoverStaPoint = unitCurrentParameter.StaminaRecovery;
                    float delayPoint = unitIngameParameter.GetSkillDelay();
                    float movePoint = unitCurrentParameter.Move;

                    MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInGame.MonsterInfo.monsterID);

                    string monstername = monsterScriptable.Monster.monsterName;

                    UIUnitObserveData unitObserveData = new(monsterInGameId: monsterId, monsterName: monstername, monsterRank: monsterRank, liveParameter: unitIngameParameter, recoverPoint: recoverStaPoint, speedPoint: delayPoint, movePoint: movePoint, battleInfoUI: hostileInfoUI, monsterSprite: monsterScriptable.Monster.monsterAvatar, monsterTeam: GameTeam.Red);
                    unitObserveData.SetBattleMonsterDetail(battleMonsterDetail);
                    UIMonsterBattleBar uiMonsterBattleBar = battleMonsterPool.Pool.Get();
                    uiMonsterBattleBar.SetCanvasMonster(uiCamera, parentCanvas, monsterInGame.transform);
                    unitObserveData.SetMonsterBar(uiMonsterBattleBar);
                    hostileInfoUI.SetHostile();

                    onTurnSwitched += unitObserveData.ObserveTurn;
                    onApplyDelay += unitObserveData.ObserveDelay;
                    onHealthChanged += unitObserveData.ObserveHealth;
                    onStaminaChanged += unitObserveData.ObserveStamina;
                    onActionPerformed += unitObserveData.ObserveBuff;
                }
            }
        }
    }
}