﻿using Assets.Scripts.Monster;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleResultContent : MonoBehaviour
    {
        [SerializeField] private List<UIBattleMonsterResult> teamAMonsters;
        [SerializeField] private List<UIBattleMonsterResult> teamBMonsters;
        [SerializeField] private TextMeshProUGUI teamAWinStateText;
        [SerializeField] private TextMeshProUGUI teamBWinStateText;
        [SerializeField] private TextMeshProUGUI resultText;
        [SerializeField] private RectTransform controlContainer;
        [SerializeField] private float openMenuTime;
        [SerializeField] private float closeMenuTime;
        [SerializeField] private Ease openMenuEase;
        [SerializeField] private Ease closeMenuEase;
        [SerializeField] private Vector2 openMenuSize;
        [SerializeField] private Vector2 closeMenuSize;
        //[SerializeField] private Button resultButton;
        [SerializeField] private Button damageResultButton;
        [SerializeField] private Button healthResultButton;
        [SerializeField] private Button homeBtn;
        [SerializeField] private Color healthColor;
        [SerializeField] private Color damageColor;
        private bool isHealthResult;

        private void Awake()
        {
            homeBtn.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1));
            /*resultButton.onClick.AddListener(() =>
            {
                GetResult();
            });
            GetResult();*/
            damageResultButton.onClick.AddListener(() =>
            {
                GetDamageResult();
            });
            healthResultButton.onClick.AddListener(() =>
            {
                GetHealthResult();
            });
            SetMonstersBasicInfo();
        }

        /*private void GetResult()
        {
            if (isHealthResult)
            {
                GetHealthResult();
                resultButton.targetGraphic.color = healthColor;
                resultText.text = "Health Result";
            }
            else
            {
                GetDamageResult();
                resultButton.targetGraphic.color = damageColor;
                resultText.text = "Damage Result";
            }
            isHealthResult = !isHealthResult;
        }*/

        public void OpenMenu()
        {
            gameObject.SetActive(true);
            controlContainer.sizeDelta = closeMenuSize;
            controlContainer.DOKill();
            controlContainer.DOSizeDelta(openMenuSize, openMenuTime).SetEase(openMenuEase).OnComplete(() =>
            {
                isHealthResult = true;
                GetDamageResult();
            });
        }

        public void CloseMenu()
        {
            controlContainer.sizeDelta = openMenuSize;
            controlContainer.DOKill();
            controlContainer.DOSizeDelta(closeMenuSize, closeMenuTime).SetEase(closeMenuEase).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }

        public void SetWinningTeam(GameTeam team)
        {
            teamAWinStateText.text = team == GameTeam.Blue ? "winner" : "loser";
            teamBWinStateText.text = team == GameTeam.Red ? "winner" : "loser";

        }

        private void GetDamageResult()
        {
            if (!isHealthResult) return;
            isHealthResult = false;
            GetTeamDamage(teamAMonsters, GameTeam.Blue);
            GetTeamDamage(teamBMonsters, GameTeam.Red);
            resultText.DOText("Damage Dealt", .2f,true,ScrambleMode.All);
        }

        private void GetTeamDamage(List<UIBattleMonsterResult> teamMonstersUI, GameTeam team)
        {
            float maxValue = BattleStatsCollector.Instance.GetTeamDamageCaused(team);
            for (int i = 0; i < teamMonstersUI.Count; i++)
            {
                int monsterIndex = i;
                if (teamMonstersUI[monsterIndex].Unit == null) continue;
                teamMonstersUI[monsterIndex].SetMonsterValue(maxValue, BattleStatsCollector.Instance.GetMonsterDamageCaused(teamMonstersUI[monsterIndex].Unit, team));
            }
        }

        private void GetHealthResult()
        {
            if (isHealthResult) return;
            isHealthResult = true;
            GetTeamHealth(teamAMonsters, GameTeam.Blue);
            GetTeamHealth(teamBMonsters, GameTeam.Red);
            resultText.DOText("Health Remain", .2f,true,ScrambleMode.All);
        }

        private void GetTeamHealth(List<UIBattleMonsterResult> teamMonstersUI, GameTeam team)
        {
            for (int i = 0; i < teamMonstersUI.Count; i++)
            {
                int monsterIndex = i;
                if (teamMonstersUI[monsterIndex].Unit == null) continue;
                teamMonstersUI[monsterIndex].SetMonsterValue(teamMonstersUI[monsterIndex].Unit.GameMonster.InGameParameter.GetDefaultComplexParameter().health, Mathf.Clamp(teamMonstersUI[monsterIndex].InGamePara.GetCurrentComplexParameter().health, 0, 999));
            }
        }

        private void SetMonstersBasicInfo()
        {
            SetTeamBasicInfo(teamAMonsters, CustomGameManager.Instance.FriendlySpawnListInfo);
            SetTeamBasicInfo(teamBMonsters, CustomGameManager.Instance.EnemySpawnListInfos);
        }

        private void SetTeamBasicInfo(List<UIBattleMonsterResult> teamMonsters, List<CustomGameManager.SpawnListInfo> spawnList)
        {
            int usedFrameCount = 0;
            for (int i = 0; i < teamMonsters.Count; i++)
            {
                int monsterIndex = i;
                if (monsterIndex < spawnList.Count)
                {
                    usedFrameCount++;
                    PlayerInfo.PlayerMonsterInfo playerMonsterInfo = spawnList[monsterIndex].playerMonsterInfo;
                    MonsterDataScriptObj monsterScriptableData = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);
                    if (spawnList[monsterIndex].spawnedModel.TryGetComponent(out GridUnit unit))
                    {
                        teamMonsters[monsterIndex].SetMonsterBasicData(monsterScriptableData.Monster.monsterAvatar, monsterScriptableData.Monster.monsterName, unit);
                    }
                }
            }
            for (int i = usedFrameCount; i < teamMonsters.Count; i++)
            {
                teamMonsters[i].gameObject.SetActive(false);
            }
        }
    }
}