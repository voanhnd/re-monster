﻿using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleResultController : MonoBehaviour
    {
        [SerializeField] private UIBattleWinAnnouncer winAnnouncer;
        [SerializeField] private UIBattleLoseAnnouncer loseAnnouncer;
        [SerializeField] private UIBattleResultContent resultAnnouner;

        private void Start()
        {
            GameManager.Get().OnTeamWon.AddListener(HandleTeamWin);
            winAnnouncer.gameObject.SetActive(false);
            loseAnnouncer.gameObject.SetActive(false);
        }

        private void HandleTeamWin(GameTeam team)
        {
            gameObject.SetActive(true);
            resultAnnouner.SetWinningTeam(team);
            if (team == GameTeam.Blue)
            {
                winAnnouncer.gameObject.SetActive(true);
                winAnnouncer.TriggerAnimation();
                return;
            }
            loseAnnouncer.gameObject.SetActive(true);
            loseAnnouncer.TriggerAnimation();
        }
    }
}