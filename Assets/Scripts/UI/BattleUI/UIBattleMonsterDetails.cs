﻿using Assets.Scripts.Monster;
using Assets.Scripts.UI.Utilities;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleMonsterDetails : UIMenuStruct
    {
        [Header("Child ui")]
        [SerializeField] private UIBattleBuffDetail buffDetails;
        [SerializeField] private UIBattleMonsterInfo monsterBattleInfo;
        [SerializeField] private TextMeshProUGUI monsterNameText;
        [SerializeField] private TextMeshProUGUI monsterInGameIdText;

        public void SetTargetMonster(ParameterInGame monsterParameter,MonsterDataScriptObj monsterScriptable)
        {
            buffDetails.SetMonsterBuff(monsterParameter);
            monsterBattleInfo.SetParameters(monsterScriptable, monsterParameter);

            monsterInGameIdText.text = Helpers.ConvertNumberToAlphabet(monsterParameter.MonsterInGame.GetBattleID());
            monsterNameText.text = monsterScriptable.Monster.monsterName;

            OpenMenu();
        }
    }
}