﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleStartAnnouncer : MonoBehaviour
    {
        [SerializeField] private RectTransform outerRing;
        [SerializeField] private RectTransform middleRing;
        [SerializeField] private RectTransform innerRing;
        [SerializeField] private RectTransform battleIcon;
        [SerializeField] private RectTransform startTextImage;
        [SerializeField] private RectTransform line;
        [SerializeField] private float hideUIDelay = 3;
        private float hideCountDown = 0;
        private bool isClosing = false;

        private void Awake()
        {
            hideCountDown = hideUIDelay;
        }

        private void OnEnable()
        {
            outerRing.localScale = new Vector3(3, 3, 3);
            middleRing.localScale = new Vector3(5, 5, 5);
            innerRing.localScale = new Vector3(6, 6, 6);
            line.localScale = new Vector3(0, 1, 1);
            battleIcon.anchoredPosition = new Vector2(0, 725);
            startTextImage.anchoredPosition = new Vector2(0, -615);
            hideCountDown = hideUIDelay;
            isClosing = false;

            Sequence openSequence = DOTween.Sequence();
            openSequence.Append(outerRing.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack))
                        .Insert(0, innerRing.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack))
                        .Insert(0, middleRing.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack))
                        .Insert(0, line.DOScale(Vector2.one, 0.5f).SetEase(Ease.Linear))
                        .Append(startTextImage.DOAnchorPos(Vector2.zero, 0.2f).SetEase(Ease.OutBack))
                        .Insert(0.5f, battleIcon.DOAnchorPos(Vector2.zero, 0.2f).SetEase(Ease.OutBack));

        }

        private void Update()
        {
            HideUICountDown();
        }

        private void HideUICountDown()
        {
            if (isClosing) return;
            if (hideCountDown > 0)
            {
                hideCountDown -= Time.deltaTime;
                return;
            }
            isClosing = true;
            Sequence closeSequence = DOTween.Sequence();
            closeSequence.Append(startTextImage.DOAnchorPos(new Vector2(0, -615), 0.2f).SetEase(Ease.InBack))
                         .Insert(0, battleIcon.DOAnchorPos(new Vector2(0, 725), 0.2f).SetEase(Ease.InBack))
                         .Append(outerRing.DOScale(new Vector3(3, 3, 3), 0.5f).SetEase(Ease.InBack))
                         .Insert(0.2f, innerRing.DOScale(new Vector3(6, 6, 6), 0.5f).SetEase(Ease.InBack))
                         .Insert(0.2f, middleRing.DOScale(new Vector3(5, 5, 5), 0.5f).SetEase(Ease.InBack))
                         .Insert(0.2f, line.DOScale(new Vector3(0, 1, 1), 0.5f).SetEase(Ease.Linear))
                         .OnComplete(() => gameObject.SetActive(false));
        }
    }
}