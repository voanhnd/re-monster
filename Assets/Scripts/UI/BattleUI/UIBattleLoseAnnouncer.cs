﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleLoseAnnouncer : MonoBehaviour
    {
        [SerializeField] private RectTransform loseLine;
        [SerializeField] private RectTransform loseLogo;
        [SerializeField] private RectTransform loseTextImage;
        [SerializeField] private Image background;
        [SerializeField] private float delayTrigger = 3;

        [SerializeField] private Button battleResultBtn;
        [SerializeField] private UIBattleResultContent battleResultUI;
        [SerializeField] private bool canTriggerAnimation = false;
        private float delayTimer = 0;
        private bool isAnimationPlaying = false;

        private void Awake()
        {
            battleResultBtn.onClick.AddListener(() =>
            {
                battleResultUI.OpenMenu();
            });
        }

        private void Update()
        {
            if (isAnimationPlaying) return;
            if (!canTriggerAnimation) return;
            if (delayTimer > 0)
            {
                delayTimer -= Time.deltaTime;
                return;
            }
            isAnimationPlaying = true;
            background.gameObject.SetActive(true);
            Sequence animationSequence = DOTween.Sequence();
            animationSequence.Append(loseLine.DOAnchorPos(Vector2.zero, 0.5f))
                             .Insert(0.3f, loseLogo.DOAnchorPos(Vector2.zero, 0.5f).SetEase(Ease.OutBack))
                             .Insert(0.5f, loseTextImage.DOAnchorPos(new Vector2(0, -200), 0.2f).SetEase(Ease.OutBack))
                             .OnComplete(() =>
                             {
                                 battleResultBtn.gameObject.SetActive(true);
                             });
        }

        internal void TriggerAnimation()
        {
            canTriggerAnimation = true;
            delayTimer = delayTrigger;
        }
    }
}