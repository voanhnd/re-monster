﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBuffFragment : MonoBehaviour
    {
        [SerializeField] private Image buffGraphic;
        [SerializeField] private Image buffTypeGraphic;
        [SerializeField] private Color goodTypeColor = Color.green;
        [SerializeField] private Color badTypeColor = Color.red;
        [SerializeField] private Sprite goodBuffSprite;
        [SerializeField] private Sprite badBuffSprite;

        internal void SetBufficon(Sprite buffSprite, bool isGoodBuff)
        {
            buffGraphic.sprite = buffSprite;
            buffTypeGraphic.sprite = isGoodBuff ? goodBuffSprite : badBuffSprite;
            buffTypeGraphic.color = isGoodBuff ? goodTypeColor : badTypeColor;
        }
    }
}