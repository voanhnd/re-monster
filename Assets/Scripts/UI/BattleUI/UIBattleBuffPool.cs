﻿using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleBuffPool : MonoBehaviour
    {
        private IObjectPool<UIBattleBuffDetailDisplay> pool;
        [SerializeField] private PoolType poolType;
        [SerializeField] private GameObject objectPrefab;
        [SerializeField] private int maxPoolCapital = 6;
        [SerializeField] private bool isCollectionCheckOn = true;

        public IObjectPool<UIBattleBuffDetailDisplay> Pool
        {
            get
            {
                if (pool == null)
                {
                    if (poolType == PoolType.Stack)
                        pool = new ObjectPool<UIBattleBuffDetailDisplay>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, 10, maxPoolCapital);
                    else
                        pool = new LinkedPool<UIBattleBuffDetailDisplay>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, maxPoolCapital);
                }
                return pool;
            }
        }

        #region Pool controller
        public UIBattleBuffDetailDisplay CreatePoolItem()
        {
            GameObject spawnObject = Instantiate(this.objectPrefab, transform);

            UIBattleBuffDetailDisplay uibattleBuffDisplay = spawnObject.GetComponent<UIBattleBuffDetailDisplay>();

            uibattleBuffDisplay.Pool = pool;

            return uibattleBuffDisplay;
        }

        public void OnDestroyFromPool(UIBattleBuffDetailDisplay item) => Destroy(item.gameObject);

        public void OnReturnToPool(UIBattleBuffDetailDisplay item) => item.gameObject.SetActive(false);

        public void OnTakenFromPool(UIBattleBuffDetailDisplay item) => item.gameObject.SetActive(true);
        #endregion
    }
}