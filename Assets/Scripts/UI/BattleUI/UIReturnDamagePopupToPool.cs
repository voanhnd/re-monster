﻿using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIReturnDamagePopupToPool : MonoBehaviour
    {
        public IObjectPool<UIDamagePopup> pool;
        private IRemoveablePoolObject disposeable;
        private UIDamagePopup poolObject;

        public IRemoveablePoolObject Disposeable => disposeable;
        public UIDamagePopup PoolObject => poolObject;

        private void Awake()
        {
            disposeable = GetComponent<IRemoveablePoolObject>();
            disposeable.OnDispose += ReturnToPool;
            poolObject = GetComponent<UIDamagePopup>();
        }

        private void ReturnToPool()
        {
            pool.Release(poolObject);
        }

        private void OnDestroy()
        {
            disposeable.OnDispose -= ReturnToPool;
        }
    }
}