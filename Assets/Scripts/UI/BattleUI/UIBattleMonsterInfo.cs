﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.HomeMenu;
using Assets.Scripts.UI.MonsterDetailsInfo;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleMonsterInfo : MonoBehaviour
    {
        [SerializeField] private List<UIMonsterDetailInfoBasicParameters> monsterParameters = new();
        [SerializeField] private UIMonsterDetailInfoBattleInfo monsterBattleInfo;
        [SerializeField] private UIMonsterDetailInfoEnviroment monsterEnviroment;
        [SerializeField] private List<UIBattleSkillSummaryBtn> battleSkillSummaryList = new();
        [SerializeField] private UIMonsterSkillDetail uiSkillDetail;
        [SerializeField] private TextMeshProUGUI monsterMainSpecText;
        [SerializeField] private TextMeshProUGUI monsterSubSpecText;

        private void Awake()
        {
            for (int i = 0; i < battleSkillSummaryList.Count; i++)
            {
                battleSkillSummaryList[i].SetDetailUI(uiSkillDetail);
            }
        }

        public void SetParameters(MonsterDataScriptObj monsterScriptable, ParameterInGame monsterParameter)
        {
            SetMonsterBasicParameters(monsterParameter);

            SetMonsterResData(monsterScriptable);

            monsterEnviroment.SetTerrains(monsterScriptable.Monster.mainSuitTerrain, monsterScriptable.Monster.subSuitTerrain,
                                          monsterScriptable.Monster.mainNoSuitTerrain, monsterScriptable.Monster.subNoSuitTerrain);

            SetMonsterRace(monsterScriptable);

            SetMonsterSkills(monsterParameter);
        }

        private void SetMonsterBasicParameters(ParameterInGame monsterParameter)
        {
            for (int i = 0; i < monsterParameters.Count; i++)
            {
                BasicParameters defaultParameter = monsterParameter.GetDefaultComplexParameter();

                switch (monsterParameters[i].Parameter)
                {
                    case ParameterType.HP:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.health), defaultParameter.health, 999);
                        break;
                    case ParameterType.STR:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.strenght), defaultParameter.strenght, 999);
                        break;
                    case ParameterType.INT:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.intelligent), defaultParameter.intelligent, 999);
                        break;
                    case ParameterType.DEX:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.dexterity), defaultParameter.dexterity, 999);
                        break;
                    case ParameterType.AGI:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.agility), defaultParameter.agility, 999);
                        break;
                    case ParameterType.VIT:
                        monsterParameters[i].SetParameterValues(Monster.Monster.Get_Monster_Parameter_Rank_By_Value(defaultParameter.vitality), defaultParameter.vitality, 999);
                        break;
                }
            }
        }

        private void SetMonsterSkills(ParameterInGame monsterParameter)
        {
            List<UnitAbilityPlayerData> unitSkillData = monsterParameter.MonsterInGame.UnitAi.GetAbilities();
            int skillCount = 0;
            for (int i = 0; i < unitSkillData.Count; i++)
            {
                SkillDataScriptObj skillScriptable = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(unitSkillData[i].unitAbility.GetSkillID());
                UnitAbility unitAbilityScriptable = unitSkillData[i].unitAbility;
                Monster.Monster.MonsterSkillInfo monsterSkillInfo = monsterParameter.MonsterInGame.MonsterInfo.GetPlayerMonsterSkillInfoByID(unitSkillData[i].unitAbility.GetSkillID());
                int skillLevel;
                if (monsterSkillInfo != null)
                    skillLevel = monsterSkillInfo.skillLevel;
                else
                    skillLevel = 1;
                battleSkillSummaryList[skillCount].gameObject.SetActive(true);
                battleSkillSummaryList[skillCount++].SetSkillData(skillScriptable, unitAbilityScriptable, skillLevel);
            }
            for (int i = skillCount; i < battleSkillSummaryList.Count; i++)
            {
                battleSkillSummaryList[i].gameObject.SetActive(false);
            }
        }

        private void SetMonsterResData(MonsterDataScriptObj monsterScriptable)
        {
            monsterBattleInfo.SetAttributeRes(monsterScriptable.Monster.Hit, monsterScriptable.Monster.Cut,
                                                    monsterScriptable.Monster.Thrust, monsterScriptable.Monster.Water,
                                                    monsterScriptable.Monster.Flame, monsterScriptable.Monster.Wind,
                                                    monsterScriptable.Monster.Soil);

            monsterBattleInfo.SetAlimentRes(monsterScriptable.Monster.Poison, monsterScriptable.Monster.Weakness,
                                      monsterScriptable.Monster.Blindness, monsterScriptable.Monster.Slowdown,
                                      monsterScriptable.Monster.Paralysis, monsterScriptable.Monster.Sleep,
                                      monsterScriptable.Monster.Anger, monsterScriptable.Monster.Enchantment,
                                      monsterScriptable.Monster.Chaos, monsterScriptable.Monster.Fear);
        }

        private void SetMonsterRace(MonsterDataScriptObj monsterScriptable)
        {
            monsterMainSpecText.text = $"{monsterScriptable.Monster.monsterRace.mainRace}";
            monsterSubSpecText.text = $"{monsterScriptable.Monster.monsterRace.subRace}";
        }
    }
}