﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.UI.Utilities;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIComplexDamagePopup : MonoBehaviour, IRemoveablePoolObject
    {
        #region Private variables
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private TextMeshProUGUI delayText;
        [SerializeField] private RectTransform controlRect;
        [SerializeField] private float randomMinX;
        [SerializeField] private float randomMaxX;
        [SerializeField] private float randomMinY;
        [SerializeField] private float randomMaxY;
        [SerializeField] private float popupJumpHeight;
        [SerializeField] private Vector3 scaleDownSize;
        [SerializeField] private CanvasGroup group;

        [Header("Animation timing")]
        [SerializeField] private float scaleUpTime;
        [SerializeField] private float scaleDownTime;
        [SerializeField] private float fadeOutTime;
        [SerializeField] private float outDelayTime;
        [SerializeField] private Ease easeIn;
        [SerializeField] private Ease easeOut;
        private Transform cachedTransform;
        #endregion

        #region Public variables
        public Action OnDispose { get; set; }
        public Transform CachedTransform
        {
            get
            {
                if (cachedTransform == null)
                {
                    cachedTransform = transform;
                }
                return cachedTransform;
            }
        }
        #endregion

        private void PlayAnimation()
        {
            Vector2 currentAnchor = controlRect.anchoredPosition;
            group.alpha = 1f;
            controlRect.localScale = Vector3.zero;
            float randomStartX = currentAnchor.x + UnityEngine.Random.Range(randomMinX, randomMaxX);

            float randomStartY = currentAnchor.y + UnityEngine.Random.Range(randomMinY, randomMaxY);

            Sequence animationSeq = DOTween.Sequence();

            Vector2 spawnAnchor = new(randomStartX, randomStartY);
            controlRect.anchoredPosition = spawnAnchor;
            animationSeq.Append(controlRect.DOScale(Vector3.one, scaleUpTime).SetEase(easeIn))
                .Insert(0, controlRect.DOAnchorPos(spawnAnchor + new Vector2(0, popupJumpHeight), scaleUpTime).SetEase(easeIn))
                .Append(group.DOFade(0, fadeOutTime).SetEase(easeOut).SetDelay(outDelayTime))
                .Insert(scaleUpTime, controlRect.DOAnchorPos(spawnAnchor, scaleDownTime).SetEase(easeOut).SetDelay(outDelayTime))
                .Insert(scaleUpTime, controlRect.DOScale(scaleDownSize, scaleDownTime).SetEase(easeOut).SetDelay(outDelayTime))
                .OnKill(() =>
                {
                    OnDispose?.Invoke();
                });
        }

        public void SetDataValues(float damageValue, float delayValue, Color damageColor, Color delayColor, Vector3 spawnPosition)
        {
            damageText.text = $"-{SpecialFloatToInt.Convert(damageValue)}";
            damageText.color = damageColor;
            if (delayValue != 0)
            {
                delayText.text = $"spd-{SpecialFloatToInt.Convert(delayValue)}";
                delayText.color = delayColor;
            }
            else
            {
                delayText.text = string.Empty;
            }
            cachedTransform.position = spawnPosition;
            PlayAnimation();
        }

        public void SetDataValues(string damageValue, string delayValue, Color damageColor, Color delayColor, Vector3 spawnPosition)
        {
            damageText.text = $"{damageValue}";
            damageText.color = damageColor;
            delayText.text = $"{delayValue}";
            delayText.color = delayColor;
            cachedTransform.position = spawnPosition;
            PlayAnimation();
        }
    }
}