﻿using Assets.Scripts.Monster.Skill;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Assets.Scripts.Monster.Skill.Skill;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIMonsterBattleInfo : MonoBehaviour
    {
        [SerializeField] private Transform buffContainer;
        [SerializeField] private GameObject buffFragmentPrefab;
        [SerializeField] private TextMeshProUGUI monsterNameText;
        [SerializeField] private TextMeshProUGUI monsterBattleIdText;
        [SerializeField] private Image monsterRankImage;
        [Space(5)]
        [SerializeField] private TextMeshProUGUI monsterDelayText;
        [SerializeField] private TextMeshProUGUI mosnterMoveText;
        [SerializeField] private TextMeshProUGUI monsterStaRegenText;
        [Space(5)]
        [SerializeField] private TextMeshProUGUI monsterHealthValueText;
        [SerializeField] private TextMeshProUGUI monsterStaValueText;
        [SerializeField] private Slider monsterHealthSlider;
        [SerializeField] private Slider monsterStaSlider;
        [Header("Graphic components")]
        [SerializeField] private Image backgroundContainerGraph;
        [SerializeField] private Image backgroundOutlineGraph;
        [Space(5)]
        [SerializeField] private Image monsterBackground;
        [SerializeField] private Image monsterOutlineGraph;
        [Space(5)]
        [SerializeField] private Image monsterImage;
        [SerializeField] private Image turnBlockImage;
        [Header("Color control")]
        [SerializeField] private Color blockColor;
        [Space(5)]
        [SerializeField] private Color friendlyBackgroundColor;
        [SerializeField] private Color friendlyBackgroundOutlineColor;
        [SerializeField] private Color friendlyMonsterColor;
        [SerializeField] private Color friendlyOutlineMonsterColor;
        [Space(5)]
        [SerializeField] private Color hostileBackgroundColor;
        [SerializeField] private Color hostileBackgroundOutlineColor;
        [SerializeField] private Color hostileMonsterColor;
        [SerializeField] private Color hostileOutlineMonsterColor;

        [SerializeField] private Button detailBuffBtn;
        private int monsterRefId;
        private List<MonsterBuffData> buffDataList = new();
        private float originalSpd;
        private float currentSpd;

        public int MonsterRefId => monsterRefId;
        public Button DetailBuffBtn => detailBuffBtn;

        public void SetMonsterRef(int monsterRefId, string monsterName, RankTypeEnums monsterRank, float maxSta, float currentSta, float maxHp, float currentHp, float spd, float move, float staRegen, Sprite monsterSprite)
        {
            this.monsterRefId = monsterRefId;
            monsterNameText.text = monsterName;
            monsterRankImage.sprite = RankText.Instance.GetRank(monsterRank);
            monsterStaRegenText.text = Mathf.CeilToInt(staRegen).ToString();
            monsterImage.sprite = monsterSprite;
            monsterBattleIdText.text = Helpers.ConvertNumberToAlphabet(this.monsterRefId);

            if (monsterName.Trim().ToLower() == "アイスライム".ToLower())
                monsterImage.rectTransform.sizeDelta = new Vector2(92, 92);
            if (monsterName.Trim().ToLower() == "ゴーレム".ToLower())
            {
                monsterImage.rectTransform.sizeDelta = new Vector2(180, 180);
                monsterImage.rectTransform.anchoredPosition = new(0, -10);
            }

            monsterDelayText.text = spd.ToString();
            originalSpd = spd;
            currentSpd = spd;
            mosnterMoveText.text = move.ToString();

            monsterHealthValueText.text = Mathf.CeilToInt(currentHp).ToString();
            monsterHealthSlider.maxValue = maxHp;
            monsterHealthSlider.value = currentHp;

            monsterStaSlider.maxValue = maxSta;
            monsterStaSlider.value = currentSta;
            monsterStaValueText.text = Mathf.CeilToInt(currentSta).ToString();
        }

        public void UpdateMonsterHealthValue(float currentHp)
        {
            monsterHealthSlider.DOValue(currentHp, 0.5f, true).SetEase(Ease.Linear);
            monsterHealthValueText.text = Mathf.Clamp(Mathf.CeilToInt(currentHp), 0, monsterHealthSlider.maxValue).ToString();
        }

        public void UpdateMonsterStaValue(float currentSta)
        {
            monsterStaSlider.DOValue(currentSta, 0.5f, true).SetEase(Ease.Linear);
            monsterStaValueText.text = Mathf.Clamp(Mathf.CeilToInt(currentSta), 0, 100).ToString();
        }

        public void UpdateMonsterDelay(float delay)
        {
            float newSpd = originalSpd - delay;
            if (currentSpd == Mathf.Max(newSpd, 0)) return;
            currentSpd = Mathf.Max(newSpd, 0);
            if (currentSpd > originalSpd)
            {
                monsterDelayText.DOText($"{currentSpd:0}(+{delay:0})", 0.5f, true, ScrambleMode.Numerals);
                return;
            }

            if (currentSpd < this.originalSpd)
            {
                monsterDelayText.DOText($"{currentSpd:0}(-{delay:0})", 0.5f, true, ScrambleMode.Numerals);
                return;
            }
            if (currentSpd == originalSpd)
            {
                monsterDelayText.DOText($"{currentSpd:0}", 0.5f, true, ScrambleMode.Numerals);
                return;
            }
        }

        public void SetTurn(bool isTurn) => turnBlockImage.color = isTurn ? Color.clear : blockColor;

        public void SetIsDefeated(bool isDefeated) => turnBlockImage.color = isDefeated ? blockColor : Color.clear;

        internal void SetHostile()
        {
            backgroundContainerGraph.color = hostileBackgroundColor;
            backgroundOutlineGraph.color = hostileBackgroundOutlineColor;
            monsterBackground.color = hostileMonsterColor;
            monsterOutlineGraph.color = hostileOutlineMonsterColor;
        }

        internal void SetFriendly()
        {
            backgroundContainerGraph.color = friendlyBackgroundColor;
            backgroundOutlineGraph.color = friendlyBackgroundOutlineColor;
            monsterBackground.color = friendlyMonsterColor;
            monsterOutlineGraph.color = friendlyOutlineMonsterColor;
        }

        internal void UpdateBuff(List<InGameBuffDetail> inGameBuffDetails)
        {
            if (buffDataList.Count <= 0)
            {
                for (int i = 0; i < inGameBuffDetails.Count; i++)
                {
                    GameObject buffFragmentGameobject = Instantiate(buffFragmentPrefab, buffContainer);
                    UIBuffFragment buffFragment = buffFragmentGameobject.GetComponent<UIBuffFragment>();
                    SkillBuffImageManagerScriptObj.BuffEffectsData buffEffectData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffEffectData(inGameBuffDetails[i].monsterSpecialBufferType);
                    buffFragment.SetBufficon(buffEffectData.BuffSprite, true);
                    buffDataList.Add(new(buffFragment, true, MonsterSpecialDeBufferType.None, inGameBuffDetails[i].monsterSpecialBufferType));
                }
                return;
            }

            List<MonsterBuffData> existBuffs = new();

            for (int i = 0; i < inGameBuffDetails.Count; i++)
            {
                existBuffs.Add(new(null, true, MonsterSpecialDeBufferType.None, inGameBuffDetails[i].monsterSpecialBufferType));
            }

            // Remove elements from listA that don't exist in listB and have isGoodBuff = true
            List<MonsterBuffData> removebuffs = buffDataList.FindAll(a => a.IsGoodBuff && !existBuffs.Exists(b => b.Comparebuff(a)));
            for (int i = 0; i < removebuffs.Count; i++)
            {
                GameObject referenceObject = removebuffs[i].BuffFragment.gameObject;
                buffDataList.Remove(removebuffs[i]);
                Destroy(referenceObject);
            }

            // Add elements from listB that are not present in listA
            for (int i = 0; i < existBuffs.Count; i++)
            {
                int buffIndex = i;
                MonsterBuffData buffData = existBuffs[buffIndex];
                if (!buffDataList.Exists(a => a.Comparebuff(buffData)))
                {
                    GameObject buffFragmentGameobject = Instantiate(buffFragmentPrefab, buffContainer);
                    UIBuffFragment buffFragment = buffFragmentGameobject.GetComponent<UIBuffFragment>();

                    SkillBuffImageManagerScriptObj.BuffEffectsData buffDetail = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffEffectData(inGameBuffDetails[i].monsterSpecialBufferType);
                    buffFragment.SetBufficon(buffDetail.BuffSprite, true);

                    buffDataList.Add(new MonsterBuffData(buffFragment, existBuffs[buffIndex].IsGoodBuff, existBuffs[buffIndex].Debuff, existBuffs[buffIndex].Buff));
                }
            }
        }

        internal void UpdateBuff(List<InGameDeBuffDetail> inGameDeBuffDetails)
        {
            if (buffDataList.Count <= 0)
            {
                for (int i = 0; i < inGameDeBuffDetails.Count; i++)
                {
                    GameObject buffFragmentGameobject = Instantiate(buffFragmentPrefab, buffContainer);
                    UIBuffFragment buffFragment = buffFragmentGameobject.GetComponent<UIBuffFragment>();
                    SkillBuffImageManagerScriptObj.DebuffEffectsData debuffDetail = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDebuffEffectData(inGameDeBuffDetails[i].monsterSpecialDeBufferType);
                    buffFragment.SetBufficon(debuffDetail.DebuffSprite, false);
                    buffDataList.Add(new(buffFragment, false, inGameDeBuffDetails[i].monsterSpecialDeBufferType, MonsterSpecialBufferType.None));
                }
                return;
            }

            List<MonsterBuffData> existBuffs = new();

            for (int i = 0; i < inGameDeBuffDetails.Count; i++)
            {
                existBuffs.Add(new(null, false, inGameDeBuffDetails[i].monsterSpecialDeBufferType, MonsterSpecialBufferType.None));
            }

            // Remove elements from listA that don't exist in listB and have isGoodBuff = true
            List<MonsterBuffData> removebuffs = buffDataList.FindAll(a => !a.IsGoodBuff && !existBuffs.Exists(b => b.Comparebuff(a)));
            for (int i = 0; i < removebuffs.Count; i++)
            {
                GameObject referenceObject = removebuffs[i].BuffFragment.gameObject;
                buffDataList.Remove(removebuffs[i]);
                Destroy(referenceObject);
            }

            // Add elements from listB that are not present in listA
            for (int i = 0; i < existBuffs.Count; i++)
            {
                int buffIndex = i;
                MonsterBuffData buffData = existBuffs[buffIndex];
                if (!buffDataList.Exists(a => a.Comparebuff(buffData)))
                {
                    GameObject buffFragmentGameobject = Instantiate(buffFragmentPrefab, buffContainer);
                    UIBuffFragment buffFragment = buffFragmentGameobject.GetComponent<UIBuffFragment>();
                    SkillBuffImageManagerScriptObj.DebuffEffectsData debuffData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDebuffEffectData(inGameDeBuffDetails[buffIndex].monsterSpecialDeBufferType);
                    buffFragment.SetBufficon(debuffData.DebuffSprite, false);

                    buffDataList.Add(new MonsterBuffData(buffFragment, existBuffs[buffIndex].IsGoodBuff, existBuffs[buffIndex].Debuff, existBuffs[buffIndex].Buff));
                }
            }
        }

        private class MonsterBuffData
        {
            private UIBuffFragment buffFragment;
            private bool isGoodBuff = false;
            private MonsterSpecialDeBufferType debuff;
            private MonsterSpecialBufferType buff;

            public MonsterBuffData() { }

            public MonsterBuffData(UIBuffFragment buffFragment, bool isGoodBuff, MonsterSpecialDeBufferType debuff, MonsterSpecialBufferType buff)
            {
                this.buffFragment = buffFragment;
                this.isGoodBuff = isGoodBuff;
                this.debuff = debuff;
                this.buff = buff;
            }

            public UIBuffFragment BuffFragment => buffFragment;
            public bool IsGoodBuff => isGoodBuff;
            public MonsterSpecialDeBufferType Debuff => debuff;
            public MonsterSpecialBufferType Buff => buff;

            public bool Comparebuff(MonsterBuffData otherBuff)
            {
                if (otherBuff == null) return false;
                if (otherBuff.Debuff == this.debuff && !isGoodBuff) return true;
                if (otherBuff.Buff == this.buff && isGoodBuff) return true;
                return false;
            }
        }
    }
}