﻿using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIReturnComplexPopupToPool : MonoBehaviour
    {
        public IObjectPool<UIComplexDamagePopup> pool;
        private IRemoveablePoolObject disposeable;
        private UIComplexDamagePopup poolObject;

        public IRemoveablePoolObject GetDisposeable() => disposeable;

        public UIComplexDamagePopup PoolObject => poolObject;

        private void Awake()
        {
            disposeable = GetComponent<IRemoveablePoolObject>();
            disposeable.OnDispose += ReturnToPool;
            poolObject = GetComponent<UIComplexDamagePopup>();
        }

        private void ReturnToPool()
        {
            pool.Release(poolObject);
        }

        private void OnDestroy()
        {
            disposeable.OnDispose -= ReturnToPool;
        }
    }
}