﻿using Assets.Scripts.UI.Utilities;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIDamgePopupController : MonoBehaviour
    {
        [Header("Critical need special design")]
        [SerializeField] private UIComplexPoolDamagePopup critPopupPool;
        [SerializeField] private UIComplexPoolDamagePopup damagePopupPool;
        [SerializeField] private UIPoolDamgePopup popupPool;
        [SerializeField] private Camera uiCam;
        [SerializeField] private Canvas usingCanvas;
        [SerializeField] private Color staminaColor;
        [SerializeField] private Color critColor;
        [SerializeField] private Color friendlyMissColor;
        [SerializeField] private Color hostileMissColor;
        [SerializeField] private Color healColor;
        [SerializeField] private Color hostileDamageColor;
        [SerializeField] private Color friendlyDamageColor;
        [SerializeField] private Color delayDamageColor;
        [SerializeField] private Vector3 damageSpawnOffset;
        [SerializeField] private Vector3 staminaSpawnOffset;
        [SerializeField] private Vector3 delaySpawnOffset;
        [SerializeField] private Vector3 healSpawnOffset;

        public static Action<Vector3, float> OnStaminaChanged { get; private set; }
        public static Action<Vector3, float> OnSkillDelay { get; private set; }
        public static Action<Vector3, float, float, bool, bool> OnReciveDamage { get; private set; }
        public static Action<Vector3, float> OnHeal { get; private set; }
        public static Action<Vector3, bool> OnMissed { get; private set; }

        private void Awake()
        {
            OnStaminaChanged = null;
            OnSkillDelay = null;
            OnReciveDamage = null;
            OnHeal = null;
            OnMissed = null;
        }

        private void Start()
        {
            OnStaminaChanged += SetStamina;
            OnSkillDelay += SetSelfDelay;
            OnReciveDamage += SetDamageApply;
            OnHeal += SetHeal;
            OnMissed += SetMiss;
        }

        private void OnDestroy()
        {
            OnStaminaChanged -= SetStamina;
            OnSkillDelay -= SetSelfDelay;
            OnReciveDamage -= SetDamageApply;
            OnHeal -= SetHeal;
            OnMissed -= SetMiss;
        }

        private void SetMiss(Vector3 worldPosition, bool isFriendly)
        {
            UIDamagePopup poolItem = popupPool.Pool.Get();
            poolItem.CachedTransform.SetAsFirstSibling();
            Vector3 canvasPos = WorldToCanvasCamPos(worldPosition);
            canvasPos += damageSpawnOffset;
            poolItem.SetData("Miss", isFriendly ? friendlyMissColor : hostileMissColor, canvasPos);
            return;
        }

        private void SetHeal(Vector3 worldPosition, float healIncome)
        {
            UIDamagePopup poolItem = popupPool.Pool.Get();
            poolItem.CachedTransform.SetAsFirstSibling();
            Vector3 canvasPos = WorldToCanvasCamPos(worldPosition);
            canvasPos += healSpawnOffset;
            poolItem.SetData($"+{Mathf.RoundToInt(healIncome)}", healColor, canvasPos);
            return;
        }

        private void SetDamageApply(Vector3 worldPosition, float damage, float delayValue, bool isFriendly, bool isCrit)
        {
            if (damage == 0) return;
            Vector3 canvasPos = WorldToCanvasCamPos(worldPosition);
            canvasPos += damageSpawnOffset;
            if (isCrit)
            {
                UIComplexDamagePopup poolItem = critPopupPool.Pool.Get();
                poolItem.CachedTransform.SetAsFirstSibling();
                if(delayValue != 0)
                {
                    poolItem.SetDataValues($"{SpecialFloatToInt.Convert(damage)}!", $"spd-{SpecialFloatToInt.Convert(delayValue)}", critColor, delayDamageColor, canvasPos);
                }
                else
                {
                    poolItem.SetDataValues($"{SpecialFloatToInt.Convert(damage)}!", string.Empty, critColor, delayDamageColor, canvasPos);
                }
            }
            else
            {
                UIComplexDamagePopup poolItem = damagePopupPool.Pool.Get();
                poolItem.CachedTransform.SetAsFirstSibling();
                poolItem.SetDataValues(damage, delayValue, isFriendly ? friendlyDamageColor : hostileDamageColor, delayDamageColor, canvasPos);
            }
        }

        private void SetStamina(Vector3 worldPosition, float value)
        {
            Vector3 canvasPos = WorldToCanvasCamPos(worldPosition);
            canvasPos += staminaSpawnOffset;
            UIDamagePopup poolItem = popupPool.Pool.Get();
            poolItem.CachedTransform.SetAsFirstSibling();
            if (value > 0)
                poolItem.SetData($"+{Mathf.RoundToInt(value)}", staminaColor, canvasPos);
            else
                poolItem.SetData($"{Mathf.RoundToInt(value)}", staminaColor, canvasPos);
        }

        private void SetSelfDelay(Vector3 worldPosition, float value)
        {
            if (value == 0) return;
            Vector3 canvasPos = WorldToCanvasCamPos(worldPosition);
            canvasPos += delaySpawnOffset;
            UIDamagePopup poolItem = popupPool.Pool.Get();
            poolItem.CachedTransform.SetAsFirstSibling();
            if (value > 0)
                poolItem.SetData($"+{Mathf.FloorToInt(value)}", delayDamageColor, canvasPos);
            else
                poolItem.SetData($"{Mathf.FloorToInt(value)}", delayDamageColor, canvasPos);
        }

        private Vector3 WorldToCanvasCamPos(Vector3 worldPosition)
        {
            Vector3 screen = Camera.main.WorldToScreenPoint(worldPosition);
            screen.z = (usingCanvas.transform.position - uiCam.transform.position).magnitude;
            Vector3 position = uiCam.ScreenToWorldPoint(screen);
            return position;
        }
    }
}