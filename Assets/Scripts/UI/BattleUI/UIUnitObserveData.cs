﻿using UnityEngine;

namespace Assets.Scripts.UI.BattleUI
{
    internal class UIUnitObserveData
    {
        private int monsterInGameId;
        private string monsterName;
        private RankTypeEnums monsterRank;
        private ParameterInGame liveParameter; // for taking hp and sta points
        private float recoverPoint;
        private float speedPoint;
        private float movePoint;
        private GameTeam monsterTeam;
        private UIMonsterBattleInfo battleInfoUI;
        private UIBattleMonsterDetails battleMonsterDetail;
        private UIMonsterBattleBar uiMonsterBattleBar;

        public UIUnitObserveData(int monsterInGameId, string monsterName, RankTypeEnums monsterRank, ParameterInGame liveParameter, float recoverPoint, float speedPoint, float movePoint, UIMonsterBattleInfo battleInfoUI, Sprite monsterSprite, GameTeam monsterTeam)
        {
            this.monsterInGameId = monsterInGameId;
            this.monsterName = monsterName;
            this.monsterRank = monsterRank;
            this.liveParameter = liveParameter;
            this.recoverPoint = recoverPoint;
            this.speedPoint = speedPoint;
            this.movePoint = movePoint;
            this.battleInfoUI = battleInfoUI;
            this.monsterTeam = monsterTeam;

            this.battleInfoUI.SetMonsterRef(monsterInGameId, monsterName, monsterRank, 100, CurrentSta, this.liveParameter.GetDefaultComplexParameter().health, CurrentHealth, Speed, movePoint, recoverPoint, monsterSprite);
        }

        /// <summary>
        /// Must be call after filling all monster info
        /// </summary>
        /// <param name="battleMonsterDetail"></param>
        public void SetBattleMonsterDetail(UIBattleMonsterDetails battleMonsterDetail)
        {
            this.battleMonsterDetail = battleMonsterDetail;
            battleInfoUI.DetailBuffBtn.onClick.AddListener(() =>
            {
                this.battleMonsterDetail.SetTargetMonster(liveParameter, PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(liveParameter.MonsterInGame.MonsterInfo.monsterID));
            });
        }

        public void SetMonsterBar(UIMonsterBattleBar uiMonsterBattleBar)
        {
            this.uiMonsterBattleBar = uiMonsterBattleBar;
            this.uiMonsterBattleBar.SetData(liveParameter.GetDefaultComplexParameter().health, 100, Helpers.ConvertNumberToAlphabet(monsterInGameId));
            this.uiMonsterBattleBar.AdjustStamina(CurrentSta);
            this.uiMonsterBattleBar.AdjustHealth(liveParameter.GetDefaultComplexParameter().health);
        }

        public int MonsterId => monsterInGameId;
        public string MonsterName => monsterName;
        public RankTypeEnums MonsterRank => monsterRank;
        public ParameterInGame LiveParameter => liveParameter;
        public float RecoverPoint => recoverPoint;
        public float SpeedPoint => speedPoint;
        public float MovePoint => movePoint;
        public float CurrentHealth => liveParameter.GetHealth();
        public float CurrentSta => liveParameter.GetCurrentStamina();
        public float DelayStat => liveParameter.GetSkillDelay();
        public float Speed => liveParameter.GetCurrentBattleParametersSpeed();
        public UIMonsterBattleInfo BattleInfoUI => battleInfoUI;

        public void ObserveTurn(int monsterId)
        {
            battleInfoUI.SetTurn(this.monsterInGameId == monsterId);
            if (liveParameter == null) return;
            BattleInfoUI.UpdateBuff(liveParameter.inGameBuffDetails);
            BattleInfoUI.UpdateBuff(liveParameter.inGameDeBuffDetails);
        }

        public void ObserveBuff()
        {
            if (liveParameter == null) return;
            BattleInfoUI.UpdateBuff(liveParameter.inGameBuffDetails);
            BattleInfoUI.UpdateBuff(liveParameter.inGameDeBuffDetails);
        }

        public void ObserveHealth(int monsterId)
        {
            if (this.monsterInGameId == monsterId)
            {
                battleInfoUI.UpdateMonsterHealthValue(CurrentHealth);
                uiMonsterBattleBar.AdjustHealth(CurrentHealth);
                if (CurrentHealth <= 0)
                {
                    uiMonsterBattleBar.RemoveTargetControl();
                }
            }
        }

        public void ObserveStamina(int monsterId)
        {
            if (this.monsterInGameId == monsterId)
            {
                battleInfoUI.UpdateMonsterStaValue(CurrentSta);
                uiMonsterBattleBar.AdjustStamina(CurrentSta);
            }
        }

        public void ObserveDelay(int monsterId)
        {
            if (this.monsterInGameId == monsterId)
            {
                battleInfoUI.UpdateMonsterDelay(DelayStat);
            }
        }
    }
}