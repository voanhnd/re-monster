﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIBattleWinAnnouncer : MonoBehaviour
    {
        [SerializeField] private RectTransform winningLine;
        [SerializeField] private RectTransform winningBackLogo;
        [SerializeField] private RectTransform winningFrontLogo;
        [SerializeField] private RectTransform winningTextImage;
        [SerializeField] private Image background;
        [SerializeField] private float delayTrigger = 3;

        [SerializeField] private UIBattleResultContent battleResultUI;
        [SerializeField] private Button battleResult;
        private float delayTimer = 0;
        [SerializeField] private bool canTriggerAnimation = false;
        private bool isAnimationPlaying = false;

        private void Awake()
        {
            battleResult.onClick.AddListener(() =>
            {
                battleResultUI.OpenMenu();
            });
        }

        private void Update()
        {
            if (isAnimationPlaying) return;
            if (!canTriggerAnimation) return;
            if (delayTimer > 0)
            {
                delayTimer -= Time.deltaTime;
                return;
            }
            isAnimationPlaying = true;
            background.gameObject.SetActive(true);
            Sequence animationSequence = DOTween.Sequence();
            animationSequence.Append(winningLine.DOAnchorPos(Vector2.zero, 0.5f))
                             .Insert(0.4f, winningBackLogo.DOAnchorPos(new Vector2(0, winningBackLogo.anchoredPosition.y), 0.2f).SetEase(Ease.OutBack))
                             .Insert(0.4f, winningFrontLogo.DOAnchorPos(new Vector2(0, winningFrontLogo.anchoredPosition.y), 0.2f).SetEase(Ease.OutBack))
                             .Insert(0.6f, winningTextImage.DOAnchorPos(Vector2.zero, 0.2f).SetEase(Ease.OutBack))
                             .OnComplete(() =>
                             {
                                 battleResult.gameObject.SetActive(true);
                             });
        }

        internal void TriggerAnimation()
        {
            canTriggerAnimation = true;
            delayTimer = delayTrigger;
        }
    }
}