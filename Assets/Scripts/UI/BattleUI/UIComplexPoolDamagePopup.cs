﻿using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIComplexPoolDamagePopup : MonoBehaviour
    {
        private IObjectPool<UIComplexDamagePopup> pool;
        [SerializeField] private PoolType poolType;
        [SerializeField] private GameObject damagePopupPrefab;
        [SerializeField] private Transform prefabContainer;
        [SerializeField] private int maxPoolCapital = 6;
        private bool isCollectionCheckOn = true;

        #region Public variables
        public IObjectPool<UIComplexDamagePopup> Pool
        {
            get
            {
                if (pool == null)
                {
                    if (poolType == PoolType.Stack)
                        pool = new ObjectPool<UIComplexDamagePopup>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, 10, maxPoolCapital);
                    else
                        pool = new LinkedPool<UIComplexDamagePopup>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, maxPoolCapital);
                }
                return pool;
            }
        }

        private void OnDestroyFromPool(UIComplexDamagePopup obj)
        {
            Destroy(obj.gameObject);
        }

        private void OnReturnToPool(UIComplexDamagePopup obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void OnTakenFromPool(UIComplexDamagePopup obj)
        {
            obj.gameObject.SetActive(true);
        }

        private UIComplexDamagePopup CreatePoolItem()
        {
            GameObject damgePopupObject = Instantiate(this.damagePopupPrefab, prefabContainer);

            UIComplexDamagePopup damagePopup = damgePopupObject.GetComponent<UIComplexDamagePopup>();

            UIReturnComplexPopupToPool returnObjectToPool = damgePopupObject.AddComponent<UIReturnComplexPopupToPool>();

            returnObjectToPool.pool = pool;

            return damagePopup;
        }
        #endregion
    }
}