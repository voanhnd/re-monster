﻿using Assets.Scripts.Interfaces;
using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UI;

namespace Assets.Scripts.UI.BattleUI
{
    public class UIMonsterBattleBar : MonoBehaviour, IRemoveablePoolObject
    {
        [SerializeField] private RectTransform controlRectTransform;
        [SerializeField] private Slider healthSlider;
        [SerializeField] private Slider staminaSlider;
        [SerializeField] private TextMeshProUGUI monsterBattleId;
        [SerializeField] private Vector3 offsetPosition;

        public IObjectPool<UIMonsterBattleBar> pool;

        private Transform monsterTransform;
        private Canvas parentCanvas;
        private Camera displayCamera;
        private Transform cachedTransform;

        public Action OnDispose { get; set; }

        private void Awake()
        {
            cachedTransform = transform;
            OnDispose += ReturnToPool;
        }

        private void LateUpdate()
        {
            if (parentCanvas == null || displayCamera == null) return;
            cachedTransform.position = monsterTransform.position + offsetPosition;
            cachedTransform.LookAt(cachedTransform.position + displayCamera.transform.rotation * Vector3.forward, displayCamera.transform.rotation * Vector3.up);
            cachedTransform.eulerAngles = new Vector3(cachedTransform.eulerAngles.x, cachedTransform.eulerAngles.y, 0f);
        }

        private void ReturnToPool()
        {
            pool.Release(this);
        }

        public void RemoveTargetControl()
        {
            OnDispose?.Invoke();
            parentCanvas = null;
        }

        public void AdjustHealth(float value)
        {
            healthSlider.DOValue(value, 0.5f).SetEase(Ease.OutQuad);
        }

        public void AdjustStamina(float value)
        {
            staminaSlider.DOValue(value, 0.5f).SetEase(Ease.OutQuad);
        }

        public void SetCanvasMonster(Camera displayCamera, Canvas parentCanvas, Transform monsterTransform)
        {
            this.displayCamera = displayCamera;
            this.parentCanvas = parentCanvas;
            this.monsterTransform = monsterTransform;
        }

        public void SetData(float maxHealth, float maxStamina, string battleId)
        {
            healthSlider.maxValue = maxHealth;
            staminaSlider.maxValue = maxStamina;
            monsterBattleId.text = battleId;
        }
    }
}