﻿using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.HomeMenu
{
    public class ReturnUIToPool : MonoBehaviour
    {
        public IObjectPool<UIMonsterSkillDetailBuff> pool;
        private IRemoveablePoolObject disposeable;
        private UIMonsterSkillDetailBuff poolObject;

        public IRemoveablePoolObject Disposeable { get => disposeable; }
        public UIMonsterSkillDetailBuff PoolObject { get => poolObject; }

        private void Awake()
        {
            disposeable = GetComponent<IRemoveablePoolObject>();
            disposeable.OnDispose += ReturnToPool;
            poolObject = GetComponent<UIMonsterSkillDetailBuff>();
        }

        private void ReturnToPool()
        {
            pool.Release(poolObject);
        }

        private void OnDestroy()
        {
            disposeable.OnDispose -= ReturnToPool;
        }
    }
}