﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIMoneyListener : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI bitValueText;

        private void Awake()
        {
            PlayerInfo.OnBitValueChanged += UpdateCurrentValue;
        }

        private void Start()
        {
            UpdateCurrentValue();
        }

        private void OnDestroy()
        {
            PlayerInfo.OnBitValueChanged -= UpdateCurrentValue;
        }

        private void UpdateCurrentValue() => bitValueText.text = $"{PlayerStats.Instance.playerInfo.bit:0}";
    }
}