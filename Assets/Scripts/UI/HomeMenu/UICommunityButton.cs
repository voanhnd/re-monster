﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UICommunityButton : MonoBehaviour
    {
        private Button button;
        [SerializeField] private RectTransform chatlogObj;
        [SerializeField] private Vector2 chatlogSizeClose;
        [SerializeField] private Vector2 chatlogSizeOpen;
        [SerializeField] private float animationTime;
        private bool isChatlogOpen = false;
        private bool isAnimating = false;

        private void Awake()
        {
            button = GetComponent<Button>();
            /*button.onClick.AddListener(() =>
            {
                ToggleChatLog();
            });*/
            chatlogObj.sizeDelta = chatlogSizeClose;
            isChatlogOpen = false;
            chatlogObj.gameObject.SetActive(false);
        }

        private void ToggleChatLog()
        {
            if (isAnimating) return;
            isAnimating = true;
            if (isChatlogOpen)
                CloseChatAnimation();
            else
                OpenChatAnimation();
        }
        private void OpenChatAnimation()
        {
            chatlogObj.gameObject.SetActive(true);
            chatlogObj.DOKill();
            chatlogObj.DOSizeDelta(chatlogSizeOpen, animationTime).OnComplete(() =>
            {
                isChatlogOpen = true;
                isAnimating = false;
            });
        }

        private void CloseChatAnimation()
        {
            chatlogObj.DOKill();
            chatlogObj.DOSizeDelta(chatlogSizeClose, animationTime).OnComplete(() =>
            {
                chatlogObj.gameObject.SetActive(false);
                isChatlogOpen = false;
                isAnimating = false;
            });
        }
    }
}