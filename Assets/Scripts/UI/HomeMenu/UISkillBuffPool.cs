﻿using Assets.Scripts.UI.BattleUI;
using UnityEngine;
using UnityEngine.Pool;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UISkillBuffPool : MonoBehaviour
    {
        [SerializeField] private GameObject poolObject;
        [SerializeField] private Transform container;
        [SerializeField] private PoolType type;
        [SerializeField] private bool isCollectionCheckOn = true;
        [SerializeField] private int maxPoolCapital = 10;

        private IObjectPool<UIMonsterSkillDetailBuff> pool;

        #region Public variables
        public IObjectPool<UIMonsterSkillDetailBuff> Pool
        {
            get
            {
                if (pool == null)
                {
                    if (type == PoolType.Stack)
                        pool = new ObjectPool<UIMonsterSkillDetailBuff>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, 10, maxPoolCapital);
                    else
                        pool = new LinkedPool<UIMonsterSkillDetailBuff>(CreatePoolItem, OnTakenFromPool, OnReturnToPool, OnDestroyFromPool, isCollectionCheckOn, maxPoolCapital);
                }
                return pool;
            }
        }
        #endregion

        #region Pool controller
        public UIMonsterSkillDetailBuff CreatePoolItem()
        {
            GameObject skillbuffObject = Instantiate(this.poolObject, container);

            UIMonsterSkillDetailBuff skillBuffDetailUI = skillbuffObject.GetComponent<UIMonsterSkillDetailBuff>();

            ReturnUIToPool returnObjectToPool = skillbuffObject.AddComponent<ReturnUIToPool>();

            returnObjectToPool.pool = pool;

            return skillBuffDetailUI;
        }

        public void OnDestroyFromPool(UIMonsterSkillDetailBuff item)
        {
            Destroy(item.gameObject);
        }

        public void OnReturnToPool(UIMonsterSkillDetailBuff item)
        {
            item.gameObject.SetActive(false);
        }

        public void OnTakenFromPool(UIMonsterSkillDetailBuff item)
        {
            item.gameObject.SetActive(true);
        }
        #endregion
    }
}