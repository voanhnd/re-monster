﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIGameDate : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI weekTimeText;
        [SerializeField] private TextMeshProUGUI monthTimeText;
        [SerializeField] private TextMeshProUGUI yearTimeText;
        private int weekTime = 1;
        private int monthTime = 4;
        private int yearTime = 1;

        private void Start()
        {
            weekTime = PlayerPrefs.GetInt("Week_Pass", 1);
            monthTime = PlayerPrefs.GetInt("Month_Pass", 4);
            yearTime = PlayerPrefs.GetInt("Year_Pass", 1);
            UpdateTimeText();
        }

        public void AddWeekTime(int week)
        {
            weekTime += week;
            UpdateTimeText();
        }

        private void UpdateTimeText()
        {
            if (weekTime > 4)
            {
                weekTime -= 4;
                monthTime++;
            }
            if (monthTime > 12)
            {
                yearTime++;
                monthTime -= 12;
            }
            weekTimeText.text = $"Week: {weekTime}";
            monthTimeText.text = $"Month: {monthTime}";
            yearTimeText.text = $"Year: {yearTime}";
            PlayerPrefs.SetInt("Week_Pass", weekTime);
            PlayerPrefs.SetInt("Month_Pass", monthTime);
            PlayerPrefs.SetInt("Year_Pass", yearTime);
        }
    }
}