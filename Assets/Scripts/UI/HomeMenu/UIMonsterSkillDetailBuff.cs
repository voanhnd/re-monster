﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.UI.Utilities;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIMonsterSkillDetailBuff : MonoBehaviour, IRemoveablePoolObject
    {
        [SerializeField] private Image buffImage;
        [SerializeField] private TextMeshProUGUI buffDescriptionText;
        [SerializeField] private TextMeshProUGUI buffValueText;
        [SerializeField] private TextMeshProUGUI buffRateText;

        public Action OnDispose { get; set; }
        private Transform cachedTransform;
        public Transform CachedTransform
        {
            get
            {
                if (cachedTransform == null)
                    cachedTransform = transform;
                return cachedTransform;
            }
        }


        public void SetBuffDetail(Sprite buffSprite, string buffDescriptionText, float buffValue)
        {
            buffImage.sprite = buffSprite;
            this.buffDescriptionText.text = buffDescriptionText;
            if (buffValue > 0)
                buffValueText.text = $"Buff value: {SpecialFloatToInt.Convert(buffValue)}%";
            else
                buffValueText.text = $"Buff value: -%";
        }

        public void SetEffectDetail(Sprite effectSprite, string effectName, string effectSummary, float effectRate = 0)
        {
            buffImage.sprite = effectSprite;
            this.buffDescriptionText.text = effectName;
            this.buffValueText.text = effectSummary;
            if(effectRate != 0)
            {
                buffRateText.text = $"Success rate: {SpecialFloatToInt.Convert(effectRate)}%";
                buffRateText.gameObject.SetActive(true);
            }
            else
            {
                buffRateText.text = string.Empty;
                buffRateText.gameObject.SetActive(false);
            }
        }

        public void SetDeBuffDetail(Sprite buffSprite, string buffDescriptionText, float buffValue)
        {
            buffImage.sprite = buffSprite;
            this.buffDescriptionText.text = $"{buffDescriptionText}";
            if (buffValue > 0)
                buffValueText.text = $"Debuff value: {SpecialFloatToInt.Convert(buffValue)}%";
            else
                buffValueText.text = $"Debuff value: -%";
        }
    }
}