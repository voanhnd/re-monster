﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIDayNightController : MonoBehaviour
    {
        [SerializeField] private float changeAfterTime = 10f;
        [SerializeField] private float shiftTime = 3;
        [SerializeField] private List<CanvasGroup> dayItems = new();
        [SerializeField] private List<CanvasGroup> sunsetItems = new();
        [SerializeField] private List<CanvasGroup> nightItems = new();
        private float elapseTime = 0;
        private float shiftCountDown = 0;
        private bool canChangeNextDay = false;
        private DayTimeCycle currentDaytime = DayTimeCycle.Day;

        private void Start()
        {
            shiftCountDown = changeAfterTime - shiftTime;
            elapseTime = shiftTime;
        }

        private void LateUpdate()
        {
            DayShiftTimer();

            DayLerpTimer();
        }

        private void DayShiftTimer()
        {
            shiftCountDown -= Time.deltaTime;
            if (shiftCountDown > 0) { return; }
            canChangeNextDay = true;
            switch (currentDaytime)
            {
                case DayTimeCycle.Day:
                    currentDaytime = DayTimeCycle.Sunset;
                    break;
                case DayTimeCycle.Sunset:
                    currentDaytime = DayTimeCycle.Night;
                    break;
                case DayTimeCycle.Night:
                    currentDaytime = DayTimeCycle.Day;
                    break;
            }
            shiftCountDown = changeAfterTime;
            elapseTime = 0;
        }

        private void DayLerpTimer()
        {
            if (!canChangeNextDay) return;

            elapseTime += Time.deltaTime;
            elapseTime = Mathf.Clamp(elapseTime, 0, shiftTime);
            float time = elapseTime / shiftTime;
            time = Mathf.Clamp01(time);

            LerpTimeDay(time);

            if (time == 1) canChangeNextDay = false;
        }

        private void LerpTimeDay(float time)
        {
            switch (currentDaytime)
            {
                case DayTimeCycle.Day:
                    for (int i = 0; i < nightItems.Count; i++)
                        nightItems[i].alpha = 1 - time;

                    for (int i = 0; i < dayItems.Count; i++)
                        dayItems[i].alpha = time;
                    break;
                case DayTimeCycle.Sunset:
                    for (int i = 0; i < dayItems.Count; i++)
                        dayItems[i].alpha = 1 - time;

                    for (int i = 0; i < sunsetItems.Count; i++)
                        sunsetItems[i].alpha = time;
                    break;
                case DayTimeCycle.Night:
                    for (int i = 0; i < sunsetItems.Count; i++)
                        sunsetItems[i].alpha = 1 - time;

                    for (int i = 0; i < nightItems.Count; i++)
                        nightItems[i].alpha = time;
                    break;
            }
        }

        public enum DayTimeCycle
        {
            Day,
            Sunset,
            Night
        }
    }
}