﻿using Assets.Scripts.Monster.Skill;
using Assets.Scripts.Scriptables;
using Assets.Scripts.UI.Utilities;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIMonsterSkillDetail : UIMenuStruct
    {
        [Serializable]
        private class UISkillParameters
        {
            [SerializeField] private TextMeshProUGUI parameterText;
            [SerializeField] private Image parameterRankImage;
            [SerializeField] private Image parameterFillImage;
            [SerializeField] private Slider parameterSlider;
            [SerializeField] private Color parameterColor;

            public TextMeshProUGUI ParameterText => parameterText;
            public Image ParameterRankImage => parameterRankImage;
            public Image ParameterFillImage => parameterFillImage;
            public Slider ParameterSlider => parameterSlider;
            public Color ParameterColor => parameterColor;
        }

        [Serializable]
        private class UISkillType
        {
            [SerializeField] private Skill.SkillGenType skillType;
            [SerializeField] private Color skillTypeColor;

            public Skill.SkillGenType SkillType => skillType;
            public Color SkillTypeColor => skillTypeColor;
        }
        [Header("Skill infos")]
        [SerializeField] private Image skillImage;
        [SerializeField] private Image skillRankImage;
        [SerializeField] private Image skillElementImage;
        [SerializeField] private Image skillElementBackground;
        [SerializeField] private Image skillTypeBackground;
        [SerializeField] private TextMeshProUGUI skillNameText;
        [SerializeField] private TextMeshProUGUI skillDescriptionText;
        [SerializeField] private TextMeshProUGUI skillLevelText;
        [SerializeField] private TextMeshProUGUI skillCostText;
        [SerializeField] private TextMeshProUGUI skillConsumeText;
        [SerializeField] private TextMeshProUGUI skillDelayText;
        [SerializeField] private TextMeshProUGUI skillTypeText;
        [SerializeField] private TextMeshProUGUI skillRangeText;
        [SerializeField] private TextMeshProUGUI skillOccurType;
        [SerializeField] private TextMeshProUGUI skillScopeOfEffectText;
        [SerializeField] private Transform skillBuffContainer;
        [SerializeField] private Transform skillDebuffContainer;
        [SerializeField] private UISkillBuffPool skillBuffPool;
        [Header("HP dmg, ST dmg, Crit, Hit, HP rev, ST rev, St delay dmg")]
        [SerializeField] private List<UISkillParameters> skillParameters = new();
        [SerializeField] private List<UISkillType> skillTypeColor = new();
        [SerializeField] private List<UIElementsScriptable> elementsData = new();
        private List<UIMonsterSkillDetailBuff> skillBuffDetailList = new();
        private Dictionary<Skill.SkillGenType, Color> skillTypeColorDict = new();
        private Dictionary<AttributeType, UIElementsScriptable> skillElementColorDict = new();

        public override void Start()
        {
            base.Start();
            for (int i = 0; i < skillTypeColor.Count; i++)
            {
                if (!skillTypeColorDict.TryAdd(skillTypeColor[i].SkillType, skillTypeColor[i].SkillTypeColor))
                {
                    Debug.Log($"Failed to add {skillTypeColor[i].SkillType}");
                }
            }

            for (int i = 0; i < elementsData.Count; i++)
            {
                if (!skillElementColorDict.TryAdd(elementsData[i].Element, elementsData[i]))
                {
                    Debug.Log($"Failed to add {elementsData[i].Element}");
                }
            }
            gameObject.SetActive(false);
        }

        public void SetSkillDisplay(SkillDataScriptObj skillScriptable, UnitAbility unitAbilityScriptable, int skillLevel)
        {
            Skill.SkillDetail skillDetail = skillScriptable.Skill.skillDetails[skillLevel - 1];

            skillImage.sprite = unitAbilityScriptable.GetIconSprite();
            skillRankImage.sprite = RankText.Instance.GetRank(skillDetail.rankType);
            if(skillElementColorDict.TryGetValue(skillDetail.attributeType, out UIElementsScriptable elementScriptable))
            {
                skillElementImage.sprite = elementScriptable.ElementIcon;
                skillElementImage.color = Color.white;
                skillElementBackground.color = elementScriptable.ElementColor;
            }
            else
            {
                skillElementImage.sprite = null;
                skillElementImage.color = Color.clear;
                skillElementBackground.color = Color.white;
            }

            if(skillTypeColorDict.TryGetValue(skillScriptable.Skill.skillGenType,out Color skillTypeColor))
            {
                skillTypeBackground.color = skillTypeColor;
            }
            else
            {
                skillTypeBackground.color = Color.white;
            }

            skillNameText.text = skillScriptable.Skill.skillName;
            skillDescriptionText.text = skillScriptable.Skill.Description;

            skillLevelText.text = $"Level {skillLevel}/{skillScriptable.Skill.skillDetails.Count}";
            skillConsumeText.text = SpecialFloatToInt.Convert(skillDetail.consumedST).ToString() + " ST";
            skillDelayText.text = SpecialFloatToInt.Convert(skillDetail.delay).ToString();
            skillTypeText.text = SkillAttribute.GetSkillGenTypeString(skillScriptable.Skill.skillGenType);
            skillRangeText.text = SpecialFloatToInt.Convert(skillDetail.range).ToString();
            skillOccurType.text = AreaScopeOfEffectConvert.Instance.ConvertScopeEng(skillDetail.occurrenceType);
            skillScopeOfEffectText.text = $"{AreaScopeOfEffectConvert.Instance.ConvertScopeEng(skillDetail.areaOfEffectType)}: {skillDetail.areaOfEffectRange}";
            skillCostText.text = skillDetail.cost.ToString();

            SetSkillParameter(0, skillDetail.hp_dmg, 250, Skill.Get_Skill_HP_DMG_Rank(skillDetail.hp_dmg));
            SetSkillParameter(1, skillDetail.st_dmg, 35, Skill.Get_Skill_ST_DMG_Rank(skillDetail.st_dmg));
            SetSkillParameter(2, skillDetail.crt, 50, Skill.Get_Skill_CRT_Rank(skillDetail.crt), true);

            SetSkillParameter(3, skillDetail.hit, 80, Skill.Get_Skill_HIT_Rank(skillDetail.hit), true);
            SetSkillParameter(4, skillDetail.hp_recovery, 40, Skill.Get_Skill_HP_HEAL_Rank(skillDetail.hp_recovery), true);
            SetSkillParameter(5, skillDetail.st_recovery, 40, Skill.Get_Skill_ST_HEAL_Rank(skillDetail.st_recovery));
            SetSkillParameter(6, skillDetail.delayDamage, 25, Skill.Get_Skill_DELAY_DMG_Rank(skillDetail.delayDamage));

            SetBuffDetail(skillDetail);
            SetDebuffDetail(skillDetail);
        }

        private void SetBuffDetail(Skill.SkillDetail skillDetail)
        {
            for (int i = 0; i < skillDetail.skillBuffDetails.Count; i++)
            {
                UIMonsterSkillDetailBuff item = skillBuffPool.Pool.Get();
                SkillBuffImageManagerScriptObj.BuffEffectsData buffInfoData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetBuffEffectData(skillDetail.skillBuffDetails[i].monsterSpecialBufferType);
                //item.SetBuffDetail(buffInfoData.BuffSprite, buffInfoData.BuffDescription, skillScriptable.Skill.skillDetails[^skillLevel].skillBuffDetails[i].value);
                if (buffInfoData.IsSkillDependent)
                    item.SetEffectDetail(buffInfoData.BuffSprite, buffInfoData.BuffJPName, SkillBuffImageManagerScriptObj.SkillSummaryConvert(buffInfoData.BuffSummary, skillDetail.skillBuffDetails[i].value));
                else
                    item.SetEffectDetail(buffInfoData.BuffSprite, buffInfoData.BuffJPName, buffInfoData.BuffSummary);
                item.CachedTransform.SetParent(skillBuffContainer);
                skillBuffDetailList.Add(item);
            }
        }

        private void SetDebuffDetail(Skill.SkillDetail skillDetail)
        {
            for (int i = 0; i < skillDetail.skillDeBuffDetails.Count; i++)
            {
                UIMonsterSkillDetailBuff item = skillBuffPool.Pool.Get();
                Skill.SkillDeBuffDetail debuffDetail = skillDetail.skillDeBuffDetails[i];
                SkillBuffImageManagerScriptObj.DebuffEffectsData debuffInfoData = PlayerStats.Instance.skillBuffImageManagerScriptObj.GetDebuffEffectData(debuffDetail.monsterSpecialDeBufferType);
                if (debuffInfoData.IsSkillDependent)
                    item.SetEffectDetail(debuffInfoData.DebuffSprite, debuffInfoData.DebuffJPName, SkillBuffImageManagerScriptObj.SkillSummaryConvert(debuffInfoData.DebuffSummary, debuffDetail.value), debuffDetail.value);
                else
                    item.SetEffectDetail(debuffInfoData.DebuffSprite, debuffInfoData.DebuffJPName, debuffInfoData.DebuffSummary, debuffDetail.value);
                item.CachedTransform.SetParent(skillDebuffContainer);
                skillBuffDetailList.Add(item);
            }
        }

        private void SetSkillParameter(int index, float parameterValue, float parameterMaxValue, RankTypeEnums parameterRank, bool isPercentage = false)
        {
            skillParameters[index].ParameterText.text = SpecialFloatToInt.Convert(parameterValue).ToString();
            Sprite skillRankSprite;
            if (parameterValue > 0)
                skillRankSprite = RankText.Instance.GetRank(parameterRank);
            else
                skillRankSprite = RankText.Instance.NullRank;

            skillParameters[index].ParameterRankImage.sprite = skillRankSprite;
            skillParameters[index].ParameterFillImage.color = skillParameters[index].ParameterColor;
            skillParameters[index].ParameterSlider.maxValue = parameterMaxValue;
            skillParameters[index].ParameterSlider.value = parameterValue;
            if (isPercentage) skillParameters[index].ParameterText.text += "%";
        }

        public void ClearSkillEffectData()
        {
            for (int i = 0; i < skillBuffDetailList.Count; i++)
            {
                skillBuffDetailList[i].OnDispose?.Invoke();
            }
            skillBuffDetailList.Clear();
        }
    }
}