﻿using System;
using System.Linq;
using Assets.Scripts.UI.FarmMenu;
using Assets.Scripts.UI.Utilities;
using Proyecto26;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.HomeMenu
{
    public class UIHomeMenu : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI playerName;
        [SerializeField] private TextMeshProUGUI playerGold;
        [SerializeField] private TextMeshProUGUI playerNFTMoney;
        [SerializeField] private TextMeshProUGUI playerWallet;
        [SerializeField] private UIPopupPanel popupUI;
        [SerializeField] private UIMonsterFarmDetails farmDetail;

        private void Awake()
        {
            PlayerInfo.OnBitValueChanged += UpdateMoneyValue;
        }

        private void OnDestroy()
        {
            PlayerInfo.OnBitValueChanged -= UpdateMoneyValue;
        }

        private async void Start()
        {
            playerName.text = PlayerStats.Instance.playerInfo.playerName;
            playerGold.text = $"{PlayerStats.Instance.playerInfo.bit:0}";
            //playerNFTMoney.text = PlayerStats.Instance.playerInfo.oas.ToString();
            //playerNFTMoney.text = PlayerPrefs.GetString("balance") ?? "-";

            string wallet = "";

            try
            {
                wallet = await ThirdwebManager.Instance.SDK.wallet.GetAddress();

                if (!string.IsNullOrEmpty(wallet))
                    playerWallet.text = Helpers.TruncateString(wallet);
            }
            catch
            {
                Debug.Log("Error try get wallet");
            }

            try
            {
                var token = await ThirdwebManager.Instance.SDK.wallet.GetBalance("0x9e5aac1ba1a2e6aed6b32689dfcf62a509ca96f3");

                if (!string.IsNullOrEmpty(token.displayValue))
                    playerNFTMoney.text = token.displayValue;

                Debug.Log($"Wallet value: name {token.name}/ symbol {token.symbol}/ decimals {token.decimals}/ display value {token.displayValue}/ value {token.value}");
            }
            catch
            {
                Debug.Log("Error try get balance");

                if (wallet == "")
                {
                    playerNFTMoney.text = "0";
                }
                else 
                {
                    RestClient.Get<BalanceApi>($"http://20.78.119.161/api/oas/balance?address={wallet}")
                        .Then(response =>
                        {
                            playerNFTMoney.text = response.balance;
                        })
                        .Catch(err =>
                        {
                            playerNFTMoney.text = PlayerPrefs.GetString("balance") ?? "-";
                        });
                }
            }
        }

        private void UpdateMoneyValue() => playerGold.text = $"{PlayerStats.Instance.playerInfo.bit:0}";

        public void ToBattleScene()
        {
            int monsterCount = PlayerStats.Instance.playerInfo.PlayerMonsterInfos.Count(t => !string.IsNullOrEmpty(t.usingFarmID));
            if (monsterCount <= 0)
            {
                popupUI.Setup("No monster found!", "Please add at least 1 monster to farm for battle", null, null);
                popupUI.OpenMenu();
                return;
            }
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}


[Serializable]
public class BalanceApi
{
    public string message;
    public string balance;
}