﻿using Assets.Scripts.Monster;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class UIMapAreaLoader : MonoBehaviour
    {
        [SerializeField] private UIExploreMenu exploreMenu;
        [SerializeField] private UIIntensiveTrainningMenu trainningMenu;
        private readonly List<UIAreaData> trainningAreaDataList = new();
        private readonly List<UIAreaData> exploreAreaDataList = new();

        public List<UIAreaData> TrainningAreaDataList => trainningAreaDataList;
        public List<UIAreaData> ExploreAreaDataList => exploreAreaDataList;

        private void Start()
        {
            exploreMenu.SetLoader(this);

            trainningMenu.SetLoader(this);

            LoadTrainningMap();

            SetDataToUI();
        }

        private void SetDataToUI()
        {
            SetTrainningMenu();
            SetExploreMenu();
        }

        private void SetTrainningMenu()
        {
            for (int i = 0; i < trainningMenu.MapToggles.Count; i++)
            {
                int index = i;
                UIAreaData areaData = GetTrainningMapByType(trainningMenu.MapToggles[index].AreaType);
                trainningMenu.MapToggles[index].SetAreaRank(areaData.TrainningAreaRank);
                trainningMenu.MapToggles[index].UiToggle.onValueChanged.AddListener((bool value) =>
                {
                    if (!value) return;
                    trainningMenu.SetAreaComp(areaData.AreaType);
                    trainningMenu.SetMainDataParameters(hpValue: areaData.ParameterCorrection.health,
                                                        dexValue: areaData.ParameterCorrection.dexterity,
                                                        strValue: areaData.ParameterCorrection.strenght,
                                                        agiValue: areaData.ParameterCorrection.agility,
                                                        intValue: areaData.ParameterCorrection.intelligent,
                                                        vitValue: areaData.ParameterCorrection.vitality);
                    trainningMenu.CheckRank(trainningMenu.MapToggles[index].AreaRank);
                    trainningMenu.SetDataParameters(areaData.TrainingAreaName, areaData.TrainningAreaRank, areaData.TrainningCost, areaData.RiseParameters);
                    trainningMenu.SetTrainningArea(areaData.AreaType);
                    trainningMenu.MapLocations[index].ToggleControl.isOn = true;
                    //trainningMenu.SetSkill(skillname, successRate:);
                    //trainningMenu.SetCompatibilityText(compatibility: compatibilityRate);
                });
                trainningMenu.MapLocations[index].SetLocation(areaData.TrainingAreaName);
                trainningMenu.MapLocations[index].ToggleControl.onValueChanged.AddListener((bool value) =>
                {
                    if (!value) return;
                    if (trainningMenu.MapToggles[index].UiToggle.isOn) return;
                    trainningMenu.MapToggles[index].UiToggle.isOn = true;
                });
            }
        }

        private void SetExploreMenu()
        {
            for (int i = 0; i < exploreMenu.MapToggles.Count; i++)
            {
                int index = i;
                UIAreaData areaData = GetExploreMapByType(exploreMenu.MapToggles[index].AreaType);
                exploreMenu.MapToggles[index].SetAreaRank(areaData.TrainningAreaRank);
                exploreMenu.MapToggles[index].UiToggle.onValueChanged.AddListener((bool value) =>
                {
                    if (!value) return;
                    exploreMenu.SetComp(areaData.AreaType);
                    exploreMenu.SetMainDataParameters(hpValue: areaData.ParameterCorrection.health,
                                                      dexValue: areaData.ParameterCorrection.dexterity,
                                                      strValue: areaData.ParameterCorrection.strenght,
                                                      agiValue: areaData.ParameterCorrection.agility,
                                                      intValue: areaData.ParameterCorrection.intelligent,
                                                      vitValue: areaData.ParameterCorrection.vitality);
                    exploreMenu.CheckRank(exploreMenu.MapToggles[index].AreaRank);
                    exploreMenu.SetDataParameters(areaData.TrainingAreaName, areaData.TrainningAreaRank, areaData.TrainningCost, areaData.RiseParameters);
                    exploreMenu.SetTrainningArea(areaData.AreaType);
                    exploreMenu.MapLocations[index].ToggleControl.isOn = true;
                    //exploreMenu.SetItemCollect(itemList, successRate:);
                    //exploreMenu.SetCompatibilityTextValue(compatibility: compatibilityRate);
                });
                exploreMenu.MapLocations[index].SetLocation(areaData.TrainingAreaName);
                exploreMenu.MapLocations[index].ToggleControl.onValueChanged.AddListener((bool value) =>
                {
                    if (!value) return;
                    if (exploreMenu.MapToggles[index].UiToggle.isOn) return;
                    exploreMenu.MapToggles[index].UiToggle.isOn = true;
                });
            }
        }

        private UIAreaData GetTrainningMapByType(TrainingArea.TrainingArea.TrainingAreaType area)
        {
            for (int i = 0; i < TrainningAreaDataList.Count; i++)
            {
                if (trainningAreaDataList[i].AreaType == area)
                    return trainningAreaDataList[i];
            }
            return null;
        }

        private UIAreaData GetExploreMapByType(TrainingArea.TrainingArea.TrainingAreaType area)
        {
            for (int i = 0; i < exploreAreaDataList.Count; i++)
            {
                if (exploreAreaDataList[i].AreaType == area)
                    return exploreAreaDataList[i];
            }
            return null;
        }

        private void LoadTrainningMap()
        {
            foreach (var itemKeyValue in PlayerStats.Instance.trainingAreaDataManagerScriptObj.trainingAreaDataDictionary)
            {
                //set fixed data here
                var readValue = itemKeyValue.Value.TrainingArea;

                List<(string, float)> trainningRiseParameters = new();
                trainningRiseParameters.AddRange(GetComplexStatRaise(readValue.mainParameters));
                trainningRiseParameters.AddRange(GetComplexStatRaise(readValue.subParameters));
                UIAreaData trainningAreaData = new(trainingAreaName: readValue.areaName,
                                                   trainningAreaRank: readValue.rankType,
                                                   trainningCost: readValue.AreaPrice,
                                                   riseParameters: trainningRiseParameters.Distinct().ToList(),
                                                   areaType: readValue.trainingAreaType,
                                                   terrainType: readValue.trainingTerrainType,
                                                   parameterCorrection: readValue.parametersCorrection,
                                                   mainParameters: readValue.mainParameters,
                                                   subParameters: readValue.subParameters);
                trainningAreaDataList.Add(trainningAreaData);

                List<(string, float)> exploreRiseParameters = new();
                exploreRiseParameters.AddRange(GetComplexStatRaise(readValue.explorationRiseParameter));
                UIAreaData exploreAreaData = new(trainingAreaName: readValue.areaName,
                                                 trainningAreaRank: readValue.explorationRankType,
                                                 trainningCost: readValue.explorationAreaPrice,
                                                 riseParameters: exploreRiseParameters.Distinct().ToList(),
                                                 areaType: readValue.trainingAreaType,
                                                 terrainType: readValue.trainingTerrainType,
                                                 parameterCorrection: readValue.explorationParametersCorrection,
                                                 mainParameters: readValue.explorationRiseParameter,
                                                 subParameters: null);
                exploreAreaDataList.Add(exploreAreaData);
            }
        }

        private List<(string, float)> GetComplexStatRaise(BasicParameters statParemeters)
        {
            List<(string, float)> result = new();
            if (statParemeters == null)
                return result;
            if (statParemeters.strenght > 0)
                result.Add(new("Str", statParemeters.strenght));
            if (statParemeters.intelligent > 0)
                result.Add(new("Int", statParemeters.intelligent));
            if (statParemeters.dexterity > 0)
                result.Add(new("Dex", statParemeters.dexterity));
            if (statParemeters.agility > 0)
                result.Add(new("Agi", statParemeters.agility));
            if (statParemeters.vitality > 0)
                result.Add(new("Vit", statParemeters.vitality));
            if (statParemeters.health > 0)
                result.Add(new("Hp", statParemeters.health));
            return result;
        }

        [System.Serializable]
        public class UIAreaData
        {
            private readonly string trainingAreaName;
            private readonly RankTypeEnums trainningAreaRank;
            private readonly float trainningCost;
            private readonly List<(string, float)> riseParameters;
            private readonly TrainingArea.TrainingArea.TrainingAreaType areaType;
            private readonly TrainingArea.TrainingArea.TerrainType terrain;
            private readonly BasicParameters parameterCorrection;
            private readonly BasicParameters mainParameters;
            private readonly BasicParameters subParameters;

            public UIAreaData() { }

            public UIAreaData(string trainingAreaName, RankTypeEnums trainningAreaRank, float trainningCost, List<(string, float)> riseParameters, TrainingArea.TrainingArea.TrainingAreaType areaType, TrainingArea.TrainingArea.TerrainType terrainType, BasicParameters parameterCorrection, BasicParameters mainParameters, BasicParameters subParameters)
            {
                this.trainingAreaName = trainingAreaName;
                this.trainningAreaRank = trainningAreaRank;
                this.trainningCost = trainningCost;
                this.riseParameters = riseParameters;
                this.areaType = areaType;
                this.terrain = terrainType;
                this.parameterCorrection = parameterCorrection;
                this.mainParameters = mainParameters;
                this.subParameters = subParameters;
            }

            public string TrainingAreaName => trainingAreaName;
            public RankTypeEnums TrainningAreaRank => trainningAreaRank;
            public float TrainningCost => trainningCost;
            public List<(string, float)> RiseParameters => riseParameters;
            public TrainingArea.TrainingArea.TerrainType Terrain => terrain;
            public TrainingArea.TrainingArea.TrainingAreaType AreaType => areaType;
            public BasicParameters ParameterCorrection => parameterCorrection;
            public BasicParameters MainParameters => mainParameters;
            public BasicParameters SubParameters => subParameters;

        }
    }
}