using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.TrainingArea;
using Assets.Scripts.UI.FarmMenu;
using System;
using UnityEngine;

public class UISpecialTrainingCaculator : MonoBehaviour
{
    [SerializeField]
    UIMonsterFarmDetails farmDetail;

    private void Start()
    {
        farmDetail.OnAccecpt.AddListener(SetUpStartAction);
    }

    public void SetUpStartAction()
    {

        if (farmDetail.MonsterInfoData.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.SpecialTraining)
        {
            PlayerInfo.PlayerMonsterInfo playerMonsterInfo = farmDetail.MonsterInfoData;
            MonsterDataScriptObj monsterScriptableData = farmDetail.ScriptableMonsterData;
            playerMonsterInfo.lastMonsterAction.actionDone = false;

            playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);
            playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);

            playerMonsterInfo.lastMonsterAction.weekPassed = 0;

            playerMonsterInfo.lastMonsterAction.actionWeek = 1;

            Food.FoodType foodType = playerMonsterInfo.lastFoodType;
            Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

            float price = 0f;

            price += food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            PlayerStats.Instance.playerInfo.SubtractBiT(price);

            Debug.Log("Setup Special Training Start Action for monster id = " + playerMonsterInfo.monsterID);

            CaculateTrainingAction(playerMonsterInfo, monsterScriptableData);
        }
    }

    public static void CaculateTrainingAction(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj scriptableMonsterData)
    {
        if (playerMonsterInfo.lastMonsterAction.actionDone) return;
        DoSpecialTraining(playerMonsterInfo, scriptableMonsterData);
        //DateTime startDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix);

        //Debug.Log("startDate = " + startDate);

        //int days = (int)(DateTime.UtcNow - startDate).TotalDays + 1;

        //Debug.Log("Total Days = " + days);

        //int totalWeeks = days / 7;

        //Debug.Log("total Weeks = " + totalWeeks);

        //DateTime lastDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix);

        //Debug.Log("lastDate = " + lastDate);

        //int newDays = (int)(DateTime.UtcNow - lastDate).TotalDays + 1;

        //Debug.Log("newDays = " + newDays);

        //int newWeeks = newDays / 7;

        //Debug.Log("newWeeks = " + newWeeks);

        //if (newWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
        //{
        //    newWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
        //}

        //if (totalWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
        //{
        //    totalWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
        //}

        //if (newWeeks > 0)
        //{
        //    DateTime trueLastDate = DateTime.UtcNow.AddDays(-((newDays - 1) - (7 * newWeeks)));

        //    Debug.Log("trueLastDate = " + trueLastDate);

        //    playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(trueLastDate);
        //}

        //for (int i = 0; i < newWeeks; i++)
        //{
        //    DoSpecialTraining(playerMonsterInfo, totalWeeks);
        //}

    }

    public static void DoSpecialTraining(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj scriptableMonsterData)
    {
        DebugPopup.InTraining(true);

        Debug.Log("Do Special Training");
        DebugPopup.LogRaise($"Do Special monster = {playerMonsterInfo.monsterName}");

        BasicParameters complexParameter = Monster.Get_Complex_Parameter(playerMonsterInfo, scriptableMonsterData);

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        FarmDataScriptObj farmDataScriptObj = PlayerStats.Instance.farmDataManagerScriptObj.GetFarmDataObj(playerFarmInfo.usingFarmType);

        Food.FoodType foodType = playerMonsterInfo.lastFoodType;

        Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Food selected = {food.FoodName}");
        DebugPopup.LogRaise($"Energy: E = {food.Energy}");
        DebugPopup.LogRaise($"Body: B = {food.Body}");
        DebugPopup.LogRaise($"Condition: C = {food.Condition}");

        #region Get Check Data
        //Check
        //////////////////////////////////////////
        //Main race, sub race stats

        int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.Monster.mainSuitTerrain, scriptableMonsterData.Monster.mainNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(scriptableMonsterData.Monster.subSuitTerrain, scriptableMonsterData.Monster.subNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        //Check Fatigue
        Monster.FatigueType fatigueType = Monster.Get_Fatigue_Type_By_Value(playerMonsterInfo.growthParameters.fatigue);

        //Check Stress
        Monster.StressType stressType = Monster.Get_Stress_Type_By_Value(playerMonsterInfo.growthParameters.stress);

        Monster.InjuryType lastInjuryType = playerMonsterInfo.growthParameters.injuryType;
        bool injuryChange = false;

        Monster.DiseasesType lastDiseasesType = playerMonsterInfo.growthParameters.diseasesType;
        bool diseasesChange = false;

        //Growth Type
        Monster.GrowthType growthType = scriptableMonsterData.Monster.growthType;

        GrowthParameters newGrowthParameter = playerMonsterInfo.growthParameters.Clone();

        //////////////////////////////////////////

        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Farm terrain = {farmDataScriptObj.Farm.TerrainType}");
        DebugPopup.LogRaise($"Monster Main Suit Terrain = {scriptableMonsterData.Monster.mainSuitTerrain}");
        DebugPopup.LogRaise($"Monster Main No Suit Terrain = {scriptableMonsterData.Monster.mainNoSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub Suit Terrain = {scriptableMonsterData.Monster.subSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub No Suit Terrain = {scriptableMonsterData.Monster.subNoSuitTerrain}");
        //End Check
        #endregion

        //Characteristic Correction

        InnateTrait.InnateTraitInfo innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);
        DebugPopup.LogRaise($"Innate Trait = {playerMonsterInfo.innateTrait}");

        int passWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;

        #region Meal

        //Energy
        newGrowthParameter.AddEnergy(-5f);
        newGrowthParameter.AddEnergy(food.Energy);

        //Body
        newGrowthParameter.AddBody(-5f);
        newGrowthParameter.AddBody(food.Body);

        //Condition
        newGrowthParameter.AddCondition(-5f);
        newGrowthParameter.AddCondition(food.Condition);

        #region Caculate Fatigue

        Vector2 fatigueMinMax = Monster.Get_Fatigue_Min_Max();

        newGrowthParameter.AddFatigue(food.Fatigue);

        if (scriptableMonsterData.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddFatigue(-1);
        }

        fatigueType = Monster.Get_Fatigue_Type_By_Value(newGrowthParameter.fatigue);

        #endregion Caculate Fatigue

        #region Caculate Stress

        Vector2 stressMinMax = Monster.Stress_Min_Max;

        newGrowthParameter.AddStress(food.Stress);

        if (scriptableMonsterData.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddStress(-1);
        }

        stressType = Monster.Get_Stress_Type_By_Value(newGrowthParameter.stress);

        #endregion Caculate Stress

        #region Training Policy

        Vector2 trainingPolicyMinMax = Monster.Get_TrainingPolicy_Value_MinMax();

        if (scriptableMonsterData.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(0));
        }
        else
        if (scriptableMonsterData.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(1));
        }

        newGrowthParameter.AddTrainingPolicyValue(-1);

        #endregion Training Policy

        #region Affection

        Vector2 affectionMinMax = Monster.Get_Affection_Value_MinMax();

        if (scriptableMonsterData.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(0));
        }

        if (scriptableMonsterData.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(1));
        }

        #endregion Affection

        #region Body Type

        Vector2 bodyTypeValueMinMax = Monster.Get_BodyType_Value_MinMax();

        newGrowthParameter.AddBodyTypeValue(food.BodyTypeValue);

        #endregion Body Type

        #endregion Meal

        #region Action

        #region Caculate training success or fail

        float successTraitCorrection = innateTrait.Training_Success_Rate;

        float failTraitCorrection = innateTrait.Training_Fail_Rate;

        TrainingArea.TrainingType trainingType = TrainingArea.Get_Training_Sucess_Failure_Probability(newGrowthParameter.trainingPolicyValue,
                                                                                                      newGrowthParameter.affection,
                                                                                                      successTraitCorrection, failTraitCorrection, newGrowthParameter.injuryType, newGrowthParameter.diseasesType);



        #endregion Caculate training success or fail

        #region Caculate Fatigue

        float fatigueTraitCorrection = innateTrait.Training_Fatigue_Reduction;

        newGrowthParameter.AddFatigue(Monster.Get_Fatigue_Increase_Caculated(Monster.Get_Fatigue_IncreaseOrDecrease_Value(1),
                                                                      Monster.Get_Fatigue_Farm_Reduction_Enhancement_Value(farmDataScriptObj.Farm.fatigueEnhancValue),
                                                                      mainRaceStat, subRaceStat, fatigueTraitCorrection,
                                                                      newGrowthParameter.bodyTypeValue,
                                                                      newGrowthParameter.trainingPolicyValue));

        fatigueType = Monster.Get_Fatigue_Type_By_Value(newGrowthParameter.fatigue);
        DebugPopup.LogRaise($"Fatigue Type = {fatigueType}");

        #endregion Caculate Fatigue

        #region Caculate Stress

        float stressTraitCorrection = innateTrait.Training_Stress_Reduction;

        newGrowthParameter.AddStress(Monster.Get_Stress_Increase_Caculated(Monster.Get_Stress_IncreaseOrDecrease_Value(1),
                                                                    Monster.Get_Stress_Farm_Reduction_Enhancement_Value(farmDataScriptObj.Farm.stressEnhancValue),
                                                                    mainRaceStat, subRaceStat, stressTraitCorrection,
                                                                    newGrowthParameter.bodyTypeValue,
                                                                    newGrowthParameter.trainingPolicyValue));

        stressType = Monster.Get_Stress_Type_By_Value(newGrowthParameter.stress);
        DebugPopup.LogRaise($"Stress Type = {stressType}");

        #endregion Caculate Stress

        #region Affection

        if (fatigueType == Monster.FatigueType.Fatigue)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(6));
        }
        else if (fatigueType == Monster.FatigueType.Overwork)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(8));
        }

        if (stressType == Monster.StressType.Stress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(9));
        }
        else if (stressType == Monster.StressType.OverStress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(11));
        }

        if (trainingType != TrainingArea.TrainingType.failure)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(2));
        }
        else
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(3));
        }

        #endregion Affection

        #region Body Type

        newGrowthParameter.AddBodyTypeValue(Monster.Get_BodyType_IncreaseOrDecrease_Value(1));

        #endregion Body Type

        #endregion Action

        /////////////////////////////////////////

        # region Caculate Injury

        ///100% = 1f;
        float totalInjuryProbability = 0f;

        if (trainingType == TrainingArea.TrainingType.failure)
        {
            totalInjuryProbability += Monster.Get_Injuries_Probability(1);
        }

        float injuryTraitCorrection = innateTrait.Injury_Probability;

        //Caculate monster will be injury or not
        {
            Vector2 injuryRate = Monster.Get_Injury_Probability_Rate(totalInjuryProbability, injuryTraitCorrection, fatigueType, 0f);

            newGrowthParameter.injuryType = Monster.Get_Injury_Caculate_Is_Injury(newGrowthParameter.injuryType, injuryRate.x, injuryRate.y);

            if (newGrowthParameter.injuryType != lastInjuryType)
            {
                injuryChange = true;
            }
        }
        DebugPopup.LogRaise($"Injury Type = {newGrowthParameter.injuryType}");

        #endregion Caculate Injury

        #region Caculate Disease

        //Probability

        float numberOfSickMonster = 0f;

        float diseaseTraitCorrection = innateTrait.Disease_Probability;

        //Caculate monster will be disease or not
        {
            Vector2 diseasesRate = Monster.Get_Disease_Probability_Rate
                (newGrowthParameter.energy, newGrowthParameter.body, newGrowthParameter.condition,
                numberOfSickMonster,
                diseaseTraitCorrection,
                stressType
                );

            newGrowthParameter.diseasesType = Monster.Get_Disease_Caculate_Is_Disease(newGrowthParameter.diseasesType, diseasesRate.x, diseasesRate.y);

            if (newGrowthParameter.diseasesType != lastDiseasesType)
            {
                diseasesChange = true;
            }
        }
        DebugPopup.LogRaise($"Disease Type = {newGrowthParameter.diseasesType}");

        #endregion Caculate Disease

        /////////////////////////////////////////

        #region Caculate LifeSpan

        //Check life span concum percent if it is fatigue or stress

        newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Consumption(passWeeks, -8f, fatigueType, stressType));

        if (newGrowthParameter.injuryType == Monster.InjuryType.Injury)
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(4));
        else if (newGrowthParameter.injuryType == Monster.InjuryType.Serious_Injury)
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(5));

        if (newGrowthParameter.diseasesType == Monster.DiseasesType.Diseases)
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(6));
        else if (newGrowthParameter.diseasesType == Monster.DiseasesType.Serious_Diseases)
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(7));

        #endregion Caculate LifeSpan

        #region Calculate Parameter Rise

        BasicParameters caculatedRaiseParameter = CaculateParameterRise
            (playerMonsterInfo, scriptableMonsterData, complexParameter, farmDataScriptObj, mainRaceStat, subRaceStat,
            newGrowthParameter.injuryType, newGrowthParameter.diseasesType, growthType, trainingType,
            farmDataScriptObj.Farm.mainParameter, farmDataScriptObj.Farm.subParameter, farmDataScriptObj.Farm.deParameter);

        #endregion Calculate Parameter Rise

        BasicParameters newComplexParameters = Monster.Get_Complex_Parameter(complexParameter, caculatedRaiseParameter);

        #region Update action infos

        //Get changed value between old and new
        GrowthParameters actionGrowthParameter = new()
        {
            lifeSpan = newGrowthParameter.lifeSpan - playerMonsterInfo.growthParameters.lifeSpan,
            bodyTypeValue = newGrowthParameter.bodyTypeValue - playerMonsterInfo.growthParameters.bodyTypeValue,
            trainingPolicyValue = newGrowthParameter.trainingPolicyValue - playerMonsterInfo.growthParameters.trainingPolicyValue,
            affection = newGrowthParameter.affection - playerMonsterInfo.growthParameters.affection,
            fatigue = newGrowthParameter.fatigue - playerMonsterInfo.growthParameters.fatigue,
            stress = newGrowthParameter.stress - playerMonsterInfo.growthParameters.stress,
            injuryType = newGrowthParameter.injuryType,
            diseasesType = newGrowthParameter.diseasesType,

            energy = newGrowthParameter.energy - playerMonsterInfo.growthParameters.energy,
            body = newGrowthParameter.body - playerMonsterInfo.growthParameters.body,
            condition = newGrowthParameter.condition - playerMonsterInfo.growthParameters.condition
        };

        //End changed value between old and new

        UpdateActionChange(playerMonsterInfo, caculatedRaiseParameter, actionGrowthParameter, injuryChange, diseasesChange, passWeeks, trainingType);

        #endregion

        #region Apply change to playerstats

        playerMonsterInfo.growthParameters = newGrowthParameter;

        playerMonsterInfo.basicParameters = Monster.Get_Subtract_Parameter(newComplexParameters, scriptableMonsterData.Monster.basicParameters);

        playerMonsterInfo.rankType = Monster.Get_Monster_BasicParameters_Rank_By_Value(newComplexParameters);

        //PlayerStats.instance.Save();

        #endregion

        DebugPopup.InTraining(false);
    }

    /// <summary>
    /// <para> Basic training has two basic parameters raised, the main parameter is +8 and the sub-parameter is +4. </para>
    /// <para> 4.1.8 Training in english document </para>
    /// <para> basicTrainType from 1 = heath to 6 fit 6 basic parameters </para>
    /// </summary>
    /// <param name="playerMonsterInfo"></param>
    /// <param name="basicMonsterDataScriptObj"></param>
    /// <param name="farmDataScriptObj"></param>
    /// <param name="characteristicCorrection"></param>
    /// <param name="injuryType"></param>
    /// <param name="diseaseType"></param>
    /// <param name="growthType"></param>
    /// <param name="trainingType"></param>
    /// <param name="basicTrainType"></param>
    /// <returns></returns>
    public static BasicParameters CaculateParameterRise(
        PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj basicMonsterDataScriptObj,
        BasicParameters complexParameter, FarmDataScriptObj farmDataScriptObj, int mainRaceStat, int subRaceStat,
        Monster.InjuryType injuryType, Monster.DiseasesType diseaseType,
        Monster.GrowthType growthType, TrainingArea.TrainingType trainingType, ParameterType mainParameter,
        ParameterType subParameter, ParameterType downParameter)
    {

        float mainParameterTrainValue   = 12f;
        float subParameterTrainValue    = 6f;
        float downParameterTrainValue   = -6f;

        float m_health_RiseValue = 0f;
        float s_health_RiseValue = 0f;
        float d_health_RiseValue = 0f;

        float m_strenght_RiseValue = 0f;
        float s_strenght_RiseValue = 0f;
        float d_strenght_RiseValue = 0f;

        float m_intelligent_RiseValue = 0f;
        float s_intelligent_RiseValue = 0f;
        float d_intelligent_RiseValue = 0f;

        float m_dexterity_RiseValue = 0f;
        float s_dexterity_RiseValue = 0f;
        float d_dexterity_RiseValue = 0f;

        float m_agility_RiseValue = 0f;
        float s_agility_RiseValue = 0f;
        float d_agility_RiseValue = 0f;

        float m_vitality_RiseValue = 0f;
        float s_vitality_RiseValue = 0f;
        float d_vitality_RiseValue = 0f;

        PersonalityTrait.PersonalityTraitInfo trainingTrait = PersonalityTrait.GetTraitInfo(playerMonsterInfo.personalityTrait);

        DebugPopup.LogRaise($"PersonalityTrait = {playerMonsterInfo.personalityTrait}");

        switch (mainParameter)
        {
            case ParameterType.HP:
                {
                    //Main Health
                    DebugPopup.LogRaise("Main Health");
                    m_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.health, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.health, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.STR:
                {
                    //Main Strenght
                    DebugPopup.LogRaise("Main Strenght");
                    m_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.strenght, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.strenght, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.INT:
                {
                    //Main intelligent
                    DebugPopup.LogRaise("Main intelligent");
                    m_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.intelligent, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.intelligent, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.DEX:
                {
                    //Main dexterity
                    DebugPopup.LogRaise("Main dexterity");
                    m_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.dexterity, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.dexterity, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.AGI:
                {
                    //Main agility
                    DebugPopup.LogRaise("Main agility");
                    m_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.agility, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.health, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.VIT:
                {
                    //Main vitality
                    DebugPopup.LogRaise("Main vitality");
                    m_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        mainParameterTrainValue, basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.vitality, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.vitality, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }
        }

        switch (subParameter)
        {
            case ParameterType.HP:
                {
                    //Sub Health
                    DebugPopup.LogRaise("Sub Health");
                    s_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.health, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.health, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.STR:
                {
                    //Sub Strenght
                    DebugPopup.LogRaise("Sub Strenght");
                    s_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.strenght, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.strenght, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.INT:
                {
                    //Sub intelligent
                    DebugPopup.LogRaise("Sub intelligent");
                    s_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.intelligent, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.intelligent, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.DEX:
                {
                    //Sub dexterity
                    DebugPopup.LogRaise("Sub dexterity");
                    s_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.dexterity, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.dexterity, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.AGI:
                {
                    //Sub agility
                    DebugPopup.LogRaise("Sub agility");
                    s_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.agility, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.agility, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.VIT:
                {
                    //Sub vitality
                    DebugPopup.LogRaise("Sub vitality");
                    s_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up(
                        subParameterTrainValue, basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.vitality, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.vitality, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }
        }

        switch (downParameter)
        {
            case ParameterType.HP:
                {
                    //Down Health
                    DebugPopup.LogRaise("Down Health");
                    d_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.health, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.health, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.STR:
                {
                    //Down Strenght
                    DebugPopup.LogRaise("Down Strenght");
                    d_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.strenght, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.strenght, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.INT:
                {
                    //Down intelligent
                    DebugPopup.LogRaise("Down intelligent");
                    d_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.intelligent, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.intelligent, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.DEX:
                {
                    //Down dexterity
                    DebugPopup.LogRaise("Down dexterity");
                    d_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.dexterity, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.dexterity, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.AGI:
                {
                    //Down agility
                    DebugPopup.LogRaise("Down agility");
                    d_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.agility, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.agility, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }

            case ParameterType.VIT:
                {
                    //Down vitality
                    DebugPopup.LogRaise("Down vitality");
                    d_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Down(
                        downParameterTrainValue, basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                        farmDataScriptObj.Farm.enhancedParameters.vitality, mainRaceStat, subRaceStat,
                        trainingTrait.TrainingCorrection.vitality, injuryType, diseaseType, growthType,
                        playerMonsterInfo.growthParameters.lifeSpan,
                        basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan, complexParameter, trainingType);
                    break;
                }
        }

        #region Add to Complex parameter

        BasicParameters raiseParameters = new()
        {
            health      = m_health_RiseValue + s_health_RiseValue + d_health_RiseValue,
            strenght    = m_strenght_RiseValue + s_strenght_RiseValue + d_strenght_RiseValue,
            intelligent = m_intelligent_RiseValue + s_intelligent_RiseValue + d_intelligent_RiseValue,
            dexterity   = m_dexterity_RiseValue + s_dexterity_RiseValue + d_dexterity_RiseValue,
            agility     = m_agility_RiseValue + s_agility_RiseValue + d_agility_RiseValue,
            vitality    = m_vitality_RiseValue + s_vitality_RiseValue + d_vitality_RiseValue
        };

        #endregion

        return raiseParameters;
    }

    public static void UpdateActionChange(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, BasicParameters changeParameters, GrowthParameters changeGrowthParameters, bool injuryChange, bool diseasesChange, int totalWeekPassed, TrainingArea.TrainingType trainingType)
    {
        playerMonsterInfo.lastMonsterAction.weekPassed = totalWeekPassed;

        #region Basic parameters
        playerMonsterInfo.lastMonsterAction.basicParameters.health      += changeParameters.health;
        playerMonsterInfo.lastMonsterAction.basicParameters.strenght    += changeParameters.strenght;
        playerMonsterInfo.lastMonsterAction.basicParameters.intelligent += changeParameters.intelligent;
        playerMonsterInfo.lastMonsterAction.basicParameters.dexterity   += changeParameters.dexterity;
        playerMonsterInfo.lastMonsterAction.basicParameters.agility     += changeParameters.agility;
        playerMonsterInfo.lastMonsterAction.basicParameters.vitality    += changeParameters.vitality;
        #endregion

        #region Growth parameters
        playerMonsterInfo.lastMonsterAction.growthParameters.lifeSpan               += changeGrowthParameters.lifeSpan;
        playerMonsterInfo.lastMonsterAction.growthParameters.bodyTypeValue          += changeGrowthParameters.bodyTypeValue;
        playerMonsterInfo.lastMonsterAction.growthParameters.trainingPolicyValue    += changeGrowthParameters.trainingPolicyValue;
        playerMonsterInfo.lastMonsterAction.growthParameters.affection              += changeGrowthParameters.affection;
        playerMonsterInfo.lastMonsterAction.growthParameters.fatigue                += changeGrowthParameters.fatigue;
        playerMonsterInfo.lastMonsterAction.growthParameters.stress                 += changeGrowthParameters.stress;

        if (injuryChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.injuryType = changeGrowthParameters.injuryType;

        if (diseasesChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.diseasesType = changeGrowthParameters.diseasesType;
        #endregion

        #region Meal parameters
        playerMonsterInfo.lastMonsterAction.growthParameters.energy += changeGrowthParameters.energy;
        playerMonsterInfo.lastMonsterAction.growthParameters.body += changeGrowthParameters.body;
        playerMonsterInfo.lastMonsterAction.growthParameters.condition += changeGrowthParameters.condition;
        #endregion

        playerMonsterInfo.lastMonsterAction.trainingType = trainingType;

        playerMonsterInfo.lastMonsterAction.actionDone = true;

        PlayerStats.Instance.Save();
    }
}
