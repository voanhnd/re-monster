﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class MapPointers : MonoBehaviour
    {
        [SerializeField] private List<MapPointHover> mapPoints = new();

        public void SetTargetPointer(int index)
        {
            SetFocusMap(index);
        }

        private void SetFocusMap(int index)
        {
            for (int i = 0; i < mapPoints.Count; i++)
            {
                if (i == index) continue;
                mapPoints[i].TargetIsland.gameObject.SetActive(false);
                mapPoints[i].MapLocationPoint.ToggleControl.isOn = false;
            }
            mapPoints[index].TargetIsland.gameObject.SetActive(true);
        }

        [System.Serializable]
        public class MapPointHover
        {
            [SerializeField] private UITrainingMapLocation mapLocationPoint;
            [SerializeField] private Image targetIsland;

            public UITrainingMapLocation MapLocationPoint => mapLocationPoint;
            public Image TargetIsland => targetIsland;
        }
    }
}