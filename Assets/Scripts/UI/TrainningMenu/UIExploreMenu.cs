﻿using Assets.Scripts.Monster;
using Assets.Scripts.UI.FarmMenu;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class UIExploreMenu : MonoBehaviour
    {
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private RectTransform container;
        [SerializeField] private UnityEvent onMenuOpen;
        [SerializeField] private UnityEvent onMenuClose;

        [Header("Animation values")]
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Ease openEase;
        [SerializeField] private Ease closeEase;

        [Header("Interaction buttons")]
        [SerializeField] private Button accecptButton;
        [SerializeField] private Button denyButton;

        [Header("Explore parameter datas")]
        [SerializeField] private TextMeshProUGUI exploreLocationText;
        [SerializeField] private TextMeshProUGUI costText;
        [SerializeField] private TextMeshProUGUI compatibilityText;
        [SerializeField] private TextMeshProUGUI riseParametersText;
        [SerializeField] private TextMeshProUGUI canAcquireItemText;
        [SerializeField] private Image exploreRankImage;

        [Header("Basic parameters")]
        [SerializeField] private TextMeshProUGUI hpParaText;
        [SerializeField] private TextMeshProUGUI dexParaText;
        [SerializeField] private TextMeshProUGUI strParaText;
        [SerializeField] private TextMeshProUGUI agiParaText;
        [SerializeField] private TextMeshProUGUI intParaText;
        [SerializeField] private TextMeshProUGUI vitParaText;
        [SerializeField] private TextMeshProUGUI itemCollectText;
        [SerializeField] private TextMeshProUGUI collectSuccessRateText;
        [SerializeField] private List<UIMapToggle> mapToggles = new();
        [SerializeField] private List<UITrainingMapLocation> mapLocations = new();
        private string monsterId;
        private UIMonsterFarmDetails farmDetails;
        private TrainingArea.TrainingArea.TrainingAreaType currentArea;
        private UIMapAreaLoader loader;
        private float exploreCost = 0;

        public List<UIMapToggle> MapToggles => mapToggles;
        public List<UITrainingMapLocation> MapLocations => mapLocations;

        private void Awake()
        {
            accecptButton.onClick.AddListener(() =>
            {
                CloseMenu();
                AccecptExplore();
            });
            denyButton.onClick.AddListener(() =>
            {
                CloseMenu();
                DenyExplore();
                currentArea = TrainingArea.TrainingArea.TrainingAreaType.Empty;
            });
        }

        private void Start()
        {
            container.sizeDelta = closeSize;
            gameObject.SetActive(false);
        }

        private void DenyExplore()
        {
            currentArea = TrainingArea.TrainingArea.TrainingAreaType.Empty;
            exploreCost = 0;
            farmDetails.RaiseActionComponents.SetExploreAction(currentArea, exploreCost);
        }

        private void AccecptExplore() => farmDetails.RaiseActionComponents.SetExploreAction(currentArea, exploreCost);

        public void OpenMenu()
        {
            gameObject.SetActive(true);
            container.DOSizeDelta(openSize, openTime).SetEase(openEase).OnComplete(() =>
            {
                onMenuOpen.Invoke();
                SetInitArea();
            });
        }

        public void CloseMenu()
        {
            container.DOSizeDelta(closeSize, closeTime).SetEase(closeEase).OnComplete(() =>
            {
                onMenuClose.Invoke();
                gameObject.SetActive(false);
            });
        }

        internal void SetDataParameters(string exploreLocation, RankTypeEnums areaRank, float cost, List<(string,float)> riseParameters)
        {
            exploreLocationText.text = exploreLocation;
            exploreRankImage.sprite = RankText.Instance.GetRank(areaRank);
            costText.text = $"{cost} B";
            exploreCost = cost;
            AddRiseText(riseParameters);
        }

        public void SetItemCollect(List<string> itemAquire, float successRate)
        {
            canAcquireItemText.text = (!(itemAquire == null || itemAquire.Count == 0)).ToString();
            if ((itemAquire == null || itemAquire.Count == 0))
            {
                itemCollectText.text = "";
                collectSuccessRateText.text = "";
            }
            else
            {
                AddItemGetString(itemAquire);
                collectSuccessRateText.text = $"{successRate} %";
            }
        }

        private void AddItemGetString(List<string> itemAquire)
        {
            string itemsString = string.Empty;
            for (int i = 0; i < itemAquire.Count; i++)
            {
                itemsString += itemAquire[i].Trim();
                if (i < itemAquire.Count - 1)
                {
                    itemsString += ", ";
                }
            }
            itemCollectText.text = itemsString + ".";
        }

        public void SetMainDataParameters(float hpValue, float dexValue, float strValue, float agiValue, float intValue, float vitValue)
        {
            hpParaText.text = $"{hpValue} %";
            dexParaText.text = $"{dexValue} %";
            strParaText.text = $"{strValue} %";
            agiParaText.text = $"{agiValue} %";
            intParaText.text = $"{intValue} %";
            vitParaText.text = $"{vitValue} %";
        }


        private void AddRiseText(List<(string, float)> riseParameters)
        {
            string riseText = "<color=green>";
            for (int i = 0; i < riseParameters.Count; i++)
            {
                riseText += $"{riseParameters[i].Item1}: {riseParameters[i].Item2}  ";
            }
            riseText.Trim();
            riseText += "</color>";
            riseParametersText.text = riseText;
        }

        public void SetTargetMonster(UIMonsterFarmDetails farmDetails)
        {
            this.monsterId = farmDetails.MonsterId;
            this.farmDetails = farmDetails;
        }

        public void SetComp(TrainingArea.TrainingArea.TrainingAreaType currentArea)
        {
            if (string.IsNullOrEmpty(monsterId)) return;
            PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);
            for (int i = 0; i < loader.TrainningAreaDataList.Count; i++)
            {
                if (loader.TrainningAreaDataList[i].AreaType == currentArea)
                {
                    int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterScriptable.Monster.mainSuitTerrain, monsterScriptable.Monster.mainNoSuitTerrain, loader.TrainningAreaDataList[i].Terrain);

                    int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterScriptable.Monster.subSuitTerrain, monsterScriptable.Monster.subNoSuitTerrain, loader.TrainningAreaDataList[i].Terrain);

                    SetCompatibilityText(new Vector2(mainRaceStat, subRaceStat));
                }
            }
        }

        private void SetCompatibilityText(Vector2 raceComp)
        {
            string mainComp = "-";
            if (raceComp.x == 1)
            {
                mainComp = "<color=green>good</color>";
            }
            if (raceComp.x == -1)
            {
                mainComp = "<color=red>bad</color>";
            }
            string subComp = "-";
            if (raceComp.y == 1)
            {
                subComp = "<color=green>good</color>";
            }
            if (raceComp.y == -1)
            {
                subComp = "<color=red>bad</color>";
            }
            compatibilityText.text = $"main: {mainComp} sub: {subComp}";
        }

        public void SetTrainningArea(TrainingArea.TrainingArea.TrainingAreaType areaType) => this.currentArea = areaType;

        public void SetTrainningArea(TrainingArea.TrainingArea.TrainingAreaType areaType, bool isLock)
        {
            this.currentArea = areaType;
            accecptButton.interactable = !isLock;
            denyButton.interactable = !isLock;
            for (int i = 0; i < mapToggles.Count; i++)
            {
                mapToggles[i].UiToggle.interactable = !isLock;
                mapLocations[i].ToggleControl.interactable = !isLock;
                if (mapToggles[i].AreaType != areaType) continue;
                mapToggles[i].UiToggle.isOn = true;
                mapLocations[i].ToggleControl.isOn = true;
            }
        }

        private void SetInitArea()
        {
            if (currentArea == TrainingArea.TrainingArea.TrainingAreaType.Empty)
            {
                currentArea = TrainingArea.TrainingArea.TrainingAreaType.PlainA;
                mapToggles[0].UiToggle.isOn = true;
                mapLocations[0].ToggleControl.isOn = true;
                return;
            }
            for (int i = 0; i < mapToggles.Count; i++)
            {
                if (mapToggles[i].AreaType == currentArea)
                {
                    mapToggles[i].UiToggle.isOn = true;
                    mapLocations[i].ToggleControl.isOn = true;
                    return;
                }
            }
        }

        internal void SetLoader(UIMapAreaLoader uiMapAreaLoader) => this.loader = uiMapAreaLoader;

        internal void CheckRank(RankTypeEnums areaRank)
        {
            if (!farmDetails.IsUILock)
            {
                PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
                accecptButton.interactable = (int)monsterInfo.rankType >= (int)areaRank;
            }
        }
    }
}