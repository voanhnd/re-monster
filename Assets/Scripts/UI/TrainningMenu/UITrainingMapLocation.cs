﻿using Assets.Scripts.UI.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class UITrainingMapLocation : MonoBehaviour
    {
        private string locationName;
        [SerializeField] private TextMeshProUGUI locationText;
        [SerializeField] private Toggle toggleControl;
        [SerializeField] private RectTransform centerFocus;
        [SerializeField] private ScrollrectCentering scrollrectCenter;

        public Toggle ToggleControl => toggleControl;
        public RectTransform CenterFocus => centerFocus;

        private void Awake()
        {
            toggleControl.onValueChanged.AddListener((bool value) =>
            {
                if (value)
                {
                    scrollrectCenter.CenterToSelectedItem(centerFocus);
                }
            });
        }

        private void OnEnable()
        {
            if (string.IsNullOrEmpty(locationName)) return;
            locationText.text = string.Empty;
            locationText.DOText(locationName, 0.2f, true, ScrambleMode.All).SetEase(Ease.InQuad).SetDelay(0.2f);
        }

        private void OnDisable()
        {
            locationText.DOKill();
        }

        public void SetLocation(string location) => locationName = location;
    }
}