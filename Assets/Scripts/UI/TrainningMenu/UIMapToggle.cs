﻿using Assets.Scripts.UI.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class UIMapToggle : MonoBehaviour
    {
        [SerializeField] private Toggle uiToggle;
        [SerializeField] private TextMeshProUGUI areaRankText;
        [SerializeField] private Image areaRankImage;
        [SerializeField] private TrainingArea.TrainingArea.TrainingAreaType areaType;
        [SerializeField] private ScrollrectCentering scrollrectCenter;
        [SerializeField] private RectTransform focusObject;
        private RankTypeEnums areaRank;

        public Toggle UiToggle => uiToggle;
        public TrainingArea.TrainingArea.TrainingAreaType AreaType => areaType;
        public RankTypeEnums AreaRank => areaRank;

        private void Awake()
        {
            if(scrollrectCenter != null)
            {
                uiToggle.onValueChanged.AddListener((bool value) =>
                {
                    if(value)
                    {
                        scrollrectCenter.CenterToSelectedItem(focusObject);
                    }
                });
            }
        }

        public void SetAreaRank(RankTypeEnums rank)
        {
            //areaRankText.text = rank.ToString();
            areaRankImage.sprite = RankText.Instance.GetRank(rank);
            areaRank = rank;
        }
    }
}