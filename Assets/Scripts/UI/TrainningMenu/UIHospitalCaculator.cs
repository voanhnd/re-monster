using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.UI.FarmMenu;
using System;
using UnityEngine;

public class UIHospitalCaculator : MonoBehaviour
{
    [SerializeField]
    UIMonsterFarmDetails uIMonsterFarmDetails;

    private void Start()
    {
        uIMonsterFarmDetails.OnAccecpt.AddListener(SetUpStartAction);
    }

    public void SetUpStartAction()
    {
        PlayerInfo.PlayerMonsterInfo playerMonsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(uIMonsterFarmDetails.MonsterId);

        if (playerMonsterInfo.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Hospital)
        {
            playerMonsterInfo.lastMonsterAction.actionDone = false;

            playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);
            playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);

            playerMonsterInfo.lastMonsterAction.weekPassed = 0;

            playerMonsterInfo.lastMonsterAction.actionWeek = 1;

            if (playerMonsterInfo.growthParameters.injuryType == Monster.InjuryType.Serious_Injury || playerMonsterInfo.growthParameters.diseasesType == Monster.DiseasesType.Serious_Diseases)
            {
                playerMonsterInfo.lastMonsterAction.actionWeek = 3;
            }

            Food.FoodType foodType = playerMonsterInfo.lastFoodType;
            Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

            float price = 0f;

            price += 1000 * playerMonsterInfo.lastMonsterAction.actionWeek;
            price += food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            PlayerStats.Instance.playerInfo.SubtractBiT(price);

            Debug.Log("SetUp Hospital StartAction for monster id = " + playerMonsterInfo.monsterID);

            CaculateTrainingAction(playerMonsterInfo);
        }
    }

    public static void CaculateTrainingAction(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        if (playerMonsterInfo.lastMonsterAction.actionDone == false)
        {
            //DateTime startDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix);

            //Debug.Log("startDate = " + startDate);

            //int days = (int)(DateTime.UtcNow - startDate).TotalDays + 1;

            //Debug.Log("Total Days = " + days);

            //int totalWeeks = days / 7;

            //Debug.Log("total Weeks = " + totalWeeks);

            //DateTime lastDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix);

            //Debug.Log("lastDate = " + lastDate);

            //int newDays = (int)(DateTime.UtcNow - lastDate).TotalDays + 1;

            //Debug.Log("newDays = " + newDays);

            //int newWeeks = newDays / 7;

            //Debug.Log("newWeeks = " + newWeeks);

            //if (newWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    newWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (totalWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    totalWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (newWeeks > 0)
            //{
            //    DateTime trueLastDate = DateTime.UtcNow.AddDays(-((newDays - 1) - (7 * newWeeks)));

            //    Debug.Log("trueLastDate = " + trueLastDate);

            //    playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(trueLastDate);
            //}

            //for (int i = 0; i < newWeeks; i++)
            //{
            //    MonsterHospital(playerMonsterInfo, totalWeeks);
            //}

            MonsterHospital(playerMonsterInfo);
        }
    }

    public static void MonsterHospital(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        DebugPopup.InTraining(true);

        DebugPopup.LogRaise("//////////");

        DebugPopup.LogRaise("Monster in Hospital = " + playerMonsterInfo.monsterName);

        int passWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;

        MonsterDataScriptObj monsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        BasicParameters complexParameter = Monster.Get_Complex_Parameter(playerMonsterInfo, monsterDataScriptObj);

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        FarmDataScriptObj farmDataScriptObj = PlayerStats.Instance.farmDataManagerScriptObj.GetFarmDataObj(playerFarmInfo.usingFarmType);

        //Main race, sub race stats
        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Farm terrain = {farmDataScriptObj.Farm.TerrainType}");
        DebugPopup.LogRaise($"Monster Main Suit Terrain = {monsterDataScriptObj.Monster.mainSuitTerrain}");
        DebugPopup.LogRaise($"Monster Main No Suit Terrain = {monsterDataScriptObj.Monster.mainNoSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub Suit Terrain = {monsterDataScriptObj.Monster.subSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub No Suit Terrain = {monsterDataScriptObj.Monster.subNoSuitTerrain}");

        int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterDataScriptObj.Monster.mainSuitTerrain, monsterDataScriptObj.Monster.mainNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterDataScriptObj.Monster.subSuitTerrain, monsterDataScriptObj.Monster.subNoSuitTerrain, farmDataScriptObj.Farm.TerrainType);

        Food.FoodType foodType = playerMonsterInfo.lastFoodType;

        Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);
        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Food selected = {food.FoodName}");
        DebugPopup.LogRaise($"Energy: E = {food.Energy}");
        DebugPopup.LogRaise($"Body: B = {food.Body}");
        DebugPopup.LogRaise($"Condition: C = {food.Condition}");

        //Growth Type
        Monster.GrowthType growthType = monsterDataScriptObj.Monster.growthType;

        //Characteristic Correction

        InnateTrait.InnateTraitInfo innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);
        DebugPopup.LogRaise($"Innate Trait = {playerMonsterInfo.innateTrait}");

        GrowthParameters newGrowthParameter = playerMonsterInfo.growthParameters.Clone();

        #region Energy
        newGrowthParameter.energy = 80f;
        #endregion Energy

        #region Body
        newGrowthParameter.body = 80f;
        #endregion Body

        #region Condition
        newGrowthParameter.condition = 80f;
        #endregion Condition

        #region BodyType
        newGrowthParameter.AddBodyTypeValue(food.BodyTypeValue * passWeeks);
        #endregion BodyType

        #region Fatigue
        newGrowthParameter.AddFatigue(food.Fatigue * passWeeks);

        newGrowthParameter.AddFatigue(-15f * passWeeks);

        if (monsterDataScriptObj.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddFatigue(-1 * passWeeks);
        }

        #endregion Fatigue

        /////////////////////////////////////////////////////

        #region Stress

        newGrowthParameter.AddStress(food.Stress * passWeeks);

        if (monsterDataScriptObj.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddStress(-1 * passWeeks);
        }

        #endregion Stress
        /////////////////////////////////////////////////////

        #region Injury
        newGrowthParameter.injuryType = Monster.InjuryType.None;

        #endregion Injury
        ///////////////////////////////////////////////////////////////////

        #region Diseases
        newGrowthParameter.diseasesType = Monster.DiseasesType.None;
        #endregion Diseases
        ///////////////////////////////////////////////////////////////////

        #region Affection
        if (monsterDataScriptObj.Monster.loveFoods.Contains(food.FoodTypeVal))
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(0));
        }
        if (monsterDataScriptObj.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(1));
        }

        #endregion Affection
        /////////////////////////////////////////////////////

        #region Training policy
        if (monsterDataScriptObj.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(0));
        }
        if (monsterDataScriptObj.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(1));
        }
        #endregion Training policy
        /////////////////////////////////////////////////////

        #region LifeSpan

        newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_PerWeek() * passWeeks);

        #endregion life span
        /////////////////////////////////////////////////////

        //Get changed value between old and new
        GrowthParameters actionGrowthParameter = new()
        {
            lifeSpan = newGrowthParameter.lifeSpan - playerMonsterInfo.growthParameters.lifeSpan,
            bodyTypeValue = newGrowthParameter.bodyTypeValue - playerMonsterInfo.growthParameters.bodyTypeValue,
            trainingPolicyValue = newGrowthParameter.trainingPolicyValue - playerMonsterInfo.growthParameters.trainingPolicyValue,
            affection = newGrowthParameter.affection - playerMonsterInfo.growthParameters.affection,
            fatigue = newGrowthParameter.fatigue - playerMonsterInfo.growthParameters.fatigue,
            stress = newGrowthParameter.stress - playerMonsterInfo.growthParameters.stress,
            injuryType = newGrowthParameter.injuryType,
            diseasesType = newGrowthParameter.diseasesType,

            energy = newGrowthParameter.energy - playerMonsterInfo.growthParameters.energy,
            body = newGrowthParameter.body - playerMonsterInfo.growthParameters.body,
            condition = newGrowthParameter.condition - playerMonsterInfo.growthParameters.condition
        };

        //End changed value between old and new

        playerMonsterInfo.growthParameters = newGrowthParameter;

        //set action
        UpdateActionChange(playerMonsterInfo, actionGrowthParameter, passWeeks);

        DebugPopup.InTraining(false);
    }

    public static void UpdateActionChange(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, GrowthParameters growthParametersChange, int totalWeekPassed)
    {
        //lifeSpan = 0f;

        //bodyTypeValue = 0f;

        //trainingPolicyValue = 0f;

        //affection = 0f;

        //fatigue = 0f;

        //stress = 0f;

        //injuryType = Monster.InjuryType.None;

        //diseasesType = Monster.DiseasesType.None;

        //energy = 0f;

        //body = 0f;

        //condition = 0f;

        playerMonsterInfo.lastMonsterAction.weekPassed = totalWeekPassed;

        playerMonsterInfo.lastMonsterAction.growthParameters.lifeSpan += growthParametersChange.lifeSpan;

        playerMonsterInfo.lastMonsterAction.growthParameters.bodyTypeValue += growthParametersChange.bodyTypeValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.trainingPolicyValue += growthParametersChange.trainingPolicyValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.affection += growthParametersChange.affection;

        playerMonsterInfo.lastMonsterAction.growthParameters.fatigue += growthParametersChange.fatigue;

        playerMonsterInfo.lastMonsterAction.growthParameters.stress += growthParametersChange.stress;

        playerMonsterInfo.lastMonsterAction.growthParameters.injuryType = growthParametersChange.injuryType;

        playerMonsterInfo.lastMonsterAction.growthParameters.diseasesType = growthParametersChange.diseasesType;

        playerMonsterInfo.lastMonsterAction.growthParameters.energy += growthParametersChange.energy;

        playerMonsterInfo.lastMonsterAction.growthParameters.body += growthParametersChange.body;

        playerMonsterInfo.lastMonsterAction.growthParameters.condition += growthParametersChange.condition;

        playerMonsterInfo.lastMonsterAction.actionDone = true;

        PlayerStats.Instance.Save();

        //Debug.Log("Action is updated");
    }
}
