using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.TrainingArea;
using Assets.Scripts.UI.FarmMenu;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UIIntensiveTrainingCaculator : MonoBehaviour
{
    [SerializeField]
    UIMonsterFarmDetails uIMonsterFarmDetails;

    private void Start()
    {
        uIMonsterFarmDetails.OnAccecpt.AddListener(SetUpStartAction);
    }

    public void SetUpStartAction()
    {
        PlayerInfo.PlayerMonsterInfo playerMonsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(uIMonsterFarmDetails.MonsterId);

        if (playerMonsterInfo.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.IntensiveTrainning)
        {
            playerMonsterInfo.lastMonsterAction.actionDone = false;

            playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);
            playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);

            playerMonsterInfo.lastMonsterAction.weekPassed = 0;

            playerMonsterInfo.lastMonsterAction.actionWeek = 4;

            TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(playerMonsterInfo.lastMonsterAction.trainingAreaType);

            Food.FoodType foodType = playerMonsterInfo.lastFoodType;
            Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

            float price = 0f;

            price += basicTrainingAreaDataScriptObj.TrainingArea.AreaPrice;

            price += food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            PlayerStats.Instance.playerInfo.SubtractBiT(price);

            playerMonsterInfo.lastMonsterAction.foodGold = food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            Debug.Log("SetUp Intensive Training StartAction for monster id = " + playerMonsterInfo.monsterID);

            CaculateTrainingAction(playerMonsterInfo);
        }
    }

    public static void CaculateTrainingAction(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        if (playerMonsterInfo.lastMonsterAction.actionDone == false)
        {
            //DateTime startDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix);

            //Debug.Log("startDate = " + startDate);

            //int days = (int)(DateTime.UtcNow - startDate).TotalDays + 1;

            //Debug.Log("Total Days = " + days);

            //int totalWeeks = days / 7;

            //Debug.Log("total Weeks = " + totalWeeks);

            //DateTime lastDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix);

            //Debug.Log("lastDate = " + lastDate);

            //int newDays = (int)(DateTime.UtcNow - lastDate).TotalDays + 1;

            //Debug.Log("newDays = " + newDays);

            //int newWeeks = newDays / 7;

            //Debug.Log("newWeeks = " + newWeeks);

            //if (newWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    newWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (totalWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    totalWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (newWeeks > 0)
            //{
            //    DateTime trueLastDate = DateTime.UtcNow.AddDays(-((newDays - 1) - (7 * newWeeks)));

            //    Debug.Log("trueLastDate = " + trueLastDate);

            //    playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(trueLastDate);
            //}

            //for (int i = 0; i < newWeeks; i++)
            //{
            //    DoIntensiveTraining(playerMonsterInfo, totalWeeks);
            //}

            DoIntensiveTraining(playerMonsterInfo);
        }
    }

    public static void DoIntensiveTraining(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        DebugPopup.InTraining(true);

        Debug.Log("Do Intensive Training");
        DebugPopup.LogRaise($"Do Intensive monster = {playerMonsterInfo.monsterName}");

        MonsterDataScriptObj basicMonsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        BasicParameters complexParameter = Monster.Get_Complex_Parameter(playerMonsterInfo, basicMonsterDataScriptObj);

        TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(playerMonsterInfo.lastMonsterAction.trainingAreaType);

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        //RM 374 Jira skip Farm enhanced value
        //FarmDataScriptObj farmDataScriptObj = PlayerStats.Instance.farmDataManagerScriptObj.GetFarmDataObj(playerFarmInfo.usingFarmType);

        Food.FoodType foodType = playerMonsterInfo.lastFoodType;

        Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Food selected = {food.FoodName}");
        DebugPopup.LogRaise($"Energy: E = {food.Energy}");
        DebugPopup.LogRaise($"Body: B = {food.Body}");
        DebugPopup.LogRaise($"Condition: C = {food.Condition}");

        #region Check

        //Check
        //////////////////////////////////////////
        //Main race, sub race stats
        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Training Area Name = {basicTrainingAreaDataScriptObj.TrainingArea.areaName}");
        DebugPopup.LogRaise($"Training Area terrain = {basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType}");
        DebugPopup.LogRaise($"Monster Main Suit Terrain = {basicMonsterDataScriptObj.Monster.mainSuitTerrain}");
        DebugPopup.LogRaise($"Monster Main No Suit Terrain = {basicMonsterDataScriptObj.Monster.mainNoSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub Suit Terrain = {basicMonsterDataScriptObj.Monster.subSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub No Suit Terrain = {basicMonsterDataScriptObj.Monster.subNoSuitTerrain}");

        int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(basicMonsterDataScriptObj.Monster.mainSuitTerrain, basicMonsterDataScriptObj.Monster.mainNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);

        int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(basicMonsterDataScriptObj.Monster.subSuitTerrain, basicMonsterDataScriptObj.Monster.subNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);

        GrowthParameters newGrowthParameter = playerMonsterInfo.growthParameters.Clone();

        //Check Fatigue
        Monster.FatigueType fatigueType = Monster.Get_Fatigue_Type_By_Value(playerMonsterInfo.growthParameters.fatigue);

        //Check Stress
        Monster.StressType stressType = Monster.Get_Stress_Type_By_Value(playerMonsterInfo.growthParameters.stress);

        Monster.InjuryType lastInjuryType = playerMonsterInfo.growthParameters.injuryType;
        bool injuryChange = false;

        Monster.DiseasesType lastDiseasesType = playerMonsterInfo.growthParameters.diseasesType;
        bool diseasesChange = false;

        //Growth Type
        Monster.GrowthType growthType = basicMonsterDataScriptObj.Monster.growthType;

        //////////////////////////////////////////

        //End Check
        #endregion

        //Characteristic Correction

        InnateTrait.InnateTraitInfo innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);
        DebugPopup.LogRaise($"Innate Trait = {playerMonsterInfo.innateTrait}");
        ////////////////////////////////////

        /////////////////////////////////////////

        # region Caculate Injury

        //Correction
        //float totalInjuryCorrection = 0f;

        //totalInjuryCorrection += basicTrainingAreaDataScriptObj.TrainingArea.injuryCorrection;

        //Probability

        ///100% = 1f;
        float totalInjuryProbability = 0f;

        totalInjuryProbability += basicTrainingAreaDataScriptObj.TrainingArea.injuryprobability;

        totalInjuryProbability += Monster.Get_Injuries_Probability(2);

        #endregion Caculate Injury

        #region Caculate Disease

        float numberOfSickMonster = 0f;

        #endregion Caculate Disease

        #region Action

        int actionWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;

        int passWeeks = 0;

        BasicParameters caculatedRaiseParameter = new BasicParameters();

        TrainingArea.TrainingType trainingResultType = TrainingArea.TrainingType.huge_success;

        for (int i = 0; i < actionWeeks; i++)
        {
            passWeeks++;

            DebugPopup.LogRaise("///////////////");
            DebugPopup.LogRaise("Week pass = " + passWeeks);

            //Meal Caculate

            if (passWeeks <= actionWeeks)
            {
                newGrowthParameter.AddEnergy(food.Energy);

                newGrowthParameter.AddBody(food.Body);

                newGrowthParameter.AddCondition(food.Condition);

                newGrowthParameter.AddBodyTypeValue(food.BodyTypeValue);

                newGrowthParameter.AddFatigue(food.Fatigue);
                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddFatigue(-1);
                }

                newGrowthParameter.AddStress(food.Stress);
                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddStress(-1);
                }

                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(0));
                }
                else
                if (basicMonsterDataScriptObj.Monster.hateFoods.Contains(foodType))
                {
                    newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(1));
                }
            }

            newGrowthParameter.AddEnergy(-5f);

            newGrowthParameter.AddBody(-5f);

            newGrowthParameter.AddCondition(-5f);

            //End Meal Caculate

            // Caculate Fatigue

            float fatigueTraitCorrection = innateTrait.Training_Fatigue_Reduction;

            newGrowthParameter.AddFatigue(Monster.Get_Fatigue_Increase_Caculated
                (
                    Monster.Get_Fatigue_IncreaseOrDecrease_Value(4) / 4f,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    fatigueTraitCorrection,
                    newGrowthParameter.bodyTypeValue,
                    newGrowthParameter.trainingPolicyValue
                ));

            fatigueType = Monster.Get_Fatigue_Type_By_Value(newGrowthParameter.fatigue);

            // Caculate Fatigue

            // Caculate Stress

            float stressTraitCorrection = innateTrait.Training_Stress_Reduction;

            newGrowthParameter.AddStress(Monster.Get_Stress_Increase_Caculated
            (
                Monster.Get_Stress_IncreaseOrDecrease_Value(5) / 4f,
                0f,
                mainRaceStat,
                subRaceStat,
                stressTraitCorrection,
                newGrowthParameter.bodyTypeValue,
                newGrowthParameter.trainingPolicyValue
            ));

            stressType = Monster.Get_Stress_Type_By_Value(newGrowthParameter.stress);

            // Caculate Stress

            float injuryTraitCorrection = innateTrait.Injury_Probability;

            //Caculate monster will be injury or not
            {
                Vector2 injuryRate = Monster.Get_Injury_Probability_Rate(totalInjuryProbability, injuryTraitCorrection, fatigueType, 0f);

                newGrowthParameter.injuryType = Monster.Get_Injury_Caculate_Is_Injury(newGrowthParameter.injuryType, injuryRate.x, injuryRate.y);

                if (newGrowthParameter.injuryType != lastInjuryType)
                {
                    injuryChange = true;
                }
            }

            float diseaseTraitCorrection = innateTrait.Disease_Probability;

            //Caculate monster will be disease or not
            {
                Vector2 diseasesRate = Monster.Get_Disease_Probability_Rate
                    (newGrowthParameter.energy, newGrowthParameter.body, newGrowthParameter.condition,
                    numberOfSickMonster,
                    diseaseTraitCorrection,
                    stressType
                    );

                newGrowthParameter.diseasesType = Monster.Get_Disease_Caculate_Is_Disease(newGrowthParameter.diseasesType, diseasesRate.x, diseasesRate.y);

                if(newGrowthParameter.diseasesType != lastDiseasesType)
                {
                    diseasesChange = true;
                }
            }

            newGrowthParameter.AddBodyTypeValue(Monster.Get_BodyType_Decrease_Per_Week());

            //Parameter Raise

            float successTraitCorrection = innateTrait.Training_Success_Rate;

            float failTraitCorrection = innateTrait.Training_Fail_Rate;

            TrainingArea.TrainingType trainingOutCome = TrainingArea.Get_Training_Sucess_Failure_Probability
                (
                   newGrowthParameter.trainingPolicyValue,
                   newGrowthParameter.affection,
                   successTraitCorrection,
                   failTraitCorrection,
                   newGrowthParameter.injuryType,
                   newGrowthParameter.diseasesType
                );

            BasicParameters newRaiseParameter = CaculateParameterRise
            (
            playerMonsterInfo, basicMonsterDataScriptObj, complexParameter, basicTrainingAreaDataScriptObj,
            mainRaceStat, subRaceStat, newGrowthParameter.injuryType, newGrowthParameter.diseasesType, growthType, trainingOutCome
            );

            caculatedRaiseParameter.health += newRaiseParameter.health;

            caculatedRaiseParameter.strenght += newRaiseParameter.strenght;

            caculatedRaiseParameter.intelligent += newRaiseParameter.intelligent;

            caculatedRaiseParameter.dexterity += newRaiseParameter.dexterity;

            caculatedRaiseParameter.agility += newRaiseParameter.agility;

            caculatedRaiseParameter.vitality += newRaiseParameter.vitality;

            //End Parameter Raise

            //Caculate LifeSpan

            //-40 (10x4) in intensive training
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Consumption(1, -10f, fatigueType, stressType));

            //Caculate LifeSpan

            if (newGrowthParameter.injuryType == Monster.InjuryType.Serious_Injury || newGrowthParameter.diseasesType == Monster.DiseasesType.Serious_Diseases)
            {
                trainingResultType = TrainingArea.TrainingType.failure;

                break;
            }
        }

        DebugPopup.LogRaise($"Fatigue Type = {fatigueType}");
        DebugPopup.LogRaise($"Stress Type = {stressType}");
        DebugPopup.LogRaise($"Injury Type = {newGrowthParameter.injuryType}");
        DebugPopup.LogRaise($"Disease Type = {newGrowthParameter.diseasesType}");

        #endregion Action

        /////////////////////////////////////////

        #region Affection

        if (fatigueType == Monster.FatigueType.Fatigue)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(6));
        }
        else if (fatigueType == Monster.FatigueType.Overwork)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(8));
        }

        if (stressType == Monster.StressType.Stress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(9));
        }
        else if (stressType == Monster.StressType.OverStress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(11));
        }

        if (trainingResultType != TrainingArea.TrainingType.failure)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(2));
        }
        else
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(3));
        }

        #endregion Affection

        #region Training Policy

        if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(0));
        }

        if (basicMonsterDataScriptObj.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(1));
        }

        newGrowthParameter.AddTrainingPolicyValue(-4);

        #endregion Training Policy

        #region Caculate LifeSpan

        if (injuryChange)
        {
            if (newGrowthParameter.injuryType == Monster.InjuryType.Injury)
            {
                newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(4));
            }
            else if (newGrowthParameter.injuryType == Monster.InjuryType.Serious_Injury)
            {
                newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(5));
            }
        }

        if (newGrowthParameter.diseasesType == Monster.DiseasesType.Diseases)
        {
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(6));
        }
        else if (newGrowthParameter.diseasesType == Monster.DiseasesType.Serious_Diseases)
        {
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(7));
        }

        #endregion Caculate LifeSpan

        BasicParameters newComplexParameters = Monster.Get_Complex_Parameter(complexParameter, caculatedRaiseParameter);

        #region Caculate Skill Up

        float skillTraitCorrection = innateTrait.Skill_Level_Up_Rate;

        if (newGrowthParameter.injuryType != Monster.InjuryType.Serious_Injury  && newGrowthParameter.diseasesType != Monster.DiseasesType.Serious_Diseases)
        {
            bool isSkillUp = CaculateSkillUp
            (
            playerMonsterInfo, basicMonsterDataScriptObj,
            basicTrainingAreaDataScriptObj, skillTraitCorrection
            );
        }

        #endregion Caculate Skill Up

        #region Caculate Acquired Trait

        int maxTraitLevel = 5;

        int maxTraitOnMonster = 2;

        List<AcquiredTrait.AcquiredTraitType> acquiredTraitTypes = CaculateAcquiredTrait(basicTrainingAreaDataScriptObj.TrainingArea, newComplexParameters, Monster.Get_Complex_Parameter(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters, basicTrainingAreaDataScriptObj.TrainingArea.subParameters), basicTrainingAreaDataScriptObj.TrainingArea.parametersCorrection, innateTrait.Acquired_Trait_Level_Up_Rate, maxTraitOnMonster);

        List<AcquiredTrait.PlayerInfoAcquiredTrait> tempTraits = new List<AcquiredTrait.PlayerInfoAcquiredTrait>();
        tempTraits.AddRange(playerMonsterInfo.acquiredTraits);

        List<AcquiredTrait.PlayerInfoAcquiredTrait> newTraits = new List<AcquiredTrait.PlayerInfoAcquiredTrait>();

        List<AcquiredTrait.AcquiredTraitType> actionNewTraits = new List<AcquiredTrait.AcquiredTraitType>();
        List<AcquiredTrait.AcquiredTraitType> actionUpTraits = new List<AcquiredTrait.AcquiredTraitType>();

        foreach (AcquiredTrait.AcquiredTraitType acquiredTraitType in acquiredTraitTypes)
        {
            int upgradeCount = 0;

            foreach (AcquiredTrait.PlayerInfoAcquiredTrait trait in tempTraits)
            {
                if(trait.acquiredTraitType == AcquiredTrait.AcquiredTraitType.None)
                {
                    trait.acquiredTraitType = acquiredTraitType;
                    trait.traitLevel = 0;

                    upgradeCount++;

                    actionNewTraits.Add(acquiredTraitType);

                    break;
                }
                else if (trait.acquiredTraitType == acquiredTraitType)
                {
                    if (trait.traitLevel < maxTraitLevel)
                    {
                        trait.traitLevel++;
                    }

                    upgradeCount++;

                    actionUpTraits.Add(acquiredTraitType);

                    break;
                }
            }

            if(upgradeCount == 0)
            {
                AcquiredTrait.PlayerInfoAcquiredTrait newInfo = new AcquiredTrait.PlayerInfoAcquiredTrait();

                newInfo.acquiredTraitType = acquiredTraitType;

                newInfo.traitLevel = 0;

                newTraits.Add(newInfo);

                actionNewTraits.Add(acquiredTraitType);
            }
        }

        for (int j = 0; j <= tempTraits.Count - 2; j++)
        {
            for (int k = 0; k <= tempTraits.Count - 2; k++)
            {
                if (tempTraits[k].traitLevel > tempTraits[k + 1].traitLevel) continue;

                AcquiredTrait.PlayerInfoAcquiredTrait temp = tempTraits[k + 1];
                tempTraits[k + 1] = tempTraits[k];
                tempTraits[k] = temp;
            }
        }

        tempTraits.AddRange(newTraits);

        int removeNum = tempTraits.Count - maxTraitOnMonster;

        for (int i = 0; i < removeNum; i++)
        {
            actionUpTraits.Remove(tempTraits[0].acquiredTraitType);
            actionNewTraits.Remove(tempTraits[0].acquiredTraitType);
            tempTraits.RemoveAt(0);
        }

        playerMonsterInfo.acquiredTraits = tempTraits;

        #endregion Caculate Acquired Trait

        #region Update action infos

        //Get changed value between old and new
        GrowthParameters actionGrowthParameter = new()
        {
            lifeSpan = newGrowthParameter.lifeSpan - playerMonsterInfo.growthParameters.lifeSpan,
            bodyTypeValue = newGrowthParameter.bodyTypeValue - playerMonsterInfo.growthParameters.bodyTypeValue,
            trainingPolicyValue = newGrowthParameter.trainingPolicyValue - playerMonsterInfo.growthParameters.trainingPolicyValue,
            affection = newGrowthParameter.affection - playerMonsterInfo.growthParameters.affection,
            fatigue = newGrowthParameter.fatigue - playerMonsterInfo.growthParameters.fatigue,
            stress = newGrowthParameter.stress - playerMonsterInfo.growthParameters.stress,
            injuryType = newGrowthParameter.injuryType,
            diseasesType = newGrowthParameter.diseasesType,

            energy = newGrowthParameter.energy - playerMonsterInfo.growthParameters.energy,
            body = newGrowthParameter.body - playerMonsterInfo.growthParameters.body,
            condition = newGrowthParameter.condition - playerMonsterInfo.growthParameters.condition
        };

        //End changed value between old and new

        UpdateActionChange(playerMonsterInfo, caculatedRaiseParameter, actionGrowthParameter, passWeeks, injuryChange, diseasesChange, trainingResultType, actionUpTraits, actionNewTraits);

        #endregion Update action infos

        #region Apply change to playerstats

        playerMonsterInfo.growthParameters = newGrowthParameter;

        playerMonsterInfo.basicParameters = Monster.Get_Subtract_Parameter(newComplexParameters, basicMonsterDataScriptObj.Monster.basicParameters);

        playerMonsterInfo.rankType = Monster.Get_Monster_BasicParameters_Rank_By_Value(newComplexParameters);

        //PlayerStats.instance.Save();

        #endregion Apply change to playerstats

        DebugPopup.InTraining(false);
    }

    /// <summary>
    /// <para> Basic training has two basic parameters raised, the main parameter is +8 and the sub-parameter is +4. </para>
    /// <para> 4.1.8 Training in english document </para>
    /// <para> basicTrainType from 1 = heath to 6 fit 6 basic parameters </para>
    /// </summary>
    /// <param name="playerMonsterInfo"></param>
    /// <param name="basicMonsterDataScriptObj"></param>
    /// <param name="farmDataScriptObj"></param>
    /// <param name="characteristicCorrection"></param>
    /// <param name="injuryType"></param>
    /// <param name="diseaseType"></param>
    /// <param name="growthType"></param>
    /// <param name="trainingResultType"></param>
    /// <param name="basicTrainType"></param>
    /// <returns></returns>
    public static BasicParameters CaculateParameterRise
        (
        PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj basicMonsterDataScriptObj, BasicParameters complexParameter, TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj,
        int mainRaceStat, int subRaceStat, Monster.InjuryType injuryType, Monster.DiseasesType diseaseType, Monster.GrowthType growthType, TrainingArea.TrainingType trainingResultType
        )
    {
        float m_health_RiseValue = 0f;
        float s_health_RiseValue = 0f;

        float m_strenght_RiseValue = 0f;
        float s_strenght_RiseValue = 0f;

        float m_intelligent_RiseValue = 0f;
        float s_intelligent_RiseValue = 0f;

        float m_dexterity_RiseValue = 0f;
        float s_dexterity_RiseValue = 0f;

        float m_agility_RiseValue = 0f;
        float s_agility_RiseValue = 0f;

        float m_vitality_RiseValue = 0f;
        float s_vitality_RiseValue = 0f;

        PersonalityTrait.PersonalityTraitInfo trainingTrait = PersonalityTrait.GetTraitInfo(playerMonsterInfo.personalityTrait);

        DebugPopup.LogRaise($"PersonalityTrait = {playerMonsterInfo.personalityTrait}");

        if(basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.health != 0f)
        {
            //Main Health
            DebugPopup.LogRaise("Main Health");
            m_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.health,
                    basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.health,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.health != 0f)
        {
            //Sub Health
            DebugPopup.LogRaise("Sub Health");
            s_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.health,
                    basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.health,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.strenght != 0f)
        {
            //Main Strenght
            DebugPopup.LogRaise("Main Strenght");
            m_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.strenght,
                    basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.strenght,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.strenght != 0f)
        {
            //Sub Strenght
            DebugPopup.LogRaise("Sub Strenght");
            s_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.strenght,
                    basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.strenght,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.intelligent != 0f)
        {
            //Main intelligent
            DebugPopup.LogRaise("Main intelligent");
            m_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.intelligent,
                    basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.intelligent,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.intelligent != 0f)
        {
            //Sub intelligent
            DebugPopup.LogRaise("Sub intelligent");
            s_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.intelligent,
                    basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.intelligent,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.dexterity != 0f)
        {
            //Main dexterity
            DebugPopup.LogRaise("Main dexterity");
            m_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.dexterity,
                    basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.dexterity,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.dexterity != 0f)
        {
            //Sub dexterity
            DebugPopup.LogRaise("Sub dexterity");
            s_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.dexterity,
                    basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.dexterity,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.agility != 0f)
        {
            //Main agility
            DebugPopup.LogRaise("Main agility");
            m_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.agility,
                    basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.agility,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.agility != 0f)
        {
            //Sub agility
            DebugPopup.LogRaise("Sub agility");
            s_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.agility,
                    basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.agility,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.vitality != 0f)
        {
            //Main vitality
            DebugPopup.LogRaise("Main vitality");
            m_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.mainParameters.vitality,
                    basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.vitality,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.subParameters.vitality != 0f)
        {
            //Sub vitality
            DebugPopup.LogRaise("Sub vitality");
            s_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.subParameters.vitality,
                    basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.vitality,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }


        #region Add to Complex parameter

        BasicParameters raiseParameters = new BasicParameters();

        raiseParameters.health = m_health_RiseValue + s_health_RiseValue;
        raiseParameters.strenght = m_strenght_RiseValue + s_strenght_RiseValue;
        raiseParameters.intelligent = m_intelligent_RiseValue + s_intelligent_RiseValue;
        raiseParameters.dexterity = m_dexterity_RiseValue + s_dexterity_RiseValue;
        raiseParameters.agility = m_agility_RiseValue + s_agility_RiseValue;
        raiseParameters.vitality = m_vitality_RiseValue + s_vitality_RiseValue;

        #endregion

        return raiseParameters;
    }

    public static bool CaculateSkillUp(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj basicMonsterDataScriptObj, TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj, float traitCorrection)
    {
        Monster.MonsterSkillInfo skillInfo = null;

        bool isUpLevel = false;

        DebugPopup.LogRaise("Caculate Skill");

        BasicParameters complexParameter = Monster.Get_Complex_Parameter(playerMonsterInfo, basicMonsterDataScriptObj);

        foreach (Monster.MonsterSkillInfo info in playerMonsterInfo.MonsterSkillInfos)
        {
            if (info.skillID == playerMonsterInfo.lastMonsterAction.trainingSkill)
            {
                skillInfo = info;

                DebugPopup.LogRaise("Selected training skill = " + info.skillID.ToString());

                break;
            }
        }

        if (skillInfo != null)
        {
            SkillDataScriptObj basicSkillObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(skillInfo.skillID);

            int maxLevel = basicSkillObj.Skill.skillDetails.Count;

            int nextLevel = Mathf.Clamp(skillInfo.skillLevel + 1, skillInfo.skillLevel, maxLevel);

            RankTypeEnums nextSkillRank = basicSkillObj.Skill.skillDetails[nextLevel - 1].rankType;

            if (playerMonsterInfo.rankType >= nextSkillRank && skillInfo.skillLevel < maxLevel)
            {
                DebugPopup.LogRaise("Check skill level up = " + skillInfo.skillID.ToString());

                float requestedTotalParameters = Skill.Get_Requested_Parameter_Value(basicSkillObj.Skill.parametersRequired, skillInfo.skillLevel);

                float skillUpRate = Skill.Get_Skill_Caculate_Level_Up_Rate(complexParameter, basicTrainingAreaDataScriptObj.TrainingArea.parametersCorrection, requestedTotalParameters, traitCorrection);

                DebugPopup.LogRaise("skill Up Rate = " + skillUpRate);

                isUpLevel = Skill.Get_Skill_Level_Up_Chance(skillUpRate);

                Debug.Log("isUpLevel = " + isUpLevel);

                if (isUpLevel == true)
                {
                    skillInfo.skillLevel = nextLevel;
                }
            }
            else
            {
                DebugPopup.LogRaise("monster rank = " + playerMonsterInfo.rankType.ToString());
                DebugPopup.LogRaise("next skill rank = " + nextSkillRank.ToString());
                DebugPopup.LogRaise("skillLevel = " + skillInfo.skillLevel.ToString());
                DebugPopup.LogRaise("maxLevel skill level = " + maxLevel);
            }
        }

        playerMonsterInfo.lastMonsterAction.skillUp = isUpLevel;

        return isUpLevel;
    }

    public static List<AcquiredTrait.AcquiredTraitType> CaculateAcquiredTrait(TrainingArea trainingArea, BasicParameters monsterComplexParameter, BasicParameters terrainParameters, BasicParameters requestParameters, float traitCorrection, int maxTraitOnMonster)
    {
        List<AcquiredTrait.AcquiredTraitType> learnList = new List<AcquiredTrait.AcquiredTraitType>();

        //int traitSlot = maxTraitOnMonster;

        foreach (AcquiredTrait.AcquiredTraitType acquiredTraitType in trainingArea.acquiredTraitTypes) 
        {
            DebugPopup.LogRaise($"///////////");
            DebugPopup.LogRaise($"Caculate Acquired Trait = {acquiredTraitType}");

            //if(traitSlot > 0 && acquiredTraitType != AcquiredTrait.AcquiredTraitType.None)
            {
                float rate = TrainingArea.Get_Intensive_Acquisition_Level_Up_Rate(monsterComplexParameter, terrainParameters, requestParameters, traitCorrection);

                float ranNum = UnityEngine.Random.Range(0f, 100f);

                if (ranNum / 100f <= rate)
                {
                    learnList.Add(acquiredTraitType);

                    //traitSlot--;
                }
            }
        }

        return learnList;
    }

    public static void UpdateActionChange(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, BasicParameters changeParameters, GrowthParameters changeGrowthParameters, int totalWeekPassed
        , bool injuryChange, bool diseasesChange, TrainingArea.TrainingType trainingType, List<AcquiredTrait.AcquiredTraitType> actionUpTraits, List<AcquiredTrait.AcquiredTraitType> actionNewTraits)
    {
        playerMonsterInfo.lastMonsterAction.weekPassed = totalWeekPassed;

        playerMonsterInfo.lastMonsterAction.basicParameters.health += changeParameters.health;
        playerMonsterInfo.lastMonsterAction.basicParameters.strenght += changeParameters.strenght;
        playerMonsterInfo.lastMonsterAction.basicParameters.intelligent += changeParameters.intelligent;
        playerMonsterInfo.lastMonsterAction.basicParameters.dexterity += changeParameters.dexterity;
        playerMonsterInfo.lastMonsterAction.basicParameters.agility += changeParameters.agility;
        playerMonsterInfo.lastMonsterAction.basicParameters.vitality += changeParameters.vitality;

        playerMonsterInfo.lastMonsterAction.growthParameters.lifeSpan += changeGrowthParameters.lifeSpan;

        playerMonsterInfo.lastMonsterAction.growthParameters.bodyTypeValue += changeGrowthParameters.bodyTypeValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.trainingPolicyValue += changeGrowthParameters.trainingPolicyValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.affection += changeGrowthParameters.affection;

        playerMonsterInfo.lastMonsterAction.growthParameters.fatigue += changeGrowthParameters.fatigue;

        playerMonsterInfo.lastMonsterAction.growthParameters.stress += changeGrowthParameters.stress;

        if (injuryChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.injuryType = changeGrowthParameters.injuryType;

        if (diseasesChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.diseasesType = changeGrowthParameters.diseasesType;

        playerMonsterInfo.lastMonsterAction.actionNewTraits = actionNewTraits;

        playerMonsterInfo.lastMonsterAction.actionUpTraits = actionUpTraits;

        #region Meal parameters
        playerMonsterInfo.lastMonsterAction.growthParameters.energy += changeGrowthParameters.energy;
        playerMonsterInfo.lastMonsterAction.growthParameters.body += changeGrowthParameters.body;
        playerMonsterInfo.lastMonsterAction.growthParameters.condition += changeGrowthParameters.condition;
        #endregion

        playerMonsterInfo.lastMonsterAction.trainingType = trainingType;

        if (changeGrowthParameters.injuryType == Monster.InjuryType.Serious_Injury || changeGrowthParameters.diseasesType == Monster.DiseasesType.Serious_Diseases)
        {
            float backGold = playerMonsterInfo.lastMonsterAction.foodGold - (playerMonsterInfo.lastMonsterAction.foodGold / playerMonsterInfo.lastMonsterAction.actionWeek) * totalWeekPassed;

            PlayerStats.Instance.playerInfo.AddGold(backGold);
        }

        playerMonsterInfo.lastMonsterAction.actionDone = true;

        PlayerStats.Instance.Save();

        //Debug.Log("Action is updated");
    }
}
