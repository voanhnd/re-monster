﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.UI.FarmMenu;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TrainningMenu
{
    public class UIIntensiveTrainningMenu : MonoBehaviour
    {
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private RectTransform container;
        [SerializeField] private UnityEvent onMenuOpen;
        [SerializeField] private UnityEvent onMenuClose;

        [Header("Animation values")]
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Ease openEase;
        [SerializeField] private Ease closeEase;

        [Header("Interaction buttons")]
        [SerializeField] private Button accecptButton;
        [SerializeField] private Button denyButton;

        [Header("Trainning parameter datas")]
        [SerializeField] private TextMeshProUGUI trainningLocationText;
        [SerializeField] private Image trainingRankImage;
        [SerializeField] private TextMeshProUGUI costText;
        [SerializeField] private TextMeshProUGUI compatibilityText;
        [SerializeField] private TextMeshProUGUI riseParametersText;

        [Header("Basic parameters")]
        [SerializeField] private TextMeshProUGUI hpParaText;
        [SerializeField] private TextMeshProUGUI dexParaText;
        [SerializeField] private TextMeshProUGUI strParaText;
        [SerializeField] private TextMeshProUGUI agiParaText;
        [SerializeField] private TextMeshProUGUI intParaText;
        [SerializeField] private TextMeshProUGUI vitParaText;

        [Header("Skill detail")]
        [SerializeField] private TextMeshProUGUI skillLevelText;
        [SerializeField] private TextMeshProUGUI canAcquireLevelSkillText;
        [SerializeField] private TextMeshProUGUI skillSuccessRateText;
        [SerializeField] private List<UIMapToggle> mapToggles = new();
        [SerializeField] private List<UITrainingMapLocation> mapLocations = new();

        private string monsterId;
        private string learnSkillId;
        private UIMonsterFarmDetails farmDetails;
        private TrainingArea.TrainingArea.TrainingAreaType currentArea;
        private UIMapAreaLoader loader;
        private float trainingCost = 0;

        public List<UIMapToggle> MapToggles => mapToggles;
        public List<UITrainingMapLocation> MapLocations => mapLocations;

        public UnityEvent OnMenuOpen => onMenuOpen;
        public UnityEvent OnMenuClose => onMenuClose;


        private void Awake()
        {
            accecptButton.onClick.AddListener(() =>
            {
                CloseMenu();
                AccecptTrainning();
            });
            denyButton.onClick.AddListener(() =>
            {
                CloseMenu();
                DenyTrainning();
                currentArea = TrainingArea.TrainingArea.TrainingAreaType.Empty;
            });
        }

        private void Start()
        {
            container.sizeDelta = closeSize;
            gameObject.SetActive(false);
        }

        public void OpenMenu()
        {
            gameObject.SetActive(true);
            container.DOSizeDelta(openSize, openTime).SetEase(openEase).OnComplete(() =>
            {
                onMenuOpen.Invoke();
                SetInitArea();
            });
        }

        public void CloseMenu()
        {
            container.DOSizeDelta(closeSize, closeTime).SetEase(closeEase).OnComplete(() =>
            {
                onMenuClose.Invoke();
                gameObject.SetActive(false);
            });
        }

        private void AccecptTrainning()
        {
            farmDetails.RaiseActionComponents.SetIntensiveTrainning(currentArea, trainingCost);
            farmDetails.SetSkillLearn(learnSkillId);
        }

        private void DenyTrainning()
        {
            currentArea = TrainingArea.TrainingArea.TrainingAreaType.Empty;
            trainingCost = 0;
            farmDetails.RaiseActionComponents.SetIntensiveTrainning(currentArea, trainingCost);
        }

        public void SetDataParameters(string trainningLocation, RankTypeEnums trainningRank, float cost, List<(string, float)> riseParameters)
        {
            trainningLocationText.text = trainningLocation;
            trainingRankImage.sprite = RankText.Instance.GetRank(trainningRank);
            costText.text = $"{cost} B";
            trainingCost = cost;
            AddRiseText(riseParameters);
        }

        public void SetMainDataParameters(float hpValue, float dexValue, float strValue, float agiValue, float intValue, float vitValue)
        {
            hpParaText.text = $"{hpValue} %";
            dexParaText.text = $"{dexValue} %";
            strParaText.text = $"{strValue} %";
            agiParaText.text = $"{agiValue} %";
            intParaText.text = $"{intValue} %";
            vitParaText.text = $"{vitValue} %";
        }

        private void AddRiseText(List<(string, float)> riseParameters)
        {
            string riseText = "<color=green>";
            for (int i = 0; i < riseParameters.Count; i++)
            {
                riseText += (riseParameters[i].Item1 + " ");
            }
            riseText += "</color>";
            riseParametersText.text = riseText;
        }

        private void SetCompatibilityText(Vector2 raceComp)
        {
            string mainComp = "-";
            if (raceComp.x == 1)
            {
                mainComp = "<color=green>good</color>";
            }
            if (raceComp.x == -1)
            {
                mainComp = "<color=red>bad</color>";
            }
            string subComp = "-";
            if (raceComp.y == 1)
            {
                subComp = "<color=green>good</color>";
            }
            if (raceComp.y == -1)
            {
                subComp = "<color=red>bad</color>";
            }
            compatibilityText.text = $"main: {mainComp} sub: {subComp}";
        }

        public void SetTargetMonster(UIMonsterFarmDetails farmDetails)
        {
            this.farmDetails = farmDetails;
            this.monsterId = farmDetails.MonsterId;
            MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterId);
            SkillDataScriptObj monsterSkillScriptable = GetMonsterSkill(monsterScriptable);
            SetMonsterSkillInfo(monsterSkillScriptable, monsterScriptable);
        }

        public void SetAreaComp(TrainingArea.TrainingArea.TrainingAreaType currentArea)
        {
            PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
            MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterInfo.monsterID);
            for (int i = 0; i < loader.TrainningAreaDataList.Count; i++)
            {
                if (loader.TrainningAreaDataList[i].AreaType == currentArea)
                {
                    int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterScriptable.Monster.mainSuitTerrain, monsterScriptable.Monster.mainNoSuitTerrain, loader.TrainningAreaDataList[i].Terrain);

                    int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(monsterScriptable.Monster.subSuitTerrain, monsterScriptable.Monster.subNoSuitTerrain, loader.TrainningAreaDataList[i].Terrain);

                    SetCompatibilityText(new Vector2(mainRaceStat, subRaceStat));
                }
            }
        }

        public void SetTrainningArea(TrainingArea.TrainingArea.TrainingAreaType areaType)
        {
            this.currentArea = areaType;
            MonsterDataScriptObj monsterScriptable = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterId);
            SkillDataScriptObj monsterSkillScriptable = GetMonsterSkill(monsterScriptable);
            SetMonsterSkillInfo(monsterSkillScriptable, monsterScriptable);
        }

        public void SetTrainningArea(TrainingArea.TrainingArea.TrainingAreaType areaType, bool isLock)
        {
            this.currentArea = areaType;
            accecptButton.interactable = !isLock;
            denyButton.interactable = !isLock;
            for (int i = 0; i < mapToggles.Count; i++)
            {
                mapToggles[i].UiToggle.interactable = !isLock;
                mapLocations[i].ToggleControl.interactable = !isLock;
                if (mapToggles[i].AreaType != areaType) continue;
                mapToggles[i].UiToggle.isOn = true;
                mapLocations[i].ToggleControl.isOn = true;
            }
        }

        private void SetInitArea()
        {
            if (currentArea == TrainingArea.TrainingArea.TrainingAreaType.Empty)
            {
                currentArea = TrainingArea.TrainingArea.TrainingAreaType.PlainA;
                mapToggles[0].UiToggle.isOn = true;
                mapLocations[0].ToggleControl.isOn = true;
                return;
            }
            for (int i = 0; i < mapToggles.Count; i++)
            {
                if (mapToggles[i].AreaType == currentArea)
                {
                    mapToggles[i].UiToggle.isOn = true;
                    mapLocations[i].ToggleControl.isOn = true;
                    return;
                }
            }
        }

        internal void SetLoader(UIMapAreaLoader uiMapAreaLoader) => this.loader = uiMapAreaLoader;

        private void SetMonsterSkillInfo(SkillDataScriptObj monsterSkillScriptable, MonsterDataScriptObj monsterScriptable)
        {
            if (monsterSkillScriptable == null)
            {
                canAcquireLevelSkillText.text = "<color=red>false</color>";
                skillLevelText.text = "-";
                skillSuccessRateText.text = "-";
                learnSkillId = string.Empty;
                return;
            }
            for (int i = 0; i < farmDetails.MonsterInfoData.MonsterSkillInfos.Count; i++)
            {
                if (farmDetails.MonsterInfoData.MonsterSkillInfos[i].skillID == monsterSkillScriptable.Skill.skillID
                    && farmDetails.MonsterInfoData.MonsterSkillInfos[i].skillLevel < monsterSkillScriptable.Skill.skillDetails.Count
                    && farmDetails.MonsterInfoData.rankType >= monsterSkillScriptable.Skill.skillDetails[farmDetails.MonsterInfoData.MonsterSkillInfos[i].skillLevel].rankType)
                {
                    canAcquireLevelSkillText.text = "<color=green>true</color>";
                    skillLevelText.text = $"{monsterSkillScriptable.Skill.skillName} level {farmDetails.MonsterInfoData.MonsterSkillInfos[i].skillLevel + 1}";
                    float skillChance = GetSkillChance(monsterSkillScriptable, farmDetails.MonsterInfoData.MonsterSkillInfos[i], farmDetails.MonsterInfoData, monsterScriptable, 0) * 100;
                    skillSuccessRateText.text = $"{Mathf.Clamp(skillChance, 0, 100):0.##} %";
                    learnSkillId = monsterSkillScriptable.Skill.skillID;
                    return;
                }
            }
            learnSkillId = string.Empty;
            canAcquireLevelSkillText.text = "<color=red>false</color>";
            skillLevelText.text = "-";
            skillSuccessRateText.text = "-";
        }

        internal void CheckRank(RankTypeEnums areaRank)
        {
            if (!farmDetails.IsUILock)
            {
                PlayerInfo.PlayerMonsterInfo monsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(monsterId);
                accecptButton.interactable = (int)monsterInfo.rankType >= (int)areaRank;
            }
        }

        private SkillDataScriptObj GetMonsterSkill(MonsterDataScriptObj monsterScript)
        {
            for (int i = 0; i < monsterScript.Monster.monsterSkillInfos.Count; i++)
            {
                SkillDataScriptObj skillScript = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(monsterScript.Monster.monsterSkillInfos[i].skillID);
                if (skillScript.Skill.learnArea == currentArea)
                {
                    return skillScript;
                }
            }
            return null;
        }

        private float GetSkillChance(SkillDataScriptObj skillSciptable, Monster.Monster.MonsterSkillInfo skillInfo, PlayerInfo.PlayerMonsterInfo monsterInfo, MonsterDataScriptObj monsterScriptable, float characteristicCorrection)
        {
            float requestedTotalParameters = Skill.Get_Requested_Parameter_Value(skillSciptable.Skill.parametersRequired, skillInfo.skillLevel);

            BasicParameters complexParameters = Monster.Monster.Get_Complex_Parameter(monsterInfo, monsterScriptable);

            return Skill.Get_Skill_Caculate_Level_Up_Rate(complexParameters, skillSciptable.Skill.skillLearnCorrection, requestedTotalParameters, characteristicCorrection);
        }
    }
}