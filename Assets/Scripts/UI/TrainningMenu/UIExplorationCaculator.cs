using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.TrainingArea;
using Assets.Scripts.UI.FarmMenu;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UIExplorationCaculator : MonoBehaviour
{
    [SerializeField]
    UIMonsterFarmDetails uIMonsterFarmDetails;

    private void Start()
    {
        uIMonsterFarmDetails.OnAccecpt.AddListener(SetUpStartAction);
    }

    public void SetUpStartAction()
    {
        PlayerInfo.PlayerMonsterInfo playerMonsterInfo = PlayerStats.Instance.playerInfo.GetPlayerMonsterInfoByID(uIMonsterFarmDetails.MonsterId);

        if (playerMonsterInfo.lastMonsterAction.monsterActionType == MonsterAction.MonsterActionType.Exploration)
        {
            playerMonsterInfo.lastMonsterAction.actionDone = false;

            playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);
            playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(DateTime.UtcNow);

            playerMonsterInfo.lastMonsterAction.weekPassed = 0;

            playerMonsterInfo.lastMonsterAction.actionWeek = 4;

            TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(playerMonsterInfo.lastMonsterAction.trainingAreaType);

            Food.FoodType foodType = playerMonsterInfo.lastFoodType;
            Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

            float price = 0f;

            price += basicTrainingAreaDataScriptObj.TrainingArea.explorationAreaPrice;

            price += food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            PlayerStats.Instance.playerInfo.SubtractBiT(price);

            playerMonsterInfo.lastMonsterAction.foodGold = food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

            Debug.Log("SetUp Exploration Training StartAction for monster id = " + playerMonsterInfo.monsterID);

            CaculateTrainingAction(playerMonsterInfo);
        }
    }

    public static void CaculateTrainingAction(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        if (playerMonsterInfo.lastMonsterAction.actionDone == false)
        {
            //DateTime startDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingStartUtcUnix);

            //Debug.Log("startDate = " + startDate);

            //int days = (int)(DateTime.UtcNow - startDate).TotalDays + 1;

            //Debug.Log("Total Days = " + days);

            //int totalWeeks = days / 7;

            //Debug.Log("total Weeks = " + totalWeeks);

            //DateTime lastDate = DateTimeExtension.UnixTimestampToDateTime(playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix);

            //Debug.Log("lastDate = " + lastDate);

            //int newDays = (int)(DateTime.UtcNow - lastDate).TotalDays + 1;

            //Debug.Log("newDays = " + newDays);

            //int newWeeks = newDays / 7;

            //Debug.Log("newWeeks = " + newWeeks);

            //if (newWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    newWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (totalWeeks > playerMonsterInfo.lastMonsterAction.actionWeek)
            //{
            //    totalWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;
            //}

            //if (newWeeks > 0)
            //{
            //    DateTime trueLastDate = DateTime.UtcNow.AddDays(-((newDays - 1) - (7 * newWeeks)));

            //    Debug.Log("trueLastDate = " + trueLastDate);

            //    playerMonsterInfo.lastMonsterAction.trainingLastUtcUnix = DateTimeExtension.DateTimeToUnixTimestamp(trueLastDate);
            //}

            //for (int i = 0; i < newWeeks; i++)
            //{
            //    DoExploration(playerMonsterInfo, totalWeeks);
            //}


            DoExploration(playerMonsterInfo);
        }
    }

    public static void DoExploration(PlayerInfo.PlayerMonsterInfo playerMonsterInfo)
    {
        DebugPopup.InTraining(true);

        Debug.Log("Do Exploration");
        DebugPopup.LogRaise($"Do Exploration monster = {playerMonsterInfo.monsterName}");

        MonsterDataScriptObj basicMonsterDataScriptObj = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(playerMonsterInfo.monsterID);

        BasicParameters complexParameter = Monster.Get_Complex_Parameter(playerMonsterInfo, basicMonsterDataScriptObj);

        TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(playerMonsterInfo.lastMonsterAction.trainingAreaType);

        PlayerInfo.PlayerFarmInfo playerFarmInfo = PlayerStats.Instance.playerInfo.GetPlayerFarmInfoByID(playerMonsterInfo.usingFarmID);

        //RM 374 Jira skip Farm enhanced value
        //FarmDataScriptObj farmDataScriptObj = PlayerStats.Instance.farmDataManagerScriptObj.GetFarmDataObj(playerFarmInfo.usingFarmType);

        Food.FoodType foodType = playerMonsterInfo.lastFoodType;

        Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Food selected = {food.FoodName}");
        DebugPopup.LogRaise($"Energy: E = {food.Energy}");
        DebugPopup.LogRaise($"Body: B = {food.Body}");
        DebugPopup.LogRaise($"Condition: C = {food.Condition}");

        #region Check

        //Check
        //////////////////////////////////////////
        //Main race, sub race stats
        DebugPopup.LogRaise("//////////");
        DebugPopup.LogRaise($"Training Area Name = {basicTrainingAreaDataScriptObj.TrainingArea.areaName}");
        DebugPopup.LogRaise($"Training Area terrain = {basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType}");
        DebugPopup.LogRaise($"Monster Main Suit Terrain = {basicMonsterDataScriptObj.Monster.mainSuitTerrain}");
        DebugPopup.LogRaise($"Monster Main No Suit Terrain = {basicMonsterDataScriptObj.Monster.mainNoSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub Suit Terrain = {basicMonsterDataScriptObj.Monster.subSuitTerrain}");
        DebugPopup.LogRaise($"Monster Sub No Suit Terrain = {basicMonsterDataScriptObj.Monster.subNoSuitTerrain}");

        int mainRaceStat = Farm.Get_Race_SuitTerrain_Stats(basicMonsterDataScriptObj.Monster.mainSuitTerrain, basicMonsterDataScriptObj.Monster.mainNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);

        int subRaceStat = Farm.Get_Race_SuitTerrain_Stats(basicMonsterDataScriptObj.Monster.subSuitTerrain, basicMonsterDataScriptObj.Monster.subNoSuitTerrain, basicTrainingAreaDataScriptObj.TrainingArea.trainingTerrainType);

        GrowthParameters newGrowthParameter = playerMonsterInfo.growthParameters.Clone();

        //Check Fatigue
        Monster.FatigueType fatigueType = Monster.Get_Fatigue_Type_By_Value(playerMonsterInfo.growthParameters.fatigue);

        //Check Stress
        Monster.StressType stressType = Monster.Get_Stress_Type_By_Value(playerMonsterInfo.growthParameters.stress);

        Monster.InjuryType lastInjuryType = playerMonsterInfo.growthParameters.injuryType;
        bool injuryChange = false;

        Monster.DiseasesType lastDiseasesType = playerMonsterInfo.growthParameters.diseasesType;
        bool diseasesChange = false;

        //Growth Type
        Monster.GrowthType growthType = basicMonsterDataScriptObj.Monster.growthType;

        //////////////////////////////////////////

        //End Check
        #endregion

        //Characteristic Correction

        InnateTrait.InnateTraitInfo innateTrait = InnateTrait.GetInnateTraitInfo(playerMonsterInfo.innateTrait);
        DebugPopup.LogRaise($"Innate Trait = {playerMonsterInfo.innateTrait}");
        ////////////////////////////////////

        /////////////////////////////////////////

        # region Caculate Injury

        //Correction
        //float totalInjuryCorrection = 0f;

        //totalInjuryCorrection += basicTrainingAreaDataScriptObj.TrainingArea.injuryCorrection;

        //Probability

        ///100% = 1f;
        float totalInjuryProbability = 0f;

        totalInjuryProbability += basicTrainingAreaDataScriptObj.TrainingArea.injuryprobability;

        totalInjuryProbability += Monster.Get_Injuries_Probability(3);

        #endregion Caculate Injury

        #region Caculate Disease

        float numberOfSickMonster = 0f;

        #endregion Caculate Disease

        #region Action

        int actionWeeks = playerMonsterInfo.lastMonsterAction.actionWeek;

        int passWeeks = 0;

        #region Distress (Lost)

        bool isLost = false;

        int lostWeek = 0;

        bool isReturned = false;

        float Distress_Total_Parameter_Correction = TrainingArea.Get_Exploration_Distress_Total_Parameter_Correction
            (
            complexParameter, basicTrainingAreaDataScriptObj.TrainingArea.explorationParametersCorrection,
            basicTrainingAreaDataScriptObj.TrainingArea.explorationRecommendedTotal, mainRaceStat, subRaceStat
            );

        #endregion Distress (Lost)

        #region Item Discovery

        float numberOfTicketConsumed = PlayerStats.Instance.playerInfo.itemRank2Ticket + PlayerStats.Instance.playerInfo.itemRank3Ticket;

        List<ExplorationItem> explorationItems = new List<ExplorationItem>();

        #endregion Item Discovery

        TrainingArea.TrainingType trainingResultType = TrainingArea.TrainingType.success;

        BasicParameters caculatedRaiseParameter = new BasicParameters();

        BasicParameters newComplexParameters = Monster.Get_Complex_Parameter(complexParameter, caculatedRaiseParameter);

        for (int i = 0; i < actionWeeks + lostWeek; i++)
        {
            passWeeks++;

            DebugPopup.LogRaise("///////////////");
            DebugPopup.LogRaise("Week pass = " + passWeeks);

            //Meal Caculate

            if (passWeeks <= actionWeeks)
            {
                newGrowthParameter.AddEnergy(food.Energy);

                newGrowthParameter.AddBody(food.Body);

                newGrowthParameter.AddCondition(food.Condition);

                newGrowthParameter.AddBodyTypeValue(food.BodyTypeValue);

                newGrowthParameter.AddFatigue(food.Fatigue);
                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddFatigue(-1);
                }

                newGrowthParameter.AddStress(food.Stress);
                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddStress(-1);
                }

                if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
                {
                    newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(0));
                }
                else
                if (basicMonsterDataScriptObj.Monster.hateFoods.Contains(foodType))
                {
                    newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(1));
                }
            }

            newGrowthParameter.AddEnergy(-5f);

            newGrowthParameter.AddBody(-5f);

            newGrowthParameter.AddCondition(-5f);

            //End Meal Caculate

            // Caculate Fatigue

            float fatigueTraitCorrection = innateTrait.Training_Fatigue_Reduction;

            newGrowthParameter.AddFatigue(Monster.Get_Fatigue_Increase_Caculated
                (
                    Monster.Get_Fatigue_IncreaseOrDecrease_Value(4) / 4f,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    fatigueTraitCorrection,
                    newGrowthParameter.bodyTypeValue,
                    newGrowthParameter.trainingPolicyValue
                ));

            fatigueType = Monster.Get_Fatigue_Type_By_Value(newGrowthParameter.fatigue);

            // Caculate Fatigue

            // Caculate Stress

            float stressTraitCorrection = innateTrait.Training_Stress_Reduction;

            newGrowthParameter.AddStress(Monster.Get_Stress_Increase_Caculated
            (
                Monster.Get_Stress_IncreaseOrDecrease_Value(5) / 4f,
                0f,
                mainRaceStat,
                subRaceStat,
                stressTraitCorrection,
                newGrowthParameter.bodyTypeValue,
                newGrowthParameter.trainingPolicyValue
            ));

            stressType = Monster.Get_Stress_Type_By_Value(newGrowthParameter.stress);

            // Caculate Stress

            float injuryTraitCorrection = innateTrait.Injury_Probability;

            //Caculate monster will be injury or not
            {
                Vector2 injuryRate = Monster.Get_Injury_Probability_Rate(totalInjuryProbability, injuryTraitCorrection, fatigueType, 0f);

                newGrowthParameter.injuryType = Monster.Get_Injury_Caculate_Is_Injury(newGrowthParameter.injuryType, injuryRate.x, injuryRate.y);

                if (newGrowthParameter.injuryType != lastInjuryType)
                {
                    injuryChange = true;
                }
            }

            float diseaseTraitCorrection = innateTrait.Disease_Probability;

            //Caculate monster will be disease or not
            {
                Vector2 diseasesRate = Monster.Get_Disease_Probability_Rate
                    (newGrowthParameter.energy, newGrowthParameter.body, newGrowthParameter.condition,
                    numberOfSickMonster,
                    diseaseTraitCorrection,
                    stressType
                    );

                newGrowthParameter.diseasesType = Monster.Get_Disease_Caculate_Is_Disease(newGrowthParameter.diseasesType, diseasesRate.x, diseasesRate.y);

                if (newGrowthParameter.diseasesType != lastDiseasesType)
                {
                    diseasesChange = true;
                }
            }

            newGrowthParameter.AddBodyTypeValue(Monster.Get_BodyType_Decrease_Per_Week());

            // Distress (Lost)

            if (isReturned == false)
            {
                if (isLost == false)
                {
                    isLost = TrainingArea.Get_Exploration_Distress_Is_Lost
                        (
                        Distress_Total_Parameter_Correction, 0f,
                        numberOfTicketConsumed, newGrowthParameter.injuryType, newGrowthParameter.diseasesType
                        );
                }
                else
                {
                    lostWeek++;

                    float firstWeekReturnProbability = TrainingArea.Get_Exploration_Distress_First_Week_Return_Probability(Distress_Total_Parameter_Correction, 0f, numberOfTicketConsumed, newGrowthParameter.injuryType, newGrowthParameter.diseasesType);

                    if (lostWeek == 1)
                    {
                        isReturned = TrainingArea.Get_Exploration_Distress_Is_Return(firstWeekReturnProbability, 0f, numberOfTicketConsumed);
                    }
                    else
                    {
                        float N_WeekReturnProbability = TrainingArea.Get_Exploration_Distress_N_Week_Return_Probability(firstWeekReturnProbability, lostWeek, newGrowthParameter.injuryType, newGrowthParameter.diseasesType);

                        isReturned = TrainingArea.Get_Exploration_Distress_Is_Return(N_WeekReturnProbability, 0f, numberOfTicketConsumed);
                    }

                    if (isReturned)
                    {
                        isLost = false;
                    }
                }
            }

            if (isLost == false)
            {
                if (passWeeks >= actionWeeks)
                {
                    #region Caculate Parameter Rise

                    Debug.Log("passWeeks = " + passWeeks);
                    Debug.Log("actionWeeks = " + actionWeeks);

                    caculatedRaiseParameter.health += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health;

                    caculatedRaiseParameter.strenght += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght;

                    caculatedRaiseParameter.intelligent += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent;

                    caculatedRaiseParameter.dexterity += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity;

                    caculatedRaiseParameter.agility += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility;

                    caculatedRaiseParameter.vitality += basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality;

                    #endregion Caculate Parameter Rise

                    newComplexParameters = Monster.Get_Complex_Parameter(complexParameter, caculatedRaiseParameter);
                }

                // Caculate Item discovery

                explorationItems.AddRange(GetExplorationItemFound
                    (
                    actionWeeks, playerMonsterInfo, newComplexParameters, basicMonsterDataScriptObj, basicTrainingAreaDataScriptObj, mainRaceStat, subRaceStat,
                    innateTrait, newGrowthParameter.injuryType, newGrowthParameter.diseasesType, PlayerStats.Instance.playerInfo.itemRank2Ticket, PlayerStats.Instance.playerInfo.itemRank3Ticket
                    ));

                DebugPopup.LogRaise("Item found count = " + explorationItems.Count);
            }

            // Caculate LifeSpan

            //-60 (10x4) on exploration
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Consumption(1, -15f, fatigueType, stressType));

            // Caculate LifeSpan

            if (isLost == false && newGrowthParameter.injuryType == Monster.InjuryType.Serious_Injury || isLost == false && newGrowthParameter.diseasesType == Monster.DiseasesType.Serious_Diseases)
            {
                trainingResultType = TrainingArea.TrainingType.failure;

                break;
            }

            if(isLost == false && passWeeks >= actionWeeks)
            {
                break;
            }
        }

        DebugPopup.LogRaise($"Fatigue Type = {fatigueType}");
        DebugPopup.LogRaise($"Stress Type = {stressType}");
        DebugPopup.LogRaise($"Injury Type = {newGrowthParameter.injuryType}");
        DebugPopup.LogRaise($"Disease Type = {newGrowthParameter.diseasesType}");

        #region Caculate training success or fail

        //if (newGrowthParameter.injuryType != Monster.InjuryType.Serious_Injury && newGrowthParameter.diseasesType != Monster.DiseasesType.Serious_Diseases)
        //{
        //    trainingResultType = TrainingArea.Get_Training_Sucess_Failure_Probability
        //        (
        //           newGrowthParameter.trainingPolicyValue,
        //           newGrowthParameter.affection,
        //           0f,
        //           0f,
        //           newGrowthParameter.injuryType,
        //           newGrowthParameter.diseasesType
        //        );
        //}

        #endregion Caculate training success or fail

        #endregion Action

        /////////////////////////////////////////

        #region Affection

        if (fatigueType == Monster.FatigueType.Fatigue)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(6));
        }
        else if (fatigueType == Monster.FatigueType.Overwork)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(8));
        }

        if (stressType == Monster.StressType.Stress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(9));
        }
        else if (stressType == Monster.StressType.OverStress)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(11));
        }

        if (trainingResultType != TrainingArea.TrainingType.failure)
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(2));
        }
        else
        {
            newGrowthParameter.AddAffection(Monster.Get_Affection_IncreaseOrDecrease_Value(3));
        }

        #endregion Affection

        #region Training Policy

        if (basicMonsterDataScriptObj.Monster.loveFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(0));
        }

        if (basicMonsterDataScriptObj.Monster.hateFoods.Contains(foodType))
        {
            newGrowthParameter.AddTrainingPolicyValue(Monster.Get_TrainingPolicy_IncreaseOrDecrease_Value(1));
        }

        newGrowthParameter.AddTrainingPolicyValue(-4);

        #endregion Training Policy

        #region Caculate LifeSpan

        if (injuryChange)
        {
            if (newGrowthParameter.injuryType == Monster.InjuryType.Injury)
            {
                newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(4));
            }
            else if (newGrowthParameter.injuryType == Monster.InjuryType.Serious_Injury)
            {
                newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(5));
            }
        }

        if (newGrowthParameter.diseasesType == Monster.DiseasesType.Diseases)
        {
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(6));
        }
        else if (newGrowthParameter.diseasesType == Monster.DiseasesType.Serious_Diseases)
        {
            newGrowthParameter.AddLifeSpan(Monster.Get_LifeSpan_Decrease_Value(7));
        }

        #endregion Caculate LifeSpan

        #region Update action infos

        //Get changed value between old and new
        GrowthParameters actionGrowthParameter = new()
        {
            lifeSpan = newGrowthParameter.lifeSpan - playerMonsterInfo.growthParameters.lifeSpan,
            bodyTypeValue = newGrowthParameter.bodyTypeValue - playerMonsterInfo.growthParameters.bodyTypeValue,
            trainingPolicyValue = newGrowthParameter.trainingPolicyValue - playerMonsterInfo.growthParameters.trainingPolicyValue,
            affection = newGrowthParameter.affection - playerMonsterInfo.growthParameters.affection,
            fatigue = newGrowthParameter.fatigue - playerMonsterInfo.growthParameters.fatigue,
            stress = newGrowthParameter.stress - playerMonsterInfo.growthParameters.stress,
            injuryType = newGrowthParameter.injuryType,
            diseasesType = newGrowthParameter.diseasesType,

            energy = newGrowthParameter.energy - playerMonsterInfo.growthParameters.energy,
            body = newGrowthParameter.body - playerMonsterInfo.growthParameters.body,
            condition = newGrowthParameter.condition - playerMonsterInfo.growthParameters.condition
        };

        //End changed value between old and new

        UpdateActionChange(playerMonsterInfo, caculatedRaiseParameter, actionGrowthParameter, passWeeks, injuryChange, diseasesChange, explorationItems, isLost, lostWeek, trainingResultType);

        #endregion Update action infos

        #region Apply change to playerstats

        playerMonsterInfo.growthParameters = newGrowthParameter;

        playerMonsterInfo.basicParameters = Monster.Get_Subtract_Parameter(newComplexParameters, basicMonsterDataScriptObj.Monster.basicParameters);

        playerMonsterInfo.rankType = Monster.Get_Monster_BasicParameters_Rank_By_Value(newComplexParameters);

        //PlayerStats.instance.Save();

        #endregion Apply change to playerstats

        DebugPopup.InTraining(false);
    }

    public static BasicParameters CaculateParameterRise
    (
    PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj basicMonsterDataScriptObj, BasicParameters complexParameter, TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj,
    int mainRaceStat, int subRaceStat, Monster.InjuryType injuryType, Monster.DiseasesType diseaseType, Monster.GrowthType growthType, TrainingArea.TrainingType trainingResultType
    )
    {
        float m_health_RiseValue = 0f;

        float m_strenght_RiseValue = 0f;

        float m_intelligent_RiseValue = 0f;

        float m_dexterity_RiseValue = 0f;

        float m_agility_RiseValue = 0f;

        float m_vitality_RiseValue = 0f;

        PersonalityTrait.PersonalityTraitInfo trainingTrait = PersonalityTrait.GetTraitInfo(playerMonsterInfo.personalityTrait);

        DebugPopup.LogRaise($"PersonalityTrait = {playerMonsterInfo.personalityTrait}");

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health != 0f)
        {
            //Main Health
            DebugPopup.LogRaise("Main Health");
            m_health_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.health,
                    basicMonsterDataScriptObj.Monster.HP_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.health,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght != 0f)
        {
            //Main Strenght
            DebugPopup.LogRaise("Main Strenght");
            m_strenght_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.strenght,
                    basicMonsterDataScriptObj.Monster.STR_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.strenght,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent != 0f)
        {
            //Main intelligent
            DebugPopup.LogRaise("Main intelligent");
            m_intelligent_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.intelligent,
                    basicMonsterDataScriptObj.Monster.INT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.intelligent,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity != 0f)
        {
            //Main dexterity
            DebugPopup.LogRaise("Main dexterity");
            m_dexterity_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.dexterity,
                    basicMonsterDataScriptObj.Monster.DEX_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.dexterity,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility != 0f)
        {
            //Main agility
            DebugPopup.LogRaise("Main agility");
            m_agility_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.agility,
                    basicMonsterDataScriptObj.Monster.AGI_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.agility,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        if (basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality != 0f)
        {
            //Main vitality
            DebugPopup.LogRaise("Main vitality");
            m_vitality_RiseValue = TrainingArea.Get_Training_Parameter_Rise_Up
                    (
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRiseParameter.vitality,
                    basicMonsterDataScriptObj.Monster.VIT_GA_Rank,
                    0f,
                    mainRaceStat,
                    subRaceStat,
                    trainingTrait.TrainingCorrection.vitality,
                    injuryType,
                    diseaseType,
                    growthType,
                    playerMonsterInfo.growthParameters.lifeSpan,
                    basicMonsterDataScriptObj.Monster.growthParameters.lifeSpan,
                    complexParameter,
                    trainingResultType
                   );
        }

        #region Add to Complex parameter

        BasicParameters raiseParameters = new BasicParameters();

        raiseParameters.health = m_health_RiseValue;
        raiseParameters.strenght = m_strenght_RiseValue;
        raiseParameters.intelligent = m_intelligent_RiseValue;
        raiseParameters.dexterity = m_dexterity_RiseValue;
        raiseParameters.agility = m_agility_RiseValue;
        raiseParameters.vitality = m_vitality_RiseValue;

        #endregion

        return raiseParameters;
    }

    public static List<ExplorationItem> GetExplorationItemFound
        (
        int actionWeeks,PlayerInfo.PlayerMonsterInfo playerMonsterInfo, BasicParameters monsterComplexParameters, MonsterDataScriptObj basicMonsterDataScriptObj, TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj,
        int mainRaceStat, int subRaceStat, InnateTrait.InnateTraitInfo innateTrait, Monster.InjuryType injuryType, Monster.DiseasesType diseasesType, float rank2TicketConsumed, float rank3TicketConsumed)
    {
        DebugPopup.LogRaise("Find Item");

        DebugPopup.LogRaise("rank2 Ticket Consumed = " + rank2TicketConsumed);
        DebugPopup.LogRaise("rank3 Ticket Consumed = " + rank3TicketConsumed);

        float itemAcquisitionTraitCorrection = 0f;

        float itemTraitCorrection = innateTrait.Find_Item_Rate;

        float itemRankUpTraitCorrection = innateTrait.Item_Rank_Up_Rate;

        float totalInjuryCorrection = 0f;
        ///If you are injured, the parameters at the time of judgment are reduced by 10%.
        if (injuryType != Monster.InjuryType.None)
        {
            totalInjuryCorrection = 0.1f;
        }

        float totalDiseaseCorrection = 0f;
        ///If you are sick, the parameters at the time of judgment are reduced by 10%.
        if (diseasesType != Monster.DiseasesType.None)
        {
            totalDiseaseCorrection = 0.1f;
        }

        List<ExplorationItem> foundList = new List<ExplorationItem>();

        for (int i = 0; i < actionWeeks; i++)
        {
            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Find Item A");
            //Find item A
            float itemA_DiscoveryProbability = TrainingArea.Get_Exploration_Item_Discovery_Probability
                (
                    monsterComplexParameters,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationParametersCorrection,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRecommendedTotal,
                    totalInjuryCorrection,
                    injuryType,
                    totalDiseaseCorrection,
                    diseasesType,
                    mainRaceStat,
                    subRaceStat,
                    rank2TicketConsumed + rank3TicketConsumed,
                    itemAcquisitionTraitCorrection,
                    ExplorationItem.ItemAcquisitionType.a
                );

            itemA_DiscoveryProbability += itemTraitCorrection;

            float Rank2IncreaseCompensation = 0f;
            float Rank3IncreaseCompensation = 0f;

            Vector2 rank2rank3Rate = TrainingArea.Get_Exploration_Item_Rank_Increase_Rate(rank2TicketConsumed, rank3TicketConsumed, Rank2IncreaseCompensation, Rank3IncreaseCompensation);
            rank2rank3Rate += new Vector2(itemRankUpTraitCorrection, itemRankUpTraitCorrection);

            ExplorationItem itemA = TrainingArea.Get_Exploration_Item_Found(ExplorationItem.ItemAcquisitionType.a, itemA_DiscoveryProbability, rank2rank3Rate.x, rank2rank3Rate.y);

            if (itemA != null)
            {
                itemA.itemAcquisitionType = ExplorationItem.ItemAcquisitionType.a;
                foundList.Add(itemA);

                DebugPopup.LogRaise("Item A found");
                DebugPopup.LogRaise("Item rank = " + itemA.discoveryRank);
            }

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Find Item B");
            //Find item B
            float itemB_DiscoveryProbability = TrainingArea.Get_Exploration_Item_Discovery_Probability
                (
                    monsterComplexParameters,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationParametersCorrection,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRecommendedTotal,
                    totalInjuryCorrection,
                    injuryType,
                    totalDiseaseCorrection,
                    diseasesType,
                    mainRaceStat,
                    subRaceStat,
                    rank2TicketConsumed + rank3TicketConsumed,
                    itemAcquisitionTraitCorrection,
                    ExplorationItem.ItemAcquisitionType.a
                );

            itemB_DiscoveryProbability += itemTraitCorrection;

            Rank2IncreaseCompensation = 0f;
            Rank3IncreaseCompensation = 0f;

            rank2rank3Rate = TrainingArea.Get_Exploration_Item_Rank_Increase_Rate(rank2TicketConsumed, rank3TicketConsumed, Rank2IncreaseCompensation, Rank3IncreaseCompensation);
            rank2rank3Rate += new Vector2(itemRankUpTraitCorrection, itemRankUpTraitCorrection);

            ExplorationItem itemB = TrainingArea.Get_Exploration_Item_Found(ExplorationItem.ItemAcquisitionType.b, itemB_DiscoveryProbability, rank2rank3Rate.x, rank2rank3Rate.y);

            if (itemB != null)
            {
                itemB.itemAcquisitionType = ExplorationItem.ItemAcquisitionType.b;
                foundList.Add(itemB);

                DebugPopup.LogRaise("Item B found");
                DebugPopup.LogRaise("Item rank = " + itemB.discoveryRank);
            }

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Find Item C");
            //Find item C
            float itemC_DiscoveryProbability = TrainingArea.Get_Exploration_Item_Discovery_Probability
                (
                    monsterComplexParameters,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationParametersCorrection,
                    basicTrainingAreaDataScriptObj.TrainingArea.explorationRecommendedTotal,
                    totalInjuryCorrection,
                    injuryType,
                    totalDiseaseCorrection,
                    diseasesType,
                    mainRaceStat,
                    subRaceStat,
                    rank2TicketConsumed + rank3TicketConsumed,
                    itemAcquisitionTraitCorrection,
                    ExplorationItem.ItemAcquisitionType.a
                );

            itemC_DiscoveryProbability += itemTraitCorrection;

            Rank2IncreaseCompensation = 0f;
            Rank3IncreaseCompensation = 0f;

            rank2rank3Rate = TrainingArea.Get_Exploration_Item_Rank_Increase_Rate(rank2TicketConsumed, rank3TicketConsumed, Rank2IncreaseCompensation, Rank3IncreaseCompensation);
            rank2rank3Rate += new Vector2(itemRankUpTraitCorrection, itemRankUpTraitCorrection);

            ExplorationItem itemC = TrainingArea.Get_Exploration_Item_Found(ExplorationItem.ItemAcquisitionType.c, itemC_DiscoveryProbability, rank2rank3Rate.x, rank2rank3Rate.y);

            if (itemC != null)
            {
                itemC.itemAcquisitionType = ExplorationItem.ItemAcquisitionType.c;
                foundList.Add(itemC);

                DebugPopup.LogRaise("Item C found");
                DebugPopup.LogRaise("Item rank = " + itemC.discoveryRank);
            }
        }

        //Debug

        //foreach (ExplorationItem item in foundList)
        //{
        //    DebugPopup.LogRaise("Item found = " + item.itemAcquisitionType.ToString());
        //    DebugPopup.LogRaise("Item rank = " + item.discoveryRank.ToString());
        //}

        //End debug

        return foundList;
    }

    public static void UpdateActionChange(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, BasicParameters changeParameters, GrowthParameters changeGrowthParameters, int totalWeekPassed
        , bool injuryChange, bool diseasesChange, List<ExplorationItem> explorationItems, bool isLost, int lostWeek, TrainingArea.TrainingType trainingType)
    {
        TrainingAreaDataScriptObj basicTrainingAreaDataScriptObj = PlayerStats.Instance.trainingAreaDataManagerScriptObj.GetTrainingAreaDataObj(playerMonsterInfo.lastMonsterAction.trainingAreaType);

        playerMonsterInfo.lastMonsterAction.weekPassed = totalWeekPassed;

        playerMonsterInfo.lastMonsterAction.basicParameters.health += changeParameters.health;
        playerMonsterInfo.lastMonsterAction.basicParameters.strenght += changeParameters.strenght;
        playerMonsterInfo.lastMonsterAction.basicParameters.intelligent += changeParameters.intelligent;
        playerMonsterInfo.lastMonsterAction.basicParameters.dexterity += changeParameters.dexterity;
        playerMonsterInfo.lastMonsterAction.basicParameters.agility += changeParameters.agility;
        playerMonsterInfo.lastMonsterAction.basicParameters.vitality += changeParameters.vitality;

        playerMonsterInfo.lastMonsterAction.growthParameters.lifeSpan += changeGrowthParameters.lifeSpan;

        playerMonsterInfo.lastMonsterAction.growthParameters.bodyTypeValue += changeGrowthParameters.bodyTypeValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.trainingPolicyValue += changeGrowthParameters.trainingPolicyValue;

        playerMonsterInfo.lastMonsterAction.growthParameters.affection += changeGrowthParameters.affection;

        playerMonsterInfo.lastMonsterAction.growthParameters.fatigue += changeGrowthParameters.fatigue;

        playerMonsterInfo.lastMonsterAction.growthParameters.stress += changeGrowthParameters.stress;

        if (injuryChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.injuryType = changeGrowthParameters.injuryType;

        if (diseasesChange)
            playerMonsterInfo.lastMonsterAction.growthParameters.diseasesType = changeGrowthParameters.diseasesType;

        playerMonsterInfo.lastMonsterAction.isLost = isLost;

        playerMonsterInfo.lastMonsterAction.lostWeek = lostWeek;

        Food.FoodType foodType = playerMonsterInfo.lastFoodType;
        Food food = PlayerStats.Instance.foodDataManagerScriptObj.GetFood(foodType);

        float price = 0f;
        price += basicTrainingAreaDataScriptObj.TrainingArea.explorationAreaPrice;
        price += food.FoodPrice * playerMonsterInfo.lastMonsterAction.actionWeek;

        playerMonsterInfo.lastMonsterAction.lostGold = lostWeek * (price * 0.25f);

        playerMonsterInfo.lastMonsterAction.explorationItems.AddRange(explorationItems);

        #region Meal parameters
        playerMonsterInfo.lastMonsterAction.growthParameters.energy += changeGrowthParameters.energy;
        playerMonsterInfo.lastMonsterAction.growthParameters.body += changeGrowthParameters.body;
        playerMonsterInfo.lastMonsterAction.growthParameters.condition += changeGrowthParameters.condition;
        #endregion

        playerMonsterInfo.lastMonsterAction.trainingType = trainingType;

        if (lostWeek > 0)
        {
            int lostItem = Mathf.FloorToInt(playerMonsterInfo.lastMonsterAction.explorationItems.Count / 2);

            for (int i = 0; i < lostItem; i++)
            {
                if (playerMonsterInfo.lastMonsterAction.explorationItems.Count > 0)
                {
                    int ranNum = UnityEngine.Random.Range(0, playerMonsterInfo.lastMonsterAction.explorationItems.Count);

                    playerMonsterInfo.lastMonsterAction.explorationItems.Remove(playerMonsterInfo.lastMonsterAction.explorationItems[ranNum]);
                }
            }
        }

        if (changeGrowthParameters.injuryType == Monster.InjuryType.Serious_Injury || changeGrowthParameters.diseasesType == Monster.DiseasesType.Serious_Diseases)
        {
            float backGold = playerMonsterInfo.lastMonsterAction.foodGold - (playerMonsterInfo.lastMonsterAction.foodGold / playerMonsterInfo.lastMonsterAction.actionWeek) * totalWeekPassed;

            PlayerStats.Instance.playerInfo.AddGold(backGold);
        }

        playerMonsterInfo.lastMonsterAction.actionDone = true;

        DebugPopup.LogRaise("Lost Week = " + lostWeek);

        PlayerStats.Instance.playerInfo.SubtractBiT(playerMonsterInfo.lastMonsterAction.lostGold);

        PlayerStats.Instance.Save();

        //Debug.Log("Action is updated");
    }
}
