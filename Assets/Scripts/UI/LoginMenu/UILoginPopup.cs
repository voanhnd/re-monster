﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.UI.LoginMenu
{
    public class UILoginPopup : MonoBehaviour
    {
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private RectTransform container;
        [SerializeField] private float animationDurationOpen;
        [SerializeField] private float animationDurationClose;
        [SerializeField] private float animationDelayOpen;
        [SerializeField] private float animationDelayClose;
        [SerializeField] private Ease animationEaseOpen;
        [SerializeField] private Ease animationEaseClose;
        [SerializeField] private GameObject closeBtnGameObj;

        private void Start()
        {
            gameObject.SetActive(false);
            closeBtnGameObj.SetActive(false);
            container.sizeDelta = closeSize;
        }

        public void OpenPopup()
        {
            gameObject.SetActive(true);
            container.DOSizeDelta(openSize, animationDurationOpen).SetEase(animationEaseOpen).SetDelay(animationDelayOpen).OnComplete(() =>
            {
                closeBtnGameObj.SetActive(true);
            });
        }

        public void ClosePopup()
        {
            closeBtnGameObj.SetActive(false);
            container.DOSizeDelta(closeSize, animationDurationClose).SetEase(animationEaseClose).SetDelay(animationDelayClose).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}