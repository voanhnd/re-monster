﻿using UnityEngine;

namespace Assets.Scripts.UI.TabMenu
{
    public class UITabGroupSprite : UITabGroup
    {
        [SerializeField] private Sprite idleSpriteBtn;
        [SerializeField] private Sprite hoverSpriteBtn;
        [SerializeField] private Sprite selectedSpriteBtn;

        public override void SetTabEnter(UITabButton button)
        {
            base.SetTabEnter(button);
            if (CurrentButton != null && CurrentButton == button) return;
            UITabButtonSprite colorTabButton = button as UITabButtonSprite;
            colorTabButton.SetCurrentSprite(hoverSpriteBtn);
        }

        public override void SetTabSelected(UITabButton button)
        {
            base.SetTabSelected(button);
            UITabButtonSprite currentButton = CurrentButton as UITabButtonSprite;
            currentButton.SetCurrentSprite(selectedSpriteBtn);
        }

        public override void ResetTabs()
        {
            for (int i = 0; i < TabButtons.Count; i++)
            {
                if (CurrentButton != null && CurrentButton == TabButtons[i]) { continue; }
                UITabButtonSprite tabColorBtn = TabButtons[i] as UITabButtonSprite;
                tabColorBtn.SetCurrentSprite(idleSpriteBtn);
            }
        }
    }
}