﻿using UnityEngine;

namespace Assets.Scripts.UI.TabMenu
{
    public class UITabGroupColor : UITabGroup
    {
        [SerializeField] private Color idleColorBtn;
        [SerializeField] private Color hoverColorBtn;
        [SerializeField] private Color selectedColorBtn;

        public override void SetTabEnter(UITabButton button)
        {
            base.SetTabEnter(button);
            if (CurrentButton != null && CurrentButton == button) return;
            UITabButtonColor colorTabButton = button as UITabButtonColor;
            colorTabButton.SetCurrentColor(hoverColorBtn);
        }

        public override void SetTabSelected(UITabButton button)
        {
            base.SetTabSelected(button);
            UITabButtonColor currentButton = CurrentButton as UITabButtonColor;
            currentButton.SetCurrentColor(selectedColorBtn);
        }

        public override void ResetTabs()
        {
            for (int i = 0; i < TabButtons.Count; i++)
            {
                UITabButtonColor tabColorBtn = TabButtons[i] as UITabButtonColor;
                if (CurrentButton != null && CurrentButton == TabButtons[i])
                {
                    tabColorBtn.SetCurrentColor(selectedColorBtn);
                    continue;
                }
                tabColorBtn.SetCurrentColor(idleColorBtn);
            }
        }
    }
}