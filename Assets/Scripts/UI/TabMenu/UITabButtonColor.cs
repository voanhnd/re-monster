﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TabMenu
{
    [RequireComponent(typeof(Image))]
    public class UITabButtonColor : UITabButton
    {
        private Image buttonGraphic;

        public Image ButtonGraphic => buttonGraphic;

        protected override void Awake()
        {
            base.Awake();
            buttonGraphic = GetComponent<Image>();
        }

        public void SetCurrentColor(Color color)
        {
            buttonGraphic.color = color;
        }
    }
}