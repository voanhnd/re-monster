﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI.TabMenu
{
    public abstract class UITabGroup : MonoBehaviour
    {
        [Header("Events")]
        [SerializeField] private UnityEvent onTabSwitched;
        [Space(10)]
        private List<UITabButton> tabButtons = new();
        [SerializeField] private List<UITabPage> tabPages = new();
        private UITabPage targetPage;
        private UITabPage currentPage;
        private UITabButton currentButton;

        public UnityEvent OnTabSwitched { get => onTabSwitched; set => onTabSwitched = value; }
        public UITabPage TargetPage => targetPage;
        public UITabPage CurrentPage => currentPage;
        public UITabButton CurrentButton => currentButton;
        public List<UITabButton> TabButtons => tabButtons;

        protected virtual void Start()
        {
            SetCurrentPage(tabPages[0]);
            if (tabButtons.Count > 0)
                currentButton = tabButtons[0];
        }

        private void SetCurrentPage(UITabPage targetPage)
        {
            if (CurrentPage == null || targetPage != CurrentPage)
            {
                this.targetPage = targetPage;
                OnTabSwitched.Invoke();
            }

            for (int i = 0; i < tabPages.Count; i++)
            {
                if (tabPages[i] == targetPage)
                {
                    tabPages[i].OpenMenu();
                }
                else
                {
                    tabPages[i].CloseMenu();
                }
            }
        }

        public virtual void SubcribeToGroup(UITabButton button)
        {
            tabButtons ??= new();
            tabButtons.Add(button);
            if (button.transform.GetSiblingIndex() == 0)
            {
                currentButton = button;
                button.Select();
            }
        }

        public void ClearTargetPage()
        {
            targetPage = null;
        }

        #region Tab control
        public virtual void SetTabEnter(UITabButton button)
        {
            ResetTabs();
        }

        public virtual void SetTabExit(UITabButton button)
        {
            ResetTabs();
        }

        public virtual void SetTabSelected(UITabButton button)
        {
            if (currentButton != null)
            {
                currentButton.Deselect();
            }
            currentButton = button;
            currentButton.Select();
            ResetTabs();
            int index = button.transform.GetSiblingIndex();
            for (int i = 0; i < tabPages.Count; i++)
            {
                if (i == index)
                {
                    SetCurrentPage(tabPages[i]);
                }
            }
        }

        public abstract void ResetTabs();
        #endregion
    }
}