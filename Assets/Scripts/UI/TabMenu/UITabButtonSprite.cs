﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.TabMenu
{
    [RequireComponent(typeof(Image))]
    public class UITabButtonSprite : UITabButton
    {
        private Image graphicImage;

        protected override void Awake()
        {
            base.Awake();
            graphicImage = GetComponent<Image>();
        }

        internal void SetCurrentSprite(Sprite targetSprite)
        {
            graphicImage.sprite = targetSprite;
        }
    }
}