﻿namespace Assets.Scripts.UI.TabMenu
{
    public class UITabPageActive : UITabPage
    {
        private void Awake()
        {
            IsPageOpened = gameObject.activeSelf;
        }

        public override void OpenMenu()
        {
            base.OpenMenu();
            ShowMenu();
        }

        public override void CloseMenu()
        {
            base.CloseMenu();
            HideMenu();
        }

        private void HideMenu()
        {
            gameObject.SetActive(false);
        }

        private void ShowMenu()
        {
            gameObject.SetActive(true);
        }
    }
}