﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI.TabMenu
{
    public abstract class UITabPage : MonoBehaviour
    {
        [SerializeField] private UnityEvent onTabMenuOpened;
        [SerializeField] private UnityEvent onTabMenuClosed;

        public UnityEvent OnTabMenuOpened { get => onTabMenuOpened; set => onTabMenuOpened = value; }
        public UnityEvent OnTabMenuClosed { get => onTabMenuClosed; set => onTabMenuClosed = value; }
        private bool isPageOpened = false;
        public bool IsPageOpened { get => isPageOpened; internal set => isPageOpened = value; }

        public virtual void OpenMenu()
        {
            if (IsPageOpened) return;
            onTabMenuOpened.Invoke();
            IsPageOpened = true;
        }

        public virtual void CloseMenu()
        {
            if (!IsPageOpened) return;
            onTabMenuClosed.Invoke();
            IsPageOpened = false;
        }
    }
}