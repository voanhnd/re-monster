﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI.TabMenu
{
    public abstract class UITabButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private UITabGroup tabGroup;
        [SerializeField] private UnityEvent onTabSelected;
        [SerializeField] private UnityEvent onTabDeselected;

        public UnityEvent OnTabSelected { get => onTabSelected; set => onTabSelected = value; }
        public UnityEvent OnTabDeselected { get => onTabDeselected; set => onTabDeselected = value; }
        public UITabGroup TabGroup => tabGroup;

        protected virtual void Awake()
        {
            tabGroup.SubcribeToGroup(this);
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            tabGroup.SetTabSelected(this);
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            tabGroup.SetTabEnter(this);
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            tabGroup.SetTabExit(this);
        }

        public virtual void Select()
        {
            onTabSelected.Invoke();
        }

        public virtual void Deselect()
        {
            onTabDeselected.Invoke();
        }
    }
}