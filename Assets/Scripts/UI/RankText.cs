﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class RankText : Singleton<RankText>
    {
        [SerializeField] private List<RankDisplayText> rankDisplays = new();
        [SerializeField] private Sprite nullSprite;
        public List<RankDisplayText> RankDisplays => rankDisplays;

        private void Start()
        {
            Destroy(gameObject);
        }

        public Sprite GetRank(RankTypeEnums rank)
        {
            for (int i = 0; i < rankDisplays.Count; i++)
            {
                if (rank == rankDisplays[i].Rank)
                {
                    return rankDisplays[i].RankSprite;
                }
            }
            return nullSprite;
        }

        public Sprite NullRank => nullSprite;

        [System.Serializable]
        public class RankDisplayText
        {
            [SerializeField] private RankTypeEnums rank;
            [SerializeField] private Sprite rankSprite;

            public RankTypeEnums Rank => rank;
            public Sprite RankSprite => rankSprite;
        }
    }
}