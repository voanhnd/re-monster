﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class AutoScrollTween : MonoBehaviour
    {
        [SerializeField] private ScrollRect controlScrollRect;
        [SerializeField] private float scrollTime;
        [SerializeField] private float delayTime;
        [SerializeField] private Ease scrollEase;
        [SerializeField] private bool isVertical = true;
        private Sequence animationSequence;
        private void OnEnable()
        {
            controlScrollRect.verticalNormalizedPosition = 0;
             animationSequence = DOTween.Sequence();
            if (isVertical)
            {
                animationSequence.Append(controlScrollRect.DOVerticalNormalizedPos(1, scrollTime).SetEase(scrollEase))
                    .Append(controlScrollRect.DOVerticalNormalizedPos(0, scrollTime).SetEase(scrollEase).SetDelay(delayTime))
                    .SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                animationSequence.Append(controlScrollRect.DOHorizontalNormalizedPos(1, scrollTime).SetEase(scrollEase))
                    .Append(controlScrollRect.DOHorizontalNormalizedPos(0, scrollTime).SetEase(scrollEase).SetDelay(delayTime))
                    .SetLoops(-1, LoopType.Yoyo);
            }
        }

        private void OnDisable()
        {
            animationSequence.Kill(true);
        }
    }
}