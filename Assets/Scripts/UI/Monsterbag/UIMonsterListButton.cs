﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Monsterbag
{
    public class UIMonsterListButton : MonoBehaviour
    {
        [SerializeField] private Image monsterImage;
        [SerializeField] private Image monsterRankImage;
        private Button functionButton;

        public Button FunctionButton => functionButton;

        private void Awake()
        {
            functionButton = GetComponent<Button>();
        }

        public void SetupButton(Sprite monsterImage, RankTypeEnums monsterRank)
        {
            this.monsterImage.sprite = monsterImage;
            this.monsterRankImage.sprite = RankText.Instance.GetRank(monsterRank);
        }
    }
}