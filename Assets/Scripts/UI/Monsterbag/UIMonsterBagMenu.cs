﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.Monsterbag
{
    public class UIMonsterBagMenu : MonoBehaviour
    {
        [SerializeField] private GameObject monsterButtonPrefab;
        [SerializeField] private Transform bagContainer;
        private List<UIMonsterListButton> monsterListBtn = new();

        public List<UIMonsterListButton> MonsterListBtn => monsterListBtn;

        public UIMonsterListButton AddMonsterToBag(RankTypeEnums monsterRank, Sprite monsterSprite)
        {
            GameObject monsterButtonRef = Instantiate(monsterButtonPrefab, bagContainer);
            if (monsterButtonRef.TryGetComponent(out UIMonsterListButton monsterButton))
            {
                monsterListBtn.Add(monsterButton);
                monsterButton.SetupButton(monsterSprite, monsterRank);
                return monsterButton;
            }
            return null;
        }

        public void RemoveMonsterFromBag(UIMonsterListButton monsterButton)
        {
            if (monsterListBtn.Contains(monsterButton))
            {
                Destroy(monsterButton);
                monsterListBtn.Remove(monsterButton);
            }
        }

        public void Clearbag()
        {
            Helpers.DestroyChildren(bagContainer);
            monsterListBtn.Clear();
        }
    }
}