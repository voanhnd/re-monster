﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI.Utilities
{
    public class HoverListener : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private UnityEvent<PointerEventData> onPointerEnter;
        [SerializeField] private UnityEvent<PointerEventData> onPointerExit;

        public UnityEvent<PointerEventData> OnPointerEnter1 => onPointerEnter;
        public UnityEvent<PointerEventData> OnPointerExit1 => onPointerExit;

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter.Invoke(eventData);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onPointerExit.Invoke(eventData);
        }
    }
}