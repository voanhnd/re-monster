﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Utilities
{
    public class UIToggleableRect : MonoBehaviour
    {
        [SerializeField] private Toggle uiToggle;
        [SerializeField] private RectTransform controlRect;
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private Ease openEase;
        [SerializeField] private Ease closeEase;
        [SerializeField] private Image toggleGraphic;
        [SerializeField] private Sprite toggleOnSprite;
        [SerializeField] private Sprite toggleOffSprite;

        public Toggle UiToggle => uiToggle;

        private void Awake()
        {
            uiToggle.onValueChanged.AddListener((bool value) =>
            {
                ToggleComponent(value);
            });
            controlRect.sizeDelta = uiToggle.isOn ? openSize : closeSize;
            if (toggleGraphic != null)
                toggleGraphic.sprite = uiToggle.isOn ? toggleOnSprite : toggleOffSprite;
        }

        private void ToggleComponent(bool value)
        {
            controlRect.DOKill();
            if (value)
            {
                controlRect.DOSizeDelta(openSize, openTime).SetEase(openEase);
                if (toggleGraphic != null)
                    toggleGraphic.sprite = toggleOnSprite;
                return;
            }
            controlRect.DOSizeDelta(closeSize, closeTime).SetEase(closeEase);
            if (toggleGraphic != null)
                toggleGraphic.sprite = toggleOffSprite;
        }
    }
}