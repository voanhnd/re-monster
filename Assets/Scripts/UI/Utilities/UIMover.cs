﻿using UnityEngine;

namespace Assets.Scripts.UI.Utilities
{
    public class UIMover : MonoBehaviour
    {
        [SerializeField] private Vector2 speed;
        [SerializeField] private bool limitboundToLocalPos = true;
        private Vector2 objectBound;
        private Vector2 origincalPos;
        private Vector2 screenBound;
        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            objectBound = rectTransform.sizeDelta;
            origincalPos = rectTransform.localPosition;
            float cameraSize = Camera.main.orthographicSize;
            float cameraWidth = cameraSize * Camera.main.aspect * 2f;
            float cameraHeight = cameraSize * 2;
            screenBound = new(cameraWidth, cameraHeight);
        }

        private void Update()
        {
            if (limitboundToLocalPos)
                CheckOutOfBound();
            else
                CheckOutOfCamera();
        }

        private void LateUpdate()
        {
            MoveObject();
        }

        private void MoveObject()
        {
            Vector2 currentPos = rectTransform.position;
            Vector2 nextPos = currentPos + speed * Time.deltaTime;
            rectTransform.position = nextPos;
        }

        private void CheckOutOfBound()
        {
            if (speed.x < 0 && rectTransform.localPosition.x < -objectBound.x - origincalPos.x)
                rectTransform.localPosition = new(objectBound.x - rectTransform.localPosition.x, rectTransform.localPosition.y);

            if (speed.x > 0 && rectTransform.localPosition.x > objectBound.x + origincalPos.x)
                rectTransform.localPosition = new(-objectBound.x + rectTransform.localPosition.x, rectTransform.localPosition.y);

            if (speed.y > 0 && rectTransform.localPosition.y > objectBound.y + origincalPos.y)
                rectTransform.localPosition = new(rectTransform.localPosition.x, -objectBound.y + rectTransform.localPosition.y);

            if (speed.y < 0 && rectTransform.localPosition.y < -objectBound.y - origincalPos.y)
                rectTransform.localPosition = new(rectTransform.localPosition.x, objectBound.y - rectTransform.localPosition.y);
        }

        private void CheckOutOfCamera()
        {
            if (speed.x > 0 && rectTransform.position.x > screenBound.x)
                rectTransform.position = new Vector3(-screenBound.x, rectTransform.position.y);

            if (speed.x < 0 && rectTransform.position.x < -screenBound.x)
                rectTransform.position = new Vector3(screenBound.x, rectTransform.position.y);

            if (speed.y > 0 && rectTransform.position.y > screenBound.x)
                rectTransform.position = new Vector3(rectTransform.position.x, -screenBound.y);

            if (speed.y < 0 && rectTransform.position.y < -screenBound.y)
                rectTransform.position = new Vector3(rectTransform.position.x, screenBound.y);
        }
    }
}