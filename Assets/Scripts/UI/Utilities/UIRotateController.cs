﻿using UnityEngine;

namespace Assets.Scripts.UI.Utilities
{
    public class UIRotateController : MonoBehaviour
    {
        [SerializeField] private Vector3 rotateSpeed;
        [SerializeField] private Vector3 rotateAngle;

        // Update is called once per frame
        void Update()
        {
            Vector3 rotation = new(rotateAngle.x * rotateSpeed.x, rotateAngle.y * rotateSpeed.y, rotateAngle.z * rotateSpeed.z);
            // Rotate the object based on rotation speed
            transform.Rotate(rotation * Time.deltaTime);
        }
    }
}