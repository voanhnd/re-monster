﻿using UnityEngine;

namespace Assets.Scripts.UI.Utilities
{
    public static class SpecialFloatToInt
    {
        public static int Convert(float value)
        {
            int intValue = Mathf.FloorToInt(value);
            if (value - intValue >= 0.5f)
            {
                intValue++;
            }
            return intValue;
        }
    }
}