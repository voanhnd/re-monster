﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Utilities
{
    public class ScrollrectCentering : MonoBehaviour
    {
        [SerializeField] private ScrollRect scrollRect;
        [SerializeField] private float animationTime = 0.1f;
        [SerializeField] private Ease animationEase;
        [SerializeField] private float shiftPosition = 0.25f;

        private void OnDisable()
        {
            scrollRect.DOKill();
        }

        public void CenterToSelectedItem(RectTransform selectedItem)
        {
            // Get the scroll rect's content transform
            RectTransform contentTransform = scrollRect.content;

            // Get the selected item's position within the content
            Vector2 selectedPosition = GetSelectedPosition(selectedItem);
            selectedPosition += scrollRect.viewport.rect.size * shiftPosition;
            // Calculate the normalized position within the scroll rect
            Vector2 normalizedPosition = CalculateNormalizedPosition(selectedPosition, contentTransform.rect.size, scrollRect.viewport.rect.size);

            // Apply the normalized position to center the scroll rect
            ApplyNormalizedPosition(normalizedPosition);
        }

        private Vector2 GetSelectedPosition(RectTransform selectedItem)
        {
            // Get the selected item's position relative to the content
            return selectedItem.anchoredPosition;
        }

        private Vector2 CalculateNormalizedPosition(Vector2 selectedPosition, Vector2 contentSize, Vector2 viewportSize)
        {
            // Calculate the position required to center the selected item
            Vector2 targetPosition = selectedPosition - (viewportSize - contentSize) * 0.5f;

            // Normalize the position within the range of [0, 1]
            Vector2 normalizedPosition = new(targetPosition.x / (contentSize.x - viewportSize.x),
                                                     targetPosition.y / (contentSize.y - viewportSize.y));

            // Clamp the normalized position to ensure it stays within the valid range
            normalizedPosition = new Vector2(Mathf.Clamp01(normalizedPosition.x), Mathf.Clamp01(normalizedPosition.y));

            return normalizedPosition;
        }

        private void ApplyNormalizedPosition(Vector2 normalizedPosition)
        {
            // Apply the normalized position to the scroll rect's normalized position
            scrollRect.DOKill();
            scrollRect.DOHorizontalNormalizedPos(normalizedPosition.x, animationTime).SetEase(animationEase);
            scrollRect.DOVerticalNormalizedPos(normalizedPosition.y, animationTime).SetEase(animationEase);
        }
    }
}