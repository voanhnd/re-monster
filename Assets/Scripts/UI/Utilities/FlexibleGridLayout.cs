﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Utilities
{
    public class FlexibleGridLayout : LayoutGroup
    {
        public enum FitType
        {
            Uniform,
            Width,
            Height,
            FixedRows,
            FixedColumns,
        }
        #region Private variables
        [SerializeField] private FitType fitType;
        [SerializeField] private Vector2 cellSize;
        [SerializeField] private Vector2 spacing;
        [SerializeField] private int rows;
        [SerializeField] private int columns;
        [SerializeField] private bool fitX;
        [SerializeField] private bool fitY;
        [SerializeField] private bool horizontalFit;
        [SerializeField] private bool verticalFit;

        #endregion

        #region Public variables
        public int Rows => rows;
        public int Columns => columns;
        #endregion

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            if (fitType == FitType.Uniform || fitType == FitType.Width || fitType == FitType.Height)
            {
                fitX = true;
                fitY = true;
                float sqrRt = Mathf.Abs(transform.childCount);
                rows = Mathf.CeilToInt(sqrRt);
                columns = Mathf.CeilToInt(sqrRt);

            }

            if (fitType == FitType.Width || fitType == FitType.FixedColumns)
            {
                rows = Mathf.CeilToInt(transform.childCount / (float)columns);
            }
            else if (fitType == FitType.Height || fitType == FitType.FixedRows)
            {
                columns = Mathf.CeilToInt(transform.childCount / (float)rows);
            }

            float parentWidth = rectTransform.rect.width;
            float parentHeight = rectTransform.rect.height;

            float cellWidth = parentWidth / columns - spacing.x / ((float)columns / (columns - 1)) - padding.left / (float)columns - padding.right / (float)columns;
            float cellHeight = parentHeight / rows - spacing.y / ((float)rows / (rows - 1)) - padding.top / (float)rows - padding.bottom / (float)rows;

            cellSize.x = fitX ? cellWidth : cellSize.x;
            cellSize.y = fitY ? cellHeight : cellSize.y;
            for (int i = 0; i < rectChildren.Count; i++)
            {
                int rowCount = i / columns;
                int columnCount = i % columns;

                RectTransform childItem = rectChildren[i];

                float xPos = cellSize.x * columnCount + spacing.x * columnCount + padding.left;
                float yPos = cellSize.y * rowCount + spacing.y * rowCount + padding.top;
                SetChildAlignment(ref xPos, ref yPos);

                SetChildAlongAxis(childItem, 0, xPos, cellSize.x);
                SetChildAlongAxis(childItem, 1, yPos, cellSize.y);
            }
            if (verticalFit)
            {
                float totalHeight = cellSize.y * rows + spacing.y * (rows - 1) + padding.top + padding.bottom;
                rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, totalHeight);
            }
            if (horizontalFit)
            {
                float totalWidth = cellSize.x * columns + spacing.x * (columns - 1) + padding.left + padding.right;
                rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, totalWidth);
            }
        }

        private void SetChildAlignment(ref float xPos, ref float yPos)
        {
            switch (m_ChildAlignment)
            {
                case TextAnchor.UpperLeft:
                default:
                    //No need to change xPos;
                    //No need to change yPos;
                    break;
                case TextAnchor.UpperCenter:
                    xPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.x + (spacing.x + padding.left + padding.left) - columns * (cellSize.x + spacing.x + padding.left)); //Center xPos
                                                                                                                                                                                           //No need to change yPos;
                    break;
                case TextAnchor.UpperRight:
                    xPos = -xPos + gameObject.GetComponent<RectTransform>().rect.size.x - cellSize.x; //Flip xPos to go bottom-up
                                                                                                      //No need to change yPos;
                    break;
                case TextAnchor.MiddleLeft:
                    //No need to change xPos;
                    yPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.y + (spacing.y + padding.top + padding.top) - rows * (cellSize.y + spacing.y + padding.top)); //Center yPos
                    break;
                case TextAnchor.MiddleCenter:
                    xPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.x + (spacing.x + padding.left + padding.left) - columns * (cellSize.x + spacing.x + padding.left)); //Center xPos
                    yPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.y + (spacing.y + padding.top + padding.top) - rows * (cellSize.y + spacing.y + padding.top)); //Center yPos
                    break;
                case TextAnchor.MiddleRight:
                    xPos = -xPos + gameObject.GetComponent<RectTransform>().rect.size.x - cellSize.x; //Flip xPos to go bottom-up
                    yPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.y + (spacing.y + padding.top + padding.top) - rows * (cellSize.y + spacing.y + padding.top)); //Center yPos
                    break;
                case TextAnchor.LowerLeft:
                    //No need to change xPos;
                    yPos = -yPos + gameObject.GetComponent<RectTransform>().rect.size.y - cellSize.y; //Flip yPos to go Right to Left
                    break;
                case TextAnchor.LowerCenter:
                    xPos += 0.5f * (gameObject.GetComponent<RectTransform>().rect.size.x + (spacing.x + padding.left + padding.left) - columns * (cellSize.x + spacing.x + padding.left)); //Center xPos
                    yPos = -yPos + gameObject.GetComponent<RectTransform>().rect.size.y - cellSize.y; //Flip yPos to go Right to Left
                    break;
                case TextAnchor.LowerRight:
                    xPos = -xPos + gameObject.GetComponent<RectTransform>().rect.size.x - cellSize.x; //Flip xPos to go bottom-up
                    yPos = -yPos + gameObject.GetComponent<RectTransform>().rect.size.y - cellSize.y; //Flip yPos to go Right to Left
                    break;
            }
        }

        public override void CalculateLayoutInputVertical()
        {
        }

        public override void SetLayoutHorizontal()
        {
        }

        public override void SetLayoutVertical()
        {
        }
    }
}