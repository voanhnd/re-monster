﻿using System.Collections.Generic;

public class ListScrambler<T>
{
    private List<T> originalList;
    private List<T> scrambledList;

    public ListScrambler(List<T> originalList)
    {
        this.originalList = originalList;
        this.scrambledList = new List<T>(originalList);
    }

    public void Scramble()
    {
        int n = scrambledList.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T temp = scrambledList[k];
            scrambledList[k] = scrambledList[n];
            scrambledList[n] = temp;
        }
    }

    public List<T> GetScrambledList()
    {
        return scrambledList;
    }
}
