﻿using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Utilities
{
    public class UIPopupPanel : UIMenuStruct
    {
        [Header("Components")]
        [SerializeField] private TextMeshProUGUI popupLabel;
        [SerializeField] private TextMeshProUGUI popupContent;
        [SerializeField] private Button accecptBtn;
        [SerializeField] private Button denyBtn;
        [SerializeField] private GameObject labelObject;

        private Action onAccecpt;
        private Action onDeny;

        private void Awake()
        {
            OnMenuOpen.AddListener(() =>
            {
                accecptBtn.onClick.AddListener(() =>
                {
                    this.onAccecpt?.Invoke();
                    CloseMenu();
                });

                denyBtn.onClick.AddListener(() =>
                {
                    this.onDeny?.Invoke();
                    CloseMenu();
                });

                labelObject.SetActive(true);
                popupContent.gameObject.SetActive(true);
                accecptBtn.interactable = true;
                denyBtn.interactable = true;
            });
        }

        public override void Start()
        {
            base.Start();
            labelObject.SetActive(false);
            popupContent.gameObject.SetActive(false);
            
            gameObject.SetActive(false);
        }

        public void Setup(string popupLabel, string popupContent, Action onAccecpt, Action onDeny)
        {
            this.popupLabel.text = popupLabel;
            this.popupContent.text = popupContent;

            //apply new action
            this.onAccecpt = onAccecpt;
            this.onDeny = onDeny;
        }

        public override void CloseMenu()
        {
            labelObject.SetActive(false);
            popupContent.gameObject.SetActive(false);
            accecptBtn.interactable = false;
            denyBtn.interactable = false;
            base.CloseMenu();
        }
    }
}