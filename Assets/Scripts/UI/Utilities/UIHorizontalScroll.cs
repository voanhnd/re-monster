﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.Utilities
{
    public class UIHorizontalScroll : MonoBehaviour
    {
        [Header("Container need to be set at top left")]
        [Header("Pivot set to 0, 1")]
        [SerializeField] private int pageCount;
        [SerializeField] private float pageWidth;
        [SerializeField] private float pageSpacing;
        [SerializeField] private RectTransform contentContainer;
        [Header("Animation values")]
        [SerializeField] private bool isAnimation = false;
        [SerializeField] private Ease animationEase;
        [SerializeField] private float animationTime;
        private int currentPageIndex = 0;
        private readonly List<Vector2> pagePositioning = new();

        private void Awake()
        {
            CalculatePagePositions();
        }

        private void CalculatePagePositions()
        {
            pagePositioning.Clear();

            for (int i = 0; i < pageCount; i++)
            {
                float pagePosition = -(pageWidth + pageSpacing) * i;
                pagePositioning.Add(new(pagePosition, contentContainer.anchoredPosition.y));
            }
        }

        public void ScrollToPage(int pageIndex)
        {
            pageIndex = Mathf.Clamp(pageIndex, 0, pageCount - 1);
            currentPageIndex = pageIndex;
            if (!isAnimation)
            {
                contentContainer.anchoredPosition = pagePositioning[currentPageIndex];
            }
            else
            {
                contentContainer.DOAnchorPos(pagePositioning[currentPageIndex], animationTime).SetEase(animationEase);
            }
        }
    }
}