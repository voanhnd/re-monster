﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Utilities
{
    public abstract class UIMenuStruct : MonoBehaviour
    {
        [SerializeField] private RectTransform controlContainer;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Button outlineBackgroundClose;
        [Header("Menu Events")]
        [SerializeField] private UnityEvent onMenuOpen;
        [SerializeField] private UnityEvent onMenuClose;
        [Header("Animations")]
        [SerializeField] private float openTime;
        [SerializeField] private float closeTime;
        [SerializeField] private Vector2 openSize;
        [SerializeField] private Vector2 closeSize;
        [SerializeField] private Ease openEase = Ease.InQuad;
        [SerializeField] private Ease closeEase = Ease.OutQuad;

        public Vector2 OpenSize => openSize;
        public Vector2 CloseSize => closeSize;
        public Ease OpenEase => openEase;
        public Ease CloseEase => closeEase;
        public RectTransform ControlContainer => controlContainer;
        public UnityEvent OnMenuOpen => onMenuOpen;
        public UnityEvent OnMenuClose => onMenuClose;

        public virtual void Start()
        {
            if (closeBtn != null)
            {
                closeBtn.onClick.AddListener(() =>
                {
                    CloseMenu();
                });
            }

            if (outlineBackgroundClose != null)
            {
                outlineBackgroundClose.onClick.AddListener(() =>
                {
                    CloseMenu();
                });
            }
        }

        public virtual void OpenMenu()
        {
            gameObject.SetActive(true);
            controlContainer.DOKill();
            controlContainer.sizeDelta = closeSize;
            controlContainer.DOSizeDelta(openSize, openTime).SetEase(openEase).OnComplete(() =>
            {
                onMenuOpen.Invoke();
            });
        }

        public virtual void CloseMenu()
        {
            controlContainer.DOKill();
            controlContainer.sizeDelta = openSize;
            controlContainer.DOSizeDelta(closeSize, closeTime).SetEase(closeEase).OnComplete(() =>
            {
                onMenuClose.Invoke();
                gameObject.SetActive(false);
            });
        }
    }
}