using UnityEngine;

namespace Assets.Scripts.UI.Utilites
{
    public class UIColorCopy : MonoBehaviour
    {
        [SerializeField] private CanvasRenderer copyRenderer;
        private CanvasRenderer thisRenderer;

        private void Awake()
        {
            thisRenderer = GetComponent<CanvasRenderer>();
        }

        private void Update()
        {
            thisRenderer.SetColor(copyRenderer.GetColor());
        }
    }
}