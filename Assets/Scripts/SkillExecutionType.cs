﻿using Assets.Scripts.Monster.Skill;

public static class SkillExecutionType
{
    public static string GetExecutionType(Skill.OccurrenceType executionType)
    {
        return executionType switch
        {
            Skill.OccurrenceType.指定_designation => "指定",
            Skill.OccurrenceType.自身_self => "自身",
            Skill.OccurrenceType.投射_projection => "投射",
            Skill.OccurrenceType.直線_straight => "直線",
            Skill.OccurrenceType.貫通_penetrating => "貫通",
            Skill.OccurrenceType.溜め_sink => "溜め",
            Skill.OccurrenceType.設置_installation => "設置",
            _ => string.Empty,
        };
    }
}

public static class SkillAttribute
{
    public static string GetSkillAttributeString(AttributeType skillAttribute)
    {
        return skillAttribute switch
        {
            AttributeType.なし_None => "None",
            AttributeType.打_Hit => "Blunt",
            AttributeType.斬_Cut => "Slash",
            AttributeType.突_Thrust => "Thrust",
            AttributeType.炎_Flame => "Fire",
            AttributeType.水_Water => "Water",
            AttributeType.風_Wind => "Wind",
            AttributeType.土_Soil => "Earth",
            _ => "None"
        };
    }

    public static string GetSkillGenTypeString(Skill.SkillGenType skillGenType)
    {
        return skillGenType switch
        {
            Skill.SkillGenType.STR => "STR",
            Skill.SkillGenType.INT => "INT",
            Skill.SkillGenType.回復_Heal => "Heal",
            Skill.SkillGenType.補助_Support => "Support",
            _ => "なし"
        }; ;
    }
}