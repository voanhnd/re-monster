using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebugPopup : MonoBehaviour
{
    static DebugPopup instance;

    public TextMeshProUGUI textMeshProUGUI;

    public List<string> printLine = new List<string>();

    public Color teamColor;
    public Color enemyColor;

    public GameTeam gameTeam;

    public GameObject popup;

    bool isTraining = false;

    bool isAttack = false;

    private void Awake()
    {
        instance = this;
    }
    public DebugPopup Instance()
    {
        return instance;
    }

    private void Start()
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.unityLogger.logEnabled = true;
        popup.gameObject.SetActive(false);
#else
        Debug.unityLogger.logEnabled = false;
        gameObject.SetActive(false);
#endif
    }

    public void Active()
    {
        if(popup.activeSelf == false)
        {
            popup.SetActive(true);
        }
        else
        {
            popup.SetActive(false);
        }
    }

    public static void Log(string messageStr)
    {
        if (instance.isAttack == false) return;

        if (instance.gameTeam == GameTeam.Blue)
        {
            instance.textMeshProUGUI.text = instance.textMeshProUGUI.text + "<color=#FFC700>" + messageStr + @"</color><br>";
        }
        else
        {
            instance.textMeshProUGUI.text = instance.textMeshProUGUI.text + "<color=#FF6162>" + messageStr + @"</color><br>";
        }
    }

    public static void LogRaise(string messageStr)
    {
        if (instance.isTraining == false) return;

        instance.textMeshProUGUI.text = instance.textMeshProUGUI.text + "<color=#FFC700>" + messageStr + @"</color><br>";
    }

    public static void InTraining(bool m_bool)
    {
        instance.isTraining = m_bool;
    }

    public static void InAttack(bool m_bool)
    {
        instance.isAttack = m_bool;
    }

    public static void RegLog(GameTeam regTeam)
    {
        instance.gameTeam = regTeam;
    }

}
