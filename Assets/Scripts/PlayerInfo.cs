using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class PlayerInfo
{
    #region Private variables

    [SerializeField] private List<PlayerMonsterInfo> playerMonsterInfos = new();

    [SerializeField] private List<PlayerFarmInfo> playerFarmInfos = new();

    private Dictionary<string, PlayerMonsterInfo> playerMonsterDictionary = new();

    private Dictionary<string, PlayerFarmInfo> playerFarmDictionary = new();
    #endregion

    #region Public variables
    public string playerName;

    public float bit = 0;

    public float oas = 0;

    public int itemRank2Ticket = 0;

    public int itemRank3Ticket = 0;
    public static Action OnBitValueChanged { get; set; }
    public List<PlayerMonsterInfo> PlayerMonsterInfos => playerMonsterInfos;
    public List<PlayerFarmInfo> PlayerFarmInfos => playerFarmInfos;
    #endregion

    public void AddGold(float cGold) => ChangeGold(+cGold);

    public void SubtractBiT(float cGold) => ChangeGold(-cGold);

    private void ChangeGold(float cbit)
    {
        float temp = bit + cbit;

        if (temp > 0)
        {
            bit = temp;
        }
        else if (temp < 0)
        {
            bit = 0;
        }

        Debug.Log("Current bit = " + (temp - cbit));
        Debug.Log("Change bit = " + cbit);
        Debug.Log("Updated bit = " + bit);

        PlayerStats.Instance.Save();
        OnBitValueChanged?.Invoke();
    }

    #region Monster
    public void SetPlayerMonsterInfos(List<PlayerMonsterInfo> tempList)
    {
        playerMonsterInfos.AddRange(tempList);

        CreateMonsterDictionary();
    }

    private void CreateMonsterDictionary()
    {
        if (playerMonsterInfos.Count > 0)
        {
            playerMonsterDictionary.Clear();

            for (int i = 0; i < playerMonsterInfos.Count; i++)
            {
                playerMonsterDictionary.Add(playerMonsterInfos[i].monsterID, playerMonsterInfos[i]);
            }
        }
    }

    public PlayerMonsterInfo GetPlayerMonsterInfoByID(string idStr)
    {

        if (!playerMonsterDictionary.TryGetValue(idStr, out PlayerMonsterInfo playerMonsterInfo))
        {
            // the key isn't in the dictionary.
            return playerMonsterInfo; // or whatever you want to do
        }

        return playerMonsterInfo;
    }

    public void AddMonster(PlayerMonsterInfo newMonster)
    {
        if (playerMonsterDictionary.TryAdd(newMonster.monsterID, newMonster))
        {
            playerMonsterInfos.Add(newMonster);
        }
        else
        {
            Debug.Log($"monster {newMonster.monsterID} already existed");
        }
    }
    #endregion

    #region Farm
    public void SetPlayerFarmInfos(List<PlayerFarmInfo> tempList)
    {
        playerFarmInfos = tempList;

        CreateFarmDictionary();
    }

    private void CreateFarmDictionary()
    {
        if (playerFarmInfos.Count > 0)
        {
            playerFarmDictionary.Clear();

            for (int i = 0; i < playerFarmInfos.Count; i++)
            {
                playerFarmDictionary.Add(playerFarmInfos[i].farmId, playerFarmInfos[i]);
            }
        }
    }

    public PlayerFarmInfo GetPlayerFarmInfoByID(string idStr)
    {

        //Debug.Log("farm id = " + idStr);

        if (!playerFarmDictionary.TryGetValue(idStr, out PlayerFarmInfo playerFarmInfo))
        {
            //Debug.Log("the key farm isn't in the dictionary.");

            //Debug.Log("playerFarmDictionary count = " + playerFarmDictionary.Count);

            //Debug.Log("playerMonsterDictionary count = " + playerMonsterDictionary.Count);

            // the key isn't in the dictionary.
            return playerFarmInfo; // or whatever you want to do
        }

        return playerFarmInfo;
    }

    internal void AddFarm(PlayerFarmInfo newFarm)
    {
        if (playerFarmDictionary.TryAdd(newFarm.farmId, newFarm))
        {
        playerFarmInfos.Add(newFarm);
        }
        else
        {
            Debug.Log($"already existed {newFarm.farmId}");
        }
    }
    #endregion

    #region Serializeables
    [Serializable]
    public class PlayerMonsterInfo
    {
        #region public variables
        public string monsterID = "";

        public string monsterName = "";

        public Monster.MonsterRace monsterRace = new();

        public RankTypeEnums rankType = RankTypeEnums.f;

        public PersonalityTrait.PersonalityTraitType personalityTrait = PersonalityTrait.PersonalityTraitType.None;

        public InnateTrait.InnateTraitType innateTrait = InnateTrait.InnateTraitType.None;

        public List<AcquiredTrait.PlayerInfoAcquiredTrait> acquiredTraits = new List<AcquiredTrait.PlayerInfoAcquiredTrait>();

        public string nftID = "";

        /// <summary>
        ///This is basic parameter trained not total
        ///Total = trained parameters + default parameters
        /// </summary>
        public BasicParameters basicParameters = new();

        public GrowthParameters growthParameters = new();

        public Food.FoodType lastFoodType = Food.FoodType.Empty;

        public MonsterAction lastMonsterAction = new();

        public string usingFarmID = "";

        public List<string> SelectedBattleSkillsId => selectedBattleSkills ??= new();
        public List<Monster.MonsterSkillInfo> MonsterSkillInfos => monsterSkillInfos;
        #endregion

        #region private variables
        [SerializeField] private List<Monster.MonsterSkillInfo> monsterSkillInfos = new();
        [SerializeField] private List<string> selectedBattleSkills = new();
        private Dictionary<string, Monster.MonsterSkillInfo> monsterSkillDictionary = new();
        #endregion

        public void SetPlayerMonsterSkillInfos(List<Monster.MonsterSkillInfo> skillList)
        {
            monsterSkillInfos = skillList;

            CreateMonsterDictionary();
        }

        private void CreateMonsterDictionary()
        {
            if (monsterSkillInfos.Count > 0)
            {
                monsterSkillDictionary.Clear();

                //for (int i = 0; i < monsterSkillInfos.Count; i++)
                //{
                //    monsterSkillDictionary.Add(monsterSkillInfos[i].skillType, monsterSkillInfos[i]);
                //}
                monsterSkillDictionary = monsterSkillInfos.ToDictionary(data => data.skillID);
            }
        }

        public Monster.MonsterSkillInfo GetPlayerMonsterSkillInfoByID(string skillID)
        {
            if (!monsterSkillDictionary.TryGetValue(skillID, out Monster.MonsterSkillInfo skillInfo))
            {
                // the key isn't in the dictionary.
                return skillInfo; // or whatever you want to do
            }

            return skillInfo;
        }

        public void SelectBattleSkill(string skillId)
        {
            if (!selectedBattleSkills.Exists(x => x == skillId))
            {
                selectedBattleSkills.Add(skillId);
            }
        }

        internal void SelectBattleSkill(List<string> monsterBattleSkills)
        {
            selectedBattleSkills.Clear();

            selectedBattleSkills.AddRange(monsterBattleSkills);
        }
    }


    [Serializable]
    public class PlayerFarmInfo
    {
        public Farm.FarmType usingFarmType = Farm.FarmType.Empty;

        public string farmId = "";

        public string nftID = "";

        public FarmTrait.FarmTraitType farmTraitType = FarmTrait.FarmTraitType.none;

        [SerializeField]
        private string monsterId = string.Empty;

        public string MonsterId => monsterId;

        public void SetMonsterId(string monsterId) => this.monsterId = monsterId;

        public PlayerFarmInfo Clone()
        {
            PlayerFarmInfo playerFarmInfo = new PlayerFarmInfo();

            playerFarmInfo.usingFarmType = usingFarmType;

            playerFarmInfo.farmId = farmId;

            playerFarmInfo.nftID = nftID;

            playerFarmInfo.farmTraitType = farmTraitType;

            playerFarmInfo.monsterId = monsterId;

            return playerFarmInfo;
        }
    }
    #endregion
}
