using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Trait
{
    public class FarmTrait : MonoBehaviour
    {
        public enum FarmTraitType 
        {
            none,
            hp_plus_f,
            str_plus_f,
            int_plus_f,
            dex_plus_f,
            agi_plus_f,
            vit_plus_f,
            hit_plus_f,
            avd_plus_f,
            cri_plus_f,
            res_plus_f,
            mnd_plus_f,
            st_plus_f,
            spd_plus_f,
            fire_res_plus_f,
            water_res_plus_f,
            wind_res_plus_f,
            earth_res_plus_f,
            slash_res_plus_f,
            blunt_res_plus_f,
            thrust_res_plus_f
        }

        [System.Serializable]
        public class FarmTraitInfo
        {
            public FarmTraitType farmTraitType = FarmTraitType.none;

            public float health = 0;

            public float strenght = 0f;

            public float intelligent = 0f;

            public float dexterity = 0f;

            public float agility = 0f;

            public float vitality = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float accuracy = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float evansion = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float critical = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float physical_Debuff_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float spirit_Debuff_Resistance = 0f;

            public float battle_Start_Stamina = 0f;

            public float speed = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float fire_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float water_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float wind_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float earth_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float slash_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float blunt_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float thrust_Resistance = 0f;
        }

        public static FarmTraitInfo GetFarmTraitInfo(FarmTraitType farmTraitType)
        {
            FarmTraitInfo farmTraitInfo = new FarmTraitInfo();
            farmTraitInfo.farmTraitType = FarmTraitType.none;

            return farmTraitInfo;

            /*switch (farmTraitType)
            {
                case FarmTraitType.hp_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.hp_plus_f;

                        farmTraitInfo.health = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.str_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.str_plus_f;

                        farmTraitInfo.strenght = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.int_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.int_plus_f;

                        farmTraitInfo.intelligent = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.dex_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.dex_plus_f;

                        farmTraitInfo.dexterity = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.agi_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.agi_plus_f;

                        farmTraitInfo.agility = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.vit_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.vit_plus_f;

                        farmTraitInfo.vitality = 50f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.hit_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.hit_plus_f;

                        farmTraitInfo.accuracy = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.avd_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.avd_plus_f;

                        farmTraitInfo.evansion = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.cri_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.cri_plus_f;

                        farmTraitInfo.critical = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.res_plus_f;

                        farmTraitInfo.physical_Debuff_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.mnd_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.mnd_plus_f;

                        farmTraitInfo.spirit_Debuff_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.st_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.st_plus_f;

                        farmTraitInfo.battle_Start_Stamina = 5f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.spd_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.spd_plus_f;

                        farmTraitInfo.speed = 5f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.fire_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.fire_res_plus_f;

                        farmTraitInfo.fire_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.water_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.water_res_plus_f;

                        farmTraitInfo.water_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.wind_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.wind_res_plus_f;

                        farmTraitInfo.wind_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.earth_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.earth_res_plus_f;

                        farmTraitInfo.earth_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.slash_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.slash_res_plus_f;

                        farmTraitInfo.slash_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.blunt_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.blunt_res_plus_f;

                        farmTraitInfo.blunt_Resistance = 0.05f;

                        return farmTraitInfo;
                    }

                case FarmTraitType.thrust_res_plus_f:
                    {
                        farmTraitInfo = new FarmTraitInfo();

                        farmTraitInfo.farmTraitType = FarmTraitType.thrust_res_plus_f;

                        farmTraitInfo.thrust_Resistance = 0.05f;

                        return farmTraitInfo;
                    }
            }

            return farmTraitInfo;*/
        }
    }
}