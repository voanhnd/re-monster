using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster
{
    [CreateAssetMenu(fileName = "MonsterDataManagerScriptObj", menuName = "ScriptableObjects/Monster/MonsterDataManagerScriptObj", order = 1)]
    public class MonsterDataManagerScriptObj : ScriptableObject
    {
        public Dictionary<string, MonsterDataScriptObj> monsterDataDictionary = new();

        [SerializeField]
        List<MonsterDataScriptObj> monsterDataList = new();

        public MonsterDataScriptObj GetMonsterDataObj(string monsterID)
        {
            if (!monsterDataDictionary.TryGetValue(monsterID, out MonsterDataScriptObj monsterDataObj))
            {
                // the key isn't in the dictionary.
                return monsterDataObj; // or whatever you want to do
            }

            //Debug.Log("Can not get monster with main race = " + monsterRace.mainRace.ToString() + " sub race = " + monsterRace.subRace);

            return monsterDataObj;
        }

        private void OnValidate()
        {
            if (Application.isEditor && Application.isPlaying == false)
            {
                //Debug.Log("OnValidate");

                CreateDictionary();
            }
        }

        public void CreateDictionary()
        {
            if (monsterDataList.Count > 0)
            {
                monsterDataDictionary.Clear();

                for (int i = 0; i < monsterDataList.Count; i++)
                {
                    monsterDataDictionary.Add(monsterDataList[i].Monster.monsterID, monsterDataList[i]);
                }
            }
        }

        public PlayerInfo.PlayerMonsterInfo CreatPlayerMonsterInfo(string monsterID)
        {
            PlayerInfo.PlayerMonsterInfo newMonster = new();
            MonsterDataScriptObj monsterScriptObj = GetMonsterDataObj(monsterID); ;
            newMonster.monsterID = monsterID;
            newMonster.monsterName = monsterScriptObj.Monster.monsterName;
            newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
            //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
            newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
            newMonster.usingFarmID = string.Empty;
            newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);

            return newMonster;
        }

        public List<MonsterDataScriptObj> GetMonsterList()
        {
            return monsterDataList;
        }
    }
}