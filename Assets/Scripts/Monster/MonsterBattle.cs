﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.Monster.Trait;
using UnityEngine;

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class MonsterBattle
    {

        #region Basic Parameters

        /// <summary>
        /// <para> return value 100% = 1f </para>
        /// </summary>
        /// <param name="parameterValue"></param>
        /// <returns></returns>
        public static float Get_Parameters_Rank_Correction(float parameterValue)
        {
            RankTypeEnums rankType = Monster.Get_Monster_Parameter_Rank_By_Value(parameterValue);

            switch (rankType)
            {
                case RankTypeEnums.s:
                    {
                        return 1.3f;
                    }

                case RankTypeEnums.a:
                    {
                        return 1.25f;
                    }

                case RankTypeEnums.b:
                    {
                        return 1.20f;
                    }

                case RankTypeEnums.c:
                    {
                        return 1.15f;
                    }

                case RankTypeEnums.d:
                    {
                        return 1.1f;
                    }

                case RankTypeEnums.e:
                    {
                        return 1.05f;
                    }

                case RankTypeEnums.f:
                    {
                        return 1f;
                    }
            }

            return 0f;
        }

        #endregion Basic Parameters

        #region Stamina

        public static float GetStartMonsterStamina()
        {
            return 50f;
        }

        /// <summary>
        /// <para> vector2.x = min </para>
        /// <para> vector2.y = max </para>
        /// </summary>
        public static Vector2 Get_Stamina_Min_Max()
        {
            return new Vector2(0f, 100f);
        }

        /// <summary>
        /// <para> Move 1 square = 5 </para>
        /// <para> move 2 squares = 12 </para>
        /// <para> move 3 squares = 21 </para>
        /// <para> move 4 squares = 32 </para>
        /// <para> move 5 squares = 45 </para>
        /// </summary>
        public static float Get_Move_Stamina_Cost(int moveDistance)
        {
            switch (moveDistance)
            {
                case 1:
                    {
                        return 5;
                    }

                case 2:
                    {
                        return 12;
                    }

                case 3:
                    {
                        return 21;
                    }

                case 4:
                    {
                        return 32;
                    }

                case 5:
                    {
                        return 45;
                    }
            }

            return 0;
        }

        public static bool Check_Is_Stamina_Enough(float staminaCost, float currentStamina)
        {
            if (staminaCost <= currentStamina)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Stamina damage formula </para>
        /// <para> Skill stamina damage or recovery amount: aST (Skill stamina damage or heal) </para>
        /// <para> Distance Correction: aD (Distance) </para>
        /// <para> Remaining stamina correction: aSC (Stamina correction) </para>
        /// <para> Final Stamina Damage: R (Result) </para>
        /// <para> R = aST × (aD + aSC) </para>
        /// </summary>
        /// <param name="stamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Stamina_Damage(float stamina, float distanceCorrection, float remainingStaminaCorrection)
        {
            float result = stamina * (distanceCorrection + remainingStaminaCorrection);

            DebugPopup.Log("Stamina damage");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Skill stamina damage or recovery amount: aST = " + stamina);
            DebugPopup.Log("Distance Correction: aD = " + distanceCorrection);
            DebugPopup.Log("Remaining stamina correction: aSRC = " + remainingStaminaCorrection);
            DebugPopup.Log("Final Stamina Damage: R = " + result);

            return result;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Stamina Consumption Calculation Formula </para>
        /// <para> Consumed stamina : ST (Stamina) </para>
        /// <para> Trait Correction: TC (Trait correction) </para>
        /// <para> Stamina Consumption: R (Result) </para>
        /// <para> R = ST × (1 + TC) </para>
        /// </summary>
        /// <param name="stamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Stamina_Consumption(float stamina, float traitCorrection)
        {
            float result = stamina * (1f + traitCorrection);

            DebugPopup.Log("//////////");
            DebugPopup.Log($"Stamina Consumption Calculation Formula");
            DebugPopup.Log($"Consumed stamina : ST = {stamina}");
            DebugPopup.Log($"Trait Correction: TC = {traitCorrection}");
            DebugPopup.Log($"Stamina Consumption: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Stamina recovery amount calculation formula for each turn </para>
        /// <para> Stamina recovery value : SR (Stamina recovery) </para>
        /// <para> Trait Correction: TC (Trait correction) </para>
        /// <para> Buff state change correction: bSC </para>
        /// <para> Debuff state change correction: dSC </para>
        /// <para> [Buffer] </para>
        /// <para> Trait Correction: bTC (Trait correction) </para>
        /// <para> [Debuffer] </para>
        /// <para> Trait Correction: dTC (Trait correction) </para>
        /// <para> Stamina recovery amount: R (Result) </para>
        /// <para> R = SR × ( 1 + TC + bSC × ( 1 + bTC ) - dSC × ( 1 + dTC ) ) </para>
        /// </summary>
        /// <param name="stamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Stamina_Recovery_Turn(float stamina, float traitCorrection, float buffStateCorrection, float debuffStateCorrection, float buffTraitCorrection, float debuffTraitCorrection)
        {
            float result = stamina * (1f + traitCorrection + buffStateCorrection * (1f + buffTraitCorrection) - debuffStateCorrection * (1f + debuffTraitCorrection));

            DebugPopup.Log("//////////");
            DebugPopup.Log("Stamina recovery amount calculation formula for each turn");
            DebugPopup.Log($"Stamina recovery value : SR = {stamina}");
            DebugPopup.Log($"Trait Correction: TC = {traitCorrection}");
            DebugPopup.Log($"Buff state change correction: bSC = {buffStateCorrection}");
            DebugPopup.Log($"Debuff state change correction: dSC = {debuffStateCorrection}");
            DebugPopup.Log($"Buff Trait Correction: bTC = {buffTraitCorrection}");
            DebugPopup.Log($"Debuff Trait Correction: dTC = {debuffTraitCorrection}");
            DebugPopup.Log($"Stamina recovery amount: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Stamina recovery amount calculation formula by skill </para>
        /// <para> [Healer] </para>
        /// <para> Skill stamina recovery value : SR (Stamina recovery) </para>
        /// <para> Trait Correction: hTC (Trait correction) </para>
        /// <para> [Target] </para>
        /// <para> Stamina recovery value : SR (Stamina recovery) </para>
        /// <para> Buff state change correction: bSC (State correction) </para>
        /// <para> Debuff state change correction: dSC (State correction) </para>
        /// <para> Stamina recovery amount: R (Result)</para>
        /// <para> R = SR × ( 1 + hTC + bSC - dSC ) </para>
        /// </summary>
        /// <param name="skillHealStamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Stamina_Recovery_Skill
            (
            float skillHealStamina, float h_traitCorrection,
            float t_buffStateCorrection, float t_debuffStateCorrection)
        {
            float result = skillHealStamina * (1f + h_traitCorrection + t_buffStateCorrection - t_debuffStateCorrection);

            DebugPopup.Log("//////////");
            DebugPopup.Log("Stamina recovery amount calculation formula by skill");
            DebugPopup.Log("[Healer]");
            DebugPopup.Log($"Skill stamina recovery value : hSR = {skillHealStamina}");
            DebugPopup.Log($"Trait Correction: hTC = {h_traitCorrection}");
            DebugPopup.Log("[Target]");
            DebugPopup.Log($"Buff state change correction: bSC = {t_buffStateCorrection}");
            DebugPopup.Log($"Debuff state change correction: dSC = {t_debuffStateCorrection}");
            DebugPopup.Log($"Stamina recovery amount: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Performance improvement formula </para>
        /// <para> Remaining Stamina: RST (Remaining Stamina) </para>
        /// <para> Consumed stamina : ST (Stamina) </para>
        /// <para> Increased value: R (Result) </para>
        /// <para> R = MIN(10%, RST/ST/30) </para>
        /// </summary>
        /// <param name="stamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_Stamina_Performance_Improvement(float remainingStamina, float consumedStamina)
        {
            float result = Mathf.Min(0.1f, (remainingStamina / consumedStamina - 1) / 20f);

            DebugPopup.Log("//////////");
            DebugPopup.Log("Stamina Performance Improvement");
            DebugPopup.Log($"Remaining Stamina: RST = {remainingStamina}");
            DebugPopup.Log($"Consumed stamina : ST = {consumedStamina}");
            DebugPopup.Log($"Increased value: R = {result}");

            return result;
        }

        #endregion

        #region HP Damage

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Damage formula </para>
        /// <para> [Attacker] </para>
        /// <para> Skill Damage Value: aSDMG (Skill Damage) </para>
        /// <para> IDistance Correction: aD (Distance) </para>
        /// <para> Correction by state change: aSC (State correction) </para>
        /// <para> Damage correction by traits: aDC (Damage correction) </para>
        /// <para> element correction by traits: aAC (element Correction) </para>
        /// <para> Random number correction by traits: aRC (Random number correction) </para>
        /// <para> Remaining stamina correction: aRSC (Stamina correction) </para>
        /// <para> Critical damage correction by traits: aCC (Critical correction) </para>
        /// <para> Attack Ability: aATK (Attack) </para>
        /// <para> Rank Correction : aR </para>
        /// <para> [Defender] </para>
        /// <para> Correction due to state change: dSC (State correction) </para>
        /// <para> element endurance: dAE (element endurance) </para>
        /// <para> Damage correction by traits: dDC (Damege correction) </para>
        /// <para> element correction: dAC (element correction) </para>
        /// <para> Defensive Ability: dVIT </para>
        /// <para> Rank Correction : dR </para>
        /// <para> +++++++++++++++++++++++ </para>
        /// <para> Basic damage multiplier: BDMG (Basic damage) </para>
        /// <para> Final damage correction: FDC (Final damage correction) </para>
        /// <para> Final damage: R (Result) </para>
        /// <para> BDMG = MIN(3, aATK / dVIT) </para>
        /// <para> FDC = aD + aSC + aDC + aAC + aRSC - dSC - dAE - dDC </para>
        /// <para> R = aSDMG × BDMG × FDC × ( 0.95 - aRC ) ~ ( 1.05 + aRC ) </para>
        /// <para> IF Critical R = R × (1.25 + aCC) </para>
        /// </summary>
        /// <param name="stamina"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static float Get_HP_Damage
            (
            float a_skillDamage, float a_distanceCorrection, float a_StateCorrection, float a_DamageCorrection, float a_elementCorrectionByTrait, float a_RandomNumberCorrection, float a_RemainingStaminaCorrection, float a_CriticalCorrection, float a_AttackAbility,
            float d_StateCorrection, float d_AttributeEndurance, float d_elementCorrectionByTrait, float d_DamageCorrection, float d_VitalityAbility,
            bool isCritical
            )
        {
            float a_AttackAbilityCorrection = Get_Parameters_Rank_Correction(a_AttackAbility);
            float d_VitalityAbilityCorrection = Get_Parameters_Rank_Correction(d_VitalityAbility);

            float basicDamage = Mathf.Max(0.3f, 1f + (a_AttackAbility * a_AttackAbilityCorrection - d_VitalityAbility * d_VitalityAbilityCorrection) / 1000f);

            Debug.Log("basicDamage = " + basicDamage);

            float finalDamageCorrection = a_distanceCorrection + a_StateCorrection + a_DamageCorrection + a_elementCorrectionByTrait + a_RemainingStaminaCorrection - d_StateCorrection - d_AttributeEndurance - d_DamageCorrection - d_elementCorrectionByTrait;

            Debug.Log("final Damage Correction = " + finalDamageCorrection);

            float ranValue = UnityEngine.Random.Range(0.95f - a_RandomNumberCorrection, 1.05f + a_RandomNumberCorrection);

            float result = a_skillDamage * basicDamage * finalDamageCorrection * ranValue;

            if (isCritical)
            {
                result = result * (1.25f + a_CriticalCorrection);
            }

            DebugPopup.Log("HP Damage");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Attacker");
            DebugPopup.Log("Skill Damage Value: aSDMG = " + a_skillDamage);
            DebugPopup.Log("Distance Correction: aD = " + a_distanceCorrection);
            DebugPopup.Log("Correction by state change = " + a_StateCorrection);
            DebugPopup.Log("Damage correction by traits: aDC = " + a_DamageCorrection);
            DebugPopup.Log("element correction by traits: aAC = " + a_elementCorrectionByTrait);
            DebugPopup.Log("Random number correction by traits: aRC = " + a_RandomNumberCorrection);
            DebugPopup.Log("Remaining stamina correction: aSRC = " + a_RemainingStaminaCorrection);
            DebugPopup.Log("Critical damage correction by traits: aCC = " + a_CriticalCorrection);
            DebugPopup.Log("Attack Ability: aATK = " + a_AttackAbility);
            DebugPopup.Log("Rank Correction : aR = " + a_AttackAbilityCorrection);

            DebugPopup.Log("//////////");
            DebugPopup.Log("Defender");
            DebugPopup.Log("Correction by state change: dSC = " + d_StateCorrection);
            DebugPopup.Log("element endurance: dAE = " + d_AttributeEndurance);
            DebugPopup.Log("Damage correction by traits: dDC = " + d_DamageCorrection);
            DebugPopup.Log("element correction: dAC = " + d_elementCorrectionByTrait);
            DebugPopup.Log("Defensive Ability: dVIT = " + d_VitalityAbility);
            DebugPopup.Log("Rank Correction : dR = " + d_VitalityAbilityCorrection);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Basic damage multiplier: BDMG = " + basicDamage);
            DebugPopup.Log("Final damage correction: FDC = " + finalDamageCorrection);
            DebugPopup.Log("Final damage: R = " + result);
            DebugPopup.Log("End HP Damage");
            DebugPopup.Log("//////////////////////////////");

            return result;
        }

        public static float Get_Caculate_HP_Damage
            (
            InGameBuffDetail atk_inGameBuffDetail, InGameBuffDetail def_inGameBuffDetail,
            InGameDeBuffDetail atk_inGameDeBuffDetail, InGameDeBuffDetail def_inGameDeBuffDetail,
            InnateTrait.InnateTraitInfo atk_innateTraitInfo, InnateTrait.InnateTraitInfo def_innateTraitInfo,
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo, AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo,
            FarmTrait.FarmTraitInfo atk_farmTraitInfo, FarmTrait.FarmTraitInfo def_farmTraitInfo,
            MonsterDataScriptObj atk_monsterDataScriptObj, MonsterDataScriptObj def_monsterDataScriptObj,
            Skill.Skill.SkillDetail atk_skillDetail, Skill.Skill.SkillGenType a_skillGenType, float a_DistanceCorrection, float a_RemainingStaminaCorrection, float a_AttackAbility,
            float d_VitalityAbility,
            bool isCritical
            )
        {
            DebugPopup.Log("Attacker Attribute Type = " + atk_skillDetail.attributeType.ToString());

            float a_stateCorrection = atk_inGameBuffDetail.atk_Damage_Correction + atk_inGameDeBuffDetail.atk_Damage_Correction;

            float d_stateCorrection = def_inGameBuffDetail.def_Damage_Correction + def_inGameDeBuffDetail.def_Damage_Correction;

            float a_DamageCorrectionByTrait = Skill.Skill.GetDamageCorrectionByTrait(atk_innateTraitInfo, a_skillGenType, atk_skillDetail.attributeType);

            float d_DamageCorrectionByTrait = Skill.Skill.GetDamageCorrectionByTrait(def_innateTraitInfo, a_skillGenType, atk_skillDetail.attributeType);

            float a_ElementCorrectionByTrait = Skill.Skill.ElementCorrectionByTrait(atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo, atk_skillDetail.attributeType);

            float d_ElementCorrectionByTrait = Skill.Skill.ElementCorrectionByTrait(def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo, atk_skillDetail.attributeType);

            float d_ElementEndurance = Skill.Skill.GetAttributeEndurance(def_monsterDataScriptObj, atk_skillDetail.attributeType);

            float a_CriticalCorrection = atk_innateTraitInfo.Critical_Damage_Correction;

            float a_RandomNumberCorrection = atk_innateTraitInfo.Random_Number;

            float a_skillDamage = atk_skillDetail.hp_dmg;
            Debug.Log("a_skillDamage = " + a_skillDamage);

            float hp_Damage = MonsterBattle.Get_HP_Damage
                (
                a_skillDamage, a_DistanceCorrection, a_stateCorrection, a_DamageCorrectionByTrait, a_ElementCorrectionByTrait, a_RandomNumberCorrection, a_RemainingStaminaCorrection, a_CriticalCorrection, a_AttackAbility,
                d_stateCorrection, d_ElementEndurance, d_ElementCorrectionByTrait, d_DamageCorrectionByTrait, d_VitalityAbility, isCritical
                );

            return hp_Damage;
        }

        #endregion

        #region Accuracy

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Damage formula </para>
        /// <para> [Attacker] </para>
        /// <para> Skill hit rate: aHIT (Skill hit) </para>
        /// <para> IDistance Correction: aD (Distance) </para>
        /// <para> Correction by state change: aSC (State correction) </para>
        /// <para> Correction by characteristics: aHC (Hit correction) </para>
        /// <para> Remaining stamina correction: aRSC (Stamina correction) </para>
        /// <para> Accuracy: aDEX </para>
        /// <para> [Defender] </para>
        /// <para> Correction due to state change: dSC (State correction) </para>
        /// <para> Correction by characteristics: dHC (Hit correction) </para>
        /// <para> Evasion Ability: dAGI </para>
        /// <para> ++++++++++++++++++++++ </para>
        /// <para> Basic hit multiplier: BHIT (Basic hit) </para>
        /// <para> Final hit correction: FDH </para>
        /// <para> Final Hit Rate: R (Result) </para>
        /// <para> BHIT = MIN( 1.7, aDEX / dAGI ) </para>
        /// <para> FDH = aD + aSC + aHC + aRSC - dSC - dHC </para>
        /// <para> R = aHIT × BHIT × FDH </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Accuracy
            (
            float a_SkillHitRate, float a_DistanceCorrection, float a_StateCorrection, float a_HitCorrection, float a_RemainStaminaCorrection, float a_dex,
            float d_StateCorrection, float d_HitCorrection, float d_agi
            )
        {
            //Debug.Log("Get_Accuracy");

            float basicHitMultiplier = Mathf.Min(a_dex / d_agi, 1.7f);

            //Debug.Log("basicHitMultiplier = " + basicHitMultiplier);

            float finalHitCorrection = a_DistanceCorrection + a_StateCorrection + a_HitCorrection + a_RemainStaminaCorrection - d_StateCorrection - d_HitCorrection;

            //Debug.Log("finalHitCorrection = " + finalHitCorrection);

            float result = a_SkillHitRate * basicHitMultiplier * finalHitCorrection;

            //Debug.Log("Get_Accuracy result = " + result);

            DebugPopup.Log("Accuracy");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Attacker");
            DebugPopup.Log("Skill hit rate: aHIT = " + a_SkillHitRate);
            DebugPopup.Log("Distance Correction: aD = " + a_DistanceCorrection);
            DebugPopup.Log("Correction by state change: aSC = " + a_StateCorrection);
            DebugPopup.Log("Correction by characteristics: aHC = " + a_HitCorrection);
            DebugPopup.Log("Remaining stamina correction: aSRC = " + a_RemainStaminaCorrection);
            DebugPopup.Log("Accuracy: aDEX = " + a_dex);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Defender");
            DebugPopup.Log("Correction due to state change: dSC = " + d_StateCorrection);
            DebugPopup.Log("Correction by characteristics: dHC = " + d_HitCorrection);
            DebugPopup.Log("Evasion Ability: dAGI = " + d_agi);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Basic hit multiplier: BHIT = " + basicHitMultiplier);
            DebugPopup.Log("Final hit correction: FDH = " + finalHitCorrection);
            DebugPopup.Log("Final Hit Rate: R = " + result);
            DebugPopup.Log("End Accuracy");
            DebugPopup.Log("//////////////////////////////");

            return result;
        }

        public static bool Get_Accuracy_IsHit
            (
            ParameterInGame atk_parameterInGame, ParameterInGame def_parameterInGame,
            InGameBuffDetail atk_inGameBuffDetail, InGameBuffDetail def_inGameBuffDetail,
            InGameDeBuffDetail atk_inGameDeBuffDetail, InGameDeBuffDetail def_inGameDeBuffDetail,
            InnateTrait.InnateTraitInfo atk_innateTraitInfo, InnateTrait.InnateTraitInfo def_innateTraitInfo,
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo, AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo,
            FarmTrait.FarmTraitInfo atk_farmTraitInfo, FarmTrait.FarmTraitInfo def_farmTraitInfo,
            float a_SkillHitRate, float a_DistanceCorrection, float a_RemainStaminaCorrection, float a_dex,
            float d_agi
            )
        {
            float a_TraitHitCorrection = atk_innateTraitInfo.Accurancy + atk_acquiredTraitInfo.accuracy + atk_farmTraitInfo.accuracy;

            float d_TraitHitCorrection = def_innateTraitInfo.Evasion + def_acquiredTraitInfo.evasion + def_farmTraitInfo.evansion;

            float a_accuracyStateChangeCorrection = atk_inGameBuffDetail.atk_AccuracyRate_Correction + atk_inGameDeBuffDetail.atk_AccuracyRate_Correction;

            float d_accuracyStateChangeCorrection = def_inGameBuffDetail.def_AccuracyRate_Correction + def_inGameDeBuffDetail.def_AccuracyRate_Correction;

            float acuracyRate = MonsterBattle.Get_Accuracy(a_SkillHitRate, a_DistanceCorrection, a_accuracyStateChangeCorrection, a_TraitHitCorrection, a_RemainStaminaCorrection, a_dex, d_accuracyStateChangeCorrection, d_TraitHitCorrection, d_agi);

            Debug.Log("acuracy rate = " + acuracyRate);

            Debug.Log("acuracy rate after buff = " + acuracyRate);

            //isCriticalRate converted to 100% = 100f
            float percentageAcuracyRate = Mathf.Clamp(acuracyRate * 100f, 0f, 100f);

            float ranHitVal = UnityEngine.Random.Range(0f, 100f);

            Debug.Log("ranHitVal = " + ranHitVal);

            bool isHit = ranHitVal <= percentageAcuracyRate;

            Debug.Log("Hit = " + isHit);

            return isHit;
        }

        #endregion

        #region Critical Rate

        /// <summary>
        /// <para> 100% = 1f </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Critical_Rate_Basic_Value()
        {
            return 0.05f;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Critical rate calculation formula </para>
        /// <para> [Attacker] </para>
        /// <para> Base Critical Chance: 5% </para>
        /// <para> Skill critical rate: aCRT (Skill critical) </para>
        /// <para> Distance Correction: aD (Distance) </para>
        /// <para> Correction by state change: aSC (State correction) </para>
        /// <para> Characteristic correction: aCC (Critical correction) </para>
        /// <para> Remaining stamina correction: aRSC (Stamina correction) </para>
        /// <para> [Defender] </para>
        /// <para> Characteristic correction: dCC (Critical correction) </para>
        /// <para> ++++++++++++++++++++++ </para>
        /// <para> Final critical rate: R (Result) </para>
        /// <para> R = 5% + aCRT × (aD + aSC + aCC + aRSC - dCC) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Critical_Rate
            (
            float a_SkillCriticalRate, float a_DistanceCorrection, float a_StateCorrection, float a_CriticalCorrection, float a_RemainStaminaCorrection,
            float d_CriticalCorrection
            )
        {
            float basicCriticalRate = Get_Critical_Rate_Basic_Value();

            float result = basicCriticalRate + a_SkillCriticalRate * (a_DistanceCorrection + a_RemainStaminaCorrection + a_StateCorrection + a_CriticalCorrection - d_CriticalCorrection);

            DebugPopup.Log("Critical Rate");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Attacker");
            DebugPopup.Log("Base Critical Chance = " + basicCriticalRate);
            DebugPopup.Log("Skill critical rate: aCRI = " + a_SkillCriticalRate);
            DebugPopup.Log("Distance Correction: aD = " + a_DistanceCorrection);
            DebugPopup.Log("Correction by state change: aSC = " + a_StateCorrection);
            DebugPopup.Log("Trait Correction: aCC = " + a_CriticalCorrection);
            DebugPopup.Log("Remaining stamina correction: aSRC = " + a_RemainStaminaCorrection);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Defender");
            DebugPopup.Log("Trait Correction: dCC = " + d_CriticalCorrection);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Final critical rate: R = " + result);
            DebugPopup.Log("Critical Rate");
            DebugPopup.Log("//////////////////////////////");

            return result;
        }

        public static bool Get_Critical_IsCritical
            (
            ParameterInGame atk_parameterInGame, ParameterInGame def_parameterInGame,
            InGameBuffDetail atk_inGameBuffDetail, InGameBuffDetail def_inGameBuffDetail,
            InGameDeBuffDetail atk_inGameDeBuffDetail, InGameDeBuffDetail def_inGameDeBuffDetail,
            InnateTrait.InnateTraitInfo atk_innateTraitInfo, InnateTrait.InnateTraitInfo def_innateTraitInfo,
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo, AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo,
            FarmTrait.FarmTraitInfo atk_farmTraitInfo, FarmTrait.FarmTraitInfo def_farmTraitInfo,
            float a_SkillCriticalRate, float a_DistanceCorrection, float a_RemainStaminaCorrection
            )
        {
            float a_CriticalCorrection = atk_innateTraitInfo.Critical + atk_acquiredTraitInfo.critical + atk_farmTraitInfo.critical;

            float d_CriticalCorrection = def_innateTraitInfo.Taken_Critical_Correction;

            float a_criticalStateChangeCorrection = atk_inGameBuffDetail.criticalRate_Correction;

            float isCriticalRate = MonsterBattle.Get_Critical_Rate(a_SkillCriticalRate, a_DistanceCorrection, a_criticalStateChangeCorrection, a_CriticalCorrection, a_RemainStaminaCorrection, d_CriticalCorrection);

            Debug.Log("isCriticalRate = " + isCriticalRate);

            isCriticalRate += atk_inGameBuffDetail.criticalRate_Correction;

            Debug.Log("isCriticalRate buff = " + isCriticalRate);

            //isCriticalRate converted to 100% = 100f
            float percentageCriticalRate = Mathf.Clamp(isCriticalRate * 100f, 0f, 100f);

            float ranCritVal = UnityEngine.Random.Range(0f, 100f);

            Debug.Log("ranCritVal = " + ranCritVal);

            bool isCritical = ranCritVal <= percentageCriticalRate;

            Debug.Log("isCritical = " + isCritical);

            return isCritical;
        }

        #endregion

        #region Order of Action

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Delay damage formula </para>
        /// <para> [Attacker] </para>
        /// <para> Skill Delay Damage: aDLY (Skill delay) </para>
        /// <para> Distance Correction: aD (Distance) </para>
        /// <para> Remaining stamina correction: aRSC (Stamina correction) </para>
        /// <para> Final Delay Damage: R (Result) </para>
        /// <para> R = aDLY × (aD + aSC) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Order_Delay_Damage(float a_SkillDelay, float a_DistanceCorrection, float a_RemainStaminaCorrection)
        {
            float result = a_SkillDelay * (a_DistanceCorrection + a_RemainStaminaCorrection);

            DebugPopup.Log("Delay damage");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Attacker");
            DebugPopup.Log("Skill Delay Damage: aDLY = " + a_SkillDelay);
            DebugPopup.Log("Distance Correction: aD = " + a_DistanceCorrection);
            DebugPopup.Log("Remaining stamina correction: aRSC = " + a_RemainStaminaCorrection);
            DebugPopup.Log("Final Delay Damage: R = " + result);

            return result;
        }

        #endregion

        #region HP Recovery

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Recovery formula </para>
        /// <para> [Healer] </para>
        /// <para> Skill recovery amount: hHL (Skill heal) it is percentage 100% = 1f </para>
        /// <para> Distance Correction: aD (Distance) </para>
        /// <para> Trait Correction: hTC (Critical correction) </para>
        /// <para> Remaining stamina correction: hRSC (Stamina correction) </para>
        /// <para> [Target] </para>
        /// <para> HP: tHP </para>
        /// <para> Correction by state change: tSC (State correction) </para>
        /// <para> Final recovery amount: R (Result) </para>
        /// <para> R = tHP × hHL × ( hD + hCC + hRSC - tSC - tCC ) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_HP_Recovery
            (
            float h_SkillHeal, float h_DistanceCorrection, float h_CriticalCorrection, float h_RemainStaminaCorrection,
            float t_Health, float t_StateCorrection
            )
        {
            float result = t_Health * h_SkillHeal * (h_DistanceCorrection + h_CriticalCorrection + h_RemainStaminaCorrection - t_StateCorrection);

            DebugPopup.Log("HP Recovery");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Healer");
            DebugPopup.Log("Skill recovery amount: hHL = " + h_SkillHeal);
            DebugPopup.Log("Distance Correction: aD = " + h_DistanceCorrection);
            DebugPopup.Log("Trait Correction: hTC = " + h_CriticalCorrection);
            DebugPopup.Log("Remaining stamina correction: hSC = " + h_RemainStaminaCorrection);
            DebugPopup.Log("Target");
            DebugPopup.Log("Max HP: tHP = " + t_Health);
            DebugPopup.Log("Correction by state change: tSC = " + t_StateCorrection);
            DebugPopup.Log("Final recovery amount: R = " + result);

            return result;
        }

        #endregion

        #region Debuff Success Rate

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Debuff Success Rate Calculation Formula </para>
        /// <para> [Attacker] </para>
        /// <para> Status change success rate: aS (Skill success) </para>
        /// <para> Distance Correction: aD (Distance) </para>
        /// <para> Trait Correction: aTC </para>
        /// <para> Remaining stamina correction: hSC (Stamina correction) </para>
        /// <para> [Defender] </para>
        /// <para> State change resistance: dSR (State change resistance) </para>
        /// <para> Final success probability: R (Result) </para>
        /// <para> ++++++++++++++++++++++ </para>
        /// <para> R = aS × ( aD + aTC + aSC - dSR ) </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Debuff_Success_Rate
            (
            float a_DebuffSuccessRate, float a_DistanceCorrection, float a_traitCorrection, float a_RemainStaminaCorrection,
            float d_StateChangeResistance
            )
        {
            float result = a_DebuffSuccessRate * (a_DistanceCorrection + a_traitCorrection + a_RemainStaminaCorrection - d_StateChangeResistance);

            DebugPopup.Log("Debuff Success Rate");
            DebugPopup.Log("//////////");
            DebugPopup.Log("Attacker");
            DebugPopup.Log("Status change success rate: aS = " + a_DebuffSuccessRate);
            DebugPopup.Log("Distance Correction: aD = " + a_DistanceCorrection);
            DebugPopup.Log("Trait Correction: hTC = " + a_traitCorrection);
            DebugPopup.Log("Remaining stamina correction: hSC = " + a_RemainStaminaCorrection);
            DebugPopup.Log("//////////");
            DebugPopup.Log("Defender");
            DebugPopup.Log("State change resistance: dSR = " + d_StateChangeResistance);
            DebugPopup.Log("Final success probability: R = " + result);

            return result;
        }

        #endregion

        #region Distance

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Skill performance decreases by 5% for each square away from the skill's spawn point </para>
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static float Get_Distance_Correction(int distance)
        {
            float result = 1f;

            for (int i = 1; i < distance; i++)
            {
                result -= 0.05f;
            }

            if (result < 0f)
            {
                return 0f;
            }

            return result;
        }

        /// <summary>
        /// <para> 100% = 1f </para>
        /// <para> Skill performance decreases by 5% for each square away from the skill's spawn point </para>
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static float Get_Caculated_Distance_Correction(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility, Skill.Skill.OccurrenceType occurrenceType)
        {
            DebugPopup.Log($"occurrenceType = {occurrenceType}");

            float DistanceCorrection = 0f;

            if (occurrenceType == Skill.Skill.OccurrenceType.指定_designation)
            {
                List<ILevelCell> abilityCells = unitAbility.GetAbilityCells(InCaster);
                List<ILevelCell> PathCells = InCaster.GetPathTo(selectedCell, abilityCells);
                int distanceAtkToDef = PathCells.Count - 1;

                DistanceCorrection = MonsterBattle.Get_Distance_Correction(distanceAtkToDef);
                Debug.Log("PathCells base designation distance count = " + PathCells.Count);
                Debug.Log("skill base designation Distance Correction = " + DistanceCorrection);

                List<ILevelCell> abilityEffectCells = unitAbility.GetEffectedCells(InCaster, selectedCell);
                List<ILevelCell> effectPathCells = InCaster.GetEffectPathTo(selectedCell, InObject.GetCell(), abilityEffectCells);
                int effectDistanceAtkToDef = effectPathCells.Count - 1;
                for (int i = 0; i < effectDistanceAtkToDef; i++)
                {
                    DistanceCorrection = DistanceCorrection * 0.95f;
                }
                Debug.Log("skill caculated designation Distance Correction = " + DistanceCorrection);
            }
            else
            {
                List<ILevelCell> abilityCells = unitAbility.GetAbilityCells(InCaster);
                List<ILevelCell> PathCells = InCaster.GetPathTo(InCaster.GetCell(), abilityCells);
                int distanceAtkToDef = PathCells.Count - 1;

                DistanceCorrection = MonsterBattle.Get_Distance_Correction(distanceAtkToDef);
                Debug.Log("PathCells base distance count = " + PathCells.Count);
                Debug.Log("skill base Distance Correction = " + DistanceCorrection);
            }

            return DistanceCorrection;
        }

        #endregion
    }
}