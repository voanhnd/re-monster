using UnityEngine;

namespace Assets.Scripts.Monster.Skill
{
    [CreateAssetMenu(fileName = "SkillDataScriptObj", menuName = "ScriptableObjects/Monster/SkillDataScriptObj", order = 1)]
    public class SkillDataScriptObj : ScriptableObject
    {
        [SerializeField]
        Skill skill;

        public Skill Skill { get => skill; private set => skill = value; }

        //string lastSkillTypeEnumString = string.Empty;

        //private void OnValidate()
        //{
        //    if(lastSkillTypeEnumString != string.Empty && lastSkillTypeEnumString != skill.skillType.ToString())
        //    {
        //        Skill.SkillType skillType = Skill.SkillType.

        //        Enum.TryParse<TEnum>(enumStr, out result)
        //    }

        //    if(lastSkillTypeEnumString == string.Empty)
        //    {
        //        lastSkillTypeEnumString = skill.skillType.ToString();
        //    }
        //}


    }
}