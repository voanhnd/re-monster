﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;

namespace Assets.Scripts.Monster.Skill
{
    [System.Serializable]
    public class InGameBuffDetail
    {
        public MonsterSpecialBufferType monsterSpecialBufferType;

        public int buffTurn = 0;

        public int disableDeBuff = 0;

        public bool isInVisible = false;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float criticalRate_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float atk_Damage_Correction = 0f;
        public float def_Damage_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float health_Heal_Correction = 0f;

        /// <summary>
        /// fixed number
        /// </summary>
        public float stamina_Recovery = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float stamina_Recovery_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float speed_Up_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float atk_AccuracyRate_Correction = 0f;
        public float def_AccuracyRate_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float STR_Inscrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float INT_Inscrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float DEX_Inscrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float AGI_Inscrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float VIT_Inscrease_Correction = 0f;

        /// <summary>
        /// /////////////////////////////////////////
        /// </summary>

        public float STR_Inscrease = 0f;

        public float INT_Inscrease = 0f;

        public float DEX_Inscrease = 0f;

        public float AGI_Inscrease = 0f;

        public float VIT_Inscrease = 0f;

        public float EffectValue { get; private set; }

        public InGameBuffDetail()
        {
            buffTurn = 0;

            disableDeBuff = 0;

            isInVisible = false;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            criticalRate_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            atk_Damage_Correction = 0f;
            def_Damage_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            health_Heal_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            stamina_Recovery_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            speed_Up_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            atk_AccuracyRate_Correction = 0f;
            def_AccuracyRate_Correction = 0f;

            //STR_Inscrease_Correction = 0f;
            //INT_Inscrease_Correction = 0f;
            //DEX_Inscrease_Correction = 0f;
            //AGI_Inscrease_Correction = 0f;
            //VIT_Inscrease_Correction = 0f;

            STR_Inscrease = 0f;
            INT_Inscrease = 0f;
            DEX_Inscrease = 0f;
            AGI_Inscrease = 0f;
            VIT_Inscrease = 0f;
        }

        public static bool IsPsychic(MonsterSpecialBufferType monsterSpecialBufferType)
        {
            return monsterSpecialBufferType switch
            {
                MonsterSpecialBufferType.勇気_Courage => true,
                MonsterSpecialBufferType.応援_Cheering => true,
                MonsterSpecialBufferType.集中_Concentration => true,
                _ => false,
            };
        }

        public static InGameBuffDetail GetBuffComplex(List<InGameBuffDetail> inGameBuffDetails)
        {
            InGameBuffDetail complexBuff = new InGameBuffDetail();

            foreach (InGameBuffDetail buff in inGameBuffDetails)
            {
                complexBuff.disableDeBuff = buff.disableDeBuff;

                if (complexBuff.isInVisible == false)
                {
                    complexBuff.isInVisible = buff.isInVisible;
                }

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.criticalRate_Correction += buff.criticalRate_Correction;

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.atk_Damage_Correction += buff.atk_Damage_Correction;
                complexBuff.def_Damage_Correction += buff.def_Damage_Correction;

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.health_Heal_Correction += buff.health_Heal_Correction;

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.stamina_Recovery_Correction += buff.stamina_Recovery_Correction;

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.speed_Up_Correction += buff.speed_Up_Correction;

                /// <summary>
                /// this value must be converted 100% = 1f;
                /// </summary>
                complexBuff.atk_AccuracyRate_Correction += buff.atk_AccuracyRate_Correction;
                complexBuff.def_AccuracyRate_Correction += buff.def_AccuracyRate_Correction;

                complexBuff.STR_Inscrease += buff.STR_Inscrease;
                complexBuff.INT_Inscrease += buff.INT_Inscrease;
                complexBuff.DEX_Inscrease += buff.DEX_Inscrease;
                complexBuff.AGI_Inscrease += buff.AGI_Inscrease;
                complexBuff.VIT_Inscrease += buff.VIT_Inscrease;
            }

            return complexBuff;
        }

        public static List<InGameBuffDetail> AddBuff(GridUnit caster, GridUnit target,
                                                     ParameterInGame atkParameterInGame, MonsterInGame monsterAtk,
                                                     MonsterDataScriptObj monsterAtkScriptObject, BasicParameters atk_cur_ComplexParameter,
                                                     ParameterInGame defParameterInGame, MonsterInGame monsterDef,
                                                     MonsterDataScriptObj monsterDefScriptObject, BasicParameters def_cur_ComplexParameter,
                                                     SkillDataScriptObj atk_skillDataScriptObj, Skill.SkillDetail atk_skillDetail)
        {
            List<InGameBuffDetail> newBuffList = new List<InGameBuffDetail>();

            foreach (Skill.SkillBuffDetail skillBuffDetail in atk_skillDetail.skillBuffDetails)
            {
                InGameBuffDetail inGameBuffDetail = null;

                inGameBuffDetail = Buff_Veil(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Barrier(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Active(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Energize(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Vitality(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_SpeedUp(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Store(caster, target, skillBuffDetail, atk_skillDetail, atk_skillDataScriptObj);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Illusion(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Provoke(caster, target, skillBuffDetail, monsterDef.MonsterInfo.monsterID);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Hide(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Increase_STR(caster, target, skillBuffDetail, atk_cur_ComplexParameter);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Increase_INT(caster, target, skillBuffDetail, atk_cur_ComplexParameter);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Increase_DEX(caster, target, skillBuffDetail, atk_cur_ComplexParameter);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Increase_AGI(caster, target, skillBuffDetail, atk_cur_ComplexParameter);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Increase_VIT(caster, target, skillBuffDetail, atk_cur_ComplexParameter);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Bravery(caster, target, skillBuffDetail, atk_skillDetail, atk_skillDataScriptObj);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Support(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }

                inGameBuffDetail = Buff_Focus(caster, target, skillBuffDetail);
                if (inGameBuffDetail != null)
                {
                    newBuffList.Add(inGameBuffDetail);
                    continue;
                }
            }

            return newBuffList;
        }

        public static InGameBuffDetail Buff_Veil(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.ベール_Veil)
            {
                return null;
            }

            //  ベール Veil
            //  "一度だけ、新規にかかるデバフを無効化する、2ターン継続
            //  Disable Debuff mới 1 lần và kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.ベール_Veil;

            inGameBuffDetail.disableDeBuff = 1;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Barrier(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.バリア_Barrier)
                return null;

            //  バリア Barrier
            //  "被ダメージ数%減少（スキルによる）、2ターン継続
            //  Giảm trấn thương nhận được(tuỳ vào skill), kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.バリア_Barrier;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.def_Damage_Correction = skillBuffDetail.value / 100f;

            inGameBuffDetail.buffTurn = 2;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;
            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Active(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.賦活_Activation)
                return null;

            //  賦活 Kích hoạt
            //  "そのMonsterのターン毎に最大HPの数%回復（スキルによる）、3ターン継続
            //  Phục hồi tối đa bao nhiêu % HP cho 1 turn của Monster đó, kéo dài 3 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.賦活_Activation;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.health_Heal_Correction = skillBuffDetail.value / 100f;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Energize(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.活力_Energize)
                return null;

            //  活力 Energize
            //  Increases ST recovery amount by a few percent (depending on skill), lasts 3 turns

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.活力_Energize;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.stamina_Recovery_Correction = skillBuffDetail.value / 100f;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Vitality(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.活力_Vitality)
                return null;

            //  活力 Sức sống
            //  "ST回復量が数%増加、3ターン継続
            //  Tăng lượng phục hồi ST %, kéo dài 3 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.活力_Vitality;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.stamina_Recovery_Correction = skillBuffDetail.value / 100f;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_SpeedUp(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.加速_Acceleration)
                return null;

            //  加速 tăng tốc
            //  "スピード+10%、2ターン継続
            //  Speed + 10 %, kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.加速_Acceleration;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.speed_Up_Correction = skillBuffDetail.value / 100f;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Store(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, Skill.SkillDetail skillDetail, SkillDataScriptObj skillDataScriptObj)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.溜め_Charge)
                return null;

            //  溜め Chứa
            //  "次回攻撃時にスキル毎に決められた量の与ダメージ上昇
            //  Tăng trấn thương gây ra được xác định cho mỗi skill trong lần tấn công tiếp theo"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.溜め_Charge;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.atk_Damage_Correction = skillValue;

            inGameBuffDetail.buffTurn = 1;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Illusion(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.幻影_Illusion)
                return null;

            //  幻影 Ảo giác
            //  "回避率数%上昇（スキルによる）、2ターン継続
            //  Tăng tỷ lệ né tránh % (tuỳ skill), kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.幻影_Illusion;

            //if value is percentage it must be convected to 100f = 1f;
            inGameBuffDetail.def_AccuracyRate_Correction = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Provoke(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, string monsterID)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.挑発_Provocation)
                return null;

            //  挑発 Khiêu khích
            //  "攻撃範囲に入っていると、このMonsterしか狙えなくなる、xターン（スキルによる）継続
            //  Khi trong phạm vi tấn công, chỉ có thể nhắm vào con monster này và kéo dài x turn(tuỳ skill)"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.挑発_Provocation;

            inGameBuffDetail.buffTurn = (int)skillBuffDetail.value;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Hide(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.潜伏_Invisibility)
                return null;

            //  潜伏 Trốn
            //  "相手から見えなくなり攻撃のターゲットに出来なくなる、3ターン継続
            //  ターン経過以外に範囲攻撃のヒット、サーチスキル、スキルを使うことで解除される
            //  Tàng hình trc đối thủ và k phải là mục tiêu tấn công, kéo dài 3 turn
            //  Sẽ huỷ bằng cách sử dụng các đòn tấn công khu vuưc, search skill, skill ngoài quá trình turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.潜伏_Invisibility;

            inGameBuffDetail.isInVisible = true;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Increase_STR(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, BasicParameters atk_complexParameters)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.Increase_STR)
                return null;

            //  STR増加 Tăng

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.Increase_STR;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.STR_Inscrease = atk_complexParameters.strenght * skillValue;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Increase_INT(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, BasicParameters atk_complexParameters)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.Increase_INT)
                return null;

            //  INT増加 Tăng

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.Increase_INT;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.INT_Inscrease = atk_complexParameters.intelligent * skillValue;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Increase_DEX(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, BasicParameters atk_complexParameters)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.Increase_DEX)
                return null;

            //  DEX増加 Tăng

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.Increase_DEX;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.DEX_Inscrease = atk_complexParameters.dexterity * skillValue;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Increase_AGI(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, BasicParameters atk_complexParameters)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.Increase_AGI)
                return null;

            //  AGI増加 Tăng

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.Increase_AGI;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.AGI_Inscrease = atk_complexParameters.agility * skillValue;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Increase_VIT(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, BasicParameters atk_complexParameters)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.Increase_VIT)
                return null;

            //  VIT増加 Tăng

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.Increase_VIT;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.VIT_Inscrease = atk_complexParameters.vitality * skillValue;

            inGameBuffDetail.buffTurn = 2;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Bravery(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail, Skill.SkillDetail skillDetail, SkillDataScriptObj skillDataScriptObj)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.勇気_Courage)
                return null;

            //  勇気 Dũng khí
            //  "与ダメージ、クリティカル率を数%上昇（スキルによる）、3ターン継続
            //  Tăng trấn thương gây ra và critical rate(tuỳ skill), kéo dài 3 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.勇気_Courage;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.atk_Damage_Correction = skillValue;

            inGameBuffDetail.criticalRate_Correction = skillValue;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Support(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.応援_Cheering)
                return null;

            //  応援 Ủng hộ
            //  "クリティカル率数%上昇（スキルによる）、3ターン継続
            //  Tăng critical rate(tuỳ skill), kéo dài 3 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.応援_Cheering;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.criticalRate_Correction = skillValue;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }

        public static InGameBuffDetail Buff_Focus(GridUnit InCaster, GridUnit InTarget, Skill.SkillBuffDetail skillBuffDetail)
        {
            if (skillBuffDetail.monsterSpecialBufferType != MonsterSpecialBufferType.集中_Concentration)
                return null;

            //  集中 Tập trung
            //  "命中率と回避率数%上昇（スキルによる）、2ターン継続 
            //  Tăng tỷ lệ chính xác và tỷ lệ né(tuỳ skill) kéo dà 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameBuffDetail inGameBuffDetail = new InGameBuffDetail();

            inGameBuffDetail.monsterSpecialBufferType = MonsterSpecialBufferType.集中_Concentration;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillBuffDetail.value / 100f;
            inGameBuffDetail.EffectValue = skillBuffDetail.value;

            inGameBuffDetail.atk_AccuracyRate_Correction = skillValue;

            inGameBuffDetail.def_AccuracyRate_Correction = skillValue;

            inGameBuffDetail.buffTurn = 3;

            return inGameBuffDetail;
        }
    }
}