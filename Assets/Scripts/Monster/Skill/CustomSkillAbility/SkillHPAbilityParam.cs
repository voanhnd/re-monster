using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill.CustomSkillAbility
{
    [CreateAssetMenu(fileName = "SkillHealAbilityParam", menuName = "TurnBasedTools/Custom Ability/Parameters/ Create SkillHealAbilityParam", order = 1)]
    public class SkillHPAbilityParam : AbilityParam
    {
        public int removePhysicDebuffNum = 0;

        public override void ApplyTo(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility)
        {
            //Check team
            if (IsEffectTeam(InCaster, InObject, unitAbility) == false)
            {
                return;
            }
            //

            float hp_heal = 0f;

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();
            MonsterDataScriptObj atk_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.MonsterInfo.monsterID);
            BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();

            ParameterInGame defParameterInGame = InObject.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InObject.GetComponent<MonsterInGame>();
            MonsterDataScriptObj def_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterDef.MonsterInfo.monsterID);
            BasicParameters def_cur_ComplexParameter = defParameterInGame.GetCurrentComplexParameter();

            SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(unitAbility.GetSkillID());
            int skillLvl = GetSkillLevel(monsterAtk.MonsterInfo, atk_skillDataScriptObj);

            Debug.Log("Use skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
            Debug.Log("skill level = " + skillLvl);

            Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[skillLvl - 1];
            DebugPopup.RegLog(InCaster.GetTeam());
            DebugPopup.InAttack(true);
            DebugPopup.Log("Caculate Heal ability monster = " + atk_monsterScriptObject.Monster.monsterName);
            DebugPopup.Log("Use Skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
            DebugPopup.Log("Target = " + def_monsterScriptObject.Monster.monsterName);

            //Add buff

            if (unitAbility.m_BuffSeft)
            {
                atkParameterInGame.AddNewBuffs
                    (
                        InGameBuffDetail.AddBuff
                            (
                            InCaster, InObject.GetCell().GetUnitOnCell(),
                            atkParameterInGame, monsterAtk, atk_monsterScriptObject, atk_cur_ComplexParameter,
                            defParameterInGame, monsterDef, def_monsterScriptObject, def_cur_ComplexParameter,
                            atk_skillDataScriptObj, atk_skillDetail
                            )
                    );
            }
            else
            {
                defParameterInGame.AddNewBuffs
                    (
                        InGameBuffDetail.AddBuff
                            (
                            InCaster, InObject.GetCell().GetUnitOnCell(),
                            atkParameterInGame, monsterAtk, atk_monsterScriptObject, atk_cur_ComplexParameter,
                            defParameterInGame, monsterDef, def_monsterScriptObject, def_cur_ComplexParameter,
                            atk_skillDataScriptObj, atk_skillDetail
                            )
                    );
            }

            //GetComplex buff and debuff befor add new buff

            InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();

            InGameBuffDetail def_InGameBuffDetail = defParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail def_InGameDeBuffDetail = defParameterInGame.GetInGameComplexDeBuff();

            //End 

            #region ATK Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("ATK Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo atk_innateTraitInfo = atkParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {atk_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in atkParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo = atkParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo atk_farmTraitInfo = atkParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {atk_farmTraitInfo.farmTraitType}");

            #endregion ATK trait

            #region DEF Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("DEF Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo def_innateTraitInfo = defParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {def_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in defParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo = defParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo def_farmTraitInfo = defParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {def_farmTraitInfo.farmTraitType}");

            #endregion DEF trait

            //Basic parameter

            float atk_str = Mathf.Clamp(atk_cur_ComplexParameter.strenght + atk_inGameBuffDetail.STR_Inscrease + atk_inGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float atk_int = Mathf.Clamp(atk_cur_ComplexParameter.intelligent + atk_inGameBuffDetail.INT_Inscrease + atk_inGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float atk_dex = Mathf.Clamp(atk_cur_ComplexParameter.dexterity + atk_inGameBuffDetail.DEX_Inscrease + atk_inGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float atk_agi = Mathf.Clamp(atk_cur_ComplexParameter.agility + atk_inGameBuffDetail.AGI_Inscrease + atk_inGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float atk_vit = Mathf.Clamp(atk_cur_ComplexParameter.vitality + atk_inGameBuffDetail.VIT_Inscrease + atk_inGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            float def_str = Mathf.Clamp(def_cur_ComplexParameter.strenght + def_InGameBuffDetail.STR_Inscrease + def_InGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float def_int = Mathf.Clamp(def_cur_ComplexParameter.intelligent + def_InGameBuffDetail.INT_Inscrease + def_InGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float def_dex = Mathf.Clamp(def_cur_ComplexParameter.dexterity + def_InGameBuffDetail.DEX_Inscrease + def_InGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float def_agi = Mathf.Clamp(def_cur_ComplexParameter.agility + def_InGameBuffDetail.AGI_Inscrease + def_InGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float def_vit = Mathf.Clamp(def_cur_ComplexParameter.vitality + def_InGameBuffDetail.VIT_Inscrease + def_InGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            //End basicParameter

            //Distance correction
            float a_DistanceCorrection = MonsterBattle.Get_Caculated_Distance_Correction(InCaster, InObject, selectedCell, unitAbility, atk_skillDetail.occurrenceType);

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //Caculate stamina

            DebugPopup.Log("Attacker current Stamina = " + atkParameterInGame.GetCurrentStamina());
            float consumedStaminaTraitCorrection = atk_innateTraitInfo.ST_Consumption_Correction;
            float consumedStamina = MonsterBattle.Get_Stamina_Consumption(atk_skillDetail.consumedST, consumedStaminaTraitCorrection);
            float a_RemainingStaminaCorrection = MonsterBattle.Get_Stamina_Performance_Improvement(atkParameterInGame.GetCurrentStamina(), consumedStamina);
            Debug.Log("a_RemainingStaminaCorrection = " + a_RemainingStaminaCorrection);

            //End Caculate stamina

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////


            //Add debuff
            if (unitAbility.m_DeBuffSeft)
            {
                atkParameterInGame.AddNewDeBuffs
                    (
                        InGameDeBuffDetail.AddDeBuff
                            (
                            InCaster, InObject.GetCell().GetUnitOnCell(),
                            atkParameterInGame, monsterAtk, atk_monsterScriptObject, atk_cur_ComplexParameter,
                            defParameterInGame, monsterDef, def_monsterScriptObject, def_cur_ComplexParameter,
                            atk_skillDataScriptObj, atk_skillDetail, a_DistanceCorrection, a_RemainingStaminaCorrection
                            )
                    );
            }
            else
            {
                defParameterInGame.AddNewDeBuffs
                    (
                        InGameDeBuffDetail.AddDeBuff
                            (
                            InCaster, InObject.GetCell().GetUnitOnCell(),
                            atkParameterInGame, monsterAtk, atk_monsterScriptObject, atk_cur_ComplexParameter,
                            defParameterInGame, monsterDef, def_monsterScriptObject, def_cur_ComplexParameter,
                            atk_skillDataScriptObj, atk_skillDetail, a_DistanceCorrection, a_RemainingStaminaCorrection
                            )
                    );
            }

            //Remove Debuff

            List<InGameDeBuffDetail> removePhysicDebuffList = new();
            int m_removePhysicDebuffNum = removePhysicDebuffNum;

            foreach (InGameDeBuffDetail debuff in defParameterInGame.inGameDeBuffDetails)
            {
                if (m_removePhysicDebuffNum > 0 && InGameDeBuffDetail.IsPsychic(debuff.monsterSpecialDeBufferType) == false)
                {
                    m_removePhysicDebuffNum--;

                    removePhysicDebuffList.Add(debuff);
                }
            }

            foreach (InGameDeBuffDetail debuff in removePhysicDebuffList)
            {
                defParameterInGame.inGameDeBuffDetails.Remove(debuff);
            }

            //End remove Debuff

            //Caculat HP Heal
            //it must be converted to 100% = 1f
            float h_skillHPHeal = atk_skillDetail.hp_recovery / 100f;
            Debug.Log("h_skillHPHeal = " + h_skillHPHeal);

            float a_CriticalCorrection = atk_innateTraitInfo.HP_Recovery_Correction;

            float d_stateCorrection = def_InGameBuffDetail.health_Heal_Correction + def_InGameDeBuffDetail.Health_Heal_Correction;

            hp_heal = MonsterBattle.Get_HP_Recovery(h_skillHPHeal, a_DistanceCorrection, a_CriticalCorrection, a_RemainingStaminaCorrection, defParameterInGame.GetDefaultComplexParameter().health, d_stateCorrection);

            if (defParameterInGame)
            {
                defParameterInGame.Heal_Health(hp_heal);
            }

            //End HP Heal

            float skillDelayDamage = MonsterBattle.Get_Order_Delay_Damage(atk_skillDetail.delayDamage, a_DistanceCorrection, a_RemainingStaminaCorrection);
            //Debug.Log("Skill attack skillDelay = " + skillDelay);

            defParameterInGame.AddSkillDelay(skillDelayDamage, false);

            DebugPopup.InAttack(false);
        }

        int GetSkillLevel(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, SkillDataScriptObj skillDataScriptObj)
        {
            for (int i = 0; i < playerMonsterInfo.MonsterSkillInfos.Count; i++)
            {
                int skillIndex = i;// to prevent wrong data transfer
                Monster.MonsterSkillInfo skillInfo = playerMonsterInfo.MonsterSkillInfos[skillIndex];
                if (skillInfo.skillID == skillDataScriptObj.Skill.skillID)
                {
                    return skillInfo.skillLevel;
                }
            }

            return 0;
        }

        //public override string GetAbilityInfo()
        //{
        //    return "Damage" + m_Damage.ToString();
        //}
    }
}