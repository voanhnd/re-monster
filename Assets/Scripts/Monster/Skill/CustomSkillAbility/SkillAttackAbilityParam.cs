﻿using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Trait;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill.CustomSkillAbility
{
    [CreateAssetMenu(fileName = "SkillAttackAbilityParam", menuName = "TurnBasedTools/Custom Ability/Parameters/ Create SkillAttackAbilityParam", order = 1)]
    public class SkillAttackAbilityParam : AbilityParam
    {
        public override void ApplyTo(GridUnit InCaster, GridObject InObject, ILevelCell selectedCell, UnitAbility unitAbility)
        { 
            //Check team
            if (!IsEffectTeam(InCaster, InObject, unitAbility))
            {
                return;
            }
            //

            ParameterInGame atkParameterInGame = InCaster.GameMonster.InGameParameter;
            MonsterInGame monsterAtk = InCaster.GameMonster;
            MonsterDataScriptObj atk_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.MonsterInfo.monsterID);
            BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();

            MonsterInGame monsterDef = InObject.GetComponent<MonsterInGame>();
            ParameterInGame defParameterInGame = monsterDef.InGameParameter;
            MonsterDataScriptObj def_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterDef.MonsterInfo.monsterID);
            BasicParameters def_cur_ComplexParameter = defParameterInGame.GetCurrentComplexParameter();

            SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(unitAbility.GetSkillID());
            int skillLvl = Skill.GetSkillLevel(monsterAtk.MonsterInfo, atk_skillDataScriptObj);

            Debug.Log("Use skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
            Debug.Log("skill level = " + skillLvl);

            Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[skillLvl - 1];

            DebugPopup.RegLog(InCaster.GetTeam());
            DebugPopup.InAttack(true);
            DebugPopup.Log("Caculate Attack ability monster = " + atk_monsterScriptObject.Monster.monsterName);
            DebugPopup.Log("Use Skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
            DebugPopup.Log("Target = " + def_monsterScriptObject.Monster.monsterName);

            //Add buff
            AddBuffList(InCaster, InObject, unitAbility,
                        atkParameterInGame, monsterAtk,
                        atk_monsterScriptObject, atk_cur_ComplexParameter,
                        defParameterInGame, monsterDef,
                        def_monsterScriptObject, def_cur_ComplexParameter,
                        atk_skillDataScriptObj, atk_skillDetail);


            //End add buff

            //GetComplex buff and debuff befor add new buff

            InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();

            InGameBuffDetail def_InGameBuffDetail = defParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail def_InGameDeBuffDetail = defParameterInGame.GetInGameComplexDeBuff();

            //End 

            #region ATK Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("ATK Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo atk_innateTraitInfo = atkParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {atk_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in atkParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo = atkParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo atk_farmTraitInfo = atkParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {atk_farmTraitInfo.farmTraitType}");

            #endregion ATK trait

            #region DEF Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("DEF Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo def_innateTraitInfo = defParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {def_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in defParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo = defParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo def_farmTraitInfo = defParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {def_farmTraitInfo.farmTraitType}");

            #endregion DEF trait

            //Basic parameter

            float atk_str = Mathf.Clamp(atk_cur_ComplexParameter.strenght + atk_inGameBuffDetail.STR_Inscrease + atk_inGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float atk_int = Mathf.Clamp(atk_cur_ComplexParameter.intelligent + atk_inGameBuffDetail.INT_Inscrease + atk_inGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float atk_dex = Mathf.Clamp(atk_cur_ComplexParameter.dexterity + atk_inGameBuffDetail.DEX_Inscrease + atk_inGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float atk_agi = Mathf.Clamp(atk_cur_ComplexParameter.agility + atk_inGameBuffDetail.AGI_Inscrease + atk_inGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float atk_vit = Mathf.Clamp(atk_cur_ComplexParameter.vitality + atk_inGameBuffDetail.VIT_Inscrease + atk_inGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            float def_str = Mathf.Clamp(def_cur_ComplexParameter.strenght + def_InGameBuffDetail.STR_Inscrease + def_InGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float def_int = Mathf.Clamp(def_cur_ComplexParameter.intelligent + def_InGameBuffDetail.INT_Inscrease + def_InGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float def_dex = Mathf.Clamp(def_cur_ComplexParameter.dexterity + def_InGameBuffDetail.DEX_Inscrease + def_InGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float def_agi = Mathf.Clamp(def_cur_ComplexParameter.agility + def_InGameBuffDetail.AGI_Inscrease + def_InGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float def_vit = Mathf.Clamp(def_cur_ComplexParameter.vitality + def_InGameBuffDetail.VIT_Inscrease + def_InGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            //End basicParameter

            //Distance correction
            float a_DistanceCorrection = MonsterBattle.Get_Caculated_Distance_Correction(InCaster, InObject, selectedCell, unitAbility, atk_skillDetail.occurrenceType);

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //Caculate stamina

            DebugPopup.Log("Attacker current Stamina = " + atkParameterInGame.GetCurrentStamina());
            float consumedStaminaTraitCorrection = atk_innateTraitInfo.ST_Consumption_Correction;
            float consumedStamina = MonsterBattle.Get_Stamina_Consumption(atk_skillDetail.consumedST, consumedStaminaTraitCorrection);
            float a_RemainingStaminaCorrection = MonsterBattle.Get_Stamina_Performance_Improvement(atkParameterInGame.GetCurrentStamina(), consumedStamina);
            Debug.Log("a_RemainingStaminaCorrection = " + a_RemainingStaminaCorrection);

            //End Caculate stamina

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //Caculate Acuracy

            //it must be converted to 100% = 1f
            float a_SkillHitRate = atk_skillDetail.hit / 100f;

            bool isHit = MonsterBattle.Get_Accuracy_IsHit
                (
                    atkParameterInGame, defParameterInGame,
                    atk_inGameBuffDetail, def_InGameBuffDetail,
                    atk_inGameDeBuffDetail, def_InGameDeBuffDetail,
                    atk_innateTraitInfo, def_innateTraitInfo,
                    atk_acquiredTraitInfo, def_acquiredTraitInfo,
                    atk_farmTraitInfo, def_farmTraitInfo,
                    a_SkillHitRate, a_DistanceCorrection, a_RemainingStaminaCorrection, atk_dex,
                    def_agi
                );

            DebugPopup.Log("Hit = " + isHit);

            //End Caculate Acuracy

            float skillDelayDamage = MonsterBattle.Get_Order_Delay_Damage(atk_skillDetail.delayDamage, a_DistanceCorrection, a_RemainingStaminaCorrection);
            //Debug.Log("Skill attack skillDelay = " + skillDelay);

            float hp_Damage = 0f;

            if (isHit)
            {
                //Add debuff
                AddDebuffList(InCaster, InObject, unitAbility,
                              atkParameterInGame, monsterAtk,
                              atk_monsterScriptObject, atk_cur_ComplexParameter,
                              defParameterInGame, monsterDef,
                              def_monsterScriptObject, def_cur_ComplexParameter,
                              atk_skillDataScriptObj, atk_skillDetail,
                              a_DistanceCorrection, a_RemainingStaminaCorrection);

                //End add debuff

                /////////////////////////////////////////////////////
                ////////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //Caculate Critical

                //it must be converted to 100% = 1f
                float a_SkillCriticalRate = atk_skillDetail.crt / 100f;

                bool isCritical = MonsterBattle.Get_Critical_IsCritical
                    (
                        atkParameterInGame, defParameterInGame,
                        atk_inGameBuffDetail, def_InGameBuffDetail,
                        atk_inGameDeBuffDetail, def_InGameDeBuffDetail,
                        atk_innateTraitInfo, def_innateTraitInfo,
                        atk_acquiredTraitInfo, def_acquiredTraitInfo,
                        atk_farmTraitInfo, def_farmTraitInfo,
                        a_SkillCriticalRate, a_DistanceCorrection, a_RemainingStaminaCorrection
                    );

                DebugPopup.Log("Is Critical = " + isCritical);

                //End Caculate Critical

                /////////////////////////////////////////////////////
                ////////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //Caculate Damage

                if (atk_skillDataScriptObj.Skill.skillGenType == Skill.SkillGenType.STR)
                {
                    hp_Damage = MonsterBattle.Get_Caculate_HP_Damage
                                    (
                                        atk_inGameBuffDetail, def_InGameBuffDetail,
                                        atk_inGameDeBuffDetail, def_InGameDeBuffDetail,
                                        atk_innateTraitInfo, def_innateTraitInfo,
                                        atk_acquiredTraitInfo, def_acquiredTraitInfo,
                                        atk_farmTraitInfo, def_farmTraitInfo,
                                        atk_monsterScriptObject, def_monsterScriptObject,
                                        atk_skillDetail, atk_skillDataScriptObj.Skill.skillGenType, a_DistanceCorrection, a_RemainingStaminaCorrection, atk_str,
                                        def_vit, isCritical
                                    );
                }
                else if (atk_skillDataScriptObj.Skill.skillGenType == Skill.SkillGenType.INT)
                {
                    hp_Damage = MonsterBattle.Get_Caculate_HP_Damage
                                    (
                                        atk_inGameBuffDetail, def_InGameBuffDetail,
                                        atk_inGameDeBuffDetail, def_InGameDeBuffDetail,
                                        atk_innateTraitInfo, def_innateTraitInfo,
                                        atk_acquiredTraitInfo, def_acquiredTraitInfo,
                                        atk_farmTraitInfo, def_farmTraitInfo,
                                        atk_monsterScriptObject, def_monsterScriptObject,
                                        atk_skillDetail, atk_skillDataScriptObj.Skill.skillGenType, a_DistanceCorrection, a_RemainingStaminaCorrection, atk_int,
                                        def_vit, isCritical
                                    );
                }

                BattleStatsCollector.Instance.AddDamageTake(InCaster, hp_Damage, InCaster.GetTeam());

                //Caculate Stamina damage

                float a_st_dmg = atk_skillDetail.st_dmg;

                float staminaDamage = MonsterBattle.Get_Stamina_Damage(a_st_dmg, a_DistanceCorrection, a_RemainingStaminaCorrection);

                Debug.Log("Skill attack stamina damage = " + staminaDamage);

                defParameterInGame.AddStamina(-staminaDamage);

                defParameterInGame.AddSkillDelay(skillDelayDamage, false);

                defParameterInGame.Damage(hp_Damage, isCritical);
                //End Caculate Stamina damage

                //End Damage
            }
            else
            {
                defParameterInGame.MissHit();
            }

            //public override string GetAbilityInfo()
            //{
            //    return "Damage" + m_Damage.ToString();
            //}

            //Custom debug

            DebugPopup.InAttack(false);
        }

        private static void AddBuffList(GridUnit InCaster, GridObject InObject, UnitAbility unitAbility, ParameterInGame atkParameterInGame, MonsterInGame monsterAtk, MonsterDataScriptObj monsterAtkScriptObject, BasicParameters atk_cur_ComplexParameter, ParameterInGame defParameterInGame, MonsterInGame monsterDef, MonsterDataScriptObj monsterDefScriptObject, BasicParameters def_cur_ComplexParameter, SkillDataScriptObj atk_skillDataScriptObj, Skill.SkillDetail atk_skillDetail)
        {
            if (unitAbility.m_BuffSeft)
            {
                List<InGameBuffDetail> atkBuffList = InGameBuffDetail.AddBuff(InCaster, InObject.GetCell().GetUnitOnCell(),
                                                         atkParameterInGame, monsterAtk,
                                                         monsterAtkScriptObject, atk_cur_ComplexParameter,
                                                         defParameterInGame, monsterDef,
                                                         monsterDefScriptObject, def_cur_ComplexParameter,
                                                         atk_skillDataScriptObj, atk_skillDetail);
                atkParameterInGame.AddNewBuffs(atkBuffList);
                return;
            }
            List<InGameBuffDetail> defBuffList = InGameBuffDetail.AddBuff(InCaster, InObject.GetCell().GetUnitOnCell(),
                                                    atkParameterInGame, monsterAtk,
                                                    monsterAtkScriptObject, atk_cur_ComplexParameter,
                                                    defParameterInGame, monsterDef,
                                                    monsterDefScriptObject, def_cur_ComplexParameter,
                                                    atk_skillDataScriptObj, atk_skillDetail);
            defParameterInGame.AddNewBuffs(defBuffList);
        }

        private static void AddDebuffList(GridUnit InCaster, GridObject InObject, UnitAbility unitAbility, ParameterInGame atkParameterInGame, MonsterInGame monsterAtk, MonsterDataScriptObj monsterAtkScriptObject, BasicParameters atk_cur_ComplexParameter, ParameterInGame defParameterInGame, MonsterInGame monsterDef, MonsterDataScriptObj monsterDefScriptObject, BasicParameters def_cur_ComplexParameter, SkillDataScriptObj atk_skillDataScriptObj, Skill.SkillDetail atk_skillDetail, float a_DistanceCorrection, float a_RemainingStaminaCorrection)
        {
            if (unitAbility.m_DeBuffSeft)
            {
                List<InGameDeBuffDetail> atkDeBuffsList = InGameDeBuffDetail.AddDeBuff(InCaster, InObject.GetCell().GetUnitOnCell(),
                                                            atkParameterInGame, monsterAtk,
                                                            monsterAtkScriptObject, atk_cur_ComplexParameter,
                                                            defParameterInGame, monsterDef,
                                                            monsterDefScriptObject, def_cur_ComplexParameter,
                                                            atk_skillDataScriptObj, atk_skillDetail,
                                                            a_DistanceCorrection, a_RemainingStaminaCorrection);
                atkParameterInGame.AddNewDeBuffs(atkDeBuffsList);
                return;
            }
            List<InGameDeBuffDetail> defDeBuffsList = InGameDeBuffDetail.AddDeBuff(InCaster, InObject.GetCell().GetUnitOnCell(),
                                                        atkParameterInGame, monsterAtk,
                                                        monsterAtkScriptObject, atk_cur_ComplexParameter,
                                                        defParameterInGame, monsterDef,
                                                        monsterDefScriptObject, def_cur_ComplexParameter,
                                                        atk_skillDataScriptObj, atk_skillDetail,
                                                        a_DistanceCorrection, a_RemainingStaminaCorrection);
            defParameterInGame.AddNewDeBuffs(defDeBuffsList);
        }
    }
}