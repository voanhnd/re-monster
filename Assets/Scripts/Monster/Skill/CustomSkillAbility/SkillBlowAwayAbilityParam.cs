using Assets.Scripts.Monster.PVE_Monster;
using Assets.Scripts.Monster.Trait;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill.CustomSkillAbility
{
    [CreateAssetMenu(fileName = "SkillBlowAwayAbilityParam", menuName = "TurnBasedTools/Custom Ability/Parameters/ Create SkillBlowAwayAbilityParam", order = 1)]
    public class SkillBlowAwayAbilityParam : AbilityParam
    {
        public int m_Distance;

        public override void ApplyTo(GridUnit InCaster, ILevelCell InCell, ILevelCell selectedCell, UnitAbility unitAbility)
        {
            Debug.Log("InCell = " + InCell.name);

            GridUnit TargetUnit = InCell.GetUnitOnCell();

            ParameterInGame atkParameterInGame = InCaster.GameMonster.InGameParameter;
            MonsterInGame monsterAtk = InCaster.GameMonster;
            MonsterDataScriptObj atk_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterAtk.MonsterInfo.monsterID);
            BasicParameters atk_cur_ComplexParameter = atkParameterInGame.GetCurrentComplexParameter();

            MonsterInGame monsterDef = TargetUnit.GetComponent<MonsterInGame>();
            ParameterInGame defParameterInGame = monsterDef.InGameParameter;
            MonsterDataScriptObj def_monsterScriptObject = PlayerStats.Instance.monsterDataManagerScriptObj.GetMonsterDataObj(monsterDef.MonsterInfo.monsterID);
            BasicParameters def_cur_ComplexParameter = defParameterInGame.GetCurrentComplexParameter();

            SkillDataScriptObj atk_skillDataScriptObj = PlayerStats.Instance.skillDataManagerScriptObj.GetSkillDataObj(unitAbility.GetSkillID());

            Debug.Log("Use skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());

            Skill.SkillDetail atk_skillDetail = atk_skillDataScriptObj.Skill.skillDetails[0];

            DebugPopup.RegLog(InCaster.GetTeam());
            DebugPopup.InAttack(true);
            DebugPopup.Log("Caculate Attack ability monster = " + atk_monsterScriptObject.Monster.monsterName);
            DebugPopup.Log("Use Skill = " + atk_skillDataScriptObj.Skill.skillID.ToString());
            DebugPopup.Log("Target = " + def_monsterScriptObject.Monster.monsterName);

            //GetComplex buff and debuff befor add new buff

            InGameBuffDetail atk_inGameBuffDetail = atkParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail atk_inGameDeBuffDetail = atkParameterInGame.GetInGameComplexDeBuff();

            InGameBuffDetail def_InGameBuffDetail = defParameterInGame.GetInGameComplexBuff();
            InGameDeBuffDetail def_InGameDeBuffDetail = defParameterInGame.GetInGameComplexDeBuff();

            //End 

            #region ATK Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("ATK Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo atk_innateTraitInfo = atkParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {atk_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in atkParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo = atkParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo atk_farmTraitInfo = atkParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {atk_farmTraitInfo.farmTraitType}");

            #endregion ATK trait

            #region DEF Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("DEF Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo def_innateTraitInfo = defParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {def_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in defParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo = defParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo def_farmTraitInfo = defParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {def_farmTraitInfo.farmTraitType}");

            #endregion DEF trait

            //Basic parameter

            float atk_str = Mathf.Clamp(atk_cur_ComplexParameter.strenght + atk_inGameBuffDetail.STR_Inscrease + atk_inGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float atk_int = Mathf.Clamp(atk_cur_ComplexParameter.intelligent + atk_inGameBuffDetail.INT_Inscrease + atk_inGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float atk_dex = Mathf.Clamp(atk_cur_ComplexParameter.dexterity + atk_inGameBuffDetail.DEX_Inscrease + atk_inGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float atk_agi = Mathf.Clamp(atk_cur_ComplexParameter.agility + atk_inGameBuffDetail.AGI_Inscrease + atk_inGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float atk_vit = Mathf.Clamp(atk_cur_ComplexParameter.vitality + atk_inGameBuffDetail.VIT_Inscrease + atk_inGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            float def_str = Mathf.Clamp(def_cur_ComplexParameter.strenght + def_InGameBuffDetail.STR_Inscrease + def_InGameDeBuffDetail.STR_Decrease, 0, float.MaxValue);
            float def_int = Mathf.Clamp(def_cur_ComplexParameter.intelligent + def_InGameBuffDetail.INT_Inscrease + def_InGameDeBuffDetail.INT_Decrease, 0, float.MaxValue);
            float def_dex = Mathf.Clamp(def_cur_ComplexParameter.dexterity + def_InGameBuffDetail.DEX_Inscrease + def_InGameDeBuffDetail.DEX_Decrease, 0, float.MaxValue);
            float def_agi = Mathf.Clamp(def_cur_ComplexParameter.agility + def_InGameBuffDetail.AGI_Inscrease + def_InGameDeBuffDetail.AGI_Decrease, 0, float.MaxValue);
            float def_vit = Mathf.Clamp(def_cur_ComplexParameter.vitality + def_InGameBuffDetail.VIT_Inscrease + def_InGameDeBuffDetail.VIT_Decrease, 0, float.MaxValue);

            //End basicParameter

            //Distance correction
            float a_DistanceCorrection = MonsterBattle.Get_Caculated_Distance_Correction(InCaster, InCell.GetUnitOnCell(), selectedCell, unitAbility, atk_skillDetail.occurrenceType);

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //Caculate stamina

            DebugPopup.Log("Attacker current Stamina = " + atkParameterInGame.GetCurrentStamina());
            float consumedStaminaTraitCorrection = atk_innateTraitInfo.ST_Consumption_Correction;
            float consumedStamina = MonsterBattle.Get_Stamina_Consumption(atk_skillDetail.consumedST, consumedStaminaTraitCorrection);
            float a_RemainingStaminaCorrection = MonsterBattle.Get_Stamina_Performance_Improvement(atkParameterInGame.GetCurrentStamina(), consumedStamina);
            Debug.Log("a_RemainingStaminaCorrection = " + a_RemainingStaminaCorrection);

            //End Caculate stamina

            /////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //Caculate Acuracy

            //it must be converted to 100% = 1f
            float a_SkillHitRate = atk_skillDetail.hit / 100f;

            bool isHit = MonsterBattle.Get_Accuracy_IsHit
                (
                    atkParameterInGame, defParameterInGame,
                    atk_inGameBuffDetail, def_InGameBuffDetail,
                    atk_inGameDeBuffDetail, def_InGameDeBuffDetail,
                    atk_innateTraitInfo, def_innateTraitInfo,
                    atk_acquiredTraitInfo, def_acquiredTraitInfo,
                    atk_farmTraitInfo, def_farmTraitInfo,
                    a_SkillHitRate, a_DistanceCorrection, a_RemainingStaminaCorrection, atk_dex,
                    def_agi
                );

            DebugPopup.Log("Hit = " + isHit);

            if (TargetUnit && InCaster && isHit)
            {
                //CompassDir PushDirection = InCaster.GetCell().GetDirectionToAdjacentCell( InCell );

                ILevelCell targetCell = TargetUnit.GetCell();
                Debug.Log("targetCell = " + targetCell.name);

                CompassDir PushDirection = GetDirectionToTarget(InCaster, InCaster.GetCell(), unitAbility.GetRadius(), unitAbility.DoesAllowBlocked(), unitAbility.GetEffectedTeam(), targetCell);
                Debug.Log("Push dir = " + PushDirection.ToString());

                ILevelCell dirCell = targetCell;

                Debug.Log("m_Distance = " + m_Distance);

                for (int i = 0; i < m_Distance; i++)
                {
                    if (dirCell)
                    {
                        Debug.Log("dirCell = " + dirCell.name);

                        dirCell = dirCell.GetAdjacentCell(PushDirection);
                    }

                    if (dirCell && dirCell.IsCellAccesible())
                    {
                        targetCell = dirCell;
                    }
                }

                TargetUnit.MoveTo(targetCell);
            }
            else
            {
                defParameterInGame.MissHit();
            }
        }

        public override string GetAbilityInfo()
        {
            return "Push Back: " + m_Distance.ToString() + " Space" + ((m_Distance > 1) ? "s" : "");
        }

        CompassDir GetDirectionToTarget(GridUnit InCaster, ILevelCell InCell, int InRange, bool bAllowBlocked, GameTeam m_EffectedTeam, ILevelCell targetCell)
        {
            List<ILevelCell> cells = new List<ILevelCell>();

            if (InCell && InRange > 0)
            {
                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.N, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.N.ToString());
                    return CompassDir.N;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.E, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.E.ToString());
                    return CompassDir.E;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NE, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.NE.ToString());
                    return CompassDir.NE;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.NW, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.NW.ToString());
                    return CompassDir.NW;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SE, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.SE.ToString());
                    return CompassDir.SE;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.S, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.S.ToString());
                    return CompassDir.S;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.SW, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.SW.ToString());
                    return CompassDir.SW;
                }

                cells.Clear();
                cells.AddRange(GetCellsInDirection(InCell, InRange, CompassDir.W, bAllowBlocked, m_EffectedTeam));
                if (isTargetOnDirection(cells, targetCell))
                {
                    Debug.Log("Target cell on direction = " + CompassDir.W.ToString());
                    return CompassDir.W;
                }
            }

            Debug.Log("Can not caculate direction");

            return CompassDir.N;
        }

        List<ILevelCell> GetCellsInDirection(ILevelCell StartCell, int InRange, CompassDir Dir, bool bAllowBlocked, GameTeam m_EffectedTeam)
        {
            List<ILevelCell> cells = new List<ILevelCell>();

            if (InRange > 0)
            {
                ILevelCell CurserCell = StartCell.GetAdjacentCell(Dir);

                int Count = 0;
                while (CurserCell)
                {
                    if (CurserCell.IsBlocked() && !bAllowBlocked)
                    {
                        break;
                    }

                    GridObject gridObj = CurserCell.GetObjectOnCell();
                    if (gridObj)
                    {
                        if (m_EffectedTeam == GameTeam.None)
                        {
                            break;
                        }

                        GameTeam ObjAffinity = GameManager.GetTeamAffinity(gridObj.GetTeam(), StartCell.GetCellTeam());
                        if (ObjAffinity == GameTeam.Blue && m_EffectedTeam == GameTeam.Red)
                        {
                            break;
                        }
                    }

                    cells.Add(CurserCell);
                    CurserCell = CurserCell.GetAdjacentCell(Dir);
                    Count++;
                    if (InRange != -1 && Count >= InRange)
                    {
                        break;
                    }
                }
            }

            return cells;
        }

        bool isTargetOnDirection(List<ILevelCell> directionCellList, ILevelCell targetCell)
        {
            foreach (ILevelCell cell in directionCellList)
            {
                if (cell == targetCell)
                {
                    Debug.Log("Target cell on direction cell = " + cell.name);

                    return true;
                }
            }

            return false;
        }
    }
}
