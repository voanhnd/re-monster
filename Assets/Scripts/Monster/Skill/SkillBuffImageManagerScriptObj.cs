using Assets.Scripts.UI.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill
{
    [CreateAssetMenu(fileName = "SkillBuffImageManagerScriptObj", menuName = "ScriptableObjects/Monster/SkillBuffImageManagerScriptObj", order = 1)]
    public class SkillBuffImageManagerScriptObj : ScriptableObject
    {
        [System.Serializable]
        public class BuffImage
        {
            public MonsterSpecialBufferType buffType;

            public Sprite sprite;
        }

        [System.Serializable]
        public class DeBuffImage
        {
            public MonsterSpecialDeBufferType deBuffType;

            public Sprite sprite;
        }

        [System.Serializable]
        public class BuffDescription
        {
            public MonsterSpecialBufferType buffType;

            public string description;
        }

        [System.Serializable]
        public class DeBuffDescription
        {
            public MonsterSpecialDeBufferType deBuffType;

            public string description;
        }

        [System.Serializable]
        public class BuffEffectsData
        {
            [SerializeField] private MonsterSpecialBufferType buffType;
            [SerializeField] private string buffJPfName;
            [SerializeField, Multiline] private string buffDescription;
            [Header("To convert runs correctly need to set the param to {skill} to be affected")]
            [SerializeField, Multiline] private string buffSummary;
            [SerializeField] private Sprite buffSprite;
            [Header("For buffs base on skills")]
            [SerializeField] private bool isSkillDependent = false;
            [SerializeField] private bool isMentalBuff = false;

            public MonsterSpecialBufferType BuffType => buffType;
            public string BuffJPName => buffJPfName;
            public string BuffDescription => buffDescription;
            public string BuffSummary => buffSummary;
            public bool IsSkillDependent => isSkillDependent;
            public Sprite BuffSprite => buffSprite;
            public bool IsMentalBuff => isMentalBuff;
        }

        [System.Serializable]
        public class DebuffEffectsData
        {
            [SerializeField] private MonsterSpecialDeBufferType debuffType;
            [SerializeField] private string debuffJPfName;
            [SerializeField, Multiline] private string debuffDescription;
            [Header("To convert runs correctly need to set the param to {skill} to be affected")]
            [SerializeField, Multiline] private string debuffSummary;
            [SerializeField] private Sprite debuffSprite;
            [Header("For buffs base on skills")]
            [SerializeField] private bool isSkillDependent = false;
            [SerializeField] private bool isMentalDebuff = false;

            public MonsterSpecialDeBufferType DebuffType => debuffType;
            public string DebuffJPName => debuffJPfName;
            public string DebuffDescription => debuffDescription;
            public string DebuffSummary => debuffSummary;
            public Sprite DebuffSprite => debuffSprite;
            public bool IsSkillDependent => isSkillDependent;
            public bool IsMentalDebuff => isMentalDebuff;
        }

        [SerializeField] private List<BuffEffectsData> buffDataList = new();
        [SerializeField] private List<DebuffEffectsData> debuffDataList = new();

        private Dictionary<MonsterSpecialBufferType, BuffEffectsData> buffDataDictionary = new();
        private Dictionary<MonsterSpecialDeBufferType, DebuffEffectsData> debuffDataDictionary = new();

        public List<BuffImage> buffImages = new();
        public List<DeBuffImage> deBuffImages = new();
        private Dictionary<MonsterSpecialBufferType, BuffImage> buffImagesDictionary = new();
        private Dictionary<MonsterSpecialDeBufferType, DeBuffImage> deBuffImagesDictionary = new();

        public List<BuffDescription> buffDescriptions = new();
        public List<DeBuffDescription> deBuffDescriptions = new();

        private Dictionary<MonsterSpecialBufferType, BuffDescription> buffDescriptionDictionary = new();
        private Dictionary<MonsterSpecialDeBufferType, DeBuffDescription> deBuffDescriptionDictionary = new();

        public BuffImage GetBuffSprite(MonsterSpecialBufferType buffType)
        {

            if (!buffImagesDictionary.TryGetValue(buffType, out BuffImage buffImage))
            {
                // the key isn't in the dictionary.
                return buffImage; // or whatever you want to do
            }

            return buffImage;
        }

        public BuffDescription GetBuffDescription(MonsterSpecialBufferType buffType)
        {

            if (!buffDescriptionDictionary.TryGetValue(buffType, out BuffDescription buffDescription))
            {
                // the key isn't in the dictionary.
                return buffDescription; // or whatever you want to do
            }

            return buffDescription;
        }

        public DeBuffImage GetDeBuffSprite(MonsterSpecialDeBufferType buffType)
        {
            if (!deBuffImagesDictionary.TryGetValue(buffType, out DeBuffImage deBuffImage))
            {
                return deBuffImage;
            }

            return deBuffImage;
        }

        public DeBuffDescription GetDeBuffDescription(MonsterSpecialDeBufferType buffType)
        {
            if (!deBuffDescriptionDictionary.TryGetValue(buffType, out DeBuffDescription deBuffDescription))
            {
                return deBuffDescription;
            }

            return deBuffDescription;
        }

        public void CreateDictionary()
        {
            buffImagesDictionary.Clear();
            buffImagesDictionary = buffImages.ToDictionary(x => x.buffType);

            buffDescriptionDictionary.Clear();
            buffDescriptionDictionary = buffDescriptions.ToDictionary(x => x.buffType);

            deBuffImagesDictionary.Clear();
            deBuffImagesDictionary = deBuffImages.ToDictionary(x => x.deBuffType);

            deBuffDescriptionDictionary.Clear();
            deBuffDescriptionDictionary = deBuffDescriptions.ToDictionary(x => x.deBuffType);

            buffDataDictionary.Clear();
            buffDataDictionary = buffDataList.ToDictionary(x => x.BuffType);

            debuffDataDictionary.Clear();
            debuffDataDictionary = debuffDataList.ToDictionary(x => x.DebuffType);
        }

        public DebuffEffectsData GetDebuffEffectData(MonsterSpecialDeBufferType debuffType)
        {
            if (debuffDataDictionary.TryGetValue(debuffType, out DebuffEffectsData result))
            {
                return result;
            }
            return null;
        }

        public BuffEffectsData GetBuffEffectData(MonsterSpecialBufferType debuffType)
        {
            if (buffDataDictionary.TryGetValue(debuffType, out BuffEffectsData result))
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// <span>To convert runs correctly need to set the param to {skill} to be affected</span>
        /// </summary>
        /// <param name="summaryString"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SkillSummaryConvert(string summaryString, float value)
        {
            string replaceString = summaryString.Replace("{skill}", SpecialFloatToInt.Convert(value).ToString());
            return replaceString;
        }
    }
}