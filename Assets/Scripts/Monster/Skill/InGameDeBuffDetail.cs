﻿using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.PVE_Monster;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill
{
    [System.Serializable]
    public class InGameDeBuffDetail
    {
        public MonsterSpecialDeBufferType monsterSpecialDeBufferType;

        //public bool caculateOnEndTurn = false;

        public string avoidMonsterID = string.Empty;

        public int deBuffTurn = 0;

        public int avoidTurn = 0;

        public bool canNotUseSkill = false;

        public bool attackFriendly = false;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float atk_Damage_Correction = 0f;
        public float def_Damage_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Health_Heal_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Health_Turn_Dam_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Health_Recovery_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Stamina_Recovery_Correction = 0f;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Stamina_Turn_Dam_Correction;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float Stamina_Skill_Correction;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float speed_Decrease_Correction = 0f;

        public int move_Decrease = 0;

        /// <summary>
        /// this value must be converted 100% = 1f;
        /// </summary>
        public float atk_AccuracyRate_Correction = 0f;
        public float def_AccuracyRate_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float HP_Decrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float STR_Decrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float INT_Decrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float DEX_Decrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float AGI_Decrease_Correction = 0f;

        ///// <summary>
        ///// this value must be converted 100% = 1f;
        ///// </summary
        //public float VIT_Decrease_Correction = 0f;

        /// <summary>
        /// this values are fixed number
        /// </summary
        public float HP_Decrease = 0f;

        public float STR_Decrease = 0f;

        public float INT_Decrease = 0f;

        public float DEX_Decrease = 0f;

        public float AGI_Decrease = 0f;

        public float VIT_Decrease = 0f;

        public float EffectValue { get; private set; }

        public InGameDeBuffDetail()
        {
            //caculateOnEndTurn = false;

            avoidMonsterID = string.Empty;

            deBuffTurn = 0;

            avoidTurn = 0;

            canNotUseSkill = false;

            attackFriendly = false;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            atk_Damage_Correction = 0f;
            def_Damage_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            Health_Heal_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            Health_Turn_Dam_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            Health_Recovery_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            Stamina_Recovery_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            Stamina_Turn_Dam_Correction = 0f;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            speed_Decrease_Correction = 0f;

            move_Decrease = 0;

            /// <summary>
            /// this value must be converted 100% = 1f;
            /// </summary>
            atk_AccuracyRate_Correction = 0f;
            def_AccuracyRate_Correction = 0f;

            //HP_Decrease_Correction = 0f;
            //STR_Decrease_Correction = 0f;
            //INT_Decrease_Correction = 0f;
            //DEX_Decrease_Correction = 0f;
            //AGI_Decrease_Correction = 0f;
            //VIT_Decrease_Correction = 0f;

            /// <summary>
            /// this values are fixed number
            /// </summary
            HP_Decrease = 0f;
            STR_Decrease = 0f;
            INT_Decrease = 0f;
            DEX_Decrease = 0f;
            AGI_Decrease = 0f;
            VIT_Decrease = 0f;
        }

        public static bool IsPsychic(MonsterSpecialDeBufferType monsterSpecialDeBufferType)
        {
            switch (monsterSpecialDeBufferType)
            {
                case MonsterSpecialDeBufferType.激怒_Rage:
                    {
                        return true;
                    }
                case MonsterSpecialDeBufferType.恐怖_Fear:
                    {
                        return true;
                    }
                case MonsterSpecialDeBufferType.混乱_Confusion:
                    {
                        return true;
                    }
                case MonsterSpecialDeBufferType.魅了_Charm:
                    {
                        return true;
                    }
            }

            return false;
        }

        public static InGameDeBuffDetail GetDeBuffComplex(List<InGameDeBuffDetail> inGameDeBuffDetails)
        {
            InGameDeBuffDetail complexBuff = new InGameDeBuffDetail();

            foreach (InGameDeBuffDetail debuff in inGameDeBuffDetails)
            {
                complexBuff.avoidMonsterID = debuff.avoidMonsterID;

                if (complexBuff.canNotUseSkill == false)
                {
                    complexBuff.canNotUseSkill = debuff.canNotUseSkill;
                }

                if (complexBuff.attackFriendly == false)
                {
                    complexBuff.attackFriendly = debuff.attackFriendly;
                }

                complexBuff.atk_Damage_Correction = debuff.atk_Damage_Correction;
                complexBuff.def_Damage_Correction = debuff.def_Damage_Correction;

                complexBuff.Health_Heal_Correction = debuff.Health_Heal_Correction;

                complexBuff.Health_Turn_Dam_Correction = debuff.Health_Turn_Dam_Correction;

                complexBuff.Health_Recovery_Correction = debuff.Health_Recovery_Correction;

                complexBuff.Stamina_Recovery_Correction = debuff.Stamina_Recovery_Correction;

                complexBuff.Stamina_Turn_Dam_Correction = debuff.Stamina_Turn_Dam_Correction;

                complexBuff.speed_Decrease_Correction = debuff.speed_Decrease_Correction;

                complexBuff.move_Decrease = debuff.move_Decrease;

                complexBuff.atk_AccuracyRate_Correction = debuff.atk_AccuracyRate_Correction;

                complexBuff.def_AccuracyRate_Correction = debuff.def_AccuracyRate_Correction;

                complexBuff.HP_Decrease = debuff.HP_Decrease;
                complexBuff.STR_Decrease = debuff.def_AccuracyRate_Correction;
                complexBuff.INT_Decrease = debuff.INT_Decrease;
                complexBuff.DEX_Decrease = debuff.DEX_Decrease;
                complexBuff.AGI_Decrease = debuff.AGI_Decrease;
                complexBuff.VIT_Decrease = debuff.VIT_Decrease;
            }

            return complexBuff;
        }

        public static List<InGameDeBuffDetail> AddDeBuff(
            GridUnit InCaster, GridUnit InTarget,
            ParameterInGame atkParameterInGame, MonsterInGame monsterAtk, MonsterDataScriptObj atk_monsterScriptObject, BasicParameters atk_cur_ComplexParameter,
            ParameterInGame defParameterInGame, MonsterInGame monsterDef, MonsterDataScriptObj def_monsterScriptObject, BasicParameters def_cur_ComplexParameter,
            SkillDataScriptObj atk_skillDataScriptObj, Skill.SkillDetail atk_skillDetail,
            float a_DistanceCorrection, float a_RemainStaminaCorrection
            )
        {
            List<InGameDeBuffDetail> newDeBuffList = new List<InGameDeBuffDetail>();

            #region ATK Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("ATK Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo atk_innateTraitInfo = atkParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {atk_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in atkParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo atk_acquiredTraitInfo = atkParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo atk_farmTraitInfo = atkParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {atk_farmTraitInfo.farmTraitType}");

            #endregion ATK trait

            #region DEF Trait

            DebugPopup.Log("//////////");
            DebugPopup.Log("DEF Trait");
            DebugPopup.Log("__________");
            InnateTrait.InnateTraitInfo def_innateTraitInfo = defParameterInGame.GetInnateTrait();
            DebugPopup.Log($"InnateTrait {def_innateTraitInfo.innateTraitType}");

            foreach (AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo in defParameterInGame.GetAcquiredTraits())
            {
                DebugPopup.Log($"AcquiredTrait {acquiredTraitInfo.acquiredTraitType}");
            }
            AcquiredTrait.AcquiredTraitInfo def_acquiredTraitInfo = defParameterInGame.GetAcquiredTraitComplex();

            FarmTrait.FarmTraitInfo def_farmTraitInfo = defParameterInGame.GetFarmTrait();
            DebugPopup.Log($"FarmTrait {def_farmTraitInfo.farmTraitType}");

            #endregion DEF trait

            DebugPopup.Log($"//////////");
            DebugPopup.Log($"Add DeBuff");

            foreach (Skill.SkillDeBuffDetail skillBuffDetail in atk_skillDetail.skillDeBuffDetails)
            {
                InGameDeBuffDetail inGameDeBuffDetail = null;

                inGameDeBuffDetail = DeBuff_Poison(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.毒_Poison, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_HighPoison(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.猛毒_Deadly_Poison, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Weakness(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.衰弱_Weakness, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Blind(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.盲目_Blindness, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_SlownDown(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.加速_Slowness, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Paralysis(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.麻痺_Paralysis, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Sleep(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.睡眠_Sleep, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Decrease_STR(InCaster, InTarget, skillBuffDetail, def_cur_ComplexParameter);
                if (inGameDeBuffDetail != null)
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Decrease_INT(InCaster, InTarget, skillBuffDetail, def_cur_ComplexParameter);
                if (inGameDeBuffDetail != null)
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Decrease_DEX(InCaster, InTarget, skillBuffDetail, def_cur_ComplexParameter);
                if (inGameDeBuffDetail != null)
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Decrease_AGI(InCaster, InTarget, skillBuffDetail, def_cur_ComplexParameter);
                if (inGameDeBuffDetail != null)
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Decrease_VIT(InCaster, InTarget, skillBuffDetail, def_cur_ComplexParameter);
                if (inGameDeBuffDetail != null)
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Anger(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.激怒_Rage, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Fear(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.恐怖_Fear, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Chaos(InCaster, InTarget, skillBuffDetail, monsterAtk.MonsterInfo.monsterID);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.混乱_Confusion, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

                inGameDeBuffDetail = DeBuff_Enchantment(InCaster, InTarget, skillBuffDetail);
                if (inGameDeBuffDetail != null
                    && IsBuffed(atk_skillDataScriptObj, atk_skillDetail, MonsterSpecialDeBufferType.魅了_Charm, atk_innateTraitInfo, atk_acquiredTraitInfo, atk_farmTraitInfo,
                                a_DistanceCorrection, a_RemainStaminaCorrection,
                                def_monsterScriptObject, def_innateTraitInfo, def_acquiredTraitInfo, def_farmTraitInfo))
                {
                    newDeBuffList.Add(inGameDeBuffDetail);
                    continue;
                }

            }

            return newDeBuffList;
        }

        public static bool IsBuffed(SkillDataScriptObj atk_skillDataScriptObj, Skill.SkillDetail atk_skillDetail, MonsterSpecialDeBufferType monsterSpecialDeBufferType,
                                    InnateTrait.InnateTraitInfo a_innateTraitInfo, AcquiredTrait.AcquiredTraitInfo a_acquiredTraitInfo, FarmTrait.FarmTraitInfo a_farmTraitInfo,
                                    float a_DistanceCorrection, float a_RemainStaminaCorrection,
                                    MonsterDataScriptObj def_monsterDataScriptObj,
                                    InnateTrait.InnateTraitInfo d_innateTraitInfo, AcquiredTrait.AcquiredTraitInfo d_acquiredTraitInfo, FarmTrait.FarmTraitInfo d_farmTraitInfo)
        {
            DebugPopup.Log("//////////");
            DebugPopup.Log($"Debuff type = {monsterSpecialDeBufferType}");
            float a_DebuffSuccessRate = 0f;
            for (int i = 0; i < atk_skillDetail.skillDeBuffDetails.Count; i++)
            {
                Skill.SkillDeBuffDetail detail = atk_skillDetail.skillDeBuffDetails[i];
                if (detail.monsterSpecialDeBufferType == monsterSpecialDeBufferType)
                {
                    a_DebuffSuccessRate = detail.value / 100f;
                }
            }

            float a_traitCorrection = a_innateTraitInfo.DeBuff_Success_Rate;

            float d_StateChangeResistance = Skill.GetAttributeEndurance(def_monsterDataScriptObj, atk_skillDetail.attributeType);

            d_StateChangeResistance += Skill.ElementCorrectionByTrait(d_innateTraitInfo, a_acquiredTraitInfo, a_farmTraitInfo, atk_skillDetail.attributeType);

            if (IsPsychic(monsterSpecialDeBufferType))
            {
                d_StateChangeResistance += d_innateTraitInfo.Psychich_Debuff_Resistance + d_acquiredTraitInfo.psychic_Debuff_Resistance + d_farmTraitInfo.spirit_Debuff_Resistance;
            }
            else
            {
                d_StateChangeResistance += d_innateTraitInfo.Physical_Debuff_Resistance + d_acquiredTraitInfo.physic_Debuff_Resistance + d_farmTraitInfo.physical_Debuff_Resistance;
            }

            float debuffRate = MonsterBattle.Get_Debuff_Success_Rate(a_DebuffSuccessRate, a_DistanceCorrection, a_traitCorrection, a_RemainStaminaCorrection, d_StateChangeResistance);

            float percentageDebuffRate = Mathf.Clamp(debuffRate * 100f, 0f, 100f);

            float ranDeBuffVal = Random.Range(0f, 100f);

            return ranDeBuffVal <= percentageDebuffRate;
        }

        public static InGameDeBuffDetail DeBuff_Poison(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.毒_Poison)
                return null;

            //  毒 Độc
            //  "そのMonsterのターン毎に最大HPの5%のダメージ、3ターン継続
            //  Damage 5 % HP tối đã mỗi turn của Monster đó, kéo dài 3 turrn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.毒_Poison;

            inGameDeBuffDetail.Health_Turn_Dam_Correction = -0.05f;

            inGameDeBuffDetail.deBuffTurn = 3;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_HighPoison(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.猛毒_Deadly_Poison)
                return null;

            //  猛毒 Kịch độc
            //  "そのMonsterのターン毎に最大HPの10%のダメージ、3ターン継続
            //  Damage 10 % HP tối đã mỗi turn của Monster đó, kéo dài 3 turrn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.猛毒_Deadly_Poison;

            inGameDeBuffDetail.Health_Turn_Dam_Correction = -0.1f;

            inGameDeBuffDetail.deBuffTurn = 3;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Weakness(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.衰弱_Weakness)
                return null;

            //  衰弱 Kiệt sức, suy nhược
            //  "Monsterの番になるごとに、スタミナ回復後にスタミナ10%減少
            //  スキルによるHP、スタミナ回復20 % 減少、2ターン継続
            //  Giảm 10 % thể lực sau khi phục hồi mỗi khi đến phiên cuar Monster
            //  Tuỳ vào skill thì giảm HP và khả năng phục hồi 20 %, kéo dàu 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.衰弱_Weakness;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;

            inGameDeBuffDetail.Stamina_Turn_Dam_Correction = -0.1f;

            inGameDeBuffDetail.Health_Turn_Dam_Correction = skillValue;

            inGameDeBuffDetail.Health_Recovery_Correction = -0.2f;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Blind(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.盲目_Blindness)
                return null;

            //  盲目 Mù mắt
            //  "命中率と回避率-15%、1ターン継続
            //  Chính xác và tỷ lệ né tránh - 15 %, kéo dài 1 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.盲目_Blindness;

            inGameDeBuffDetail.atk_AccuracyRate_Correction = -0.15f;

            inGameDeBuffDetail.def_AccuracyRate_Correction = -0.15f;

            inGameDeBuffDetail.deBuffTurn = 1;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_SlownDown(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.加速_Slowness)
                return null;

            //  鈍化 Chậm lại
            //  "スピード-10%、移動力-1（移動力の下限は1）、1ターン継続
            //  Tốc độ -10 %, di chuyển -1(di chuyển tối thiểu là 1), kéo dài 1 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.加速_Slowness;

            inGameDeBuffDetail.speed_Decrease_Correction = -0.1f;

            inGameDeBuffDetail.move_Decrease = -1;

            inGameDeBuffDetail.deBuffTurn = 1;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Paralysis(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.麻痺_Paralysis)
                return null;

            //  麻痺 Bại liệt
            //  "1ターン行動不能
            //  Không thể hành động 1 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.麻痺_Paralysis;

            inGameDeBuffDetail.avoidTurn = 1;

            inGameDeBuffDetail.deBuffTurn = 1;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Sleep(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.睡眠_Sleep)
                return null;

            //  睡眠 Ngủ
            //  "1ターン行動不能、スタミナ回復+50%、被ダメージ時解除
            //  Không thể hành động 1 turn, Phục hồi thể lực +50 % và huỷ bỏ khi nhận trấn thương"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.睡眠_Sleep;

            inGameDeBuffDetail.Stamina_Recovery_Correction = 0.5f;

            inGameDeBuffDetail.avoidTurn = 1;

            inGameDeBuffDetail.deBuffTurn = 1;

            return inGameDeBuffDetail;

            //Debug.Log("DeBuff_Sleep");
        }

        public static InGameDeBuffDetail DeBuff_Decrease_STR(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, BasicParameters def_complexParameters)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.Decrease_STR)
                return null;

            //  STR減少 Giảm STR

            DebugPopup.Log($"Debuff type = {skillDeBuffDetail.monsterSpecialDeBufferType}");

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.Decrease_STR;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;
            inGameDeBuffDetail.EffectValue = skillDeBuffDetail.value;

            inGameDeBuffDetail.STR_Decrease = -def_complexParameters.strenght * skillValue;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Decrease_INT(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, BasicParameters def_complexParameters)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.Decrease_INT)
                return null;

            //  INT減少 Giảm INT

            DebugPopup.Log($"Debuff type = {skillDeBuffDetail.monsterSpecialDeBufferType}");

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.Decrease_INT;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;
            inGameDeBuffDetail.EffectValue = skillDeBuffDetail.value;

            inGameDeBuffDetail.INT_Decrease = -def_complexParameters.intelligent * skillValue;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Decrease_DEX(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, BasicParameters def_complexParameters)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.Decrease_DEX)
                return null;

            //  DEX減少 Giảm DEX

            DebugPopup.Log($"Debuff type = {skillDeBuffDetail.monsterSpecialDeBufferType}");

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.Decrease_DEX;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;
            inGameDeBuffDetail.EffectValue = skillDeBuffDetail.value;

            inGameDeBuffDetail.DEX_Decrease = -def_complexParameters.dexterity * skillValue;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Decrease_AGI(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, BasicParameters def_complexParameters)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.Decrease_AGI)
                return null;

            //  AGI減少 Giảm AGI

            DebugPopup.Log($"Debuff type = {skillDeBuffDetail.monsterSpecialDeBufferType}");

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.Decrease_AGI;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;
            inGameDeBuffDetail.EffectValue = skillDeBuffDetail.value;

            inGameDeBuffDetail.AGI_Decrease = -def_complexParameters.agility * skillValue;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Decrease_VIT(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, BasicParameters def_complexParameters)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.Decrease_VIT)
                return null;

            //  VIT減少 Giảm VIT

            DebugPopup.Log($"Debuff type = {skillDeBuffDetail.monsterSpecialDeBufferType}");

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.Decrease_VIT;

            //if value is percentage it must be convected to 100f = 1f;
            float skillValue = skillDeBuffDetail.value / 100f;
            inGameDeBuffDetail.EffectValue = skillDeBuffDetail.value;

            inGameDeBuffDetail.VIT_Decrease = -def_complexParameters.vitality * skillValue;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Anger(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.激怒_Rage)
                return null;

            //  激怒 Mất bình tĩnh
            //  "与ダメージ+10%、被ダメージ+10%、消費スタミナ+10%、3ターン継続
            //  +10 % trấn thương gây ra, +10 % trấn thương nhận vào, +10 % thể lực tiêu hao, kéo dài 3 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.激怒_Rage;

            inGameDeBuffDetail.atk_Damage_Correction = -0.1f;
            inGameDeBuffDetail.def_Damage_Correction = -0.1f;
            inGameDeBuffDetail.Stamina_Skill_Correction = 0.1f;

            inGameDeBuffDetail.deBuffTurn = 3;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Fear(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.恐怖_Fear)
                return null;

            //  恐怖 Sợ hãi
            //  "スキル使用不可、2ターン継続
            //  Không thể sử dụng skill, kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.恐怖_Fear;

            inGameDeBuffDetail.canNotUseSkill = true;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Chaos(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail, string monsterID)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.混乱_Confusion)
                return null;

            //  混乱 Hỗn loạn
            // "敵Monsterが遠くになるように移動する（逃げる）2ターン継続
            //  Di chuyển để Monster đối thủ ở xa(trốn thoát), kéo dài 2 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.混乱_Confusion;

            inGameDeBuffDetail.avoidMonsterID = monsterID;

            inGameDeBuffDetail.deBuffTurn = 2;

            return inGameDeBuffDetail;
        }

        public static InGameDeBuffDetail DeBuff_Enchantment(GridUnit InCaster, GridUnit InTarget, Skill.SkillDeBuffDetail skillDeBuffDetail)
        {
            if (skillDeBuffDetail.monsterSpecialDeBufferType != MonsterSpecialDeBufferType.魅了_Charm)
                return null;

            //  魅了 Mê hoặc
            // "最も近い味方Monsterにランダムでスキルを使う 1ターン継続
            //  Sử dụng random skill cho Monster đồng minh gần nhất, kéo dài 1 turn"

            ParameterInGame atkParameterInGame = InCaster.GetComponent<ParameterInGame>();
            MonsterInGame monsterAtk = InCaster.GetComponent<MonsterInGame>();

            ParameterInGame defParameterInGame = InTarget.GetComponent<ParameterInGame>();
            MonsterInGame monsterDef = InTarget.GetComponent<MonsterInGame>();

            InGameDeBuffDetail inGameDeBuffDetail = new InGameDeBuffDetail();

            inGameDeBuffDetail.monsterSpecialDeBufferType = MonsterSpecialDeBufferType.魅了_Charm;

            inGameDeBuffDetail.attackFriendly = true;

            inGameDeBuffDetail.deBuffTurn = 1;

            return inGameDeBuffDetail;
        }
    }
}