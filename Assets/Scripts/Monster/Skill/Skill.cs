﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill
{
    [Serializable]
    public class Skill
    {
        [Serializable]
        public class SkillBuffDetail
        {
            public MonsterSpecialBufferType monsterSpecialBufferType;

            /// <summary>
            /// <para> this value is percentage 100% </para>
            /// </summary>
            public float value = 0f;

            public SkillBuffDetail()
            {

            }

            public SkillBuffDetail(MonsterSpecialBufferType buffType, float val)
            {
                monsterSpecialBufferType = buffType;

                value = val;
            }

            public SkillBuffDetail Clone()
            {
                SkillBuffDetail skillBuffDetail = new()
                {
                    monsterSpecialBufferType = monsterSpecialBufferType,

                    value = value
                };

                return skillBuffDetail;
            }
        }

        [Serializable]
        public class SkillDeBuffDetail
        {
            public MonsterSpecialDeBufferType monsterSpecialDeBufferType;

            /// <summary>
            /// <para> this value is percentage 100% </para>
            /// </summary>
            public float value = 0f;

            public SkillDeBuffDetail()
            {

            }

            public SkillDeBuffDetail(MonsterSpecialDeBufferType deBuffType, float val)
            {
                monsterSpecialDeBufferType = deBuffType;

                value = val;
            }

            public SkillDeBuffDetail Clone()
            {
                SkillDeBuffDetail skillDeBuffDetail = new()
                {
                    monsterSpecialDeBufferType = monsterSpecialDeBufferType,

                    value = value
                };

                return skillDeBuffDetail;
            }
        }

        [Serializable]
        public class SkillTrainingAreaCorrection
        {
            public TrainingArea.TrainingArea.TrainingAreaType trainingAreaType;

            /// <summary>
            /// 100% = 100f
            /// </summary>
            public BasicParameters correctionParameter;
        }

        //public enum SkillID
        //{
        //    //P1_XSoldier_P1(ピーワン)
        //    P1_XSoldier_Skill_1, P1_XSoldier_Skill_2, P1_XSoldier_Skill_3, P1_XSoldier_Skill_4, P1_XSoldier_Skill_5, P1_XSoldier_Skill_6,

        //    //Aries_アリーズ
        //    SignWitch_Aries_Skill_1, SignWitch_Aries_Skill_2, SignWitch_Aries_Skill_3, SignWitch_Aries_Skill_4, SignWitch_Aries_Skill_5, SignWitch_Aries_Skill_6,

        //    Tofu_Skill_1, Tofu_Skill_2, Tofu_Skill_3, Tofu_Skill_4, Tofu_Skill_5, Tofu_Skill_6,

        //    //Golem_ゴーレム
        //    Golem_Original_Skill_1, Golem_Original_Skill_2, Golem_Original_Skill_3, Golem_Original_Skill_4, Golem_Original_Skill_5, Golem_Original_Skill_6,

        //    //Kids Hero_キッズヒーロー
        //    Kidsfight_Hero_Skill_1, Kidsfight_Hero_Skill_2, Kidsfight_Hero_Skill_3, Kidsfight_Hero_Skill_4, Kidsfight_Hero_Skill_5, Kidsfight_Hero_Skill_6,

        //    //Ossan Paradise_おっさんパラダイス
        //    Ossan_01_Skill_1, Ossan_01_Skill_2, Ossan_01_Skill_3, Ossan_01_Skill_4, Ossan_01_Skill_5, Ossan_01_Skill_6,

        //    //Icelime_アイスライム
        //    IceLime_Eien_Skill_1, IceLime_Eien_Skill_2, IceLime_Eien_Skill_3, IceLime_Eien_Skill_4, IceLime_Eien_Skill_5, IceLime_Eien_Skill_6,

        //    None, Defense, BlowAway
        //}

        public string skillID = "";

        #region Enums
        public enum SkillGenType
        {
            STR, INT, 回復_Heal, 補助_Support
        }

        public enum SkillRole
        {
            //アタッカー Attacker
            STR_Close_Attack, STR_Medium_Attack, STR_Long_Attack,
            INT_Close_Attack, INT_Medium_Attack, INT_Long_Attack,
            //タンク Tank
            Defensive_Tank, Dodge_Tank,
            //バッファー Buffer
            Self_Buffer, PT_Buffer,
            //ヒーラー Healer
            Seft_Healer, PT_Healer,
            //デバッファー Debuffer
            Abnormal_Debuffer, Stamina_Debuffer
        }

        public enum AreaOfEffectType
        {
            点_Point, 円形_Circle, 放射_Ray, 直線_Straight, 拡散_Diffusive, X
        }

        public enum OccurrenceType
        {
            指定_designation, 自身_self, 投射_projection, 直線_straight, 貫通_penetrating, 溜め_sink, 設置_installation
        }
        #endregion

        public SkillGenType skillGenType;

        public string skillName = "";

        [TextArea]
        public string Description = "";

        /// <summary>
        /// <para> The parameters required when leveling up are +20% of the parameters required when learning, and +20% each time the level increases.  </para>
        /// </summary>
        public List<float> parametersRequired = new();

        /// <summary>
        /// <para> SkillLearn Correction </para>
        /// </summary>
        public TrainingArea.TrainingArea.TrainingAreaType learnArea;

        /// <summary>
        /// <para> 100%= 100f </para>
        /// </summary>
        [Header("100% = 100f")]
        public float SkillCharacteristicCorrection = 0f;

        /// <summary>
        /// <para> SkillLearn Correction </para>
        /// </summary>
        public BasicParameters skillLearnCorrection = new();

        /// <summary>
        /// <para> The parameters required when leveling up (up to 6) will be +50% of the parameters required for acquisition, and +50% will be +50% each time you level up </para>
        /// </summary>
        public List<BasicParameters> characteristicAcquisitionList = new();

        [Header("100% = 100f")]
        /// <summary>
        /// <para> 100% = 100f </para>
        /// <para> skill level == 0 mean it is not learned </para>
        /// </summary>
        public List<SkillDetail> skillDetails = new();

        [Serializable]
        public class SkillDetail
        {
            public RankTypeEnums rankType;

            public float attackRating = 0f;

            public float recoveryRating = 0f;

            public float supportRating = 0f;

            public float costRating = 0f;

            public float rangeRating = 0f;

            public float cost = 0f;

            public float consumedST = 0f;

            public float delay = 0f;

            public int range = 0;

            public AreaOfEffectType areaOfEffectType;
            public int areaOfEffectRange = 0;

            public OccurrenceType occurrenceType;

            public float hp_dmg = 0f;

            public float st_dmg = 0f;

            /// <summary>
            /// <para> this value is percentage 100% </para>
            /// </summary>
            public float hit = 0f;

            /// <summary>
            /// <para> this value is percentage 100% </para>
            /// </summary>
            public float crt = 0f;

            public float delayDamage = 0f;

            public float hp_recovery = 0f;

            public float st_recovery = 0f;

            public List<SkillBuffDetail> skillBuffDetails = new();
            public List<SkillDeBuffDetail> skillDeBuffDetails = new();

            public List<MonsterRemoveDebuffType> skillDeBuffRelease = new();

            public AttributeType attributeType;

            public int CheckBuffCount(MonsterSpecialBufferType buffType)
            {
                int count = 0;

                for (int i = 0; i < skillBuffDetails.Count; i++)
                {
                    SkillBuffDetail b = skillBuffDetails[i];
                    if (buffType == b.monsterSpecialBufferType)
                    {
                        count++;
                    }
                }

                return count;
            }

            public int CheckDeBuffCount(MonsterSpecialDeBufferType buffType)
            {
                int count = 0;

                for (int i = 0; i < skillDeBuffDetails.Count; i++)
                {
                    SkillDeBuffDetail b = skillDeBuffDetails[i];
                    if (buffType == b.monsterSpecialDeBufferType)
                    {
                        count++;
                    }
                }

                return count;
            }
        }

        public static UnitAbility GetUnitAbility(string skillID, MonsterDataScriptObj monsterDataScriptObj)
        {
            for (int i = 0; i < monsterDataScriptObj.unitData.m_Abilities.Length; i++)
            {
                UnitAbilityPlayerData unitAbility = monsterDataScriptObj.unitData.m_Abilities[i];
                if (unitAbility.unitAbility.GetSkillID() == skillID)
                {
                    //Debug.Log("unitAbility.unitAbility.GetSkillType()" + unitAbility.unitAbility.GetSkillType().ToString());
                    //Debug.Log("skillType" + skillType.ToString());

                    return unitAbility.unitAbility;
                }
            }

            return null;
        }

        public static List<Monster.MonsterSkillInfo> GetUsingMonsterSkillInfos(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, List<string> usingSkillIDs)
        {
            List<Monster.MonsterSkillInfo> usingSkills = new();

            for (int i = 0; i < playerMonsterInfo.MonsterSkillInfos.Count; i++)
            {
                int skillIndex = i;
                Monster.MonsterSkillInfo info = playerMonsterInfo.MonsterSkillInfos[skillIndex];
                if (usingSkillIDs.Contains(info.skillID))
                {
                    usingSkills.Add(info);
                }
            }

            return usingSkills;
        }

        public static int GetSkillLevel(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, SkillDataScriptObj skillDataScriptObj)
        {
            if (skillDataScriptObj == null)
            {
                Debug.Log("skillDataScriptObj null");
                return 0;
            }

            for (int i = 0; i < playerMonsterInfo.MonsterSkillInfos.Count; i++)
            {
                int skillIndex = i;
                Monster.MonsterSkillInfo skillInfo = playerMonsterInfo.MonsterSkillInfos[skillIndex];
                if (skillInfo.skillID == skillDataScriptObj.Skill.skillID)
                {
                    //Debug.Log("Skill found = " + skillInfo.skillType);

                    return skillInfo.skillLevel;
                }
            }

            //Debug.Log("Skill not found = " + skillDataScriptObj.Skill.skillType.ToString());
            return 0;
        }

        public static float Get_Requested_Parameter_Value(List<float> parametersRequired, int skillLevelIndex)
        {
            skillLevelIndex = Mathf.Clamp(skillLevelIndex, 0, parametersRequired.Count - 1);

            return parametersRequired[skillLevelIndex];
        }

        /// <summary>
        /// levelUpRate: 100% = 1f;
        /// </summary>
        /// <param name="levelUpRate"></param>
        /// <returns></returns>
        public static bool Get_Skill_Level_Up_Chance(float levelUpRate)
        {
            if (levelUpRate > 1f)
            {
                levelUpRate = 1f;
            }

            float ranNum = UnityEngine.Random.Range(0f, 100f);

            return ranNum / 100f <= levelUpRate;
        }

        /// <summary>
        /// <para> Skill acquisition or level increase calculation formula </para>
        /// <para> HP：MonsterHP </para>
        /// <para> STR:MonsterSTR </para>
        /// <para> INT：MonsterINT </para>
        /// <para> DEX：MonsterDEX </para>
        /// <para> AGI:MonsterAGI </para>
        /// <para> VIT:MonsterVIT </para>
        /// <para> parameter correction is percentage, it must be converted to 100% = 1f </para>
        /// <para> HP:HPC (HP correction) </para>
        /// <para> STR:STRC(STR correction) </para>
        /// <para> INT:INTC（INT correction) </para>
        /// <para> DEX：DEXC（DEX correction) </para>
        /// <para> AGI:AGIC(AGI correction) </para>
        /// <para> VIT:VITC(VIT correction) </para>
        /// <para> Request total parameters: RP (Requested total parameters)</para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Probability of learning or level increase: R (Result) </para>
        /// <para> R = ( HP × HPC + STR × STRC + INT  × INTC + DEX × DEXC + AGI × AGIC + VIT × VITC )  / RP × ( 1 + CC ) </para>
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="parametersCorrection"></param>
        ///     /// <param name="requestedTotalParameters"></param>
        /// <param name="traitCorrection"></param>
        /// <returns></returns>
        public static float Get_Skill_Caculate_Level_Up_Rate(BasicParameters parameters, BasicParameters parametersCorrection, float requestedTotalParameters, float traitCorrection)
        {
            float HPxHPC = parameters.health * parametersCorrection.health;
            float STRxSTRC = parameters.strenght * parametersCorrection.strenght;
            float INTxINTC = parameters.intelligent * parametersCorrection.intelligent;
            float DEXxDEXC = parameters.dexterity * parametersCorrection.dexterity;
            float AGIxAGIC = parameters.agility * parametersCorrection.agility;
            float VITxVITC = parameters.vitality * parametersCorrection.vitality;

            float totalParameters = HPxHPC + STRxSTRC + INTxINTC + DEXxDEXC + AGIxAGIC + VITxVITC;

            totalParameters /= 100f;

            float result = totalParameters / requestedTotalParameters * (1f + traitCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Calculation formula for skill acquisition or level increase");
            DebugPopup.LogRaise($"hp = {parameters.health}");
            DebugPopup.LogRaise($"str = {parameters.strenght}");
            DebugPopup.LogRaise($"int = {parameters.intelligent}");
            DebugPopup.LogRaise($"dex = {parameters.dexterity}");
            DebugPopup.LogRaise($"agi = {parameters.agility}");
            DebugPopup.LogRaise($"vit = {parameters.vitality}");
            DebugPopup.LogRaise($"hp correction = {parametersCorrection.health}");
            DebugPopup.LogRaise($"str correction = {parametersCorrection.strenght}");
            DebugPopup.LogRaise($"int correction = {parametersCorrection.intelligent}");
            DebugPopup.LogRaise($"dex correction = {parametersCorrection.dexterity}");
            DebugPopup.LogRaise($"agi correction = {parametersCorrection.agility}");
            DebugPopup.LogRaise($"vit correction = {parametersCorrection.vitality}");
            DebugPopup.LogRaise($"Requested total parameters: RP = {requestedTotalParameters}");
            DebugPopup.LogRaise($"Acquisition or level increase probability: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> Characteristic acquisition or level increase calculation formula </para>
        /// <para> Monster Reference Parameter 1: MP1 (Monster's parameter1) </para>
        /// <para> Monster's Reference Parameter 2: MP2 (Monster's parameter2)</para>
        /// <para> Monster's Reference Parameter 3: MP3 (Monster's parameter3) </para>
        /// <para> Monster's Reference Parameter 4: MP4 (Monster's parameter4) </para>
        /// <para> Monster's Reference Parameter 5: MP5 (Monster's parameter5) </para>
        /// <para> Monster Reference Parameter 6: MP6 (Monster's parameter6) </para>
        /// <para> Request Parameter 1: PR1 (Requested Parameter1) </para>
        /// <para> Request Parameter 2: PR2(Requested Parameter2) </para>
        /// <para> Request Parameter 3: PR3 (Requested Parameter3) </para>
        /// <para> Request Parameter 4: PR4 (Requested Parameter4) </para>
        /// <para> Request Parameter 5: PR5 (Requested Parameter5) </para>
        /// <para> Request Parameter 6: PR6 (Requested Parameter6) </para>
        /// <para> Number of request parameters: N(Number of request parameters) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Probability of learning or level increase: R (Result) </para>
        /// <para> R = ( MIN ( 1, MP1 / PR1 ) + MIN ( 1, MP2 / PR2 ) + MIN ( 1, MP3 / PR3 ) + MIN ( 1, MP4 / PR4 ) + MIN ( 1, MP5 / PR5 ) + MIN ( 1, MP6 / PR6 ) ) / N × ( 1 + CC ) </para>
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="parametersCorrection"></param>
        ///     /// <param name="requestedTotalParameters"></param>
        /// <param name="characteristicCorrection"></param>
        /// <returns></returns>
        public static float Get_Characteristic_Caculate_Level_Up_Rate(BasicParameters parameters, BasicParameters requestedParameters, float characteristicCorrection)
        {
            float min_health = Mathf.Min(1f, parameters.health / requestedParameters.health);

            float min_strenght = Mathf.Min(1f, parameters.strenght / requestedParameters.strenght);

            float min_intelligent = Mathf.Min(1f, parameters.intelligent / requestedParameters.intelligent);

            float min_dexterity = Mathf.Min(1f, parameters.dexterity / requestedParameters.dexterity);

            float min_agility = Mathf.Min(1f, parameters.agility / requestedParameters.agility);

            float min_vitality = Mathf.Min(1f, parameters.vitality / requestedParameters.vitality);

            float numberOfRequestParameter = 0f;

            if (requestedParameters.health > 0f)
                numberOfRequestParameter++;

            if (requestedParameters.strenght > 0f)
                numberOfRequestParameter++;

            if (requestedParameters.intelligent > 0f)
                numberOfRequestParameter++;

            if (requestedParameters.dexterity > 0f)
                numberOfRequestParameter++;

            if (requestedParameters.agility > 0f)
                numberOfRequestParameter++;

            if (requestedParameters.vitality > 0f)
                numberOfRequestParameter++;

            float totalMin = min_health + min_strenght + min_intelligent + min_dexterity + min_agility + min_vitality;

            float result = totalMin / numberOfRequestParameter * (1f + characteristicCorrection);

            return result;
        }

        /// <summary>
        /// <para> Return value will be converted to 100% = 1f </para>
        /// </summary>
        public static float GetAttributeEndurance(MonsterDataScriptObj monsterDataScriptObj, AttributeType attributeType)
        {
            float AttributeEnduranceVal = 0f;

            switch (attributeType)
            {
                case AttributeType.なし_None:
                    {
                        AttributeEnduranceVal = 0f;

                        break;
                    }

                case AttributeType.土_Soil:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Soil;

                        break;
                    }

                case AttributeType.打_Hit:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Hit;

                        break;
                    }

                case AttributeType.斬_Cut:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Cut;

                        break;
                    }

                case AttributeType.水_Water:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Water;

                        break;
                    }

                case AttributeType.炎_Flame:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Flame;

                        break;
                    }

                case AttributeType.突_Thrust:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Thrust;

                        break;
                    }

                case AttributeType.風_Wind:
                    {
                        AttributeEnduranceVal = monsterDataScriptObj.Monster.Wind;

                        break;
                    }
            }

            AttributeEnduranceVal /= 100f;

            return AttributeEnduranceVal;
        }

        /// <summary>
        /// <para> Return value will be converted to 100% = 1f </para>
        /// </summary>
        public static float GetDamageCorrectionByTrait
            (
            InnateTrait.InnateTraitInfo innateTraitInfo, 
            Skill.SkillGenType skillGenType,
            AttributeType attributeType
            )
        {
            float AttributeCorrectionVal = 0f;

            switch (attributeType)
            {
                case AttributeType.なし_None:
                    {
                        AttributeCorrectionVal = 0f;

                        break;
                    }

                case AttributeType.土_Soil:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Earth_Damage_Correction;

                        break;
                    }

                case AttributeType.打_Hit:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Blunt_Damage_Correction;

                        break;
                    }

                case AttributeType.斬_Cut:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Slash_Damage_Correction;

                        break;
                    }

                case AttributeType.水_Water:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Water_Damage_Correction;

                        break;
                    }

                case AttributeType.炎_Flame:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Fire_Damage_Correction;

                        break;
                    }

                case AttributeType.突_Thrust:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Thrust_Damage_Correction;

                        break;
                    }

                case AttributeType.風_Wind:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Wind_Damage_Correction;

                        break;
                    }
            }

            switch (skillGenType)
            {
                case SkillGenType.STR:
                    {
                        AttributeCorrectionVal += innateTraitInfo.STR_Skill_Damage_Correction;

                        break;
                    }

                case SkillGenType.INT:
                    {
                        AttributeCorrectionVal += innateTraitInfo.INT_Skill_Damage_Correction;

                        break;
                    }
            }

            return AttributeCorrectionVal;
        }

        /// <summary>
        /// <para> Return value will be converted to 100% = 1f </para>
        /// </summary>
        public static float ElementCorrectionByTrait
            (
            InnateTrait.InnateTraitInfo innateTraitInfo,
            AcquiredTrait.AcquiredTraitInfo acquiredTraitInfo,
            FarmTrait.FarmTraitInfo farmTraitInfo,
            AttributeType attributeType
            )
        {
            float AttributeCorrectionVal = 0f;

            switch (attributeType)
            {
                case AttributeType.なし_None:
                    {
                        AttributeCorrectionVal = 0f;

                        break;
                    }

                case AttributeType.土_Soil:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Earth_Resistance_Correction + acquiredTraitInfo.earth_resistance + farmTraitInfo.earth_Resistance;

                        break;
                    }

                case AttributeType.打_Hit:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Blunt_Resistance_Correction + acquiredTraitInfo.blunt_resistance + farmTraitInfo.blunt_Resistance;

                        break;
                    }

                case AttributeType.斬_Cut:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Slash_Resistance_Correction + acquiredTraitInfo.slash_resistance + farmTraitInfo.slash_Resistance;

                        break;
                    }

                case AttributeType.水_Water:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Water_Resistance_Correction + acquiredTraitInfo.water_resistance + farmTraitInfo.water_Resistance;

                        break;
                    }

                case AttributeType.炎_Flame:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Fire_Resistance_Correction + acquiredTraitInfo.fire_resistance + farmTraitInfo.fire_Resistance;

                        break;
                    }

                case AttributeType.突_Thrust:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Thrust_Resistance_Correction + acquiredTraitInfo.thrust_resistance + farmTraitInfo.thrust_Resistance;

                        break;
                    }

                case AttributeType.風_Wind:
                    {
                        AttributeCorrectionVal = innateTraitInfo.Wind_Resistance_Correction + acquiredTraitInfo.wind_resistance + farmTraitInfo.wind_Resistance;

                        break;
                    }
            }

            return AttributeCorrectionVal;
        }

        /// <summary>
        /// <para>S: 180+</para>
        /// <para>A: 150+</para>
        /// <para>B: 120+</para>
        /// <para>C: 90+</para>
        /// <para>D: 60+</para>
        /// <para>E: 30+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_HP_DMG_Rank(float value)
        {
            if (value >= 180)
                return RankTypeEnums.s;
            if (value >= 150)
                return RankTypeEnums.a;
            if (value >= 120)
                return RankTypeEnums.b;
            if (value >= 90)
                return RankTypeEnums.c;
            if (value >= 60)
                return RankTypeEnums.d;
            if (value >= 30)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 31+</para>
        /// <para>A: 26+</para>
        /// <para>B: 21+</para>
        /// <para>C: 16+</para>
        /// <para>D: 11+</para>
        /// <para>E: 6+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_ST_DMG_Rank(float value)
        {
            if (value >= 31)
                return RankTypeEnums.s;
            if (value >= 26)
                return RankTypeEnums.a;
            if (value >= 21)
                return RankTypeEnums.b;
            if (value >= 16)
                return RankTypeEnums.c;
            if (value >= 11)
                return RankTypeEnums.d;
            if (value >= 6)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 72+</para>
        /// <para>A: 65+</para>
        /// <para>B: 58+</para>
        /// <para>C: 51+</para>
        /// <para>D: 44+</para>
        /// <para>E: 37+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_HIT_Rank(float value)
        {
            if (value >= 72)
                return RankTypeEnums.s;
            if (value >= 65)
                return RankTypeEnums.a;
            if (value >= 58)
                return RankTypeEnums.b;
            if (value >= 51)
                return RankTypeEnums.c;
            if (value >= 44)
                return RankTypeEnums.d;
            if (value >= 37)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 43+</para>
        /// <para>A: 36+</para>
        /// <para>B: 29+</para>
        /// <para>C: 22+</para>
        /// <para>D: 15+</para>
        /// <para>E: 8+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_CRT_Rank(float value)
        {
            if (value >= 43)
                return RankTypeEnums.s;
            if (value >= 36)
                return RankTypeEnums.a;
            if (value >= 29)
                return RankTypeEnums.b;
            if (value >= 22)
                return RankTypeEnums.c;
            if (value >= 15)
                return RankTypeEnums.d;
            if (value >= 8)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 19+</para>
        /// <para>A: 16+</para>
        /// <para>B: 13+</para>
        /// <para>C: 10+</para>
        /// <para>D: 7+</para>
        /// <para>E: 4+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_DELAY_DMG_Rank(float value)
        {
            if (value >= 19)
                return RankTypeEnums.s;
            if (value >= 16)
                return RankTypeEnums.a;
            if (value >= 13)
                return RankTypeEnums.b;
            if (value >= 10)
                return RankTypeEnums.c;
            if (value >= 7)
                return RankTypeEnums.d;
            if (value >= 4)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 31+</para>
        /// <para>A: 27.5+</para>
        /// <para>B: 24+</para>
        /// <para>C: 20.5+</para>
        /// <para>D: 17+</para>
        /// <para>E: 13.5+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_HP_HEAL_Rank(float value)
        {
            if (value >= 31)
                return RankTypeEnums.s;
            if (value >= 27.5f)
                return RankTypeEnums.a;
            if (value >= 24)
                return RankTypeEnums.b;
            if (value >= 20.5f)
                return RankTypeEnums.c;
            if (value >= 17)
                return RankTypeEnums.d;
            if (value >= 13.5f)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }

        /// <summary>
        /// <para>S: 31+</para>
        /// <para>A: 26+</para>
        /// <para>B: 21+</para>
        /// <para>C: 16+</para>
        /// <para>D: 11+</para>
        /// <para>E: 6+</para>
        /// <para>F: 0</para>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Skill_ST_HEAL_Rank(float value)
        {
            if (value >= 31)
                return RankTypeEnums.s;
            if (value >= 26)
                return RankTypeEnums.a;
            if (value >= 21)
                return RankTypeEnums.b;
            if (value >= 16)
                return RankTypeEnums.c;
            if (value >= 11)
                return RankTypeEnums.d;
            if (value >= 6)
                return RankTypeEnums.e;
            return RankTypeEnums.f;
        }
    }
}