using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Skill
{
    [CreateAssetMenu(fileName = "SkillDataManagerScriptObj", menuName = "ScriptableObjects/Monster/SkillDataManagerScriptObj", order = 1)]
    public class SkillDataManagerScriptObj : ScriptableObject
    {
        Dictionary<string, SkillDataScriptObj> skillDataDictionary = new Dictionary<string, SkillDataScriptObj>();

        [SerializeField]
        List<SkillDataScriptObj> skillDataList = new List<SkillDataScriptObj>();

        public SkillDataScriptObj GetSkillDataObj(string skillID)
        {
            SkillDataScriptObj skillDataObj = null;

            if (!skillDataDictionary.TryGetValue(skillID, out skillDataObj))
            {
                // the key isn't in the dictionary.
                return skillDataObj; // or whatever you want to do
            }

            return skillDataObj;
        }

        private void OnValidate()
        {
            if (Application.isEditor && Application.isPlaying == false)
            {
                //Debug.Log("OnValidate");

                CreateDictionary();
            }
        }

        public void CreateDictionary()
        {
            if (skillDataList.Count > 0)
            {
                skillDataDictionary.Clear();

                for (int i = 0; i < skillDataList.Count; i++)
                {
                    skillDataDictionary.Add(skillDataList[i].Skill.skillID, skillDataList[i]);
                }
            }
        }
    }
}