using Assets.Scripts.Monster.Trait;
using Assets.Scripts.TrainingArea;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class MonsterAction
    {
        public enum MonsterActionType
        {
            Empty, BasicTraining, SpecialTraining, IntensiveTrainning, Exploration, Tournament, Rest, Hospital
        }

        public MonsterActionType monsterActionType = MonsterActionType.Empty;

        public bool actionDone = true;

        public double trainingStartUtcUnix = 0;

        public double trainingLastUtcUnix = 0;

        public int weekPassed = 0;

        public ParameterType basicTrainingMainParameter;

        public TrainingArea.TrainingArea.TrainingAreaType trainingAreaType = TrainingArea.TrainingArea.TrainingAreaType.Empty;

        public string trainingSkill = string.Empty;
        public bool skillUp = false;

        public int actionWeek = 0;

        public bool isLost = false;
        public int lostWeek = 0;
        public float lostGold = 0f;
        public List<ExplorationItem> explorationItems = new();

        public TrainingArea.TrainingArea.TrainingType trainingType = TrainingArea.TrainingArea.TrainingType.success;

        public float foodGold = 0f;

        public BasicParameters basicParameters;

        public GrowthParameters growthParameters;

        public List<AcquiredTrait.AcquiredTraitType> actionNewTraits = new();

        public List<AcquiredTrait.AcquiredTraitType> actionUpTraits = new();

        public MonsterAction()
        {
            monsterActionType = MonsterActionType.Empty;

            actionDone = true;

            trainingStartUtcUnix = 0;

            trainingLastUtcUnix = 0;

            weekPassed = 0;

            basicTrainingMainParameter = ParameterType.HP;

            trainingAreaType = TrainingArea.TrainingArea.TrainingAreaType.Empty;

            trainingSkill = string.Empty;
            skillUp = false;

            actionWeek = 0;
            isLost = false;
            lostWeek = 0;
            explorationItems = new List<ExplorationItem>();

            trainingType = TrainingArea.TrainingArea.TrainingType.success;

            foodGold = 0f;

            basicParameters = new BasicParameters();

            growthParameters = new GrowthParameters();

            actionNewTraits = new();

            actionUpTraits = new();
        }
    }
}