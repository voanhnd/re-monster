﻿using System;
using UnityEngine;

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class BattleParameters
    {
        public float StaminaRecovery = 0f;
        public float Speed = 0f;

        [Range(1, 5)]
        public int Move = 0;

        public BattleParameters Clone()
        {
            BattleParameters newParameters = new()
            {
                StaminaRecovery = StaminaRecovery,
                Speed = Speed,
                Move = Move
            };

            return newParameters;
        }
    }
}