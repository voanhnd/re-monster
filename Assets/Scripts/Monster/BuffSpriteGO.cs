using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Monster.Skill;
using UnityEngine;

public class BuffSpriteGO : MonoBehaviour
{
    [SerializeField]
    GameObject buff, debuff, mentalBuff, mentalDebuff;

    public void ActiveBuffSprite(ParameterInGame parameterInGame)
    {
        int buffCount = 0;

        int debuffCount = 0;

        int mentalBuffCount = 0;

        int mentalDebuffCount = 0;

        foreach (InGameBuffDetail inGameBuffDetail in parameterInGame.inGameBuffDetails)
        {
            if (InGameBuffDetail.IsPsychic(inGameBuffDetail.monsterSpecialBufferType))
            {
                mentalBuffCount++;
            }
            else
            {
                buffCount++;
            }
        }

        foreach (InGameDeBuffDetail inGameBuffDetail in parameterInGame.inGameDeBuffDetails)
        {
            if (InGameDeBuffDetail.IsPsychic(inGameBuffDetail.monsterSpecialDeBufferType))
            {
                mentalDebuffCount++;
            }
            else
            {
                debuffCount++;
            }
        }

        if(buffCount > 0 && buff.activeSelf == false)
        {
            buff.SetActive(true);
        }
        else if (buffCount <= 0 && buff.activeSelf)
        {
            buff.SetActive(false);
        }

        if (debuffCount > 0 && debuff.activeSelf == false)
        {
            debuff.SetActive(true);
        }
        else if (debuffCount <= 0 && debuff.activeSelf)
        {
            debuff.SetActive(false);
        }

        if (mentalBuffCount > 0 && mentalBuff.activeSelf == false)
        {
            mentalBuff.SetActive(true);
        }
        else if (mentalBuffCount <= 0 && mentalBuff.activeSelf)
        {
            mentalBuff.SetActive(false);
        }

        if (mentalDebuffCount > 0 && mentalDebuff.activeSelf == false)
        {
            mentalDebuff.SetActive(true);
        }
        else if (mentalDebuffCount <= 0 && mentalDebuff.activeSelf)
        {
            mentalDebuff.SetActive(false);
        }
    }
}
