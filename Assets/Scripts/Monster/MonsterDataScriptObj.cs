using UnityEngine;

namespace Assets.Scripts.Monster
{
    [CreateAssetMenu(fileName = "MonsterDataScriptObj", menuName = "ScriptableObjects/Monster/MonsterDataScriptObj", order = 1)]
    public class MonsterDataScriptObj : ScriptableObject
    {
        [SerializeField]
        private Monster monster;

        public Monster Monster { get => monster; private set => monster = value; }

        public UnitData unitData;
    }
}