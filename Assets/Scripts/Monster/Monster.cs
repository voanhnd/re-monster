﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ParameterType
{
    HP,
    STR,
    INT,
    DEX,
    AGI,
    VIT
}

public enum AttributeType
{
    なし_None, 打_Hit, 斬_Cut, 突_Thrust, 炎_Flame, 水_Water, 風_Wind, 土_Soil
}

public enum MonsterSpecialBufferType
{
    None,

    /// <summary>
    /// <para> "一度だけ、新規にかかるデバフを無効化する、2ターン継続 </para>
    /// <para> Disable Debuff mới 1 lần và kéo dài 2 turn" </para>
    /// </summary>
    ベール_Veil,

    /// <summary>
    /// <para> "被ダメージ数%減少（スキルによる）、2ターン継続 </para>
    /// <para> Giảm trấn thương nhận được (tuỳ vào skill), kéo dài 2 turn" </para>
    /// </summary>
    バリア_Barrier,

    /// <summary>
    /// <para> そのMonsterのターン毎に最大HPの数%回復（スキルによる）、3ターン継続 </para>
    /// <para> Phục hồi tối đa bao nhiêu %HP cho 1 turn của Monster đó, kéo dài 3 turn </para>
    /// </summary>
    賦活_Activation,

    /// <summary>
    /// <para> ST回復量が数%増加、3ターン継続 </para>
    /// <para> Tăng lượng phục hồi ST %, kéo dài 3 turn </para>
    /// </summary>
    活力_Vitality,

    /// <summary>
    /// <para> スピード+10%、2ターン継続 </para>
    /// <para> Speed + 10%, kéo dài 2 turn </para>
    /// </summary>
    加速_Acceleration,

    /// <summary>
    /// <para> 次回攻撃時にスキル毎に決められた量の与ダメージ上昇 </para>
    /// <para> Tăng trấn thương gây ra được xác định cho mỗi skill trong lần tấn công tiếp theo </para>
    /// </summary>
    溜め_Charge,

    /// <summary>
    /// <para> 回避率数%上昇（スキルによる）、2ターン継続 </para>
    /// <para> Tăng tỷ lệ né tránh % (tuỳ skill), kéo dài 2 turn </para>
    /// </summary>
    幻影_Illusion,

    /// <summary>
    /// <para> 攻撃範囲に入っていると、このMonsterしか狙えなくなる、xターン（スキルによる）継続 </para>
    /// <para> Khi trong phạm vi tấn công, chỉ có thể nhắm vào con monster này và kéo dài x turn (tuỳ skill) </para>
    /// </summary>
    挑発_Provocation,

    /// <summary>
    /// <para> 相手から見えなくなり攻撃のターゲットに出来なくなる、3ターン継続 </para>
    /// <para> ターン経過以外に範囲攻撃のヒット、サーチスキル、スキルを使うことで解除される </para>
    /// <para> Tàng hình trc đối thủ và k phải là mục tiêu tấn công, kéo dài 3 turn  </para>
    /// <para> Sẽ huỷ bằng cách sử dụng các đòn tấn công khu vuưc, search skill, skill ngoài quá trình turn </para>
    /// </summary>
    潜伏_Invisibility,

    Increase_STR,
    Increase_INT,
    Increase_DEX,
    Increase_AGI,
    Increase_VIT,

    /// <summary>
    /// <para> 与ダメージ、クリティカル率を数%上昇（スキルによる）、3ターン継続 </para>
    /// <para> Tăng trấn thương gây ra và critical rate (tuỳ skill), kéo dài 3 turn  </para>
    /// </summary>
    勇気_Courage,

    /// <summary>
    /// <para> クリティカル率数%上昇（スキルによる）、3ターン継続 </para>
    /// <para> Tăng critical rate (tuỳ skill), kéo dài 3 turn  </para>
    /// </summary>
    応援_Cheering,

    /// <summary>
    /// <para> 命中率と回避率数%上昇（スキルによる）、2ターン継続  </para>
    /// <para> Tăng tỷ lệ chính xác và tỷ lệ né (tuỳ skill) kéo dà 2 turn  </para>
    /// </summary>
    集中_Concentration,

    /// <summary>
    /// <para> 命中率と回避率数%上昇（スキルによる）、2ターン継続  </para>
    /// <para> Tăng tỷ lệ chính xác và tỷ lệ né (tuỳ skill) kéo dà 2 turn  </para>
    /// </summary>
    活力_Energize
}

public enum MonsterSpecialDeBufferType
{
    None,

    /// <summary>
    /// <para> そのMonsterのターン毎に最大HPの5%のダメージ、3ターン継続 </para>
    /// <para> Damage 5% HP tối đã mỗi turn của Monster đó, kéo dài 3 turrn </para>
    /// </summary>
    毒_Poison,

    /// <summary>
    /// <para> そのMonsterのターン毎に最大HPの10%のダメージ、3ターン継続 </para>
    /// <para> Damage 10% HP tối đã mỗi turn của Monster đó, kéo dài 3 turrn </para>
    /// </summary>
    猛毒_Deadly_Poison,

    /// <summary>
    /// <para> Monsterの番になるごとに、スタミナ回復後にスタミナ10%減少 </para>
    /// <para> スキルによるHP、スタミナ回復20%減少、2ターン継続 </para>
    /// <para> Giảm 10 % thể lực sau khi phục hồi mỗi khi đến phiên cuar Monster </para>
    /// <para> Tuỳ vào skill thì giảm HP và khả năng phục hồi 20%, kéo dàu 2 turn </para>
    /// </summary>
    衰弱_Weakness,

    /// <summary>
    /// <para> 命中率と回避率-15%、1ターン継続 </para>
    /// <para> Chính xác và tỷ lệ né tránh -15%, kéo dài 1 turn </para>
    /// </summary>
    盲目_Blindness,

    /// <summary>
    /// <para> スピード-10%、移動力-1（移動力の下限は1）、1ターン継続 </para>
    /// <para> Tốc độ -10%, di chuyển -1 (di chuyển tối thiểu là 1), kéo dài 1 turn </para>
    /// </summary>
    加速_Slowness,

    /// <summary>
    /// <para> 1ターン行動不能 </para>
    /// <para> Không thể hành động 1 turn </para>
    /// </summary>
    麻痺_Paralysis,

    /// <summary>
    /// <para> 1ターン行動不能、スタミナ回復+50%、被ダメージ時解除 </para>
    /// <para> Không thể hành động 1 turn, Phục hồi thể lực + 50% và huỷ bỏ khi nhận trấn thương </para>
    /// </summary>
    睡眠_Sleep,

    Decrease_STR,
    Decrease_INT,
    Decrease_DEX,
    Decrease_AGI,
    Decrease_VIT,

    /// <summary>
    /// <para> 与ダメージ+10%、被ダメージ+10%、消費スタミナ+10%、3ターン継続 </para>
    /// <para> +10% trấn thương gây ra, +10% trấn thương nhận vào, +10% thể lực tiêu hao, kéo dài 3 turn </para>
    /// </summary>
    激怒_Rage,

    /// <summary>
    /// <para> スキル使用不可、2ターン継続 </para>
    /// <para> Không thể sử dụng skill, kéo dài 2 turn  </para>
    /// </summary>
    恐怖_Fear,

    /// <summary>
    /// <para> 敵Monsterが遠くになるように移動する（逃げる）2ターン継続 </para>
    /// <para> Di chuyển để Monster đối thủ ở xa (trốn thoát), kéo dài 2 turn  </para>
    /// </summary>
    混乱_Confusion,

    /// <summary>
    /// <para> 最も近い味方Monsterにランダムでスキルを使う 1ターン継続 </para>
    /// <para> Sử dụng random skill cho Monster đồng minh gần nhất, kéo dài 1 turn  </para>
    /// </summary>
    魅了_Charm,
}

public enum MonsterRemoveDebuffType
{
    精神_Mind, 肉体_Body
}

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class Monster
    {
        [Serializable]
        public class MonsterSkillInfo
        {
            public string skillID;

            /// <summary>
            /// <para> skill level == 0 mean it is not learned </para>
            /// </summary>
            public int skillLevel;
        }

        public enum MonsterType
        {
            P1,
            SignWitch,
            AstarCats,
            Golem,
            Kidsfight,
            Ossan,
            IceLime,
            Valius,
            Unicorn,
            Faireaf,
            SakeBarrel,
            UPA,
            Layla,
            Tofu,
            Hiyori,
            AmeChan
        }

        [Serializable]
        public class MonsterRace
        {
            public MonsterType mainRace;
            public MonsterType subRace;

            public MonsterRace()
            {

            }

            public MonsterRace(MonsterType mRace, MonsterType sRace)
            {
                mainRace = mRace;
                subRace = sRace;
            }

            public string RaceToString()
            {
                return mainRace.ToString() + "_" + subRace.ToString();
            }
        }

        public enum GrowthType
        {
            precocity,
            normal,
            late,
        }

        public enum BodyType
        {
            Thinness,
            SlightlyLeaner,
            Normal,
            SlightlyObese,
            Obesity
        }

        public enum TrainingPolicyType
        {
            Sparta,
            Severe,
            Normal,
            Sweet,
            Doting
        }

        public enum FatigueType
        {
            Normal,
            Fatigue,
            Overwork
        }

        public enum StressType
        {
            Normal,
            Stress,
            OverStress
        }

        public enum InjuryType
        {
            None,
            Injury,
            Serious_Injury
        }

        public enum DiseasesType
        {
            None,
            Diseases,
            Serious_Diseases
        }

        public string monsterID = "";

        public string monsterName = "";

        public MonsterRace monsterRace;

        public GrowthType growthType = GrowthType.precocity;

        [Header("100% = 100f")]
        public float growthScore = 0f;

        public int level = 0;

        [Header("Parameter settings")]

        /// <summary>
        /// It can be used as monster name
        /// </summary>

        public BasicParameters basicParameters;

        [Header("Growth Aptitude rank")]
        public RankTypeEnums HP_GA_Rank;
        public RankTypeEnums STR_GA_Rank;
        public RankTypeEnums INT_GA_Rank;
        public RankTypeEnums DEX_GA_Rank;
        public RankTypeEnums AGI_GA_Rank;
        public RankTypeEnums VIT_GA_Rank;

        [Header("Training Terrain Settings")]
        public TrainingArea.TrainingArea.TerrainType mainSuitTerrain;
        public TrainingArea.TrainingArea.TerrainType subSuitTerrain;
        public TrainingArea.TrainingArea.TerrainType mainNoSuitTerrain;
        public TrainingArea.TrainingArea.TerrainType subNoSuitTerrain;

        [Header("Battle Parameters settings")]

        public BattleParameters battleParameters;

        [Header("Attributes correction values are percentage 100%")]
        public float Hit = 0f;
        public float Cut = 0f;
        public float Thrust = 0f;
        public float Water = 0f;
        public float Flame = 0f;
        public float Wind = 0f;
        public float Soil = 0f;

        [Header("Special effect correction values are percentage 100%")]
        public float Poison = 0f;
        //public float SeverePoison = 0f;
        public float Weakness = 0f;
        public float Blindness = 0f;
        public float Slowdown = 0f;
        public float Paralysis = 0f;
        public float Sleep = 0f;
        public float Anger = 0f;
        public float Enchantment = 0f;
        public float Chaos = 0f;
        public float Fear = 0f;

        [Header("Addition settings")]

        public Sprite monsterAvatar;
        [SerializeField] private Sprite monsterBurstupAvatar;

        [Header("Growth settings")]
        public GrowthParameters growthParameters;

        public List<Food.FoodType> loveFoods;

        public List<Food.FoodType> hateFoods;

        [Header("Skill settings")]
        public List<MonsterSkillInfo> monsterSkillInfos = new();

        public Sprite MonsterBurstupAvatar => monsterBurstupAvatar;

        #region Basic parameter

        /// <summary>
        /// <para> complex parameter mean that basicMonsterParameter + playerInfoMonsterParameter </para> 
        /// </summary>
        /// <returns></returns>
        public static BasicParameters Get_Subtract_Parameter(BasicParameters complexParameter, BasicParameters basicParameters)
        {
            BasicParameters newComplexParameters = complexParameter.Clone();

            //Debug.Log("newComplexParameters");
            //Debug.Log("newComplexParameters.health = " + newComplexParameters.health);
            //Debug.Log("newComplexParameters.strenght = " + newComplexParameters.strenght);
            //Debug.Log("newComplexParameters.intelligent = " + newComplexParameters.intelligent);
            //Debug.Log("newComplexParameters.dexterity = " + newComplexParameters.dexterity);
            //Debug.Log("newComplexParameters.agility = " + newComplexParameters.agility);
            //Debug.Log("newComplexParameters.vitality = " + newComplexParameters.vitality);

            //Debug.Log("basicParameters");
            //Debug.Log("basicParameters.health = " + basicParameters.health);
            //Debug.Log("basicParameters.strenght = " + basicParameters.strenght);
            //Debug.Log("basicParameters.intelligent = " + basicParameters.intelligent);
            //Debug.Log("basicParameters.dexterity = " + basicParameters.dexterity);
            //Debug.Log("basicParameters.agility = " + basicParameters.agility);
            //Debug.Log("basicParameters.vitality = " + basicParameters.vitality);

            newComplexParameters.health -= basicParameters.health;
            newComplexParameters.strenght -= basicParameters.strenght;
            newComplexParameters.intelligent -= basicParameters.intelligent;
            newComplexParameters.dexterity -= basicParameters.dexterity;
            newComplexParameters.agility -= basicParameters.agility;
            newComplexParameters.vitality -= basicParameters.vitality;

            return newComplexParameters;
        }

        /// <summary>
        /// <para> complex parameter mean that basicMonsterParameter + playerInfoMonsterParameter </para> 
        /// </summary>
        /// <returns></returns>
        public static BasicParameters Get_Complex_Parameter(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        {
            BasicParameters complexParameter = new()
            {
                health = Get_Complex_Health(playerMonsterInfo, monsterDataScriptObj),
                strenght = Get_Complex_Strenght(playerMonsterInfo, monsterDataScriptObj),
                intelligent = Get_Complex_Intelligent(playerMonsterInfo, monsterDataScriptObj),
                dexterity = Get_Complex_Dexterity(playerMonsterInfo, monsterDataScriptObj),
                agility = Get_Complex_Agility(playerMonsterInfo, monsterDataScriptObj),
                vitality = Get_Complex_Vitality(playerMonsterInfo, monsterDataScriptObj)
            };

            return complexParameter;
        }

        /// <summary>
        /// <para> complex parameter mean that basicMonsterParameter + playerInfoMonsterParameter </para> 
        /// </summary>
        /// <returns></returns>
        public static BasicParameters Get_Complex_Parameter(BasicParameters playerInfoBasicParameters, BasicParameters monsterDataBasicParameters, bool isClamp = false)
        {
            BasicParameters complexParameter = new()
            {
                health = playerInfoBasicParameters.health + monsterDataBasicParameters.health,
                strenght = playerInfoBasicParameters.strenght + monsterDataBasicParameters.strenght,
                intelligent = playerInfoBasicParameters.intelligent + monsterDataBasicParameters.intelligent,
                dexterity = playerInfoBasicParameters.dexterity + monsterDataBasicParameters.dexterity,
                agility = playerInfoBasicParameters.agility + monsterDataBasicParameters.agility,
                vitality = playerInfoBasicParameters.vitality + monsterDataBasicParameters.vitality
            };

            if (isClamp)
            {
                complexParameter = Get_Clamp_Parameter(complexParameter);
            }

            return complexParameter;
        }

        /// <summary>
        /// <para> complex parameter mean that basicMonsterParameter + playerInfoMonsterParameter </para> 
        /// </summary>
        /// <returns></returns>
        public static BasicParameters Get_Complex_Parameter(BasicParameters playerInfoBasicParameters, BasicParameters monsterDataBasicParameters, BasicParameters traitParameters)
        {
            BasicParameters complexParameter = new()
            {
                health = playerInfoBasicParameters.health + monsterDataBasicParameters.health + traitParameters.health,
                strenght = playerInfoBasicParameters.strenght + monsterDataBasicParameters.strenght + traitParameters.strenght,
                intelligent = playerInfoBasicParameters.intelligent + monsterDataBasicParameters.intelligent + traitParameters.intelligent,
                dexterity = playerInfoBasicParameters.dexterity + monsterDataBasicParameters.dexterity + traitParameters.dexterity,
                agility = playerInfoBasicParameters.agility + monsterDataBasicParameters.agility + traitParameters.agility,
                vitality = playerInfoBasicParameters.vitality + monsterDataBasicParameters.vitality + traitParameters.vitality
            };

            return complexParameter;
        }

        public static BasicParameters Get_Battle_PlayerInfo_Basic_Parameter(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        {
            BasicParameters basicParameters = monsterDataScriptObj.Monster.basicParameters.Clone();

            BasicParameters tempParameter = Monster.Get_Subtract_Parameter(Monster.Get_Complex_Parameter(playerMonsterInfo.basicParameters.Clone(), basicParameters, true), basicParameters);

            //Debug.Log("tempParameter");
            //Debug.Log("tempParameter.health = " + tempParameter.health);
            //Debug.Log("tempParameter.strenght = " + tempParameter.strenght);
            //Debug.Log("tempParameter.intelligent = " + tempParameter.intelligent);
            //Debug.Log("tempParameter.dexterity = " + tempParameter.dexterity);
            //Debug.Log("tempParameter.agility = " + tempParameter.agility);
            //Debug.Log("tempParameter.vitality = " + tempParameter.vitality);

            return tempParameter;
        }

        public static BasicParameters Get_Clamp_Parameter(BasicParameters basicParameters)
        {
            BasicParameters complexParameter = new()
            {
                health = Mathf.Clamp(basicParameters.health, 0, 999),
                strenght = Mathf.Clamp(basicParameters.strenght, 0, 999),
                intelligent = Mathf.Clamp(basicParameters.intelligent, 0, 999),
                dexterity = Mathf.Clamp(basicParameters.dexterity, 0, 999),
                agility = Mathf.Clamp(basicParameters.agility, 0, 999),
                vitality = Mathf.Clamp(basicParameters.vitality, 0, 999)
            };

            return complexParameter;
        }

        public static float Get_Complex_Health(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.health + monsterDataScriptObj.Monster.basicParameters.health;

        public static float Get_Complex_Strenght(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.strenght + monsterDataScriptObj.Monster.basicParameters.strenght;

        public static float Get_Complex_Intelligent(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.intelligent + monsterDataScriptObj.Monster.basicParameters.intelligent;

        public static float Get_Complex_Dexterity(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.dexterity + monsterDataScriptObj.Monster.basicParameters.dexterity;

        public static float Get_Complex_Agility(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.agility + monsterDataScriptObj.Monster.basicParameters.agility;
        public static float Get_Complex_Vitality(PlayerInfo.PlayerMonsterInfo playerMonsterInfo, MonsterDataScriptObj monsterDataScriptObj)
        => playerMonsterInfo.basicParameters.vitality + monsterDataScriptObj.Monster.basicParameters.vitality;

        #endregion

        #region Monster rank

        /// <summary>
        /// <para> They are ranked into F~S according to numbers.  </para>
        /// <para> F : 1~150  </para>
        /// <para> E : 151~300  </para>
        /// <para> D : 301~450  </para>
        /// <para> C : 451~600  </para>
        /// <para> B : 601~750  </para>
        /// <para> A : 751~900  </para>
        /// <para> S : 901~999 </para>
        /// </summary>
        /// <param name="raparameterValue"></param>
        /// <returns></returns>
        public static float Get_Monster_Max_Parameter_By_Value(float raparameterValue)
        {
            RankTypeEnums rankType = Get_Monster_Parameter_Rank_By_Value(raparameterValue);

            int nextIndex = (int)rankType + 1;

            int enumCountMaxIndex = Enum.GetNames(typeof(RankTypeEnums)).Length - 1;

            if (nextIndex > enumCountMaxIndex)
            {
                nextIndex = enumCountMaxIndex;
            }

            rankType = (RankTypeEnums)nextIndex;

            return Get_Monster_Max_Parameter_Value_ByRank(rankType);
        }

        /// <summary>
        /// <para> They are ranked into F~S according to numbers.  </para>
        /// <para> F : 1~150  </para>
        /// <para> E : 151~300  </para>
        /// <para> D : 301~450  </para>
        /// <para> C : 451~600  </para>
        /// <para> B : 601~750  </para>
        /// <para> A : 751~900  </para>
        /// <para> S : 901~999 </para>
        /// </summary>
        /// <param name="rankType"></param>
        /// <returns></returns>
        public static float Get_Monster_Max_Parameter_Value_ByRank(RankTypeEnums rankType)
        {
            return rankType switch
            {
                RankTypeEnums.f => 150f,
                RankTypeEnums.e => 300f,
                RankTypeEnums.d => 450f,
                RankTypeEnums.c => 600f,
                RankTypeEnums.b => 750f,
                RankTypeEnums.a => 900f,
                RankTypeEnums.s => 999f,
                _ => 0f,
            };
        }

        /// <summary>
        /// <para> They are ranked into F~S according to numbers.  </para>
        /// <para> F : 1~150  </para>
        /// <para> E : 151~300  </para>
        /// <para> D : 301~450  </para>
        /// <para> C : 451~600  </para>
        /// <para> B : 601~750  </para>
        /// <para> A : 751~900  </para>
        /// <para> S : 901~999 </para>
        /// </summary>
        /// <param name="raparameterValue"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Monster_Parameter_Rank_By_Value(float raparameterValue)
        {
            RankTypeEnums selectedRankType = RankTypeEnums.f;

            if (raparameterValue <= 150f)
            {
                return RankTypeEnums.f;
            }
            if (raparameterValue <= 300f)
            {
                return RankTypeEnums.e;
            }
            if (raparameterValue <= 450f)
            {
                return RankTypeEnums.d;
            }
            if (raparameterValue <= 600f)
            {
                return RankTypeEnums.c;
            }
            if (raparameterValue <= 750f)
            {
                return RankTypeEnums.b;
            }
            if (raparameterValue <= 900f)
            {
                return RankTypeEnums.a;
            }
            if (raparameterValue > 900f)
            {
                return RankTypeEnums.s;
            }

            return selectedRankType;
        }

        /// <summary>
        /// <para> They are ranked into F~S according to numbers.  </para>
        /// <para> F : 1~150  </para>
        /// <para> E : 151~300  </para>
        /// <para> D : 301~450  </para>
        /// <para> C : 451~600  </para>
        /// <para> B : 601~750  </para>
        /// <para> A : 751~900  </para>
        /// <para> S : 901~999 </para>
        /// </summary>
        /// <param name="raparameterValue"></param>
        /// <returns></returns>
        public static RankTypeEnums Get_Monster_BasicParameters_Rank_By_Value(BasicParameters basicParameters)
        {
            float total = 0f;

            total += basicParameters.health;
            total += basicParameters.strenght;
            total += basicParameters.intelligent;
            total += basicParameters.dexterity;
            total += basicParameters.agility;
            total += basicParameters.vitality;

            float average = total / 6f;

            RankTypeEnums selectedRankType = Get_Monster_Parameter_Rank_By_Value(average);
            return selectedRankType;
        }

        #endregion

        #region Lifespan

        /// 
        /// Life consumption calculation occurs after the accumulation of fatigue and stress due to behavior
        /// 

        /// <summary>
        /// Get_LifeSpan_ChangePerWeek value = -10
        /// </summary>
        public static float Get_LifeSpan_Decrease_PerWeek()
        {
            return -10;
        }

        /// <summary>
        /// <para> return change value from condition </para>
        /// <para> 0 = basic training , </para>
        /// <para> 1 = special training, </para>
        /// <para> 2 = Exploration, intensive training</para>
        /// <para> 3 = Battle </para>
        /// <para> 4 = injured </para>
        /// <para> 5 = serious injury </para>
        /// <para> 6 = sick </para>
        /// <para> 7 = serious sick </para>
        /// <para> 8 = Over 100% damage in a single battle </para>
        /// <para> 9 = HP goes to 0 in Battle </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_LifeSpan_Decrease_Value(int changeFrom)
        {
            switch (changeFrom)
            {
                case 0:
                    {
                        return -5f;
                    }
                case 1:
                    {
                        return -8f;
                    }
                case 2:
                    {
                        return -50f;
                    }
                case 3:
                    {
                        return -40f;
                    }
                case 4:
                    {
                        return -60f;
                    }
                case 5:
                    {
                        return -100f;
                    }
                case 6:
                    {
                        return -90f;
                    }
                case 7:
                    {
                        return -150f;
                    }
            }

            return 0;
        }

        /// <summary>
        /// <para> Life consumption calculation formula </para>
        /// <para> Weeks elapsed: P (Period) </para>
        /// <para> Consumption by action: A (Action) </para>
        /// <para> Fatigue correction: F (Fatigue) 100% = 1f </para>
        /// <para> Stress correction: S (Stress) 100% = 1f </para>
        /// <para> Final consumption value: R (Result) </para>
        /// <para> R = (10 × P + A) × (1 + F + S) </para>
        /// </summary>
        /// </param>
        /// <returns></returns>
        public static float Get_LifeSpan_Consumption(float weekPassed, float consumptionByAction, FatigueType fatigueType, StressType stressType)
        {
            float fatigueCorrection = Get_Fatigue_LifeSpan_Percent(fatigueType);
            float stressCorrection = Get_Stress_LifeSpan_Percent(stressType);

            float result = (Get_LifeSpan_Decrease_PerWeek() * weekPassed + consumptionByAction) * (1 + fatigueCorrection + stressCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("LifeSpan");
            DebugPopup.LogRaise($"Weeks elapsed: P = {weekPassed}");
            DebugPopup.LogRaise($"Consumption by action: A = {consumptionByAction}");
            DebugPopup.LogRaise($"Fatigue correction: F = {fatigueCorrection}");
            DebugPopup.LogRaise($"Stress correction: S = {stressCorrection}");
            DebugPopup.LogRaise($"Final consumption value: R = {result}");

            return result;
        }

        #endregion

        #region GrowthType

        ///// <summary>
        ///// Vector3.x = current Stage, Vector3.y = next Stage, Vector3.z = stageProgress
        ///// </summary>
        ///// <param name="growthType"></param>
        ///// <param name="currentLifeSpan"></param>
        ///// <param name="maxLifeSpan"></param>
        ///// <returns></returns>
        //public static Vector3 Get_CurrentToNext_Stage_Progress(float currentLifeSpan, float maxLifeSpan)
        //{
        //    int maxStage = 10;

        //    float oneStageLifeSpan = maxLifeSpan / (float)maxStage;

        //    float lifeTimePassed = maxLifeSpan - currentLifeSpan;

        //    int currentStage = 1 + Mathf.FloorToInt(lifeTimePassed / oneStageLifeSpan);

        //    int nextStage = currentStage + 1;

        //    if (nextStage > maxStage)
        //    {
        //        nextStage = maxStage;
        //    }

        //    float nextStageLifeSpan = oneStageLifeSpan * nextStage;

        //    float nextStageProgress = lifeTimePassed / nextStageLifeSpan;

        //    return new Vector3((float)currentStage, (float)nextStage, nextStageProgress);
        //}

        ///// <summary>
        ///// total 10 stage from 1 to 10, return value is percent 100% = 1f
        ///// </summary>
        ///// <param name="stage"></param>
        ///// <param name="growthType"></param>
        ///// <returns></returns>

        //public static float Get_Growth_Stage_Correction(GrowthType growthType, float currentLifeSpan, float maxLifeSpan)
        //{
        //    float stageCorrectionValue = 0f;

        //    Vector3 stageProgress = Get_CurrentToNext_Stage_Progress(currentLifeSpan, maxLifeSpan);

        //    int currentStage = (int)stageProgress.x;

        //    float nextStageProgress = stageProgress.z;

        //    switch (currentStage)
        //    {
        //        case 1:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(0f, 4f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(0f, 9f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(0f, 14f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 2:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(5f, 9f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(10f, 19f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(15f, 29f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 3:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(10f, 19f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(20f, 34f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(30f, 34f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 4:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(20f, 29f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(35f, 49f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(35f, 59f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 5:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(30f, 34f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(50f, 59f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(60f, 69f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 6:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(35f, 39f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(60f, 64f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(70f, 79f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 7:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(40f, 54f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(65f, 74f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(80f, 84f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 8:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(55f, 69f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(75f, 79f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(85f, 89f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 9:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(70f, 79f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(80f, 84f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(90f, 94f, nextStageProgress);
        //                }

        //                break;
        //            }

        //        case 10:
        //            {
        //                if (growthType == GrowthType.precocity)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(80f, 100f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.normal)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(85f, 100f, nextStageProgress);
        //                }
        //                else if (growthType == GrowthType.late)
        //                {
        //                    stageCorrectionValue = Mathf.Lerp(95f, 100f, nextStageProgress);
        //                }

        //                break;
        //            }
        //    }

        //    stageCorrectionValue = Mathf.Round(stageCorrectionValue);

        //    return stageCorrectionValue / 100f;
        //}

        /// <summary>
        /// total 10 stage from 1 to 10, return value is percent 100% = 1f
        /// </summary>
        /// <param name="stage"></param>
        /// <param name="growthType"></param>
        /// <returns></returns>

        public static float Get_Growth_Stage_Rest_Correction(GrowthType growthType, float currentLifeSpan, float maxLifeSpan)
        {
            float stageCorrectionValue = 0f;

            float stageProgress = (1f - currentLifeSpan / maxLifeSpan) * 100f;

            if (growthType == GrowthType.precocity)
            {
                if (stageProgress >= 0f && stageProgress < 5f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 5f && stageProgress < 10f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 10f && stageProgress < 20f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 20f && stageProgress < 30f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 30f && stageProgress < 35f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 35f && stageProgress < 40f)
                {
                    stageCorrectionValue = -0.03f;
                }
                else if (stageProgress >= 40f && stageProgress < 55f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 55f && stageProgress < 70f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 70f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 80f)
                {
                    stageCorrectionValue = -0.2f;
                }
            }
            else if (growthType == GrowthType.normal)
            {
                if (stageProgress >= 0f && stageProgress < 10f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 10f && stageProgress < 20f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 20f && stageProgress < 35f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 35f && stageProgress < 50f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 50f && stageProgress < 60f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 60f && stageProgress < 65f)
                {
                    stageCorrectionValue = -0.03f;
                }
                else if (stageProgress >= 65f && stageProgress < 75f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 75f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 80f && stageProgress < 85f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 85f)
                {
                    stageCorrectionValue = -0.2f;
                }
            }
            else if (growthType == GrowthType.late)
            {
                if (stageProgress >= 0f && stageProgress < 15f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 15f && stageProgress < 30f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 30f && stageProgress < 35f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 35f && stageProgress < 60f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 60f && stageProgress < 70f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 70f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.03f;
                }
                else if (stageProgress >= 80f && stageProgress < 85f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 85f && stageProgress < 90f)
                {
                    stageCorrectionValue = -0.1f;
                }
                else if (stageProgress >= 90f && stageProgress < 95f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 95f)
                {
                    stageCorrectionValue = -0.2f;
                }
            }

            return stageCorrectionValue;
        }

        /// <summary>
        /// total 10 stage from 1 to 10, return value is percent 100% = 1f
        /// </summary>
        /// <param name="stage"></param>
        /// <param name="growthType"></param>
        /// <returns></returns>

        public static float Get_Growth_Stage_Training_Correction(GrowthType growthType, float currentLifeSpan, float maxLifeSpan)
        {
            float stageCorrectionValue = 0f;

            float stageProgress = (1 - currentLifeSpan / maxLifeSpan) * 100f;

            if (growthType == GrowthType.precocity)
            {
                if (stageProgress >= 0f && stageProgress < 5f)
                {
                    stageCorrectionValue = -0.4f;
                }
                else if (stageProgress >= 5f && stageProgress < 10f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 9f && stageProgress < 20f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 19f && stageProgress < 30f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 29f && stageProgress < 35f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 35f && stageProgress < 40f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 40f && stageProgress < 55f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 55f && stageProgress < 70f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 70f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 80f)
                {
                    stageCorrectionValue = -0.4f;
                }
            }
            else if (growthType == GrowthType.normal)
            {
                if (stageProgress >= 0f && stageProgress < 10f)
                {
                    stageCorrectionValue = -0.4f;
                }
                else if (stageProgress >= 10f && stageProgress < 20f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 20f && stageProgress < 35f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 35f && stageProgress < 50f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 50f && stageProgress < 60f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 60f && stageProgress < 65f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 65f && stageProgress < 75f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 75f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 80f && stageProgress < 85f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 85f)
                {
                    stageCorrectionValue = -0.4f;
                }
            }
            else if (growthType == GrowthType.late)
            {
                if (stageProgress >= 0f && stageProgress < 15f)
                {
                    stageCorrectionValue = -0.4f;
                }
                else if (stageProgress >= 15f && stageProgress < 30f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 30f && stageProgress < 35f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 35f && stageProgress < 60f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 60f && stageProgress < 70f)
                {
                    stageCorrectionValue = 0f;
                }
                else if (stageProgress >= 70f && stageProgress < 80f)
                {
                    stageCorrectionValue = -0.05f;
                }
                else if (stageProgress >= 80f && stageProgress < 85f)
                {
                    stageCorrectionValue = -0.15f;
                }
                else if (stageProgress >= 85f && stageProgress < 90f)
                {
                    stageCorrectionValue = -0.2f;
                }
                else if (stageProgress >= 90f && stageProgress < 95f)
                {
                    stageCorrectionValue = -0.35f;
                }
                else if (stageProgress >= 95f)
                {
                    stageCorrectionValue = -0.4f;
                }
            }

            return stageCorrectionValue;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="currentGrowParameterValues"></param>
        /// <returns></returns>
        public static float Get_Growth_Aptitude_Increase_Correction_Value(RankTypeEnums parameterRankType)
        {
            float percent = 0f;

            switch (parameterRankType)
            {
                case RankTypeEnums.s:
                    {
                        percent = 60f;

                        break;
                    }

                case RankTypeEnums.a:
                    {
                        percent = 40f;

                        break;
                    }

                case RankTypeEnums.b:
                    {
                        percent = 20f;

                        break;
                    }

                case RankTypeEnums.c:
                    {
                        percent = 0f;

                        break;
                    }

                case RankTypeEnums.d:
                    {
                        percent = -20f;

                        break;
                    }

                case RankTypeEnums.e:
                    {
                        percent = -40f;

                        break;
                    }

                case RankTypeEnums.f:
                    {
                        percent = -60f;

                        break;
                    }
            }

            return percent / 100f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="rankType"></param>
        /// <returns></returns>
        public static float Get_Growth_Aptitude_Decrease_Correction_Value(RankTypeEnums rankType)
        {
            float percent = 0f;

            switch (rankType)
            {
                case RankTypeEnums.s:
                    {
                        percent = -60f;

                        break;
                    }

                case RankTypeEnums.a:
                    {
                        percent = -40f;

                        break;
                    }

                case RankTypeEnums.b:
                    {
                        percent = -20f;

                        break;
                    }

                case RankTypeEnums.c:
                    {
                        percent = 0f;

                        break;
                    }

                case RankTypeEnums.d:
                    {
                        percent = 20f;

                        break;
                    }

                case RankTypeEnums.e:
                    {
                        percent = 40f;

                        break;
                    }

                case RankTypeEnums.f:
                    {
                        percent = 60f;

                        break;
                    }
            }

            return percent / 100f;
        }

        #endregion

        #region BodyType

        public static float Get_BodyType_Decrease_Per_Week()
        {
            return -5f;
        }

        /// <summary>
        /// <para> the return value is not percent </para>
        /// <para> 0 = In training -3 </para>
        /// <para> 1 = -5 in special training </para>
        /// <para> 2 = In competitions (PvE) -5 </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_BodyType_IncreaseOrDecrease_Value(int changeFrom)
        {
            switch (changeFrom)
            {
                case 0:
                    {
                        return -3f;
                    }
                case 1:
                    {
                        return -5f;
                    }
                case 2:
                    {
                        return -5f;
                    }
            }

            return 0;
        }

        /// <summary>
        /// return fixed value of BodyType
        /// </summary>
        /// <param name="monsterActionType">
        /// </param>
        /// <returns></returns>
        public static float Get_BodyType_Raise_Action_ChangeValue(MonsterAction.MonsterActionType monsterActionType, float weekPass)
        {
            switch (monsterActionType)
            {
                case MonsterAction.MonsterActionType.Empty:
                    {
                        return 0f;
                    }
                case MonsterAction.MonsterActionType.BasicTraining:
                    {
                        return -3f * weekPass;
                    }
                case MonsterAction.MonsterActionType.SpecialTraining:
                    {
                        return -5f * weekPass;
                    }
                case MonsterAction.MonsterActionType.IntensiveTrainning:
                    {
                        return -5f * weekPass;
                    }
                case MonsterAction.MonsterActionType.Exploration:
                    {
                        return -0f * weekPass;
                    }
                case MonsterAction.MonsterActionType.Tournament:
                    {
                        return -0f * weekPass;
                    }
                case MonsterAction.MonsterActionType.Rest:
                    {
                        return -0f * weekPass;
                    }
                case MonsterAction.MonsterActionType.Hospital:
                    {
                        return -0f * weekPass;
                    }
            }

            return 0;
        }

        /// <summary>
        /// <para> Thinness (1~15): 5% decrease in stamina recovery, 5% increase in speed, 5%  increase in stress, 5% increase in fatigue </para>
        /// <para> Slightly leaner(16~30): Decreased stamina recovery by 2%, increased speed by 2%, increased stress by 2%, increased fatigue by 2% </para>
        /// <para> Normal(31~70): No change </para>
        /// <para> Slightly obese(71~85): Decreased stamina recovery by 2%, decreased speed by 2%, decreased stress by 2%, increased fatigue by 2% </para>
        /// <para> Obesity(86~100): 5% decrease in stamina recovery, 5% decrease in speed, 5% decrease in stress rise, 5% increase in fatigue </para>
        /// </summary>
        /// <returns></returns>
        public static Vector2 Get_BodyType_Value_MinMax()
        => new Vector2(0f, 100f);

        /// <summary>
        /// return BodyType with bodyTypeValue
        /// </summary>
        /// <param name="bodyTypeVal"></param>
        /// <returns></returns>
        public static BodyType Get_BodyType_By_Value(float bodyTypeVal)
        {
            Vector2 minMax = Get_BodyType_Value_MinMax();

            bodyTypeVal = Mathf.Clamp(bodyTypeVal, minMax.x, minMax.y);

            BodyType selectedBodyType = BodyType.Normal;

            //Thinness (1~15)
            if (bodyTypeVal >= 0f && bodyTypeVal <= 15f)
            {
                selectedBodyType = BodyType.Thinness;
            }
            else         //Slightly leaner (16~30)
            if (bodyTypeVal > 15f && bodyTypeVal <= 30f)
            {
                selectedBodyType = BodyType.SlightlyLeaner;
            }
            else         //Normal (31~70): No change
            if (bodyTypeVal > 30f && bodyTypeVal <= 70f)
            {
                selectedBodyType = BodyType.Normal;
            }
            else         //Slightly obese (71~85) increased fatigue by 2%
            if (bodyTypeVal > 70f && bodyTypeVal <= 85f)
            {
                selectedBodyType = BodyType.SlightlyObese;
            }
            else         //Obesity (86~100)  5% increase in fatigue
            if (bodyTypeVal > 85f && bodyTypeVal <= minMax.y)
            {
                selectedBodyType = BodyType.Obesity;
            }

            return selectedBodyType;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="bodyTypeVal"></param>
        /// <returns></returns>
        public static float Get_BodyType_Increase_Stress_Correction(float bodyTypeVal)
        {
            switch (Get_BodyType_By_Value(bodyTypeVal))
            {
                case BodyType.Thinness:
                    {
                        return 0.05f;
                    }

                case BodyType.SlightlyLeaner:
                    {
                        return 0.02f;
                    }

                case BodyType.Normal:
                    {
                        return 0f;
                    }

                case BodyType.SlightlyObese:
                    {
                        return 0.02f;
                    }

                case BodyType.Obesity:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="bodyTypeVal"></param>
        /// <returns></returns>
        public static float Get_BodyType_Increase_Speed_Percent(float bodyTypeVal)
        {
            switch (Get_BodyType_By_Value(bodyTypeVal))
            {
                case BodyType.Thinness:
                    {
                        return 0.05f;
                    }

                case BodyType.SlightlyLeaner:
                    {
                        return 0.02f;
                    }

                case BodyType.Normal:
                    {
                        return 0f;
                    }

                case BodyType.SlightlyObese:
                    {
                        return 0.02f;
                    }

                case BodyType.Obesity:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="bodyTypeVal"></param>
        /// <returns></returns>
        public static float Get_BodyType_Decrease_Stamina_Recovery_Percent(float bodyTypeVal)
        {
            switch (Get_BodyType_By_Value(bodyTypeVal))
            {
                case BodyType.Thinness:
                    {
                        return 0.05f;
                    }

                case BodyType.SlightlyLeaner:
                    {
                        return 0.02f;
                    }

                case BodyType.Normal:
                    {
                        return 0f;
                    }

                case BodyType.SlightlyObese:
                    {
                        return 0.02f;
                    }

                case BodyType.Obesity:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="bodyTypeVal"></param>
        /// <returns></returns>
        public static float Get_BodyType_Increase_Fatigue_Correction(float bodyTypeVal)
        {
            switch (Get_BodyType_By_Value(bodyTypeVal))
            {
                case BodyType.Thinness:
                    {
                        return 0.05f;
                    }

                case BodyType.SlightlyLeaner:
                    {
                        return 0.02f;
                    }

                case BodyType.Normal:
                    {
                        return 0f;
                    }

                case BodyType.SlightlyObese:
                    {
                        return 0.02f;
                    }

                case BodyType.Obesity:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        #endregion

        #region Training Policy

        public static Vector2 Get_TrainingPolicy_Value_MinMax()
        => new Vector2(0f, 100f);

        /// <summary>
        /// return BodyType with bodyTypeValue
        /// </summary>
        /// <param name="trainingPolicyValue"></param>
        /// <returns></returns>
        public static TrainingPolicyType Get_TrainingPolicyType_By_Value(float trainingPolicyValue)
        {
            Vector2 minMax = Get_TrainingPolicy_Value_MinMax();

            trainingPolicyValue = Mathf.Clamp(trainingPolicyValue, minMax.x, minMax.y);

            TrainingPolicyType selectedTrainingPolicyType = TrainingPolicyType.Normal;

            //Thinness(1~15)
            if (trainingPolicyValue >= 0f && trainingPolicyValue <= 15f)
            {
                selectedTrainingPolicyType = TrainingPolicyType.Sparta;
            }
            else         //Slightly leaner (16~30)
            if (trainingPolicyValue > 15f && trainingPolicyValue <= 30f)
            {
                selectedTrainingPolicyType = TrainingPolicyType.Severe;
            }
            else         //Normal (31~70): No change
            if (trainingPolicyValue > 30f && trainingPolicyValue <= 70f)
            {
                selectedTrainingPolicyType = TrainingPolicyType.Normal;
            }
            else         //Slightly obese (71~85) increased fatigue by 2%
            if (trainingPolicyValue > 70f && trainingPolicyValue <= 85f)
            {
                selectedTrainingPolicyType = TrainingPolicyType.Sweet;
            }
            else         //Obesity (86~100)  5% increase in fatigue
            if (trainingPolicyValue > 85f && trainingPolicyValue <= minMax.y)
            {
                selectedTrainingPolicyType = TrainingPolicyType.Doting;
            }

            return selectedTrainingPolicyType;
        }

        /// <summary>
        /// <para> the return value is not percent </para>
        /// <para> 0 = +3 for giving your favorite food </para>
        /// <para> 1 = If you give something you don't like -3 </para>
        /// <para> 2 = +2 for a compliment </para>
        /// <para> 3 = When scolding -2 </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_TrainingPolicy_IncreaseOrDecrease_Value(int changeFrom)
        {
            switch (changeFrom)
            {
                case 0:
                    {
                        return 3f;
                    }
                case 1:
                    {
                        return -3f;
                    }
                case 2:
                    {
                        return 2f;
                    }
                case 3:
                    {
                        return -2f;
                    }
            }

            return 0;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="traininPolicyValue"></param>
        /// <returns></returns>
        public static float Get_TrainingPolicy_Reduction_Training_Failure_Rate(float traininPolicyValue)
        {
            switch (Get_TrainingPolicyType_By_Value(traininPolicyValue))
            {
                case TrainingPolicyType.Sparta:
                    {
                        return -0.05f;
                    }

                case TrainingPolicyType.Severe:
                    {
                        return -0.02f;
                    }

                case TrainingPolicyType.Normal:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Sweet:
                    {
                        return 0.02f;
                    }

                case TrainingPolicyType.Doting:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// return value is percentage, 100% = 1f
        /// </summary>
        /// <param name="traininPolicyValue"></param>
        /// <returns></returns>
        public static float Get_TrainingPolicy_Reduction_HugeSuccess_Rate(float traininPolicyValue)
        {
            switch (Get_TrainingPolicyType_By_Value(traininPolicyValue))
            {
                case TrainingPolicyType.Sparta:
                    {
                        return -0.05f;
                    }

                case TrainingPolicyType.Severe:
                    {
                        return -0.02f;
                    }

                case TrainingPolicyType.Normal:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Sweet:
                    {
                        return 0.02f;
                    }

                case TrainingPolicyType.Doting:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// 100% = 1f;
        /// </summary>
        /// <param name="traininPolicyValue"></param>
        /// <returns></returns>
        public static float Get_TrainingPolicy_Increase_Fatigue_Correction(float traininPolicyValue)
        {
            switch (Get_TrainingPolicyType_By_Value(traininPolicyValue))
            {
                case TrainingPolicyType.Sparta:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Severe:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Normal:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Sweet:
                    {
                        return 0.02f;
                    }

                case TrainingPolicyType.Doting:
                    {
                        return 0.05f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// 100% = 1f;
        /// </summary>
        /// <param name="traininPolicyValue"></param>
        /// <returns></returns>
        public static float Get_TrainingPolicy_Increase_Stress_Correction(float traininPolicyValue)
        {
            switch (Get_TrainingPolicyType_By_Value(traininPolicyValue))
            {
                case TrainingPolicyType.Sparta:
                    {
                        return 0.05f;
                    }

                case TrainingPolicyType.Severe:
                    {
                        return 0.02f;
                    }

                case TrainingPolicyType.Normal:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Sweet:
                    {
                        return 0f;
                    }

                case TrainingPolicyType.Doting:
                    {
                        return 0f;
                    }
            }

            return 0f;
        }

        #endregion

        #region Affection

        public static Vector2 Get_Affection_Value_MinMax()
        => new Vector2(0f, 100f);


        /// <summary>
        /// <para> the return value is not percent; </para>
        /// <para> 0 = +3 for giving your favorite food </para>
        /// <para> 1 = If you give something you don't like -3 </para>
        /// <para> 2 = +1 for successful training </para>
        /// <para> 3 = -1 with training failure </para>
        /// <para> 4 = +2 for a compliment </para>
        /// <para> 5 = When scolding -2 </para>
        /// <para> 6 = When training in a state of fatigue -1 </para>
        /// <para> 7 = +1 when resting in a state of fatigue/overwork </para>
        /// <para> 8 = When training in overworked conditions -3 </para>
        /// <para> 9 = -1 when training under high stress </para>
        /// <para> 10 = +1 when resting in high-stress/overstressed conditions </para>
        /// <para> 11 = When training in a state of overstress -3 </para>
        /// <para> 12 = +3 for PvE winner </para>
        /// <para> 13 = -3 in PvE lose </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Affection_IncreaseOrDecrease_Value(int changeFrom)
        {
            switch (changeFrom)
            {
                case 0:
                    {
                        return 3f;
                    }
                case 1:
                    {
                        return -3f;
                    }
                case 2:
                    {
                        return 1f;
                    }
                case 3:
                    {
                        return -1f;
                    }
                case 4:
                    {
                        return 2f;
                    }
                case 5:
                    {
                        return -2f;
                    }
                case 6:
                    {
                        return -1f;
                    }
                case 7:
                    {
                        return 1f;
                    }
                case 8:
                    {
                        return -3f;
                    }
                case 9:
                    {
                        return -1f;
                    }
                case 10:
                    {
                        return 1f;
                    }
                case 11:
                    {
                        return -3f;
                    }
                case 12:
                    {
                        return 3f;
                    }
                case 13:
                    {
                        return -3f;
                    }
            }

            return 0;
        }

        /// <summary>
        /// the return value is not percent;
        /// 0 = √ affinity % decreased from training failure rate
        /// 1 = √ affinity / 2% increased training success rate
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Affection_IncreaseOrDecrease_Value(int changeFrom, float affectionValue, float trainingPolicyvalue)
        {
            float caculatedAffection = 0f;

            switch (changeFrom)
            {
                case 0:
                    {
                        caculatedAffection = Mathf.Abs(affectionValue) * Get_TrainingPolicy_Reduction_Training_Failure_Rate(trainingPolicyvalue);

                        break;
                    }
                case 1:
                    {
                        caculatedAffection = Mathf.Abs(affectionValue) * Get_TrainingPolicy_Reduction_HugeSuccess_Rate(trainingPolicyvalue);

                        break;
                    }
            }

            return caculatedAffection;
        }

        #endregion

        #region Fatigue

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> Fatigue (51~80): +50% life consumption per action, +10% injury susceptibility, 10% reduction in basic parameters during battle </para>
        /// <para> Overwork (81~100): +100% life consumption per action, +15% injury susceptibility, 20% reduction in basic parameters during battle </para>
        /// <para> 100% = 1f </para>
        /// </summary>
        public static float Get_Fatigue_LifeSpan_Percent(FatigueType fatigueType)
        {
            switch (fatigueType)
            {
                case FatigueType.Normal:
                    {
                        return 0f;
                    }

                case FatigueType.Fatigue:
                    {
                        return 0.5f;
                    }

                case FatigueType.Overwork:
                    {
                        return 1f;
                    }
            }

            return 0f;
        }

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> Fatigue (51~80): +50% life consumption per action, +10% injury susceptibility, 10% reduction in basic parameters during battle </para>
        /// <para> Overwork (81~100): +100% life consumption per action, +15% injury susceptibility, 20% reduction in basic parameters during battle </para>
        /// </summary>
        public static Vector2 Get_Fatigue_Min_Max()
        => new Vector2(0f, 100f);

        public static float Get_Fatigue_Basic_Rest_Value()
        {
            return -30f;
        }

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> Fatigue (51~80): +50% life consumption per action, +10% injury susceptibility, 10% reduction in basic parameters during battle </para>
        /// <para> Overwork (81~100): +100% life consumption per action, +15% injury susceptibility, 20% reduction in basic parameters during battle </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static FatigueType Get_Fatigue_Type_By_Value(float fatigueValue)
        {
            FatigueType selectedType = FatigueType.Normal;

            if (fatigueValue >= 51f && fatigueValue <= 80f)
            {
                selectedType = FatigueType.Fatigue;
            }
            else if (fatigueValue >= 81f)
            {
                selectedType = FatigueType.Overwork;
            }

            return selectedType;
        }

        /// <summary>
        /// Farm fatigue reduction enhancement value × 0.3 percent reduces fatigue accumulation
        /// </summary>
        /// <param name="fatigueEnhancValue"></param>
        /// <returns></returns>
        public static float Get_Fatigue_Farm_Reduction_Enhancement_Value(float fatigueEnhancValue)
        => fatigueEnhancValue * 0.3f;

        /// <summary>
        /// <para> Farm Rest Enhancement Value×0.4 percent Improves Fatigue Recovery </para>
        /// <para> Example: If the strengthening value is 50, the recovery from fatigue by rest is 30× (100+50×0.4)/100=36. </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Fatigue_Farm_Rest_Enhancement_Value(float farmRestEnhancementValue)
        {
            float restValue = Get_Fatigue_Basic_Rest_Value();

            float result = restValue * ((100 + farmRestEnhancementValue * 0.4f) / 100f);

            return result;
        }

        /// <summary>
        /// <para> the return value is not percent </para>
        /// <para> 0 = +10 in training </para>
        /// <para> 1 = +15 in special training 1 week </para>
        /// <para> 2 = +30 in PvE </para>
        /// <para> 3 = +40 in special training 4 week </para>
        /// <para> 4 = +40 in exploration </para>
        /// <para> 5 = -15 went to the hospital </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Fatigue_IncreaseOrDecrease_Value(int changeFrom)
        {
            switch (changeFrom)
            {
                case 0:
                    {
                        return 10f;
                    }
                case 1:
                    {
                        return 15f;
                    }
                case 2:
                    {
                        return 30f;
                    }
                case 3:
                    {
                        return 40f;
                    }
                case 4:
                    {
                        return 40f;
                    }
                case 5:
                    {
                        return -15f;
                    }
            }

            return 0;
        }

        /// <summary>
        /// <para> Fatigue increase calculation formula </para>
        /// <para> Basic fatigue increase value: F(Fatigue) </para>
        /// <para> Farm fatigue reduction enhancement value: E (Enhanced value) </para> 
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para> 
        /// <para> Characteristic correction: CC (Characteristic correction) </para> 
        /// <para> Body shape correction: B (Body shape correction) </para> 
        /// <para> Correction of training policy: T (Traning policy) </para> 
        /// <para> Last increment: R(Result) </para> 
        /// <para> R = F × ( 1 - E × 0.3 / 100 + C + CC + B + T ) </para> 
        /// </summary>
        /// <param name="BasicFatigue"></param>
        /// <param name="FarmFatigueReduction"></param>
        /// <param name="mainStat"></param>
        /// <param name="subStat"></param>
        /// <param name="traitCorrection"></param>
        /// <param name="bodyTypeValue"></param>
        /// <param name="trainingPolicyCorrection"></param>
        /// <returns></returns>
        public static float Get_Fatigue_Increase_Caculated(float BasicFatigue, float FarmFatigueReduction, int mainStat, int subStat, float traitCorrection, float bodyTypeValue, float trainingPolicyValue)
        {
            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            BodyType bodyType = Get_BodyType_By_Value(bodyTypeValue);

            float bodyTypeCorrection = Get_BodyType_Increase_Fatigue_Correction(bodyTypeValue);

            float trainingPolicyCorrection = Get_TrainingPolicy_Increase_Fatigue_Correction(trainingPolicyValue);

            float result = BasicFatigue * (1f - FarmFatigueReduction * 0.3f / 100f - terrainCompatCorrection - traitCorrection + bodyTypeCorrection + trainingPolicyCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Fatigue");
            DebugPopup.LogRaise($"Basic fatigue increase value: F = {BasicFatigue}");
            DebugPopup.LogRaise($"Farm fatigue reduction boost value: E = {FarmFatigueReduction}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Trait Correction: TC = {traitCorrection}");
            DebugPopup.LogRaise($"Body Type = {bodyType}");
            DebugPopup.LogRaise($"Body Type Value = {bodyTypeValue}");
            DebugPopup.LogRaise($"Physical correction: B = {bodyTypeCorrection}");
            DebugPopup.LogRaise($"Training regimen correction: T = {trainingPolicyCorrection}");
            DebugPopup.LogRaise($"Final increment value: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> Rest calculation </para>
        /// <para> Basic fatigue recovery value: R(Rest) </para>
        /// <para> Farm rest enhancement value: E (Enhanced value) </para>
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Last recovery value: R(Result) </para>
        /// <para> R = F × ( 1 + E × 0.4 / 100 + C + CC ) × ( 1 + G ) </para>
        /// </summary>
        /// <param name="farmRestEnhancementValue"></param>
        /// <param name="characteristicCorrection"></param>
        /// <returns></returns>
        public static float Get_Fatigue_Rest_Caculated(float farmRestEnhancementValue, int mainStat, int subStat, GrowthType growthType, float currentLifeSpan, float maxLifeSpan, float characteristicCorrection)
        {
            float basicFatigueRecoveryValue = Get_Fatigue_Basic_Rest_Value();

            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float growthStageCorrection = Get_Growth_Stage_Rest_Correction(growthType, currentLifeSpan, maxLifeSpan);

            float result = basicFatigueRecoveryValue * (1f - farmRestEnhancementValue * 0.4f / 100f + terrainCompatCorrection + characteristicCorrection) * (1f + growthStageCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Fatigue Rest calculation");
            DebugPopup.LogRaise($"Basic fatigue recovery value: FR = {basicFatigueRecoveryValue}");
            DebugPopup.LogRaise($"Farm rest boost value: E = {farmRestEnhancementValue}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Trait Correction: TC = {characteristicCorrection}");
            DebugPopup.LogRaise($"Growth stage correction: G = {growthStageCorrection}");
            DebugPopup.LogRaise($"current LifeSpan = {currentLifeSpan}");
            DebugPopup.LogRaise($"max LifeSpan = {maxLifeSpan}");
            DebugPopup.LogRaise($"Final recovery value: R = {result}");

            return result;
        }

        #endregion

        #region Stress

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> Fatigue (51~80): +50% life consumption per action, +10% injury susceptibility, 10% reduction in basic parameters during battle </para>
        /// <para> Overwork (81~100): +100% life consumption per action, +15% injury susceptibility, 20% reduction in basic parameters during battle </para>
        /// <para> 100% = 1f </para>
        /// </summary>
        public static float Get_Stress_LifeSpan_Percent(StressType stressType)
        => stressType switch
        {
            StressType.Normal => 0f,
            StressType.Stress => 0.5f,
            StressType.OverStress => 1f,
            _ => 0,
        };

        public static Vector2 Stress_Min_Max => new(0f, 100f);

        public static float Stress_Basic_Rest_Value => -20f;

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> High stress(51~80): +50% lifetime consumption at each action, +5% susceptibility to disease </para>
        /// <para> Overstress(81~100): +100% lifetime consumption at each action, +10% susceptibility to disease </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static StressType Get_Stress_Type_By_Value(float stressValue)
        {
            if (stressValue >= 51f && stressValue <= 80f)
                return StressType.Stress;
            if (stressValue >= 81f)
                return StressType.Stress;
            return StressType.Normal;
        }

        /// <summary>
        /// Farm fatigue reduction enhancement value × 0.3 percent reduces fatigue accumulation
        /// </summary>
        /// <param name="fatigueEnhancValue"></param>
        /// <returns></returns>
        public static float Get_Stress_Farm_Reduction_Enhancement_Value(float stressEnhancValue) => stressEnhancValue * 0.3f;

        /// <summary>
        /// <para> Farm Rest Enhancement Value×0.4 percent Stress recovery is enhanced </para>
        /// <para> Example: If the reinforcement value is 50, the recovery of stress by rest is 20×(100+50×0.4)/100=24 </para>
        /// </summary>
        /// <returns></returns>
        public static float Get_Stress_Farm_Rest_Enhancement_Value(float farmRestEnhancementValue)
        {
            float restValue = Stress_Basic_Rest_Value;

            float result = restValue * ((100 + farmRestEnhancementValue * 0.4f) / 100f);

            return result;
        }

        /// <summary>
        /// <para> the return value is not percent </para>
        /// <para> 0 = +6 in training </para>
        /// <para> 1 = +10 in special training 1 week </para>
        /// <para> 2 = -10 for PvE winner </para>
        /// <para> 3 = +20 for PvE defeat </para>
        /// <para> 4 = +25 in special training 4 week </para>
        /// <para> 5 = +25 in exploration </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Stress_IncreaseOrDecrease_Value(int changeFrom)
        {
            return changeFrom switch
            {
                0 => 6f,
                1 => 10f,
                2 => -10f,
                3 => 20f,
                4 => 25f,
                5 => 25f,
                _ => 0,
            };
        }

        /// <summary>
        /// <para> Stress  increase calculation formula </para>
        /// <para> Basic stress increase value: S(Stress) </para>
        /// <para> Farm fatigue reduction enhancement value: E (Enhanced value) </para> 
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para> 
        /// <para> Characteristic correction: CC (Characteristic correction) </para> 
        /// <para> Body shape correction: B (Body shape correction) </para> 
        /// <para> Correction of training policy: T (Traning policy) </para> 
        /// <para> Last increment: R(Result) </para> 
        /// <para> R = F × ( 1 - E × 0.3 / 100 + C + CC + B + T ) </para> 
        /// </summary>
        /// <param name="BasicStress"></param>
        /// <param name="FarmFatigueReduction"></param>
        /// <param name="mainStat"></param>
        /// <param name="subStat"></param>
        /// <param name="characteristicCorrection"></param>
        /// <param name="bodyTypeValue"></param>
        /// <param name="trainingPolicyCorrection"></param>
        /// <returns></returns>
        public static float Get_Stress_Increase_Caculated(float BasicStress, float FarmFatigueReduction, int mainStat, int subStat, float characteristicCorrection, float bodyTypeValue, float trainingPolicyValue)
        {
            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            BodyType bodyType = Get_BodyType_By_Value(bodyTypeValue);

            float bodyTypeCorrection = Get_BodyType_Increase_Stress_Correction(bodyTypeValue);

            float trainingPolicyCorrection = Get_TrainingPolicy_Increase_Stress_Correction(trainingPolicyValue);

            float result = BasicStress * (1f - FarmFatigueReduction * 0.3f / 100f - terrainCompatCorrection - characteristicCorrection + bodyTypeCorrection + trainingPolicyCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Stress");
            DebugPopup.LogRaise($"Basic stress increase value: S = {BasicStress}");
            DebugPopup.LogRaise($"Farm fatigue reduction boost value: E = {FarmFatigueReduction}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Trait Correction: TC = {characteristicCorrection}");
            DebugPopup.LogRaise($"Body Type = {bodyType}");
            DebugPopup.LogRaise($"Body Type Value = {bodyTypeValue}");
            DebugPopup.LogRaise($"Physical correction: B = {bodyTypeCorrection}");
            DebugPopup.LogRaise($"Training regimen correction: T = {trainingPolicyCorrection}");
            DebugPopup.LogRaise($"Final increment value: R = {result}");

            return result;
        }

        /// <summary>
        /// <para> Rest calculation </para>
        /// <para> Basic stress recovery value: R(Rest) </para>
        /// <para> Farm rest enhancement value: E (Enhanced value) </para>
        /// <para> Topographic compatibility correction: C (Compatibility correction) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Last recovery value: R(Result) </para>
        /// <para> R = F × ( 1 + E × 0. 4 / 100 + C + CC ) × ( 1 + G ) </para>
        /// </summary>
        /// <param name="farmRestEnhancementValue"></param>
        /// <param name="characteristicCorrection"></param>
        /// <returns></returns>
        public static float Get_Stress_Rest_Caculated(float farmRestEnhancementValue, int mainStat, int subStat, GrowthType growthType, float currentLifeSpan, float maxLifeSpan, float characteristicCorrection)
        {
            float basicStressRecoveryValue = Stress_Basic_Rest_Value;

            float terrainCompatCorrection = Farm.Get_Terrain_Compatibility_Correction(mainStat, subStat);

            float growthStageCorrection = Get_Growth_Stage_Rest_Correction(growthType, currentLifeSpan, maxLifeSpan);

            float result = basicStressRecoveryValue * (1f - farmRestEnhancementValue * 0.4f / 100f + terrainCompatCorrection + characteristicCorrection) * (1f + growthStageCorrection);

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Stress Rest calculation");
            DebugPopup.LogRaise($"Basic fatigue recovery value: FR = {basicStressRecoveryValue}");
            DebugPopup.LogRaise($"Farm rest boost value: E = {farmRestEnhancementValue}");
            DebugPopup.LogRaise($"Terrain compatibility correction: C = {terrainCompatCorrection}");
            DebugPopup.LogRaise($"Trait Correction: TC = {characteristicCorrection}");
            DebugPopup.LogRaise($"Growth stage correction: G = {growthStageCorrection}");
            DebugPopup.LogRaise($"current LifeSpan = {currentLifeSpan}");
            DebugPopup.LogRaise($"max LifeSpan = {maxLifeSpan}");
            DebugPopup.LogRaise($"Final recovery value: R = {result}");

            return result;
        }

        #endregion

        #region Training
        #endregion

        #region Injuries

        /// <summary>
        /// <para> 100% = 1f;</para>
        /// <para> 0 = Basic training failure , </para>
        /// <para> 1 = Special training failure, </para>
        /// <para> 2 = Intensive training, </para>
        /// <para> 3 = Exploration, </para>
        /// <para> 4 = Battle (basic),</para>
        /// <para> 5 = Over 30% damage in a single battle </para>
        /// <para> 6 = Over 50% damage in a single battle </para>
        /// <para> 7 = Over 80% damage in a single battle </para>
        /// <para> 8 = Over 100% damage in a single battle </para>
        /// <para> 9 = HP goes to 0 in Battle </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Injuries_Probability(int changeFrom)
        => changeFrom switch
        {
            0 => 0.03f,
            1 => 0.05f,
            2 => 0.02f,
            3 => 0.01f,
            4 => 0.03f,
            5 => 0.05f,
            6 => 0.07f,
            7 => 0.1f,
            8 => 0.15f,
            9 => 0.15f,
            _ => 0,
        };

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> High stress(51~80): +50% lifetime consumption at each action, +5% susceptibility to disease </para>
        /// <para> Overstress(81~100): +100% lifetime consumption at each action, +10% susceptibility to disease </para>
        /// <para> 100% = 100f </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static Vector2 Injury_Min_Max => new(0f, 100f);

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> High stress(51~80): +50% lifetime consumption at each action, +5% susceptibility to disease </para>
        /// <para> Overstress(81~100): +100% lifetime consumption at each action, +10% susceptibility to disease </para>
        /// <para> 100% = 100f </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static float Get_Injury_Increase_Probability_By_Fatigue(FatigueType fatigueType)
        => fatigueType switch
        {
            FatigueType.Fatigue => 10f,
            FatigueType.Overwork => 15f,
            _ => 0f,
        };

        /// <summary>
        /// <para> it is percent rate of Injury 100% = 1f</para>
        /// <para> it is percent rate of serious Injury 100% = 1f </para>
        /// </summary>
        /// <param name="injuryRate"></param>
        /// <param name="seriousInjuryRate"></param>
        /// <returns></returns>
        public static InjuryType Get_Injury_Caculate_Is_Injury(InjuryType currentInjuryType, float injuryRate, float seriousInjuryRate)
        {
            float ranNum = UnityEngine.Random.Range(0f, 100f);

            ranNum /= 100f;

            Debug.Log("injuryRate = " + injuryRate);
            Debug.Log("seriousInjuryRate = " + seriousInjuryRate);
            Debug.Log("injury ranNum = " + ranNum);

            DebugPopup.LogRaise("Ran value = " + ranNum);

            if (currentInjuryType == InjuryType.None)
            {
                if (ranNum <= injuryRate)
                    return InjuryType.Injury;
                if (ranNum <= injuryRate + seriousInjuryRate)
                    return InjuryType.Serious_Injury;
                return InjuryType.None;
            }
            if (currentInjuryType == InjuryType.Injury)
            {
                if (ranNum <= injuryRate + seriousInjuryRate)
                    return InjuryType.Serious_Injury;
                return InjuryType.Injury;
            }
            return InjuryType.Serious_Injury;
        }

        /// <summary>
        /// <para> Injury probability calculation formula </para>
        /// <para> Basic injury probability: BPI (Basic probability of injury) </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Fatigue correction: FC (Fatigue correction) formula </para>
        /// <para> Correction by damage taken: CD (Correction by taking damage) </para>
        /// <para> Probability of injury (PI) </para>
        /// <para> Probability of serious injury (PS) </para>
        /// <para> PI = BPI + FC + CD × ( 1 + CC ) </para>
        /// <para> PS = PI / 3 </para>
        /// <para> basicProbabilityInjury 100% = 1f </para>
        /// <para> return value 100% = 1f </para>
        /// </summary>
        /// <returns></returns>
        public static Vector2 Get_Injury_Probability_Rate(float basicProbabilityInjury, float traitCorrection, FatigueType fatigueType, float correctionByTakingDamage)
        {
            float fatigueCorrection = Get_Injury_Increase_Probability_By_Fatigue(fatigueType) / 100f;

            float injuryChance = (basicProbabilityInjury + fatigueCorrection + correctionByTakingDamage) * (1f + traitCorrection);

            float seriousInjuryChance = injuryChance / 3f;

            if (injuryChance < 0f)
                injuryChance = 0f;

            if (seriousInjuryChance < 0f)
                seriousInjuryChance = 0f;

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Injury");
            DebugPopup.LogRaise($"Basic probability of injury: BPI = {basicProbabilityInjury}");
            DebugPopup.LogRaise($"Trait Correction: TC = {traitCorrection}");
            DebugPopup.LogRaise($"Fatigue correction: FC= {fatigueCorrection}");
            DebugPopup.LogRaise($"Correction by taking damage: CD = {correctionByTakingDamage}");
            DebugPopup.LogRaise($"Probability of injury: PI = {injuryChance}");
            DebugPopup.LogRaise($"Probability of serious injury: PS = {seriousInjuryChance}");

            return new Vector2(injuryChance, seriousInjuryChance);
        }

        public static float Get_Injury_Pass_Week(InjuryType injuryType)
        => injuryType switch
        {
            InjuryType.Injury => 1,
            InjuryType.Serious_Injury => 3,
            _ => (float)0,
        };

        #endregion

        #region Disease

        /// <summary>
        /// <para> 100% = 1f;</para>
        /// <para> Sick Monsters experience a 10% decrease in basic parameters when calculating Training, Exploration, Battle, etc. </para>
        /// </summary>
        /// <param name="changeFrom"> 
        /// </param>
        /// <returns></returns>
        public static float Get_Disease_Correction()
        {
            return 0.1f;
        }

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> High stress(51~80): +50% lifetime consumption at each action, +5% susceptibility to disease </para>
        /// <para> Overstress(81~100): +100% lifetime consumption at each action, +10% susceptibility to disease </para>
        /// <para> 100% = 100f </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static Vector2 Get_Disease_Min_Max()
        => new Vector2(0f, 100f);

        /// <summary>
        /// <para> Normal (0~50): No change </para>
        /// <para> High stress (51-80): life consumption +50% for each action, susceptibility to illness +5% </para>
        /// <para> Overstress (81-100): Lifespan consumption +100% for each action, susceptibility to illness +10% </para>
        /// <para> 100% = 100f </para>
        /// </summary>
        /// <param name="changeFrom"></param>
        /// <returns></returns>
        public static float Get_Disease_Increase_Probability_By_Stress(StressType stressType)
        => stressType switch
        {
            StressType.Stress => 5f,
            StressType.OverStress => 10f,
            _ => 0,
        };

        /// <summary>
        /// <para> it is percent rate of Disease 100% = 100f </para>
        /// <para> it is percent rate of serious Disease 100% = 100f </para>
        /// </summary>
        /// <param name="diseaseRate">  </param>
        /// <param name="seriousDiseaseRate">  </param>
        /// <returns></returns>
        public static DiseasesType Get_Disease_Caculate_Is_Disease(DiseasesType currentDiseasesType, float diseaseRate, float seriousDiseaseRate)
        {
            Debug.Log("diseaseRate = " + diseaseRate + @"%");
            Debug.Log("seriousDiseaseRate = " + seriousDiseaseRate + @"%");

            float ranNum = UnityEngine.Random.Range(0f, 100f);

            //DebugPopup.LogRaise($"Disease Rannum = " + ranNum);
            //DebugPopup.LogRaise($"diseaseRate = " + diseaseRate);
            //DebugPopup.LogRaise($"seriousDiseaseRate = " + seriousDiseaseRate);

            if (currentDiseasesType == DiseasesType.None)
            {
                if (ranNum <= diseaseRate)
                    return DiseasesType.Diseases;
                if (ranNum <= diseaseRate + seriousDiseaseRate)
                    return DiseasesType.Serious_Diseases;
                return DiseasesType.None;
            }
            if (currentDiseasesType == DiseasesType.None)
            {
                if (ranNum <= diseaseRate + seriousDiseaseRate)
                    return DiseasesType.Serious_Diseases;
                return DiseasesType.Diseases;
            }
            return DiseasesType.Serious_Diseases;
        }

        /// <summary>
        /// <para> The probability of getting sick fluctuates depending on nutritional status </para>
        /// <para> There are three types of nutritional status: Energy, Body, and Condition, each starting at 80. </para>
        /// <para> Each nutritional situation is reduced by 5 every week </para>
        /// <para> Each nutritional status fluctuates depending on the diet and nurturing items. </para>
        /// <para> Disease probability calculation formula </para>
        /// <para> Energy：E </para>
        /// <para> Body：B </para>
        /// <para> Condition：C </para>
        /// <para> Number of Sick Monsters: N </para>
        /// <para> Characteristic correction: CC (Characteristic correction) </para>
        /// <para> Stress correction: SC (Stress correction) </para>
        /// <para> Disease Probability: PS (Probability of sickness) </para>
        /// <para> Probability of illness (PI) </para>
        /// <para> PS = ( 1 - ( E + B + C ) × ( 1 - 0.1 × N ) / 192 + SC ) × ( 1 + CC ) </para>
        /// <para> PI = PS / 3 </para>
        /// <para> return value 100% = 100f </para>
        /// </summary>
        /// <param name="energy"> </param>
        /// <returns></returns>
        public static Vector2 Get_Disease_Probability_Rate(float energy, float body, float condition, float numberOfSickMonster, float traitCorrection, StressType stressType)
        {
            float stressCorrection = Get_Disease_Increase_Probability_By_Stress(stressType) / 100f;

            float healthStandardValue = 192f;

            float diseaseChance = (1f - (energy + body + condition) * (1f - 0.1f * numberOfSickMonster) / healthStandardValue + stressCorrection) * (1f - traitCorrection);

            float seriousIllChance = diseaseChance / 3f;

            DebugPopup.LogRaise("//////////");
            DebugPopup.LogRaise("Disease");
            DebugPopup.LogRaise($"Energy: E = {energy}");
            DebugPopup.LogRaise($"Body: B = {body}");
            DebugPopup.LogRaise($"Condition: C = {condition}");
            DebugPopup.LogRaise($"Number of sick monsters: N = {numberOfSickMonster}");
            DebugPopup.LogRaise($"Health standard value = {healthStandardValue}");
            DebugPopup.LogRaise($"Trait Correction: TC = {traitCorrection}");
            DebugPopup.LogRaise($"Stress correction: SC = {stressCorrection}");
            DebugPopup.LogRaise($"Disease probability: PS = {diseaseChance}");
            DebugPopup.LogRaise($"Probability of serious illness: PI = {seriousIllChance}");

            return new Vector2(diseaseChance * 100f, seriousIllChance * 100f);
        }

        public static float Get_Disease_Pass_Week(DiseasesType diseasesType)
        => diseasesType switch
        {
            DiseasesType.Diseases => 1,
            DiseasesType.Serious_Diseases => 3,
            _ => (float)0,
        };

        #endregion
    }
}
