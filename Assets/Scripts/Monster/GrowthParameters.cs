﻿using System;
using UnityEngine;

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class GrowthParameters
    {
        public float lifeSpan = 0f;

        /// <summary>
        /// default = 50f
        /// max = 100f
        /// 4.1.3 Body Type in english document
        /// </summary>
        //[Range(0f,100f)]
        public float bodyTypeValue = 0f;

        /// <summary>
        /// default = 50f
        /// max = 100f
        /// 4.1.4 Training Policy in english document
        /// </summary>
        //[Range(0f,100f)]
        public float trainingPolicyValue = 0f;

        //[Range(0f, 100f)]
        public float affection = 0f;

        /// <summary>
        /// default = 50f
        /// max = 100f
        /// 4.1.6 Fatigue in english document
        /// </summary>
        //[Range(0f, 100f)]
        public float fatigue = 0f;

        /// <summary>
        /// default = 50f
        /// max = 100f
        /// 4.1.7 Stress in english document
        /// </summary>
        //[Range(0f, 100f)]
        public float stress = 0f;

        public Monster.InjuryType injuryType = Monster.InjuryType.None;

        public Monster.DiseasesType diseasesType = Monster.DiseasesType.None;

        public float energy = 0f;

        public float body = 0f;

        public float condition = 0f;

        public GrowthParameters()
        {
            lifeSpan = 0f;

            bodyTypeValue = 0f;

            trainingPolicyValue = 0f;

            affection = 0f;

            fatigue = 0f;

            stress = 0f;

            injuryType = Monster.InjuryType.None;

            diseasesType = Monster.DiseasesType.None;

            energy = 0f;

            body = 0f;

            condition = 0f;
        }

        public GrowthParameters Clone()
        {
            GrowthParameters newGrowthParameters = new()
            {
                lifeSpan = lifeSpan,

                bodyTypeValue = bodyTypeValue,

                trainingPolicyValue = trainingPolicyValue,

                affection = affection,

                fatigue = fatigue,

                stress = stress,

                injuryType = injuryType,

                energy = energy,

                body = body,

                condition = condition
            };

            return newGrowthParameters;
        }

        public void AddLifeSpan(float value)
        {
            lifeSpan = Mathf.Clamp(lifeSpan + value, 0f, lifeSpan + value);
        }

        public void AddBodyTypeValue(float value)
        {
            bodyTypeValue = Mathf.Clamp(bodyTypeValue + value, 0f, 100f);
        }

        public void AddTrainingPolicyValue(float value)
        {
            trainingPolicyValue = Mathf.Clamp(trainingPolicyValue + value, 0f, 100f);
        }

        public void AddAffection(float value)
        {
            affection = Mathf.Clamp(affection + value, 0f, 100f);
        }

        public void AddFatigue(float value)
        {
            fatigue = Mathf.Clamp(fatigue + value, 0f, 100f);
        }

        public void AddStress(float value)
        {
            stress = Mathf.Clamp(stress + value, 0f, 100f);
        }

        public void AddEnergy(float value)
        {
            energy = Mathf.Clamp(energy + value, 0f, 100f);
        }

        public void AddBody(float value)
        {
            body = Mathf.Clamp(body + value, 0f, 100f);
        }

        public void AddCondition(float value)
        {
            condition = Mathf.Clamp(condition + value, 0f, 100f);
        }
    }
}