using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster
{
    [CreateAssetMenu(fileName = "MonsterActionDataManagerScriptObj", menuName = "ScriptableObjects/ActionData/MonsterActionDataManagerScriptObj", order = 1)]
    public class MonsterActionDataManagerScriptObj : ScriptableObject
    {
        public Dictionary<MonsterAction.MonsterActionType, MonsterAction> monsterActionDictionary = new Dictionary<MonsterAction.MonsterActionType, MonsterAction>();

        [SerializeField]
        List<MonsterAction> monsterActionList = new List<MonsterAction>();

        public MonsterAction GetAction(MonsterAction.MonsterActionType ennumType)
        {
            MonsterAction action = null;

            if (!monsterActionDictionary.TryGetValue(ennumType, out action))
            {
                // the key isn't in the dictionary.
                // or whatever you want to do
            }
            else
            {

            }

            return action;
        }

        private void OnValidate()
        {
            if (Application.isEditor && Application.isPlaying == false)
            {
                //Debug.Log("OnValidate");

                CreateActionDictionary();

                foreach (MonsterAction a in monsterActionList)
                {
                    GetAction(a.monsterActionType);
                }
            }
        }

        public void CreateActionDictionary()
        {
            if (monsterActionList.Count > 0)
            {
                monsterActionDictionary.Clear();

                for (int i = 0; i < monsterActionList.Count; i++)
                {
                    monsterActionDictionary.Add(monsterActionList[i].monsterActionType, monsterActionList[i]);
                }
            }
        }
    }
}