﻿using UnityEngine;

namespace Assets.Scripts.Monster.Trait
{
    public class InnateTrait : MonoBehaviour
    {
        public enum InnateTraitType
        {
            None,
            Healthy_Body,
            Iron_Heart,
            Agility,
            Insight,
            Hyper_Reactivity,
            Sniper,
            Surviver,
            Armor_Piercing,
            Flame_Aptitude,
            Water_Aptitude,
            Wind_Aptitude,
            Earth_Aptitude,
            Slashing_Aptitude,
            Blunt_Aptitude,
            Thrust_Aptitude,
            Flame_Mastery,
            Water_Mastery,
            Wind_Mastery,
            Earth_Mastery,
            Slash_Mastery,
            Blunt_Mastery,
            Thrust_Mastery,
            Fountain_Of_Wisdom,
            Converging_Muscles,
            Take_A_Chance,
            Healing_Wave,
            Support_Master,
            Contrarian,
            Elemental_Body,
            Flexible_Body,
            Strong_Heart,
            Lightning,
            Concentration,
            Longevity,
            Talent_For_Sleep,
            Treasure_Hunter,
            Genius,
            Adaptability,
            Inheritance,
            Coaching
        }

        public class InnateTraitInfo
        {
            public InnateTraitType innateTraitType = InnateTraitType.None;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float DeBuff_Success_Rate = 0f;

            #region Battle
            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Random_Number = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Taken_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Critical_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Critical = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Taken_Critical_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Evasion = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Accurancy = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float STR_Skill_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float INT_Skill_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float HP_Recovery_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float ST_Correction = 0;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float ST_Recovery_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float ST_Consumption_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float SPD_Correction = 0f;

            #endregion Battle

            #region Buff
            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Buff_Effect_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float DeBuff_Effect_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Physical_Debuff_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Psychich_Debuff_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Sleep_Resistance = 0f;

            #endregion Buff

            #region Training
            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Lifespan = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Training_Fatigue_Reduction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Training_Stress_Reduction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Injury_Probability = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Disease_Probability = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Training_Success_Rate = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Training_Fail_Rate = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Rest_Effect_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Find_Item_Rate = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Item_Rank_Up_Rate = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Skill_Level_Up_Rate = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Acquired_Trait_Level_Up_Rate = 0f;

            #endregion Training

            #region Fusion

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Parameter_Crystalized_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Parameter_Coached_Correction = 0f;

            #endregion

            #region Attribut Damage Correction

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Fire_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Water_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Wind_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Earth_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Slash_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Blunt_Damage_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Thrust_Damage_Correction = 0f;

            #endregion Attribut Damage Correction

            #region Attribut Resistance Correction

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Fire_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Water_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Wind_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Earth_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Slash_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Blunt_Resistance_Correction = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float Thrust_Resistance_Correction = 0f;

            #endregion Attribut Resistance Correction
        }

        public static InnateTraitInfo GetInnateTraitInfo(InnateTraitType innateTraitType)
        {
            InnateTraitInfo innateTraitInfo = new InnateTraitInfo();
            innateTraitInfo.innateTraitType = InnateTraitType.None;

            return innateTraitInfo;

            /*switch (innateTraitType)
            {
                case InnateTraitType.Healthy_Body:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Healthy_Body;

                        innateTraitInfo.Physical_Debuff_Resistance = 0.15f;

                        innateTraitInfo.Training_Fatigue_Reduction = 0.05f;

                        innateTraitInfo.Disease_Probability = -0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Iron_Heart:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Iron_Heart;

                        innateTraitInfo.Psychich_Debuff_Resistance = 0.15f;

                        innateTraitInfo.Training_Fatigue_Reduction = 0.05f;

                        innateTraitInfo.Disease_Probability = -0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Agility:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Agility;

                        innateTraitInfo.Evasion = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Insight:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Insight;

                        innateTraitInfo.Accurancy = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Hyper_Reactivity:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Hyper_Reactivity;

                        innateTraitInfo.Evasion = 0.1f;

                        innateTraitInfo.Accurancy = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Sniper:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Sniper;

                        innateTraitInfo.Accurancy = 0.1f;

                        innateTraitInfo.Critical = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Surviver:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Surviver;

                        innateTraitInfo.Evasion = 0.15f;

                        innateTraitInfo.Taken_Critical_Correction = -0.03f;

                        innateTraitInfo.Taken_Damage_Correction = -0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Armor_Piercing:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Armor_Piercing;

                        innateTraitInfo.Critical = 0.05f;

                        innateTraitInfo.Critical_Damage_Correction = 0.2f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Flame_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Flame_Aptitude;

                        innateTraitInfo.Fire_Damage_Correction = 0.1f;

                        innateTraitInfo.Fire_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Water_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Water_Aptitude;

                        innateTraitInfo.Water_Damage_Correction = 0.1f;

                        innateTraitInfo.Water_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Wind_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Wind_Aptitude;

                        innateTraitInfo.Wind_Damage_Correction = 0.1f;

                        innateTraitInfo.Wind_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Earth_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Earth_Aptitude;

                        innateTraitInfo.Earth_Damage_Correction = 0.1f;

                        innateTraitInfo.Earth_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Slashing_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Slashing_Aptitude;

                        innateTraitInfo.Slash_Damage_Correction = 0.1f;

                        innateTraitInfo.Slash_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Blunt_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Blunt_Aptitude;

                        innateTraitInfo.Blunt_Damage_Correction = 0.1f;

                        innateTraitInfo.Blunt_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Thrust_Aptitude:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Thrust_Aptitude;

                        innateTraitInfo.Thrust_Damage_Correction = 0.1f;

                        innateTraitInfo.Thrust_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Flame_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Flame_Mastery;

                        innateTraitInfo.Fire_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Water_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Water_Mastery;

                        innateTraitInfo.Water_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Wind_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Wind_Mastery;

                        innateTraitInfo.Wind_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Earth_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Earth_Mastery;

                        innateTraitInfo.Earth_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Slash_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Slash_Mastery;

                        innateTraitInfo.Slash_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Blunt_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Blunt_Mastery;

                        innateTraitInfo.Blunt_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Thrust_Mastery:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Thrust_Mastery;

                        innateTraitInfo.Thrust_Damage_Correction = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Fountain_Of_Wisdom:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Fountain_Of_Wisdom;

                        innateTraitInfo.INT_Skill_Damage_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Converging_Muscles:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Converging_Muscles;

                        innateTraitInfo.STR_Skill_Damage_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Take_A_Chance:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Take_A_Chance;

                        innateTraitInfo.INT_Skill_Damage_Correction = 0.1f;

                        innateTraitInfo.STR_Skill_Damage_Correction = 0.1f;

                        innateTraitInfo.Random_Number = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Healing_Wave:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Healing_Wave;

                        innateTraitInfo.HP_Recovery_Correction = 0.1f;

                        innateTraitInfo.ST_Recovery_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Support_Master:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Support_Master;

                        innateTraitInfo.Buff_Effect_Correction = 0.2f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Contrarian:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Contrarian;

                        innateTraitInfo.DeBuff_Effect_Correction = 0.2f;

                        innateTraitInfo.DeBuff_Success_Rate = 0.15f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Elemental_Body:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Elemental_Body;

                        innateTraitInfo.Fire_Resistance_Correction = 0.1f;

                        innateTraitInfo.Water_Resistance_Correction = 0.1f;

                        innateTraitInfo.Wind_Resistance_Correction = 0.1f;

                        innateTraitInfo.Earth_Resistance_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Flexible_Body:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Flexible_Body;

                        innateTraitInfo.Blunt_Resistance_Correction = 0.12f;

                        innateTraitInfo.Slash_Resistance_Correction = 0.12f;

                        innateTraitInfo.Thrust_Resistance_Correction = 0.12f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Strong_Heart:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Strong_Heart;

                        innateTraitInfo.ST_Recovery_Correction = 0.1f;

                        innateTraitInfo.Training_Fatigue_Reduction = 0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Lightning:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Lightning;

                        innateTraitInfo.SPD_Correction = 0.15f;

                        innateTraitInfo.ST_Consumption_Correction = -0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Concentration:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Concentration;

                        innateTraitInfo.Training_Success_Rate = 0.05f;

                        innateTraitInfo.Training_Fail_Rate = -0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Longevity:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Longevity;

                        innateTraitInfo.Lifespan = 300f;

                        innateTraitInfo.Physical_Debuff_Resistance = 0.05f;

                        innateTraitInfo.Psychich_Debuff_Resistance = 0.05f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Talent_For_Sleep:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Talent_For_Sleep;

                        innateTraitInfo.Rest_Effect_Correction = 0.1f;

                        innateTraitInfo.Sleep_Resistance = 0.2f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Treasure_Hunter:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Treasure_Hunter;

                        innateTraitInfo.Find_Item_Rate = 0.1f;

                        innateTraitInfo.Item_Rank_Up_Rate = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Genius:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Genius;

                        innateTraitInfo.Find_Item_Rate = 0.1f;

                        innateTraitInfo.Item_Rank_Up_Rate = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Adaptability:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Adaptability;

                        innateTraitInfo.Acquired_Trait_Level_Up_Rate = 0.1f;

                        innateTraitInfo.Skill_Level_Up_Rate = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Inheritance:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Inheritance;

                        innateTraitInfo.Parameter_Crystalized_Correction = 0.1f;

                        return innateTraitInfo;
                    }

                case InnateTraitType.Coaching:
                    {
                        innateTraitInfo = new InnateTraitInfo();

                        innateTraitInfo.innateTraitType = InnateTraitType.Coaching;

                        innateTraitInfo.Parameter_Coached_Correction = 0.1f;

                        return innateTraitInfo;
                    }
            }

            return innateTraitInfo;*/
        }
    }
}