﻿using System;

namespace Assets.Scripts.Monster
{
    [Serializable]
    public class BasicParameters
    {
        public float health = 0;

        public float strenght = 0f;

        public float intelligent = 0f;

        public float dexterity = 0f;

        public float agility = 0f;

        public float vitality = 0f;

        public BasicParameters()
        {
            health = 0;

            strenght = 0f;

            intelligent = 0f;

            dexterity = 0f;

            agility = 0f;

            vitality = 0f;
        }

        public BasicParameters(float health, float strenght, float intelligent, float dexterity, float agility, float vitality)
        {
            this.health = health;

            this.strenght = strenght;

            this.intelligent = intelligent;

            this.dexterity = dexterity;

            this.agility = agility;

            this.vitality = vitality;
        }

        public BasicParameters Clone()
        {
            BasicParameters newParameters = new()
            {
                health = health,

                strenght = strenght,

                intelligent = intelligent,

                dexterity = dexterity,

                agility = agility,

                vitality = vitality
            };

            return newParameters;
        }

        public float GetTotal()
        {
            return health + strenght + intelligent + dexterity + agility + vitality;
        }
    }
}