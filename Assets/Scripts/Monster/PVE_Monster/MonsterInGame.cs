using UnityEngine;

namespace Assets.Scripts.Monster.PVE_Monster
{
    public class MonsterInGame : MonoBehaviour
    {
        [SerializeField] private int inGameID = -1;
        private GridUnit unit;
        private ParameterInGame inGameParameter;
        private UnitAIComponent unitAi;
        private AilmentContainer alimentContainer;

        public BuffSpriteGO buffSpriteGOPrefab;

        public Transform uiStatsPos;

        private PlayerInfo.PlayerMonsterInfo monsterInfo;

        public PlayerInfo.PlayerMonsterInfo MonsterInfo => monsterInfo;
        public GridUnit Unit => unit;
        public ParameterInGame InGameParameter => inGameParameter;
        public UnitAIComponent UnitAi => unitAi;

        public int GetBattleID() => inGameID;

        public void SetGameID(int idNum)
        {
            inGameID = idNum;
        }

        public void SetMonsterInfo(PlayerInfo.PlayerMonsterInfo playerMonsterInfo) => monsterInfo = playerMonsterInfo;
        internal void SetGridUnit(GridUnit unit) => this.unit = unit;
        internal void SetGameParameter(ParameterInGame inGameParameter) => this.inGameParameter = inGameParameter;
        internal void SetUnitAI(UnitAIComponent unitAi) => this.unitAi = unitAi;

        public BuffSpriteGO SpawnBuffFX(ParameterInGame parameterInGame)
        {
            BuffSpriteGO spawned = Instantiate(buffSpriteGOPrefab, parameterInGame.transform);

            spawned.transform.localPosition = Vector3.zero;
            spawned.transform.rotation = Quaternion.identity;

            Vector3 parentScale = spawned.transform.parent.localScale;
            Vector3 selfScale = new Vector3(spawned.transform.localScale.x / parentScale.x, spawned.transform.localScale.y / parentScale.y, spawned.transform.localScale.z / parentScale.z);
            spawned.transform.localScale = selfScale;

            return spawned;
        }
    }
}