﻿using UnityEngine;

namespace Assets.Scripts.Monster.Trait
{
    public class PersonalityTrait
    {
        public enum PersonalityTraitType
        {
            None,
            冷静_Cool,
            陽気_Jolly,
            紳士_Gentle,
            のんびり_Lazy,
            ナイーブ_Naive,
            シャイ_Shy,
            頑固_Stubborn,
            好奇心旺盛_Curious,
            献身的_Devoted,
            食いしん坊_Foodie,
            勇敢_Brave,
            感情的_Emotional,
            冒険家_Adventurer,
            忠実_Loyal,
            真面目_Serious,
            夢見がち_Dreamer,
            無邪気_Innocent,
            淑やか_Graceful,
            あわてんぼう_Panicky,
            楽天家_Optimist
        }

        [System.Serializable]
        public class PersonalityTraitInfo
        {
            public PersonalityTraitType personalityTraitType = PersonalityTraitType.None;

            /// <summary>
            /// <para> Values in side are percentage 100% = 1f </para>
            /// </summary>
            [Header("Values in side are percentage 100% = 1f")]
            private BasicParameters trainingCorrection = new BasicParameters();

            public PersonalityTraitInfo()
            {
                personalityTraitType = PersonalityTraitType.None;

                trainingCorrection.health = 0;

                trainingCorrection.strenght = 0f;

                trainingCorrection.intelligent = 0f;

                trainingCorrection.dexterity = 0f;

                trainingCorrection.agility = 0f;

                trainingCorrection.vitality = 0f;
            }

            public PersonalityTraitInfo(PersonalityTraitType traitType, float health, float strenght, float intelligent, float dexterity, float agility, float vitality)
            {
                personalityTraitType = traitType;

                trainingCorrection.health = health / 100f;

                trainingCorrection.strenght = strenght / 100f;

                trainingCorrection.intelligent = intelligent / 100f;

                trainingCorrection.dexterity = dexterity / 100f;

                trainingCorrection.agility = agility / 100f;

                trainingCorrection.vitality = vitality / 100f;
            }

            public BasicParameters TrainingCorrection { get => trainingCorrection; set => trainingCorrection = value; }
        }

        public static PersonalityTraitInfo GetTraitInfo(PersonalityTraitType personalityTraitType)
        {
            return new PersonalityTraitInfo();

            /*switch (personalityTraitType)
            {
                case PersonalityTraitType.冷静_Cool:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.冷静_Cool, 0f, 0f, 6f, 4f, 0f, 0f
                            );
                    }

                case PersonalityTraitType.陽気_Jolly:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.陽気_Jolly, 4f, 6f, 0f, 0f, 0f, 0f
                            );
                    }

                case PersonalityTraitType.紳士_Gentle:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.紳士_Gentle, 0f, 0f, 4f, 6f, 0f, 0f
                            );
                    }

                case PersonalityTraitType.のんびり_Lazy:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.のんびり_Lazy, 5f, 0f, 0f, 0f, 0f, 5f
                            );
                    }

                case PersonalityTraitType.ナイーブ_Naive:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.ナイーブ_Naive, 0f, 0f, 4f, 3f, 3f, 0f
                            );
                    }

                case PersonalityTraitType.シャイ_Shy:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.シャイ_Shy, 0f, 3f, 3f, 0f, 4f, 0f
                            );
                    }

                case PersonalityTraitType.頑固_Stubborn:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.頑固_Stubborn, 3f, 0f, 0f, 0f, 0f, 7f
                            );
                    }

                case PersonalityTraitType.好奇心旺盛_Curious:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.好奇心旺盛_Curious, 0f, 2f, 2f, 2f, 2f, 2f
                            );
                    }

                case PersonalityTraitType.献身的_Devoted:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.献身的_Devoted, 4f, 0f, 0f, 3f, 0f, 3f
                            );
                    }

                case PersonalityTraitType.食いしん坊_Foodie:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.食いしん坊_Foodie, 4f, 0f, 0f, 2f, 0f, 4f
                            );
                    }

                case PersonalityTraitType.勇敢_Brave:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.勇敢_Brave, 0f, 3f, 3f, 2f, 2f, 0f
                            );
                    }

                case PersonalityTraitType.感情的_Emotional:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.感情的_Emotional, 0f, 5f, 5f, 0f, 0f, 0f
                            );
                    }

                case PersonalityTraitType.冒険家_Adventurer:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.冒険家_Adventurer, 3f, 0f, 3f, 0f, 2f, 2f
                            );
                    }

                case PersonalityTraitType.忠実_Loyal:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.忠実_Loyal, 2f, 2f, 2f, 0f, 4f, 0f
                            );
                    }

                case PersonalityTraitType.真面目_Serious:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.真面目_Serious, 0f, 2f, 2f, 1f, 4f, 1f
                            );
                    }

                case PersonalityTraitType.夢見がち_Dreamer:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.夢見がち_Dreamer, 4f, 4f, 0f, 2f, 0f, 0f
                            );
                    }

                case PersonalityTraitType.無邪気_Innocent:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.無邪気_Innocent, 0f, 2f, 0f, 1f, 5f, 2f
                            );
                    }

                case PersonalityTraitType.淑やか_Graceful:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.淑やか_Graceful, 2f, 0f, 3f, 2f, 0f, 3f
                            );
                    }

                case PersonalityTraitType.あわてんぼう_Panicky:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.あわてんぼう_Panicky, 1f, 3f, 0f, 0f, 4f, 2f
                            );
                    }

                case PersonalityTraitType.楽天家_Optimist:
                    {
                        return new PersonalityTraitInfo(
                            PersonalityTraitType.楽天家_Optimist, 1f, 2f, 0f, 2f, 3f, 2f
                            );
                    }
            }

            return new PersonalityTraitInfo();*/
        }
    }
}