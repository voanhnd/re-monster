using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Monster.Trait
{
    public class AcquiredTrait : MonoBehaviour
    {
        public enum AcquiredTraitType
        {
            None,
            HP_Plus,
            STR_Plus,
            INT_Plus,
            DEX_Plus,
            AGI_Plus,
            VIT_Plus,
            HIT_Plus,
            AVD_Plus,
            CRI_Plus,
            RES_Plus,
            MND_Plus,
            ST_Plus,
            SPD_Plus,
            Fire_Resistance_Plus,
            Water_Resistance_Plus,
            Wind_Resistance_Plus,
            Earth_Resistance_Plus,
            Slash_Resistance_Plus,
            Blunt_Resistance_Plus,
            Thrust_Resistance_Plus
        }

        [System.Serializable]
        public class PlayerInfoAcquiredTrait
        {
            public int traitLevel = 0;
            public AcquiredTraitType acquiredTraitType = AcquiredTraitType.None;

            public PlayerInfoAcquiredTrait()
            {
                traitLevel = 0;
                acquiredTraitType = AcquiredTraitType.None;
            }

            public PlayerInfoAcquiredTrait(int level, AcquiredTraitType acquiredTraitType)
            {
                traitLevel = level;

                this.acquiredTraitType = acquiredTraitType;
            }
        }

        public class AcquiredTraitInfo
        {
            public AcquiredTraitType acquiredTraitType = AcquiredTraitType.None;

            public float health = 0;

            public float strenght = 0f;

            public float intelligent = 0f;

            public float dexterity = 0f;

            public float agility = 0f;

            public float vitality = 0f;

            public float battle_Start_Stamina = 0f;

            public float speed = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float accuracy = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float evasion = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float critical = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float physic_Debuff_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float psychic_Debuff_Resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float fire_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float water_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float wind_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float earth_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float slash_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float blunt_resistance = 0f;

            /// <summary>
            /// <para> Value is percentage 100% = 1f </para>
            /// </summary>
            public float thrust_resistance = 0f;
        }

        public static AcquiredTraitInfo GetAcquiredTraitInfo(AcquiredTraitType acquiredTraitType, int level)
        {
            AcquiredTraitInfo acquiredTraitInfo = new AcquiredTraitInfo();
            acquiredTraitInfo.acquiredTraitType = AcquiredTraitType.None;

            return acquiredTraitInfo;

            /*switch (acquiredTraitType)
            {
                case AcquiredTraitType.HP_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.HP_Plus,

                            health = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.STR_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.STR_Plus,

                            strenght = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.INT_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.INT_Plus,

                            intelligent = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.DEX_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.DEX_Plus,

                            dexterity = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.AGI_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.AGI_Plus,

                            agility = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.VIT_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.VIT_Plus,

                            vitality = 20f + 20f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.HIT_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.HIT_Plus,

                            accuracy = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.AVD_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.AVD_Plus,

                            evasion = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.CRI_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.CRI_Plus,

                            evasion = 0.01f + 0.01f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.RES_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.RES_Plus,

                            physic_Debuff_Resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.ST_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.ST_Plus,

                            battle_Start_Stamina = 2f + 2f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.SPD_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.SPD_Plus,

                            speed = 2f + 2f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Fire_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Fire_Resistance_Plus,

                            fire_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Water_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Water_Resistance_Plus,

                            water_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Wind_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Wind_Resistance_Plus,

                            wind_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Earth_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Earth_Resistance_Plus,

                            earth_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Slash_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Slash_Resistance_Plus,

                            slash_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Blunt_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Blunt_Resistance_Plus,

                            blunt_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }

                case AcquiredTraitType.Thrust_Resistance_Plus:
                    {
                        acquiredTraitInfo = new()
                        {
                            acquiredTraitType = AcquiredTraitType.Thrust_Resistance_Plus,

                            thrust_resistance = 0.02f + 0.02f * level
                        };

                        return acquiredTraitInfo;
                    }
            }
            return acquiredTraitInfo;*/
        }

        public static List<AcquiredTraitInfo> GetAcquiredTraitList(List<AcquiredTrait.PlayerInfoAcquiredTrait> playerInfoAcquiredTraits)
        {
            List<AcquiredTraitInfo> acquiredTraitInfos = new List<AcquiredTraitInfo>();

            foreach (AcquiredTrait.PlayerInfoAcquiredTrait trait in playerInfoAcquiredTraits)
            {
                acquiredTraitInfos.Add(GetAcquiredTraitInfo(trait.acquiredTraitType, trait.traitLevel));
            }

            return acquiredTraitInfos;
        }

        public static AcquiredTraitInfo GetComplexAcquiredTrait(List<AcquiredTraitInfo> acquiredTraitInfos)
        {
            AcquiredTraitInfo compelxTrait = new AcquiredTraitInfo();

            foreach (AcquiredTraitInfo trait in acquiredTraitInfos)
            {
                compelxTrait.acquiredTraitType = AcquiredTraitType.None;

                compelxTrait.health += trait.health;

                compelxTrait.strenght += trait.strenght;

                compelxTrait.intelligent += trait.intelligent;

                compelxTrait.dexterity += trait.dexterity;

                compelxTrait.agility += trait.agility;

                compelxTrait.vitality += trait.vitality;

                compelxTrait.battle_Start_Stamina += trait.battle_Start_Stamina;

                compelxTrait.speed += trait.speed;

                compelxTrait.accuracy += trait.accuracy;

                compelxTrait.evasion += trait.evasion;

                compelxTrait.critical += trait.critical;

                compelxTrait.physic_Debuff_Resistance += trait.physic_Debuff_Resistance;

                compelxTrait.psychic_Debuff_Resistance += trait.psychic_Debuff_Resistance;

                compelxTrait.fire_resistance += trait.fire_resistance;

                compelxTrait.water_resistance += trait.water_resistance;

                compelxTrait.wind_resistance += trait.wind_resistance;

                compelxTrait.earth_resistance += trait.earth_resistance;

                compelxTrait.slash_resistance += trait.slash_resistance;

                compelxTrait.blunt_resistance += trait.blunt_resistance;

                compelxTrait.thrust_resistance += trait.thrust_resistance;
            }

            return compelxTrait;
        }
    }
}