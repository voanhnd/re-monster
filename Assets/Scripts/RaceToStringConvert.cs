﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class RaceToStringConvert : PersistentSingleton<RaceToStringConvert>
    {
        [Serializable]
        private class MonsterRaceString
        {
            [SerializeField] private Monster.Monster.MonsterType raceType;
            [SerializeField] private string raceName;

            public Monster.Monster.MonsterType RaceType => raceType;
            public string RaceName => raceName;
        }

        [SerializeField] private List<MonsterRaceString> raceStrings = new();
        private Dictionary<Monster.Monster.MonsterType, MonsterRaceString> raceMap = new();

        protected override void Awake()
        {
            base.Awake();
            raceMap.Clear();

            for (int i = 0; i < raceStrings.Count; i++)
            {
                if (!raceMap.TryAdd(raceStrings[i].RaceType, raceStrings[i]))
                {
                    Debug.Log("Already exist " + raceStrings[i].RaceName);
                }
            }
        }

        private void Start()
        {
            Destroy(gameObject);
        }

        public string ConvertRace(Monster.Monster.MonsterType raceType)
        {
            if (raceMap.TryGetValue(raceType, out MonsterRaceString value))
            {
                return value.RaceName;
            }

            Debug.Log("Race does not exist or dictionary does not exist");
            return string.Empty;
        }
    }
}