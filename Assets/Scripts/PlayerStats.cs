﻿using Assets.Scripts.Monster;
using Assets.Scripts.Monster.Trait;
using Assets.Scripts.Monster.Skill;
using Assets.Scripts.TrainingArea;
using Defective.JSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlayerStats : PersistentSingleton<PlayerStats>
{
    public MonsterDataManagerScriptObj monsterDataManagerScriptObj;

    public FoodDataManagerScriptObj foodDataManagerScriptObj;

    public FarmDataManagerScriptObj farmDataManagerScriptObj;

    public MonsterActionDataManagerScriptObj monsterActionDataManagerScriptObj;

    public TrainingAreaDataManagerScriptObj trainingAreaDataManagerScriptObj;

    public SkillDataManagerScriptObj skillDataManagerScriptObj;

    public SkillBuffImageManagerScriptObj skillBuffImageManagerScriptObj;

    static string playerDataFileName = "player_data.json";
    public bool load = false;
    public bool save = false;

    public PlayerInfo playerInfo;

    protected override void Awake()
    {
        base.Awake();
        CreateScriptObjectData();

        CreateTestValues();

        _Load();
    }

    // Update is called once per frame
    void Update()
    {
        if (load)
        {
            load = false;

            _Load();
        }

        if (save)
        {
            save = false;

            _Save();
        }
    }

    // Required import from HandleIO.jslib for WebGL persistence.
    [DllImport("__Internal")]
    private static extern void SyncFiles();

    void CreateScriptObjectData()
    {
        monsterDataManagerScriptObj.CreateDictionary();

        foodDataManagerScriptObj.CreateFoodDictionary();

        farmDataManagerScriptObj.CreateFarmDictionary();

        monsterActionDataManagerScriptObj.CreateActionDictionary();

        trainingAreaDataManagerScriptObj.CreateDictionary();

        skillDataManagerScriptObj.CreateDictionary();

        skillBuffImageManagerScriptObj.CreateDictionary();
    }

    private void CreateTestValues()
    {
        if (!string.IsNullOrEmpty(playerInfo.playerName)) return;
        playerInfo.playerName = "Test Player";

        playerInfo.bit = 9999999;

        playerInfo.itemRank2Ticket = 1;
        playerInfo.itemRank3Ticket = 1;

        //Create test farm
        PlayerInfo.PlayerFarmInfo newFarm = new();
        FarmDataScriptObj farmScriptObj = farmDataManagerScriptObj.GetFarmDataObj(Farm.FarmType.Mountain);
        newFarm.farmId = "0";
        newFarm.usingFarmType = farmScriptObj.Farm.FarmTypeVal;
        newFarm.farmTraitType = FarmTrait.FarmTraitType.hp_plus_f;
        playerInfo.AddFarm(newFarm);

        newFarm = new PlayerInfo.PlayerFarmInfo();
        farmScriptObj = farmDataManagerScriptObj.GetFarmDataObj(Farm.FarmType.Plain);
        newFarm.farmId = "1";
        newFarm.usingFarmType = farmScriptObj.Farm.FarmTypeVal;
        newFarm.farmTraitType = FarmTrait.FarmTraitType.hit_plus_f;
        playerInfo.AddFarm(newFarm);

        newFarm = new PlayerInfo.PlayerFarmInfo();
        farmScriptObj = farmDataManagerScriptObj.GetFarmDataObj(Farm.FarmType.WildLand);
        newFarm.farmId = "2";
        newFarm.usingFarmType = farmScriptObj.Farm.FarmTypeVal;
        newFarm.farmTraitType = FarmTrait.FarmTraitType.st_plus_f;
        playerInfo.AddFarm(newFarm);

        playerInfo.SetPlayerFarmInfos(playerInfo.PlayerFarmInfos);


        //Create test monster
        PlayerInfo.PlayerMonsterInfo newMonster = new()
        {
            monsterID = "P1_XSoldier"
        };
        MonsterDataScriptObj monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "0";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.冷静_Cool;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Healthy_Body;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        newMonster = new PlayerInfo.PlayerMonsterInfo
        {
            monsterID = "SignWitch_Aries"
        };
        monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "1";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.忠実_Loyal;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Iron_Heart;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        newMonster = new PlayerInfo.PlayerMonsterInfo
        {
            monsterID = "Golem_Original"
        };
        monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "2";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.勇敢_Brave;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Agility;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        newMonster = new PlayerInfo.PlayerMonsterInfo
        {
            monsterID = "Ossan_01"
        };
        monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "2";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.真面目_Serious;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Insight;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        newMonster = new PlayerInfo.PlayerMonsterInfo
        {
            monsterID = "Kidsfight_Hero"
        };
        monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "2";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.夢見がち_Dreamer;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Hyper_Reactivity;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        newMonster = new PlayerInfo.PlayerMonsterInfo
        {
            monsterID = "IceLime_Eien"
        };
        monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        newMonster.monsterName = monsterScriptObj.Monster.monsterName;
        newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        //newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        //newMonster.usingFarmID = "1";
        newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        newMonster.acquiredTraits.Clear();
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        playerInfo.AddMonster(newMonster);

        //newMonster = new PlayerInfo.PlayerMonsterInfo
        //{
        //    monsterID = "SignWitch_Tarus"
        //};
        //monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        //newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        ////newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        //newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        ////newMonster.usingFarmID = "1";
        //newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        //newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        //newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        //newMonster.acquiredTraits.Clear();
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //playerInfo.AddMonster(newMonster);

        //newMonster = new PlayerInfo.PlayerMonsterInfo
        //{
        //    monsterID = "SignWitch_GeminiA"
        //};
        //monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        //newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        ////newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        //newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        ////newMonster.usingFarmID = "1";
        //newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        //newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        //newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        //newMonster.acquiredTraits.Clear();
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //playerInfo.AddMonster(newMonster);

        //newMonster = new PlayerInfo.PlayerMonsterInfo
        //{
        //    monsterID = "SignWitch_GeminiB"
        //};
        //monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        //newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        ////newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        //newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        ////newMonster.usingFarmID = "1";
        //newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        //newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        //newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        //newMonster.acquiredTraits.Clear();
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //playerInfo.AddMonster(newMonster);

        //newMonster = new PlayerInfo.PlayerMonsterInfo
        //{
        //    monsterID = "SignWitch_Cancer"
        //};
        //monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        //newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        ////newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        //newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        ////newMonster.usingFarmID = "1";
        //newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        //newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        //newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        //newMonster.acquiredTraits.Clear();
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //playerInfo.AddMonster(newMonster);

        //newMonster = new PlayerInfo.PlayerMonsterInfo
        //{
        //    monsterID = "SignWitch_Leo"
        //};
        //monsterScriptObj = monsterDataManagerScriptObj.GetMonsterDataObj(newMonster.monsterID);
        //newMonster.monsterRace = monsterScriptObj.Monster.monsterRace;
        ////newMonster.basicParameters = monsterScriptObj.Monster.basicParameters;
        //newMonster.growthParameters = monsterScriptObj.Monster.growthParameters.Clone();
        ////newMonster.usingFarmID = "1";
        //newMonster.SetPlayerMonsterSkillInfos(monsterScriptObj.Monster.monsterSkillInfos);
        //newMonster.personalityTrait = PersonalityTrait.PersonalityTraitType.食いしん坊_Foodie;
        //newMonster.innateTrait = InnateTrait.InnateTraitType.Sniper;
        //newMonster.acquiredTraits.Clear();
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //newMonster.acquiredTraits.Add(new AcquiredTrait.PlayerInfoAcquiredTrait(0, AcquiredTrait.AcquiredTraitType.None));
        //playerInfo.AddMonster(newMonster);

        //Debug.Log("Create player test data");
    }

    /// <summary>
    /// Using for offline dev
    /// </summary>
    void _Load()
    {
        string filePath = Path.Combine(Application.persistentDataPath, playerDataFileName);

        Debug.Log("Load path = " + filePath);

        if (File.Exists(filePath))
        {
            string dataText = File.ReadAllText(filePath);

            JSONObject rawdata = new(dataText);

            //Debug.Log("rawdata = " + json);

            ParseJsonToPlayerData(rawdata["data"][0].ToString());

            //////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////
            /////
            //_CreateTestValues();
            ////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////
            ///////////////////////////

            Debug.Log("PlayerStats load success");

            Debug.Log("load jsonString = " + dataText);
        }
        else
        {
            Debug.Log("Load path does not exist");

            _Save();
        }
    }

    public void Save()
    {
        save = true;
    }

    /// <summary>
    /// Using for offline dev
    /// </summary>
    void _Save()
    {
        //_CreateTestValues();

        string filePath = Path.Combine(Application.persistentDataPath, playerDataFileName);

        Debug.Log("Save path = " + filePath);

        string jsonString;

        JSONObject jsonRaw = new();

        JSONObject jsonData = new()
        {
            ParsePlayerDataToJson(playerInfo)
        };

        jsonRaw.AddField("data", jsonData);

        /////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////
        ///

        jsonString = jsonRaw.ToString();

        Debug.Log("save jsonString = " + jsonString);

        try
        {
            File.WriteAllText(filePath, jsonString);

            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                SyncFiles();
            }

            Debug.Log("PlayerStats save success");
        }
        catch (FileLoadException e)
        {
            Debug.Log("PlayerStats save error");
        }
    }

    void ParseJsonToPlayerData(string jsonData)
    {
        //Debug.Log("player data = " + jsonData);

        JSONObject json = new(jsonData);

        ////Convert json PlayerInfo

        PlayerInfo newPlayerData = new()
        {
            playerName = playerDataFileName
        };

        TryParseJsonToString(json, "playerName", out newPlayerData.playerName);
        TryParseJsonToFloat(json, "gold", out newPlayerData.bit);
        TryParseJsonToFloat(json, "oas", out newPlayerData.oas);

        ///Get player monster list from json data
        List<PlayerInfo.PlayerMonsterInfo> temp_PlayerMonsterInfos = new();

        JSONObject playerMonsterInfosData = json["playerMonsterInfos"];

        if (playerMonsterInfosData != null)
        {
            for (int i = 0; i < playerMonsterInfosData.count; i++)
            {
                JSONObject monsterDataJson = playerMonsterInfosData[i];

                //Debug.Log("dataJson " + i + " = " + dataJson);

                PlayerInfo.PlayerMonsterInfo newMonsterInfo = new();

                TryParseJsonToString(monsterDataJson, "monsterID", out newMonsterInfo.monsterID);

                TryParseJsonToString(monsterDataJson, "monsterName", out newMonsterInfo.monsterName);

                if (!TryParseJsonToEnum<Monster.MonsterType>(monsterDataJson, "mainRace", out newMonsterInfo.monsterRace.mainRace))
                {
                    newMonsterInfo.monsterRace.subRace = Monster.MonsterType.P1;
                }

                if (!TryParseJsonToEnum<Monster.MonsterType>(monsterDataJson, "subRace", out newMonsterInfo.monsterRace.subRace))
                {
                    newMonsterInfo.monsterRace.subRace = Monster.MonsterType.P1;
                }

                if (!TryParseJsonToEnum<RankTypeEnums>(monsterDataJson, "rankType", out newMonsterInfo.rankType))
                {
                    newMonsterInfo.rankType = RankTypeEnums.f;
                }

                if (!TryParseJsonToEnum<PersonalityTrait.PersonalityTraitType>(monsterDataJson, "personalityTrait", out newMonsterInfo.personalityTrait))
                {
                    newMonsterInfo.personalityTrait = PersonalityTrait.PersonalityTraitType.None;
                }

                if (!TryParseJsonToEnum<InnateTrait.InnateTraitType>(monsterDataJson, "innateTrait", out newMonsterInfo.innateTrait))
                {
                    newMonsterInfo.innateTrait = InnateTrait.InnateTraitType.None;
                }

                JSONObject acquiredTraitsData = playerMonsterInfosData["acquiredTraits"];
                if (acquiredTraitsData != null)
                {
                    List<AcquiredTrait.PlayerInfoAcquiredTrait> tempAcquiredTraits = new();

                    for (int k = 0; k < acquiredTraitsData.count; k++)
                    {
                        JSONObject traitData = acquiredTraitsData[i];

                        AcquiredTrait.PlayerInfoAcquiredTrait newTrait = new();

                        if (!TryParseJsonToEnum<AcquiredTrait.AcquiredTraitType>(traitData, "acquiredTraitType", out newTrait.acquiredTraitType))
                        {
                            newTrait.acquiredTraitType = AcquiredTrait.AcquiredTraitType.None;
                        }

                        TryParseJsonToInt(traitData, "traitLevel", out newTrait.traitLevel);

                        tempAcquiredTraits.Add(newTrait);
                    }

                    newMonsterInfo.acquiredTraits = tempAcquiredTraits;
                }

                TryParseJsonToString(monsterDataJson, "nftID", out newMonsterInfo.nftID);

                //Get trained BasicParameters
                BasicParameters monsterBasicParameters = new();

                TryParseJsonToFloat(monsterDataJson, "health", out monsterBasicParameters.health);
                TryParseJsonToFloat(monsterDataJson, "strenght", out monsterBasicParameters.strenght);
                TryParseJsonToFloat(monsterDataJson, "intelligent", out monsterBasicParameters.intelligent);
                TryParseJsonToFloat(monsterDataJson, "dexterity", out monsterBasicParameters.dexterity);
                TryParseJsonToFloat(monsterDataJson, "agility", out monsterBasicParameters.agility);
                TryParseJsonToFloat(monsterDataJson, "vitality", out monsterBasicParameters.vitality);

                newMonsterInfo.basicParameters = monsterBasicParameters;
                //end get trained BasicParameters

                //Get trained GrowthParameters
                GrowthParameters monsterGrowthParameters = new();

                TryParseJsonToFloat(monsterDataJson, "lifeSpan", out monsterGrowthParameters.lifeSpan);

                TryParseJsonToFloat(monsterDataJson, "bodyTypeValue", out monsterGrowthParameters.bodyTypeValue);

                TryParseJsonToFloat(monsterDataJson, "trainingPolicyValue", out monsterGrowthParameters.trainingPolicyValue);

                TryParseJsonToFloat(monsterDataJson, "affection", out monsterGrowthParameters.affection);

                TryParseJsonToFloat(monsterDataJson, "fatigue", out monsterGrowthParameters.fatigue);

                TryParseJsonToFloat(monsterDataJson, "stress", out monsterGrowthParameters.stress);

                if (!TryParseJsonToEnum<Monster.InjuryType>(monsterDataJson, "injuryType", out monsterGrowthParameters.injuryType))
                {
                    monsterGrowthParameters.injuryType = Monster.InjuryType.None;
                }

                if (!TryParseJsonToEnum<Monster.DiseasesType>(monsterDataJson, "diseasesType", out monsterGrowthParameters.diseasesType))
                {
                    monsterGrowthParameters.diseasesType = Monster.DiseasesType.None;
                }

                TryParseJsonToFloat(monsterDataJson, "energy", out monsterGrowthParameters.energy);
                TryParseJsonToFloat(monsterDataJson, "body", out monsterGrowthParameters.body);
                TryParseJsonToFloat(monsterDataJson, "condition", out monsterGrowthParameters.condition);

                newMonsterInfo.growthParameters = monsterGrowthParameters;
                //end Get trained GrowthParameters

                JSONObject playerActionMonsterData = monsterDataJson["lastMonsterAction"];

                if (playerActionMonsterData != null)
                {
                    MonsterAction newAction = newMonsterInfo.lastMonsterAction;

                    if (!TryParseJsonToEnum<MonsterAction.MonsterActionType>(playerActionMonsterData, "monsterActionType", out newAction.monsterActionType))
                    {
                        newAction.monsterActionType = MonsterAction.MonsterActionType.Empty;
                    }

                    TryParseJsonToBool(playerActionMonsterData, "actionDone", out newAction.actionDone);

                    TryParseJsonToDouble(playerActionMonsterData, "trainingStartUtcUnix", out newAction.trainingStartUtcUnix);

                    TryParseJsonToDouble(playerActionMonsterData, "trainingLastUtcUnix", out newAction.trainingLastUtcUnix);

                    TryParseJsonToInt(playerActionMonsterData, "weekPassed", out newAction.weekPassed);

                    if (!TryParseJsonToEnum<ParameterType>(playerActionMonsterData, "basicTrainingMainParameter", out newAction.basicTrainingMainParameter))
                    {
                        newAction.basicTrainingMainParameter = ParameterType.HP;
                    }

                    if (!TryParseJsonToEnum<TrainingArea.TrainingAreaType>(playerActionMonsterData, "trainingAreaType", out newAction.trainingAreaType))
                    {
                        newAction.trainingAreaType = TrainingArea.TrainingAreaType.Empty;
                    }

                    if (!TryParseJsonToString(playerActionMonsterData, "trainingSkill", out newAction.trainingSkill))
                    {
                        newAction.trainingSkill = string.Empty;
                    }

                    TryParseJsonToBool(playerActionMonsterData, "skillUp", out newAction.skillUp);

                    TryParseJsonToInt(playerActionMonsterData, "actionWeek", out newAction.actionWeek);

                    TryParseJsonToBool(playerActionMonsterData, "isLost", out newAction.isLost);
                    TryParseJsonToInt(playerActionMonsterData, "lostWeek", out newAction.lostWeek);
                    TryParseJsonToFloat(playerActionMonsterData, "lostGold", out newAction.lostGold);

                    //Get exploration item list 

                    JSONObject actionExplorationItemsJsonData = playerActionMonsterData["explorationItems"];

                    if (actionExplorationItemsJsonData != null)
                    {
                        List<ExplorationItem> tempExplorationItemList = new();

                        for (int k = 0; k < actionExplorationItemsJsonData.count; k++)
                        {
                            ExplorationItem newItem = new();

                            if (!TryParseJsonToEnum<ExplorationItem.ItemAcquisitionType>(actionExplorationItemsJsonData, "trainingSkill", out newItem.itemAcquisitionType))
                            {
                                continue;
                            }

                            TryParseJsonToInt(actionExplorationItemsJsonData, "lostWeek", out newItem.discoveryRank);

                            tempExplorationItemList.Add(newItem);
                        }

                        newAction.explorationItems = tempExplorationItemList;
                    }

                    //End get  exploration item list

                    TryParseJsonToFloat(playerActionMonsterData, "foodGold", out newAction.foodGold);

                    //Get lastaction BasicParameters
                    BasicParameters actionBasicParameters = new();

                    TryParseJsonToFloat(playerActionMonsterData, "health", out actionBasicParameters.health);
                    TryParseJsonToFloat(playerActionMonsterData, "strenght", out actionBasicParameters.strenght);
                    TryParseJsonToFloat(playerActionMonsterData, "intelligent", out actionBasicParameters.intelligent);
                    TryParseJsonToFloat(playerActionMonsterData, "dexterity", out actionBasicParameters.dexterity);
                    TryParseJsonToFloat(playerActionMonsterData, "agility", out actionBasicParameters.agility);
                    TryParseJsonToFloat(playerActionMonsterData, "vitality", out actionBasicParameters.vitality);

                    newAction.basicParameters = actionBasicParameters;
                    //end Get lastaction BasicParameters

                    //Get lastaction GrowthParameters
                    GrowthParameters actionGrowthParameters = new();

                    TryParseJsonToFloat(playerActionMonsterData, "lifeSpan", out actionGrowthParameters.lifeSpan);
                    TryParseJsonToFloat(playerActionMonsterData, "bodyTypeValue", out actionGrowthParameters.bodyTypeValue);
                    TryParseJsonToFloat(playerActionMonsterData, "trainingPolicyValue", out actionGrowthParameters.trainingPolicyValue);
                    TryParseJsonToFloat(playerActionMonsterData, "affection", out actionGrowthParameters.affection);
                    TryParseJsonToFloat(playerActionMonsterData, "fatigue", out actionGrowthParameters.fatigue);
                    TryParseJsonToFloat(playerActionMonsterData, "stress", out actionGrowthParameters.stress);

                    if (!TryParseJsonToEnum<Monster.InjuryType>(playerActionMonsterData, "injuryType", out actionGrowthParameters.injuryType))
                    {
                        actionGrowthParameters.injuryType = Monster.InjuryType.None;
                    }

                    if (!TryParseJsonToEnum<Monster.DiseasesType>(playerActionMonsterData, "diseasesType", out actionGrowthParameters.diseasesType))
                    {
                        actionGrowthParameters.diseasesType = Monster.DiseasesType.None;
                    }

                    TryParseJsonToFloat(playerActionMonsterData, "energy", out actionGrowthParameters.energy);
                    TryParseJsonToFloat(playerActionMonsterData, "body", out actionGrowthParameters.body);
                    TryParseJsonToFloat(playerActionMonsterData, "condition", out actionGrowthParameters.condition);

                    newAction.growthParameters = actionGrowthParameters;

                    if (!TryParseJsonToEnum<TrainingArea.TrainingType>(playerActionMonsterData, "trainingType", out newAction.trainingType))
                    {
                        newAction.trainingType = TrainingArea.TrainingType.success;
                    }

                    newMonsterInfo.lastMonsterAction = newAction;
                    //end Get lastaction GrowthParameters
                }

                if (!TryParseJsonToEnum<Food.FoodType>(monsterDataJson, "lastFoodType", out newMonsterInfo.lastFoodType))
                {
                    newMonsterInfo.lastFoodType = Food.FoodType.Empty;
                }

                TryParseJsonToString(monsterDataJson, "usingFarmID", out newMonsterInfo.usingFarmID);

                //Get Monster skill list from json
                List<Monster.MonsterSkillInfo> temp_MonsterSkillInfos = new();

                JSONObject monsterSkillInfosDataJson = monsterDataJson["monsterSkillInfos"];

                if (monsterSkillInfosDataJson != null)
                {
                    for (int k = 0; k < monsterSkillInfosDataJson.count; k++)
                    {
                        JSONObject monsterSkillDataJson = monsterSkillInfosDataJson[k];

                        Monster.MonsterSkillInfo skilInfo = new();

                        if (!TryParseJsonToString(monsterSkillDataJson, "skillID", out skilInfo.skillID))
                        {
                            continue;
                        }

                        TryParseJsonToInt(monsterSkillDataJson, "skillLevel", out skilInfo.skillLevel);

                        temp_MonsterSkillInfos.Add(skilInfo);
                    }

                    newMonsterInfo.SetPlayerMonsterSkillInfos(temp_MonsterSkillInfos);
                }

                //End Get Monster skill list from json

                //Get Monster battle skill list from json
                List<string> monsterSelectedSkills = new();

                JSONObject monsterSkillBattleDataJson = monsterDataJson["selectedBattleSkills"];

                if (monsterSkillBattleDataJson != null)
                {
                    for (int k = 0; k < monsterSkillBattleDataJson.count; k++)
                    {
                        string selectedSkillID = monsterSkillBattleDataJson[k].ToString().Replace("\"", "");

                        monsterSelectedSkills.Add(selectedSkillID);
                    }

                    newMonsterInfo.SelectBattleSkill(monsterSelectedSkills);
                }

                //End Get Monster battle skill list from json

                //Add monster info
                temp_PlayerMonsterInfos.Add(newMonsterInfo);
            }

            newPlayerData.SetPlayerMonsterInfos(temp_PlayerMonsterInfos);
        }
        //////////////////////////
        ///////////////////////////
        ///

        ///Get player farm list from json data

        List<PlayerInfo.PlayerFarmInfo> temp_PlayerFarmInfos = new();

        JSONObject playerFarmInfosData = json["playerFarmInfos"];

        if (playerFarmInfosData != null)
        {
            for (int i = 0; i < playerFarmInfosData.count; i++)
            {
                JSONObject dataJson = playerFarmInfosData[i];

                //Debug.Log("dataJson " + i + " = " + dataJson);

                PlayerInfo.PlayerFarmInfo newInfo = new();

                if (!TryParseJsonToEnum<Farm.FarmType>(dataJson, "usingFarmType", out newInfo.usingFarmType))
                {
                    continue;
                }

                TryParseJsonToString(dataJson, "farmId", out newInfo.farmId);

                TryParseJsonToString(dataJson, "nftID", out newInfo.nftID);

                if (!TryParseJsonToEnum<FarmTrait.FarmTraitType>(dataJson, "farmTraitType", out newInfo.farmTraitType))
                {
                    newInfo.farmTraitType = FarmTrait.FarmTraitType.none;
                }

                string monsterId = string.Empty;

                TryParseJsonToString(dataJson, "monsterId", out monsterId);
                newInfo.SetMonsterId(monsterId);

                temp_PlayerFarmInfos.Add(newInfo);
            }

            newPlayerData.SetPlayerFarmInfos(temp_PlayerFarmInfos);
        }

        playerInfo = newPlayerData;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        /////

        Debug.Log("ParseJsonToPlayerData done");
    }

    JSONObject ParsePlayerDataToJson(PlayerInfo playerInfo)
    {
        ////Convert PlayerInfo to Json
        ///
        JSONObject jsonPlayerData = new();

        jsonPlayerData.AddField("playerName", playerInfo.playerName);

        jsonPlayerData.AddField("gold", playerInfo.bit);

        jsonPlayerData.AddField("oas", playerInfo.oas);

        ///Convert player monster list To json data

        JSONObject jsonPlayerMonsterDataList = new();

        foreach (PlayerInfo.PlayerMonsterInfo info in playerInfo.PlayerMonsterInfos)
        {
            JSONObject jsonPlayerMonsterData = new();

            jsonPlayerMonsterData.AddField("monsterID", info.monsterID);

            jsonPlayerMonsterData.AddField("monsterName", info.monsterName);

            jsonPlayerMonsterData.AddField("mainRace", info.monsterRace.mainRace.ToString());
            jsonPlayerMonsterData.AddField("subRace", info.monsterRace.subRace.ToString());

            jsonPlayerMonsterData.AddField("rankType", info.rankType.ToString());

            jsonPlayerMonsterData.AddField("personalityTrait", info.personalityTrait.ToString());

            jsonPlayerMonsterData.AddField("innateTrait", info.innateTrait.ToString());

            // AcquiredTraits
            JSONObject jsonAcquiredTraitList = new();

            foreach (AcquiredTrait.PlayerInfoAcquiredTrait acquiredTrait in info.acquiredTraits)
            {
                JSONObject acquiredTraitJson = new();

                acquiredTraitJson.AddField("acquiredTraitType", acquiredTrait.acquiredTraitType.ToString());
                acquiredTraitJson.AddField("traitLevel", acquiredTrait.traitLevel);

                jsonAcquiredTraitList.Add(acquiredTraitJson);
            }

            jsonPlayerMonsterData.AddField("acquiredTraits", jsonAcquiredTraitList);
            // AcquiredTraits

            jsonPlayerMonsterData.AddField("nftID", info.nftID);

            // BasicParameters
            jsonPlayerMonsterData.AddField("health", info.basicParameters.health);
            jsonPlayerMonsterData.AddField("strenght", info.basicParameters.strenght);
            jsonPlayerMonsterData.AddField("intelligent", info.basicParameters.intelligent);
            jsonPlayerMonsterData.AddField("dexterity", info.basicParameters.dexterity);
            jsonPlayerMonsterData.AddField("agility", info.basicParameters.agility);
            jsonPlayerMonsterData.AddField("vitality", info.basicParameters.vitality);
            //end BasicParameters

            /// GrowthParameters
            jsonPlayerMonsterData.AddField("lifeSpan", info.growthParameters.lifeSpan);
            jsonPlayerMonsterData.AddField("bodyTypeValue", info.growthParameters.bodyTypeValue);
            jsonPlayerMonsterData.AddField("trainingPolicyValue", info.growthParameters.trainingPolicyValue);
            jsonPlayerMonsterData.AddField("affection", info.growthParameters.affection);
            jsonPlayerMonsterData.AddField("fatigue", info.growthParameters.fatigue);
            jsonPlayerMonsterData.AddField("stress", info.growthParameters.stress);
            jsonPlayerMonsterData.AddField("injuryType", info.growthParameters.injuryType.ToString());
            jsonPlayerMonsterData.AddField("diseasesType", info.growthParameters.diseasesType.ToString());

            jsonPlayerMonsterData.AddField("energy", info.growthParameters.energy.ToString());
            jsonPlayerMonsterData.AddField("body", info.growthParameters.body.ToString());
            jsonPlayerMonsterData.AddField("condition", info.growthParameters.condition.ToString());

            // end GrowthParameters

            jsonPlayerMonsterData.AddField("lastFoodType", info.lastFoodType.ToString());

            jsonPlayerMonsterData.AddField("usingFarmID", info.usingFarmID.ToString());

            {
                JSONObject jsonActionMonsterData = new();

                jsonActionMonsterData.AddField("monsterActionType", info.lastMonsterAction.monsterActionType.ToString());

                jsonActionMonsterData.AddField("actionDone", info.lastMonsterAction.actionDone);

                jsonActionMonsterData.AddField("trainingStartUtcUnix", info.lastMonsterAction.trainingStartUtcUnix);

                jsonActionMonsterData.AddField("trainingLastUtcUnix", info.lastMonsterAction.trainingLastUtcUnix);

                jsonActionMonsterData.AddField("weekPassed", info.lastMonsterAction.weekPassed);

                jsonActionMonsterData.AddField("basicTrainingMainParameter", info.lastMonsterAction.basicTrainingMainParameter.ToString());

                jsonActionMonsterData.AddField("trainingAreaType", info.lastMonsterAction.trainingAreaType.ToString());

                jsonActionMonsterData.AddField("trainingSkill", info.lastMonsterAction.trainingSkill.ToString());

                jsonActionMonsterData.AddField("skillUp", info.lastMonsterAction.skillUp);

                jsonActionMonsterData.AddField("actionWeek", info.lastMonsterAction.actionWeek);

                jsonActionMonsterData.AddField("isLost", info.lastMonsterAction.isLost);
                jsonActionMonsterData.AddField("lostWeek", info.lastMonsterAction.lostWeek);
                jsonActionMonsterData.AddField("lostGold", info.lastMonsterAction.lostGold);

                //action exploration item list
                JSONObject jsonActionExplorationItemsList = new();

                foreach (ExplorationItem item in info.lastMonsterAction.explorationItems)
                {
                    JSONObject itemJson = new();

                    itemJson.AddField("itemAcquisitionType", item.itemAcquisitionType.ToString());
                    itemJson.AddField("discoveryRank", item.discoveryRank);

                    jsonActionExplorationItemsList.Add(itemJson);
                }

                jsonActionMonsterData.AddField("explorationItems", jsonActionExplorationItemsList);
                //end action exploration item list

                jsonActionMonsterData.AddField("foodGold", info.lastMonsterAction.foodGold);

                // BasicParameters
                jsonActionMonsterData.AddField("health", info.lastMonsterAction.basicParameters.health);
                jsonActionMonsterData.AddField("strenght", info.lastMonsterAction.basicParameters.strenght);
                jsonActionMonsterData.AddField("intelligent", info.lastMonsterAction.basicParameters.intelligent);
                jsonActionMonsterData.AddField("dexterity", info.lastMonsterAction.basicParameters.dexterity);
                jsonActionMonsterData.AddField("agility", info.lastMonsterAction.basicParameters.agility);
                jsonActionMonsterData.AddField("vitality", info.lastMonsterAction.basicParameters.vitality);
                //end BasicParameters

                // GrowthParameters
                jsonActionMonsterData.AddField("lifeSpan", info.lastMonsterAction.growthParameters.lifeSpan);
                jsonActionMonsterData.AddField("bodyTypeValue", info.lastMonsterAction.growthParameters.bodyTypeValue);
                jsonActionMonsterData.AddField("trainingPolicyValue", info.lastMonsterAction.growthParameters.trainingPolicyValue);
                jsonActionMonsterData.AddField("affection", info.lastMonsterAction.growthParameters.affection);
                jsonActionMonsterData.AddField("fatigue", info.lastMonsterAction.growthParameters.fatigue);
                jsonActionMonsterData.AddField("stress", info.lastMonsterAction.growthParameters.stress);
                jsonActionMonsterData.AddField("injuryType", info.lastMonsterAction.growthParameters.injuryType.ToString());
                jsonActionMonsterData.AddField("diseasesType", info.lastMonsterAction.growthParameters.diseasesType.ToString());

                jsonActionMonsterData.AddField("energy", info.lastMonsterAction.growthParameters.energy);
                jsonActionMonsterData.AddField("body", info.lastMonsterAction.growthParameters.body);
                jsonActionMonsterData.AddField("condition", info.lastMonsterAction.growthParameters.condition);

                jsonActionMonsterData.AddField("trainingType", info.lastMonsterAction.trainingType.ToString());

                //end GrowthParameters

                jsonPlayerMonsterData.AddField("lastMonsterAction", jsonActionMonsterData);
            }

            //monster skill list
            {
                JSONObject jsonMonsterSkillDataList = new();

                for (int i = 0; i < info.MonsterSkillInfos.Count; i++)
                {
                    Monster.MonsterSkillInfo skillInfo = info.MonsterSkillInfos[i];
                    JSONObject jsonSkillData = new();

                    jsonSkillData.AddField("skillID", skillInfo.skillID.ToString());

                    jsonSkillData.AddField("skillLevel", skillInfo.skillLevel);

                    jsonMonsterSkillDataList.Add(jsonSkillData);
                }

                jsonPlayerMonsterData.AddField("monsterSkillInfos", jsonMonsterSkillDataList);
            }
            //end monster skill list

            //monster selected battle skill list
            {
                JSONObject jsonUsingSkillDataList = new();

                for (int i = 0; i < info.SelectedBattleSkillsId.Count; i++)
                {
                    string skillID = info.SelectedBattleSkillsId[i];
                    jsonUsingSkillDataList.Add(skillID.ToString());
                }

                jsonPlayerMonsterData.AddField("selectedBattleSkills", jsonUsingSkillDataList);
            }
            //end selected battle skill list

            jsonPlayerMonsterDataList.Add(jsonPlayerMonsterData);
        }

        jsonPlayerData.AddField("playerMonsterInfos", jsonPlayerMonsterDataList);

        ///End convert player monster list To json data
        ///

        ///Convert player farm list To json data

        JSONObject jsonPlayerFarmDataList = new();

        foreach (PlayerInfo.PlayerFarmInfo info in playerInfo.PlayerFarmInfos)
        {
            JSONObject jsonPlayerFarmData = new();

            jsonPlayerFarmData.AddField("usingFarmType", info.usingFarmType.ToString());

            jsonPlayerFarmData.AddField("farmId", info.farmId);

            jsonPlayerFarmData.AddField("nftID", info.nftID);

            jsonPlayerFarmData.AddField("farmTraitType", info.farmTraitType.ToString());

            jsonPlayerFarmData.AddField("monsterId", info.MonsterId);

            jsonPlayerFarmDataList.Add(jsonPlayerFarmData);
        }

        jsonPlayerData.AddField("playerFarmInfos", jsonPlayerFarmDataList);

        ///End convert player farm list To json data

        Debug.Log("ParsePlayerDataToJson done");

        return jsonPlayerData;
    }

    public static bool TryParseJsonToBool(JSONObject jsonData, string keyStr, out bool result)
    {
        result = false;

        if (jsonData[keyStr] != null && bool.TryParse(jsonData[keyStr].ToString().Replace("\"", ""), out result))
        {
            return true;
        }

        return false;
    }

    public static bool TryParseJsonToInt(JSONObject jsonData, string keyStr, out int result)
    {
        result = 0;

        if (jsonData[keyStr] != null && int.TryParse(jsonData[keyStr].ToString().Replace("\"", ""), out result))
        {
            return true;
        }

        return false;
    }

    public static bool TryParseJsonToFloat(JSONObject jsonData, string keyStr, out float result)
    {
        result = 0f;

        if (jsonData[keyStr] != null && float.TryParse(jsonData[keyStr].ToString().Replace("\"", ""), out result))
        {
            return true;
        }

        return false;
    }

    public static bool TryParseJsonToDouble(JSONObject jsonData, string keyStr, out double result)
    {
        result = 0f;

        if (jsonData[keyStr] != null && double.TryParse(jsonData[keyStr].ToString().Replace("\"", ""), out result))
        {
            return true;
        }

        return false;
    }

    public static bool TryParseJsonToString(JSONObject jsonData, string keyStr, out string result)
    {
        result = "";

        if (jsonData[keyStr] != null)
        {
            result = jsonData[keyStr].ToString().Replace("\"", "");

            return true;
        }

        return false;
    }

    public static bool TryParseJsonToEnum<TEnum>(JSONObject jsonData, string keyStr, out TEnum result) where TEnum : struct
    {
        result = default;

        if (jsonData[keyStr] != null)
        {
            string enumStr = jsonData[keyStr].ToString().Replace("\"", "");

            if (Enum.TryParse<TEnum>(enumStr, out result))
            {
                return true;
            }
        }

        return false;
    }
}
