using UnityEngine;

public class GameobjectExtension : MonoBehaviour
{
    public static GameObject GetChildGameObjectByName(GameObject fromGameObject, string withName)
    {
        Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in ts) if (t.gameObject.name == withName) return t.gameObject;
        return null;
    }
}
