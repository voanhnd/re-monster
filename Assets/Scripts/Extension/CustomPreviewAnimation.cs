using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPreviewAnimation : MonoBehaviour
{
    public float frame = 0f;

    public AnimationClip animationClip;

    private void OnValidate()
    {
        if(animationClip != null)
        {
            float frameToTime = frame / animationClip.frameRate;

            animationClip.SampleAnimation(gameObject, frameToTime);
        }
    }
}
