using MetaMask.Models;
using System;
using UnityEngine;

namespace MetaMask.Unity.Samples
{

    public class MetaMaskDemo : MonoBehaviour
    {

        #region Events

        /// <summary>Raised when the wallet is connected.</summary>
        public event EventHandler onWalletConnected;
        /// <summary>Raised when the wallet is disconnected.</summary>
        public event EventHandler onWalletDisconnected;
        /// <summary>Raised when the wallet is ready.</summary>
        public event EventHandler onWalletReady;
        /// <summary>Raised when the wallet is paused.</summary>
        public event EventHandler onWalletPaused;
        /// <summary>Raised when the user signs and sends the document.</summary>
        public event EventHandler onSignSend;
        /// <summary>Occurs when a transaction is sent.</summary>
        public event EventHandler onTransactionSent;
        /// <summary>Raised when the transaction result is received.</summary>
        /// <param name="e">The event arguments.</param>
        public event EventHandler<MetaMaskEthereumRequestResultEventArgs> onTransactionResult;

        #endregion

        #region Fields

        /// <summary>The configuration for the MetaMask client.</summary>
        [SerializeField]
        protected MetaMaskConfig config;

        #endregion

        #region Unity Methods

        /// <summary>Initializes the MetaMaskUnity instance.</summary>
        private void Awake()
        {
            MetaMaskUnity.Instance.Initialize();
            MetaMaskUnity.Instance.Wallet.WalletAuthorized += walletConnected;
            MetaMaskUnity.Instance.Wallet.WalletDisconnected += walletDisconnected;
            MetaMaskUnity.Instance.Wallet.WalletReady += walletReady;
            MetaMaskUnity.Instance.Wallet.WalletPaused += walletPaused;
            MetaMaskUnity.Instance.Wallet.EthereumRequestResultReceived += TransactionResult;
        }

        private void OnDisable()
        {
            MetaMaskUnity.Instance.Wallet.WalletAuthorized -= walletConnected;
            MetaMaskUnity.Instance.Wallet.WalletDisconnected -= walletDisconnected;
            MetaMaskUnity.Instance.Wallet.WalletReady -= walletReady;
            MetaMaskUnity.Instance.Wallet.WalletPaused -= walletPaused;
            MetaMaskUnity.Instance.Wallet.EthereumRequestResultReceived -= TransactionResult;
        }

        #endregion

        #region Event Handlers

        /// <summary>Raised when the transaction result is received.</summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        public void TransactionResult(object sender, MetaMaskEthereumRequestResultEventArgs e)
        {
            onTransactionResult?.Invoke(sender, e);
        }

        /// <summary>Raised when the wallet is connected.</summary>
        private void walletConnected(object sender, EventArgs e)
        {
            onWalletConnected?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>Raised when the wallet is disconnected.</summary>
        private void walletDisconnected(object sender, EventArgs e)
        {
            onWalletDisconnected?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>Raised when the wallet is ready.</summary>
        private void walletReady(object sender, EventArgs e)
        {
            onWalletReady?.Invoke(this, EventArgs.Empty);

        }

        /// <summary>Raised when the wallet is paused.</summary>
        private void walletPaused(object sender, EventArgs e)
        {
            onWalletPaused?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        #region Public Methods

        public void OpenDeepLink()
        {
            if (Transports.Unity.UI.MetaMaskUnityUITransport.DefaultInstance != null)
            {
                Transports.Unity.UI.MetaMaskUnityUITransport.DefaultInstance.OpenDeepLink();
            }
        }

        /// <summary>Calls the connect method to the Metamask Wallet.</summary>
        public void Connect()
        {
            MetaMaskUnity.Instance.Wallet.Connect();
        }

        /// <summary>Sends a transaction to the Ethereum network.</summary>
        /// <param name="transactionParams">The parameters of the transaction.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public async void SendTransaction()
        {
            var transactionParams = new MetaMaskTransaction
            {
                To = "0x275Ae07CAF229C824563220fcD303F22034c8f33",
                From = MetaMaskUnity.Instance.Wallet.SelectedAddress,
                Value = "0x0",
                Data = "0x6057361d0000000000000000000000000000000000000000000000000000000000000001",
                Chain = "20197"
            };

            var request = new MetaMaskEthereumRequest
            {
                Method = "eth_sendTransaction",
                Parameters = new MetaMaskTransaction[] { transactionParams }
            };
            onTransactionSent?.Invoke(this, EventArgs.Empty);
            await MetaMaskUnity.Instance.Wallet.Request(request);

        }

        /// <summary>Signs a message with the user's private key.</summary>
        /// <param name="msgParams">The message to sign.</param>
        /// <exception cref="InvalidOperationException">Thrown when the application isn't in foreground.</exception>
        public async void Sign()
        {
            string msgParams = LogIn.Instance.Message;

            string from = MetaMaskUnity.Instance.Wallet.SelectedAddress;

            LogIn.Instance.WalletAddress = from;

            var paramsArray = new string[] { from, msgParams };

            var request = new MetaMaskEthereumRequest
            {
                Method = "personal_sign",
                Parameters = paramsArray
            };
            onSignSend?.Invoke(this, EventArgs.Empty);

            var req = MetaMaskUnity.Instance.Wallet.Request(request);
            await req;

            LogIn.Instance.Signature = req.Result.ToString();

            LogIn.Instance.ApiLogInWithWallet();
        }

        #endregion
    }

}